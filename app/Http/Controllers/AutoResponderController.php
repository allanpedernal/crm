<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Remitter;
use App\Models\Remittance;
use App\Models\RemitterAddress;
use App\Models\RemitterIdentification; 
use App\Models\RemitterContact;
use App\Models\Library\LibraryReceiveOptionList;
use App\Mail\RegisterMail;
use App\Mail\AutoResponderMail;
use Illuminate\Support\Facades\Mail;

use Swift_Transport;
use Swift_Message;
use Swift_Mailer;

use Auth;

class AutoResponderController extends Controller
{
    //
    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {

    }

    public function send_auto_responder( $id_type, $id )
    {
        
      $remittance = new Remittance;
        
      $remittance_id = $remittance -> get_remittance_id( $id_type, $id );

      $email_content = $this -> validate_remittance_and_get_auto_responder_email_content( $remittance_id );

      $autoResonderMail = new AutoResponderMail( 

                                                $email_content[ 'email_content' ]

                                                , 

                                                $this->get_transaction_details( $remittance_id ) 

                                               )

      ;

      if( $email_content[ 'success' ] )
      {

        Mail::to( 'peterjohnjimenez@gmail.com' )

              ->send( $autoResonderMail  )

        ;
 
      }

      return $email_content;

    }

    private function validate_remittance_and_get_auto_responder_email_content( $remittance_id )
    {

      $response = null;

      try 
      {
                
        //INITIALIZE DEFAULT RESPONSE

        $response = array( 'success' => true, 'email_content' => '', 'error_message' => array() );

        $remittance = new Remittance;

        if( $remittance->is_transaction_in_the_same_day( $remittance_id ) )
        {

          if( $remittance->is_autoresponder_accepted( $remittance_id ) )
          {

            if( $this->validate_auto_responder( $remittance_id ) )
            {

              //initialize response

              $response[ 'success' ] = true;

              $response[ 'email_content' ] = '';

              $response[ 'error_message' ] = array();

              if( !is_null( $this->get_sunday_email_content( $remittance_id ) ) )
              {

                $response[ 'email_content' ] = $this->get_sunday_email_content( $remittance_id );

              }

              elseif ( !is_null($this->get_saturday_email_content( $remittance_id ) ) ) 
              {
                
                $response[ 'email_content' ] = $this->get_saturday_email_content( $remittance_id );

              }

              elseif( !is_null($this->get_monday_to_friday_email( $remittance_id ) ) )
              {

                $response[ 'email_content' ] = $this->get_monday_to_friday_email( $remittance_id );

              }
              else
              {

                $response[ 'success' ] = false;

                $response[ 'error_message' ][] = 'Auto responder didn\'t satify the condition!';

              }

            }

            else
            {

              $response[ 'success' ] = false;

              $response[ 'error_message' ][] = 'Auto responder didn\'t satify the condition!';

            }

          }

          else 
          {

            $response[ 'success' ] = false;

            $response[ 'error_message' ][] = 'Invalid Remittance status!';

          }

        } 

        else
        {

          $response[ 'success' ] = false;

          $response[ 'error_message' ][] = 'Remittance is not in the current day!';

        }          

      }

      catch( Exception $e ) {   

        echo "<pre>";  

        print_r( $e -> getMessage() );  

        exit;
      
      }

      return $response;

    }

    private function validate_auto_responder( $remittance_id )
    {

      $is_remittance_id_null = empty( $remittance_id ) || is_null( $remittance_id );

      $send_auto_responder = false;

      if( !$is_remittance_id_null ) 
      {

        $send_auto_responder = $this->validate_sunday( $remittance_id )

                               ||

                               $this->validate_saturday_12am_to_11am( $remittance_id )

                               ||

                               $this->validate_saturday_12am_to_11am_bdo_metrobank_cashpick( $remittance_id )

                               ||

                               $this->validate_saturday_12am_to_11am_door_to_door( $remittance_id )

                               ||

                               $this->validate_saturday_11am_onwards( $remittance_id )

                               ||

                               $this->validate_monday_to_friday_12am_to_6pm_bdo_metrobank_cashpick( $remittance_id )

                               ||

                               $this->validate_monday_to_friday_12am_to_6pm_door_to_door( $remittance_id )

                               ||

                               $this->validate_monday_to_saturday_12am_to_11am_non_bdo_metrobank( $remittance_id )

                               ||

                               $this->validate_monday_to_friday_11am_onwards_to_6pm_non_bdo_metrobank( $remittance_id )

                               ||

                               $this->validate_monday_to_friday_after_6pm( $remittance_id )                                    

        ;

      }

      return $send_auto_responder;

    }

    //satisfy_recipient_bank is is_bdo_or_metrobank || is_cash_pick_up
    private function is_bdo_or_metrobank( $remittance_id )
    {

      $remittance = new Remittance;

      $bank_source_id = $remittance -> get_bank_source_id( $remittance_id );

      return ( 

              ( $bank_source_id == 3 ) //BDO

              ||

              ( $bank_source_id == 6 ) //Metro Bank

             )

      ;

    }

    //satisfy_recipient_bank is is_bdo_or_metrobank || is_cash_pick_up
    private function is_cash_pick_up( $remittance_id )
    {

      $remittance = new Remittance;

      $receive_option_list = new LibraryReceiveOptionList;

      $remitter_bank_source_id = $remittance -> get_bank_source_id( $remittance_id );

      $receive_option_details = $receive_option_list -> get_last_receive_options_details( $remitter_bank_source_id );

      return $receive_option_details[ 'receive_options_id' ] == 1;

    }

    private function is_door_to_door( $remittance_id )
    {

      $remittance = new Remittance;

      $receive_option_list = new LibraryReceiveOptionList;

      $remitter_bank_source_id = $remittance -> get_bank_source_id( $remittance_id );

      $receive_option_details = $receive_option_list -> get_last_receive_options_details( $remitter_bank_source_id );

      return $receive_option_details[ 'receive_options_id' ] == 4;

    }

    //$satisfy_not_recipient_bank
    private function is_other_banks( $remittance_id )
    {

      return !$this->is_bdo_or_metrobank( $remittance_id ) 

             && 

             !$this->is_cash_pick_up( $remittance_id )

             &&

             !$this->is_door_to_door( $remittance_id );  

    }

    //Sunday condition
    private function validate_sunday( $remittance_id )
    {

      $remittance = new Remittance;

      $is_sunday = $remittance -> is_sunday( $remittance_id );

      $is_holiday = $remittance -> is_holiday( $remittance_id );

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return ( $is_sunday || $is_holiday ) && $is_autoresponder_accepted;

    }

    private function validate_saturday_12am_to_11am( $remittance_id )
    {

      $remittance = new Remittance;

      $is_saturday = $remittance -> is_saturday( $remittance_id );

      $is_monday_to_saturday_12am_to_11am_non_bdo_metrobank = $remittance -> is_monday_to_saturday_12am_to_11am_non_bdo_metrobank( $remittance_id ); 

      $is_other_banks = $this -> is_other_banks( $remittance_id );

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return $is_monday_to_saturday_12am_to_11am_non_bdo_metrobank

             && 

             $is_other_banks

             &&

             $is_saturday

             &&

             $is_autoresponder_accepted

      ;

    }

    private function validate_saturday_12am_to_11am_bdo_metrobank_cashpick( $remittance_id )
    {

      $remittance = new Remittance;

      $is_saturday = $remittance -> is_saturday( $remittance_id );

      $is_saturday_12am_to_11am_bdo_metrobank_cashpick = $remittance -> is_saturday_12am_to_11am_bdo_metrobank_cashpick( $remittance_id );

      $is_bdo_or_metrobank_or_cash_pick_up = $this->is_bdo_or_metrobank( $remittance_id ) || $this->is_cash_pick_up( $remittance_id ); 

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return $is_saturday

             &&

             $is_saturday_12am_to_11am_bdo_metrobank_cashpick

             &&

             $is_bdo_or_metrobank_or_cash_pick_up

             &&

             $is_autoresponder_accepted
      
      ;

    }

    private function validate_saturday_12am_to_11am_door_to_door( $remittance_id )
    {

      $remittance = new Remittance;

      $is_saturday = $remittance -> is_saturday( $remittance_id );

      $is_saturday_12am_to_11am_door_to_door = $remittance -> is_saturday_12am_to_11am_door_to_door( $remittance_id );

      $is_door_to_door = $this -> is_door_to_door( $remittance_id );

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return $is_saturday

             &&

             $is_saturday_12am_to_11am_door_to_door

             &&

             $is_door_to_door

             &&

             $is_autoresponder_accepted

      ;        

    } 

    private function validate_saturday_11am_onwards( $remittance_id )
    {

      $remittance = new Remittance;

      $is_saturday = $remittance -> is_saturday( $remittance_id );

      $is_saturday_11am_onwards = $remittance -> is_saturday_11am_onwards( $remittance_id );

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return $is_saturday

             &&

             $is_saturday_11am_onwards

             &&

             $is_autoresponder_accepted

      ;       

    } 

    private function validate_monday_to_friday_12am_to_6pm_bdo_metrobank_cashpick( $remittance_id )
    {

      $remittance = new Remittance;

      $is_saturday = $remittance -> is_saturday( $remittance_id );

      $is_monday_to_friday_12am_to_6pm_bdo_metrobank_cashpick = $remittance -> is_monday_to_friday_12am_to_6pm_bdo_metrobank_cashpick( $remittance_id );

      $is_bdo_or_metrobank_or_cash_pick_up = $this->is_bdo_or_metrobank( $remittance_id ) || $this->is_cash_pick_up( $remittance_id );

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return !$is_saturday

             &&

             $is_monday_to_friday_12am_to_6pm_bdo_metrobank_cashpick

             &&

             $is_bdo_or_metrobank_or_cash_pick_up

             &&

             $is_autoresponder_accepted

      ;               

    }

    private function validate_monday_to_friday_12am_to_6pm_door_to_door( $remittance_id )
    {

      $remittance = new Remittance;

      $is_saturday = $remittance -> is_saturday( $remittance_id );
      
      $is_monday_to_friday_12am_to_6pm_door_to_door = $remittance -> is_monday_to_friday_12am_to_6pm_door_to_door( $remittance_id );

      $is_door_to_door = $this->is_door_to_door( $remittance_id );

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return !$is_saturday

             &&

             $is_monday_to_friday_12am_to_6pm_door_to_door
             
             &&

             $is_door_to_door

             &&

             $is_autoresponder_accepted                

      ;

    }

    private function validate_monday_to_saturday_12am_to_11am_non_bdo_metrobank( $remittance_id ) 
    {

      $remittance = new Remittance;

      $is_saturday = $remittance -> is_saturday( $remittance_id );

      $is_monday_to_saturday_12am_to_11am_non_bdo_metrobank = $remittance -> is_monday_to_saturday_12am_to_11am_non_bdo_metrobank( $remittance_id );

      $is_other_banks = $this->is_other_banks( $remittance_id );

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return !$is_saturday

             &&

             $is_monday_to_saturday_12am_to_11am_non_bdo_metrobank

             &&

             $is_other_banks

             &&

             $is_autoresponder_accepted

      ;

    }

    private function validate_monday_to_friday_11am_onwards_to_6pm_non_bdo_metrobank( $remittance_id )
    {

      $remittance = new Remittance;

      $is_saturday = $remittance -> is_saturday( $remittance_id);

      $is_monday_to_friday_11am_onwards_to_6pm_non_bdo_metrobank = $remittance -> is_monday_to_friday_11am_onwards_to_6pm_non_bdo_metrobank( $remittance_id );

      $is_other_banks = $this->is_other_banks( $remittance_id );

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return !$is_saturday

             &&

             $is_monday_to_friday_11am_onwards_to_6pm_non_bdo_metrobank

             &&

             $is_other_banks

             &&

             $is_autoresponder_accepted

      ; 

    }    

    private function validate_monday_to_friday_after_6pm( $remittance_id )
    {

      $remittance = new Remittance;

      $is_monday_to_friday_after_6pm = $remittance -> is_monday_to_friday_after_6pm( $remittance_id );

      $is_autoresponder_accepted = $remittance -> is_autoresponder_accepted( $remittance_id );

      return $is_monday_to_friday_after_6pm

             &&

             $is_autoresponder_accepted

      ;        

    }

    private function get_sunday_email_content( $remittance_id )
    {

      $email_content = null;

      $remittance = new Remittance;

      $is_poli = $remittance->is_poli( $remittance_id );

      $is_non_poli = $remittance->is_remittance_instruction_received( $remittance_id );

      $bank_source_id = $remittance->get_bank_source_id( $remittance_id );

      $is_bdo_or_metrobank_or_cash_pick_up = $this->is_bdo_or_metrobank( $remittance_id ) || $this->is_cash_pick_up( $remittance_id ); 

      $is_other_banks = $this->is_other_banks( $remittance_id );

      $is_door_to_door = $this->is_door_to_door( $remittance_id );

      if( $this->validate_sunday( $remittance_id ) )
      {

        //VALIDATE IF AUTO RESPONDER TYPE IS POLI AND BANK REMITTANCE SOURCE IS EQUAL TO 5 = POLI
        if( $is_poli && $bank_source_id == 5 ) 
        {

          //BDO METROBANK CASHPICKUP
          if( $is_bdo_or_metrobank_or_cash_pick_up ) {

            $email_content = 'emails.templates.bdo_metrobank_cashpickup_sunday_poli';
            
          }

          //NOT BDO METROBANK CASHPICKUP DOOR TO DOOR - OTHER BANKS
          if( $is_other_banks ) {

            $email_content = 'emails.templates.non_bdo_metrobank_cashpickup_sunday_poli';

          }

          //DOOR TO DOOR
          if( $is_door_to_door ) {

            $email_content = 'emails.templates.sunday_poli';

          }

        }

        //VALIDATE IF AUTO RESPONDER TYPE IS NON POLI AND BANK REMITTANCE SOURCE IS NOT EQUAL TO 5 = POLI
        if( $is_non_poli && $bank_source_id != 5 ) 
        {

          //BDO METROBANK CASHPICKUP
          if( $is_bdo_or_metrobank_or_cash_pick_up ) 
          {  

            $email_content = 'emails.templates.bdo_metrobank_cashpickup_sunday_non_poli';

          }

          //NOT BDO METROBANK CASHPICKUP DOOR TO DOOR - OTHER BANKS
          if( $is_other_banks ) 
          {

            $email_content = 'emails.templates.non_bdo_metrobank_cashpickup_sunday_non_poli';

          }

          //DOOR TO DOOR
          if( $is_door_to_door ) 
          {

            $email_content = 'emails.templates.sunday_non_poli';

          }

        }

      }

      return $email_content;

    }

    private function get_saturday_email_content( $remittance_id )
    {

      $remittance = new Remittance;

      $email_content = null;

      $is_poli = $remittance->is_poli( $remittance_id );

      $is_non_poli = $remittance->is_remittance_instruction_received( $remittance_id );

      $bank_source_id = $remittance->get_bank_source_id( $remittance_id );

      if( $this->validate_saturday_12am_to_11am( $remittance_id ) ) 
      {

        if( $is_poli && $bank_source_id == 5 )
        {

          if( $remittance -> is_12am_to_9am( $remittance_id ) )
          {

            $email_content = 'emails.templates.saturday_12am_to_9am_non_bdo_metrobank_poli';

          }

          elseif( $remittance -> is_9am_to_12_noon( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.saturday_9am_to_12noon_non_bdo_metrobank_poli' ;

          }

        }

        if( $is_non_poli && $bank_source_id != 5 )
        {

          if( $remittance -> is_12am_to_9am( $remittance_id ) )
          {

            $email_content = 'emails.templates.saturday_12am_to_9am_non_bdo_metrobank_non_poli';

          }

          elseif( $remittance -> is_9am_to_12_noon( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.saturday_9am_to_12noon_non_bdo_metrobank_non_poli';

          }

        }

      }

      elseif( $this->validate_saturday_12am_to_11am_bdo_metrobank_cashpick( $remittance_id ) )
      {

        if( $is_non_poli && $bank_source_id != 5 ) 
        {

          if( $remittance -> is_12am_to_9am( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.saturday_12am_to_9am_bdo_metrobank_poli';

          }

          elseif( $remittance -> is_9am_to_12_noon( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.saturday_9am_to_6pm_bdo_metrobank_poli';
                
          }

        }

        if( $is_poli && $bank_source_id == 5 ) 
        {

          if( $remittance -> is_12am_to_9am( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.saturday_12am_to_9am_bdo_metrobank_non_poli';

          }

          elseif( $remittance -> is_9am_to_12_noon( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.saturday_9am_to_6pm_bdo_metrobank_non_poli';

          }

        }

      }

      elseif( $this->validate_saturday_12am_to_11am_door_to_door( $remittance_id ) )
      {

        if( $is_poli && $bank_source_id == 5 ) 
        {

          if( $remittance -> is_12am_to_9am( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.monday_to_saturday_12am_to_9am_poli';

          }

          else
          {

            $email_content = 'emails.templates.monday_to_friday_12am_to_6pm_and_saturday_12am_to_11am_door_to_door_poli';

          }

        }

        if( $is_non_poli && $bank_source_id != 5 ) 
        {

          $email_content = 'emails.templates.monday_to_friday_12am_to_6pm_and_saturday_12am_to_11am_door_to_door_non_poli';

        }

      }

      elseif( $this->validate_saturday_11am_onwards( $remittance_id ) )
      {

        if( $remittance -> is_12am_to_9am( $remittance_id ) && $this->is_other_banks( $remittance_id ) ) 
        {

          if( $is_poli && $bank_source_id == 5 ) 
          {

            $email_content = 'emails.templates.saturday_9am_to_12noon_non_bdo_metrobank_poli';

          }

          if( $is_non_poli && $bank_source_id != 5 ) 
          {
                  
            $email_content = 'emails.templates.saturday_9am_to_12noon_non_bdo_metrobank_non_poli';

          }         

        }

        elseif( $remittance -> is_12am_to_9am( $remittance_id ) && ( $this->is_cash_pick_up( $remittance_id ) || $this->is_bdo_or_metrobank( $remittance_id ) ) )
        {

          if( $is_poli && $bank_source_id == 5 ) 
          {

            //no view present in dev.iremit
            $email_content = 'emails.templates.saturday_9am_to_12noon_bdo_metrobank_poli';

          }

          if( $is_non_poli && $bank_source_id != 5 ) 
          {

            //no view present in dev.iremit
            $email_content = 'emails.templates.saturday_9am_to_12noon_bdo_metrobank_non_poli';

          }

        }

        //BDO METROBANK CASHPICKUP CUSTOM AUTORESPONDER FOR 12NOON TO 6PM

        elseif( $remittance -> is_12noon_to_6pm( $remittance_id ) && ( $this->is_cash_pick_up( $remittance_id ) || $this->is_bdo_or_metrobank( $remittance_id ) ) ) 
        {

          if( $is_poli && $bank_source_id == 5 ) 
          {

            $email_content = 'emails.templates.saturday_9am_to_6pm_bdo_metrobank_poli';

          }

          if( $is_non_poli && $bank_source_id != 5 ) 
          {

            $email_content = 'emails.templates.saturday_9am_to_6pm_bdo_metrobank_non_poli';

          }

        }

        //AUTORESPONDER FOR NOT QUALIFIED IN THE ABOVE CONDITION
        else 
        {

          if( $is_poli && $bank_source_id == 5 ) 
          {

            $email_content = 'emails.templates.saturday_9am_to_6pm_bdo_metrobank_non_poli';
          
          }

          if( $is_non_poli && $bank_source_id != 5 ) 
          {

            $email_content = 'emails.templates.saturday_11am_onwards_non_poli';

          }

        }

      } 

      return $email_content;

    }

    private function get_monday_to_friday_email( $remittance_id )
    {

      $email_content = null;

      $remittance = new Remittance;

      $is_poli = $remittance->is_poli( $remittance_id );

      $is_non_poli = $remittance->is_remittance_instruction_received( $remittance_id );

      $bank_source_id = $remittance->get_bank_source_id( $remittance_id );

      if( $this->validate_monday_to_friday_12am_to_6pm_bdo_metrobank_cashpick( $remittance_id ) ) 
      {

        if( $is_poli && $bank_source_id == 5 ) 
        {

          if( $this->is_12am_to_9am( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.monday_to_saturday_12am_to_9am_poli';

          }

          else
          {

            $email_content = 'emails.templates.monday_to_friday_12am_to_6pm_bdo_metrobank_cashpick_poli';

          }

        }

        if( $is_non_poli && $bank_source_id != 5 ) 
        {

          $email_content = 'emails.templates.monday_to_friday_12am_to_6pm_bdo_metrobank_cashpick_non_poli';

        }

      } 

      else if( $this->validate_monday_to_friday_12am_to_6pm_door_to_door( $remittance_id ) ) 
      {

        if( $is_poli && $bank_source_id == 5 ) 
        {

          if( $this->is_12am_to_9am( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.monday_to_saturday_12am_to_9am_poli';

          }

          else
          {

            $email_content = 'emails.templates.monday_to_friday_12am_to_6pm_and_saturday_12am_to_11am_door_to_door_poli';

          }

        }

        if( $is_non_poli && $bank_source_id != 5 ) 
        {

          $email_content = 'emails.templates.monday_to_friday_12am_to_6pm_and_saturday_12am_to_11am_door_to_door_non_poli';

        }

      } 
      
      /*

      * FOR NON BDO METROBANK

      * IS CREATED IN MONDAY TO FRIDAY

      * FROM 12AM to 11AM SYDNEY TIME

      */

      else if( $this->validate_monday_to_saturday_12am_to_11am_non_bdo_metrobank( $remittance_id ) ) 
      {

        if( $is_poli && $bank_source_id == 5 ) {

          if( $this->is_12am_to_9am( $remittance_id ) ) 
          {

            $email_content = 'emails.templates.monday_to_saturday_12am_to_9am_poli';

          }

          else
          {

            $email_content = 'emails.templates.monday_to_saturday_12am_to_11am_non_bdo_metrobank_poli';

          }

        }

        if( $is_non_poli && $bank_source_id != 5 ) 
        {

          $email_content = 'emails.templates.monday_to_saturday_12am_to_11am_non_bdo_metrobank_non_poli';

        }

      } 

      /*

      * FOR NON BDO METROBANK

      * IS CREATED IN MONDAY TO FRIDAY

      * FROM 11AM ONWARDS TO 6PM SYDNEY TIME

      */

      else if( $this->validate_monday_to_friday_11am_onwards_to_6pm_non_bdo_metrobank( $remittance_id ) )
      {

        if( $is_poli && $bank_source_id == 5 ) 
        {

          $email_content = 'emails.templates.monday_to_friday_11am_onwards_to_6pm_non_bdo_metrobank_poli';

        }

        if( $is_non_poli && $bank_source_id != 5 ) 
        {

          $email_content = 'emails.templates.monday_to_friday_11am_onwards_to_6pm_non_bdo_metrobank_non_poli';

        }

      } 

      return $email_content;      

    }

    public function get_transaction_details( $remittance_id ) 
    {
      
      $transaction_details = null;

      //CHECK REMITTANCE ID
      if( !is_null( $remittance_id ) || !empty( $remittance_id ) ) 
      {

        $remittance_details = Remittance::find( $remittance_id );

        $remitter_details = Remittance::find( $remittance_id )->remitter;

        $recipient_details = Remittance::find( $remittance_id )->recipient;

        $recipient_persons = Remittance::find( $remittance_id )->recipient->recipientPersons;

        $recipient_banks = $recipient_details->bank;

        $promo_code = Remittance::find( $remittance_id )->promo_code;

        $bank_remittance_source_details = Remittance::find( $remittance_id )->bank_source;

        $receiving_options_details = Remittance::find( $remittance_id )->RecipientReceiveOption;

        $receive_options_transfer_details = $receiving_options_details->ReceiveOptionList;

        if( !is_null( $promo_code ) ) 
        {
        
          $new_service_charge = $promo_code[ 'value' ];
        
        } 

        else 
        {
          
          $new_service_charge = $remittance_details[ 'service_fee' ];
          
        }
        
        $new_transaction_details = array();

        $new_transaction_details[ 'transaction_no' ] = $remittance_details[ 'transaction_number' ];
        
        //Can't find tracking number
        //$new_transaction_details[ 'tracking_number' ] = $remittance_details[ 'tracking_number' ];
        
        $new_transaction_details[ 'transaction_datetime' ] = $remittance_details[ 'date_transferred' ];

        //REMITTER DETAILS
        $new_transaction_details[ 'remitter_firstname' ] = $remitter_details[ 'firstname' ];
        
        $new_transaction_details[ 'remitter_middlename' ] = $remitter_details[ 'middlename' ];
        
        $new_transaction_details[ 'remitter_lastname' ] = $remitter_details[ 'lastname' ];
        
        $new_transaction_details[ 'remitter_email' ] = $remitter_details[ 'email' ];
        
        $new_transaction_details[ 'remitter_fullname' ] = $remitter_details[ 'firstname' ] . ' ' . $remitter_details[ 'middlename' ] . ' ' . $remitter_details[ 'lastname' ];
        
        //RECIPIENT DETAILS
        $new_transaction_details[ 'recipient_firstname' ] = $recipient_persons->firstname;
        
        $new_transaction_details[ 'recipient_middlename' ] = $recipient_persons->middlename;
        
        $new_transaction_details[ 'recipient_lastname' ] = $recipient_persons->lastname;
        
        $new_transaction_details[ 'recipient_phone' ] = $recipient_details->contact['contact'];
        
        $new_transaction_details[ 'recipient_fullname' ] = $recipient_persons->fullname;
        
        $new_transaction_details[ 'alternative_benefiiciary' ] = $recipient_details['alternative'];
        
        $new_transaction_details[ 'receive_option_transfer' ] = $receive_options_transfer_details[ 'name' ];
        
        $new_transaction_details[ 'receive_options' ] = $receive_options_transfer_details[ 'receive_options' ];
        
        $new_transaction_details[ 'receive_options_name' ] = $receiving_options_details[ 'name' ];
        
        $new_transaction_details[ 'transaction_instruction_type' ] = $receive_options_transfer_details[ 'name' ] . ' ' . $remittance_details[ 'other_bank_name' ];
        
        /*
        * RECIPIENT BANK DETAILS
        */
        $new_transaction_details[ 'bank_branch' ] = $recipient_banks[ 'account_branch' ];
        
        $new_transaction_details[ 'bank_accnt_name' ] = $recipient_banks[ 'account_name' ];
        
        $new_transaction_details[ 'bank_accnt_no' ] = $recipient_banks[ 'account_no' ];
        
        
        $new_transaction_details[ 'recipient_address' ] = $recipient_details->address['current_address'];
        
        $new_transaction_details[ 'recipient_birthday' ] = $recipient_details[ 'birthday' ];
        
        $new_transaction_details[ 'remittance_amount' ] = number_format( ( ( float ) $remittance_details[ 'amount_sent' ] ), 2, '.', ',' );
        
        $new_transaction_details[ 'transfer_fee' ] = $new_service_charge;
        
        $new_transaction_details[ 'total_payable_to_iremit' ] = number_format( ( float ) $remittance_details[ 'amount_sent' ] + $new_service_charge, 2, '.', ',');

        $new_transaction_details[ 'bank_source' ] = $bank_remittance_source_details[ 'shortname' ];
        
        $new_transaction_details[ 'bank_bsb' ] = $bank_remittance_source_details[ 'account_branch' ];
        
        $new_transaction_details[ 'bank_accountno' ] = $bank_remittance_source_details[ 'account_no' ];
        
        $new_transaction_details[ 'forex_rate' ] = $remittance_details->get_exchange_rate( $remittance_id );

        if( $new_transaction_details[ 'forex_rate' ] == 'TBA Next Business Day' )
        {

          $new_transaction_details[ 'peso_amount' ] = $new_transaction_details[ 'forex_rate' ];

        }

        else
        {

          $new_transaction_details[ 'peso_amount' ] = number_format( ( ( ( float ) $remittance_details[ 'amount_sent' ] ) * $remittance_details[ 'forex_rate' ] ), 2, '.', ',' );

        }
         
        /*
        * BONUS DATA
        */
        
        $new_transaction_details[ 'other_bank_name' ] = $remittance_details[ 'other_bank_name' ];
        
        $new_transaction_details[ 'promo_code' ] = $promo_code[ 'name' ];
        
        return $new_transaction_details;
        
      }

      return array();
      
    }

    public function postForm( Request $request ) {

      //Perform add if user is not authenticated
      if( !Auth::check() )
      {

          $remitter = $this -> create_remitter( $request -> all( ) );

          Mail::to( 'peterjohnjimenez@gmail.com' )->send( new RegisterMail( ) );

          return response()->json( $remitter );

      }

    }

    public function create_remitter( $data )
    {

        $remitter = $this -> save_to_remitter( $data );

        //Append remitter_id to $data
        $data[ 'remitter_id' ] = $remitter->id;

        $remitter_address = $this -> save_to_remitter_address( $data );

        $remitter_identification = $this -> save_to_remitter_identification( $data );

        $remitter_contact = $this -> save_to_remitter_contact( $data );

        return [

                'remitter' => $remitter

                , 

                'remitter_address' => $remitter_address

                ,

                'remitter_identification' => $remitter_identification

                ,

                'remitter_contact' => $remitter_contact

               ]

        ;

    }

    /*
        
        save to remitter
            
    */
    public function save_to_remitter( $data )
    {

        $remitter = new Remitter;

        $remitter->id_number = $remitter -> generate_id_number();

        $remitter->firstname = ( isset( $data[ 'first_name' ] ) ? $data[ 'first_name' ] : '' );

        $remitter->middlename = ( isset( $data[ 'middle_name' ] ) ? $data[ 'middle_name' ] : '' );

        $remitter->lastname = ( isset( $data[ 'last_name' ] ) ? $data[ 'last_name' ] : '' );

        $remitter->birthday = ( isset( $data[ 'remitter_birthday' ] ) ? date("Y-m-d", strtotime( $data[ 'remitter_birthday' ] ) ) : '' );

        $remitter->civil_status_id = ( isset( $data[ 'remitter_civil_status' ] ) ? $data[ 'remitter_civil_status' ] : '' );

        $remitter->email = ( isset( $data[ 'email' ] ) ? $data[ 'email' ] : '' ); 

        $remitter->secondary_email = ( isset( $data[ 'sec_email' ] ) ? $data[ 'sec_email' ] : '' );

        $remitter->nationality_id = ( isset( $data[ 'nationality' ] ) ? $data[ 'nationality' ] : '' );

        $remitter->save();

        return $remitter;

    }

    public function save_to_remitter_address( $data )
    {

        $remitter_address = new RemitterAddress;

        $remitter_address->remitter_id = ( isset( $data[ 'remitter_id' ] ) ? $data[ 'remitter_id' ] : '' );

        $remitter_address->current_address = ( isset( $data[ 'street1' ] ) ? $data[ 'street1' ] : '' );

        $remitter_address->country_id = ( isset( $data[ 'country' ] ) ? $data[ 'country' ] : '' );

        $remitter_address->state_id = ( isset( $data[ 'state' ] ) ? $data[ 'state' ] : '' );

        $remitter_address->city_id = ( isset( $data[ 'city' ] ) ? $data[ 'city' ] : '' );

        $remitter_address->postal_code = ( isset( $data[ 'zip' ] ) ? $data[ 'zip' ] : '' );

        $remitter_address->save( );

        return $remitter_address;

    }

    public function save_to_remitter_identification( $data )
    {

        $remitter_identification = new RemitterIdentification;

        $remitter_identification->remitter_id = ( isset( $data[ 'remitter_id' ] ) ? $data[ 'remitter_id' ] : '' );       

        $remitter_identification->number = ( isset( $data[ 'idnumber' ] ) ? $data[ 'idnumber' ] : '' );

        $remitter_identification->expiration_date = ( isset( $data[ 'expiredid' ] ) ? date("Y-m-d", strtotime( $data[ 'expiredid' ] ) ) : '' );

        $remitter_identification->filename = ( isset( $data[ 'file_attachment' ] ) ? $data[ 'file_attachment' ] : '' );
        
        $remitter_identification->identification_type_id = ( isset( $data[ 'any_id' ] ) ? $data[ 'any_id' ] : '' );

        $remitter_identification->save( );

        return $remitter_identification;

    }

    public function save_to_remitter_contact( $data )
    {

        $remitter_contact = new RemitterContact;

        $remitter_contact->remitter_id = ( isset( $data[ 'remitter_id' ] ) ? $data[ 'remitter_id' ] : '' );
   
        $remitter_contact->contact = ( isset( $data[ 'phone_no' ] ) ? $data[ 'phone_no' ] : '' );

        $remitter_contact->save( );

        return $remitter_contact;

    }

    /*
        For testing 
    */
    public function test( $id )
    {

      //return response()->json( [ $this->get_transaction_details( $id ) ] );

      return $this->send_auto_responder( 'remittance-id', $id );

      $remittance = new Remittance;

      return view( 'emails.templates.bdo_metrobank_cashpickup_sunday' )->with( 'transaction_data', $this->get_transaction_details( 15 ) );

      return response()->json( [ $this->get_transaction_details( 15 ) ] );

      $recipient_receive_option = Remittance::find( 15 )->RecipientReceiveOption;

      return response()->json( [ Remittance::find( 15 )->promo_code[ 'value' ] ] );

      return response()->json( [ $recipient_receive_option->ReceiveOptionList ] );

      return view( 'emails.templates.bdo_metrobank_cashpickup_sunday_poli' );

        $remitter = new Remitter;

        $Collection = $remitter->generate_id_number();

        return response()->json([ $Collection ]);

    }

}
