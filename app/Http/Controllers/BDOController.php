<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Remittance;
use App\Models\RemittanceStatus;

class BDOController extends Controller {
	
    public function __construct() {
		
        $this->middleware( 'auth' );

    }
    
    public function index() 
    {
		
    	$remittances = Remittance::where('status_id', RemittanceStatus::REMITTANCE_STATUS_PROCESSING_B)
    		->paginate(20);

		return view('connects.bdo.index')->with(compact('remittances'));
	}
    
}
