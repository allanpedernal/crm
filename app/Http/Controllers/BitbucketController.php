<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BitbucketController extends Controller {
	
	//DEPLOY BIT BUCKET UPDATE
    public function deploy() {
		
		//SET REPOSITORIES
		$repositories = array(
		
			'crm.iremit.com.au' => array(
			
				'repo_dir' => '/srv/crm.iremit.com.au.git',
				
				'web_root_dir' => '/var/www/html/crm.iremit.com.au'
				
			)
			
		);
		
		$repo_dir = $repositories[ 'crm.iremit.com.au' ][ 'repo_dir' ];
		
		$web_root_dir = $repositories[ 'crm.iremit.com.au' ][ 'web_root_dir' ];

		$git_bin_path = 'git';

		$update = false;

		$payLoad = json_decode( request() -> getContent() );

		if ( empty( $payload -> commits ) ) {
			
		  $update = true;
		  
		} 
		
		else {
			
			foreach ( $payload -> commits as $commit ) {

			$branch = $commit -> branch;

				if ( $branch === 'production' || isset( $commit -> branches ) && in_array( 'production', $commit -> branches ) ) {

					$update =	true;

					break;

				}

			}

		}

		if ( $update ) {

		  exec( 'cd ' . $repo_dir . ' && ' . $git_bin_path  . ' fetch' );
		  
		  exec( 'cd ' . $repo_dir . ' && GIT_WORK_TREE=' . $web_root_dir . ' ' . $git_bin_path  . ' checkout -f' );

		  $commit_hash = shell_exec( 'cd ' . $repo_dir . ' && ' . $git_bin_path  . ' rev-parse --short HEAD' );
		  
		  file_put_contents( '/var/www/html/crm.iremit.com.au/storage/log/deploy.log', date('m/d/Y h:i:s a') . " Deployed branch: " .  $branch . " Commit: " . $commit_hash . "\n", FILE_APPEND);
		
		}
		

	
    }

}
