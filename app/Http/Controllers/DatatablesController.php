<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use User;

use Yajra\Datatables\Facades\Datatables;


class DatatablesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
	/**
	 * Displays datatables front end view
	 *
	 * @return \Illuminate\View\View
	 */
	public function getIndex()
	{
		return view('vendor.datatables.index');
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function anyData()
	{
		return Datatables::of(User::query())->make(true);
	}
}
