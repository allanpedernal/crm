<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Library\LibraryState;

use App\Models\Library\LibraryCity;

use App\Models\Library\LibraryCountry;

use App\Models\Library\LibraryNationality;

use App\Models\Library\LibraryCivilStatus;

use App\Models\Library\LibraryIdentificationType;

class LibraryController extends Controller {
	
	//CONSTRUCT
    public function __construct() {
		
        $this->middleware( 'auth' );

    }
    
    //GET STATES FROM COUNTRY
    public function getStatesFromCountry( Request $request ) {
		
		
		//CAN GET VIA AJAX
		if( $request -> ajax() ) {
			
			$success = false;
			
			$error_message = '';
			

			$country_id = $request -> country_id;
			
			$hash = $request -> hash;

			//CHECK COUNTRY ID
			if( $country_id > 0) {
				
				$success = true;
				
				$states = LibraryState::all() 
				
									-> where( 'country_id', $country_id ) 
									
									-> sortBy( 'id' );
				
			}
			
			//MISSING STATE ID
			else {
				
				$success = false;
				
				$error_message = 'Missing country id!';
				
			}
			
			//SEND SOME RESPONSE
			return \Response::json( [ 'success' => $success, 'result' => [ 'hash' => $hash, 'states' => $states ], 'error_message' => $error_message ] );
			
		}
		
		
	}
    
    //GET CITIES FROM STATE
    public function getCitiesFromState( Request $request ) {
		
		//CAN GET VIA AJAX
		if( $request -> ajax() ) {
			
			$success = false;
			
			$error_message = '';
			

			$state_id = $request -> state_id;
			
			$hash = $request -> hash;


			//CHECK STATE ID
			if( $state_id > 0 ) {
				
				$success = true;
				
				$cities = LibraryCity::all() 
				
									-> where( 'state_id', $state_id ) 
									
									-> sortBy( 'id' );
				
			}
			
			//MISSING STATE ID
			else {
				
				$success = false;
				
				$error_message = 'Missing state id!';
				
			}
			
			//SEND SOME RESPONSE
			return \Response::json( [ 'success' => $success, 'result' => [ 'hash' => $hash, 'cities' => $cities ], 'error_message' => $error_message ] );
			
		}
		
	}
	
	//GET POSTAL FROM CITY
	public function getPostalFromCity( Request $request ) {
		
		//CAN GET VIA AJAX
		if( $request -> ajax() ) {
		
			$success = false;
			
			$error_message = '';
			

			$city_id = $request -> city_id;
			
			$hash = $request -> hash;


			//CHECK CITY ID
			if( $city_id ) {
				
				$success = true;
				
				$postal_code = LibraryCity::where( 'id', $city_id )
									
									-> first( [ 'postal_code' ] );
									
				$postal_code = $postal_code[ 'postal_code' ];

			}
			
			//MISSING CITY ID
			else {
				
				$success = false;
				
				$error_message = 'Missing city id!';
				
			}
			
			//SEND SOME RESPONSE
			return \Response::json( [ 'success' => $success, 'result' => [ 'hash' => $hash, 'postal_code' => $postal_code ], 'error_message' => $error_message ] );
			
		}
		
	}
	
	//GET COUNTRIES
	public function getCountries(){

    	return response()->json( [ 'success' => true, 'result' => LibraryCountry::all( ) ] );

    }
	
	//GET LIBRARY
    public function getLibrary( Request $request ) {

    	if( $request -> ajax() ) {
		
			$success = false;
			
			$error_message = '';

			$response = array();

			switch( $request -> request_type ) {

				case 'get_nationalities':

						$response = [ 

							'success' => true, 

							'result' => [ 

								'nationalities' => LibraryNationality::all( ) -> sortBy( 'id' )

							]

						];

					break;

				case 'get_civil_statuses':

						$response = [ 

							'success' => true, 

							'result' => [ 

								'civil_statuses' => LibraryCivilStatus::all( ) -> sortBy( 'id' )

							]

						];

					break;

				case 'get_id_type':

						$response = [ 

							'success' => true, 

							'result' => [ 

								'id_type' => LibraryIdentificationType::all( ) -> sortBy( 'id' )

							]

						];

					break;

				default:

			}

			return response() -> json( $response );

		}

    }
    
}
