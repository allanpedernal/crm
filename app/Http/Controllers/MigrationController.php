<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\MigrateRemitter;
use App\Jobs\MigrateRecipient;
use App\Jobs\MigrateRemittance;
use App\Jobs\MigratePromoCode;
use App\Jobs\MigrateMobilePromotion;
use App\Jobs\MigrateLibraryCity;
use App\Jobs\MigrateRemoveRecipientDup;

class MigrationController extends Controller
{

    public function __construct() {

		$this->middleware( 'auth' );

    }

    public function migrateRemitter(){
		
		dispatch(new MigrateRemitter());
		
	}
	
    public function migrateRecipient(){
		
		dispatch(new MigrateRecipient());
		
	}
	
    public function migrateRemittance(){
		
		dispatch(new MigrateRemittance());
		
	}
	
    public function migratePromoCode(){
		
		dispatch(new MigratePromoCode());
		
	}

	public function migrateMobilePromotion(){
		
		dispatch(new MigrateMobilePromotion());
		
	}

	public function migrateLibraryCity(){
		
		dispatch(new MigrateLibraryCity());
		
	}

	public function migrateRemoveRecipientDup(){
		
		dispatch(new MigrateRemoveRecipientDup());
		
	}


}
