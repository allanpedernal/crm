<?php


namespace App\Http\Controllers;


use Illuminate\Support\Str;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\Auth;


use App\Models\MobilePromotion;

use App\Models\RemitterAccessToken;


class MobilePromotionsController extends Controller {
	
	
	const FIREBASE_KEY = 'AIzaSyDGjpaE736T3Jm9kfqAULoRu_MAxmBS4HM';
	
	
    public function __construct() {
		
        $this -> middleware( 'auth' );
        
    }
    
    public function index() {
		
		$mobile_promotions = $this -> getMobilePromotions();
		
		return view( 'mobile-promotions', array( 'mobile_promotions' => $mobile_promotions ) );
		
	}
    
    public function sendPromotion( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {

			//EDIT FORM
			if( $request -> id != NULL || $request -> id != '' ) {
				
				//GET NEW PROMOTION
				$new_promotion = MobilePromotion::where( 'id', '=', $request -> id ) -> first();
				
				//CHECK NEW PROMOTION IF NOT EMPTY
				if ( ! empty( $new_promotion ) ) {
					
					
					//UPDATE NEW PROMOTION
					$new_promotion -> is_send = 1;

					$new_promotion -> send_times += 1;

					$new_promotion -> save();
					
					
					//GET DEVICE TOKENS
					$device_tokens = RemitterAccessToken::whereNotNull( 'device_token' ) -> get();
					
					
					$response = [];
					
					$token_ids = [];

					foreach ( $device_tokens as $device_token ) {
						
						if ( substr( $device_token -> device_token, 0, 1 ) !== "<" ) {
							
							$token_ids[] = $device_token -> device_token;

						}  
					 
					}


					$slice_token = array_chunk( $token_ids, 1000 );

					for ( $x = 0; $x < count( $slice_token ); $x++ ) {
						
						$http = new \GuzzleHttp\Client();

						$key = self::FIREBASE_KEY;

						$content = [

							'data' => [

								'title' => 'iRemit.com.au',

								'body' => $new_promotion -> short_description,

								'status_type' => 2,

								'detailDescription' => $new_promotion -> detail_description

							],

							'notification' => [

								'title' => 'iRemit.com.au',

								'body' => $new_promotion -> short_description,

								'status_type' => 2, 

								'detailDescription' => $new_promotion -> detail_description,

								'sound' => 'default'

							],

							'priority' => 'high',

							'registration_ids' => $slice_token[ $x ]

						];

						$response_tmp = $http -> request( 'POST', 'https://fcm.googleapis.com/fcm/send', [
						
							'json' => $content,
							
							'headers' => [ 'Authorization' => 'key=' . $key ]

						] );

						$response[ $x ] = $response_tmp -> getBody();
						
					}

					//GET ALL MOBILE PROMOTION TABLES
					$mobile_promotion_table_template = view( 'tables.mobile_promotion_table',
					
						array( 'mobile_promotions' => $this -> getMobilePromotions() )
						
					)
					
					-> render();
					
					
					//SEND SOME RESPONSE
					return \Response::json( [ 'success' => true, 'result' => [ 'mobile_promotion_table_template' => $mobile_promotion_table_template, 'action' => 'send' ] ] );
					

				}

				else {
					
					return \Response::json( [ 'success' => false, 'error_message' => 'Promotion doesn \'t exists!' ] );

				}
			
			}
			
		}
			
	}	
	
	public function getModal( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {

			//EDIT FORM
			if( $request -> id != NULL || $request -> id != '' ) {
				
				$mobile_promotion_details = MobilePromotion::where( 'id', '=', $request -> id ) -> orderBy( 'id', 'DESC' ) -> first();
				
				$content = view( 'modals.mobile-promotions' ) -> with( [ 'action' => 'edit', 'mobile_promotion_id' => $mobile_promotion_details[ 'id' ], 'mobile_promotion_details' => $mobile_promotion_details ] ) -> render();
				
				//SHOW SOME RESPONSE
				return \Response::json( [ 'success' => true, 'result' => [ 'mobile_promotion_id' => $mobile_promotion_details[ 'id' ], 'content' => $content, 'action' => 'edit' ] ] );
				
			}
			
			else {
				
				$content = view( 'modals.mobile-promotions' ) -> with( [ 'action' => 'new' ] ) -> render();
				
				//SHOW SOME RESPONSE
				return \Response::json( [ 'success' => true, 'result' => [ 'mobile_promotion_id' => 0, 'content' => $content, 'action' => 'new' ] ] );
				
			}
			
		}
		
	}
	
	public function postForm( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {
				
				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					
					
					//CHECK IF MOBILE PROMOTION IS NOT EMPTY
					if( ! empty( $input[ 'mobile_promotion_id' ] ) ) {

						$new_mobile_promotion = MobilePromotion::find( $input[ 'mobile_promotion_id' ] );

						$action = 'edit';

					}
					
					else {

						$new_mobile_promotion = new MobilePromotion;

						$new_mobile_promotion -> send_times = 0;

						$new_mobile_promotion -> created_by = Auth::user()->ID;

						$action = 'add';

					}
					
					$new_mobile_promotion -> excerpt = ( isset( $input[ 'mobile_promotion_excerpt' ] ) && ! empty( $input[ 'mobile_promotion_excerpt' ] ) ? $input[ 'mobile_promotion_excerpt' ] : NULL );
					
					$new_mobile_promotion -> description = ( isset( $input[ 'mobile_promotion_excerpt' ] ) && ! empty( $input[ 'mobile_promotion_description' ] ) ? $input[ 'mobile_promotion_description' ] : NULL );
					
					$new_mobile_promotion -> save();
					
					$mobile_promotion_id = $new_mobile_promotion[ 'id' ];
					
					//COMMIT ALL TRANSACTION
					DB::commit();
					
					
					$mobile_promotion_table_template = view( 'tables.mobile_promotion_table',
					
						array( 'mobile_promotions' => $this -> getMobilePromotions() )
						
					)
					
					-> render();
					
					
					//SHOW SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'mobile_promotion_id' => $mobile_promotion_id, 'mobile_promotion_table_template' => $mobile_promotion_table_template, 'action' => $action ) ] );
					
					
				}
				
				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
				
			}
			
		}
		
	}

	public function deletePromotions( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {
				
				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					
					
					//CHECK IF MOBILE PROMOTION IS ISSET AND NOT EMPTY
					if( isset( $input[ 'mobile_promotion' ] ) && ! empty( $input[ 'mobile_promotion' ] ) && count( $input[ 'mobile_promotion' ] ) ) {
						
						//LOOP ALL MOBILE PROMOTION
						foreach( $input[ 'mobile_promotion' ] AS $key => $mobile_promotion ) {
							
							//REMOVE MOBILE PROMOTION
							$remove_sms_code = MobilePromotion::find( $mobile_promotion ) -> delete();
							
						}
						
					}
					
					
					//COMMIT ALL TRANSACTION
					DB::commit();
					
					
					//GET ALL MOBILE PROMOTION TABLES
					$mobile_promotion_table_template = view( 'tables.mobile_promotion_table',
					
						array( 'mobile_promotions' => $this -> getMobilePromotions() )
						
					)
					
					-> render();
					
					//SEND SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'total_mobile_promotion' => count( $input[ 'mobile_promotion' ] ), 'mobile_promotion_table_template' => $mobile_promotion_table_template, 'action' => 'delete' ) ] );
					
					
				}
				
				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
				
			}
			
		}
    
	}


	private function getMobilePromotions() {
		
		//REMOVE PAGE FROM SEARCH QUERY
		UNSET( $_GET[ 'page' ] );
		
		//SEARCH QUERY IS ISSET AND NOT EMPTY
		if( isset( $_GET ) && ! empty( array_filter( $_GET ) ) ) {
			
			$search_query = $this -> searchParameter();

			$mobile_promotions = DB::table( 'mobile_promotions' )
			
					-> select( DB::Raw( '

						crm_mobile_promotions.id AS id,

						crm_mobile_promotions.excerpt AS excerpt,

						crm_mobile_promotions.send_times AS send_times,

						crm_mobile_promotions.is_send AS is_send,

						crm_mobile_promotions.created_at AS date
					
					' ) )
					
					-> whereRaw( $search_query )
					
					-> whereNull( 'mobile_promotions.deleted_at' )
					
					-> orderBy( 'mobile_promotions.id','DESC' )
					
					-> paginate( 20 )

					-> setPath( 'promotions' );
			
		}
		
		else {
			
			$mobile_promotions = DB::table( 'mobile_promotions' )
			
					-> select( DB::Raw( '

						crm_mobile_promotions.id AS id,

						crm_mobile_promotions.excerpt AS excerpt,

						crm_mobile_promotions.send_times AS send_times,

						crm_mobile_promotions.is_send AS is_send,

						crm_mobile_promotions.created_at AS date
					
					' ) )
					
					-> whereNull( 'mobile_promotions.deleted_at' )
					
					-> orderBy( 'mobile_promotions.id','DESC' )
					
					-> paginate( 20 )

					-> setPath( 'promotions' );
			
		}
		
		return $mobile_promotions;
		
	}

	private function searchParameter() {
		
		//CONSTRUCT QUERY STRING
		$query_string = array();
		
		//CHECK IF GET IS SET AND NOT EMPTY
		if( isset( $_GET ) && ! empty( $_GET ) ) {

			//MOBILE PROMOTION EXCERPT
			if( isset( $_GET[ 'search_mobile_promotion_excerpt' ] ) && ! empty( $_GET[ 'search_mobile_promotion_excerpt' ] ) ) {
				
				$query_string[] = " crm_mobile_promotions.excerpt LIKE '%{$_GET[ 'search_mobile_promotion_excerpt' ]}%' ";
				
			}
			
			//MOBILE PROMOTION SEND TIMES
			if( isset( $_GET[ 'search_mobile_promotion_send_times' ] ) && ! empty( $_GET[ 'search_mobile_promotion_send_times' ] ) ) {
				
				$query_string[] = " crm_mobile_promotions.send_times = '{$_GET[ 'search_mobile_promotion_send_times' ]}' ";
				
			}
			
			//MOBILE PROMOTION IS SEND
			if( isset( $_GET[ 'search_mobile_promotion_is_send' ] ) && ! empty( $_GET[ 'search_mobile_promotion_is_send' ] ) ) {
				
				$query_string[] = " crm_mobile_promotions.is_send = 1 ";
				
			}
			
			//MOBILE PROMOTION DATE
			if(

				( isset( $_GET[ 'search_mobile_promotion_start_date' ] ) && ! empty( $_GET[ 'search_mobile_promotion_start_date' ] ) )

				&&

				( isset( $_GET[ 'search_mobile_promotion_end_date' ] ) && ! empty( $_GET[ 'search_mobile_promotion_end_date' ] ) )

			) {

				$query_string[] = " 

					DATE( crm_mobile_promotions.created_at ) >= '{$_GET[ 'search_mobile_promotion_start_date' ]}' 

					AND 

					DATE( crm_mobile_promotions.created_at ) <= '{$_GET[ 'search_mobile_promotion_end_date' ]}' 

				";

			}
			
			//RETURN SEARCH QUERY
			return implode( ' AND ', array_filter( $query_string ) );

		}
		
	}

}
