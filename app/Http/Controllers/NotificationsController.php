<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Remittance;
use App\Models\RemittetAccessToken;


class NotificationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('notifications');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function send_push_notification( $remittance_id )
    {

        $remittance_details = Remittance::find( $remittance_id );

        $push_notification_data = array();
        
        $push_notification_data[ 'title' ] = 'iRemit.com.au';
        
        $push_notification_data[ 'transaction_no' ] = ( 

                                                        isset( $remittance_details[ 'transaction_number' ] ) 

                                                        && 

                                                        ! empty( $remittance_details[ 'transaction_number' ] ) 

                                                        ? 

                                                        $remittance_details[ 'transaction_number' ] 

                                                        : 

                                                        '' 

                                                    )

        ;
        
        $push_notification_data[ 'status' ] = ( 

                                                isset( $remittance_details[ 'status_id' ] ) 

                                                && 

                                                !empty( $remittance_details[ 'status_id' ] ) 

                                                ? 

                                                $remittance_details[ 'status_id' ] 

                                                : 

                                                '' 

                                              )

        ;

        $access_token = $remittance_details->access_token;

        $push_notification_data[ 'device_token' ] = $access_token[ 'device_token' ];

        $device_type = $access_token[ 'device_type_id' ];

        $result = null;

        if( !empty( $push_notification_data[ 'transaction_no' ] ) )
        {

            $result = $this->firebase_push_notification( $push_notification_data );

        }      

        return $result;

    }

    public function firebase_push_notification( $data ) 
    {

        $key = 'AIzaSyDGjpaE736T3Jm9kfqAULoRu_MAxmBS4HM';
            
        $content = [
            
            'data' => [
            
                'title' => $data[ 'title' ],
                
                'body' => 'Remittance ref# '.$data['transaction_no'].' is now '. ucwords($data['status']),
                
                'status_type' => 1, // 1 for transaction
                
                'status' => 'Remittance ref# '.$data['transaction_no'].' is now '. ucwords($data['status']),
                
                'remittance_id' => $data['remittance_id'],
                
            ],
                
            'notification' => [
            
                'title' => $data[ 'title' ],
                
                'body' => 'Remittance ref# '.$data['transaction_no'].' is now '. ucwords($data['status']),
                
                'status_type' => 1, // 1 for transaction
                
                'status' => 'Remittance ref# '.$data['transaction_no'].' is now '. ucwords($data['status']),
                
                'remittance_id' => $data['remittance_id'],
                
            ],
            
            'priority' => 'high',
            
            'to' => $data['device_token']
            
        ];
        
        $ch = curl_init();
        
        curl_setopt($ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');

        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($content));
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization: key={$key}"));

        $result = curl_exec($ch);
        
        curl_close($ch);
        
        return $result;
        
    }

    public function test( $remittance_id )
    {

        $data = null;

        $remittance = Remittance::find( $remittance_id );

        $data['transaction_no'] = $remittance[ 'transaction_no' ];
                        
        $data['status'] = $remittance[ 'status' ];
                        
        $data['remittance_id'] = $remittance[ 'id' ];

        $data[ 'title' ] = 'iRemit.com.au';

        $data['device_token'] = 'cJF2plbR-6s:APA91bF0RomR1tZwUei_Fops-ZlvWhzRWTunAZjt4B4BrnYxiYjOZjHmJTgW4kkRKv6YXwLkjNom2lOjyJNtYkVzXfoxR0ftA3WtBUUOfdkxJoswRPFFGTy1aDysyVLeCfHp7g8fiyQs'; //pj@iremit.com.au ----- iOS  Pj's device

        // $data['device_token'] = 'fm3e1b-adFQ:APA91bEkj__7jhKDo0JeJB_0kFyt6MM1eY-j1WiLFhp6-bYmjbo_PbVwV_UvuhEEJ16rDOQEwB36amGDeIFefBuwJbyVV0VewUXiFhCP0Q1oxkEcX21sv8-IWqWMDfJf5tEy-r31MXfH'; //PJ's android device

        return $this->firebase_push_notification( $data ); 



    }
    
}
