<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Models\Remittance; 
use App\Models\RemittancePromoCode; 
use App\Models\RemittancePromoCodeUser; 
use App\Models\RemittancePromoCodeUserRecipients; 
use App\Models\BusinessReferrals; 
use App\Models\Recipient; 
use App\Models\Remitter; 

class PromoCodesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return $this->get_response();

    }

    private function get_response()
    {

        $btn_submit = Input::get( 'btn_submit' );

        $response = null;

        switch ( $btn_submit ) 
        {

            case 'search_promo_code':

                $response = $this->search();

                break;
            
            default:        

              $response = view('codes-promo') -> with( 'remittancePromoCodes', RemittancePromoCode::orderBy( 'id','desc') -> paginate(15) );

        }

        return $response;      

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
   

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getModal( )
    {

        $remittancePromoCodes = RemittancePromoCode::orderBy( 'id', 'desc')->paginate(15);

        return view( 'codes-promo' )->with
                                      (

                                        'remittancePromoCode'

                                        , 

                                        $remittancePromoCode

                                      )
        ;

    }

    public function getData( Request $request ) 
    {

        $input = $request->all( );

        $response = null;

        switch ( $input[ 'state' ] )
        {

          case 'get_promo_code_details':

            $response = $this->get_promo_code_details( $input[ 'promo_code_id' ] );

            break;

          default:
            # code...
            break;

        }
        
        return response()->json( $response );

    }

    public function postForm( Request $request ) {

        //FORM IS POST VIA AJAX
        if( $request -> ajax() ) {

            //AJAX IS POST
            if( $request -> isMethod( 'post' ) ) {
                    
                //GET ALL INPUT
                $input = $request -> all();

                $response = null;

                switch ( $input[ 'state' ] ) {
                    
                    case 'get_business_list':

                        $business_name = $input[ 'data' ][ 'q' ];

                        $result = $this->get_business_referrals( $business_name );

                        break;

                    case 'get_recipient_list':

                        $remitter = $input[ 'remitter_id' ];

                        $result = $this->get_recipients( $remitter );

                        break;

                    case 'save_promo_code':

                        $result = $this->save_promo_code( $input );

                        break;

                    case 'edit_promo_code':

                        $result = $this->edit_promo_code( $input );

                        break;

                    case 'delete_promo_code':

                        $result = $this->delete_promo_code( $input );

                        break;

                    default:
                        # code...
                        break;
                }

                return response()->json( $result );

            }

        }

    }

    private function get_business_referrals( $keyword ){

        $results = BusinessReferrals::where('name','LIKE','%'.$keyword.'%')

                                        ->orWhere('email','LIKE','%'.$keyword.'%')

                                        ->orWhere('fb','LIKE','%'.$keyword.'%')

                                        ->orWhere('website','LIKE','%'.$keyword.'%')

                                        ->get();

        $response = array();                                

        foreach ($results as $key => $result) {
            
            $response [ $key ][ 'id' ] = "{$result [ 'id' ]}";

            $response [ $key ][ 'name' ] = $result[ 'name' ] . ' | ' . $result[ 'email' ];

        }

        return [ 'q' => $keyword, 'results' => $response ];

    }

    private function get_recipients( $remitter_ids )
    {

      $remitter_ids = is_array( $remitter_ids ) ? $remitter_ids : array( $remitter_ids );

      $response = array();

      $ids = Remittance::select( 'remitter_id', 'recipient_id' ) 

                          -> whereIn( 'remitter_id', $remitter_ids )

                          -> groupby( 'remitter_id', 'recipient_id' )

                          -> get();

      $ids_arr = json_decode( $ids );

      $recipient_ids = array_column( $ids_arr, 'recipient_id' );

      $recipients = Recipient::findmany( $recipient_ids );

      $response = $this->getRecipientsNames( $recipients );

      return [ 'success' => true, 'results' => $response ];

    }

    /**
     * For edit promo code
     * @param  int $promo_code_id - instance of crm_remittance_promo_codes.id
     * @return array objects - details on promo code query
     */
    public function get_promo_code_details( $promo_code_id )
    {

        $promoCode = RemittancePromoCode::find( $promo_code_id );

        $promoCodeUsers = '';

        $promoCodeRemitters = '';

        $promoCodeRecipients = '';

        if( !isset( $promoCode ) || !empty( $promoCode ) )
        {

          /*
           * Get promo code users( a.k.a remitters ) based from the remittance promo code id given.
           */
          $promoCodeUsers = RemittancePromoCodeUser::where( 'remittance_promo_code_id', '=', $promo_code_id )

                                                    -> groupby( 'remittance_promo_code_id' )

                                                    -> get();

        }

        if( !empty( $promoCodeUsers )  )
        {

          /*
           * Convert model to array
           */
          $promoCodeUsers = json_decode( $promoCodeUsers, true );

          if( !isset( $promoCodeUsers ) || !empty( $promoCodeUsers ) )
          {

            /*
             * Get all remitter id
             */
            $remitter_ids = array_column( $promoCodeUsers, 'remitter_business_id' );

            if( !empty( $remitter_ids ) )
            {

              $promoCodeRemitters = Remitter::findmany( $remitter_ids );

            }

          }

        }
        
        if( !empty( $promo_code_id ) && !empty( $remitter_ids ) )
        {

          /*
           * Get all recipients with respect to promo code id and remitter id
           */
          $promoCodeRecipients = RemittancePromoCodeUserRecipients::where( 'remittance_promo_code_id', '=', $promo_code_id )

                                                                  -> where( 'remitter_id', '=', $remitter_ids )

                                                                  -> groupby( 'remitter_id', 'recipient_id' )

                                                                  -> get();

          /*
           * Convert model to array
           */
          $promoCodeRecipients = !empty( $promoCodeRecipients ) ? json_decode( $promoCodeRecipients, true ) : ''; 

          /*
           * Get all remitter id
           */
          $recipient_ids = !empty( $promoCodeRecipients ) ? array_column( $promoCodeRecipients, 'recipient_id' ) : '';

          $promoCodeRecipients = !empty( $recipient_ids ) ? Recipient::findmany( $recipient_ids ) : '';

          $recipientsId = !empty( $promoCodeRecipients ) ? $this->getRecipientsNames( $promoCodeRecipients ) : '';

        }

        $promoCode[ 'promoCodeUsers' ] = $promoCodeRemitters;

        $promoCode[ 'promoCodeRecipients' ] = $recipientsId;

        return $promoCode;

    }

    /**
     * Restructure recipient array
     * @param  array $recipients set of recipients
     * @return array restructured recipient containing id and name               
     */
    public function getRecipientsNames( $recipients )
    {

      $isAvailable = !isset( $recipients ) || !empty( $recipients );

      $response = [];

      if( $isAvailable ) 
      {

        foreach( $recipients as $recipient )
        {

          if( is_null( $recipient ) || empty( $recipient ) ) 
          {
              
              continue;

          }

          $name = '';

          $id = $recipient[ 'id' ];

          /*
           * If RecipientPerson
           */
          if( $recipient->type = 'person' )
          {

              $recipientPerson = $recipient->recipientPerson;

              $name = ( !isset( $recipientPerson ) || !empty( $recipientPerson ) ) ? $recipientPerson[ 'firstname' ] . ' ' . $recipientPerson[ 'lastname' ] : ''; 

          }

          /*
           * If RecipientCompany
           */
          else if( $recipient->type = 'company' )
          {

            $recipientCompany = $recipient->recipientCompany;

            $name = ( !isset( $recipientCompany ) || !empty( $recipientCompany ) ) ? $recipientCompany[ 'company_name' ] : '';

          }

          $name .= ' | ' . $recipient[ 'email' ];

          $response [ $id ] = $name;

        }

      }

      return $response;

    }


    private function save_promo_code( $data ){

        $new_promo_code_data = array();

        $new_promo_code_data[ 'name' ] = $data[ 'promo_code' ];

        $new_promo_code_data[ 'description' ] = $data[ 'description' ];

        $new_promo_code_data[ 'value' ] = $data[ 'discounted_value' ];

        $new_promo_code_data[ 'type' ] = $data[ 'usage_type' ];

        $new_promo_code_data[ 'classification' ] = ( $data[ 'usage_type' ] == 'unli100' ? 'individual' : $data[ 'usage_classification' ] );

        $new_promo_code_data[ 'total_consumable' ] = ( $data[ 'usage_type' ] == 'unli100' ? 0 : $data[ 'total_consumable' ] );

        if( array_key_exists( 'user_classification', $data ) )
        {

            $new_promo_code_data[ 'user_action' ] = ( $data[ 'user_classification' ] == 'existing' ? $data[ 'user_action' ] : 'assign' );
        
        }

        $new_promo_code_data[ 'user_classification' ] = ( isset( $data[ 'user_classification_both' ] ) && ! empty( $data[ 'user_classification_both' ] ) ? $data[ 'user_classification_both' ] : ( $data[ 'usage_type' ] == 'unli100' ? 'existing' : $data[ 'user_classification' ] ) );

        $new_promo_code_data[ 'duration' ] = $data[ 'modal_duration' ];

        $new_promo_code_data[ 'expiration_date' ] = $data[ 'expiration_date' ];

        $new_promo_code_data[ 'created_by' ] = \Auth::user()->id;

        $new_promo_code_data[ 'created_at' ] = date( 'Y-m-d H:i:s', strtotime( '+10 hour' ) );

        $has_promo_code = !RemittancePromoCode::where( 'name', '=', $data[ 'promo_code' ] )

                                                -> get()

                                                -> isEmpty();

        if( !$has_promo_code ){     

            RemittancePromoCode::insert( $new_promo_code_data );

            return $new_promo_code_data;

        } 

        return null;

    }

    private function edit_promo_code( $data ){

        $new_promo_code_data = array();

        $new_promo_code_data[ 'name' ] = $data[ 'promo_code' ];

        $new_promo_code_data[ 'description' ] = $data[ 'description' ];

        $new_promo_code_data[ 'value' ] = $data[ 'discounted_value' ];

        $new_promo_code_data[ 'type' ] = $data[ 'usage_type' ];

        $new_promo_code_data[ 'classification' ] = ( $data[ 'usage_type' ] == 'unli100' ? 'individual' : $data[ 'usage_classification' ] );

        $new_promo_code_data[ 'total_consumable' ] = ( $data[ 'usage_type' ] == 'unli100' ? 0 : $data[ 'total_consumable' ] );

        if( array_key_exists( 'user_classification', $data ) )
        {

            $new_promo_code_data[ 'user_action' ] = ( $data[ 'user_classification' ] == 'existing' ? $data[ 'user_action' ] : 'assign' );
        
        }

        $new_promo_code_data[ 'user_classification' ] = ( isset( $data[ 'user_classification_both' ] ) && ! empty( $data[ 'user_classification_both' ] ) ? $data[ 'user_classification_both' ] : ( $data[ 'usage_type' ] == 'unli100' ? 'existing' : $data[ 'user_classification' ] ) );

        $new_promo_code_data[ 'duration' ] = $data[ 'duration' ];

        $new_promo_code_data[ 'expiration_date' ] = $data[ 'expiration_date' ];

        $new_promo_code_data[ 'created_by' ] = \Auth::user()->id;

        $new_promo_code_data[ 'created_at' ] = date( 'Y-m-d H:i:s', strtotime( '+10 hour' ) );

        RemittancePromoCode::where( 'id', '=', $data[ 'promo_code_id' ] )

                            ->update( $new_promo_code_data );

        return $new_promo_code_data;

    }

    private function is_promo_code_available()
    {


    }

    private function delete_promo_code( $data ){
        
        RemittancePromoCode::where( 'id', '=', $data[ 'promo_code_id' ] )

                            ->delete();

        return ['success'=>'true'];

    }

    public function search(){        

        $query = RemittancePromoCode::query();

        $inputs = Input::all();

        //Remove btn_sumbit before processing the fields
        unset( $inputs[ 'btn_submit' ] );

        //filter out empty values
        $inputs = array_filter( $inputs );

        foreach ( $inputs as $key => $value ) 
        {
            $key = preg_replace( '/^search_/', '', $key );
            $query->orWhere( $key, 'LIKE', '%'.$value.'%' );

        }

        return view('codes-promo')->with( 'remittancePromoCodes', $query->orderBy( 'id', 'desc' ) ->paginate( 15 ) );

    }

    // public function get_all_recipients_of_each_remitters( $remitters )
    // {

    //   $remitter_array = array();

    //   //Loop through all the remitters
    //   foreach ( $remitters as $remitter ) 
    //   {
        
    //     $recipients = $remitter->recipients;

    //     $recipients_array = array();

    //     //Loop though all the recipients with respect to this remitter
    //     foreach ( $recipients as $recipient ) 
    //     {

    //       $recipient_array = array();

    //       //Appened recipientPerson
    //       if( $recipient->type == 'person' )
    //       {

    //         $recipient_array[ 'recipientPerson' ] = $recipient->recipientPerson;

    //       }
          
    //       array_push( $recipients_array,  $recipient_array );

    //     }

    //     $remitter[ 'recipients' ] = $recipients_array;

    //     array_push( $remitter_array, $remitter );

    //   }

    //   return $remitter_array;

    // }

    public function test( $id )
    {

      //return response() -> json( $this->get_recipients( $id ) );

      return response()->json( $this -> get_promo_code_details( $id ) );

            //convert $remittances to array
      $remitters = json_decode( $this -> get_promo_code_remitters( $id ), true );

      //get all remitter_id from remittances
      $remitter_ids = array_column( $remitters, 'id' );

      //remove duplicate remitter_ids
      $remitter_ids = array_unique( $remitter_ids );

      return response()->json( $this -> get_recipients( $remitter_ids  ) );

      return response()->json( $this -> get_all_recipients_of_each_remitters( $remitters ) );

      // $recipients = $this->getRemitterRecipients( $id );

      // return response()->json( [$recipients] );

    }

}
