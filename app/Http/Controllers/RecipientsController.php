<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;


use Illuminate\Support\Str;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\File;

use Illuminate\Pagination\Paginator;


use App\Models\Recipient;

use App\Models\RecipientPerson;

use App\Models\RecipientCompany;

use App\Models\RecipientOtherRelationship;

use App\Models\RecipientAddress;

use App\Models\RecipientBank;

use App\Models\RecipientOtherBank;

use App\Models\RecipientContact;

use App\Models\RecipientNote;


use App\Models\Remitter;


use App\Models\Library\LibraryTitle;

use App\Models\Library\LibraryCivilStatus;

use App\Models\Library\LibraryCountry;

use App\Models\Library\LibraryState;

use App\Models\Library\LibraryCity;

use App\Models\Library\LibraryNationality;

use App\Models\Library\LibraryContactType;

use App\Models\Library\LibraryReceiveOptionList;

use App\Models\Library\LibraryRemitterRelationship;


class RecipientsController extends Controller {

    public function __construct() {
		
        $this->middleware( 'auth' );

    }
	
	public function index() {
		
		$recipients = $this -> getRecipients();
		
		//LIBRARIES
		$countries = LibraryCountry::orderBy( 'id', 'ASC' ) -> get();
		
		$contact_types = LibraryContactType::orderBy( 'id', 'ASC' ) -> get();
		
		$bank_types = LibraryReceiveOptionList::where( 'receive_option_id', '=', 2 ) -> orderBy( 'name', 'ASC' ) -> get();
		
		$relationships = LibraryRemitterRelationship::orderBy( 'name', 'ASC' ) -> get();
		
		$remitter = array();
		
		//CHECK IF SEARCH RECIPIENT REMITTER IS ISSET AND NOT EMPTY
		if( isset( $_GET[ 'search_recipient_remitter' ] ) && ! empty( $_GET[ 'search_recipient_remitter' ] ) ) {
			
			$remitter = Remitter::selectRaw( 'id, email, CONCAT_WS( " ", firstname, lastname ) as fullname' )
			
								-> where( 'id', '=', $_GET[ 'search_recipient_remitter' ] )

								-> get()
								
								-> first()

								-> toArray();		

		}
		
		return view( 'recipients',

			array(
			
				'recipients' => $recipients,
				
				'countries' => $countries, 
				
				'contact_types' => $contact_types,
				
				'bank_types' => $bank_types,
				
				'relationship' => $relationships,
				
				'remitter' => $remitter

			)

		);
        
    }

	public function getRemitters( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			$post = $request -> all();
			
			$remitter = $post[ 'data' ][ 'q' ];
			
			$search_results = Remitter::selectRaw( 'id, email, CONCAT_WS( " ", firstname, lastname ) as fullname' )
			
									-> having( 'fullname' , 'LIKE', '%'.$remitter.'%' )

									-> get() 
									
									-> toArray();

			$new_search_result = array();
			
			//LOOP ALL SEARCH RESULT
			foreach( $search_results as $key => $search_result ) {
				
				$new_search_result[ $key ][ 'id' ] = "{$search_result[ 'id' ]}";
				
				$new_search_result[ $key ][ 'text' ] = $search_result[ 'fullname' ] . ' | ' . $search_result[ 'email' ];
				
			}
			
			//SEND SOME RESPONSE
			return \Response::json( [ 'q' => $remitter, 'results' => $new_search_result ] );
							
		}
		
	}

	public function getModal( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			
			
			//LIBRARIES
			$titles = LibraryTitle::orderBy( 'ordering', 'ASC' ) -> get();

			$civil_status = LibraryCivilStatus::orderBy( 'ordering', 'ASC' ) -> get();

			$countries = LibraryCountry::orderBy( 'id', 'ASC' ) -> get();

			$states = LibraryState::where( 'country_id', 175 ) -> orderBy( 'id', 'ASC' ) -> get();

			$cities = LibraryCity::orderBy( 'id', 'ASC' ) -> get();

			$nationalities = LibraryNationality::orderBy( 'id', 'ASC' ) -> get();

			$contact_types = LibraryContactType::orderBy( 'id', 'ASC' ) -> get();
			
			$bank_types = LibraryReceiveOptionList::where( 'receive_option_id', '=', 2 ) -> orderBy( 'name', 'ASC' ) -> get();
			
			$relationships = LibraryRemitterRelationship::orderBy( 'name', 'ASC' ) -> get();
			
			$receive_option_list = LibraryReceiveOptionList::orderBy( 'receive_option_id', 'ASC' ) -> get();
			
			
			
			//EDIT FORM
			if( $request -> id != NULL || $request -> id != '' ) {
				
				//RECIPIENT DETAILS
				$recipient_details = Recipient::where( 'id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();
				
				//CHECK IF RECIPIENT DETAILS TYPE IS EQUAL TO PERSON
				if( $recipient_details[ 'type' ] == 'person' ) {
					
					$recipient_person_details = RecipientPerson::where( 'recipient_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();
					
				}
				
				//RECIPIENT DETAILS TYPE IS COMPANY
				else {
					
					$recipient_company_details = RecipientCompany::where( 'recipient_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();
					
				}
				
				//RECIPIENT REMITTER DETAILS
				$recipient_remitter = Remitter::where( 'id', '=', $recipient_details[ 'remitter_id' ] ) -> orderBy( 'id', 'ASC' ) -> first();

				//RECIPIENT ADDRESSES
				$recipient_addresses = RecipientAddress::where( 'recipient_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get();
				
				//RECIPIENT CONTACTS
				$recipient_contacts = RecipientContact::where( 'recipient_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get();
				
				//RECIPIENT BANKS
				$recipient_banks = RecipientBank::leftJoin( 'recipient_other_banks', function( $join ) {
					
										$join -> on( 'recipient_other_banks.recipient_bank_id', '=', 'recipient_banks.id' );
										
									} )
									
									-> select(

										'recipient_banks.id AS id',

										'recipient_banks.recipient_id AS recipient_id',

										'recipient_banks.receive_option_list_id AS receive_option_list_id',

										'recipient_banks.account_name AS account_name',

										'recipient_banks.account_branch AS account_branch',

										'recipient_banks.account_no AS account_no',

										'recipient_other_banks.bank_name AS bank_name',
										
										'recipient_banks.is_preferred AS is_preferred'

									)
									
									-> where( 'recipient_banks.recipient_id', '=', $request -> id ) 
									
									-> orderBy( 'recipient_banks.id', 'ASC' ) 
									
									-> get();
									
					
				//RECIPIENT NOTE
				$recipient_notes = RecipientNote::where( 'recipient_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get() -> first();
				
				//RECIPIENT OTHER RELATIONSHIP
				$recipient_other_relationship = RecipientOtherRelationship::where( 'recipient_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get() -> first();
				
				

				//RECIPIENT DETAILS
				$histories[] = $recipient_details[ 'revisionHistory' ];

				//CHECK IF RECIPIENT DETAILS TYPE IS EQUAL TO PERSON
				if( $recipient_details[ 'type' ] == 'person' ) {
					
					$histories[] = $recipient_person_details[ 'revisionHistory' ];
					
				}

				//RECIPIENT DETAILS TYPE IS COMPANY
				else {
					
					$histories[] = $recipient_company_details[ 'revisionHistory' ];
					
				}

				//RECIPIENT REMITTER
				$histories[] = $recipient_remitter[ 'revisionHistory' ];

				//RECIPIENT ADDRESS
				foreach( $recipient_addresses as $recipient_address ) {
					
					$histories[] = $recipient_address[ 'revisionHistory' ];
					
				}

				//RECIPIENT CONTACT
				foreach( $recipient_contacts as $recipient_contact ) {
					
					$histories[] = $recipient_contact[ 'revisionHistory' ];
					
				}

				//RECIPIENT BANKS
				foreach( $recipient_banks as $recipient_bank ) {
					
					$histories[] = $recipient_bank[ 'revisionHistory' ];
					
				}
				
				//RECIPIENT NOTES
				$histories[] = $recipient_notes[ 'revisionHistory' ];
				
				//RECIPIENT OTHER RELATIONSHIP
				$histories[] = $recipient_other_relationship[ 'revisionHistory' ];

				
				
				//DOUBLE RECOIL HISTORIES
				$new_histories = array();
				
				$x = 0;
				
				foreach( array_filter( $histories ) as $history ) {
					
					foreach( $history as $key => $hist ) {
						
						$new_histories[ $x ][ 'field_name' ] = $hist -> fieldName();
						
						$new_histories[ $x ][ 'old_value' ] = $hist -> oldValue();
						
						$new_histories[ $x ][ 'new_value' ] = $hist -> newValue();
						
						$new_histories[ $x ][ 'full_name' ] = $hist -> userResponsible() -> first_name . ' ' . $hist -> userResponsible() -> last_name;
						
						$x++;
						
					}
					
				}
			
			
			
				//RECIPIENT DATA
				$recipient_data = array(
				
					'action' => 'edit',
					
					'titles' => $titles,

					'civil_status' => $civil_status, 

					'countries' => $countries,

					'states' => $states,
					
					'cities' => $cities,

					'nationalities' => $nationalities,
					
					'contact_types' => $contact_types,
					
					'bank_types' => $bank_types,
					
					'relationships' => $relationships,
					
					'receive_option_list' => $receive_option_list,


					'recipient_id' => $recipient_details[ 'id' ],
					
					'recipient_details' => $recipient_details,
					
					'recipient_remitter' => $recipient_remitter,

					'recipient_addresses' => $recipient_addresses -> toArray(),

					'recipient_banks' => $recipient_banks -> toArray(),

					'recipient_contacts' => $recipient_contacts -> toArray(),
					
					'recipient_notes' => $recipient_notes,
					
					'recipient_other_relationship' => $recipient_other_relationship,
					
					
					'histories' => $new_histories


				);
			
			
			
				//CHECK IF RECIPIENT DETAILS TYPE IS EQUAL TO PERSON
				if( $recipient_details[ 'type' ] == 'person' ) {
					
					$recipient_data = array_merge( $recipient_data, array( 'recipient_person_details' => $recipient_person_details ) );
					
				}

				//RECIPIENT DETAILS TYPE IS COMPANY
				else {
					
					$recipient_data = array_merge( $recipient_data, array( 'recipient_company_details' => $recipient_company_details ) );
					
				}
			
			
			
				$content = view( 'modals.recipients' ) -> with( $recipient_data ) -> render();
								
				return \Response::json( ['success' => true, 'result' => [ 'recipient_id' => $request -> id, 'content' => $content, 'action' => 'edit' ] ] );
				
			}
			
			
			
			//NEW FORM
			else {

				$content = view( 'modals.recipients' )

							-> with( [

								'action' => 'new',

								'titles' => $titles,

								'civil_status' => $civil_status, 

								'countries' => $countries,

								'states' => $states,
								
								'cities' => $cities,

								'nationalities' => $nationalities,
								
								'contact_types' => $contact_types,
								
								'bank_types' => $bank_types,
								
								'relationships' => $relationships,
								
								'receive_option_list' => $receive_option_list

							] ) 

							-> render();

				return \Response::json( ['success' => true, 'result' => [ 'recipient_id' => 0, 'content' => $content, 'action' => 'new' ] ] );
				
			}

		}
		
	}

	public function postForm( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {
				
				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					

					/* 
					*-----------------------------------------
					* REMITTER PROFILE VALIDATING EMAILS START
					*-----------------------------------------
					*/
					
					
					
					
					//CHECK IF RECIPIENT ID IS EMPTY
					if( empty( $input[ 'recipient_id' ] ) ) {
						
						//GET PRIMARY EMAIL AND CHECK IF ALREADY EXISTS FOR NEW REMITTER
						$email = Recipient::where( 'email', $input[ 'recipient_primary_email_address' ] ) -> first( [ 'email' ] );
						
						if( ! is_null( $email ) && empty( $input[ 'recipient_id' ] ) ) {
							
							return \Response::json( ['success' => false, 'error_message' => 'Existing primary email address!' ] );
							
						}
					
						//GET SECONDARY EMAIL AND CHECK IF ALREADY EXISTS FOR NEW REMITTER
						$secondary_email = Recipient::where( 'secondary_email', $input[ 'recipient_secondary_email_address' ] ) -> first( [ 'secondary_email' ] );
						
						if( ! is_null( $secondary_email ) && empty( $input[ 'recipient_id' ] ) ) {
							
							return \Response::json( ['success' => false, 'error_message' => 'Existing secondary email address!' ] );
							
						}

					}
					
					
					
					
					/* 
					*-----------------------------------------
					* REMITTER PROFILE VALIDATING EMAILS END
					*-----------------------------------------
					*/
					
					
					
					
					/* 
					*------------------------------
					* RECIPIENT PROFILE SAVING START
					*------------------------------
					*/
					
					
					
					
					//CHECK IF RECIPIENT ID IS NOT EMPTY
					if( ! empty( $input[ 'recipient_id' ] ) ) {
						
						$new_recipient = Recipient::find( $input[ 'recipient_id' ] ) ;
						
						$action = 'edit';
						
					}
					
					else {
						
						$new_recipient = new Recipient;
						
						//GENERATE SOME ID NUMBER
						$new_recipient -> id_number = Str::random( 8 );
						
						$new_recipient -> email = ( isset( $input[ 'recipient_primary_email_address' ] ) && ! empty( $input[ 'recipient_primary_email_address' ] ) ? $input[ 'recipient_primary_email_address' ] : NULL );

						$action = 'add';

					}
					
					$new_recipient -> remitter_id = ( isset( $input[ 'recipient_remitter' ] ) && ! empty( $input[ 'recipient_remitter' ] ) ? $input[ 'recipient_remitter' ] : NULL);
					
					$new_recipient -> secondary_email = ( isset( $input[ 'recipient_secondary_email_address' ] ) && ! empty( $input[ 'recipient_secondary_email_address' ] ) ? $input[ 'recipient_secondary_email_address' ] : NULL );
					
					$new_recipient -> filename = ( isset( $input[ 'recipient_profile_avatar' ] ) && ! empty( $input[ 'recipient_profile_avatar' ] ) ? $input[ 'recipient_profile_avatar' ] : NULL );
					
					$new_recipient -> remitter_relationship_id = ( isset( $input[ 'recipient_relationship' ] ) && ! empty( $input[ 'recipient_relationship' ] ) ? $input[ 'recipient_relationship' ] : NULL );
					
					$new_recipient -> type = ( isset( $input[ 'recipient_is_company' ] ) && ! empty( $input[ 'recipient_is_company' ] ) ? 'company' : 'person' );
					
					$new_recipient -> save();
					
					$recipient_id = $new_recipient['id'];
					
					

					/* 
					*------------------------------
					* RECIPIENT PROFILE SAVING END
					*------------------------------
					*/
					
					
					
					
					/* 
					*-----------------------
					* RECIPIENT PERSON START
					*-----------------------
					*/
					
					
					
					//CHECK IF RECIPIENT IS COMPANY IS NOT ISSET
					if( ! isset( $input[ 'recipient_is_company' ] ) ) {
						
						//CHECK IF RECIPIENT ID IS NOT EMPTY
						if( ! empty( $input[ 'recipient_id' ] ) ) {
							
							$new_recipient_person = RecipientPerson::where( 'recipient_id', '=', $input[ 'recipient_id' ] ) -> first();
							
							//CHECK IF NEW RECIPIENT PERSON IS EMPTY
							if( empty( $new_recipient_person ) ) {
								
								$new_recipient_person = new RecipientPerson;
								
							}
							
						}
						
						else {
							
							$new_recipient_person = new RecipientPerson;
							
						}
						
						$new_recipient_person -> recipient_id = $recipient_id;
						
						$new_recipient_person -> title_id = ( isset( $input[ 'recipient_title' ] ) && ! empty( $input[ 'recipient_title' ] ) ? $input[ 'recipient_title' ] : NULL);
						
						$new_recipient_person -> firstname = ( isset( $input[ 'recipient_firstname' ] ) && ! empty( $input[ 'recipient_firstname' ] ) ? $input[ 'recipient_firstname' ] : NULL );
						
						$new_recipient_person -> middlename = ( isset( $input[ 'recipient_middlename' ] ) && ! empty( $input[ 'recipient_middlename' ] ) ? $input[ 'recipient_middlename' ] : NULL );
						
						$new_recipient_person -> lastname = ( isset( $input[ 'recipient_lastname' ] ) && ! empty( $input[ 'recipient_lastname' ] ) ? $input[ 'recipient_lastname' ] : NULL );
						
						$new_recipient_person -> gender = ( isset( $input[ 'recipient_gender' ] ) && ! empty( $input[ 'recipient_gender' ] ) ? $input[ 'recipient_gender' ] : NULL );
						
						$new_recipient_person -> birthday = ( isset( $input[ 'recipient_birthday' ] ) && ! empty( $input[ 'recipient_birthday' ] ) ? $input[ 'recipient_birthday' ] : NULL );
						
						$new_recipient_person -> civil_status_id = ( isset( $input[ 'recipient_civil_status' ] ) && ! empty( $input[ 'recipient_civil_status' ] ) ? $input[ 'recipient_civil_status' ] : NULL );
						
						$new_recipient_person -> nationality_id = ( isset( $input[ 'recipient_nationality' ] ) && ! empty( $input[ 'recipient_nationality' ] ) ? $input[ 'recipient_nationality' ] : NULL );

						$new_recipient_person -> save();
						
						$recipient_person_id = $new_recipient_person['id'];
					
					}
					
					//REMOVE EXISTING RECIPIENT PERSON
					else {
						
						//CHECK IF RECIPIENT ID IS NOT EMPTY
						if( ! empty( $input[ 'recipient_id' ] ) ) {
							
							//REMOVE RECIPIENT PERSON
							$remove_recipient_person = RecipientPerson::where( 'recipient_id', '=', $input[ 'recipient_id' ] ) -> delete();
							
						}
						
					}
					
					
					
					/* 
					*---------------------
					* RECIPIENT PERSON END
					*---------------------
					*/
					
					
					
					
					/* 
					*------------------------
					* RECIPIENT COMPANY START
					*------------------------
					*/
					
					
					
					//CHECK IF RECIPIENT IS COMPANY IS NOT ISSET
					if( isset( $input[ 'recipient_is_company' ] ) ) {
					
						//CHECK IF RECIPIENT ID IS NOT EMPTY
						if( ! empty( $input[ 'recipient_id' ] ) ) {
							
							$new_recipient_company = RecipientCompany::where( 'recipient_id', '=', $input[ 'recipient_id' ] ) -> first();
							
							//CHECK IF NEW RECIPIENT COMPANY IS EMPTY
							if( empty( $new_recipient_company ) ) {
								
								$new_recipient_company = new RecipientCompany;
								
							}
							
						}
						
						else {
							
							$new_recipient_company = new RecipientCompany;
							
						}
						
						$new_recipient_company -> recipient_id = $recipient_id;
						
						$new_recipient_company -> company_name = ( isset( $input[ 'recipient_company_name' ] ) && ! empty( $input[ 'recipient_company_name' ] ) ? $input[ 'recipient_company_name' ] : NULL);
						
						$new_recipient_company -> reg_number = ( isset( $input[ 'recipient_reg_number' ] ) && ! empty( $input[ 'recipient_reg_number' ] ) ? $input[ 'recipient_reg_number' ] : NULL );
						
						$new_recipient_company -> contact_person = ( isset( $input[ 'recipient_contact_person' ] ) && ! empty( $input[ 'recipient_contact_person' ] ) ? $input[ 'recipient_contact_person' ] : NULL );
						
						$new_recipient_company -> authorized_person = ( isset( $input[ 'recipient_authorized_person' ] ) && ! empty( $input[ 'recipient_authorized_person' ] ) ? $input[ 'recipient_authorized_person' ] : NULL );
						
						$new_recipient_company -> save();
						
						$recipient_company_id = $new_recipient_company['id'];
					
					}
					
					//REMOVE EXISTING RECIPIENT COMPANY
					else {
						
						//CHECK IF RECIPIENT ID IS NOT EMPTY
						if( ! empty( $input[ 'recipient_id' ] ) ) {
							
							//REMOVE RECIPIENT COMPANY
							$remove_recipient_company = RecipientCompany::where( 'recipient_id', '=', $input[ 'recipient_id' ] ) -> delete();
							
						}
						
					}
					
					
					
					/* 
					*-----------------------
					* RECIPIENT COMPANY END
					*----------------------
					*/
					
					
					
					
					/* 
					*-----------------------------------
					* RECIPIENT OTHER RELATIONSHIP START
					*-----------------------------------
					*/
					
					
					
					//CHECK IF RECIPIENT RELATIONSHIP IS ISSET AND NOT EMPTY AND EQUAL TO 10 - OTHER
					if( isset( $input[ 'recipient_relationship' ] ) && ! empty( $input[ 'recipient_relationship' ] ) && $input[ 'recipient_relationship' ] == 1 ) {

						//CHECK IF RECIPIENT ID IS NOT EMPTY
						if( ! empty( $input[ 'recipient_id' ] ) ) {
							
							$new_recipient_other_relationship = RecipientOtherRelationship::where( 'recipient_id', '=', $input[ 'recipient_id' ] ) -> first();
							
							//CHECK IF NEW RECIPIENT OTHER RELATIONSHIP IS EMPTY
							if( empty( $new_recipient_other_relationship ) ) {
								
								$new_recipient_other_relationship = new RecipientOtherRelationship;
								
							}
							
						}
						
						else {
							
							$new_recipient_other_relationship = new RecipientOtherRelationship;
							
						}
						
						$new_recipient_other_relationship -> recipient_id  = $recipient_id;
						
						$new_recipient_other_relationship -> relationship = ( isset( $input[ 'recipient_other_relationship' ] ) && ! empty( $input[ 'recipient_other_relationship' ] ) ? $input[ 'recipient_other_relationship' ] : NULL );
						
						$new_recipient_other_relationship -> save();
						
						$recipient_other_relationship_id = $new_recipient_other_relationship[ 'id' ];

					}
					
					else {
						
						//CHECK IF RECIPIENT ID IS NOT EMPTY
						if( ! empty( $input[ 'recipient_id' ] ) ) {
							
							//DELETE RECIPIENT OTHER RELATIONSHIP
							$remove_recipient_other_relationship = RecipientOtherRelationship::where( 'recipient_id', '=', $input[ 'recipient_id' ] ) -> delete();
							
						}
						
					}
					
					
					
					/* 
					*---------------------------------
					* RECIPIENT ADDRESS SAVING START
					*---------------------------------
					*/
					
					
					
					$recipient_address_is_preferred = FALSE;
					
					$existing_recipient_address_ids = array();
					
					$old_recipient_address_ids = RecipientAddress::where( 'recipient_id', '=', $recipient_id ) -> pluck( 'id' ) -> toArray();
					

					$recipient_address_olds = ( isset( $input[ 'recipient_address_old' ] ) && ! empty( $input[ 'recipient_address_old' ] ) ? array_values( $input[ 'recipient_address_old' ] ) : '' );
					
					//CHECK IF RECIPIENT ADDRESS OLD IS NOT EMPTY
					if( ! empty ( $recipient_address_olds ) ) {
						
						//LOOP ALL RECIPIENT ADDRESSES
						foreach( $recipient_address_olds as $key => $recipient_address_old ) {
							
							$existing_recipient_address_ids[] = $recipient_address_old[ 'id' ];
							
							$new_recipient_address_old = RecipientAddress::find( $recipient_address_old[ 'id' ] );

							$new_recipient_address_old -> recipient_id = $recipient_id;
							
							$new_recipient_address_old -> current_address = ( isset( $recipient_address_old[ 'current_address' ] ) && ! empty( $recipient_address_old[ 'current_address' ] ) ? $recipient_address_old[ 'current_address' ] : NULL );

							$new_recipient_address_old -> permanent_address = ( isset( $recipient_address_old[ 'permanent_address' ] ) && ! empty( $recipient_address_old[ 'permanent_address' ] ) ? $recipient_address_old[ 'permanent_address' ] : NULL );
							
							$new_recipient_address_old -> country_id = ( isset( $recipient_address_old[ 'country' ] ) && ! empty( $recipient_address_old[ 'country' ] ) ? $recipient_address_old[ 'country' ] : NULL );
							
							$new_recipient_address_old -> state_id = ( isset( $recipient_address_old[ 'state' ] ) && ! empty( $recipient_address_old[ 'state' ] ) ? $recipient_address_old[ 'state' ] : NULL );

							$new_recipient_address_old -> city_id = ( isset( $recipient_address_old[ 'city' ] ) && ! empty( $recipient_address_old[ 'city' ] ) ? $recipient_address_old[ 'city' ] : NULL );
							
							$new_recipient_address_old -> postal_code = ( isset( $recipient_address_old[ 'postal' ] ) && ! empty( $recipient_address_old[ 'postal' ] ) ? $recipient_address_old[ 'postal' ] : NULL );
							
							$new_recipient_address_old -> is_preferred = ( isset( $recipient_address_old[ 'is_preferred' ] ) && ! empty( $recipient_address_old[ 'is_preferred' ] ) ? TRUE : FALSE );

							$new_recipient_address_old -> save();
							
							$recipient_address_id  = $new_recipient_address_old[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $recipient_address_old[ 'is_preferred' ] ) && ! empty( $recipient_address_old[ 'is_preferred' ] ) ) {
								
								$recipient_address_is_preferred = TRUE;
								
							}
							
						}
						
					}
					
					
					
					//DELETE RECORD
					$remove_recipient_address_ids = array_diff( $old_recipient_address_ids, $existing_recipient_address_ids );
					
					//CHECK IF REMOVE RECIPIENT ADDRESS IS NOT EMPTY
					if( ! empty ( $remove_recipient_address_ids ) ) {
						
						//LOOP ALL RECORD TO BE REMOVED
						foreach( $remove_recipient_address_ids as $remove_recipient_address_id ) {
							
							//FIND REMOVE RECIPIENT ADDRESS
							$remove_recipient_address = RecipientAddress::find( $remove_recipient_address_id );
							
							//DELETE REMOVE RECIPIENT ADDRESS
							$remove_recipient_address -> delete();
							
						}
						
					}
					
					
					
					//INSERT NEW RECORD
					$recipient_addresses = ( isset( $input[ 'recipient_address' ] ) && ! empty( $input[ 'recipient_address' ] ) ? array_values( $input[ 'recipient_address' ] ) : '' );
					
					//CHECK IF REMITTER ADDRESS IS NOT EMPTY
					if( ! empty ( $recipient_addresses ) ) {
						
						//LOOP ALL REMITTER ADDRESSES
						foreach( $recipient_addresses as $key => $recipient_address ) {

							$new_recipient_address = new RecipientAddress;

							$new_recipient_address -> recipient_id = $recipient_id;
							
							$new_recipient_address -> current_address = ( isset( $recipient_address[ 'current_address' ] ) && ! empty( $recipient_address[ 'current_address' ] ) ? $recipient_address[ 'current_address' ] : NULL );

							$new_recipient_address -> permanent_address = ( isset( $recipient_address[ 'permanent_address' ] ) && ! empty( $recipient_address[ 'permanent_address' ] ) ? $recipient_address[ 'permanent_address' ] : NULL );
							
							$new_recipient_address -> country_id = ( isset( $recipient_address[ 'country' ] ) && ! empty( $recipient_address[ 'country' ] ) ? $recipient_address[ 'country' ] : NULL );
							
							$new_recipient_address -> state_id = ( isset( $recipient_address[ 'state' ] ) && ! empty( $recipient_address[ 'state' ] ) ? $recipient_address[ 'state' ] : NULL );

							$new_recipient_address -> city_id = ( isset( $recipient_address[ 'city' ] ) && ! empty( $recipient_address[ 'city' ] ) ? $recipient_address[ 'city' ] : NULL );
							
							$new_recipient_address -> postal_code = ( isset( $recipient_address[ 'postal' ] ) && ! empty( $recipient_address[ 'postal' ] ) ? $recipient_address[ 'postal' ] : NULL );
							
							$new_recipient_address -> is_preferred = ( isset( $recipient_address[ 'is_preferred' ] ) && ! empty( $recipient_address[ 'is_preferred' ] ) ? TRUE : FALSE );

							$new_recipient_address -> save();
							
							$recipient_address_id  = $new_recipient_address[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $recipient_address[ 'is_preferred' ] ) && ! empty( $recipient_address[ 'is_preferred' ] ) ) {
								
								$recipient_address_is_preferred = TRUE;
								
							}
							
						}
						
					}
					
					
					
					/* 
					*---------------------------------
					* RECIPIENT ADDRESS SAVING END
					*---------------------------------
					*/
					
					
					
					
					$recipient_contact_is_preferred = FALSE;
					
					$existing_recipient_contact_ids = array();
					
					$old_recipient_contact_ids = RecipientContact::where( 'recipient_id', '=', $recipient_id ) -> pluck( 'id' ) -> toArray();
					
					
					
					$recipient_contact_olds = ( isset( $input[ 'recipient_contact_old' ] ) && ! empty( $input[ 'recipient_contact_old' ] ) ? array_values( $input[ 'recipient_contact_old' ] ) : '' );
					
					//RECIPIENT CONTACT EXISTING
					if( ! empty( $recipient_contact_olds ) ) {
						
						//LOOP ALL RECIPIENT CONTACT
						foreach( $recipient_contact_olds as $key => $recipient_contact_old ) {
							
							$existing_recipient_contact_ids[] = $recipient_contact_old[ 'id' ];

							$new_recipient_contact_old = RecipientContact::find( $recipient_contact_old[ 'id' ] );

							$new_recipient_contact_old -> recipient_id = $recipient_id;
							
							$new_recipient_contact_old -> contact_type_id = ( isset( $recipient_contact_old[ 'type' ] ) && ! empty( $recipient_contact_old[ 'type' ] ) ? $recipient_contact_old[ 'type' ] : NULL );
							
							$new_recipient_contact_old -> contact = ( isset( $recipient_contact_old[ 'value' ] ) && ! empty( $recipient_contact_old[ 'value' ] ) ? $recipient_contact_old[ 'value' ] : NULL );
							
							$new_recipient_contact_old -> is_preferred = ( isset( $recipient_contact_old[ 'is_preferred' ] ) && ! empty( $recipient_contact_old[ 'is_preferred' ] ) ? TRUE : FALSE );
							
							$new_recipient_contact_old -> save();
							
							$recipient_contact_id  = $new_recipient_contact_old[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $recipient_contact_old[ 'is_preferred' ] ) && ! empty( $recipient_contact_old[ 'is_preferred' ] ) ) {
								
								$recipient_contact_is_preferred = TRUE;
								
							}
							
						}
						
					}
					
					
					
					//DELETE RECORD
					$remove_recipient_contact_ids = array_diff( $old_recipient_contact_ids, $existing_recipient_contact_ids );
					
					//CHECK IF REMOVE RECIPIENT CONTACT IS NOT EMPTY
					if( ! empty ( $remove_recipient_contact_ids ) ) {
						
						//LOOP ALL RECORD TO BE REMOVED
						foreach( $remove_recipient_contact_ids as $remove_recipient_contact_id ) {
							
							//FIND REMOVE RECIPIENT CONTACT
							$remove_recipient_contact = RecipientContact::find( $remove_recipient_contact_id );
							
							//DELETE REMOVE RECIPIENT CONTACT
							$remove_recipient_contact -> delete();
							
						}
						
					}
					
					
					
					$recipient_contacts = ( isset( $input[ 'recipient_contact' ] ) && ! empty( $input[ 'recipient_contact' ] ) ? array_values( $input[ 'recipient_contact' ] ) : '' );
					
					//REMITTER CONTACT EXISTING
					if( ! empty( $recipient_contacts ) ) {
						
						//LOOP ALL REMITTER CONTACT
						foreach( $recipient_contacts as $key => $recipient_contact ) {

							$new_recipient_contact = new RecipientContact;

							$new_recipient_contact -> recipient_id = $recipient_id;
							
							$new_recipient_contact -> contact_type_id = ( isset( $recipient_contact[ 'type' ] ) && ! empty( $recipient_contact[ 'type' ] ) ? $recipient_contact[ 'type' ] : NULL );
							
							$new_recipient_contact -> contact = ( isset( $recipient_contact[ 'value' ] ) && ! empty( $recipient_contact[ 'value' ] ) ? $recipient_contact[ 'value' ] : NULL );
							
							$new_recipient_contact -> is_preferred = ( isset( $recipient_contact[ 'is_preferred' ] ) && ! empty( $recipient_contact[ 'is_preferred' ] ) ? TRUE : FALSE );
							
							$new_recipient_contact -> save();
							
							$recipient_contact_id  = $new_recipient_contact[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $recipient_contact[ 'is_preferred' ] ) && ! empty( $recipient_contact[ 'is_preferred' ] ) ) {
								
								$recipient_contact_is_preferred = TRUE;
								
							}
							
						}
						
					}
					
					
					
					/* 
					*------------------------------
					* REMITTER CONTACT SAVING END
					*------------------------------
					*/
					
					
					
					
					
					/* 
					*----------------------------
					* RECIPIENT BANK SAVING START
					*----------------------------
					*/
					
					
					
					$recipient_bank_is_preferred = FALSE;
					
					$existing_recipient_bank_ids = array();
					
					$old_recipient_bank_ids = RecipientBank::where( 'recipient_id', '=', $recipient_id ) -> pluck( 'id' ) -> toArray();
					
					
					
					$recipient_bank_olds = ( isset( $input[ 'recipient_bank_old' ] ) && ! empty( $input[ 'recipient_bank_old' ] ) ? array_values( $input[ 'recipient_bank_old' ] ) : '' );
					
					//RECIPIENT BANK EXISTING
					if( ! empty ( $recipient_bank_olds ) ) {
						
						//LOOP ALL RECIPIENT BANK
						foreach( $recipient_bank_olds as $key => $recipient_bank_old ) {
							
							$existing_recipient_bank_ids[] = $recipient_bank_old[ 'id' ];

							$new_recipient_bank_old = RecipientBank::find( $recipient_bank_old[ 'id' ] );
							
							$new_recipient_bank_old -> recipient_id = $recipient_id;
							
							$new_recipient_bank_old -> receive_option_list_id = ( isset( $recipient_bank_old[ 'receive_option_list_id' ] ) && ! empty( $recipient_bank_old[ 'receive_option_list_id' ] ) ? $recipient_bank_old[ 'receive_option_list_id' ] : NULL );
							
							$new_recipient_bank_old -> account_name = ( isset( $recipient_bank_old[ 'account_name' ] ) && ! empty( $recipient_bank_old[ 'account_name' ] ) ? $recipient_bank_old[ 'account_name' ] : NULL );
							
							$new_recipient_bank_old -> account_branch = ( isset( $recipient_bank_old[ 'account_branch' ] ) && ! empty( $recipient_bank_old[ 'account_branch' ] ) ? $recipient_bank_old[ 'account_branch' ] : NULL );
							
							$new_recipient_bank_old -> account_no = ( isset( $recipient_bank_old[ 'account_no' ] ) && ! empty( $recipient_bank_old[ 'account_no' ] ) ? $recipient_bank_old[ 'account_no' ] : NULL );
							
							$new_recipient_bank_old -> is_preferred = ( isset( $recipient_bank_old[ 'is_preferred' ] ) && ! empty( $recipient_bank_old[ 'is_preferred' ] ) ? TRUE : FALSE );
							
							$new_recipient_bank_old -> save();

							$recipient_bank_old_id  = $new_recipient_bank_old[ 'id' ];
							
							
							//CHECK IF RECIPIENT RECEIVE OPTION LIST ID IS EQUAL TO 41 = OTHER BANK
							if( $recipient_bank_old[ 'receive_option_list_id' ] == 41 ) {
								
								
								$existing_recipient_other_bank = RecipientOtherBank::where( 'recipient_bank_id', '=', $recipient_bank_old[ 'id' ] ) -> first( [ 'id' ] );
								
								
								//CHECK IF RECIPIENT OTHER BANK ID EXISTS
								if( $existing_recipient_other_bank[ 'id' ] ) {
									
									$new_receive_option_other_bank_old = RecipientOtherBank::find( $existing_recipient_other_bank[ 'id' ] );
									
								}
								
								//RECIPIENT OTHER BANK DOESN'T EXISTS
								else {
								
									$new_receive_option_other_bank_old = new RecipientOtherBank;
									
									$new_receive_option_other_bank_old -> recipient_bank_id = $recipient_bank_old_id;
								
								}
								
								
								$new_receive_option_other_bank_old -> bank_name = ( isset( $recipient_bank_old[ 'bank_name' ] ) && ! empty( $recipient_bank_old[ 'bank_name' ] ) ? $recipient_bank_old[ 'bank_name' ] : NULL );
								
								$new_receive_option_other_bank_old -> save();
								
								$receive_option_other_bank_old_id  = $new_receive_option_other_bank_old[ 'id' ];
								
								
							}
							
							else {
								
								$remove_recipient_other_bank = RecipientOtherBank::where( 'recipient_bank_id', '=', $recipient_bank_old[ 'id' ] ) -> delete();
								
							}
							
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $recipient_bank_old[ 'is_preferred' ] ) && ! empty( $recipient_bank_old[ 'is_preferred' ] ) ) {
								
								$recipient_bank_is_preferred = TRUE;
								
							}

						}

					}
					
					
					
					//DELETE RECORD
					$remove_recipient_bank_ids = array_diff( $old_recipient_bank_ids, $existing_recipient_bank_ids );
					
					//CHECK IF REMOVE RECIPIENT BANK IS NOT EMPTY
					if( ! empty ( $remove_recipient_bank_ids ) ) {
						
						//LOOP ALL RECORD TO BE REMOVED
						foreach( $remove_recipient_bank_ids as $remove_recipient_bank_id ) {
								
								
							//FIND REMOVE RECIPIENT OTHER BANK
							$remove_recipient_other_bank = RecipientOtherBank::where( 'recipient_bank_id', '=', $remove_recipient_bank_id );
							
							//DELETE REMOVE RECIPIENT BANK
							$remove_recipient_other_bank -> delete();
								
								
							//FIND REMOVE RECIPIENT BANK
							$remove_recipient_bank = RecipientBank::find( $remove_recipient_bank_id );
							
							//DELETE REMOVE RECIPIENT BANK
							$remove_recipient_bank -> delete();
								
								
						}
						
					}
					
					
					
					$recipient_banks = ( isset( $input[ 'recipient_bank' ] ) && ! empty( $input[ 'recipient_bank' ] ) ? array_values( $input[ 'recipient_bank' ] ) : '' );
					
					//RECIPIENT BANK EXISTING
					if( ! empty ( $recipient_banks ) ) {
						
						//LOOP ALL RECIPIENT BANK
						foreach( $recipient_banks as $key => $recipient_bank ) {

							$new_recipient_bank = new RecipientBank;

							$new_recipient_bank -> recipient_id = $recipient_id;

							$new_recipient_bank -> receive_option_list_id = ( isset( $recipient_bank[ 'receive_option_list_id' ] ) && ! empty( $recipient_bank[ 'receive_option_list_id' ] ) ? $recipient_bank[ 'receive_option_list_id' ] : NULL );

							$new_recipient_bank -> account_name = ( isset( $recipient_bank[ 'account_name' ] ) && ! empty( $recipient_bank[ 'account_name' ] ) ? $recipient_bank[ 'account_name' ] : NULL );

							$new_recipient_bank -> account_branch = ( isset( $recipient_bank[ 'account_branch' ] ) && ! empty( $recipient_bank[ 'account_branch' ] ) ? $recipient_bank[ 'account_branch' ] : NULL );

							$new_recipient_bank -> account_no = ( isset( $recipient_bank[ 'account_no' ] ) && ! empty( $recipient_bank[ 'account_no' ] ) ? $recipient_bank[ 'account_no' ] : NULL );

							$new_recipient_bank -> is_preferred = ( isset( $recipient_bank[ 'is_preferred' ] ) && ! empty( $recipient_bank[ 'is_preferred' ] ) ? TRUE : FALSE );

							$new_recipient_bank -> save();

							$recipient_bank_id  = $new_recipient_bank[ 'id' ];
							
							
							//CHECK IF RECEIVE OPTION LIST ID IS EQUAL TO 41 - OTHER BANK
							if( $recipient_bank[ 'receive_option_list_id' ] == 41 ) {
								
								$new_receive_option_other_bank = new RecipientOtherBank;
								
								$new_receive_option_other_bank -> recipient_bank_id = $recipient_bank_id;
								
								$new_receive_option_other_bank -> bank_name = ( isset( $recipient_bank[ 'bank_name' ] ) && ! empty( $recipient_bank[ 'bank_name' ] ) ? $recipient_bank[ 'bank_name' ] : NULL );
								
								$new_receive_option_other_bank -> save();
								
								$receive_option_other_bank_id  = $new_receive_option_other_bank[ 'id' ];
								
							}
							
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $recipient_bank[ 'is_preferred' ] ) && ! empty( $recipient_bank[ 'is_preferred' ] ) ) {
								
								$recipient_bank_is_preferred = TRUE;
								
							}
							
						}

					}
					
					
					
					/* 
					*--------------------------
					* RECIPIENT BANK SAVING END
					*--------------------------
					*/
					
					
					
					
					
					//ADD SOME NOTE COMING FROM CURRENT LOGGED IN USER
					if( isset( $input[ 'recipient_added_note' ] ) && ! empty( $input[ 'recipient_added_note' ] ) ) {
						
						//CHECK IF RECIPIENT ID IS NOT EMPTY
						if( ! empty( $input[ 'recipient_id' ] ) ) {
							
							$new_recipient_note = RecipientNote::where( 'recipient_id', '=', $input[ 'recipient_id' ] ) -> first();
							
							//CHECK IF NEW RECIPIENT NOTE IS EMPTY
							if( empty( $new_recipient_note ) ) {
								
								$new_recipient_note = new RecipientNote;
								
							}
							
						}
						
						else {
							
							$new_recipient_note = new RecipientNote;

						}
						
						$new_recipient_note -> recipient_id = $recipient_id;
						
						$new_recipient_note -> note = ( isset( $input[ 'recipient_added_note' ] ) && ! empty( $input[ 'recipient_added_note' ] ) ? trim( $input[ 'recipient_added_note' ] ) : NULL );
						
						$new_recipient_note -> created_by = Auth::user()->ID;
						
						$new_recipient_note -> save();
						
						$recipient_note_id = $new_recipient_note[ 'id' ];
						
					}
					
					
					
					
					//COMMIT ALL TRANSACTION
					DB::commit();




					$recipient_table_template = view( 'tables.recipient_table',
					
						array( 'recipients' => $this -> getRecipients() )
						
					)
					
					-> render();




					//SEND SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'recipient_id' => $recipient_id, 'recipient_table_template' => $recipient_table_template, 'action' => $action ) ] );




				}
				
				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
			
			}
			
		}
		
	}
	
	public function deleteRecipients( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {
				
				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					
					
					//CHECK IF RECIPIENT IS ISSET AND NOT EMPTY
					if( isset( $input[ 'recipient' ] ) && ! empty( $input[ 'recipient' ] ) && count( $input[ 'recipient' ] ) ) {
						
						//LOOP ALL RECIPIENT
						foreach( $input[ 'recipient' ] AS $key => $recipient ) {
							
							//REMOVE RECIPIENT
							$remove_recipient = Recipient::find( $recipient ) -> delete();
							
						}
						
					}
					
					
					//COMMIT ALL TRANSACTION
					DB::commit();
					
					
					//GET ALL REMITTANCE TABLES
					$recipient_table_template = view( 'tables.recipient_table',
					
						array( 'recipients' => $this -> getRecipients() )
						
					)
					
					-> render();
					
					//SEND SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'total_recipient' => count( $input[ 'recipient' ] ), 'recipient_table_template' => $recipient_table_template, 'action' => 'delete' ) ] );
					
					
				
			}
			
				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
				
			}

		}
		
	}
	
	
	private function getRecipients() {
		
		//REMOVE PAGE FROM SEARCH QUERY
		UNSET( $_GET[ 'page' ] );
		
		if( isset( $_GET ) && ! empty( array_filter( $_GET ) ) ) {
			
			$search_query = $this -> searchParameter();
			
			$recipients = DB::table( 'recipients' )
			
							-> leftJoin( 'recipient_persons', 'recipients.id', '=', 'recipient_persons.recipient_id' )
							
							-> leftJoin( 'recipient_companies', 'recipients.id', '=', 'recipient_companies.recipient_id' )

							-> leftJoin( 'recipient_contacts', function( $join ) {
								
								$join -> on( 'recipient_contacts.recipient_id', '=', 'recipients.id' );
								
								$join -> on( 'recipient_contacts.is_preferred', '=', DB::Raw( 1 ) );
								
							} )
							
							-> leftJoin( 'library_remitter_relationships', 'recipients.remitter_relationship_id', '=', 'library_remitter_relationships.id' )
							
							-> leftJoin( 'recipient_other_relationships', 'recipient_other_relationships.recipient_id', '=', 'recipients.id' )

							-> leftJoin( 'remitters', 'recipients.remitter_id', '=', 'remitters.id' )
							
							-> select( DB::Raw( '

								DISTINCT

								crm_recipients.id AS recipient_id,
								
								crm_recipients.remitter_id AS remitter_id,
								
								IF( crm_recipients.type = "company", crm_recipient_companies.company_name, CONCAT_WS( " ", crm_recipient_persons.firstname, crm_recipient_persons.lastname ) ) AS recipient_full_name,

								crm_recipients.email AS recipient_email, 

								crm_recipient_contacts.contact AS recipient_contact, 

								IF( crm_library_remitter_relationships.id = 1, crm_recipient_other_relationships.relationship, crm_library_remitter_relationships.name ) AS recipient_relationship,

								CONCAT_WS( " ", crm_remitters.firstname, crm_remitters.lastname ) AS remitter_full_name

							' ) )
							
							-> whereRaw( $search_query )
							
							-> whereNull( 'recipients.deleted_at' )

							-> orderBy( 'recipients.id','DESC' )
							
							-> groupBy( 'recipients.id' )

							-> paginate( 20 )
							
							-> setPath( 'recipients' );
							
		}
		
		else {
		
			$recipients = DB::table( 'recipients' )
			
							-> leftJoin( 'recipient_persons', 'recipients.id', '=', 'recipient_persons.recipient_id' )
							
							-> leftJoin( 'recipient_companies', 'recipients.id', '=', 'recipient_companies.recipient_id' )

							-> leftJoin( 'recipient_contacts', function( $join ) {
								
								$join -> on( 'recipient_contacts.recipient_id', '=', 'recipients.id' );
								
								$join -> on( 'recipient_contacts.is_preferred', '=', DB::Raw( 1 ) );
								
							} )
							
							-> leftJoin( 'library_remitter_relationships', 'recipients.remitter_relationship_id', '=', 'library_remitter_relationships.id' )
							
							-> leftJoin( 'recipient_other_relationships', 'recipient_other_relationships.recipient_id', '=', 'recipients.id' )

							-> leftJoin( 'remitters', 'recipients.remitter_id', '=', 'remitters.id' )
							
							-> select( DB::Raw( '

								DISTINCT

								crm_recipients.id AS recipient_id,
								
								crm_recipients.remitter_id AS remitter_id,
								
								IF( crm_recipients.type = "company", crm_recipient_companies.company_name, CONCAT_WS( " ", crm_recipient_persons.firstname, crm_recipient_persons.lastname ) ) AS recipient_full_name,

								crm_recipients.email AS recipient_email, 

								crm_recipient_contacts.contact AS recipient_contact, 

								IF( crm_library_remitter_relationships.id = 1, crm_recipient_other_relationships.relationship, crm_library_remitter_relationships.name ) AS recipient_relationship,

								CONCAT_WS( " ", crm_remitters.firstname, crm_remitters.lastname ) AS remitter_full_name

							' ) )
							
							-> whereNull( 'recipients.deleted_at' )

							-> orderBy( 'recipients.id','DESC' )
							
							-> groupBy( 'recipients.id' )

							-> paginate( 20 )
							
							-> setPath( 'recipients' );
			
		}
		
		return $recipients;
		
	}

	private function searchParameter() {
		
		//CONSTRUCT QUERY STRING
		$query_string = array();
		
		//CHECK IF GET IS SET AND NOT EMPTY
		if( isset( $_GET ) && ! empty( $_GET ) ) {
			
			//RECIPIENT FULLNAME
			if( isset( $_GET[ 'search_recipient_full_name' ] ) && ! empty( $_GET[ 'search_recipient_full_name' ] ) ) {
				
				$query_string[] = ' IF( crm_recipients.type = "company", crm_recipient_companies.company_name, CONCAT_WS( " ", crm_recipient_persons.firstname, crm_recipient_persons.lastname ) ) LIKE "%'.$_GET[ "search_recipient_full_name" ].'%" ';
				
			}
			
			//RECIPIENT CONTACT
			if( isset( $_GET[ 'search_recipient_contact' ] ) && ! empty( $_GET[ 'search_recipient_contact' ] ) ) {
				
				$query_string[] = " crm_recipient_contacts.contact LIKE '%{$_GET[ 'search_recipient_contact' ]}%' ";
				
			}
			
			//RECIPIENT REMITTER
			if( isset( $_GET[ 'search_recipient_remitter' ] ) && ! empty( $_GET[ 'search_recipient_remitter' ] ) ) {
				
				$query_string[] = " crm_recipients.remitter_id = {$_GET[ 'search_recipient_remitter' ]} ";
				
			}
			
			//RETURN SEARCH QUERY
			return implode( ' AND ', array_filter( $query_string ) );

		}
		
	}
	
	
}
