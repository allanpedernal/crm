<?php

namespace App\Http\Controllers;


use Mail;

use Illuminate\Support\Str;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\File;

use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\Auth;


use App\Models\Remitter;

use App\Models\RemitterAddress;

use App\Models\RemitterContact;

use App\Models\RemitterIdentification;

use App\Models\RemitterNote;


use App\Models\Recipient;

use App\Models\RecipientPerson;

use App\Models\RecipientCompany;

use App\Models\RecipientOtherRelationship;

use App\Models\RecipientAddress;

use App\Models\RecipientReceiveOption;

use App\Models\RecipientReceiveOptionOtherBank;

use App\Models\RecipientBank;

use App\Models\RecipientOtherBank;

use App\Models\RecipientContact;

use App\Models\RecipientNote;


use App\Models\Remittance;

use App\Models\RemittanceAdditionalRequirement;

use App\Models\RemittanceNote;

use App\Models\RemittanceOtherNatureOfWork;

use App\Models\RemittanceOtherReason;

use App\Models\RemittanceOtherSourceOfFund;

use App\Models\RemittancePolipayment;

use App\Models\RemittancePromoCode;

use App\Models\RemittanceReceipt;


use App\Models\Library\LibraryReceiveOption;

use App\Models\Library\LibraryReceiveOptionList;

use App\Models\Library\LibraryBankSource;

use App\Models\Library\LibraryRemittanceStatus;

use App\Models\Library\LibraryReason;

use App\Models\Library\LibraryNatureOfWork;

use App\Models\Library\LibrarySourceOfFund;


class RemittancesController extends Controller {
	
	
	private $remittance_receipt_temp_path;

	private $remittance_receipt_path;
	
	
	private $remittance_addtional_requirement_temp_path;
	
	private $remittance_addtional_requirement_path;
	
	
    public function __construct() {
		
		
		//REMITTANCE RECEIPT FILE PATH
		$this -> remittance_receipt_temp_path = public_path( 'img/remittance_receipt/temp' );
		
		$this -> remittance_receipt_path = public_path( 'img/remittance_receipt' );
		
		
		//REMITTANCE ADDITIONAL REQUIREMENT FILE PATH
		$this -> remittance_addtional_requirement_temp_path = public_path( 'img/remittance_additional_requirement/temp' );
		
		$this -> remittance_addtional_requirement_path = public_path( 'img/remittance_additional_requirement' );
		
		
        $this -> middleware( 'auth' );
		
		
    }

    public function index() {
		
		
		$remittances = $this -> getRemittances();
		
		
		$remitter = array();
		
		//CHECK IF SEARCH RECIPIENT REMITTER IS ISSET AND NOT EMPTY
		if( isset( $_GET[ 'search_remittance_remitter' ] ) && ! empty( $_GET[ 'search_remittance_remitter' ] ) ) {
			
			$remitter = Remitter::selectRaw( 'id, email, CONCAT_WS( " ", firstname, lastname ) as fullname' )
			
								-> where( 'id', '=', $_GET[ 'search_remittance_remitter' ] )

								-> get()
								
								-> first()

								-> toArray();		

		}
		
		$recipient = array();
		
		//CHECK IF SEARCH RECIPIENT REMITTER IS ISSET AND NOT EMPTY
		if( isset( $_GET[ 'search_remittance_recipient' ] ) && ! empty( $_GET[ 'search_remittance_recipient' ] ) ) {

			//GET RECIPIENT
			$recipient = Recipient::selectRaw( '
			
								crm_recipients.id as id, 
								
								crm_recipients.email as email, 
								
								IF( 

									crm_recipients.type = "company", 

									crm_recipient_companies.company_name, 

									CONCAT_WS( " ", crm_recipient_persons.firstname, crm_recipient_persons.lastname )

								) AS fullname
								
							' )
			
							-> leftJoin( 'recipient_persons', 'recipients.id', '=', 'recipient_persons.recipient_id' )
							
							-> leftJoin( 'recipient_companies', 'recipients.id', '=', 'recipient_companies.recipient_id' )
			
							-> where( 'recipients.id', '=', $_GET[ 'search_remittance_recipient' ] )

							-> get()
							
							-> first()

							-> toArray();		

		}
		
		$receive_option_list = LibraryReceiveOptionList::orderBy( 'ordering', 'ASC' ) -> get();
		
		$remittance_statuses = LibraryRemittanceStatus::orderBy( 'ordering', 'ASC' ) -> get();
		
		$bank_sources = LibraryBankSource::orderBy( 'ordering', 'ASC' ) -> get();
		
		return view( 'remittances',
		
			array( 
			
				'remitter' => $remitter,
				
				'recipient' => $recipient,
				
				'remittances' => $remittances,
				
				'receive_option_list' => $receive_option_list,
				
				'remittance_statuses' => $remittance_statuses,
				
				'bank_sources' => $bank_sources
				
			) 
		
		);
        
    }

	public function showDetails( Request $request ) {
		
		if( $request -> id ) {

			return view( 'remittance-details' );
			
		}
		
	}
	
	public function getRemitters( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			$post = $request -> all();
			
			$remitter = $post[ 'data' ][ 'q' ];
			
			$search_results = Remitter::selectRaw( 'id, email, CONCAT_WS( " ", firstname, lastname ) as fullname' )
			
									-> having( 'fullname' , 'LIKE', '%'.$remitter.'%' )

									-> get() 
									
									-> toArray();

			$new_search_result = array();
			
			//LOOP ALL SEARCH RESULT
			foreach( $search_results as $key => $search_result ) {
				
				$new_search_result[ $key ][ 'id' ] = "{$search_result[ 'id' ]}";
				
				$new_search_result[ $key ][ 'text' ] = $search_result[ 'fullname' ] . ' | ' . $search_result[ 'email' ];
				
			}
			
			//SEND SOME RESPONSE
			return \Response::json( [ 'q' => $remitter, 'results' => $new_search_result ] );
							
		}
		
	}

	public function getRecipients( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			$post = $request -> all();
			
			$recipient = $post[ 'data' ][ 'q' ];
			
			$search_results = 
			
			Recipient::selectRaw( '
			
								crm_recipients.id as id, 
								
								crm_recipients.email as email, 
								
								IF( 

									crm_recipients.type = "company", 

									crm_recipient_companies.company_name, 

									CONCAT_WS( " ", crm_recipient_persons.firstname, crm_recipient_persons.lastname )

								) AS fullname
								
							' )
			
							-> leftJoin( 'recipient_persons', 'recipients.id', '=', 'recipient_persons.recipient_id' )

							-> leftJoin( 'recipient_companies', 'recipients.id', '=', 'recipient_companies.recipient_id' )

							-> having( 'fullname' , 'LIKE', '%'.$recipient.'%' )

							-> get() 

							-> toArray();

			$new_search_result = array();
			
			//LOOP ALL SEARCH RESULT
			foreach( $search_results as $key => $search_result ) {
				
				$new_search_result[ $key ][ 'id' ] = "{$search_result[ 'id' ]}";
				
				$new_search_result[ $key ][ 'text' ] = $search_result[ 'fullname' ] . ' | ' . $search_result[ 'email' ];
				
			}
			
			//SEND SOME RESPONSE
			return \Response::json( [ 'q' => $recipient, 'results' => $new_search_result ] );
							
		}
		
	}

	public function getRemitterRecipients( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			//GET REMITTER ID
			$remitter_id = $request -> remitter_id;
			
			//GET RECIPIENTS
			$recipients = Recipient::selectRaw( '
			
								crm_recipients.id as id, 
								
								crm_recipients.email as email, 
								
								IF( 

									crm_recipients.type = "company", 

									crm_recipient_companies.company_name, 

									CONCAT_WS( " ", crm_recipient_persons.firstname, crm_recipient_persons.lastname )

								) AS fullname
								
							' )
			
							-> leftJoin( 'recipient_persons', 'recipients.id', '=', 'recipient_persons.recipient_id' )
							
							-> leftJoin( 'recipient_companies', 'recipients.id', '=', 'recipient_companies.recipient_id' )
			
							-> where( 'remitter_id', '=', $remitter_id )
			
							-> get()
							
							-> toArray();
							
			//NEW REMITTER RECIPIENT		
			$new_remitter_recipient = array();
			
			//LOOP ALL SEARCH RESULT
			foreach( $recipients as $key => $recipient ) {
				
				$new_remitter_recipient[ $key ][ 'id' ] = "{$recipient[ 'id' ]}";
				
				$new_remitter_recipient[ $key ][ 'text' ] = $recipient[ 'fullname' ];
				
				//CHECK IF RECIPIENT EMAIL IS NOT EMPTY
				if( ! empty( strtolower( trim( $recipient[ 'email' ] ) ) ) ) {
					
					//CONCAT EMAIL ADDRESS
					$new_remitter_recipient[ $key ][ 'text' ] .= ' | ' . $recipient[ 'email' ];
					
				}
				
			}
			
			//SEND SOME RESPONSE
			return \Response::json( [ 'success' => true, 'result' => [ 'remitter_recipient' => $new_remitter_recipient ] ] );
							
		}
		
	}

	public function getReceiveOptionList( Request $request ) {

		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {

			//GET RECEIVE OPTION ID
			$receive_option_id = $request -> id;

			$receive_option_list = LibraryReceiveOptionList::where( 'receive_option_id', '=', $receive_option_id ) -> get() -> toArray();

			//SEND SOME RESPONSE
			return \Response::json( [ 'success' => true, 'result' => [ 'receive_option_list' => $receive_option_list ] ] );
			
		}
		
	}

	public function getModal( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			
			$forex_rate = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_default_xrate' ) -> value( 'option_value' );
			
			$trade_rate = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_default_traderrate' ) -> value( 'option_value' );
			
			
			
			$receive_options = LibraryReceiveOption::orderBy( 'ordering', 'ASC' ) -> get();
			
			$bank_sources = LibraryBankSource::orderBy( 'ordering', 'ASC' ) -> get();
			
			$remittance_statuses = LibraryRemittanceStatus::orderBy( 'ordering', 'ASC' ) -> get();
			
			$promo_codes = RemittancePromoCode::orderBy( 'id', 'ASC' ) -> get();
			
			$reasons = LibraryReason::orderBy( 'ordering', 'ASC' ) -> get();
			
			$nature_of_works = LibraryNatureOfWork::orderBy( 'ordering', 'ASC' ) -> get();
			
			$source_of_funds = LibrarySourceOfFund::orderBy( 'ordering', 'ASC' ) -> get();
			
			
			//EDIT FORM
			if( $request -> id != NULL || $request -> id != '' ) {


				$remittance_details = Remittance::where( 'id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();
				
				$remittance_details -> total_php_deposit = number_format( ( ( $remittance_details[ 'amount_sent' ] + $remittance_details[ 'service_fee' ] ) * $remittance_details[ 'forex_rate' ] ) , 2, '.', ',' );

				$remittance_remitter = Remitter::selectRaw( 'id, email, CONCAT_WS( " ", firstname, lastname ) as fullname' )

												-> where( 'id', '=', $remittance_details[ 'remitter_id' ] )

												-> get()

												-> first()

												-> toArray();

				$remittance_recipient = Recipient::selectRaw( '

										crm_recipients.id as id, 

										crm_recipients.email as email, 

										IF( 

											crm_recipients.type = "company", 

											crm_recipient_companies.company_name, 

											CONCAT_WS( " ", crm_recipient_persons.firstname, crm_recipient_persons.lastname )

										) AS fullname

									' )

									-> leftJoin( 'recipient_persons', 'recipients.id', '=', 'recipient_persons.recipient_id' )

									-> leftJoin( 'recipient_companies', 'recipients.id', '=', 'recipient_companies.recipient_id' )

									-> where( 'recipients.id', '=', $remittance_details[ 'recipient_id' ] )

									-> get()

									-> first()

									-> toArray();	

				$remittance_addtional_requirement = RemittanceAdditionalRequirement::where( 'remittance_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();

				$remittance_notes = RemittanceNote::where( 'remittance_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get() -> first();
					
				$remittance_other_nature_of_work = RemittanceOtherNatureOfWork::where( 'remittance_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get() -> first();
				
				$remittance_other_reason = RemittanceOtherReason::where( 'remittance_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get() -> first();

				$remittance_other_source_of_fund = RemittanceOtherSourceOfFund::where( 'remittance_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get() -> first();

				$remittance_polipayment = RemittancePolipayment::where( 'transaction_number', '=', $remittance_details[ 'transaction_number' ] ) -> orderBy( 'id', 'ASC' ) -> get() -> first();

				$remittance_promo_code = RemittancePromoCode::where( 'id', '=', $remittance_details[ 'promo_code_id' ] ) -> orderBy( 'id', 'ASC' ) -> get() -> first();

				$remittance_receipt = RemittanceReceipt::where( 'remittance_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get() -> first();


				//REMITTANCE DETAILS
				$histories[] = $remittance_details[ 'revisionHistory' ];
				
				//REMITTANCE ADDITIONAL REQUIREMENTS
				$histories[] = $remittance_addtional_requirement[ 'revisionHistory' ];

				//REMITTANCE NOTES
				$histories[] = $remittance_notes[ 'revisionHistory' ];
				
				//REMITTANCE OTHER NATURE OF WORK
				$histories[] = $remittance_other_nature_of_work[ 'revisionHistory' ];
				
				//REMITTANCE OTHER REASON
				$histories[] = $remittance_other_reason[ 'revisionHistory' ];
				
				//REMITTANCE OTHER SOURCE OF FUND
				$histories[] = $remittance_other_source_of_fund[ 'revisionHistory' ];
				
				//REMITTANCE RECEIPT
				$histories[] = $remittance_receipt[ 'revisionHistory' ];
				
				
				//DOUBLE RECOIL HISTORIES
				$new_histories = array();
				
				$x = 0;
				
				foreach( array_filter( $histories ) as $history ) {
					
					foreach( $history as $key => $hist ) {
						
						$new_histories[ $x ][ 'field_name' ] = $hist -> fieldName();
						
						$new_histories[ $x ][ 'old_value' ] = $hist -> oldValue();
						
						$new_histories[ $x ][ 'new_value' ] = $hist -> newValue();
						
						$new_histories[ $x ][ 'full_name' ] = $hist -> userResponsible() -> first_name . ' ' . $hist -> userResponsible() -> last_name;
						
						$x++;
						
					}
					
				}
				


				$content = view( 'modals.remittances' )

							-> with( [
								
								
								'action' => 'edit',
								
								'forex_rate' => $forex_rate,
								
								'trade_rate' => $trade_rate,
								
								
								'receive_options' => $receive_options,
								
								'bank_sources' => $bank_sources,
								
								'remittance_statuses' => $remittance_statuses,
								
								'promo_codes' => $promo_codes,
								
								'reasons' => $reasons,
								
								'nature_of_works' => $nature_of_works,
								
								'source_of_funds' => $source_of_funds,
								
								
								'remittance_id' => $remittance_details[ 'id' ],
								
								'remitter_id' => $remittance_details[ 'remitter_id' ],
								
								'recipient_id' => $remittance_details[ 'recipient_id' ],
								
								'remittance_details' => $remittance_details,
								
								'remittance_remitter' => $remittance_remitter,
								
								'remittance_recipient' => $remittance_recipient,
								
								'remittance_addtional_requirement' => $remittance_addtional_requirement,
								
								'remittance_notes' => $remittance_notes,
								
								'remittance_other_nature_of_work' => $remittance_other_nature_of_work,
								
								'remittance_other_reason' => $remittance_other_reason,
								
								'remittance_other_source_of_fund' => $remittance_other_source_of_fund,
								
								'remittance_polipayment' => $remittance_polipayment,
								
								'remittance_promo_code' => $remittance_promo_code,
								
								'remittance_receipt' => $remittance_receipt,
								
								
								'histories' => $new_histories
								
								
							] ) 

							-> render();
								
				return \Response::json( ['success' => true, 'result' => [ 'remittance_id' => $request -> id, 'content' => $content, 'action' => 'edit' ] ] );
				
			}
			
			//NEW FORM
			else {

				$content = view( 'modals.remittances' )

							-> with( [
								
								
								'action' => 'new',
								
								'forex_rate' => $forex_rate,
								
								'trade_rate' => $trade_rate,
								
								
								'receive_options' => $receive_options,
								
								'bank_sources' => $bank_sources,
								
								'remittance_statuses' => $remittance_statuses,
								
								'promo_codes' => $promo_codes,
								
								'reasons' => $reasons,
								
								'nature_of_works' => $nature_of_works,
								
								'source_of_funds' => $source_of_funds
								
								
							] ) 

							-> render();

				return \Response::json( ['success' => true, 'result' => [ 'remittance_id' => 0, 'content' => $content, 'action' => 'new' ] ] );
				
			}
			
		}
		
	}

	public function postForm( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {
				
				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					
					
					
					
					/* 
					*------------------------
					* REMITTANCE SAVING START
					*------------------------
					*/
					
					
					
					
					//CHECK IF REMITTANCE ID IS NOT EMPTY
					if( ! empty( $input[ 'remittance_id' ] ) ) {
						
						$new_remittance = Remittance::find( $input[ 'remittance_id' ] );
						
						$action = 'edit';
						
					}
					
					else {
						
						$new_remittance = new Remittance;

						//GENERATE TRANSACTION NUMBER FORMAT - FORMAT(REMITTER ID - TOTAL REMITTANCE COUNT - CURRENT YEAR)
						$new_remittance -> transaction_number = $input[ 'remittance_remitter_id' ] . '-' . Remittance::count() . '-' . date( 'y' );
						
						$action = 'add';

					}


					$new_remittance -> remitter_id = ( isset( $input[ 'remittance_remitter_id' ] ) && ! empty( $input[ 'remittance_remitter_id' ] ) ? $input[ 'remittance_remitter_id' ] : NULL );
					
					$new_remittance -> recipient_id = ( isset( $input[ 'remittance_recipient_id' ] ) && ! empty( $input[ 'remittance_recipient_id' ] ) ? $input[ 'remittance_recipient_id' ] : NULL );
					
					$new_remittance -> payment_type = ( isset( $input[ 'remittance_bank_source_id' ] ) && ! empty( $input[ 'remittance_bank_source_id' ] ) &&  $input[ 'remittance_bank_source_id' ] == 5 ?  'polipayment' : 'eft' );
					
					$new_remittance -> bank_source_id = ( isset( $input[ 'remittance_bank_source_id' ] ) && ! empty( $input[ 'remittance_bank_source_id' ] ) ? $input[ 'remittance_bank_source_id' ] : NULL );
					
					$new_remittance -> amount_sent = ( isset( $input[ 'remittance_amount_sent' ] ) && ! empty( $input[ 'remittance_amount_sent' ] ) ? $input[ 'remittance_amount_sent' ] : NULL );
					
					$new_remittance -> status_id = ( isset( $input[ 'remittance_status_id' ] ) && ! empty( $input[ 'remittance_status_id' ] ) ? $input[ 'remittance_status_id' ] : NULL );
					
					//CHECK IF REMITTANCE RECEIPT IS ISSET AND NOT EMPTY
					if( isset( $input[ 'remittance_receipt' ] ) && ! empty( $input[ 'remittance_receipt' ] ) ) {
						
						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {
							
							$remittance_receipt_details = RemittanceReciept::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first( [ 'filename' ] );
						
							$remittance_receipt = $remittance_receipt_details[ 'filename' ];
							
							//CHECK IF REMITTANCE RECEIPT IS NOT EMPTY AND NOT NULL
							if( ! empty( $remittance_receipt ) && ! is_null( $remittance_receipt ) ) {
							
								//CHECK IF REMITTANCE RECEIPT IS NOT EQUAL TO INPUT REMITTANCE RECEIPT
								if( trim( $remittance_receipt ) != trim( $input[ 'remittance_receipt' ] ) ) {

									//DELETE THE EXISTING REMITTANCE RECEIPT
									File::delete( $this -> remittance_receipt_path . '/' . $remittance_receipt );

									//MOVE THE NEW REMITTANCE RECEIPT
									File::move( $this -> remittance_receipt_temp_path . '/' . $input[ 'remittance_receipt' ] , $this -> remittance_receipt_path . '/' . $input[ 'remittance_receipt' ] );

								}
							
							}

							//REMITTANCE RECEIPT IS EMPTY
							else {
								
								//MOVE THE NEW REMITTANCE RECEIPT
								File::move( $this -> remittance_receipt_temp_path . '/' . $input[ 'remittance_receipt' ] , $this -> remittance_receipt_path . '/' . $input[ 'remittance_receipt' ] );

							}
						
						}
						
						//JUST MOVE REMITTANCE RECEIPT FROM TEMP FOLDER
						else {
							
							//MOVE REMITTANCE RECEIPT FROM TEMP
							File::move( $this -> remittance_receipt_temp_path . '/' . $input[ 'remittance_receipt' ] , $this -> remittance_receipt_path . '/' . $input[ 'remittance_receipt' ] );
							
						}
						
					}
					
					//REMOVE REMITTANCE RECEIPT
					else {
						
						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {

							$remittance_receipt_details = RemittanceReceipt::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first( [ 'filename' ] );
						
							$remittance_receipt = $remittance_receipt_details[ 'filename' ];
							
							//CHECK IF REMITTANCE RECEIPT IS NOT EMPTY
							if( ! empty( $remittance_receipt ) ) {

								//DELETE THE EXISTING REMITTANCE RECEIPT
								File::delete( $this -> remittance_receipt_path . '/' . $remittance_receipt );
								
							}
						
						}
						
					}
					
					$new_remittance -> has_receipt = ( isset( $input[ 'remittance_receipt' ] ) && ! empty( $input[ 'remittance_receipt' ] ) ? TRUE : FALSE );
					
					$new_remittance -> service_fee = ( isset( $input[ 'remittance_service_fee' ] ) && ! empty( $input[ 'remittance_service_fee' ] ) ? $input[ 'remittance_service_fee' ] : NULL );
					
					$new_remittance -> forex_rate = ( isset( $input[ 'remittance_forex_rate' ] ) && ! empty( $input[ 'remittance_forex_rate' ] ) ? $input[ 'remittance_forex_rate' ] : NULL );
					
					$new_remittance -> trade_rate = ( isset( $input[ 'remittance_trade_rate' ] ) && ! empty( $input[ 'remittance_trade_rate' ] ) ? $input[ 'remittance_trade_rate' ] : NULL );
					
					$new_remittance -> reason_id = ( isset( $input[ 'remittance_reason_id' ] ) && ! empty( $input[ 'remittance_reason_id' ] ) ? $input[ 'remittance_reason_id' ] : NULL );
					
					$new_remittance -> nature_of_work_id = ( isset( $input[ 'remittance_nature_of_work_id' ] ) && ! empty( $input[ 'remittance_nature_of_work_id' ] ) ? $input[ 'remittance_nature_of_work_id' ] : NULL );
					
					$new_remittance -> source_of_fund_id = ( isset( $input[ 'remittance_source_of_fund_id' ] ) && ! empty( $input[ 'remittance_source_of_fund_id' ] ) ? $input[ 'remittance_source_of_fund_id' ] : NULL );
					
					$new_remittance -> promo_code_id = ( isset( $input[ 'remittance_promo_code_id' ] ) && ! empty( $input[ 'remittance_promo_code_id' ] ) ? $input[ 'remittance_promo_code_id' ] : NULL );
					
					//CHECK IF REMITTANCE STATUS IS ISSET AND NOT EMPTY AND EQUAL TO TRANSFER COMPLETE
					if( isset( $input[ 'remittance_status_id' ] ) && ! empty( $input[ 'remittance_status_id' ] ) && $input[ 'remittance_status_id' ] == 24 ) {
						
						//GET REMITTANCE STATUS ID
						$remittance_status_id = Remittance::find( $input['remittance_id' ] );
						
						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( 
							
							//REMITTANCE ID IS EMPTY
							( empty( $input[ 'remittance_id' ] ) )
							
							||
							
							//STATUS ID IS NOT TRANSFER COMPLETE
							( isset( $remittance_status_id[ 'status_id' ] ) && ! empty( $remittance_status_id[ 'status_id' ] ) && $remittance_status_id[ 'status_id' ] != 24 )
							
						) {
						
							//ADD DATE TRANSFER DATE
							$new_remittance -> date_transferred = date( 'Y-m-d H:i:s' );
						
						}
					
					}
					
					$new_remittance -> save();

					$remittance_id = $new_remittance[ 'id' ];
					
					
					
					
					//CHECK IF REMITTANCE RECEIPT IS ISSET AND NOT EMPTY
					if( isset( $input[ 'remittance_receipt' ] ) && ! empty( $input[ 'remittance_receipt' ] ) ) {

						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {
							
							$new_remittance_receipt = RemittanceReceipt::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first();
							
							//CHECK IF NEW REMITTANCE RECEIPT IS EMPTY
							if( empty( $new_remittance_receipt ) ) {
								
								$new_remittance_receipt = new RemittanceReceipt;
								
							}
							
						}
						
						else {
							
							$new_remittance_receipt = new RemittanceReceipt;

						}
						
						$new_remittance_receipt -> remittance_id = $remittance_id;
						
						$new_remittance_receipt -> filename = ( isset( $input[ 'remittance_receipt' ] ) && ! empty( $input[ 'remittance_receipt' ] ) ? $input[ 'remittance_receipt' ] : NULL );
						
						$new_remittance_receipt -> save();
						
						$remittance_receipt_id = $new_remittance_receipt[ 'id' ];
						
					}
					
					//REMOVE REMITTANCE RECEIPT
					else {

						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {

							$remove_remittance_receipt = RemittanceReceipt::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> delete();

						}

					}
					
					
					
					
					//CHECK IF REMITTANCE REASON ID IS ISSET AND NOT EMPTY AND EQUAL TO OTHER
					if( isset( $input[ 'remittance_reason_id' ] ) && ! empty( $input[ 'remittance_reason_id' ] ) && $input[ 'remittance_reason_id' ] == 1 ) {

						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {
							
							$new_remittance_other_reason = RemittanceOtherReason::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first();
							
							//CHECK IF NEW REMITTANCE OTHER REASON IS EMPTY
							if( empty( $new_remittance_other_reason ) ) {
								
								$new_remittance_other_reason = new RemittanceOtherReason;
								
							}
							
						}
						
						else {
							
							$new_remittance_other_reason = new RemittanceOtherReason;

						}
						
						$new_remittance_other_reason -> remittance_id = $remittance_id;
						
						$new_remittance_other_reason -> reason = ( isset( $input[ 'remittance_other_reason' ] ) && ! empty( $input[ 'remittance_other_reason' ] ) ? $input[ 'remittance_other_reason' ] : NULL );
						
						$new_remittance_other_reason -> save();
						
						$remittance_other_reason_id = $new_remittance_other_reason[ 'id' ];
						
					}
					
					//REMOVE REMITTANCE OTHER REASON
					else {

						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {

							$remove_remittance_other_reason = RemittanceOtherReason::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> delete();

						}

					}
					
					
					
					
					//CHECK IF REMITTANCE NATURE OF WORK ID IS ISSET AND NOT EMPTY AND EQUAL TO OTHER
					if( isset( $input[ 'remittance_nature_of_work_id' ] ) && ! empty( $input[ 'remittance_nature_of_work_id' ] ) && $input[ 'remittance_nature_of_work_id' ] == 1 ) {
					
						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {
							
							$new_remittance_other_nature_of_work = RemittanceOtherNatureOfWork::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first();
							
							//CHECK IF NEW REMITTANCE OTHER NATURE OF WORK IS EMPTY
							if( empty( $new_remittance_other_nature_of_work ) ) {
								
								$new_remittance_other_nature_of_work = new RemittanceOtherNatureOfWork;
								
							}
							
						}
						
						else {
							
							$new_remittance_other_nature_of_work = new RemittanceOtherNatureOfWork;

						}
						
						$new_remittance_other_nature_of_work -> remittance_id = $remittance_id;
						
						$new_remittance_other_nature_of_work ->	nature_of_work = ( isset( $input[ 'remittance_other_nature_of_work' ] ) && ! empty( $input[ 'remittance_other_nature_of_work' ] ) ? $input[ 'remittance_other_nature_of_work' ] : NULL );
					
						$new_remittance_other_nature_of_work -> save();
						
						$remittance_other_nature_of_work_id = $new_remittance_other_nature_of_work[ 'id' ];
					
					}
					
					//REMOVE REMITTANCE OTHER NATURE OF WORK
					else {

						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {

							$remove_remittance_other_nature_of_work = RemittanceOtherNatureOfWork::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> delete();

						}

					}
					
					
					
					
					//CHECK IF REMITTANCE SOURCE OF FUND ID IS ISSET AND NOT EMPTY AND EQUAL TO OTHER
					if( isset( $input[ 'remittance_source_of_fund_id' ] ) && ! empty( $input[ 'remittance_source_of_fund_id' ] ) && $input[ 'remittance_source_of_fund_id' ] == 1 ) {
					
						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {
							
							$new_remittance_other_source_of_fund = RemittanceOtherSourceOfFund::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first();
							
							//CHECK IF NEW REMITTANCE OTHER SOURCE OF FUND IS EMPTY
							if( empty( $new_remittance_other_source_of_fund ) ) {
								
								$new_remittance_other_source_of_fund = new RemittanceOtherSourceOfFund;
								
							}
							
						}
						
						else {
							
							$new_remittance_other_source_of_fund = new RemittanceOtherSourceOfFund;

						}
						
						$new_remittance_other_nature_of_work -> remittance_id = $remittance_id;
						
						$new_remittance_other_nature_of_work ->	source_of_fund = ( isset( $input[ 'remittance_other_source_of_fund' ] ) && ! empty( $input[ 'remittance_other_source_of_fund' ] ) ? $input[ 'remittance_other_source_of_fund' ] : NULL );
					
						$new_remittance_other_nature_of_work -> save();
						
						$remittance_other_nature_of_work_id = $new_remittance_other_nature_of_work[ 'id' ];
					
					}
					
					//REMOVE REMITTANCE OTHER SOURCE OF FUND
					else {

						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {

							$remove_remittance_other_source_of_fund = RemittanceOtherSourceOfFund::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> delete();

						}

					}
					
					
					
					
					/* 
					*----------------------
					* REMITTANCE SAVING END
					*----------------------
					*/
					
					
					
					
					/* 
					*----------------------------------------
					* REMITTANCE ADDITIONAL REQUIREMENT START
					*----------------------------------------
					*/
					
					
					
					
					//CHECK IF AMOUNT SENT IS ISSET AND NOT EMPTY AND GREATER THAN OR EQUAL TO AMOUNT SENT
					if( 

						( isset( $input[ 'amount_sent' ] ) && ! empty( $input[ 'amount_sent' ] ) )

						&&

						( $input[ 'amount_sent' ] >= 10000 ) 

						
					) {
					
					
					
					
						//CHECK IF REMITTANCE ADDITIONAL REQUIREMENT IS ISSET AND NOT EMPTY
						if( isset( $input[ 'remittance_additional_requirement' ] ) && ! empty( $input[ 'remittance_additional_requirement' ] ) ) {
							
							//CHECK IF REMITTANCE ID IS NOT EMPTY
							if( ! empty( $input[ 'remittance_id' ] ) ) {
								
								$remittance_additional_requirement_details = RemittanceAdditionalRequirement::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first( [ 'filename' ] );
							
								$remittance_additional_requirement_filename = $remittance_additional_requirement_details[ 'filename' ];
								
								//CHECK IF REMITTANCE ADDITIONAL REQUIREMENT FILENAME IS NOT EMPTY AND NOT NULL
								if( ! empty( $remittance_additional_requirement_filename ) && ! is_null( $remittance_additional_requirement_filename ) ) {
								
									//CHECK IF REMITTANCE ADDITIONAL REQUIREMENT FILENAME IS NOT EQUAL TO INPUT REMITTANCE RECEIPT
									if( trim( $remittance_additional_requirement_filename ) != trim( $input[ 'remittance_additional_requirement' ] ) ) {

										//DELETE THE EXISTING REMITTANCE ADDITIONAL REQUIREMENT FILENAME
										File::delete( $this -> remittance_addtional_requirement_path . '/' . $remittance_additional_requirement_filename );

										//MOVE THE NEW REMITTANCE ADDITIONAL REQUIREMENT FILENAME
										File::move( $this -> remittance_addtional_requirement_temp_path . '/' . $input[ 'remittance_additional_requirement' ] , $this -> remittance_addtional_requirement_path . '/' . $input[ 'remittance_additional_requirement' ] );

									}
								
								}

								//REMITTANCE RECEIPT IS EMPTY
								else {
									
									//MOVE THE NEW REMITTANCE RECEIPT
									File::move( $this -> remittance_addtional_requirement_temp_path . '/' . $input[ 'remittance_additional_requirement' ] , $this -> remittance_addtional_requirement_path . '/' . $input[ 'remittance_additional_requirement' ] );

								}
							
							}
							
							//JUST MOVE REMITTANCE RECEIPT FROM TEMP FOLDER
							else {
								
								//MOVE REMITTANCE RECEIPT FROM TEMP
								File::move( $this -> remittance_addtional_requirement_temp_path . '/' . $input[ 'remittance_additional_requirement' ] , $this -> remittance_addtional_requirement_path . '/' . $input[ 'remittance_additional_requirement' ] );
								
							}
							
						}
						
						//REMOVE REMITTANCE ADDITIONAL REQUIREMENT
						else {
							
							//CHECK IF REMITTANCE ID IS NOT EMPTY
							if( ! empty( $input[ 'remittance_id' ] ) ) {

								$remittance_additional_requirement_details = RemittanceAdditionalRequirement::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first( [ 'filename' ] );
							
								$remittance_additional_requirement_filename = $remittance_additional_requirement_details[ 'filename' ];
								
								//CHECK IF REMITTANCE ADDITIONAL REQUIREMENT FILENAME IS NOT EMPTY
								if( ! empty( $remittance_additional_requirement_filename ) ) {

									//DELETE THE EXISTING REMITTANCE ADDITIONAL REQUIREMENT
									File::delete( $this -> remittance_addtional_requirement_path . '/' . $remittance_additional_requirement_filename );
									
								}
							
							}
							
						}
					
					
					
					
						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {
							
							$new_remittance_additional_requirement = RemittanceAdditionalRequirement::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first();
							
							//CHECK IF NEW REMITTANCE OTHER SOURCE OF FUND IS EMPTY
							if( empty( $new_remittance_additional_requirement ) ) {
								
								$new_remittance_additional_requirement = new RemittanceAdditionalRequirement;
								
							}
							
						}
						
						else {
							
							$new_remittance_additional_requirement = new RemittanceAdditionalRequirement;

						}

						$new_remittance_additional_requirement -> source_of_income = ( isset( $input[ 'remittance_source_of_income' ] ) && ! empty( $input[ 'remittance_source_of_income' ] ) ? $input[ 'remittance_source_of_income' ] : NULL );
						
						$new_remittance_additional_requirement -> filename = ( isset( $input[ 'remittance_additional_requirement' ] ) && ! empty( $input[ 'remittance_additional_requirement' ] ) ? $input[ 'remittance_additional_requirement' ] : NULL );
						
						$new_remittance_additional_requirement -> save();
						
						$remittance_additional_requirement_id = $new_remittance_additional_requirement[ 'id' ];
					
					
					
					
					}


						
						
					/* 
					*---------------------------------------
					* REMITTANCE ADDITIONAL REQUIREMENT END
					*---------------------------------------
					*/
					
					
					
					
					
					//ADD SOME NOTE COMING FROM CURRENT LOGGED IN USER
					if( isset( $input[ 'remittance_added_note' ] ) && ! empty( $input[ 'remittance_added_note' ] ) ) {
						
						//CHECK IF REMITTANCE ID IS NOT EMPTY
						if( ! empty( $input[ 'remittance_id' ] ) ) {
							
							$new_remittance_note = RemittanceNote::where( 'remittance_id', '=', $input[ 'remittance_id' ] ) -> first();
							
							//CHECK IF NEW REMITTANCE NOTE IS EMPTY
							if( empty( $new_remittance_note ) ) {
								
								$new_remittance_note = new RemittanceNote;
								
							}
							
						}
						
						else {
							
							$new_remittance_note = new RemittanceNote;

						}
						
						$new_remittance_note -> remittance_id = $remittance_id;
						
						$new_remittance_note -> note = ( isset( $input[ 'remittance_added_note' ] ) && ! empty( $input[ 'remittance_added_note' ] ) ? trim( $input[ 'remittance_added_note' ] ) : NULL );
						
						$new_remittance_note -> created_by = Auth::user()->ID;
						
						$new_remittance_note -> save();
						
						$remittance_note_id = $new_remittance_note[ 'id' ];
						
					}
					
					
					
					
					//COMMIT ALL TRANSACTION
					DB::commit();


						
						
					/* 
					*--------------------------
					* REMITTANCE SETTINGS START
					*---------------------------
					*/

						//REMITTANCE ADVICE CLIENT AND SUPPORT
						if( isset( $input[ 'remittance_advice_client_and_support' ] ) && ! empty( $input[ 'remittance_advice_client_and_support' ] ) ) {
							
						}
						
						//REMITTANCE UPDATED RATE
						if( isset( $input[ 'remittance_updated_rate' ] ) && ! empty( $input[ 'remittance_updated_rate' ] ) ) {
							
						}

						//REMITTANCE POLI LINK
						if( isset( $input[ 'remittance_poli_link' ] ) && ! empty( $input[ 'remittance_poli_link' ] ) ) {
							
							$new_remittance_details = array();
							
							$new_remittance_details[ 'remittance_id' ] = $remittance_id;
							
							$new_remittance_details[ 'service_fee' ] = $input[ 'remittance_service_fee' ];

							$response = $this -> generatePoliLink( $new_remittance_details );

							//CHECK IF SUCCESS
							if( $response[ 'success' ] ) {
								
								//GET REMITTER
								$remitter = Remitter::find( $input[ 'remitter_id' ] );
								
								//SEND POLI LINK TO CLIENT
								Mail::send( 'emails.template.poli_link', [ 'remitter' => $remitter ], function ( $m ) use ( $remitter ) {
									
									$m -> from( 'support@iremit.com.au', 'iRemit to the Philippines' );

									$m -> to( $remitter -> email, $remitter -> firstname . ' ' . $remitter -> middlename . ' ' . $remitter -> lastname ) -> subject( 'iRemit::Polipayment Payment Link' );

								} );

							}
							
							else {
								
								//SHOW SOME ERROR
								return \Response::json( ['success' => false, 'error_message' => $generate_poli_link_response[ 'error_code' ] ] );
								
							}
							
						}
						
						//REMITTANCE STATUS AUTORESPONDER
						if( isset( $input[ 'remittance_status_autoresponder' ] ) && ! empty( $input[ 'remittance_status_autoresponder' ] ) ) {
							
							$response = file_get_contents( url( 'autoresponder/remittance-id/' . $remittance_id ) );
							
						}

						//REMITTANCE STATUS PUSH NOTIFICATION
						if( isset( $input[ 'remittance_status_push_notification' ] ) && ! empty( $input[ 'remittance_status_push_notification' ] ) ) {
							
							$response = file_get_contents( url( 'notifications/sendpushnotification/' . $remittance_id ) );
							
						}

					/* 
					*--------------------------
					* REMITTANCE SETTINGS END
					*---------------------------
					*/
					
					
					
					
					
					$remittance_table_template = view( 'tables.remittance_table',
					
						array( 'remittances' => $this -> getRemittances() )
						
					)
					
					-> render();




					//SEND SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'remittance_id' => $remittance_id, 'remittance_table_template' => $remittance_table_template, 'action' => $action ) ] );




				}
				
				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
			
				
			}
			
		}
		
	}
	
	public function poliNudge( Request $request ) {
			
			
			
		//CHECK IF REQUEST METHOD IS POST
		if( $request -> isMethod( 'post' ) ) {
			
			
			
			$input = $request -> all();
			
			
			
			$ch = curl_init( 'https://poliapi.apac.paywithpoli.com/api/Transaction/GetTransaction?token='. urlencode( $input[ 'Token' ] ) );

			curl_setopt( $ch, CURLOPT_CAINFO, storage_path( 'app/crt/ca-bundle.crt' ) );

			curl_setopt( $ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2 );

			curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Authorization: Basic ' . base64_encode( 'S6101170:tJELmlUGCjulRrK' ) ) );

			curl_setopt( $ch, CURLOPT_HEADER, 0 );

			curl_setopt( $ch, CURLOPT_POST, 0 );

			curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 0 );

			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

			$result = curl_exec( $ch );

			curl_close ( $ch );

			$response = json_decode( $result, true );
			
			
			
			$poli_ref = $response[ 'TransactionRefNo' ];

			$poli_status = trim( strtolower( $response[ 'TransactionStatusCode' ] ) );
			
			$transaction_number = $response[ 'MerchantReference' ];
			
			
			
			//GET REMITTANCE
			$new_remittance = Remittance::where( 'transaction_number', '=', $transaction_number ) -> first();
			
			
			
			//CHECK POLI STATUS IF COMPLETE
			if( $poli_status == 'completed' ) {

				$new_remittance -> status_id = 25; //POLi Successful

			}

			//CHECK POLI STATUS IF CANCELLED
			if( $poli_status == 'cancelled' ) {
				
				$new_remittance -> status_id = 26; //POLi Cancelled
				
			}
			
			$new_remittance -> poli_ref = $poli_ref;

			$new_remittance -> save();

			$remittance_id = $new_remittance[ 'id' ];
			
			//SEND AUTORESPONDER
			if( $poli_status == 'completed' ) {
				
				//SEND SOME AUTORESPONDER
				$response = file_get_contents( url( 'autoresponder/remittance-id/' . $remittance_id ) );
				
			}
			
			
			
			//CREATE NEW REMITTANCE POLIPAYMENT
			$new_remittance_polipayment = new RemittancePolipayment;
			
			$new_remittance_polipayment -> transaction_number = $transaction_number;
			
			$new_remittance_polipayment -> token = $input[ 'Token' ];
			
			$new_remittance_polipayment -> reference_number = $poli_ref;
			
			$new_remittance_polipayment -> status = $poli_status;
			
			$new_remittance_polipayment -> content = serialize( $response );
			
			$new_remittance_polipayment -> save();
			
			$remittance_polipayment_id = $new_remittance_polipayment[ 'id' ];
			
			
			
		}
		
	}
	
	public function bulkAction( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {

				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					
					//GET ACTION
					$action = $input[ 'action' ];
					
					//GET CONTENT
					$content = $input[ 'content' ];
					
					
					//CHECK IF REMITTANCE IS ISSET AND NOT EMPTY
					if( isset( $input[ 'remittance' ] ) && ! empty( $input[ 'remittance' ] ) && count( $input[ 'remittance' ] ) ) {
						
						//LOOP ALL REMITTANCE
						foreach( $input[ 'remittance' ] AS $key => $remittance ) {
							
							//SWITCH ACTION
							switch( $action ) {
								
								//CASE NOTE
								case 'note':
								
									$new_remittance_note = RemittanceNote::where( 'remittance_id', '=', $remittance ) -> first();
									
									//CHECK IF REMITTANCE NOTE IS EMPTY
									if( empty( $new_remittance_note ) ) {
										
										$new_remittance_note = new RemittanceNote;
										
									}
									
									$new_remittance_note -> note = $content;
									
									$new_remittance_note -> created_by = Auth::user()->ID;;
									
									$new_remittance_note -> save();
									
									$remittance_note_id = $new_remittance_note[ 'id' ];

								break;
								
								//CASE FOREX RATE
								case 'forex_rate':
								
									$new_remittance = Remittance::find( $remittance );
									
									//CHECK IF REMITTANCE IS EMPTY
									if( empty( $new_remittance ) ) {
										
										$new_remittance = new Remittance;
										
									}

									$new_remittance -> forex_rate = $content;

									$new_remittance -> save();

									$remittance_id = $new_remittance[ 'id' ];

								break;
								
								//CASE TRADE RATE
								case 'trade_rate':

									$new_remittance = Remittance::find( $remittance );

									//CHECK IF REMITTANCE IS EMPTY
									if( empty( $new_remittance ) ) {
										
										$new_remittance = new Remittance;
										
									}

									$new_remittance -> trade_rate = $content;

									$new_remittance -> save();

									$remittance_id = $new_remittance[ 'id' ];

								break;
								
								//CASE DEFAULT
								default:

									continue;

							}
							
						}
						
					}
					
					
					//COMMIT ALL TRANSACTION
					DB::commit();
					
					
					//GET ALL REMITTANCE TABLES
					$remittance_table_template = view( 'tables.remittance_table',
					
						array( 'remittances' => $this -> getRemittances() )
						
					)
					
					-> render();
					
					//SEND SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'total_remittance' => count( $input[ 'remittance' ] ), 'remittance_table_template' => $remittance_table_template, 'action' => $action ) ] );
					
					
				}
				
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
				
			}
			
		}
		
	}
	
	public function deleteRemittances( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {

				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					
					
					//CHECK IF REMITTANCE IS ISSET AND NOT EMPTY
					if( isset( $input[ 'remittance' ] ) && ! empty( $input[ 'remittance' ] ) && count( $input[ 'remittance' ] ) ) {
						
						//LOOP ALL REMITTANCE
						foreach( $input[ 'remittance' ] AS $key => $remittance ) {
							
							//REMOVE REMITTANCE
							$remove_remittance = Remittance::find( $remittance ) -> delete();
							
						}
						
					}
					
					
					//COMMIT ALL TRANSACTION
					DB::commit();
					
					
					//GET ALL REMITTANCE TABLES
					$remittance_table_template = view( 'tables.remittance_table',
					
						array( 'remittances' => $this -> getRemittances() )
						
					)
					
					-> render();
					
					//SEND SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'total_remittance' => count( $input[ 'remittance' ] ), 'remittance_table_template' => $remittance_table_template, 'action' => 'delete' ) ] );
					
					
				}

				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
				
			}

		}
		
	}
	
	
	public function getRemitterFullDetails( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			$remitter_id = $request -> remitter_id;
			
			$remitter_details = Remitter::where( 'id', '=', $remitter_id ) -> first();
			
			$remitter_address = RemitterAddress::where( [ [ 'remitter_id', '=', $remitter_id ], [ 'is_preferred', '=', 1 ] ] ) -> first();
			
			$remitter_contact = RemitterContact::where( [ [ 'remitter_id', '=', $remitter_id ], [ 'is_preferred', '=', 1 ] ] ) -> first();
			
			$remitter_identification = RemitterIdentification::where( [ [ 'remitter_id', '=', $remitter_id ], [ 'is_preferred', '=', 1 ] ] ) -> first();

			$remitter_note = RemitterNote::where( 'remitter_id', '=', $remitter_id ) -> first();
			
			$content = view( 'modals.content.remitter_full_details' )

						-> with( [
						
							'remitter_id' => $remitter_id,

							'remitter_details' => $remitter_details,

							'remitter_address' => $remitter_address,

							'remitter_contact' => $remitter_contact,

							'remitter_identification' => $remitter_identification,

							'remitter_note' => $remitter_note

						] ) 

						-> render();
						
			return \Response::json( ['success' => true, 'result' => [ 'remitter_id' => $remitter_id, 'content' => $content ] ] );
			
		}
		
	}

	public function getRecipientFullDetails( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			$recipient_id = $request -> recipient_id;
			
			$recipient_details = Recipient::find( $recipient_id );
			
			//RECIPIENT DETAIL IS PERSON
			if( $recipient_details -> type == 'person' ) {
				
				$recipient_person_details = RecipientPerson::where( 'recipient_id', '=', $recipient_id ) -> first();
				
			}
			
			//RECIPIENT DETAIL IS COMPANY
			else {
				
				$recipient_company_details = RecipientCompany::where( 'recipient_id', '=', $recipient_id ) -> first();
				
			}
			
			$recipient_other_relationship = RecipientOtherRelationship::where( 'recipient_id', '=', $recipient_id ) -> first();

			$recipient_address = RecipientAddress::where( [ [ 'recipient_id', '=', $recipient_id ], [ 'is_preferred', '=', 1 ] ] ) -> first();

			$recipient_receive_option = RecipientReceiveOption::where( [ [ 'recipient_id', '=', $recipient_id ], [ 'is_preferred', '=', 1 ] ] ) -> first();

			$recipient_receive_option_other_bank = RecipientReceiveOptionOtherBank::where( 'recipient_id', '=', $recipient_id ) -> first();

			$recipient_bank = RecipientBank::where( [ [ 'recipient_id', '=', $recipient_id ], [ 'is_preferred', '=', 1 ] ] ) -> first();

			$recipient_other_bank = RecipientOtherBank::where( 'recipient_bank_id', '=', $recipient_bank[ 'id' ] ) -> first();

			$recipient_contact = RecipientContact::where( [ [ 'recipient_id', '=', $recipient_id ], [ 'is_preferred', '=', 1 ] ] ) -> first();

			$recipient_note = RecipientNote::where( 'recipient_id', '=', $recipient_id ) -> first();
			
			
			//RECIPIENT FULL DETAILS DATA
			$recipient_full_details_data = array(
				
				'recipient_id' => $recipient_id,

				'recipient_details' => $recipient_details,

				'recipient_other_relationship' => $recipient_other_relationship,

				'recipient_address' => $recipient_address,

				'recipient_receive_option' => $recipient_receive_option,

				'recipient_receive_option_other_bank' => $recipient_receive_option_other_bank,

				'recipient_bank' => $recipient_bank,

				'recipient_other_bank' => $recipient_other_bank,

				'recipient_contact' => $recipient_contact,

				'recipient_note' => $recipient_note

			);
			
			
			//RECIPIENT DETAIL IS PERSON
			if( $recipient_details -> type == 'person' ) {
				
				$recipient_full_details_data = array_merge( $recipient_full_details_data, array( 'recipient_person_details' => $recipient_person_details ) );

			}
			
			//RECIPIENT DETAIL IS COMPANY
			else {

				$recipient_full_details_data = array_merge( $recipient_full_details_data, array( 'recipient_company_details' => $recipient_company_details ) );
				
			}
			

			$content = view( 'modals.content.recipient_full_details' ) -> with( $recipient_full_details_data ) -> render();
						
			return \Response::json( ['success' => true, 'result' => [ 'recipient_id' => $recipient_id, 'content' => $content ] ] );
			
		}
		
	}
	
	
	private function getRemittances() {
		
		//REMOVE PAGE FROM SEARCH QUERY
		UNSET( $_GET[ 'page' ] );
		
		//SEARCH QUERY IS ISSET AND NOT EMPTY
		if( isset( $_GET ) && ! empty( array_filter( $_GET ) ) ) {
			
			$search_query = $this -> searchParameter();
			
			$remittance = DB::table( 'remittances' )
			
			
					-> leftJoin( 'remitters', 'remitters.id', '=', 'remittances.remitter_id' )
					
					
					-> leftJoin( 'recipients', 'recipients.id', '=', 'remittances.recipient_id' )
					
					-> leftJoin( 'library_receive_option_lists', 'library_receive_option_lists.id', '=', 'remittances.receive_option_list_id' )
					
					
					-> leftJoin( 'library_bank_sources', 'library_bank_sources.id', '=', 'remittances.bank_source_id' )
					
					-> leftJoin( 'library_remittance_statuses', 'library_remittance_statuses.id', '=', 'remittances.status_id' )
					
					-> leftJoin( 'library_reasons', 'library_reasons.id', '=', 'remittances.reason_id' )
					
					-> leftJoin( 'remittance_promo_codes', 'remittance_promo_codes.id', '=', 'remittances.promo_code_id' )


					-> select( DB::Raw( '
					
						crm_remittances.id AS remittance_id,

						crm_remittances.transaction_number AS transaction_no,

						crm_remittances.created_at AS date,

						crm_library_receive_option_lists.name AS receive_method,

						crm_library_bank_sources.shortname AS bank_source,
						
						COALESCE( crm_remittances.service_fee, 0 ) AS service_fee,

						COALESCE( crm_remittances.amount_sent, 0 ) AS aud,
						
						COALESCE( crm_remittances.forex_rate, 0 ) AS forex_rate,
						
						( 
						
							( 
							
								COALESCE( crm_remittances.amount_sent, 0 ) 
							
								+
								
								COALESCE( crm_remittances.service_fee, 0 ) 
								
							)
							
							* COALESCE( crm_remittances.forex_rate, 0 ) 
						
						) AS php,

						crm_library_remittance_statuses.name AS status

					' ) )
					
					-> whereRaw( $search_query )
					
					-> whereNull( 'remittances.deleted_at' )
					
					-> orderBy( 'remittances.id','DESC' )
					
					-> paginate( 20 )

					-> setPath( 'remittances' );
			
		}
		
		else {
		
			$remittance = DB::table( 'remittances' )
			
			
					-> leftJoin( 'remitters', 'remitters.id', '=', 'remittances.remitter_id' )
					
					
					-> leftJoin( 'recipients', 'recipients.id', '=', 'remittances.recipient_id' )
					
					-> leftJoin( 'library_receive_option_lists', 'library_receive_option_lists.id', '=', 'remittances.receive_option_list_id' )
					
					
					-> leftJoin( 'library_bank_sources', 'library_bank_sources.id', '=', 'remittances.bank_source_id' )
					
					-> leftJoin( 'library_remittance_statuses', 'library_remittance_statuses.id', '=', 'remittances.status_id' )
					
					-> leftJoin( 'library_reasons', 'library_reasons.id', '=', 'remittances.reason_id' )
					
					-> leftJoin( 'remittance_promo_codes', 'remittance_promo_codes.id', '=', 'remittances.promo_code_id' )


					-> select( DB::Raw( '
					
						crm_remittances.id AS remittance_id,

						crm_remittances.transaction_number AS transaction_no,

						crm_remittances.created_at AS date,

						crm_library_receive_option_lists.name AS receive_method,

						crm_library_bank_sources.shortname AS bank_source,
						
						COALESCE( crm_remittances.service_fee, 0 ) AS service_fee,

						COALESCE( crm_remittances.amount_sent, 0 ) AS aud,
						
						COALESCE( crm_remittances.forex_rate, 0 ) AS forex_rate,
						
						( 
						
							( 
							
								COALESCE( crm_remittances.amount_sent, 0 ) 
							
								+
								
								COALESCE( crm_remittances.service_fee, 0 ) 
								
							)
							
							* COALESCE( crm_remittances.forex_rate, 0 ) 
						
						) AS php,

						crm_library_remittance_statuses.name AS status

					' ) )

					-> whereNull( 'remittances.deleted_at' )

					-> orderBy( 'remittances.id','DESC' )

					-> paginate( 20 )

					-> setPath( 'remittances' );
				
		}

		return $remittance;
		
	}
	
	private function searchParameter() {
		
		//CONSTRUCT QUERY STRING
		$query_string = array();
		
		//CHECK IF GET IS SET AND NOT EMPTY
		if( isset( $_GET ) && ! empty( $_GET ) ) {

			//REMITTANCE TRANSACTION NO
			if( isset( $_GET[ 'search_remittance_transaction_no' ] ) && ! empty( $_GET[ 'search_remittance_transaction_no' ] ) ) {
				
				$query_string[] = " crm_remittances.transaction_number LIKE '%{$_GET[ 'search_remittance_transaction_no' ]}%' ";
				
			}

			//REMITTANCE REMITTER
			if( isset( $_GET[ 'search_remittance_remitter' ] ) && ! empty( $_GET[ 'search_remittance_remitter' ] ) ) {
				
				$query_string[] = " crm_remittances.remitter_id = {$_GET[ 'search_remittance_remitter' ]} ";
				
			}

			//REMITTANCE RECIPIENT
			if( isset( $_GET[ 'search_remittance_recipient' ] ) && ! empty( $_GET[ 'search_remittance_recipient' ] ) ) {
				
				$query_string[] = " crm_remittances.recipient_id = {$_GET[ 'search_remittance_recipient' ]} ";
				
			}

			//REMITTANCE BANK SOURCE
			if( isset( $_GET[ 'search_remittance_bank_source' ] ) && ! empty( $_GET[ 'search_remittance_bank_source' ] ) ) {
				
				$query_string[] = " crm_remittances.bank_source_id = {$_GET[ 'search_remittance_bank_source' ]} ";
				
			}

			//REMITTANCE STATUS
			if( isset( $_GET[ 'search_remittance_status' ] ) && ! empty( $_GET[ 'search_remittance_status' ] ) ) {
				
				$query_string[] = " crm_remittances.status_id = {$_GET[ 'search_remittance_status' ]} ";
				
			}

			//REMITTANCE DATE
			if(

				( isset( $_GET[ 'search_remittance_transaction_start_date' ] ) && ! empty( $_GET[ 'search_remittance_transaction_start_date' ] ) )

				&&

				( isset( $_GET[ 'search_remittance_transaction_end_date' ] ) && ! empty( $_GET[ 'search_remittance_transaction_end_date' ] ) )

			) {

				$query_string[] = " 

					DATE( crm_remittances.created_at ) >= '{$_GET[ 'search_remittance_transaction_start_date' ]}' 

					AND 

					DATE( crm_remittances.created_at ) <= '{$_GET[ 'search_remittance_transaction_end_date' ]}' 

				";

			}
			
			//RETURN SEARCH QUERY
			return implode( ' AND ', array_filter( $query_string ) );

		}
		
	}
	
	private function generatePoliLink( $remittance_details = array() ) {
		
		//POLI LINK
		$poli_xml = '
					
			<?xml version="1.0" encoding="utf-8"?>

			<InitiateTransactionRequest xmlns="http://schemas.datacontract.org/2004/07/Centricom.POLi.Services.MerchantAPI.Contracts" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">

				<AuthenticationCode>tJELmlUGCjulRrK</AuthenticationCode>

				<Transaction xmlns:dco="http://schemas.datacontract.org/2004/07/Centricom.POLi.Services.MerchantAPI.DCO">

					<dco:CurrencyAmount>' . round( str_replace( ',', '', $remittance_details[ 'service_fee' ] ), 2, PHP_ROUND_HALF_UP ) . '</dco:CurrencyAmount>

					<dco:CurrencyCode> AUD </dco:CurrencyCode>

					<dco:MerchantCheckoutURL>' . url( 'iremit/remittances/poli-nudge' ) . '</dco:MerchantCheckoutURL>

					<dco:MerchantCode> S6101170 </dco:MerchantCode>

					<dco:MerchantData></dco:MerchantData>

					<dco:MerchantDateTime>' . date( 'Y-m-d' ) . 'T' . date( 'H:i:s' ) . '</dco:MerchantDateTime>

					<dco:MerchantHomePageURL> https://iremit.com.au </dco:MerchantHomePageURL>

					<dco:MerchantRef> '. ( ! empty( $remittance_details[ 'transaction_number' ] ) ? $remittance_details[ 'transaction_number' ] : date( 'YmdHis' ) ) .'</dco:MerchantRef>

					<dco:MerchantReferenceFormat></dco:MerchantReferenceFormat>

					<dco:NotificationURL> https://iremit.com.au/notificationurl?remittance_id=' . $remittance_details[ 'remittance_id' ] . ' </dco:NotificationURL>

					<dco:SelectedFICode i:nil="true" />

					<dco:SuccessfulURL> https://iremit.com.au/thank-you?remittance_id=' . $remittance_details[ 'remittance_id' ] . ' </dco:SuccessfulURL>

					<dco:Timeout> 900 </dco:Timeout>

					<dco:UnsuccessfulURL> https://iremit.com.au/unsuccessfulurl?remittance_id=' . $remittance_details[ 'remittance_id' ] . ' </dco:UnsuccessfulURL>

					<dco:UserIPAddress> ' . $_SERVER[ 'REMOTE_ADDR' ] . '</dco:UserIPAddress>

				</Transaction>

			</InitiateTransactionRequest>
					
		';
		
		
		//CURL
		$ch = curl_init( $url );

		curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type:  text/xml' ) );

		curl_setopt( $ch, CURLOPT_HEADER, 0 );

		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );

		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );

		curl_setopt( $ch, CURLOPT_POST, 1 );

		curl_setopt( $ch, CURLOPT_POSTFIELDS, $poli_xml );

		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 0 );
		
		curl_setopt( $ch, CURLOPT_REFERER, '' );

		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		
		$response = curl_exec( $ch );
		
		curl_close ( $ch );
		
		
		//SIMPLE XML ELEMENT
		$xml = new SimpleXMLElement( $response );
		
		$xml -> registerXPathNamespace( '','http://schemas.datacontract.org/2004/07/Centricom.POLi.Services.MerchantAPI.Contracts' );

		$xml -> registerXPathNamespace( 'a','http://schemas.datacontract.org/2004/07/Centricom.POLi.Services.MerchantAPI.DCO' );

		$xml -> registerXPathNamespace( 'i','http://www.w3.org/2001/XMLSchema-instance' );
		
		//GET POLI LINK
		foreach( $xml -> xpath( '//a:NavigateURL' ) AS $navigation_url ) {
			
			$poli_link = $navigation_url;

			break;

		}
		
		//GET ERROR CODE
		foreach( $xml -> xpath( '///a:Code' ) as $code ) {
			
			$error_code = $code;
			
			break;
			
		}
		
		$success = ( ! empty( trim( $poli_link ) ) ? TRUE : FALSE );
		
		return array( 'success' => $success, 'result' => array( 'poli_link' => $poli_link ), 'error_code' => $error_code );
		
		exit;
		
		
	}
	
}
