<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;


use Illuminate\Support\Str;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\File;

use Illuminate\Pagination\Paginator;


//REMITTER DETAILS
use App\Models\Remitter;

use App\Models\RemitterAddress;

use App\Models\RemitterContact;

use App\Models\RemitterIdentification;

use App\Models\RemitterOtherMarketingSource;

use App\Models\RemitterNote;


//REMITTER COMPANY
use App\Models\RemitterCompany;

use App\Models\RemitterCompanyAuthorization;

use App\Models\RemitterCompanyOtherRelationship;

use App\Models\RemitterCompanyOtherSourceOfFund;


//LIBRARIES
use App\Models\Library\LibraryTitle;

use App\Models\Library\LibraryCivilStatus;

use App\Models\Library\LibraryIdentificationType;

use App\Models\Library\LibraryCountry;

use App\Models\Library\LibraryState;

use App\Models\Library\LibraryCity;

use App\Models\Library\LibraryNationality;

use App\Models\Library\LibraryContactType;

use App\Models\Library\LibraryRemitterAddressTenancyLength;

use App\Models\Library\LibraryRemitterType;

use App\Models\Library\LibraryRemitterMarketingSource;

use App\Models\Library\LibraryRemitterRelationship;

use App\Models\Library\LibrarySourceOfFund;

use App\Models\Library\LibraryRemittanceFrequency;

use App\Models\Library\LibraryBankType;


class RemittersController extends Controller {
	
	
	private $remitter_id_temp_path;

	private $remitter_id_path;
	
	
	private $remitter_avatar_temp_path;
	
	private $remitter_avatar_path;
	
	
	private $remitter_company_authorization_temp_path;
	
	private $remitter_company_authorization_path;

	
    public function __construct() {
		
		
		//REMITTER ID FILE PATH
		$this -> remitter_id_temp_path = public_path( 'img/remitter_id/temp' );
		
		$this -> remitter_id_path = public_path( 'img/remitter_id' );
		
		
		//REMITTER PROFILE FILE PATH
		$this -> remitter_avatar_temp_path = public_path( 'img/remitter_avatar/temp' );
		
		$this -> remitter_avatar_path = public_path( 'img/remitter_avatar' );
		
		
		//REMITTER COMPANY AUTHORIZATION FILE PATH
		$this -> remitter_company_authorization_temp_path = public_path( 'img/remitter_company_authorization/temp' );
		
		$this -> remitter_company_authorization_path = public_path( 'img/remitter_company_authorization' );
		
		
        $this -> middleware( 'auth' );
		
		
    }

    public function index() {

		$remitters = $this -> getRemitters();
		
		//LIBRARIES
		$countries = LibraryCountry::orderBy( 'id', 'ASC' ) -> get();
		
		$contact_types = LibraryContactType::orderBy( 'id', 'ASC' ) -> get();
		
		$identification_types = LibraryIdentificationType::orderBy( 'ordering', 'ASC' ) -> get();
		
		$tenancy_frequencies = LibraryRemitterAddressTenancyLength::orderBy( 'ordering', 'ASC' ) -> get();
		
		$remitter_types = LibraryRemitterType::orderBy( 'ordering', 'ASC' ) -> get();
		
		return view( 'remitters', 
		
			array(
			
				'remitters' => $remitters,

				'countries' => $countries, 
				
				'contact_types' => $contact_types,
				
				'identification_types' => $identification_types,
				
				'tenancy_frequencies' => $tenancy_frequencies,
				
				'remitter_types' => $remitter_types
			
			) 
		
		);
		
    }
    
	public function showProfile( Request $request ) {
		
		if( $request -> id_number ) {

			return view( 'remitter-profile' );
			
		}

	}

	public function getProfileAvatar( Request $request ) {
		
		//GET VIA AJAX
		if( $request -> ajax() ) {
			
			//REMITTER DETAILS
			$remitter_details = Remitter::where( 'id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();
			
			//GET FILENAME
			$filename = ( ! empty( $remitter_details[ 'filename' ] ) ? $remitter_details[ 'filename' ] : '' );
			
			//GET FILE SIZE
			$size = ( ! empty( $filename ) ? \File::size( public_path( "img/remitter_avatar/{$filename}" ) ) : 0 );
			
			//SHOW SOME RESPONSE
			return \Response::json( ['success' => true, 'result' => [ 'filename' => $filename, 'size' => $size ] ] );
			
		}
		
	}

	public function getIdentificationImages( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			//GET REMITTER IDENTIFICATION
			$remitter_identifications = RemitterIdentification::where( 'remitter_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get() -> toArray();

			//RESET IDENTIFICATION IMAGES
			$idenfitication_images = array();

			//LOOP ALL REMITTER IDENTIFICATIONS
			foreach ( $remitter_identifications as $key => $remitter_identification ) {

				//GET IDENTIFICATION ID
				$idenfitication_images[ $key ][ 'id' ] = $remitter_identification[ 'id' ];
			
				//GET FILENAME
				$idenfitication_images[ $key ][ 'filename' ] = $remitter_identification[ 'filename' ];
			
				//GET FILE SIZE
				$idenfitication_images[ $key ][ 'size' ] = ( ! empty( $remitter_identification[ 'filename' ] ) && ! is_null( $remitter_identification[ 'filename' ] ) ? \File::size( public_path( "img/remitter_id/{$remitter_identification[ 'filename' ]}" ) ) : 0 );

			}
			
			//SHOW SOME RESPONSE
			return \Response::json( ['success' => true, 'result' => [ 'idenfitication_images' => $idenfitication_images ] ] );
		
		}
		
	}

	public function getCompanyAuthorization( Request $request ) {
		
		//GET VIA AJAX
		if( $request -> ajax() ) {
			
			//REMITTER COMPANY DETAILS
			$remitter_company_details = RemitterCompanyAuthorization::where( 'remitter_company_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();
			
			//GET FILENAME
			$filename = ( ! empty( $remitter_company_details[ 'filename' ] ) ? $remitter_company_details[ 'filename' ] : '' );
			
			//GET FILE SIZE
			$size = ( ! empty( $filename ) ? \File::size( public_path( "img/remitter_company_authorization/{$filename}" ) ) : 0 );
			
			//SHOW SOME RESPONSE
			return \Response::json( ['success' => true, 'result' => [ 'filename' => $filename, 'size' => $size ] ] );
			
		}
		
	}

	public function getModal( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			
			
			//LIBRARIES
			$titles = LibraryTitle::orderBy( 'ordering', 'ASC' ) -> get();
			
			$civil_status = LibraryCivilStatus::orderBy( 'ordering', 'ASC' ) -> get();
			
			$identification_types = LibraryIdentificationType::orderBy( 'ordering', 'ASC' ) -> get();
			
			$countries = LibraryCountry::orderBy( 'id', 'ASC' ) -> get();
			
			$states = LibraryState::where( 'country_id', 14 ) -> orderBy( 'id', 'ASC' ) -> get();
			
			$cities = LibraryCity::orderBy( 'id', 'ASC' ) -> get();
			
			$nationalities = LibraryNationality::orderBy( 'id', 'ASC' ) -> get();
			
			$contact_types = LibraryContactType::orderBy( 'id', 'ASC' ) -> get();
			
			$tenancy_frequencies = LibraryRemitterAddressTenancyLength::orderBy( 'ordering', 'ASC' ) -> get();
			
			$remitter_types = LibraryRemitterType::orderBy( 'ordering', 'ASC' ) -> get();
			
			$remitter_maketing_sources = LibraryRemitterMarketingSource::orderBy( 'ordering', 'ASC' ) -> get();
			
			
			//COMPANY
			$remitter_company_countries = LibraryCountry::orderBy( 'id', 'ASC' ) -> get();
			
			$remitter_company_relationships = LibraryRemitterRelationship::orderBy( 'ordering', 'ASC' ) -> get();
			
			$remitter_company_source_of_funds = LibrarySourceOfFund::orderBy( 'ordering', 'ASC' ) -> get();
			
			$remitter_company_frequencies = LibraryRemittanceFrequency::orderBy( 'id', 'ASC' ) -> get();
			
			$remitter_company_bank_types = LibraryBankType::orderBy( 'id', 'ASC' ) -> get();
			
			
			
			//EDIT FORM
			if( $request -> id != NULL || $request -> id != '' ) {
				
				//REMITTER DETAILS
				$remitter_details = Remitter::where( 'id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();
				
				//REMITTER ADDRESSES
				$remitter_addresses = RemitterAddress::where( 'remitter_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get();
				
				//REMITTER CONTACTS
				$remitter_contacts = RemitterContact::where( 'remitter_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get();
				
				//REMITTER IDENTIFICATIONS
				$remitter_identifications = RemitterIdentification::where( 'remitter_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get();
				
				//CHECK IF REMITTER DETAILS MARKETING SOURCE ID IS NOT EMPTY AND NOT NULL AND EQUAL TO 1
				if( ! empty( $remitter_details[ 'marketing_source_id' ] ) && ! is_null( $remitter_details[ 'marketing_source_id' ] ) && $remitter_details[ 'marketing_source_id' ] == 1 ) {
					
					//REMITTER OTHER MARKETING SOURCE
					$remitter_other_marketing_source = RemitterOtherMarketingSource::where( 'remitter_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();

				}
			
				//REMITTER NOTES
				$remitter_notes = RemitterNote::where( 'remitter_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> get() -> first();
			
			
			
				//REMITTER COMPANY DETAILS
				$remitter_company_details = RemitterCompany::where( 'remitter_id', '=', $request -> id ) -> orderBy( 'id', 'ASC' ) -> first();
				


				//CHECK IF REMITTER COMPANY DETAILS IS NOT EMPTY
				if( ! empty( $remitter_company_details ) ) {
					
					//REMITTER COMPANY AUTHORIZATION
					$remitter_company_authorization = RemitterCompanyAuthorization::where( 'remitter_company_id', '=', $remitter_company_details[ 'id' ] ) -> orderBy( 'id', 'ASC' ) -> first();
					
					//REMITTER COMPANY OTHER RELATIONSHIP
					$remitter_company_other_relationship = RemitterCompanyOtherRelationship::where( 'remitter_company_id', '=', $remitter_company_details[ 'id' ] ) -> orderBy( 'id', 'ASC' ) -> first();
					
					//REMITTER COMPANY OTHER SOURCE OF FUND
					$remitter_company_other_source_of_fund = RemitterCompanyOtherSourceOfFund::where( 'remitter_company_id', '=', $remitter_company_details[ 'id' ] ) -> orderBy( 'id', 'ASC' ) -> first();
				
				}
			
			
				//REMITTER DETAILS
				$histories[] = $remitter_details[ 'revisionHistory' ];
				
				//REMITTER ADDRESS
				foreach( $remitter_addresses as $remitter_address ) {
					
					$histories[] = $remitter_address[ 'revisionHistory' ];
					
				}
				
				//REMITTER CONTACT
				foreach( $remitter_contacts as $remitter_contact ) {
					
					$histories[] = $remitter_contact[ 'revisionHistory' ];
					
				}
				
				//REMITTER IDENTIFICATION
				foreach( $remitter_identifications as $remitter_identification ) {
					
					$histories[] = $remitter_identification[ 'revisionHistory' ];
					
				}
				
				//REMITTER NOTES
				$histories[] = $remitter_notes[ 'revisionHistory' ];
			
			
			
				//CHECK IF REMITTER COMPANY DETAILS IS NOT EMPTY
				if( ! empty( $remitter_company_details ) ) {
					
					//REMITTER COMPANY DETAILS
					$histories[] = $remitter_company_details[ 'revisionHistory' ];
					
					//REMITTER COMPANY AUTHORIZATION
					$histories[] = $remitter_company_authorization[ 'revisionHistory' ];
					
					//REMITTER COMPANY OTHER RELATIONSHIP
					$histories[] = $remitter_company_other_relationship[ 'revisionHistory' ];
					
					//REMITTER COMPANY OTHER SOURCE OF FUND
					$histories[] = $remitter_company_other_source_of_fund[ 'revisionHistory' ];
				
				}
			
			
			
				//DOUBLE RECOIL HISTORIES
				$new_histories = array();
				
				$x = 0;
				
				foreach( array_filter( $histories ) as $history ) {
					
					foreach( $history as $key => $hist ) {
						
						$new_histories[ $x ][ 'field_name' ] = $hist -> fieldName();
						
						$new_histories[ $x ][ 'old_value' ] = $hist -> oldValue();
						
						$new_histories[ $x ][ 'new_value' ] = $hist -> newValue();
						
						$new_histories[ $x ][ 'full_name' ] = $hist -> userResponsible() -> first_name . ' ' . $hist -> userResponsible() -> last_name;
						
						$x++;
						
					}
					
				}
			
			
			
				//REMITTER DATA
				$remitter_data = array(
								

					'action' => 'edit',
					

					//LIBRARIES
					'titles' => $titles,

					'civil_status' => $civil_status, 

					'identification_types' => $identification_types,
					
					'countries' => $countries,

					'states' => $states,
					
					'cities' => $cities,

					'nationalities' => $nationalities,
					
					'contact_types' => $contact_types,
					
					'tenancy_frequencies' => $tenancy_frequencies,
					
					'remitter_types' => $remitter_types,
					
					'remitter_maketing_sources' => $remitter_maketing_sources,
					

					//REMITTER COMPANY LIBRARIES
					'remitter_company_relationships' => $remitter_company_relationships,
					
					'remitter_company_source_of_funds' => $remitter_company_source_of_funds,
					
					'remitter_company_frequencies' => $remitter_company_frequencies,
					
					'remitter_company_bank_types' => $remitter_company_bank_types,
					

					//REMITTER DETAILS
					'remitter_id' => $remitter_details[ 'id' ],
					
					'remitter_details' => $remitter_details,
					
					'remitter_addresses' => $remitter_addresses -> toArray(),

					'remitter_contacts' => $remitter_contacts -> toArray(),

					'remitter_identifications' => $remitter_identifications -> toArray(),
					
					
					
					'remitter_notes' => $remitter_notes,


					//HISTORIES
					'histories' => $new_histories
					

				);
			
			
			
				//CHECK IF REMITTER DETAILS MARKETING SOURCE ID IS NOT EMPTY AND NOT NULL AND EQUAL TO 1
				if( ! empty( $remitter_details[ 'marketing_source_id' ] ) && ! is_null( $remitter_details[ 'marketing_source_id' ] ) && $remitter_details[ 'marketing_source_id' ] == 1 ) {
					
					$remitter_data = array_merge( $remitter_data, array( 'remitter_other_marketing_source' => $remitter_other_marketing_source -> toArray() ) );
					
				}
			
			
			
				//CHECK IF REMITTER COMPANY DETAILS IS NOT EMPTY
				if( ! empty( $remitter_company_details ) ) {
					
					$remitter_data = array_merge( $remitter_data, array(
					
						//REMITTER COMPANY
						'remitter_company_id' => $remitter_company_details[ 'id' ],
						
						'remitter_company_details' => $remitter_company_details,
						
						'remitter_company_authorization' => $remitter_company_authorization,
						
						'remitter_company_other_relationship' => $remitter_company_other_relationship,
						
						'remitter_company_other_source_of_fund' => $remitter_company_other_source_of_fund
					
					) );
					
				}
			
			
			
				//CONTENT
				$content = view( 'modals.remitters' ) -> with( $remitter_data ) -> render();
			
			
			
				//RESULT
				$result = array( 'remitter_id' => ( int ) $request -> id, 'content' => $content, 'action' => 'edit' );
				
				//CHECK IF REMITTER COMPANY DETAILS IS NOT EMPTY
				if( ! empty( $remitter_company_details ) ) {
					
					$result = array_merge( $result, array( 'remitter_company_id' => $remitter_company_details[ 'id' ] ) );
					
				}
			
			
			
				//SHOW SOME RESPONSE
				return \Response::json( ['success' => true, 'result' => $result ] );
				
			}
			
			
			
			//NEW FORM
			else {

				$content = view( 'modals.remitters' )

							-> with( [
								

								'action' => 'new',
								

								//LIBRARIES
								'titles' => $titles,

								'civil_status' => $civil_status, 

								'identification_types' => $identification_types,
								
								'countries' => $countries,

								'states' => $states,
								
								'cities' => $cities,

								'nationalities' => $nationalities,
								
								'contact_types' => $contact_types,
								
								'tenancy_frequencies' => $tenancy_frequencies,
								
								'remitter_types' => $remitter_types,
								
								'remitter_maketing_sources' => $remitter_maketing_sources,
								

								//REMITTER COMPANY LIBRARIES
								'remitter_company_relationships' => $remitter_company_relationships,
								
								'remitter_company_source_of_funds' => $remitter_company_source_of_funds,
								
								'remitter_company_frequencies' => $remitter_company_frequencies,
								
								'remitter_company_bank_types' => $remitter_company_bank_types
								

							] ) 

							-> render();
				
				//SHOW SOME RESPONSE
				return \Response::json( ['success' => true, 'result' => [ 'remitter_id' => 0, 'content' => $content, 'action' => 'new' ] ] );
				
			}
			
			
			
		}
		
	}
	
	public function postForm( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {
				
				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					
					
					/* 
					*-----------------------------------------
					* REMITTER PROFILE VALIDATING EMAILS START
					*-----------------------------------------
					*/
					
					
					
					
					//CHECK IF REMITTER ID IS EMPTY
					if( empty( $input[ 'remitter_id' ] ) ) {
						
						//GET PRIMARY EMAIL AND CHECK IF ALREADY EXISTS FOR NEW REMITTER
						$email = Remitter::where( 'email', $input[ 'remitter_primary_email_address' ] ) -> first( [ 'email' ] );
						
						if( ! is_null( $email ) && empty( $input[ 'remitter_id' ] ) ) {
							
							return \Response::json( ['success' => false, 'error_message' => 'Existing primary email address!' ] );
							
						}
					
						//GET SECONDARY EMAIL AND CHECK IF ALREADY EXISTS FOR NEW REMITTER
						$secondary_email = Remitter::where( 'secondary_email', $input[ 'remitter_secondary_email_address' ] ) -> first( [ 'secondary_email' ] );
						
						if( ! is_null( $secondary_email ) && empty( $input[ 'remitter_id' ] ) ) {
							
							return \Response::json( ['success' => false, 'error_message' => 'Existing secondary email address!' ] );
							
						}

					}
					
					
					
					
					/* 
					*-----------------------------------------
					* REMITTER PROFILE VALIDATING EMAILS END
					*-----------------------------------------
					*/
					
					
					
					
					/* 
					*------------------------------
					* REMITTER PROFILE SAVING START
					*------------------------------
					*/
					
					
					
					
					//CHECK IF REMITTER ID IS NOT EMPTY
					if( ! empty( $input[ 'remitter_id' ] ) ) {
						
						$new_remitter = Remitter::find( $input[ 'remitter_id' ] );
						
						$action = 'edit';
						
					}
					
					else {
						
						$new_remitter = new Remitter;
						
						//GENERATE SOME ID NUMBER
						$new_remitter -> id_number = Str::random( 8 );
						
						$new_remitter -> email = ( isset( $input[ 'remitter_primary_email_address' ] ) && ! empty( $input[ 'remitter_primary_email_address' ] ) ? $input[ 'remitter_primary_email_address' ] : NULL );

						$new_remitter -> userid = NULL; //STILL NEED TO UPDATE 

						$action = 'add';

					}

					$new_remitter -> type_id = ( isset( $input[ 'remitter_type_id' ] ) && ! empty( $input[ 'remitter_type_id' ] ) ? $input[ 'remitter_type_id' ] : NULL);
					
					$new_remitter -> title_id = ( isset( $input[ 'remitter_title' ] ) && ! empty( $input[ 'remitter_title' ] ) ? $input[ 'remitter_title' ] : NULL);
					
					$new_remitter -> firstname = ( isset( $input[ 'remitter_firstname' ] ) && ! empty( $input[ 'remitter_firstname' ] ) ? $input[ 'remitter_firstname' ] : NULL );
					
					$new_remitter -> middlename = ( isset( $input[ 'remitter_middlename' ] ) && ! empty( $input[ 'remitter_middlename' ] ) ? $input[ 'remitter_middlename' ] : NULL );
					
					$new_remitter -> lastname = ( isset( $input[ 'remitter_lastname' ] ) && ! empty( $input[ 'remitter_lastname' ] ) ? $input[ 'remitter_lastname' ] : NULL );
					
					$new_remitter -> gender = ( isset( $input[ 'remitter_gender' ] ) && ! empty( $input[ 'remitter_gender' ] ) ? $input[ 'remitter_gender' ] : NULL );
					
					$new_remitter -> birthday = ( isset( $input[ 'remitter_birthday' ] ) && ! empty( $input[ 'remitter_birthday' ] ) ? $input[ 'remitter_birthday' ] : NULL );
					
					$new_remitter -> civil_status_id = ( isset( $input[ 'remitter_civil_status' ] ) && ! empty( $input[ 'remitter_civil_status' ] ) ? $input[ 'remitter_civil_status' ] : NULL );
					
					$new_remitter -> occupation = ( isset( $input[ 'remitter_occupation' ] ) && ! empty( $input[ 'remitter_occupation' ] ) ? $input[ 'remitter_occupation' ] : NULL );
					
					$new_remitter -> secondary_email = ( isset( $input[ 'remitter_secondary_email_address' ] ) && ! empty( $input[ 'remitter_secondary_email_address' ] ) ? $input[ 'remitter_secondary_email_address' ] : NULL );
					
					$new_remitter -> filename = ( isset( $input[ 'remitter_profile_avatar' ] ) && ! empty( $input[ 'remitter_profile_avatar' ] ) ? $input[ 'remitter_profile_avatar' ] : NULL );
					
					//CHECK IF REMITTER PROFILE AVATAR IS ISSET AND NOT EMPTY
					if( isset( $input[ 'remitter_profile_avatar' ] ) && ! empty( $input[ 'remitter_profile_avatar' ] ) ) {
						
						//CHECK IF REMITTER ID IS NOT EMPTY
						if( ! empty( $input[ 'remitter_id' ] ) ) {
							
							$remitter_details = Remitter::where( 'id', '=', $input[ 'remitter_id' ] ) -> first( [ 'filename' ] );
						
							$remitter_profile_avatar = $remitter_details[ 'filename' ];
							
							//CHECK IF REMITTER PROFILE AVATAR IS NOT EMPTY
							if( ! empty( $remitter_profile_avatar ) && ! is_null( $remitter_profile_avatar ) ) {
							
								//CHECK IF REMITTER PROFILE AVATAR IS NOT EQUAL TO INPUT REMITTER PROFILE AVATAR
								if( trim( $remitter_profile_avatar ) != trim( $input[ 'remitter_profile_avatar' ] ) ) {

									//DELETE THE EXISTING REMITTER PROFILE AVATAR
									File::delete( $this -> remitter_avatar_path . '/' . $remitter_profile_avatar );

									//MOVE THE NEW REMITTER PROFILE AVATAR
									File::move( $this -> remitter_avatar_temp_path . '/' . $input[ 'remitter_profile_avatar' ] , $this -> remitter_avatar_path . '/' . $input[ 'remitter_profile_avatar' ] );

								}
							
							}

							//REMITTER PROFILE AVATAR IS EMPTY
							else {
								
								//MOVE THE NEW REMITTER PROFILE AVATAR
								File::move( $this -> remitter_avatar_temp_path . '/' . $input[ 'remitter_profile_avatar' ] , $this -> remitter_avatar_path . '/' . $input[ 'remitter_profile_avatar' ] );

							}
						
						}
						
						//JUST MOVE REMITTER AVATAR FROM TEMP FOLDER
						else {
							
							//MOVE REMITTER PROFILE AVATAR FROM TEMP
							File::move( $this -> remitter_avatar_temp_path . '/' . $input[ 'remitter_profile_avatar' ] , $this -> remitter_avatar_path . '/' . $input[ 'remitter_profile_avatar' ] );
							
						}
						
					}
					
					//REMOVE REMITTER PROFILE AVATAR
					else {
						
						//CHECK IF REMITTER ID IS NOT EMPTY
						if( ! empty( $input[ 'remitter_id' ] ) ) {

							$remitter_details = Remitter::where( 'id', '=', $input[ 'remitter_id' ] ) -> first( [ 'filename' ] );
						
							$remitter_profile_avatar = $remitter_details[ 'filename' ];
							
							//CHECK IF REMITTER PROFILE AVATAR IS NOT EMPTY
							if( ! empty( $remitter_profile_avatar ) ) {

								//DELETE THE EXISTING REMITTER PROFILE AVATAR
								File::delete( $this -> remitter_avatar_path . '/' . $remitter_profile_avatar );
								
							}
						
						}
						
					}
					
					$new_remitter -> nationality_id = ( isset( $input[ 'remitter_nationality' ] ) && ! empty( $input[ 'remitter_nationality' ] ) ? $input[ 'remitter_nationality' ] : NULL );
					
					$new_remitter -> marketing_source_id = ( isset( $input[ 'remitter_marketing_source_id' ] ) && ! empty( $input[ 'remitter_marketing_source_id' ] ) ? $input[ 'remitter_marketing_source_id' ] : NULL );
					
					$new_remitter -> is_personal = ( isset( $input[ 'remitter_type_id' ] ) && ! empty( $input[ 'remitter_type_id' ] ) && $input[ 'remitter_type_id' ] == 1 ? true : false);
					
					$new_remitter -> is_company = ( isset( $input[ 'remitter_type_id' ] ) && ! empty( $input[ 'remitter_type_id' ] ) && $input[ 'remitter_type_id' ] != 1 ? true : false);
					
					$new_remitter -> save();
					
					$remitter_id = $new_remitter['id'];
					
					
					
					
					
						/* 
						*---------------------------------------------
						* REMITTER OTHER MARKETING SOURCE SAVING START
						*---------------------------------------------
						*/
					
					
					
					
					
						//CHECK IF REMITTER MARKETING SOURCE ID IS ISSET AND NOT EMPTY AND EQUAL TO 1
						if( isset( $input[ 'remitter_marketing_source_id' ] ) && ! empty( $input[ 'remitter_marketing_source_id' ] ) && $input[ 'remitter_marketing_source_id' ] == 1 ) {
							
							//CHECK IF REMITTER ID IS NOT EMPTY
							if( ! empty( $input[ 'remitter_id' ] ) ) {
								
								$new_remitter_other_marketing_source = RemitterOtherMarketingSource::where( 'remitter_id', '=', $input[ 'remitter_id' ] ) -> first();
								
								//CHECK IF NEW REMITTER OTHER MARKETING SOURCE IS EMPTY
								if( empty( $new_remitter_other_marketing_source ) ) {

									$new_remitter_other_marketing_source = new RemitterOtherMarketingSource;

								}
								
							}
							
							else {
								
								$new_remitter_other_marketing_source = new RemitterOtherMarketingSource;
								
							}
							
							$new_remitter_other_marketing_source -> remitter_id = $remitter_id;
							
							$new_remitter_other_marketing_source -> marketing_source = ( isset( $input[ 'remitter_other_marketing_source' ] ) && ! empty( $input[ 'remitter_other_marketing_source' ] ) ? $input[ 'remitter_other_marketing_source' ] : NULL );
							
							$new_remitter_other_marketing_source -> save();
						
						}
						
						//REMOVE REMITTER MARKETING SOURCE
						else {
							
							//CHECK IF REMITTER ID IS NOT EMPTY
							if( ! empty( $input[ 'remitter_id' ] ) ) {
								
								$remove_remitter_company = RemitterOtherMarketingSource::where( 'remitter_id', '=', $input[ 'remitter_id' ] ) -> delete();
								
							}
							
						}
					
					
					
					
					
						/* 
						*---------------------------------------------
						* REMITTER OTHER MARKETING SOURCE SAVING END
						*---------------------------------------------
						*/
					
					
					
					
					
					/*
					*------------------------------
					* REMITTER PROFILE SAVING END
					*------------------------------
					*/
					
					
					
					
					
					/* 
					*------------------------------
					* REMITTER COMPANY SAVING START
					*------------------------------
					*/
					
					
					
					
					
					//CHECK IF REMITTER TYPE ID IS ISSET AND NOT EMPTY AND NOT EQUAL TO 1
					if( isset( $input[ 'remitter_type_id' ] ) && ! empty( $input[ 'remitter_type_id' ] ) && $input[ 'remitter_type_id' ] != 1 ) {
					
					
					
					
						//CHECK IF REMITTER COMPANY ID IS NOT EMPTY
						if( ! empty( $input[ 'remitter_company_id' ] ) ) {
							
							$new_remitter_company = RemitterCompany::find( $input[ 'remitter_company_id' ] ) ;
							
						}
						
						else {
							
							$new_remitter_company = new RemitterCompany;
							
						}
						
						$new_remitter_company -> remitter_id = $remitter_id;
					
						$new_remitter_company -> name = ( isset( $input[ 'remitter_company_name' ] ) && ! empty( $input[ 'remitter_company_name' ] ) ? $input[ 'remitter_company_name' ] : NULL);
						
						$new_remitter_company -> address = ( isset( $input[ 'remitter_company_address' ] ) && ! empty( $input[ 'remitter_company_address' ] ) ? $input[ 'remitter_company_address' ] : NULL);
						
						$new_remitter_company -> contact = ( isset( $input[ 'remitter_company_contact' ] ) && ! empty( $input[ 'remitter_company_contact' ] ) ? $input[ 'remitter_company_contact' ] : NULL);
						
						$new_remitter_company -> registration_date = ( isset( $input[ 'remitter_company_registration' ] ) && ! empty( $input[ 'remitter_company_registration' ] ) ? $input[ 'remitter_company_registration' ] : NULL);
						
						$new_remitter_company -> country_id = ( isset( $input[ 'remitter_company_country_id' ] ) && ! empty( $input[ 'remitter_company_country_id' ] ) ? $input[ 'remitter_company_country_id' ] : NULL);
						
						$new_remitter_company -> company_relationship_id = ( isset( $input[ 'remitter_company_relationship_id' ] ) && ! empty( $input[ 'remitter_company_relationship_id' ] ) ? $input[ 'remitter_company_relationship_id' ] : NULL);
						
						$new_remitter_company -> source_of_fund_id = ( isset( $input[ 'remitter_company_source_of_fund_id' ] ) && ! empty( $input[ 'remitter_company_source_of_fund_id' ] ) ? $input[ 'remitter_company_source_of_fund_id' ] : NULL);
						
						$new_remitter_company -> remittance_range = ( isset( $input[ 'remitter_company_remittance_range' ] ) && ! empty( $input[ 'remitter_company_remittance_range' ] ) ? $input[ 'remitter_company_remittance_range' ] : NULL);
						
						$new_remitter_company -> frequency_id = ( isset( $input[ 'remitter_company_frequency_id' ] ) && ! empty( $input[ 'remitter_company_frequency_id' ] ) ? $input[ 'remitter_company_frequency_id' ] : NULL);
						
						$new_remitter_company -> bank_type_id = ( isset( $input[ 'remitter_company_bank_type_id' ] ) && ! empty( $input[ 'remitter_company_bank_type_id' ] ) ? $input[ 'remitter_company_bank_type_id' ] : NULL);
						
						$new_remitter_company -> save();
						
						$remitter_company_id = $new_remitter_company[ 'id' ];
					
					
					
					
						//CHECK IF REMITTER COMPANY RELATIONSHIP ID IS ISSET AND NOT EMPTY AND EQUAL TO 1
						if( isset( $input[ 'remitter_company_relationship_id' ] ) && ! empty( $input[ 'remitter_company_relationship_id' ] ) && $input[ 'remitter_company_relationship_id' ] == 1 ) {
							
							//CHECK IF REMITTER COMPANY ID IS NOT EMPTY
							if( ! empty( $input[ 'remitter_company_id' ] ) ) {
								
								$new_remitter_company_other_relationship = RemitterCompanyOtherRelationship::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> first();
								
							}
							
							else {
								
								$new_remitter_company_other_relationship = new RemitterCompanyOtherRelationship;
								
								$new_remitter_company_other_relationship -> remitter_company_id = $remitter_company_id;

							}
							
							$new_remitter_company_other_relationship -> relationship = ( isset( $input[ 'remitter_company_other_relationship' ] ) && ! empty( $input[ 'remitter_company_other_relationship' ] ) ? $input[ 'remitter_company_other_relationship' ] : NULL);
							
							$new_remitter_company_other_relationship -> save();
							
							$remitter_company_other_relationship_id = $new_remitter_company_other_relationship[ 'id' ];
							
						}
						
						//REMOVE REMITTER COMPANY OTHER RELATIONSHIP
						else {
							
							//CHECK IF REMITTER COMPANY ID IS NOT EMPTY
							if( ! empty( $input[ 'remitter_company_id' ] ) ) {
								
								$remove_remitter_company_other_relationship = RemitterCompanyOtherRelationship::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> delete();
							
							}
						
						}
					
					
					
					
						//CHECK IF REMITTER COMPANY SOURCE OF FUND ID IS ISSET AND NOT EMPTY AND EQUAL TO 1
						if( isset( $input[ 'remitter_company_source_of_fund_id' ] ) && ! empty( $input[ 'remitter_company_source_of_fund_id' ] ) && $input[ 'remitter_company_source_of_fund_id' ] == 1 ) {
							
							//CHECK IF REMITTER COMPANY ID IS NOT EMPTY
							if( ! empty( $input[ 'remitter_company_id' ] ) ) {
								
								$new_remitter_company_other_source_of_fund = RemitterCompanyOtherSourceOfFund::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> first();
								
							}
							
							else {
								
								$new_remitter_company_other_source_of_fund = new RemitterCompanyOtherSourceOfFund;
								
								$new_remitter_company_other_source_of_fund -> remitter_company_id = $remitter_company_id;

							}
							
							$new_remitter_company_other_source_of_fund -> source_of_fund = ( isset( $input[ 'remitter_company_other_source_of_fund' ] ) && ! empty( $input[ 'remitter_company_other_source_of_fund' ] ) ? $input[ 'remitter_company_other_source_of_fund' ] : NULL);
							
							$new_remitter_company_other_source_of_fund -> save();
							
							$remitter_company_other_source_of_fund_id = $new_remitter_company_other_source_of_fund[ 'id' ];
							
						}
						
						//REMOVE REMITTER COMPANY OTHER SOURCE OF FUND
						else {
							
							//CHECK IF REMITTER COMPANY ID IS NOT EMPTY
							if( ! empty( $input[ 'remitter_company_id' ] ) ) {
								
								$remove_remitter_company_other_source_of_fund = RemitterCompanyOtherSourceOfFund::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> delete();
							
							}
						
						}
					
					
					
					
						//CHECK IF REMITTER COMPANY AUTHORIZATION IS ISSET AND NOT EMPTY
						if( isset( $input[ 'remitter_company_authorization' ] ) && ! empty( $input[ 'remitter_company_authorization' ] ) ) {
							
							//CHECK IF REMITTER COMPANY ID IS NOT EMPTY
							if( ! empty( $input[ 'remitter_company_id' ] ) ) {
								
								$remitter_company_details = RemitterCompanyAuthorization::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> first( [ 'filename' ] );
							
								$remitter_company_authorization_filename = $remitter_company_details[ 'filename' ];
								
								//CHECK IF REMITTER COMPANY AUTHORIZATION FILENAME IS NOT EMPTY
								if( ! empty( $remitter_company_authorization_filename ) && ! is_null( $remitter_company_authorization_filename ) ) {
									
									//CHECK IF REMITTER COMPANY AUTHORIZATION FILENAME IS NOT EQUAL TO INPUT REMITTER COMPANY AUTHORIZATION FILENAME
									if( trim( $remitter_company_authorization_filename ) != trim( $input[ 'remitter_company_authorization' ] ) ) {



										//DELETE THE EXISTING REMITTER COMPANY AUTHORIZATION
										File::delete( $this -> remitter_company_authorization_path . '/' . $remitter_company_authorization_filename );
										
										//MOVE THE NEW REMITTER COMPANY AUTHORIZATION
										File::move( $this -> remitter_company_authorization_temp_path . '/' . $input[ 'remitter_company_authorization' ] , $this -> remitter_company_authorization_path . '/' . $input[ 'remitter_company_authorization' ] );



										//UPDATE REMITTER COMPANY AUTHORIZATION
										$new_remitter_company_authorization = RemitterCompanyAuthorization::where( 'remitter_company_id', '=',  $input[ 'remitter_company_id' ] ) -> first();
										
										$new_remitter_company_authorization -> remitter_company_id = $remitter_company_id;
										
										$new_remitter_company_authorization -> filename = ( isset( $input[ 'remitter_company_authorization' ] ) && ! empty( $input[ 'remitter_company_authorization' ] ) ? $input[ 'remitter_company_authorization' ] : NULL );
										
										$new_remitter_company_authorization -> save();



									}
									
								}
								
								//REMITTER COMPANY AUTHORIZATION FILENAME IS EMPTY
								else {



									//MOVE THE NEW REMITTER COMPANY AUTHORIZATION
									File::move( $this -> remitter_company_authorization_temp_path . '/' . $input[ 'remitter_company_authorization' ] , $this -> remitter_company_authorization_path . '/' . $input[ 'remitter_company_authorization' ] );



									//SAVE REMITTER COMPANY AUTHORIZATION
									$new_remitter_company_authorization = new RemitterCompanyAuthorization;
									
									$new_remitter_company_authorization -> remitter_company_id = $remitter_company_id;
									
									$new_remitter_company_authorization -> filename = ( isset( $input[ 'remitter_company_authorization' ] ) && ! empty( $input[ 'remitter_company_authorization' ] ) ? $input[ 'remitter_company_authorization' ] : NULL );
									
									$new_remitter_company_authorization -> save();



								}
							
							}
							
							//JUST MOVE REMITTER COMPANY AUTHORIZATION FROM TEMP FOLDER
							else {



								//MOVE REMITTER COMPANY AUTHORIZATION FROM TEMP
								File::move( $this -> remitter_company_authorization_temp_path . '/' . $input[ 'remitter_company_authorization' ] , $this -> remitter_company_authorization_path . '/' . $input[ 'remitter_company_authorization' ] );



								//SAVE REMITTER COMPANY AUTHORIZATION
								$new_remitter_company_authorization = new RemitterCompanyAuthorization;
								
								$new_remitter_company_authorization -> remitter_company_id = $remitter_company_id;
								
								$new_remitter_company_authorization -> filename = ( isset( $input[ 'remitter_company_authorization' ] ) && ! empty( $input[ 'remitter_company_authorization' ] ) ? $input[ 'remitter_company_authorization' ] : NULL );
								
								$new_remitter_company_authorization -> save();



							}
							
						}
						
						//REMOVE REMITTER COMPANY AUTHORIZATION
						else {
							
							//CHECK IF REMITTER COMPANY ID IS NOT EMPTY
							if( ! empty( $input[ 'remitter_company_id' ] ) ) {

								$remitter_company_details = RemitterCompanyAuthorization::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> first( [ 'filename' ] );
							
								$remitter_company_authorization_filename = $remitter_company_details[ 'filename' ];
								
								//CHECK IF REMITTER COMPANY AUTHORIZATION FILENAME IS NOT EMPTY
								if( ! empty( $remitter_company_authorization_filename ) ) {

									//DELETE THE EXISTING REMITTER COMPANY AUTHORIZATION
									File::delete( $this -> remitter_company_authorization_path . '/' . $remitter_company_authorization_filename );
									
									//REMOVE REMITTER COMPANY AUTHORIZATION
									$remove_remitter_company_authorization = RemitterCompanyAuthorization::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> delete();

								}
							
							}

						}
					
					
					
					
					}
					
					//REMOVE REMITTER COMPANY
					else {
					
						//CHECK IF REMITTER COMPANY ID IS NOT EMPTY
						if( ! empty( $input[ 'remitter_company_id' ] ) ) {
							
							$remove_remitter_company = RemitterCompany::find( $input[ 'remitter_company_id' ] ) -> delete();
							
							$remove_remitter_company_authorization = RemitterCompanyAuthorization::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> delete();
							
							$remove_remitter_company_other_relationship = RemitterCompanyOtherRelationship::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> delete();
							
							$remove_remitter_company_other_source_of_fund = RemitterCompanyOtherSourceOfFund::where( 'remitter_company_id', '=', $input[ 'remitter_company_id' ] ) -> delete();
							
						}
					
					}
					
					
					
					
					
					/* 
					*------------------------------
					* REMITTER COMPANY SAVING END
					*------------------------------
					*/
					
					
					
					
					
					/* 
					*------------------------------
					* REMITTER ADDRESS SAVING START
					*------------------------------
					*/
					
					
					
					
					
					$remitter_address_is_preferred = FALSE;
					
					$existing_remitter_address_ids = array();
					
					$old_remitter_address_ids = RemitterAddress::where( 'remitter_id', '=', $remitter_id ) -> pluck( 'id' ) -> toArray();
					
					
					
					$remitter_address_olds = ( isset( $input[ 'remitter_address_old' ] ) && ! empty( $input[ 'remitter_address_old' ] ) ? array_values( $input[ 'remitter_address_old' ] ) : '' );
					
					//CHECK IF REMITTER ADDRESS OLD IS NOT EMPTY
					if( ! empty ( $remitter_address_olds ) ) {
						
						//LOOP ALL REMITTER ADDRESSES
						foreach( $remitter_address_olds as $key => $remitter_address_old ) {
							
							$existing_remitter_address_ids[] = $remitter_address_old[ 'id' ];
							
							$new_remitter_address_old = RemitterAddress::find( $remitter_address_old[ 'id' ] );

							$new_remitter_address_old -> remitter_id = $remitter_id;
							
							$new_remitter_address_old -> current_address = ( isset( $remitter_address_old[ 'current_address' ] ) && ! empty( $remitter_address_old[ 'current_address' ] ) ? $remitter_address_old[ 'current_address' ] : NULL );

							$new_remitter_address_old -> permanent_address = ( isset( $remitter_address_old[ 'permanent_address' ] ) && ! empty( $remitter_address_old[ 'permanent_address' ] ) ? $remitter_address_old[ 'permanent_address' ] : NULL );
							
							$new_remitter_address_old -> country_id = ( isset( $remitter_address_old[ 'country' ] ) && ! empty( $remitter_address_old[ 'country' ] ) ? $remitter_address_old[ 'country' ] : NULL );
							
							$new_remitter_address_old -> state_id = ( isset( $remitter_address_old[ 'state' ] ) && ! empty( $remitter_address_old[ 'state' ] ) ? $remitter_address_old[ 'state' ] : NULL );

							$new_remitter_address_old -> city_id = ( isset( $remitter_address_old[ 'city' ] ) && ! empty( $remitter_address_old[ 'city' ] ) ? $remitter_address_old[ 'city' ] : NULL );
							
							$new_remitter_address_old -> postal_code = ( isset( $remitter_address_old[ 'postal' ] ) && ! empty( $remitter_address_old[ 'postal' ] ) ? $remitter_address_old[ 'postal' ] : NULL );
							
							$new_remitter_address_old -> tenancy_length_id = ( isset( $remitter_address_old[ 'tenancy_length_id' ] ) && ! empty( $remitter_address_old[ 'tenancy_length_id' ] ) ? $remitter_address_old[ 'tenancy_length_id' ] : NULL );
							
							$new_remitter_address_old -> is_resident = ( isset( $remitter_address_old[ 'is_resident' ] ) && ! empty( $remitter_address_old[ 'is_resident' ] ) ? TRUE : FALSE );
							
							$new_remitter_address_old -> is_preferred = ( isset( $remitter_address_old[ 'is_preferred' ] ) && ! empty( $remitter_address_old[ 'is_preferred' ] ) ? TRUE : FALSE );

							$new_remitter_address_old -> save();
							
							$remitter_address_id  = $new_remitter_address_old[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $remitter_address_old[ 'is_preferred' ] ) && ! empty( $remitter_address_old[ 'is_preferred' ] ) ) {
								
								$remitter_address_is_preferred = TRUE;
								
							}
							
						}
						
					}
					
					
					
					//DELETE RECORD
					$remove_remitter_address_ids = array_diff( $old_remitter_address_ids, $existing_remitter_address_ids );
					
					//CHECK IF REMOVE REMITTER ADDRESS IS NOT EMPTY
					if( ! empty ( $remove_remitter_address_ids ) ) {
						
						//LOOP ALL RECORD TO BE REMOVED
						foreach( $remove_remitter_address_ids as $remove_remitter_address_id ) {
							
							//FIND REMOVE REMITTER ADDRESS
							$remove_remitter_address = RemitterAddress::find( $remove_remitter_address_id );
							
							//DELETE REMOVE REMITTER ADDRESS
							$remove_remitter_address -> delete();
							
						}
						
					}
					
					
					
					//INSERT NEW RECORD
					$remitter_addresses = ( isset( $input[ 'remitter_address' ] ) && ! empty( $input[ 'remitter_address' ] ) ? array_values( $input[ 'remitter_address' ] ) : '' );
					
					//CHECK IF REMITTER ADDRESS IS NOT EMPTY
					if( ! empty ( $remitter_addresses ) ) {
						
						//LOOP ALL REMITTER ADDRESSES
						foreach( $remitter_addresses as $key => $remitter_address ) {

							$new_remitter_address = new RemitterAddress;

							$new_remitter_address -> remitter_id = $remitter_id;
							
							$new_remitter_address -> current_address = ( isset( $remitter_address[ 'current_address' ] ) && ! empty( $remitter_address[ 'current_address' ] ) ? $remitter_address[ 'current_address' ] : NULL );

							$new_remitter_address -> permanent_address = ( isset( $remitter_address[ 'permanent_address' ] ) && ! empty( $remitter_address[ 'permanent_address' ] ) ? $remitter_address[ 'permanent_address' ] : NULL );
							
							$new_remitter_address -> country_id = ( isset( $remitter_address[ 'country' ] ) && ! empty( $remitter_address[ 'country' ] ) ? $remitter_address[ 'country' ] : NULL );
							
							$new_remitter_address -> state_id = ( isset( $remitter_address[ 'state' ] ) && ! empty( $remitter_address[ 'state' ] ) ? $remitter_address[ 'state' ] : NULL );

							$new_remitter_address -> city_id = ( isset( $remitter_address[ 'city' ] ) && ! empty( $remitter_address[ 'city' ] ) ? $remitter_address[ 'city' ] : NULL );
							
							$new_remitter_address -> postal_code = ( isset( $remitter_address[ 'postal' ] ) && ! empty( $remitter_address[ 'postal' ] ) ? $remitter_address[ 'postal' ] : NULL );
							
							$new_remitter_address -> tenancy_length_id = ( isset( $remitter_address[ 'tenancy_length_id' ] ) && ! empty( $remitter_address[ 'tenancy_length_id' ] ) ? $remitter_address[ 'tenancy_length_id' ] : NULL );
							
							$new_remitter_address -> is_resident = ( isset( $remitter_address[ 'is_resident' ] ) && ! empty( $remitter_address[ 'is_resident' ] ) ? TRUE : FALSE );
							
							$new_remitter_address -> is_preferred = ( isset( $remitter_address[ 'is_preferred' ] ) && ! empty( $remitter_address[ 'is_preferred' ] ) ? TRUE : FALSE );

							$new_remitter_address -> save();
							
							$remitter_address_id  = $new_remitter_address[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $remitter_address[ 'is_preferred' ] ) && ! empty( $remitter_address[ 'is_preferred' ] ) ) {
								
								$remitter_address_is_preferred = TRUE;
								
							}
							
						}
						
					}
					
					
					
					/* 
					*------------------------------
					* REMITTER ADDRESS SAVING END
					*------------------------------
					*/
					
					
					
					
					
					/* 
					*------------------------------
					* REMITTER CONTACT SAVING START
					*------------------------------
					*/
					
					
					
					$remitter_contact_is_preferred = FALSE;
					
					$existing_remitter_contact_ids = array();
					
					$old_remitter_contact_ids = RemitterContact::where( 'remitter_id', '=', $remitter_id ) -> pluck( 'id' ) -> toArray();
					
					
					
					$remitter_contact_olds = ( isset( $input[ 'remitter_contact_old' ] ) && ! empty( $input[ 'remitter_contact_old' ] ) ? array_values( $input[ 'remitter_contact_old' ] ) : '' );
					
					//REMITTER CONTACT EXISTING
					if( ! empty( $remitter_contact_olds ) ) {
						
						//LOOP ALL REMITTER CONTACT
						foreach( $remitter_contact_olds as $key => $remitter_contact_old ) {
							
							$existing_remitter_contact_ids[] = $remitter_contact_old[ 'id' ];

							$new_remitter_contact_old = RemitterContact::find( $remitter_contact_old[ 'id' ] );

							$new_remitter_contact_old -> remitter_id = $remitter_id;
							
							$new_remitter_contact_old -> contact_type_id = ( isset( $remitter_contact_old[ 'type' ] ) && ! empty( $remitter_contact_old[ 'type' ] ) ? $remitter_contact_old[ 'type' ] : NULL );
							
							$new_remitter_contact_old -> contact = ( isset( $remitter_contact_old[ 'value' ] ) && ! empty( $remitter_contact_old[ 'value' ] ) ? $remitter_contact_old[ 'value' ] : NULL );
							
							$new_remitter_contact_old -> is_preferred = ( isset( $remitter_contact_old[ 'is_preferred' ] ) && ! empty( $remitter_contact_old[ 'is_preferred' ] ) ? TRUE : FALSE );
							
							$new_remitter_contact_old -> save();
							
							$remitter_contact_id  = $new_remitter_contact_old[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $remitter_contact_old[ 'is_preferred' ] ) && ! empty( $remitter_contact_old[ 'is_preferred' ] ) ) {
								
								$remitter_contact_is_preferred = TRUE;
								
							}
							
						}
						
					}
					
					
					
					//DELETE RECORD
					$remove_remitter_contact_ids = array_diff( $old_remitter_contact_ids, $existing_remitter_contact_ids );
					
					//CHECK IF REMOVE REMITTER CONTACT IS NOT EMPTY
					if( ! empty ( $remove_remitter_contact_ids ) ) {
						
						//LOOP ALL RECORD TO BE REMOVED
						foreach( $remove_remitter_contact_ids as $remove_remitter_contact_id ) {
							
							//FIND REMOVE REMITTER CONTACT
							$remove_remitter_contact = RemitterContact::find( $remove_remitter_contact_id );
							
							//DELETE REMOVE REMITTER CONTACT
							$remove_remitter_contact -> delete();
							
						}
						
					}
					
					
					
					$remitter_contacts = ( isset( $input[ 'remitter_contact' ] ) && ! empty( $input[ 'remitter_contact' ] ) ? array_values( $input[ 'remitter_contact' ] ) : '' );
					
					//REMITTER CONTACT EXISTING
					if( ! empty( $remitter_contacts ) ) {
						
						//LOOP ALL REMITTER CONTACT
						foreach( $remitter_contacts as $key => $remitter_contact ) {

							$new_remitter_contact = new RemitterContact;

							$new_remitter_contact -> remitter_id = $remitter_id;
							
							$new_remitter_contact -> contact_type_id = ( isset( $remitter_contact[ 'type' ] ) && ! empty( $remitter_contact[ 'type' ] ) ? $remitter_contact[ 'type' ] : NULL );
							
							$new_remitter_contact -> contact = ( isset( $remitter_contact[ 'value' ] ) && ! empty( $remitter_contact[ 'value' ] ) ? $remitter_contact[ 'value' ] : NULL );
							
							$new_remitter_contact -> is_preferred = ( isset( $remitter_contact[ 'is_preferred' ] ) && ! empty( $remitter_contact[ 'is_preferred' ] ) ? TRUE : FALSE );
							
							$new_remitter_contact -> save();
							
							$remitter_contact_id  = $new_remitter_contact[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $remitter_contact[ 'is_preferred' ] ) && ! empty( $remitter_contact[ 'is_preferred' ] ) ) {
								
								$remitter_contact_is_preferred = TRUE;
								
							}
							
						}
						
					}
					
					
					
					/* 
					*------------------------------
					* REMITTER CONTACT SAVING END
					*------------------------------
					*/
					
					
					
					
					
					/* 
					*-------------------------------------
					* REMITTER IDENTIFICATION SAVING START
					*-------------------------------------
					*/

					
					
					$remitter_identification_is_preferred = FALSE;
					
					$existing_remitter_identification_ids = array();
					
					$old_remitter_identification_ids = RemitterIdentification::where( 'remitter_id', '=', $remitter_id ) -> pluck( 'id' ) -> toArray();

					
					
					$remitter_identification_olds = ( isset( $input[ 'remitter_identification_old' ] ) && ! empty( $input[ 'remitter_identification_old' ] ) ? array_values( $input[ 'remitter_identification_old' ] ) : '' );
					
					//REMITTER IDENTIFICATION EXISTING
					if( ! empty ( $remitter_identification_olds ) ) {
						
						//LOOP ALL REMITTER IDENTIFICATION
						foreach( $remitter_identification_olds as $key => $remitter_identification_old ) {
							
							$existing_remitter_identification_ids[] = $remitter_identification_old[ 'id' ];

							$new_remitter_identification_old = RemitterIdentification::find( $remitter_identification_old[ 'id' ] );
							
							$new_remitter_identification_old -> remitter_id = $remitter_id;

							$new_remitter_identification_old -> number = ( isset( $remitter_identification_old[ 'number' ] ) && ! empty( $remitter_identification_old[ 'number' ] ) ? $remitter_identification_old[ 'number' ] : NULL );

							$new_remitter_identification_old -> expiration_date = ( isset( $remitter_identification_old[ 'expiration' ] ) && ! empty( $remitter_identification_old[ 'expiration' ] ) ? $remitter_identification_old[ 'expiration' ] : NULL );

							$new_remitter_identification_old -> filename = ( isset( $remitter_identification_old[ 'file' ] ) && ! empty( $remitter_identification_old[ 'file' ] ) ? $remitter_identification_old[ 'file' ] : NULL );

							//CHECK IF REMITTER IDENTIFICATION FILE IS ISSET AND NOT EMPTY
							if( isset( $remitter_identification_old[ 'file' ] ) && ! empty( $remitter_identification_old[ 'file' ] ) ) {
								
								//GET REMITTER IDENTIFICATION DETAILS
								$remitter_identification_details = RemitterIdentification::where( 'id', '=', $remitter_identification_old[ 'id' ] ) -> first( [ 'filename' ] );
								
								$remitter_identification_file = $remitter_identification_details[ 'filename' ];
								
								//CHECK IF REMITTER IDENFICATION FILE IS NOT EMPTY
								if( ! empty( $remitter_identification_file ) && ! is_null( $remitter_identification_file ) ) {
									
									//CHECK IF REMITTER IDENFICATION FILE IS NOT EQUAL TO INPUT REMITTER IDENTIFICATION FILE
									if( trim( $remitter_identification_file ) != trim( $remitter_identification_old[ 'file' ] ) ) {
										
										//DELETE THE EXISTING REMITTER IDENFICATION FILE
										File::delete( $this -> remitter_id_path . '/' . $remitter_identification_file );
										
										//TRANSFER FILE FROM TEMP TO ORIGINAL FOLDER
										File::move( $this -> remitter_id_temp_path . '/' . $remitter_identification_old[ 'file' ], $this -> remitter_id_path . '/' . $remitter_identification_old[ 'file' ] );

									}
								
								}
								
								//REMITTER IDENTIFICATION FILE IS EMPTY
								else {
									
									//TRANSFER FILE FROM TEMP TO ORIGINAL FOLDER
									File::move( $this -> remitter_id_temp_path . '/' . $remitter_identification_old[ 'file' ], $this -> remitter_id_path . '/' . $remitter_identification_old[ 'file' ] );

								}

							}

							$new_remitter_identification_old -> identification_type_id = ( isset( $remitter_identification_old[ 'type' ] ) && ! empty( $remitter_identification_old[ 'type' ] ) ? $remitter_identification_old[ 'type' ] : NULL );
							
							$new_remitter_identification_old -> other_type = ( isset( $remitter_identification_old[ 'other_type' ] ) && ! empty( $remitter_identification_old[ 'other_type' ] ) ? $remitter_identification_old[ 'other_type' ] : NULL ) ;
							
							$new_remitter_identification_old -> is_preferred = ( isset( $remitter_identification_old[ 'is_preferred' ] ) && ! empty( $remitter_identification_old[ 'is_preferred' ] ) ? TRUE : FALSE );

							$new_remitter_identification_old -> save();

							$remitter_identification_id  = $new_remitter_identification_old[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $remitter_identification_old[ 'is_preferred' ] ) && ! empty( $remitter_identification_old[ 'is_preferred' ] ) ) {
								
								$remitter_identification_is_preferred = TRUE;
								
							}
							
						}

					}
					
					
					
					//DELETE RECORD
					$remove_remitter_identification_ids = array_diff( $old_remitter_identification_ids, $existing_remitter_identification_ids );
					
					//CHECK IF REMOVE REMITTER IDENTIFICATION IS NOT EMPTY
					if( ! empty ( $remove_remitter_identification_ids ) ) {
						
						//LOOP ALL RECORD TO BE REMOVED
						foreach( $remove_remitter_identification_ids as $remove_remitter_identification_id ) {
							
							//GET OLD REMITTER IDENTIFICATION DETAILS
							$remitter_identification_details = RemitterIdentification::where( 'id', '=', $remove_remitter_identification_id ) -> first( [ 'filename' ] );
							
							$remitter_identification_file = $remitter_identification_details[ 'file' ];
							
							//CHECK IF REMITTER IDENFICATION FILE IS NOT EMPTY
							if( ! empty( $remitter_identification_file ) ) {
								
								//DELETE THE EXISTING REMITTER AVATAR
								File::delete( $this -> remitter_id_path . '/' . $remitter_identification_file );
								
							}
							
							//FIND REMOVE REMITTER IDENTIFICATION
							$remove_remitter_identification = RemitterIdentification::find( $remove_remitter_identification_id );
							
							//DELETE REMOVE REMITTER IDENTIFICATION
							$remove_remitter_identification -> delete();
							
						}
						
					}
					
					
					
					$remitter_identifications = ( isset( $input[ 'remitter_identification' ] ) && ! empty( $input[ 'remitter_identification' ] ) ? array_values( $input[ 'remitter_identification' ] ) : '' );
					
					//REMITTER IDENTIFICATION
					if( ! empty ( $remitter_identifications ) ) {
						
						//LOOP ALL REMITTER IDENTIFICATION
						foreach( $remitter_identifications as $key => $remitter_identification ) {

							$new_remitter_identification = new RemitterIdentification;
							
							$new_remitter_identification -> remitter_id = $remitter_id;

							$new_remitter_identification -> number = ( isset( $remitter_identification[ 'number' ] ) && ! empty( $remitter_identification[ 'number' ] ) ? $remitter_identification[ 'number' ] : NULL );

							$new_remitter_identification -> expiration_date = ( isset( $remitter_identification[ 'expiration' ] ) && ! empty( $remitter_identification[ 'expiration' ] ) ? $remitter_identification[ 'expiration' ] : NULL );

							$new_remitter_identification -> filename = ( isset( $remitter_identification[ 'file' ] ) && ! empty( $remitter_identification[ 'file' ] ) ? $remitter_identification[ 'file' ] : NULL );

							$new_remitter_identification -> identification_type_id = ( isset( $remitter_identification[ 'type' ] ) && ! empty( $remitter_identification[ 'type' ] ) ? $remitter_identification[ 'type' ] : NULL );
							
							$new_remitter_identification -> other_type = ( isset( $remitter_identification[ 'other_type' ] ) && ! empty( $remitter_identification[ 'other_type' ] ) ? $remitter_identification[ 'other_type' ] : NULL ) ;
							
							$new_remitter_identification -> is_preferred = ( isset( $remitter_identification[ 'is_preferred' ] ) && ! empty( $remitter_identification[ 'is_preferred' ] ) ? TRUE : FALSE );

							$new_remitter_identification -> save();

							$remitter_identification_id  = $new_remitter_identification[ 'id' ];
							
							//CHECK IF IS PREFERRED IS IN THIS LOOP
							if( isset( $remitter_identification[ 'is_preferred' ] ) && ! empty( $remitter_identification[ 'is_preferred' ] ) ) {
								
								$remitter_identification_is_preferred = TRUE;
								
							}
							
							//CHECK IF FILE IS ISSET AND NOT EMPTY
							if( isset( $remitter_identification[ 'file' ] ) && ! empty( $remitter_identification[ 'file' ] ) ) {
								
								//TRANSFER FILE FROM TEMP TO ORIGINAL FOLDER
								File::move( $this -> remitter_id_temp_path . '/' . $remitter_identification[ 'file' ], $this -> remitter_id_path . '/' . $remitter_identification[ 'file' ] );

							}

						}

					}
					
					
					
					/* 
					*-------------------------------------
					* REMITTER IDENTIFICATION SAVING END
					*-------------------------------------
					*/
					
					
					
					
					
					//ADD SOME NOTE COMING FROM CURRENT LOGGED IN USER
					if( isset( $input[ 'remitter_added_note' ] ) && ! empty( $input[ 'remitter_added_note' ] ) ) {
						
						//CHECK IF REMITTER ID IS NOT EMPTY
						if( ! empty( $input[ 'remitter_id' ] ) ) {
							
							$new_remitter_note = RemitterNote::where( 'remitter_id', '=', $input[ 'remitter_id' ] ) -> first();
							
							//CHECK IF NEW REMITTER NOTE IS EMPTY
							if( empty( $new_remitter_note ) ) {

								$new_remitter_note = new RemitterNote;

							}
							
						}
						
						else {
							
							$new_remitter_note = new RemitterNote;

						}
						
						$new_remitter_note -> remitter_id = $remitter_id;
						
						$new_remitter_note -> note = ( isset( $input[ 'remitter_added_note' ] ) && ! empty( $input[ 'remitter_added_note' ] ) ? trim( $input[ 'remitter_added_note' ] ) : NULL );
						
						$new_remitter_note -> created_by = Auth::user()->ID;
						
						$new_remitter_note -> save();
						
						$remitter_note_id = $new_remitter_note[ 'id' ];
						
					}
					
					
					
					
					
					//COMMIT ALL TRANSACTION
					DB::commit();
					
					
					
					$remitter_table_template = view( 'tables.remitter_table',
					
						array( 'remitters' => $this -> getRemitters() )
						
					)
					
					-> render();
					
					
					
					//SHOW SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'remitter_id' => $remitter_id, 'remitter_table_template' => $remitter_table_template, 'action' => $action ) ] );
					
					
					
				}
				
				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
			
			}
			
		}
		
	}
	
	public function deleteRemitters( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {
				
				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					
		
					//CHECK IF REMITTER IS ISSET AND NOT EMPTY
					if( isset( $input[ 'remitter' ] ) && ! empty( $input[ 'remitter' ] ) && count( $input[ 'remitter' ] ) ) {
						
						//LOOP ALL REMITTER
						foreach( $input[ 'remitter' ] AS $key => $remitter ) {
							
							//REMOVE REMITTER
							$remove_remitter = Remitter::find( $remitter ) -> delete();
							
						}
						
					}
					
					
					//COMMIT ALL TRANSACTION
					DB::commit();
					
					
					//GET ALL REMITTER TABLES
					$remitter_table_template = view( 'tables.remitter_table',
					
						array( 'remitters' => $this -> getRemitters() )
						
					)
					
					-> render();
					
					//SEND SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'total_remitter' => count( $input[ 'remitter' ] ), 'remitter_table_template' => $remitter_table_template, 'action' => 'delete' ) ] );
					
					
				
			}
			
				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
				
			}

		}
		
	}
	
	
	private function getRemitters() {
		
		//REMOVE PAGE FROM SEARCH QUERY
		UNSET( $_GET[ 'page' ] );
		
		//SEARCH QUERY IS ISSET AND NOT EMPTY
		if( isset( $_GET ) && ! empty( array_filter( $_GET ) ) ) {
			
			$search_query = $this -> searchParameter();
			
			$remitters = DB::table( 'remitters' )

					-> leftJoin( 'remitter_contacts', function( $join ) {
								
						$join -> on( 'remitter_contacts.remitter_id', '=', 'remitters.id' );
						
						$join -> on( 'remitter_contacts.is_preferred', '=', DB::Raw( 1 ) );
						
					} )

					-> leftJoin( 'remitter_identifications','remitters.id','=','remitter_identifications.remitter_id' )

					-> leftJoin( 'library_identification_types','library_identification_types.id','=','remitter_identifications.identification_type_id' )

					-> select( DB::Raw( '

						DISTINCT

						crm_remitters.id AS remitter_id,

						crm_remitters.firstname AS remitter_firstname,
						
						crm_remitters.lastname AS remitter_lastname,

						crm_remitters.email AS remitter_email, 

						crm_remitter_contacts.contact AS remitter_contact, 

						crm_remitters.email_verified AS remitter_email_verified, 

						crm_remitters.sms_verified AS remitter_sms_verified, 

						crm_remitter_identifications.number AS remitter_id_number, 

						crm_library_identification_types.name AS remitter_id_type, 

						crm_remitter_identifications.expiration_date AS remitter_id_expiration_date

					' ) )
					
					-> whereRaw( $search_query )
					
					-> whereNull( 'remitters.deleted_at' )
					
					-> orderBy( 'remitters.id','DESC' )
					
					-> groupBy( 'remitters.id' )
					
					-> paginate( 20 )

					-> setPath( 'remitters' );
			
		}
		
		else {
		
			$remitters = DB::table( 'remitters' )

					-> leftJoin( 'remitter_contacts', function( $join ) {
								
						$join -> on( 'remitter_contacts.remitter_id', '=', 'remitters.id' );
						
						$join -> on( 'remitter_contacts.is_preferred', '=', DB::Raw( 1 ) );
						
					} )

					-> leftJoin( 'remitter_identifications','remitters.id','=','remitter_identifications.remitter_id' )

					-> leftJoin( 'library_identification_types','library_identification_types.id','=','remitter_identifications.identification_type_id' )

					-> select( DB::Raw( '

						DISTINCT

						crm_remitters.id AS remitter_id,

						crm_remitters.firstname AS remitter_firstname,
						
						crm_remitters.lastname AS remitter_lastname,

						crm_remitters.email AS remitter_email, 

						crm_remitter_contacts.contact AS remitter_contact, 

						crm_remitters.email_verified AS remitter_email_verified, 

						crm_remitters.sms_verified AS remitter_sms_verified, 

						crm_remitter_identifications.number AS remitter_id_number, 

						crm_library_identification_types.name AS remitter_id_type, 

						crm_remitter_identifications.expiration_date AS remitter_id_expiration_date

					' ) )

					-> whereNull( 'remitters.deleted_at' )

					-> orderBy( 'remitters.id','DESC' )
					
					-> groupBy( 'remitters.id' )

					-> paginate( 20 )

					-> setPath( 'remitters' );
				
		}

		return $remitters;
		
	}

	private function searchParameter() {
		
		//CONSTRUCT QUERY STRING
		$query_string = array();
		
		//CHECK IF GET IS SET AND NOT EMPTY
		if( isset( $_GET ) && ! empty( $_GET ) ) {
			
			//REMITTER FIRSTNAME
			if( isset( $_GET[ 'remitter_firstname' ] ) && ! empty( $_GET[ 'remitter_firstname' ] ) ) {
				
				$query_string[] = " crm_remitters.firstname LIKE '%{$_GET[ 'remitter_firstname' ]}%' ";
				
			}
			
			//REMITTER LASTNAME
			if( isset( $_GET[ 'remitter_lastname' ] ) && ! empty( $_GET[ 'remitter_lastname' ] ) ) {
				
				$query_string[] = " crm_remitters.lastname LIKE '%{$_GET[ 'remitter_lastname' ]}%' ";
				
			}
			
			//REMITTER EMAIL
			if( isset( $_GET[ 'remitter_email' ] ) && ! empty( $_GET[ 'remitter_email' ] ) ) {
				
				$query_string[] = " crm_remitters.email LIKE '%{$_GET[ 'remitter_email' ]}%' ";
				
			}
			
			//REMITTER CONTACT
			if( isset( $_GET[ 'remitter_contact' ] ) && ! empty( $_GET[ 'remitter_contact' ] ) ) {
				
				$query_string[] = " crm_remitter_contacts.contact LIKE '%{$_GET[ 'remitter_contact' ]}%' ";
				
			}
			
			//REMITTER EMAIL VERIFIED
			if( isset( $_GET[ 'remitter_email_verified' ] ) && ! empty( $_GET[ 'remitter_email_verified' ] ) ) {
				
				$query_string[] = " crm_remitters.email_verified = 1 ";
				
			}
			
			//REMITTER SMS VERIFIED
			if( isset( $_GET[ 'remitter_sms_verified' ] ) && ! empty( $_GET[ 'remitter_sms_verified' ] ) ) {
				
				$query_string[] = " crm_remitters.sms_verified = 1 ";
				
			}
			
			//RETURN SEARCH QUERY
			return implode( ' AND ', array_filter( $query_string ) );

		}
		
	}
	
	
}
