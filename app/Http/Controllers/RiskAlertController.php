<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Remittance; 
use App\Models\Remitter;
use App\Models\Recipient;
use Maatwebsite\Excel\Facades\Excel;

class RiskAlertController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        return view( 'riskalert' );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get( Request $request )
    {

        $requestType = $request->queryType;

        $response = '';

        switch( $requestType )
        {

            case 'getMultipleRemittance':

                $response = $this -> getMultipleRemittance( $request );

                break;

            case 'getRemittanceToSingleRecipient':

                $response = $this -> getRemittanceToSingleRecipient( $request );

                break;

            case 'getAllRemittances':

                $response = $this -> getAllRemittances( $request );

                break;

            case 'getRemitterRemittances':

                $response = $this -> getRemitterDatedRemittances( $request );

                break;    

            case 'getRecipientRemittances':

                $response = $this -> getRecipientDatedRemittances( $request );

                break;  

            // case 'exportRemitterRemittance': 

            //     $response = $this->exportRemitterRemittance( $request );    

            //     break;

            // case 'exportRecipientRemittance':
            
            //     $response = $this->exportRecipientRemittance( $request );   

            //     break;        

            default:     

        }

        return $response;  

    }

    public function checkEmpty( $data )
    {

        return ( isset( $data ) || empty( $data ) ) ? $data : '';

    }

    public function getAllRemittances( Request $request )
    {

        if( $request -> ajax() )
        {

            $startDate = $request->startdate;

            $endDate = $request->enddate;

            $remittanceResult = $this->getRemittancesBetweenDates( $startDate, $endDate );

            $view = view( 'tables.riskAlert.remitterstransactions' )

                    ->with( 'remittanceResult', $remittanceResult )

                    ->render()

            ;

            $response = [ 'success' => true

                          ,


                          'remittersmultipletransaction' => $view

                        ]

            ;

            return response() 

                   -> json( $response )

            ;

        }

    }

    public function getMultipleRemittance( Request $request )
    {

        $startDate = $request->startdate;

        $endDate = $request->enddate;

        $remittanceResult = $this->getRemittancesBetweenDates( $startDate, $endDate );

        $multipleRemittances = $this->getRemittersMultipleRemittances( $remittanceResult );

        if( $request -> ajax() )
        {

            $view = view( 'tables.riskAlert.remitterstransactions' )

                    ->with( 'multipleRemittances', $multipleRemittances )

                    ->render()

            ;

            $response = [ 

                          'success' => true

                          ,


                          'remittersmultipletransaction' => $view

                        ]

            ;

            return response() 

                   -> json( $response )

            ;

        }

        return view( 'riskalert' )

               ->with( 'multipleRemittances', $multipleRemittances )

        ;

    }

    public function getRemittanceToSingleRecipient( Request $request )
    {

        $startDate = $request->startdate;

        $endDate = $request->enddate;

        $remittanceResult = $this->getRemittancesBetweenDates( $startDate, $endDate );

        $recipientRecievedTransactions = $this->getRecipientRecievedTransactions( $remittanceResult );

        if( $request -> ajax() )
        {

          $view = view( 'tables.riskAlert.remittanceToSingleRecipientTable' )

                  ->with( 'remittanceToSingleRecipient', $recipientRecievedTransactions )

                  ->render()

          ;

          $response = [ 

                        'success' => true

                        ,


                        'remittanceToSingleRecipient' => $view

                      ]

          ;

          return response() 

                 -> json( $response )

          ;

        }

        return view( 'riskalert' )

               ->with( 'remittanceToSingleRecipient', $recipientRecievedTransactions )

        ;

    }

    private function getRemitterDatedRemittances( Request $request )
    {          
        
        $startDate = $request->startdate;

        $endDate = $request->enddate;

        $remittanceResult = $this -> getRemittancesBetweenDates( $startDate, $endDate )

                                  -> select( 'id', 'transaction_number', 'remitter_id', 'recipient_id', 'amount_sent', 'bank_source_id', 'date_transferred', 'reason_id', 'source_of_fund_id' ) 

                                  -> where( 'remitter_id', $request->remitterId )

                                  -> where( 'status_id', 24 ) //24 for Transfer Complete

                                  -> orderBy( 'date_transferred', 'DESC' )

                                  -> paginate( 15 ) //Paginate data first before processing related models
        
        ;
       
        /**
         * Append related models
         */
        foreach( $remittanceResult as $remittance )
        {

          $this->appendRecipientInfo( $remittance->recipient ); 

          $remittance->remitter;

          $this->checkEmpty( $remittance->source_of_fund_id ) ? $remittance->source_of_fund : 'N/A';

          $this->checkEmpty( $remittance->reason_id ) ? $remittance->reason : 'N/A';

          // $remittance->bank_source;

        }

        if( $request -> ajax() )
        {

          $view = view( 'tables.riskAlert.remittertable' )

                  -> with( 'remitterRemittances', $remittanceResult )

                  ->render() 

          ;

          $response = [ 

              'success' => true

              ,


              'remitterRemittances' => $view,'res'=>$remittanceResult

            ]

          ;

          return response()

                 -> json( $response ) 

          ;

        }

        return view( 'tables.riskAlert.remittertable' )

               ->with( 'remitterRemittances', $remittanceResult -> paginate( 15 ) )

        ;

    }

    //End of getRemitterDatedRemittances() method
    

    private function getRecipientDatedRemittances( Request $request )
    {          

        $startDate = $request->startdate;

        $endDate = $request->enddate;

        $remittanceResult = $this->getRemittancesBetweenDates( $startDate, $endDate );

        $remittanceResult = $remittanceResult 

                            -> select( 'id', 'transaction_number', 'remitter_id', 'recipient_id', 'amount_sent', 'status_id', 'date_transferred' )

                            -> where( 'recipient_id', $request->recipientId )

                            -> where( 'status_id', 24 ) //24 for Transfer Complete

                            -> orderBy( 'date_transferred', 'DESC' )

                            -> paginate( 15 )

        ;

        /**
         * Append related models
         */
        foreach( $remittanceResult as $remittance )
        {

          $this->appendRecipientInfo( $remittance->recipient ); 

          $remittance->remitter;

          $this->checkEmpty( $remittance->source_of_fund_id ) ? $remittance->source_of_fund : 'N/A';

          $this->checkEmpty( $remittance->reason_id ) ? $remittance->reason : 'N/A';

          // $remittance->bank_source;

        }

        if( $request -> ajax() )
        {

          $view = view( 'tables.riskAlert.recipientModalTable' )

                  -> with( 'recipientRemittances', $remittanceResult )

                  ->render() 

          ;

          $response = [ 

              'success' => true

              ,
              

              'recipientRemittances' => $view
 
            ]

          ;

          return response()

                 -> json( $response ) 

          ;

        }

        return view( 'tables.riskAlert.recipientModalTable' )

               ->with( 'recipientRemittances', $remittanceResult )

        ;

    }  

    private function getRemittancesBetweenDates( $start_date, $end_date )
    {

        $startDate = $this->checkEmpty( $start_date );

        $endDate = $this->checkEmpty( $end_date );

        $startDate = !empty( $startDate ) ? date( 'Y-m-d', strtotime( $startDate ) ) : 'Invalid start date';

        $endDate = !empty( $endDate ) ? date( 'Y-m-d', strtotime( $endDate ) ) : 'Invalid end date';

        $remittance = [];

        if( $startDate != 'Invalid start date' || $endDate != 'Invalid end date' )
        {

            $remittance = Remittance::where( 'status_id', 24 ); //24 for Transfer Complete

            /**
             * whereBetween can't process the same date
             * Checking if the same day
             */
            if( $startDate == $endDate )
            {

                $remittance -> whereDate( 'date_transferred', '=', $startDate );

            }

            else
            {

                $remittance -> whereBetween( 'date_transferred', [ $startDate, $endDate ] );

            }

        }

        return $remittance;

    }

    private function getRemittersMultipleRemittances( $remittances )
    {
                
      $remittanceResult = $remittances 

                          -> selectRaw( 'remitter_id, max(date_transferred) as dateTransferred, COUNT(date_transferred) as transactionsMade' )

                          -> groupBy( 'remitter_id' )
            
                          -> havingRaw( 'COUNT(remitter_id) > 1' )

                          -> orderBy( 'dateTransferred', 'DESC' ) 
                          
                          -> paginate( 15 )

      ;

      //$this->appendRelatedModels( $remittanceResult );
      
      $remittanceResult = $this->appendRemittersName( $remittanceResult );

      return $remittanceResult;

    }

    /**
     * Extract the remittances to a single recipient from the result of getRemittancesBetweenDates
     * @param  Remittance $remittances from the result of $remittancesBetweenDates
     * @return array Remittances remittances with recipient person attached
     */
    private function getRecipientRecievedTransactions( $remittances )
    {

      $remittanceResult = $remittances 

                          -> selectRaw( 'recipient_id, max(date_transferred) as dateTransferred, count(recipient_id) as recipient_count') 

                          -> groupBy( 'remitter_id', 'recipient_id' )
        
                          -> havingRaw( 'COUNT(recipient_id) > 1' )

                          ->orderBy( 'dateTransferred', 'DESC' )

                          -> paginate( 15 )

      ;

      foreach( $remittanceResult as $remittance )
      {

        $this->appendRecipientInfo( $remittance->recipient ); 

      }

      return $remittanceResult;

    }

    /**
     * Extract recipients from given remitter from the result of getRemittancesBetweenDates
     * @param  int $remitter_id, Remittance $remittancesBetweenDates
     * @param  [date] $remittancesBetweenDates remittances from and to specific dates recipients are extracted from
     * @return array recipientRemittances array of remittances
     */
    public function getRemittancesFromSingleRemitter( $remitter_id, $remittancesBetweenDates )
    {

      $recipientRemittances = [];

      foreach( $remittancesBetweenDates as $remittance )
      {

        if( $remittance->remitter_id == $remitter_id)
        {

          array_push( $recipientRemittances, $remittance );

        } 

      }

      return $recipientRemittances;

    }

    /**
     * Extract recipients from given remitter from the result of getRemittancesBetweenDates
     * @param  Remittance $remittancesBetweenDates remittances from and to specific dates recipients are extracted from
     * @return array recipientRemittances array of remittances
     */
    private function getRemittersToSingleRecipient( $remittanceResult, Request $request )
    {

      $startDate = $request->startdate;

      $endDate = $request->enddate;

      $ids = $remittanceResult -> selectRaw('id, remitter_id, recipient_id, max(date_transferred) as date_transferred, count(recipient_id) as recipient_count') 

                               -> groupBy( 'remitter_id', 'recipient_id' )
        
                               -> havingRaw( 'COUNT(*) > 1' )

                               -> get()

      ;

      $ids = json_decode( $ids );

      $remitter_ids = array_column( $ids, 'remitter_id' );

      $recipient_ids = array_column( $ids, 'recipient_id' );

      $remitter_recipients = array( );

      $remittancesOnSingleRecipient = $this->getRemittancesBetweenDates( $startDate, $endDate )
      
                                      -> whereIn( 'remitter_id', $remitter_ids )

                                      -> whereIn( 'recipient_id', $recipient_ids) 
                              
                                      -> get()

      ;     

      foreach( $remitter_ids as $id )
      {

        $remittanceRecipients = $this->getRemittancesFromSingleRemitter( $id, $remittancesOnSingleRecipient );

        foreach( $remittanceRecipients as $remittance )
        {

          if( $this->checkEmpty( $remittance->recipient) )
          {

            $this->appendRecipientInfo( $remittance->recipient ); 
              
            $remitter_recipients[$id][] = $remittance;

          }

        }

      }

      return $remitter_recipients;

    }

    //not currently used because of slow back end response
    private function appendRelatedModels( $remittanceResult )
    {

        if( $this->checkEmpty( $remittanceResult ) )
        {

            /**
             *  Append related tables
             */
            foreach( $remittanceResult as $remittance )
            {

                if( $this->checkEmpty( $remittance ) )
                {
                  
                  $remittance->remitter;

                }

                //Not needed in UI as of the moment
                // if( $this->checkEmpty( $remittance->recipient ) )
                // {
                  
                //   $this->appendRecipientInfo( $remittance->recipient );

                // }

                //Not needed in UI as of the moment
                //$remittance->bank_source;

                //Not needed in UI as of the moment
                //$remittance->status;

            }

        }

        return $remittanceResult;

    }

    private function appendRemittersName( $remittanceResult )
    {

        $arr_result = array();

        if( $this->checkEmpty( $remittanceResult ) )
        {

          //$arr_remittance = json_decode( $remittanceResult );
          $arr_remittance = $remittanceResult->toArray();

          $remitter_ids = array_column( $arr_remittance['data'], 'remitter_id' );

          $remitters = Remitter::select( 'id', 'firstname', 'lastname' )

                                 -> whereIn( 'id', $remitter_ids )

                                 -> get( )

          ;

          foreach( $remittanceResult as $data )
          {

            $remitter_id = $data[ 'remitter_id' ];

            foreach( $remitters as $remitter )
            {

              if( $remitter->id == $remitter_id )
              {

                //Attach fullname 
                $data[ 'fullname' ] = $remitter->firstname.' '.$remitter->lastname;

              }

            }

          }

        }

        return $remittanceResult;

    }

    //This method should be attached in Recipient model
    private function appendRecipientInfo( $recipient )
    {

      /*
       * If RecipientPerson
       */  
      if( $this->checkEmpty( $recipient->type ) && $recipient->type == 'person' )
      {

        if( $this->checkEmpty( $recipient->recipientPerson ) )
        {

          $recipient[ "fullname" ] = $recipient->recipientPerson->firstname

                                   .

                                   ' '

                                   .

                                   $recipient->recipientPerson->lastname

          ;

          $recipient->relationship;

        }

      }

      /*
       * If RecipientCompany
       */
      else if( $recipient->type == 'company' )
      {

        $recipient->recipientCompany;

      }

    }

    public function convertToMultipleRemittance( $remittance )
    {

        return $remittance;

    }

    /**
     * Creates .xls file to be exported
     * @param  string $filename name of the file
     * @param  array $data content of the .xls file
     * @return [type]           [description]
     */
    public function exportExcel( $filename, $data )
    {

        \Excel::create( $filename, function($excel) use ( $filename, $data )
        {
        
            $excel->sheet( $filename, function($sheet) use ( $data )
            {
                
              $sheet->fromArray( $data, null, 'A1', false, false );

              //Format cells
              //Merge cells for document header
              $sheet->mergeCells( 'A1:Q1' );

              //Merge cells for document header
              $sheet->mergeCells( 'R1:Z1' );

              //Align text to center
              $sheet->getStyle( 'A1' )->getAlignment()->applyFromArray( array( 'horizontal' => 'center' ) );

              //Align text to center
              $sheet->getStyle( 'R1' )->getAlignment()->applyFromArray( array( 'horizontal' => 'center' ) );

              //Align text to center
              $sheet->getStyle( 'AA1' )->getAlignment()->applyFromArray( array( 'horizontal' => 'center' ) );

              $sheet->setOrientation('landscape');

            });

        })->download('xls');

    }

    public function exportRemitterRemittance( $startDate, $endDate, $remitterId )
    {

        $remittanceResult = $this->getRemittancesBetweenDates( $startDate, $endDate )

                            -> select( 'id', 'transaction_number', 'remitter_id', 'recipient_id', 'amount_sent', 'status_id', 'date_transferred' )

                            -> where( 'remitter_id', $remitterId )

                            -> where( 'status_id', 24 ) //24 for Transfer Complete

                            -> orderBy( 'date_transferred', 'DESC' )

                            -> get( )

        ;

        $remitterRemittances = $this->getExportedContent( $remittanceResult );

        $remitter = $remittanceResult->first()->remitter;

        $remitterFullName = $this->checkEmpty( $remitter ) ? $remitter->firstname.' '.$remitter->lastname : '';      

        $this->exportExcel( $remitterFullName, $remitterRemittances );

    }

    public function exportRecipientRemittance( $startDate, $endDate, $recipientId )
    {

        $remittanceResult = $this->getRemittancesBetweenDates( $startDate, $endDate )

                            -> select( 'id', 'transaction_number', 'remitter_id', 'recipient_id', 'amount_sent', 'status_id', 'date_transferred' )

                            -> where( 'recipient_id', $recipientId )

                            -> where( 'status_id', 24 ) //24 for Transfer Complete

                            -> orderBy( 'date_transferred', 'DESC' )

                            -> get( )

        ;

        $recipientRemittances = $this->getExportedContent( $remittanceResult );

        $recipientPerson = $remittanceResult->first()->recipient->recipientPerson;

        $fullname = $recipientPerson->firstname.' '.$recipientPerson->lastname;        

        $this->exportExcel( $fullname, $recipientRemittances );

    }

    private function getExportedContent( $remittances )
    {

        $content = array( $this->getExcelHeader( ) );

        //Append header titles
        array_push( $content, $this->getExcelTitles( ) );
       
        /**
         * Append related models
         */
        foreach( $remittances as $remittance )
        {

          $this->appendRecipientInfo( $remittance->recipient ); 

          $remitter = $remittance->remitter;

          $recipient = $remittance->recipient;

          $this->checkEmpty( $remittance->source_of_fund_id ) ? $remittance->source_of_fund : 'N/A';

          $this->checkEmpty( $remittance->reason_id ) ? $remittance->reason : 'N/A'; 

          $recipientFullName = $recipient[ "fullname" ];

          $transactionReferenceNumber = $remittance->transaction_number;

          $remitterFullName = $this->checkEmpty( $remitter ) ? $remitter->firstname.' '.$remitter->lastname : 'N/A';

          $amount = $remittance->amount_sent;

          $sourceOfFund = $this->checkEmpty( $remittance->source_of_fund ) ? $remittance->source_of_fund->name : 'N/A' ;

          $reason = $this->checkEmpty( $remittance->reason_id ) ? $remittance->reason->name : 'N/A' ; 

          $dateTransaction = $remittance->date_transferred; 

          $remitterBirthday = $this->checkEmpty( $remitter ) ? $remitter->birthday : 'N/A' ;

          $remitterAddress = $this->checkEmpty( $remitter ) ? $remitter->address : '' ;

          $remitterCurrentAddress = $this->checkEmpty( $remitterAddress ) ? $remitterAddress->current_address : 'N/A' ;

          $remitterCity = $this->checkEmpty( $remitterAddress ) ? $remitterAddress->city : '' ;

          $remitterCityName = $this->checkEmpty( $remitterCity ) ? $remitterCity->name : 'N/A' ;
                              
          $remitterState = $this->checkEmpty( $remitterAddress ) ? $remitterAddress->state : '' ;

          $remitterStateName = $this->checkEmpty( $remitterState ) ? $remitterState->name : 'N/A' ;          

          $remitterPostalCode = $this->checkEmpty( $remitterAddress ) ? $remitterAddress->postal_code : 'N/A';
    
          $remitterCountry = $this->checkEmpty( $remitterAddress ) ? $remitterAddress->country : '' ;

          $remitterEnCountryShortName = $this->checkEmpty( $remitterCountry ) ? $remitterCountry->en_short_name : '' ;

          $remitterContact = $this->checkEmpty( $remitter ) ? $remitter->contact : '' ;

          $remitterPhoneNumber = $this->checkEmpty( $remitterContact ) ? $remitterContact->contact : 'N/A' ;

          $remitterEmail = $this->checkEmpty( $remitter ) ? $remitter->email : 'N/A' ;

          $remitterIdType = $this->checkEmpty( $remitter ) ? $remitter->idType : '' ;

          $remitterIdTypeName = $this->checkEmpty( $remitterIdType ) ? $remitterIdType->name : 'N/A' ;

          $remitterIdNumber = $this->checkEmpty( $remitter ) ? $remitter->id_number : 'N/A' ;

          $recipientPerson = $this->checkEmpty( $recipient ) ? $recipient->recipientPerson : '' ;

          $recipientRelationship = $this->checkEmpty( $recipient ) ? $recipient->relationship : '' ;

          $relationshipName = $this->checkEmpty( $recipientRelationship ) ? $recipientRelationship->name : 'N/A' ;

          $recipientBirthday = $this->checkEmpty( $recipientPerson ) ? $recipientPerson->birthday : 'N/A' ;

          $recipientAddress = $this->checkEmpty( $recipient ) ? $recipient->address : '' ;

          $recipientCurrentAddress = $this->checkEmpty( $recipientAddress ) ? $recipientAddress->current_address : '' ;

          $recipientCity = $this->checkEmpty( $recipientAddress ) ? $recipientAddress->city : '' ;

          $recipientCityName = $this->checkEmpty( $recipientCity ) ? $recipientCity->name : 'N/A' ;

          $recipientState = $this->checkEmpty( $recipientAddress ) ? $recipientAddress->state : '' ;

          $recipientStateName = $this->checkEmpty( $recipientState ) ? $recipientState->name : 'N/A' ;

          $recipientPostalCode = $this->checkEmpty( $recipientAddress ) ? $recipientAddress->postal_code : 'N/A' ;

          $recipientCountry = $this->checkEmpty( $recipientAddress ) ? $recipientAddress->country : '';

          $recipientCountryName = $this->checkEmpty( $recipientCountry ) ? $recipientCountry->en_short_name : 'N/A' ;

          $recipientContact = $this->checkEmpty( $recipient ) ? $recipient->recipientContact : '' ;

          $recipientContactNumber = $this->checkEmpty( $recipientContact ) ? $recipientContact->contact : 'N/A' ;

          $recipientEmail = $this->checkEmpty( $recipient ) ? $recipient->email : '' ;

          $remittanceData = array
                            ( 

                              $dateTransaction

                              ,

                              'AUD' //PJ: this is hardcoded for now, since all transactions are in AUD. No data from DB as of 1/18/2018

                              ,

                              $amount

                              ,

                              $sourceOfFund  

                              ,

                              $transactionReferenceNumber

                              ,

                              $remitterFullName

                              ,

                              $remitterBirthday

                              ,

                              $remitterCurrentAddress

                              ,

                              $remitterCityName

                              ,

                              $remitterStateName

                              ,

                              $remitterPostalCode

                              ,

                              $remitterEnCountryShortName

                              ,

                              $remitterPhoneNumber

                              ,

                              $remitterEmail

                              ,

                              $remitterIdTypeName

                              ,

                              $remitterIdNumber  

                              ,

                              $relationshipName

                              ,

                              $recipientFullName

                              ,

                              $recipientBirthday
                              
                              ,

                              $recipientCurrentAddress

                              ,

                              $recipientCityName

                              ,

                              $recipientStateName

                              , 

                              $recipientPostalCode

                              ,

                              $recipientCountryName

                              ,

                              $recipientContactNumber

                              ,

                              $recipientEmail

                              ,

                              $reason     

                            )

          ;

          array_push( $content, $remittanceData );

        }

        return $content;

    }

    // ----------------------------- End of getExportedContent( ) ----------------------------- //


    /**
     * Excel export file document header
     * @return [type] [description]
     */
    private function getExcelHeader( )
    {

        //Initialize document headers
        $headers = array();

        $headers[0] = "SENDER'S INFORMATION";

        //for some reason you need to push the array to cover the cells.
        //Note that cells A1 to P1 were merge in exportExcel( )
        for( $i = 1; $i < 26 ; $i++ )
        {

            $headers[ $i ] = "Beneficiary customer";

        }
            
        $headers[ 26 ] = "Purpose of the Remittance";

        return $headers;

    }

    // ----------------------------- End of getExcelHeader( ) ----------------------------- //


    /**
     * Titles for excel export file
     * @return array excel files titles
     */
    private function getExcelTitles( )
    {

        return array
               (

                    // SENDER'S INFORMATION                               
                    'Date Transaction Completed'

                    , 

                    'Currency code'

                    , 

                    'Total amount/value'

                    ,

                    'Source of Income' 

                    ,

                    'Transaction reference number'

                    ,

                    'Full name'

                    ,

                    'Date of birth (if an individual)'

                    ,

                    'Business/residential address (not a post box address)'

                    ,

                    'City/town/suburb'

                    ,

                    'State'

                    ,

                    'Postcode'

                    ,

                    'Country'

                    ,

                    'Phone'

                    ,

                    'Email'

                    ,

                    'ID type'

                    ,

                    'ID Number'

                    ,

                    'Relationship'

                    ,

                    // Beneficiary customer                    
                    'Full name'

                    ,

                    'Date of birth (if an individual)'

                    ,

                    'Business/residential address (not a post box address)'

                    ,

                    'City/town/suburb'

                    ,

                    'State'

                    ,

                    'Postcode'

                    ,

                    'Country'

                    ,

                    'Phone'

                    ,

                    'Email'

                    ,

                    'Reason for the transfer'

               )

        ;

    }

    // ----------------------------------- End of getExcelHeaders( ) ------------------------------------ //
    

    public function test( $date )
    {

        $remittancesBetweenDates = $this -> getRemittancesBetweenDates( "October 1, 2017", "December 9, 2017" ) 

                                         -> select( 'id', 'transaction_number', 'remitter_id', 'recipient_id', 'amount_sent', 'status_id', 'date_transferred' )

                                         -> where( 'recipient_id', 61106 )
                                      
                                         -> where( 'status_id', 24 ) //24 for Transfer Complete

                                         -> orderBy( 'date_transferred', 'DESC' )

                                         -> get() //Paginate data first before processing related models
        
        ; 

        /**
         *  Add array headers
         *  $recipientRemittances array
         */
        $recipientRemittances = array( 

                                        array( 

                                                'Recipient Name'

                                                , 

                                                'Remittance Id'

                                                , 

                                                'Transaction No.'

                                                , 

                                                'Remitter Name'

                                                ,

                                                'Relationship'

                                                ,

                                                'Amount'

                                                ,

                                                'Source of Income'

                                                ,

                                                'Purpose'

                                                ,

                                                'Date'

                                             ) 

                                     )

        ;
       
        /**
         * Append related models
         */
        foreach( $remittancesBetweenDates as $remittance )
        {

          $this->appendRecipientInfo( $remittance->recipient ); 

          $remitter = $remittance->remitter;

          $recipient = $remittance->recipient;

          $this->checkEmpty( $remittance->source_of_fund_id ) ? $remittance->source_of_fund : 'N/A';

          $this->checkEmpty( $remittance->reason_id ) ? $remittance->reason : 'N/A'; 

          $recipientFullName = $remittance->recipient[ "fullname" ];

          $id = $remittance->id;

          $transactionNo = $remittance->transaction_number;

          $remitterFullName = $this->checkEmpty( $remittance->remitter ) ? $remittance->remitter->firstname.' '.$remittance->remitter->lastname : '';

          $recipientPerson = $this->checkEmpty( $recipient ) ? $recipient->recipientPerson : '' ;

          $recipientRelationship = $this->checkEmpty( $recipient ) ? $recipient->relationship : '' ;

          $relationshipName = $this->checkEmpty( $recipientRelationship ) ? $recipientRelationship->name : 'N/A' ;

          $recipientBirthday = $this->checkEmpty( $recipientPerson ) ? $recipientPerson->birthday : 'N/A' ;

          $amount = $remittance->amount_sent;

          $fundSource = $this->checkEmpty( $remittance->source_of_fund ) ? $remittance->source_of_fund->name : '' ;

          $reason = $this->checkEmpty( $remittance->reason_id ) ? $remittance->reason->name : '' ; 

          $date = $remittance->date_transferred; 

          $recipientEmail = $this->checkEmpty( $recipient ) ? $recipient->email : '' ;

          $contents = array( 

                              $recipientFullName

                              ,

                              $id

                              , 

                              $transactionNo 

                              , 

                              $remitterFullName

                              ,

                              $recipientRelationship

                              ,

                              $amount

                              ,

                              $fundSource

                              ,

                              $reason

                              ,

                              $date

                              ,

                              $remitter->birthday

                              ,

                              $recipientEmail

                            )

          ;

          array_push( $recipientRemittances, $contents );

        }
        return response()->json( $recipientRemittances );
        $fullname = $remittancesBetweenDates->first()->recipient['fullname'];

        $this->exportExcel( $fullname, $recipientRemittances );

        return $remittancesBetweenDates;

    }

}
