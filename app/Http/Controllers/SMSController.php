<?php


namespace App\Http\Controllers;

use Illuminate\Support\Str;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Pagination\Paginator;


use App\Models\Remitter;

use App\Models\RemitterConfirmationSms;


class SMSController extends Controller {

    public function __construct() {
		
        $this -> middleware( 'auth' );
        
    }

    public function index() {
		
		$sms_codes = $this -> getSMSCodes();
		
		$remitter = array();
		
		//CHECK IF SEARCH SMS CODE REMITTER IS ISSET AND NOT EMPTY
		if( isset( $_GET[ 'search_sms_code_remitter' ] ) && ! empty( $_GET[ 'search_sms_code_remitter' ] ) ) {
			
			$remitter = Remitter::selectRaw( 'id, email, CONCAT_WS( " ", firstname, lastname ) as fullname' )
			
								-> where( 'id', '=', $_GET[ 'search_sms_code_remitter' ] )

								-> get()
								
								-> first()

								-> toArray();		

		}
		
		return view( 'codes-sms', 
		
				array( 
				
					'sms_codes' => $sms_codes,
					
					'remitter' => $remitter
				
				) 
			
			);
		
    }

	public function verify( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {

			if( $request -> id != NULL || $request -> id != '' ) {
				
				$remove_sms_code = RemitterConfirmationSms::find( $request -> id );
				
				//CHECK IF REMOVE SMS CODE IS NOT EMPTY
				if( ! empty( $remove_sms_code ) ) {
					
					
					//UPDATE REMITTER
					$new_remitter = Remitter::find( $remove_sms_code -> remitter_id );
					
					$new_remitter -> sms_verified = TRUE;
					
					$new_remitter -> save();
					
					$remitter_id = $new_remitter[ 'id' ];
					
					
					//REMOVE SMS CODE
					$remove_sms_code -> delete();
					
					
					//GET ALL SMS CODE TABLES
					$sms_code_table_template = view( 'tables.sms_code_table',
					
						array( 'sms_codes' => $this -> getSMSCodes() )
						
					)
					
					-> render();
					
					
					//SEND SOME RESPONSE
					return \Response::json( [ 'success' => true, 'result' => [ 'sms_code_table_template' => $sms_code_table_template, 'action' => 'verify' ] ] );
					
					
				}
				
				else {
					
					return \Response::json( [ 'success' => false, 'error_message' => 'SMS code doesn \'t exists!' ] );

				}

			}
			
		}

	}

	public function getRemitters( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {
			
			$post = $request -> all();
			
			$remitter = $post[ 'data' ][ 'q' ];
			
			$search_results = Remitter::selectRaw( 'id, email, CONCAT_WS( " ", firstname, lastname ) as fullname' )
			
									-> where( 'sms_verified', '=', FALSE )
			
									-> having( 'fullname' , 'LIKE', '%'.$remitter.'%' )

									-> get() 
									
									-> toArray();

			$new_search_result = array();
			
			//LOOP ALL SEARCH RESULT
			foreach( $search_results as $key => $search_result ) {
				
				$new_search_result[ $key ][ 'id' ] = "{$search_result[ 'id' ]}";
				
				$new_search_result[ $key ][ 'text' ] = $search_result[ 'fullname' ] . ' | ' . $search_result[ 'email' ];
				
			}
			
			//SEND SOME RESPONSE
			return \Response::json( [ 'q' => $remitter, 'results' => $new_search_result ] );
							
		}
		
	}

	public function getModal( Request $request ) {
		
		//FORM IS GET VIA AJAX
		if( $request -> ajax() ) {

			//EDIT FORM
			if( $request -> id != NULL || $request -> id != '' ) {
				
				$sms_code_details = RemitterConfirmationSms::where( 'id', '=', $request -> id ) -> orderBy( 'id', 'DESC' ) -> first();
				
				$remitter = array();
				
				//CHECK IF SMS CODE REMITTER ID IS ISSET AND NOT EMPTY
				if( isset( $sms_code_details[ 'remitter_id' ] ) && ! empty( $sms_code_details[ 'remitter_id' ] ) ) {
					
					$remitter = Remitter::selectRaw( 'id, email, CONCAT_WS( " ", firstname, lastname ) as fullname' )
					
										-> where( 'sms_verified', '=', FALSE )
					
										-> where( 'id', '=', $sms_code_details[ 'remitter_id' ] )

										-> get()

										-> first()

										-> toArray();		
					
				}
				
				$content = view( 'modals.codes-sms' ) -> with( [ 'action' => 'edit', 'sms_code_id' => $sms_code_details[ 'id' ], 'sms_code_details' => $sms_code_details, 'remitter' => $remitter ] ) -> render();
				
				//SHOW SOME RESPONSE
				return \Response::json( [ 'success' => true, 'result' => [ 'sms_code_id' => $sms_code_details[ 'id' ], 'content' => $content, 'action' => 'edit' ] ] );

			}
			
			//NEW FORM
			else {
				
				$content = view( 'modals.codes-sms' ) -> with( [ 'action' => 'new' ] ) -> render();
				
				//SHOW SOME RESPONSE
				return \Response::json( [ 'success' => true, 'result' => [ 'sms_code_id' => 0, 'content' => $content, 'action' => 'new' ] ] );
				
			}
			
		}
		
	}

	public function postForm( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {
				
				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();


					//WE ONLY VALIDATE FOR NEW SMS CODE
					if( empty( $input[ 'sms_code_id' ] ) ) {
						
						//CHECK IF SMS CODE REMITTER IS ISSET AND NOT EMPTY
						if( isset( $input[ 'sms_code_remitter' ] ) && ! empty( $input[ 'sms_code_remitter' ] ) ) {
						
							$existing_sms_code_remitter = RemitterConfirmationSms::where( 'remitter_id', '=', $input[ 'sms_code_remitter' ] ) -> first( [ 'id' ] );
							
							//CHECK SMS CODE REMITTER ID ALREADY EXISTS
							if( ! empty( $existing_sms_code_remitter[ 'id' ] ) ) {

								return \Response::json( ['success' => false, 'error_message' => 'Remitter already have SMS code generated!' ] );

							}
						
						}
						
						//CHECK IF SMS CODE IS ISSET AND NOT EMPTY
						if( isset( $input[ 'sms_code' ] ) && ! empty( $input[ 'sms_code' ] ) ) {
							
							$existing_sms_code = RemitterConfirmationSms::where( 'code', '=', $input[ 'sms_code' ] ) -> first( [ 'id' ] );
							
							//CHECK SMS CODE ID ALREADY EXISTS
							if( ! empty( $existing_sms_code[ 'id' ] ) ) {

								return \Response::json( ['success' => false, 'error_message' => 'SMS Code is already in use!' ] );

							}
							
						}
						
					}
					
					
					//CHECK IF SMS CODE ID IS NOT EMPTY
					if( ! empty( $input[ 'sms_code_id' ] ) ) {

						$new_sms_code = RemitterConfirmationSms::find( $input[ 'sms_code_id' ] );

						$action = 'edit';

					}
					
					else {
						
						$new_sms_code = new RemitterConfirmationSms;
						
						$new_sms_code -> remitter_id = ( isset( $input[ 'sms_code_remitter' ] ) && ! empty( $input[ 'sms_code_remitter' ] ) ? $input[ 'sms_code_remitter' ] : NULL );

						$action = 'add';

					}
					
					$new_sms_code -> code = ( isset( $input[ 'sms_code' ] ) && ! empty( $input[ 'sms_code' ] ) ? $input[ 'sms_code' ] : NULL );
					
					$new_sms_code -> count = ( isset( $input[ 'sms_code_tries' ] ) && ! empty( $input[ 'sms_code_tries' ] ) ? $input[ 'sms_code_tries' ] : NULL );
					
					$new_sms_code -> save();
					
					$sms_code_id = $new_sms_code[ 'id' ];
					
					//COMMIT ALL TRANSACTION
					DB::commit();
					
					
					$sms_code_table_template = view( 'tables.sms_code_table',
					
						array( 'sms_codes' => $this -> getSMSCodes() )
						
					)
					
					-> render();
					
					
					//SHOW SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'sms_code_id' => $sms_code_id, 'sms_code_table_template' => $sms_code_table_template, 'action' => $action ) ] );
					
					
				}
				
				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
			
				
			}
			
		}
		
	}
	
	public function deleteSMSCodes( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {

				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();
					
					
					//GET ALL INPUT
					$input = $request -> all();
					
					
					//CHECK IF SMS CODE IS ISSET AND NOT EMPTY
					if( isset( $input[ 'sms_code' ] ) && ! empty( $input[ 'sms_code' ] ) && count( $input[ 'sms_code' ] ) ) {
						
						//LOOP ALL SMS CODE
						foreach( $input[ 'sms_code' ] AS $key => $sms_code ) {
							
							//REMOVE SMS CODE
							$remove_sms_code = RemitterConfirmationSms::find( $sms_code ) -> delete();
							
						}
						
					}
					
					
					//COMMIT ALL TRANSACTION
					DB::commit();
					
					
					//GET ALL SMS CODE TABLES
					$sms_code_table_template = view( 'tables.sms_code_table',
					
						array( 'sms_codes' => $this -> getSMSCodes() )
						
					)
					
					-> render();
					
					//SEND SOME RESPONSE
					return \Response::json( ['success' => true, 'result' => array( 'total_sms_code' => count( $input[ 'sms_code' ] ), 'sms_code_table_template' => $sms_code_table_template, 'action' => 'delete' ) ] );
					
					
				}

				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
				
			}

		}
		
	}
	

	private function getSMSCodes() {
		
		//REMOVE PAGE FROM SEARCH QUERY
		UNSET( $_GET[ 'page' ] );
		
		//SEARCH QUERY IS ISSET AND NOT EMPTY
		if( isset( $_GET ) && ! empty( array_filter( $_GET ) ) ) {
			
			$search_query = $this -> searchParameter();
			
			$sms_codes = DB::table( 'remitter_confirmation_sms' )
			
					-> leftJoin( 'remitters', 'remitters.id', '=', 'remitter_confirmation_sms.remitter_id' )

					-> select( DB::Raw( '

						crm_remitter_confirmation_sms.id AS id,
					
						CONCAT_WS( " ", crm_remitters.firstname, crm_remitters.lastname ) AS remitter_fullname,
						
						crm_remitters.email AS remitter_email,
						
						crm_remitter_confirmation_sms.code AS sms_code,
						
						crm_remitter_confirmation_sms.count AS tries,
						
						crm_remitter_confirmation_sms.created_at AS date
					
					' ) )
					
					-> whereRaw( $search_query )
					
					-> whereNull( 'remitter_confirmation_sms.deleted_at' )
					
					-> orderBy( 'remitter_confirmation_sms.id','DESC' )
					
					-> paginate( 20 )

					-> setPath( 'sms' );
			
		}
		
		else {
			
			$sms_codes = DB::table( 'remitter_confirmation_sms' )
					
					-> leftJoin( 'remitters', 'remitters.id', '=', 'remitter_confirmation_sms.remitter_id' )
					
					-> select( DB::Raw( '
					
						crm_remitter_confirmation_sms.id AS id,
					
						CONCAT_WS( " ", crm_remitters.firstname, crm_remitters.lastname ) AS remitter_fullname,
						
						crm_remitters.email AS remitter_email,
						
						crm_remitter_confirmation_sms.code AS sms_code,
						
						crm_remitter_confirmation_sms.count AS tries,
						
						crm_remitter_confirmation_sms.created_at AS date
					
					' ) )
					
					-> whereNull( 'remitter_confirmation_sms.deleted_at' )
					
					-> orderBy( 'remitter_confirmation_sms.id','DESC' )
					
					-> paginate( 20 )

					-> setPath( 'sms' );
			
		}
		
		return $sms_codes;
		
	}
	
	private function searchParameter() {
		
		//CONSTRUCT QUERY STRING
		$query_string = array();
		
		//CHECK IF GET IS SET AND NOT EMPTY
		if( isset( $_GET ) && ! empty( $_GET ) ) {

			//SMS CODE REMITTER
			if( isset( $_GET[ 'search_sms_code_remitter' ] ) && ! empty( $_GET[ 'search_sms_code_remitter' ] ) ) {
				
				$query_string[] = " crm_remitter_confirmation_sms.remitter_id = {$_GET[ 'search_sms_code_remitter' ]} ";
				
			}
			
			//SMS CODE REMITTER EMAIL
			if( isset( $_GET[ 'search_sms_code_remitter_email' ] ) && ! empty( $_GET[ 'search_sms_code_remitter_email' ] ) ) {
				
				$query_string[] = " crm_remitters.email = '{$_GET[ 'search_sms_code_remitter_email' ]}' ";
				
			}
			
			//SMS CODE DATE
			if(

				( isset( $_GET[ 'search_sms_code_start_date' ] ) && ! empty( $_GET[ 'search_sms_code_start_date' ] ) )

				&&

				( isset( $_GET[ 'search_sms_code_end_date' ] ) && ! empty( $_GET[ 'search_sms_code_end_date' ] ) )

			) {

				$query_string[] = " 

					DATE( crm_remitter_confirmation_sms.created_at ) >= '{$_GET[ 'search_sms_code_start_date' ]}' 

					AND 

					DATE( crm_remitter_confirmation_sms.created_at ) <= '{$_GET[ 'search_sms_code_end_date' ]}' 

				";

			}
			
			//RETURN SEARCH QUERY
			return implode( ' AND ', array_filter( $query_string ) );

		}
		
	}
	
    
}
