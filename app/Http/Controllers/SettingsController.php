<?php

namespace App\Http\Controllers;


use Illuminate\Support\Str;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\Auth;


class SettingsController extends Controller {

    public function __construct() {
		
        $this -> middleware( 'auth' );
        
    }

    public function index() {
		
		$forex_rate = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_default_xrate' ) -> value( 'option_value' );
		
		$trade_rate = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_default_traderrate' ) -> value( 'option_value' );
		
		
		$rates = array();
		
		$rates[ 'forex' ] = ( ! empty( $forex_rate ) ? $forex_rate : 0 );
		
		$rates[ 'trade' ] = ( ! empty( $trade_rate ) ? $trade_rate : 0 );
		


		$cash_pickup_fee = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_cash_pickup' ) -> value( 'option_value' );
		
		$bank_to_bank_fee = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee' ) -> value( 'option_value' );
		
		$door_to_door_fee = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_door' ) -> value( 'option_value' );
		

		$fees = array();
		
		$fees[ 'cash-pickup' ] = ( ! empty( $cash_pickup_fee ) ? $cash_pickup_fee : 0 );
		
		$fees[ 'bank-to-bank' ] = ( ! empty( $bank_to_bank_fee ) ? $bank_to_bank_fee : 0 );
		
		$fees[ 'door-to-door' ] = ( ! empty( $door_to_door_fee ) ? $door_to_door_fee : 0 );
		


		$mlhuillier_50_100_fee = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_bquad' ) -> value( 'option_value' );
		
		$mlhuillier_101_150_fee = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_dquad' ) -> value( 'option_value' );
		
		$mlhuillier_151_200_fee = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_tquad' ) -> value( 'option_value' );
		
		$mlhuillier_201_250_fee = DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_mquad' ) -> value( 'option_value' );


		$custom_fees = array();
		
		$custom_fees[ 'mlhuillier' ][ '50-100' ] = ( ! empty( $mlhuillier_50_100_fee ) ? $mlhuillier_50_100_fee : 0 );
		
		$custom_fees[ 'mlhuillier' ][ '101-150' ] = ( ! empty( $mlhuillier_101_150_fee ) ? $mlhuillier_101_150_fee : 0 );
		
		$custom_fees[ 'mlhuillier' ][ '151-200' ] = ( ! empty( $mlhuillier_151_200_fee ) ? $mlhuillier_151_200_fee : 0 );
		
		$custom_fees[ 'mlhuillier' ][ '201-250' ] = ( ! empty( $mlhuillier_201_250_fee ) ? $mlhuillier_201_250_fee : 0 );
		


		return view( 'rates-and-fees', array( 'rates' => $rates, 'fees' => $fees, 'custom_fees' => $custom_fees ) );

	}

	public function postForm( Request $request ) {
		
		//FORM IS POST VIA AJAX
		if( $request -> ajax() ) {
			
			//AJAX IS POST
			if( $request -> isMethod( 'post' ) ) {
				
				try {
					
					
					//START TRANSACTION
					DB::beginTransaction();


					//GET ALL INPUT
					$input = $request -> all();
					
					
					//RATE
					$setting_forex_rate = ( isset( $input[ 'setting_forex_rate' ] ) && ! empty( $input[ 'setting_forex_rate' ] ) ? $input[ 'setting_forex_rate' ] : NULL );
					
					$setting_trade_rate = ( isset( $input[ 'setting_trade_rate' ] ) && ! empty( $input[ 'setting_trade_rate' ] ) ? $input[ 'setting_trade_rate' ] : NULL );


					//FEES
					$setting_service_fee_cash_pickup = ( isset( $input[ 'setting_service_fee_cash_pickup' ] ) && ! empty( $input[ 'setting_service_fee_cash_pickup' ] ) ? $input[ 'setting_service_fee_cash_pickup' ] : NULL );
					
					$setting_service_fee_bank_to_bank = ( isset( $input[ 'setting_service_fee_bank_to_bank' ] ) && ! empty( $input[ 'setting_service_fee_bank_to_bank' ] ) ? $input[ 'setting_service_fee_bank_to_bank' ] : NULL );
					
					$setting_service_fee_door_to_door = ( isset( $input[ 'setting_service_fee_door_to_door' ] ) && ! empty( $input[ 'setting_service_fee_door_to_door' ] ) ?  $input[ 'setting_service_fee_door_to_door' ] : NULL );


					/*-----TABLE RATE START-----*/


						//MLHUILIER
						$mlhuilier_50_100 = ( isset( $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '50-100' ] ) && ! empty( $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '50-100' ] ) ? $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '50-100' ] : NULL );
						
						$mlhuilier_101_150 = ( isset( $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '101-150' ] ) && ! empty( $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '101-150' ] ) ? $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '101-150' ] : NULL );
						
						$mlhuilier_151_200 = ( isset( $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '151-200' ] ) && ! empty( $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '151-200' ] ) ? $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '151-200' ] : NULL );
						
						$mlhuilier_201_250 = ( isset( $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '201-250' ] ) && ! empty( $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '201-250' ]  ) ? $input[ 'setting_custom_service_fee' ][ 'mlhuilier' ][ '201-250' ] : NULL );


					/*-----TABLE RATE END-----*/




					//RATES
					DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_default_xrate' ) -> update( [ 'option_value' => $setting_forex_rate ] );

					DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_default_traderrate' ) -> update( [ 'option_value' => $setting_trade_rate ] );




					//FEES
					DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_cash_pickup' ) -> update( [ 'option_value' => $setting_service_fee_cash_pickup ] );

					DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee' ) -> update( [ 'option_value' => $setting_service_fee_bank_to_bank ] );

					DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_door' ) -> update( [ 'option_value' => $setting_service_fee_door_to_door ] );




					/*----TABLE RATE----*/




					//MLHULIER
					DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_bquad' ) -> update( [ 'option_value' => $mlhuilier_50_100 ] );

					DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_dquad' ) -> update( [ 'option_value' => $mlhuilier_101_150 ] );

					DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_tquad' ) -> update( [ 'option_value' => $mlhuilier_151_200 ] );

					DB::table( DB::Raw( 'wp_options' ) ) -> where( 'option_name', '=', 'iremit_service_fee_mquad' ) -> update( [ 'option_value' => $mlhuilier_201_250 ] );




					//COMMIT ALL TRANSACTION
					DB::commit();


					//SHOW SOME RESPONSE
					return \Response::json( [ 'success' => true ] );


				}
				
				//CHECK IF THERE WERE EXCEPTION
				catch( Exception $e ) {
					
					//ROLL BACK
					DB::rollback(); 
					
				}
			
			}
			
		}
		
	}

}
