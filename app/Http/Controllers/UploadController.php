<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Webpatser\Uuid\Uuid;

class UploadController extends Controller {


	private $remitter_id_temp_path;

	private $remitter_avatar_temp_path;
	
	private $remitter_company_authorization_temp_path;
	
	private $remittance_receipt_temp_path;
	
	private $remittance_addtional_requirement_temp_path;


	//AUTHENTICATE AND INITIALIZE VARIABLE
    public function __construct() {
		

		//REMITTER ID FILE PATH
		$this -> remitter_id_temp_path = public_path( 'img/remitter_id/temp' );
		
		
		//REMITTER PROFILE FILE PATH
		$this -> remitter_avatar_temp_path = public_path( 'img/remitter_avatar/temp' );
		
		
		//REMITTER COMPANY AUTHORIZATION FILE PATH
		$this -> remitter_company_authorization_temp_path = public_path( 'img/remitter_company_authorization/temp' );
		
		
		//REMITTANCE RECEIPT FILE PATH
		$this -> remittance_receipt_temp_path = public_path( 'img/remittance_receipt/temp' );
		
		
		//REMITTANCE ADDITIONAL REQUIREMENT FILE PATH
		$this -> remittance_addtional_requirement_temp_path = public_path( 'img/remittance_additional_requirement/temp' );
		
		
		//CHECK AUTHENTICATION
        $this -> middleware( 'auth' );
		
		
    }
    
    //UPLOAD REMITTER ID TEMPORARY FOLDER
    public function dropzoneRemitterIdTemp( Request $request ) {

		$filename = strtotime( date( 'Y-m-d H:i:s' ) );

        $image = $request -> file( 'file' );

        $image_name = $filename . '.' . $image -> getClientOriginalExtension();

        $image -> move( $this -> remitter_id_temp_path , $image_name );

        return response() -> json( [ 'filname' => $image_name ] );

	}

    //UPLOAD REMITTER PROFILE TEMPORARY FOLDER
    public function dropzoneRemitterAvatarTemp( Request $request ) {

		$filename = strtotime( date( 'Y-m-d H:i:s' ) );

        $image = $request -> file( 'file' );

        $image_name = $filename . '.' . $image -> getClientOriginalExtension();

        $image -> move( $this -> remitter_avatar_temp_path , $image_name );

        return response() -> json( [ 'filname' => $image_name ] );

	}
	
    //UPLOAD REMITTER COMPANY AUHTORIZATION TEMPORARY FOLDER
    public function dropzoneRemitterCompanyAuthorizationTemp( Request $request ) {

		$filename = strtotime( date( 'Y-m-d H:i:s' ) );

        $image = $request -> file( 'file' );

        $image_name = $filename . '.' . $image -> getClientOriginalExtension();

        $image -> move( $this -> remitter_company_authorization_temp_path , $image_name );

        return response() -> json( [ 'filname' => $image_name ] );

	}
	
    //UPLOAD REMITTANCE RECEIPT TEMPORARY FOLDER
    public function dropzoneRemittanceReceiptTemp( Request $request ) {

		$filename = strtotime( date( 'Y-m-d H:i:s' ) );

        $image = $request -> file( 'file' );

        $image_name = $filename . '.' . $image -> getClientOriginalExtension();

        $image -> move( $this -> remittance_receipt_temp_path , $image_name );

        return response() -> json( [ 'filname' => $image_name ] );

	}
	
	//UPLOAD REMITTANCE ADDITIONAL REQUIREMENT TEMPORARY FOLDER
	public function dropzoneRemittanceAdditionalReqirementTemp( Request $request ) {
		
		$filename = strtotime( date( 'Y-m-d H:i:s' ) );

        $image = $request -> file( 'file' );

        $image_name = $filename . '.' . $image -> getClientOriginalExtension();

        $image -> move( $this -> remittance_addtional_requirement_temp_path , $image_name );

        return response() -> json( [ 'filname' => $image_name ] );
		
	}
	
}
