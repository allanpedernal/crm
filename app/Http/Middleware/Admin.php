<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // Perform action
        if(Auth::check()){
			
			$user = Auth::user();
			$wp_capabilites = unserialize($user->meta->wp_capabilities);
			$admin = (isset($wp_capabilites['administrator'])&&!empty($wp_capabilites['administrator'])?true:false);
			
			//CHECK IF USER IS ADMIN
			if(!empty($admin)&&$admin){
				return redirect('/dashboard');
			}
			
			//NOT ADMIN
			else {
				Auth::logout();
				return redirect('/login');
			}
			
		}
        
        return $response;
    }
}
