<?php

namespace App\Jobs;

use Exception;

use App\Models\Library\LibraryCity;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Maatwebsite\Excel\Facades\Excel;

class MigrateLibraryCity implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
		
		try{
			
			//NO TIME LIMIT
			set_time_limit(0);

			//GET ALL RECORD FROM AU.CSV
			Excel::load(storage_path('app/csv/au.csv'), function($rows) {
				$rows->each(function($row) {

					$city = $row->toArray();
					$category = $city['category'];
					
					//CHECK IF CATEGORY IS DELIVERY AREA
					if(trim(strtolower($category))=='delivery area'){
						
						$name = $city['locality'];
						$postal_code = $city['pcode'];
						$state_id = DB::table('library_states')
									->select('id')
									->where(DB::Raw('LOWER(TRIM(shortname))'), '=', trim(strtolower($city['state'])))
									->value('id');
						
						//INSERT AU CITY
						$city_id = DB::table('library_cities')->insertGetId(['name'=>$name,'postal_code'=>$postal_code,'state_id'=>$state_id]);
						\Log::info("name: {$name} :: postal code: {$postal_code} :: state_id: {$state_id}");

					}
					
				});
			});

			$philippine_cities = file_get_contents(storage_path('app/txt/philippine-provinces.txt'),true);
			$philippine_cities = str_replace("=>",":",$philippine_cities);
			$philippine_cities = str_replace(" ","",$philippine_cities);
			$philippine_cities = str_replace("'",'"',$philippine_cities);
			$philippine_cities = str_replace(PHP_EOL,null,$philippine_cities);
			$philippine_cities = json_decode($philippine_cities,true);

			//CHECK IF PHILIPPINE CITIES
			if(count($philippine_cities)){
				
				foreach($philippine_cities as $state=>$cities){
					
					$state_id = DB::table('library_states')->insertGetId(['name'=>$state,'country_id'=>175]);
					\Log::info("state: {$state} :: country_id: 175 :: state_id: {$state_id}");
					
					if(count($cities)){
						foreach($cities as $city){
							
							$city_id = DB::table('library_cities')->insertGetId(['name'=>strtoupper($city),'state_id'=>$state_id]);
							\Log::info("city: {$city} :: state_id: {$state_id}");
							
						}
					}
					
				}
				
			}

		}
		catch(Exception $e){
			\Log::info($e->getMessage());
		}
    }
    
    /**
     * Catch the exception.
     *
     * @return void
     */
    public function failed(Exception $e){
		
        \Log::info($e->getMessage());
        
    }
}
