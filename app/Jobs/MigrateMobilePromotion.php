<?php

namespace App\Jobs;

use Exception;

use App\Models\MobilePromotion;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MigrateMobilePromotion implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
		
		try{
			
			//NO TIME LIMIT
			set_time_limit(0);
			
			while(true){
				
				$mobile_promotion_query = " SELECT ";
				$mobile_promotion_query .= " * ";
				$mobile_promotion_query .= " FROM tbl_promotions ";
				$mobile_promotion_query .= " WHERE id NOT IN (SELECT old_mobile_promotion_id FROM crm_migrate_mobile_promotions) ";
				$mobile_promotion_query .= " ORDER BY id ASC LIMIT 50 ";
				
				$mobile_promotions = array();
				$mobile_promotions = DB::select(DB::raw($mobile_promotion_query));
				
				//CHECK IF MOBILE PROMOTIONS IS NOT EMPTY
				if(empty($mobile_promotions)){
					
					break;
					
				}
				
				//LOOP ALL MOBILE PROMOTIONS
				foreach($mobile_promotions as $mobile_promotion){
					
						//INSERT NEW PROMO CODE USER
						$new_mobile_promotion = new MobilePromotion;
						$new_mobile_promotion->excerpt = $mobile_promotion['short_description'];
						$new_mobile_promotion->description = $mobile_promotion['detail_description'];
						$new_mobile_promotion->send_times = $mobile_promotion['send_times'];
						$new_mobile_promotion->is_send = $mobile_promotion['is_send'];
						$new_mobile_promotion->created_by = $mobile_promotion['user_id'];
						$new_mobile_promotion->created_at = $mobile_promotion['created_at'];
						$new_mobile_promotion->save();
						$mobile_promotion_id = $new_mobile_promotion['id'];
						
						
						//RECORD MOBILE PROMOTION THAT WILL BE PROCESSED
						$new_data = array();
						$new_data['old_mobile_promotion_id'] = $mobile_promotion['id'];
						$new_data['new_mobile_promotion_id'] = $mobile_promotion_id;
						$new_data['datetime'] = date('Y-m-d H:i:s');
						DB::table('migrate_mobile_promotions')->insert($new_data);
						
						//LOG THAT IT HAS BEEN INSERTED
						\Log::info("Old Mobile Promotion ID: {$mobile_promotion['id']} :: New Mobile Promotion ID: {$mobile_promotion_id} has been inserted!");
							
					}

				
			}

		}
		catch(Exception $e){
			\Log::info($e->getMessage());
		}
    }
    
    /**
     * Catch the exception.
     *
     * @return void
     */
    public function failed(Exception $e){
		
        \Log::info($e->getMessage());
        
    }
}
