<?php

namespace App\Jobs;

use Exception;

use App\Models\RemittancePromoCode;
use App\Models\RemittancePromoCodeUser;
use App\Models\RemittancePromoCodeUserRecipient;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MigratePromoCode implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
		
		try{
			
			//NO TIME LIMIT
			set_time_limit(0);
			
			while(true){
				
				$promo_code_query = " SELECT ";
				$promo_code_query .= " * ";
				$promo_code_query .= " FROM tbl_promo_code ";
				$promo_code_query .= " WHERE id NOT IN (SELECT promo_code_id FROM crm_migrate_remittance_promo_codes) ";
				$promo_code_query .= " ORDER BY id ASC LIMIT 50 ";
				
				$promo_codes = array();
				$promo_codes = DB::select(DB::raw($promo_code_query));
				
				//CHECK IF PROMO CODES IS NOT EMPTY
				if(empty($promo_codes)){
					
					break;
					
				}
				
				//LOOP ALL PROMO CODES
				foreach($promo_codes as $promo_code){
					
					
					//GET ALL REMITTANCE PROMO CODE USERS
					$remittance_promo_code_users = DB::table(DB::Raw('tbl_promo_code_users'))
													->where('promo_code_id','=',$promo_code['id'])
													->get();
													
					//CHECK IF THERE ARE REMITTANCE PROMO CODE USERS
					if(count($remittance_promo_code_users)){
						
						//LOOP ALL REMITTER PROMO CODE USERS
						foreach($remittance_promo_code_users as $remittance_promo_code_user){
							
							//CHECK IF REMITTER BUSINESS ID IS NOT EMPTY
							if(!empty($remittance_promo_code_user['remitter_business_id'])){
								
								//INSERT NEW PROMO CODE USER
								$new_remittance_promo_code_user = new RemittancePromoCodeUser;
								$new_remittance_promo_code_user->remittance_promo_code_id = $remittance_promo_code_user['id'];
								$new_remittance_promo_code_user->remitter_business_id = $remittance_promo_code_user['remitter_business_id'];
								$new_remittance_promo_code_user->total_consumable = ($remittance_promo_code_user['total_consumable']<=0?0:$remittance_promo_code_user['total_consumable']);
								$new_remittance_promo_code_user->all_user = $remittance_promo_code_user['all_user'];
								$new_remittance_promo_code_user->type = $remittance_promo_code_user['type'];
								$new_remittance_promo_code_user->action = $remittance_promo_code_user['action'];
								$new_remittance_promo_code_user->save();
								$remittance_promo_code_user_id = $new_remittance_promo_code_user['id'];
							
							}
							
						}
						
					}
					
					
					//GET ALL REMITTANCE PROMO CODE USER RECIPIENTS
					$remittance_promo_code_user_recipients = DB::table(DB::Raw('tbl_promo_code_user_recipients'))
															->where('promo_code_id','=',$promo_code['id'])
															->get();
															
					//CHECK IF THERE ARE REMITTANCE PROMO CODE USER RECIPIENT
					if(count($remittance_promo_code_user_recipients)){
						
						//LOOP ALL REMITTER PROMO CODE USER RECIPIENTS
						foreach($remittance_promo_code_user_recipients as $remittance_promo_code_user_recipient){
							
								//INSERT NEW PROMO CODE USER RECIPIENT
								$new_remittance_promo_code_user_recipient = new RemittancePromoCodeUserRecipient;
								$new_remittance_promo_code_user_recipient->remittance_promo_code_id = $remittance_promo_code_user['id'];
								$new_remittance_promo_code_user_recipient->remitter_id = $remittance_promo_code_user['remitter_business_id'];
								$new_remittance_promo_code_user_recipient->recipient_id = ($remittance_promo_code_user['total_consumable']<=0?0:$remittance_promo_code_user['total_consumable']);
								$new_remittance_promo_code_user_recipient->save();
								$remittance_promo_code_user_recipient_id = $new_remittance_promo_code_user_recipient['id'];
							
						}
						
					}
					
					
					/*
					* PROMO CODE TABLE FIELDS
					* 
					* name
					* description
					* classification
					* type
					* total_consumable
					* user_classification
					* duration
					* expiration_date
					* value
					* created_by
					* created_at
					* updated_at
					* deleted_at
					*/
					
					//INSERT NEW PROMO CODE
					$new_remittance_promo_code = new RemittancePromoCode;
					$new_remittance_promo_code->name = $promo_code['promo_code'];
					$new_remittance_promo_code->description = $promo_code['description'];
					$new_remittance_promo_code->classification = $promo_code['classification'];
					$new_remittance_promo_code->type = $promo_code['type'];
					$new_remittance_promo_code->total_consumable = ($promo_code['total_consumable']<=0?0:$promo_code['total_consumable']);
					$new_remittance_promo_code->user_classification = $promo_code['user_classification'];
					$new_remittance_promo_code->user_action = $promo_code['user_action'];
					$new_remittance_promo_code->duration = $promo_code['duration'];
					$new_remittance_promo_code->expiration_date = ($promo_code['expiration_date']=='0000-00-00'?NULL:$promo_code['expiration_date']);
					$new_remittance_promo_code->value = $promo_code['value'];
					$new_remittance_promo_code->created_by = $promo_code['created_by'];
					$new_remittance_promo_code->created_at = $promo_code['created_datetime'];
					$new_remittance_promo_code->save();
					$new_remittance_promo_code_id = $new_remittance_promo_code['id'];
					
					//RECORD REMITTANCE THAT WILL BE PROCESSED
					$new_data = array();
					$new_data['promo_code_id'] = $new_remittance_promo_code_id;
					$new_data['datetime'] = date('Y-m-d H:i:s');
					DB::table('migrate_remittance_promo_codes')->insert($new_data);
					
					//LOG THAT IT HAS BEEN INSERTED
					\Log::info("Promo code: {$promo_code['id']} has been inserted!");
					
				}
				
			}
			
		}
		catch(Exception $e){
			\Log::info($e->getMessage());
		}
    }
    
    /**
     * Catch the exception.
     *
     * @return void
     */
    public function failed(Exception $e){
		
        \Log::info($e->getMessage());
        
    }
}
