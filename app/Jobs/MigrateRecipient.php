<?php

namespace App\Jobs;

use Exception;

use App\Models\Remitter;
use App\Models\Recipient;
use App\Models\RecipientContact;
use App\Models\RecipientAddress;
use App\Models\RecipientReceiveOption;
use App\Models\RecipientBankDetail;

use App\Models\Library\LibraryReceiveOption;
use App\Models\Library\LibraryReceiveOptionList;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MigrateRecipient implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
	
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
		
		try{
			
			//NO TIME LIMIT
			set_time_limit(0);

			//WE DONT END THE LOOP
			while(true){
				
				/*
				* NOTE: WE ARE MIGRATING RECIPIENT BASE ON
				* REMITTER RECIPIENTS
				*/

				/*
				*-----------------------------
				* GETTING ALL REMITTERS START
				*-----------------------------
				*/

				//MIGRATE REMITTER
				$remitter_query = " SELECT ";
				
				//REMITTER DETAILS
				$remitter_query .= " wu.ID AS ID, ";
				$remitter_query .= " wu.user_email AS user_email ";
				$remitter_query .= " crm.new_remitter_id AS new_remitter_id";
				$remitter_query .= " FROM wp_users AS wu ";
				$remitter_query .= " LEFT JOIN crm_migrate_remitters AS crm ON crm.userid = wu.ID ";
				$remitter_query .= " WHERE wu.ID NOT IN (SELECT userid FROM crm_migrate_recipient_users) ";
				$remitter_query .= " ORDER BY wu.ID ASC LIMIT 500 ";
				
				//GET ALL RECORDS
				$remitters = array(); //RESET REMITTERS
				$remitters = DB::select(DB::raw($remitter_query));

				
				//CHECK IF REMITTER IS NOT EMPTY
				if(empty($remitters)){

					break;

				}

				/*
				*-----------------------------
				* GETTING ALL REMITTERS END
				*-----------------------------
				*/


				/*
				*-------------------------------------------------
				* HERE WE START GET EACH REMITTER RECIPIENT START
				*-------------------------------------------------
				*/

				foreach($remitters as $remitter){

					$recipient_query = " SELECT ";
					$recipient_query .= " trec.fname AS recipient_fname, ";
					$recipient_query .= " trec.lname AS recipient_lname ";
					$recipient_query .= " FROM tbl_remittance AS tr ";
					$recipient_query .= " LEFT JOIN tbl_remitter AS trem ON tr.remitter = trem.id ";
					$recipient_query .= " LEFT JOIN tbl_recipient AS trec ON tr.recipient = trec.id ";
					$recipient_query .= " WHERE trem.userid = {$remitter['ID']} ";
					$recipient_query .= " ORDER BY tr.id DESC ";
					
					//RESET RECIPIENTS
					$recipients = array(); //RESET RECIPIENTS
					$recipients = DB::select(DB::raw($recipient_query));
					
					//RESET NEW RECIPIENTS
					$new_recipients = array(); //RESET NEW RECIPIENTS
					$new_recipients = $this->_remove_duplicate($recipients);
					
					//COUNT IF REMITTER HAVE RECIPIENTS
					if(count($new_recipients)){
						
						//RECIPIENT MODEL
						foreach($new_recipients as $rct){ 
							
							//RECIPIENT FULLNAME
							$recipient_firstname = $rct['recipient_fname'];
							$recipient_lastname = $rct['recipient_lname'];
							
							//CHECK IF REMITTER RECIPIENT IS ALREADY SYNCED
							$migrate_recipient_id = DB::table('migrate_recipients')
														->where('new_remitter_id','=',trim($remitter['new_remitter_id']))
														->where(DB::Raw('TRIM(LOWER(firstname))'),addslashes(trim(strtolower($recipient_firstname))))
														->where(DB::Raw('TRIM(LOWER(lastname))'),addslashes(trim(strtolower($recipient_lastname))))
														->value('id');

							//CHECK IF MIGRATE RECIPEINT ID IS EMPTY
							if(empty($migrate_recipient_id)&&($migrate_recipient_id=='')){
								
								//GET NEW REMITTER ID
								$new_remitter_id = Remitter::find($remitter['new_remitter_id'])->first(['id']);
								
								//RECIPIENT QUERY
								$recipient_detail_query = "";
								$recipient_detail_query .= " SELECT ";
								$recipient_detail_query .= " tr.id AS recipient_id ";
								$recipient_detail_query .= " FROM tbl_recipient tr";
								
								//NOT EMPTY FIRSTNAME
								if(!empty($recipient_firstname)){
									$recipient_detail_query .= " WHERE (TRIM(tr.fname) LIKE '%".addslashes(trim($recipient_firstname))."%') ";
								}
								
								//NOT EMPTY LASTNAME
								if(!empty($recipient_lastname)){
									$recipient_detail_query .= " AND (TRIM(tr.lname) LIKE '%".addslashes(trim($recipient_lastname))."%') ";
								}
								
								$recipient_detail_query .= " ORDER BY tr.id DESC ";
								
								//RESET RECIPIENT
								$recipient_detail = array(); //RESET RECIPIENT
								$recipient_detail = DB::select(DB::raw($recipient_detail_query));
								$old_recipient_id = $recipient_detail[0]['recipient_id'];
								
								//GET RECIPIENT RECIPIENT
								$recipient_details = DB::table(DB::Raw('tbl_recipient'))
														->where('id','=',$old_recipient_id)
														->first();
								
								//GET REMITTANCE DETAILS
								$remittance_details = DB::table(DB::Raw('tbl_remittance'))
														->where('recipient','=',$old_recipient_id)
														->orderBy('id','DESC')
														->first();

								//GET RECIEVE OPTION TRANSER
								$recieve_option_id = DB::table(DB::Raw('tbl_receive_options_transfer'))
														->where('id','=',$remittance_details['receive_options_transfer'])
														->first(['receive_options']);
								
								//GET CIVIL STATUS DETAILS		
								$civil_status_details = DB::table('library_civil_statuses')
														->where(DB::Raw('TRIM(LOWER(name))'),'=',trim(strtolower($recipient_details['recipient_civil_status'])))
														->first();

								//RECIPIENT					
								$new_recipient = array();
								$new_recipient['id_number'] = Str::random(8);
								$new_recipient['remitter_id'] = (!empty($new_remitter_id['id']) ? $new_remitter_id['id'] : NULL);
								$new_recipient['email'] = (!empty($recipient_details['recipient_email']) ? strtolower(trim($recipient_details['recipient_email'])) : NULL);
								$new_recipient['secondary_email'] = NULL;
								$new_recipient['filename'] = NULL;
								$new_recipient['remitter_relationship_id'] = (!empty($recipient_details['relationship_id']) ? $recipient_details['relationship_id'] : NULL);
								$new_recipient['type'] = (!empty($recipient_details['type'])&&strtolower(trim($recipient_details['type']))=='company' ? 'company' : 'person');
								$new_recipient['is_active'] = (strtolower(trim($remittance_details['disable_recipient'])) == 'yes' ? 0 : 1);
								$recipient_id = DB::table('recipients')->insertGetId($new_recipient);

								//RECIPIENT COMPANY
								if(isset($recipient_details['type'])&&!empty($recipient_details['type'])&&strtolower(trim($recipient_details['type']))=='company'){
									
									$new_recipient_company = array();
									$new_recipient_company['recipient_id'] = $recipient_id;
									$new_recipient_company['company_name'] = (!empty($recipient_details['fname']) ? ucwords(strtolower($recipient_details['fname'])) : NULL);
									$new_recipient_company['reg_number'] = (!empty($recipient_details['reg_no']) ? $recipient_details['reg_no'] : NULL);
									$new_recipient_company['contact_person'] = (!empty($recipient_details['contact_person']) ? $recipient_details['contact_person'] : NULL);
									$new_recipient_company['authorized_person'] = (!empty($recipient_details['authorized_person']) ? $recipient_details['authorized_person'] : NULL);
									$recipient_company_id = DB::table('recipient_companies')->insertGetId($new_recipient_company);
									
								}

								//RECIPIENT PERSON
								else {

									$new_recipient_person = array();
									$new_recipient_person['recipient_id'] = $recipient_id;
									$new_recipient_person['title_id'] = (!empty($recipient_details['title']) ? $recipient_details['title'] : NULL);
									$new_recipient_person['firstname'] = (!empty($recipient_details['fname']) ? ucwords(strtolower($recipient_details['fname'])) : NULL);
									$new_recipient_person['middlename'] = (!empty($recipient_details['mname']) ? ucwords(strtolower($recipient_details['mname'])) : NULL);
									$new_recipient_person['lastname'] = (!empty($recipient_details['lname']) ? ucwords(strtolower($recipient_details['lname'])) : NULL);
									$new_recipient_person['gender'] = NULL;
									$new_recipient_person['birthday'] = NULL;
									$new_recipient_person['civil_status_id'] = (!empty($civil_status_details['id']) ? $civil_status_details['id'] : NULL);
									$new_recipient_person['nationality_id'] = 175; //PHILIPPINES
									$recipient_person_id = DB::table('recipient_persons')->insertGetId($new_recipient_person);

								}

								//RECIPIENT CONTACT
								if(!empty($recipient_details['phone1'])){
									
									$new_recipient_contact = array();
									$new_recipient_contact['recipient_id'] = $recipient_id;
									$new_recipient_contact['contact_type_id'] = 1; 
									$new_recipient_contact['contact'] = (!empty($recipient_details['phone1']) ? $recipient_details['phone1'] : NULL);
									$new_recipient_contact['is_preferred'] = 1;
									$recipient_contact_id = DB::table('recipient_contacts')->insertGetId($new_recipient_contact);
								
								}

								//RECIPIENT ADDRESS
								if(!empty($recipient_details['address_street1'])){

									//CITY DETAILS
									$city_details = DB::table('library_cities')
														->where(DB::Raw('TRIM(LOWER(name))'),'=',trim(strtolower($recipient_details['address_city'])))
														->first();
									
									$new_recipient_address = array();
									$new_recipient_address['recipient_id'] = $recipient_id;
									$new_recipient_address['current_address'] = (!empty(trim($recipient_details['address_street1'])) ? trim($recipient_details['address_street1']) : NULL);
									$new_recipient_address['permanent_address'] = (!empty(trim($recipient_details['address_street2'])) ? trim($recipient_details['address_street2']) : NULL);
									$new_recipient_address['country_id'] = 175; //PHILIPPINES
									$new_recipient_address['state_id'] = (!empty($city_details['id']) ? $city_details['state_id'] : NULL);
									$new_recipient_address['city_id'] = (!empty($city_details['id']) ? $city_details['id'] : NULL);
									$new_recipient_address['postal_code'] = (!empty($recipient_details['zip']) ? $recipient_details['zip'] : NULL);
									$new_recipient_address['is_preferred'] = 1;
									
									$recipient_address_id = DB::table('recipient_addresses')->insertGetId($new_recipient_address);
									
								}

								//RECIPIENT RECEIVE OPTION TRANSFER
								if(!empty($remittance_details['receive_options_transfer'])){
									
									//OTHER BANKS
									if($remittance_details['receive_options_transfer'] == 19){

										//OLD RECEIVE OPTION NAME
										$old_receive_option_name = $remittance_details['other_bank_name'];

									}
									else{

										//OLD RECEIVE OPTION NAME
										$old_receive_option_name = DB::table(DB::Raw('tbl_receive_options_transfer'))
																		->where('id','=',$remittance_details['receive_options_transfer'])
																		->where('receive_options','=',$recieve_option_id['receive_options'])
																		->first(['name']);

										$old_receive_option_name = $old_receive_option_name['name'];

									}
									
									//CHCK IF RECEIVE OPTION NAME IS NOT EMPTY
									if(!empty($old_receive_option_name)){
										
										//RECIEVE OPTION TRANSFER ID							
										$recieve_option_transfer_id = LibraryReceiveOptionList::where(DB::Raw('LOWER(TRIM(name))'),'=',strtolower(trim($old_receive_option_name)))
																				->where('receive_option_id','=',$recieve_option_id['receive_options'])
																				->whereNotNull('name')
																				->first(['id']);

									}

								}

								//RECIPIENT BANK DETAILS AND RECEIVE OPTION TRANSFER IS BANK TRANSFER
								if(!empty($remittance_details['bank_accnt_no'])&&(!empty($recieve_option_id['receive_options'])&&$recieve_option_id['receive_options']==2)){	
								
									$new_recipient_bank = array();
									$new_recipient_bank['recipient_id'] = $recipient_id;
									$new_recipient_bank['receive_option_list_id'] = (!empty($recieve_option_transfer_id['id']) ? $recieve_option_transfer_id['id'] : NULL);
									$new_recipient_bank['account_name'] = (!empty($remittance_details['bank_accnt_name']) ? $remittance_details['bank_accnt_name'] : NULL);
									$new_recipient_bank['account_branch'] = (!empty($remittance_details['bank_branch']) ? $remittance_details['bank_branch'] : NULL);
									$new_recipient_bank['account_no'] = (!empty($remittance_details['bank_accnt_no']) ? $remittance_details['bank_accnt_no'] : NULL);
									$new_recipient_bank['is_preferred'] = 1;
									$recipient_bank_id = DB::table('recipient_banks')->insertGetId($new_recipient_bank);
								
								}

								//RECORD REMITTER RECIPIENT AND NEW RECIPIENT ID THAT WILL BE PROCESSED
								$new_recipient_data = array();
								$new_recipient_data['new_remitter_id'] = trim($new_remitter_id['id']);
								$new_recipient_data['old_recipient_id'] = $old_recipient_id;
								$new_recipient_data['new_recipient_id'] = $recipient_id;
								$new_recipient_data['firstname'] = trim(strtolower($recipient_firstname));
								$new_recipient_data['lastname'] = trim(strtolower($recipient_lastname));
								$new_recipient_data['datetime'] = date('Y-m-d H:i:s');
 								DB::table('migrate_recipients')->insert($new_recipient_data);

								\Log::info("New remitter ID: {$new_remitter_id['id']} :: Old Recipient ID: {$old_recipient_id} :: New Recipient ID: {$recipient_id} has been inserted!");
								
							}

						}

					}
					
				}
				
				/*
				*-------------------------------------------------
				* HERE WE START GET EACH REMITTER RECIPIENT END
				*-------------------------------------------------
				*/
			
			}
	 
		}
		
		catch(Exception $e){
			
			\Log::info($e->getMessage());
			
		}
 
    }
    
    /**
     * Catch the exception.
     *
     * @return void
     */
    public function failed(Exception $e){
		
        \Log::info($e->getMessage());
        
    }
    
    /**
     * Remove duplicate.
     *
     * @return array
     */
    private function _remove_duplicate($recipients = array()){
		
		//CHECK IF RECIPIENT IS NOT EQUAL TO 0
		if(count($recipients)){
			
			/*
			*-----------------------------------------------------
			* HERE WE START REMOVING DUPLICATE IN RECIPIENTS START
			*-----------------------------------------------------
			*/
		
			$recipient_storage = array();
			foreach($recipients as $recipient) {
				$recipient_storage[] = array(
					'recipient_fname' => trim($recipient['recipient_fname']),
					'recipient_lname' => trim($recipient['recipient_lname'])
				);
			} 
			
			/*
			$new_recipient_storage = $recipient_storage;
			foreach($recipient_storage as $key => $val) {
				foreach($new_recipient_storage as $k => $v) {
					if($key != $k && count(array_diff($val, $v)) == 1){
						unset($new_recipient_storage[$key]);
					}
				}
			}
			*/
			
			//REMOVE REDUNDANT RECIPIENT NAME
			$new_recipient_storage = array();
			foreach($recipient_storage as $recipient){
				$new_recipient_storage[] = serialize($recipient);
			}
			
			//REMOVE REDUNDANT
			$unique_recipients = array_values(array_unique($new_recipient_storage));
			
			$new_recipient_storage = array();
			foreach($unique_recipients as $unique_recipient){
				$new_recipient_storage[] = unserialize($unique_recipient);
			}

			return $new_recipient_storage;

			/*
			*-----------------------------------------------------
			* HERE WE START REMOVING DUPLICATE IN RECIPIENTS END
			*-----------------------------------------------------
			*/
			
		}
		
	}
    
}
