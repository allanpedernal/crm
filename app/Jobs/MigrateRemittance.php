<?php

namespace App\Jobs;

use Exception;

use Corcel\User;
use App\Models\Remitter;
use App\Models\RemitterContact;
use App\Models\RemitterAddress;
use App\Models\RemitterIdentification;
use App\Models\RemitterAccessToken;
use App\Models\RemitterConfirmationSms;

use App\Models\Recipient;
use App\Models\RecipientContact;
use App\Models\RecipientAddress;
use App\Models\RecipientReceiveOption;
use App\Models\RecipientBankDetail;

use App\Models\Library\LibraryReceiveOption;
use App\Models\Library\LibraryReceiveOptionList;

use App\Models\Remittance;
use App\Models\RemittanceReceipt;
use App\Models\RemittancePolipayment;
use App\Models\RemittanceOtherReason;
use App\Models\RemittanceNote;
use App\Models\RemittanceAdditionalRequirement;

use App\Models\Library\LibraryBankSource;
use App\Models\Library\LibraryRemittanceStatus;
use App\Models\Library\LibraryReason;

use App\Models\RemittancePromoCode;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MigrateRemittance implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
		
		try{
			
			//NO TIME LIMIT
			set_time_limit(0);
			
			while(true){
				
				/*
				* NOTE: WE ONLY MIGRATE RECORDS FROM 2017-01-01 ONWARDS
				*/
				
				$remittance_query = " SELECT ";
				$remittance_query .= " * ";
				$remittance_query .= " FROM tbl_remittance ";
				/*$remittance_query .= " WHERE DATE(date_transaction) >= '2017-01-01'  ";*/
				$remittance_query .= " WHERE id NOT IN (SELECT old_remittance_id FROM crm_migrate_remittances) ";
				$remittance_query .= " ORDER BY id ASC LIMIT 50 ";
				
				$remittances = array();
				$remittances = DB::select(DB::raw($remittance_query));
				\Log::info(print_r($remittances, true));
				
				//CHECK IF REMITTANCES IS NOT EMPTY
				if(empty($remittances)){
					
					break;
					
				}
				
				//LOOP ALL REMITTANCES
				foreach($remittances as $remittance){
					
					/*
					* - new table "crm_remittances"
					* - transaction_number
					* - remitter_id
					* - recipient_id
					* - payment_type
					* - bank_source_id
					* - amount_sent
					* - status_id
					* - has_receipt
					* - service_fee
					* - forex_rate
					* - trade_rate
					* - reason_id
					* - promo_code_id
					* - date_transferred
					* - created_at
					* - updated_at
					* - deleted_at
					*/
					
					/*
					* - old table "tbl_remittance"
					* - transaction_no
					* - transaction_no2
					* - claim_no
					* - remitter
					* - recipient
					* - receive_options_transfer
					* - bank_remittance_source
					* - bank_accnt_name
					* - bank_accnt_no
					* - bank_branch
					* - forex_rate
					* - service_fee
					* - amount_sent
					* - date_transaction
					* - status
					* - scanned_receipt
					* - reason
					* - file_receipt1
					* - file_receipt2
					* - file_receipt3
					* - date_transferred
					* - austrac_reported
					* - other_bank_name
					* - tracking_number
					* - payment_options
					* - promo_code
					* - notes
					* - disable_recipient
					* - trade_rate
					* - poli_ref
					* - duplicate_checker
					* - remitter_userid
					*/
					
					//CHECK IF REMITTANCE IS ALREADY SYNCED
					$migrate_remittance_id = DB::table('migrate_remittances')
												->where('old_remittance_id','=',$remittance['id'])
												->value('id');
					
					//REMITTANCE IS NOT YET SYNCED - THEREFORE THIS IS SAVE
					if(!isset($migrate_remittance_id)||empty($migrate_remittance_id)){
						
						/*
						*----------------------------------
						* GET RECORDS FROM OLD TABLES START
						*----------------------------------
						*/
						
						//LOG OLD REMITTER ID AND OLD RECIPIENT ID AND OLD REMITTANCE ID
						\Log::info("Remitter ID: {$remittance['remitter']} :: Recipient ID : {$remittance['recipient']} :: Remittance ID: {$remittance['id']}");
					
						//MIGRATE RECIPIENT COMING FROM REMITTANCE
						$migrate_recipient_successful = $this -> _migrate_recipient($remittance['remitter'], $remittance['recipient'], $remittance['id']);
						
						//CHECK IF MIGRATE REMITTER AND MIGRATE RECIPIENT IS SUCCESSFUL
						if($migrate_recipient_successful){

							//GET REMITTER DETAILS
							$remitter_details = DB::table(DB::Raw('tbl_remitter'))
													->where('id','=',$remittance['remitter'])
													->first();
													
							//GET RECIPIENT DETAILS
							$recipient_details = DB::table(DB::Raw('tbl_recipient'))
													->where('id','=',$remittance['recipient'])
													->first();
							
							//GET RECEIVE OPTION TRANSFER DETAILS
							$receive_option_transfer_details = DB::table(DB::Raw('tbl_receive_options_transfer'))
																->where('id','=',$remittance['receive_options_transfer'])
																->first();
																	
							//GET BANK REMITTANCE SOURCE DETAILS
							$bank_remittance_source_details = DB::table(DB::Raw('tbl_bank_remittance_source'))
																->where('id','=',$remittance['bank_remittance_source'])
																->first();
																	
							//GET PROMO CODE DETAILS
							$promo_code_details = DB::table(DB::Raw('tbl_promo_code'))
													->where(DB::Raw('LOWER(TRIM(promo_code))'),'=',strtolower(trim($remittance['promo_code'])))
													->first();
													
							/*
							*----------------------------------
							* GET RECORDS FROM OLD TABLES END
							*----------------------------------
							*/



							/*
							*------------------------------------------------------------------
							* LET SYNCED RECORD FROM OLD CORE TABLE TO OUR NEW CORE TABLE START
							*------------------------------------------------------------------
							*/

							//NEW REMITTER ID
							$new_remitter_id = DB::table('migrate_remitters')
												->where('old_remitter_id','=',$remitter_details['id'])
												->value('new_remitter_id');
							
							//CHECK IF NEW REMITTER ID IS EMPTY				
							if(empty($new_remitter_id)){
								
								//NEW REMITTER ID
								$new_remitter_id = DB::table('remitters')
													->where(DB::Raw('TRIM(LOWER(email))'),'=',trim(strtolower($remitter_details['email'])))
													->value('id');
								
							}
							
							//CHECK IF NEW REMITTER ID IS EMPTY				
							if(empty($new_remitter_id)){
								
								//NEW REMITTER ID
								$new_remitter_id = DB::table('remitters')
													->where(DB::Raw('TRIM(LOWER(secondary_email))'),'=',trim(strtolower($remitter_details['email'])))
													->value('id');
								
							}

							//GET RECIPIENT ID FROM OUR NEWLY MIGRATED RECIPIENT DETAILS
							$crm_recipient_query = " SELECT ";
							$crm_recipient_query .= " cmr.new_recipient_id as recipient_id ";
							$crm_recipient_query .= " FROM crm_migrate_recipients as cmr ";
							$crm_recipient_query .= " WHERE cmr.old_recipient_id = {$recipient_details["id"]} ";
							$crm_recipient_query .= " ORDER BY cmr.id ASC LIMIT 1 ";
							$crm_recipient = DB::select(DB::raw($crm_recipient_query));
													
							//GET BANK SOURCE ID FROM OUR NEWLY MIGRATED BANK SOURCE DETAILS
							$bank_source_id = LibraryBankSource::where(DB::Raw('LOWER(TRIM(shortname))'),'=',strtolower(trim($bank_remittance_source_details['bank_shortname'])))
														->first(['id']);
														
							//GET STATUS ID FROM OUR NEWLY MIGRATED STATUS DETAILS
							$status_id = LibraryRemittanceStatus::where(DB::Raw('LOWER(TRIM(name))'),'=',strtolower(trim($remittance['status'])))
														->first(['id']);
														
							//GET REASON ID FROM OUR NEWLY MIGRATED REASON DETAILS
							$reason_id = LibraryReason::where(DB::Raw('LOWER(TRIM(name))'),'=',strtolower(trim($remittance['reason'])))
														->first(['id']);
														
							//GET PROMO CODE ID FROM OUR NEWLY MIGRATED PROMO CODE DETAILS
							$promo_code_id = RemittancePromoCode::where(DB::Raw('LOWER(TRIM(name))'),'=',strtolower(trim($remittance['promo_code'])))
																->first(['id']);
																
																
																

							//RECIPIENT RECEIVE OPTION TRANSFER
							if(!empty($remittance['receive_options_transfer'])){
								
								//OTHER BANKS
								if($remittance['receive_options_transfer'] == 19){

									//OLD RECEIVE OPTION NAME
									$old_receive_option_name = $remittance['other_bank_name'];

								}
								else{

									//OLD RECEIVE OPTION NAME
									$old_receive_option_name = DB::table(DB::Raw('tbl_receive_options_transfer'))
																	->where('id','=',$remittance['receive_options_transfer'])
																	->where('receive_options','=',$receive_option_transfer_details['receive_options'])
																	->first(['name']);

									$old_receive_option_name = $old_receive_option_name['name'];

								}
								
								//CHCK IF RECEIVE OPTION NAME IS NOT EMPTY
								if(!empty($old_receive_option_name)){
									
									//RECIEVE OPTION TRANSFER ID							
									$recieve_option_transfer_id = LibraryReceiveOptionList::where(DB::Raw('LOWER(TRIM(name))'),'=',strtolower(trim($old_receive_option_name)))
																			->where('receive_option_id','=',$receive_option_transfer_details['receive_options'])
																			->whereNotNull('name')
																			->first(['id']);

								}

							}
																
																
																

							$new_remittance = new Remittance;
							$new_remittance->transaction_number = (!empty($remittance['transaction_no']) ? trim($remittance['transaction_no']): NULL);
							$new_remittance->remitter_id = (!empty($new_remitter_id) ? (int) $new_remitter_id : NULL);
							$new_remittance->recipient_id = (!empty($crm_recipient[0]['recipient_id'])? (int) $crm_recipient[0]['recipient_id'] : NULL);
							$new_remittance->receive_option_list_id = (isset($recieve_option_transfer_id['id'])&&!empty($recieve_option_transfer_id['id']) ? $recieve_option_transfer_id['id'] : NULL);
							$new_remittance->payment_type = (!empty($remittance['payment_options']) ? (strtolower(trim($remittance['payment_options'])) == 'polipayment' ||  strtolower(trim($remittance['payment_options'])) == 'polipayments' ? 'polipayment' : 'eft') : NULL);
							$new_remittance->bank_source_id = (!empty($bank_source_id['id']) ? (int) $bank_source_id['id'] : NULL);
							$new_remittance->amount_sent = (!empty($remittance['amount_sent']) ? (double) str_replace(',','',trim($remittance['amount_sent'])) : NULL);
							$new_remittance->status_id = (!empty($status_id['id']) ? (int) $status_id['id'] : NULL);
							$new_remittance->has_receipt = (strtolower(trim($remittance['scanned_receipt'])) == 'yes' ? 1 : 0);
							$new_remittance->service_fee = (!empty($remittance['service_fee']) ? (double) trim($remittance['service_fee']) : NULL);
							$new_remittance->forex_rate = (!empty($remittance['forex_rate']) ? (double) trim($remittance['forex_rate']) : NULL);
							$new_remittance->trade_rate = (!empty($remittance['trade_rate']) ? (double) trim($remittance['trade_rate']) : NULL);
							$new_remittance->reason_id = (!empty($reason_id['id']) ? (int) $reason_id['id'] : NULL);
							$new_remittance->promo_code_id = (!empty($promo_code_id['id']) ? (int) $promo_code_id['id'] : NULL);
							$new_remittance->date_transferred = (!empty($remittance['date_transferred']) ? $remittance['date_transferred'] : NULL);
							$new_remittance->created_at = (!empty($remittance['date_transaction']) ? $remittance['date_transaction'] : NULL);
							$new_remittance->updated_at = NULL;
							$new_remittance->deleted_at = NULL;
							$new_remittance->save();
							$remittance_id = $new_remittance['id'];


							//REMITTANCE FILE
							if(!empty($remittance['file_receipt2'])){
								$new_remittance_receipt = new RemittanceReceipt;
								$new_remittance_receipt->remittance_id = $remittance_id;
								$new_remittance_receipt->filename = trim($remittance['file_receipt2']);
								$new_remittance_receipt->save();
								$new_remittance_receipt_id = $new_remittance_receipt['id'];
							}


							/*
							*----------------------------------
							* GET OLD POLIPAYMENT RECORDS START
							*----------------------------------
							*/

							//GET POLIPAYMENT RECORDS
							$polipayments = DB::table(DB::Raw('tbl_polipayment'))
											->where('remittance','=',$remittance['id'])
											->get();

							//CHECK IF POLIPAYMENTS RECORD EXISTS
							if(count($polipayments)){
								
								//LOOP ALL POLIPAYMENTS
								foreach($polipayments as $polipayment){
									
									//REMITTANCE POLIPAYMENT
									$new_remittance_polipayment = new RemittancePolipayment;
									$new_remittance_polipayment->transaction_number = (!empty($polipayment['transaction_no']) ? trim($polipayment['transaction_no']) : NULL);
									$new_remittance_polipayment->token = (!empty($polipayment['token']) ? trim($polipayment['token']) : NULL);
									$new_remittance_polipayment->reference_number = (!empty($polipayment['referrence_no']) ? trim($polipayment['referrence_no']) : NULL);
									$new_remittance_polipayment->status = (!empty($polipayment['status']) ? trim($polipayment['status']) : NULL);
									$new_remittance_polipayment->content = (!empty($polipayment['content']) ? trim($polipayment['content']) : NULL);
									$new_remittance_polipayment->save();

								}
							
							}

							/*
							*----------------------------------
							* GET OLD POLIPAYMENT RECORDS END
							*----------------------------------
							*/




							/*
							*-----------------------------------
							* GET OLD OTHER REASON RECORDS START
							*-----------------------------------
							*/
							
							//GET POLIPAYMENT RECORDS
							$other_reason = DB::table(DB::Raw('tbl_remittance_other_reason_for_transfer'))
												->where('remittance_id','=',$remittance['id'])
												->first(['reason']);
							
							//REMITTANCE OTHER REASON
							if(!empty($other_reason['other_reason'])){
								$new_remittance_other_reason = new RemittanceOtherReason;
								$new_remittance_other_reason->remittance_id = $remittance_id;
								$new_remittance_other_reason->reason = trim($other_reason['other_reason']);
								$new_remittance_other_reason->save();
								$new_remittance_other_reason_id = $new_remittance_other_reason['id'];
							}
							
							/*
							*-----------------------------------
							* GET OLD OTHER REASON RECORDS END
							*-----------------------------------
							*/




							/*
							*------------------------------
							* GET OLD REMITTANCE NOTE START
							*------------------------------
							*/
							
							//GET POLIPAYMENT RECORDS
							$remittance_notes = DB::table(DB::Raw('tbl_notes'))
													->where('remittance_id','=',$remittance['id'])
													->get();
							
							//CHECK IF REMITTANCE NOTES EXISTS
							if($remittance_notes){
								
								//LOOP ALL REMITTANCE NOTES
								foreach($remittance_notes as $remittance_note){
									
									//REMITTANCE NOTE
									if(!empty($remittance_note['notes'])){
										$new_remittance_note = new RemittanceNote;
										$new_remittance_note->remittance_id = $remittance_id;
										$new_remittance_note->note = trim($remittance_note['notes']);
										$new_remittance_note->created_by = NULL;
										$new_remittance_note->save();
										$new_remittance_note_id = $new_remittance_note['id'];
									}
								
								}
								
							}

							/*
							*------------------------------
							* GET OLD REMITTANCE NOTE END
							*------------------------------
							*/



							/*
							*--------------------------------------
							* GET OLD ADDITIONAL REQUIREMENTS START
							*--------------------------------------
							*/
							
							$additional_requirements = DB::table(DB::Raw('tbl_remittance_additional_requirement'))
														->where('remittance_id','=',$remittance['id'])
														->first(['source_of_income','filename']);

							//REMITTANCE ADDITIONAL REQUIREMENTS
							if(!empty($additional_requirements['source_of_income']) || !empty($additional_requirements['filename'])){
								$new_remittance_additional_requirement = new RemittanceAdditionalRequirement;
								$new_remittance_additional_requirement->remittance_id = $remittance_id;
								$new_remittance_additional_requirement->source_of_income = trim($additional_requirements['source_of_income']);
								$new_remittance_additional_requirement->filename = trim($additional_requirements['filename']);
								$new_remittance_additional_requirement->save();
								$new_remittance_additional_requirement_id = $new_remittance_additional_requirement['id'];
							}

							/*
							*--------------------------------------
							* GET OLD ADDITIONAL REQUIREMENTS END
							*--------------------------------------
							*/



							/*
							*------------------------------------------------------------------
							* LET SYNCED RECORD FROM OLD CORE TABLE TO OUR NEW CORE TABLE START
							*------------------------------------------------------------------
							*/
						
						}
						
					}

					//RECORD REMITTANCE THAT WILL BE PROCESSED
					$new_data = array();
					$new_data['old_remittance_id'] = $remittance['id'];
					$new_data['new_remittance_id'] = $remittance_id;
					$new_data['datetime'] = date('Y-m-d H:i:s');
					DB::table('migrate_remittances')->insert($new_data);
					
					//LOG THAT IT HAS BEEN INSERTED
					\Log::info("Old remittance ID: {$remittance['id']} :: New remittance ID: {$remittance_id}");
					
				}
				
			}
			
		}
		catch(Exception $e){
			\Log::info($e->getMessage());
		}
        
    }
    
    /**
     * Catch the exception.
     *
     * @return void
     */
    public function failed(Exception $e){
		
        \Log::info($e->getMessage());
        
    }
    
    /**
     * Migrate remitter
     *
     * @return boolean
     */
     private function _migrate_remitter($old_remitter_id = 0, $old_recipient_id = 0, $old_remittance_id = 0){
		 
		//CHECK IF RECIPIENT ID IS NOT EMPTY
		if(!empty($old_remitter_id)&&!empty($old_recipient_id)&&!empty($old_remittance_id)){
			
			//CHECK IF REMITTER IS ALREADY SYNCED
			$migrate_remitter_id = DB::table('migrate_remitters')
									->where('old_remitter_id','=',$old_remitter_id)
									->value('id');
								
			//CHECK IF MIGRATE REMITTER ID IS EMPTY
			if(empty($migrate_remitter_id)&&($migrate_remitter_id=='')){
				
				//USERID
				$userid = DB::table(DB::Raw('tbl_remitter_on_list'))
							->where('remitter','=',$old_remitter_id)
							->orderBy('id','DESC')
							->value('userid');

				//CHECK IF USERID IS EMPTY		
				if(empty($userid)){
					
					//USERID
					$userid = DB::table(DB::Raw('tbl_remitter'))
								->where('id','=',$old_remitter_id)
								->value('userid');
								
				}
				
				//REMITTER DETAILS
				$remitter_details = DB::table(DB::Raw('tbl_remitter'))
										->where('id','=',$old_remitter_id)
										->first();

				//USER CAPABILITIES
				$user = User::find($userid)->first();
				$wp_capabilites = unserialize($user->meta->wp_capabilities);
				$admin = (isset($wp_capabilites['administrator'])&&!empty($wp_capabilites['administrator'])?TRUE:FALSE);
				$subscriber = (isset($wp_capabilites['subscriber'])&&!empty($wp_capabilites['subscriber'])?TRUE:FALSE);
				
				//CIVIL STATUS DETAILS		
				$civil_status_details = DB::table('library_civil_statuses')
											->where(DB::Raw('TRIM(LOWER(name))'),'=',trim(strtolower($remitter_details['remitter_civil_status'])))
											->first();
											
				//GET REMITTER RECORD
				$new_remitter_id = Remitter::where('userid','=',$userid)->first(['id']);
				
				//CHECK IF SAVE OR UPDATE
				$new_remitter = array();
				$new_remitter['userid'] = (!empty($userid) ? $userid : NULL);
				$new_remitter['type_id'] = 1; //INDIVIDUAL
				$new_remitter['title_id'] = (!empty($remitter_details['title']) ? $remitter_details['title'] : NULL);
				$new_remitter['firstname'] = (!empty($remitter_details['fname']) ? ucwords(strtolower($remitter_details['fname'])) : NULL);
				$new_remitter['middlename'] = (!empty($remitter_details['mname']) ? ucwords(strtolower($remitter_details['mname'])) : NULL);
				$new_remitter['lastname'] = (!empty($remitter_details['lname']) ? ucwords(strtolower($remitter_details['lname'])) : NULL);
				$new_remitter['gender'] = NULL;
				$new_remitter['civil_status_id'] = (!empty($civil_status_details['id']) ? $civil_status_details['id'] : NULL);
				$new_remitter['birthday'] = NULL;
				$new_remitter['email'] = (!empty($user->user_email) ? trim(strtolower($user->user_email)) : NULL);
				$new_remitter['secondary_email'] = (empty($remitter_details['sec_email']) ? NULL : trim(strtolower($remitter_details['sec_email'])));
				$new_remitter['filename'] = NULL; //NULL BY DEFAULT
				$new_remitter['nationality_id'] = 14; //AUSTRALIA BY DEFAULT
				$new_remitter['email_verified'] = (($admin || $subscriber) ? TRUE : FALSE);
				$new_remitter['sms_verified'] = (!empty($remitter_details['active_status'])||strtotime($user->user_registered->toDateTimeString())<=strtotime('2016-11-08') ? TRUE : FALSE);
				$new_remitter['created_at'] = ($user->user_registered->toDateTimeString()=='0000-00-00 00:00:00' ? date('Y-m-d h:i:s') : $user->user_registered->toDateTimeString());
				
				//FLAG RECORD AS NEW
				$new_record = true;
				
				//UPDATE
				if(!is_null($new_remitter_id)){

					//FLAG RECORD AS EXISTING
					$new_record = false;

					DB::table('remitters')
						->where('id','=',$new_remitter_id['id'])
						->update($new_remitter);

					$remitter_id = $new_remitter_id['id'];

				}
				
				//SAVE
				else{

					$new_remitter['id_number'] = Str::random(8);
					$remitter_id = DB::table('remitters')
									->insertGetId($new_remitter);

				}
				
				//REMITTER CONTACT
				if(!empty($remitter_details['phone1'])){
					
					$remitter_contact_id = RemitterContact::where('remitter_id','=',$remitter_id)
											->first(['id']);

					$new_remitter_contact = array();
					$new_remitter_contact['remitter_id'] = $remitter_id;
					$new_remitter_contact['contact_type_id'] = 1; 
					$new_remitter_contact['contact'] = (!empty($remitter_details['phone1']) ? $remitter_details['phone1'] : NULL);
					$new_remitter_contact['is_preferred'] = 1;

					//UPDATE
					if(!is_null($remitter_contact_id)){
						
						DB::table('remitter_contacts')
							->where('remitter_id','=',$remitter_id)
							->update($new_remitter_contact);
						
						$remitter_contact_id = $remitter_contact_id['id'];
						
					}
					
					//SAVE
					else{
						
						$remitter_contact_id = DB::table('remitter_contacts')
													->insertGetId($new_remitter_contact);

					}
					
				}
				
				//REMITTER ADDRESS
				if(!empty($remitter_details['address_street1'])){
					
					$remitter_address_id = RemitterAddress::where('remitter_id','=',$remitter_id)
															->first(['id']);
											
					$city_details = DB::table('library_cities')
										->where(DB::Raw('TRIM(LOWER(name))'),'=',trim(strtolower($remitter_details['address_city'])))
										->first();
					
					$new_remitter_address = array();
					$new_remitter_address['remitter_id'] = $remitter_id;
					$new_remitter_address['current_address'] = (!empty(trim($remitter_details['address_street1'])) ? trim($remitter_details['address_street1']) : NULL);
					$new_remitter_address['permanent_address'] = (!empty(trim($remitter_details['address_street2'])) ? trim($remitter_details['address_street2']) : NULL);
					$new_remitter_address['country_id'] = 14; //AUSTRALIA
					$new_remitter_address['state_id'] = (!empty($city_details['id']) ? $city_details['state_id'] : NULL);
					$new_remitter_address['city_id'] = (!empty($city_details['id']) ? $city_details['id'] : NULL);
					$new_remitter_address['postal_code'] = (!empty($city_details['id']) ? $city_details['postal_code'] : NULL);
					$new_remitter_address['is_resident'] = 1;
					$new_remitter_address['is_preferred'] = 1;
					
					//UPDATE
					if(!is_null($remitter_address_id)){
						
						DB::table('remitter_addresses')
							->where('remitter_id','=',$remitter_id)
							->update($new_remitter_address);
						
						$remitter_address_id = $remitter_address_id['id'];
						
					}
					
					//SAVE
					else{
						
						$remitter_address_id = DB::table('remitter_addresses')
													->insertGetId($new_remitter_address);

					}
					
				}
				
				//REMITTER IDENTIFICATION
				if(!empty($remitter_details['idnumber'])){
					
					$remitter_identification_id = RemitterIdentification::where('remitter_id','=',$remitter_id)
																		->first(['id']);
													
					$identification_type_details = DB::table('library_identification_types')
													->where(DB::Raw('REPLACE(REPLACE(TRIM(LOWER(name)), "\\\\", ""),"`","\'")'),'=',trim(strtolower($remitter_details['valid_id1'])))
													->first();
					
					$new_remitter_identification = array();
					$new_remitter_identification['remitter_id'] = $remitter_id;
					$new_remitter_identification['number'] = (!empty($remitter_details['idnumber']) ? $remitter_details['idnumber'] : NULL);
					$new_remitter_identification['expiration_date'] = NULL;
					$new_remitter_identification['filename'] = (!empty($remitter_details['file_url2']) ? $remitter_details['file_url2'] : NULL);
					$new_remitter_identification['identification_type_id'] = (!empty($identification_type_details['id']) ? $identification_type_details['id'] : NULL);
					$new_remitter_identification['is_preferred'] = 1;
					
					//UPDATE
					if(!is_null($remitter_identification_id)){
						
						DB::table('remitter_identifications')
							->where('remitter_id','=',$remitter_id)
							->update($new_remitter_identification);
						
						$remitter_identification_id = $remitter_identification_id['id'];
						
					}
					
					//SAVE
					else{
						
						$remitter_identification_id = DB::table('remitter_identifications')
														->insertGetId($new_remitter_identification);

					}
					
				}


				/*
				*--------------------------
				* STAND ALONE TABLE START
				*--------------------------
				*/

				//GET REMITTER ACCESS TOKEN DETAILS
				$remitter_access_token_details = DB::table(DB::Raw('tbl_access_tokens'))
													->where('user_id','=',$user->ID)
													->first();

				//REMITTER ACCESS TOKEN
				if(!empty($remitter_access_token_details['access_token'])){
					
					$remitter_access_token_id = RemitterAccessToken::where('remitter_id','=',$remitter_id)
																	->first(['id']);
					
					$new_remitter_access_token = array();
					$new_remitter_access_token['remitter_id'] = $remitter_id;
					$new_remitter_access_token['access_token'] = (!empty($remitter_access_token_details['access_token']) ? $remitter_access_token_details['access_token'] : NULL);
					$new_remitter_access_token['device_token'] = (!empty($remitter_access_token_details['device_token']) ? $remitter_access_token_details['device_token'] : NULL);
					$new_remitter_access_token['device_type_id'] = (!empty($remitter_access_token_details['type']) ? $remitter_access_token_details['type'] : NULL);
					
					//UPDATE
					if(!is_null($remitter_access_token_id)){
						
						DB::table('remitter_access_tokens')
							->where('remitter_id','=',$remitter_id)
							->update($new_remitter_access_token);
						
						$remitter_access_token_id = $remitter_access_token_id['id'];
						
					}
					
					//SAVE
					else{
						
						$remitter_access_token_id = DB::table('remitter_access_tokens')
														->insertGetId($new_remitter_access_token);

					}
					
				}


				//GET REMITTER CONFIRMATION SMS DETAILS
				$remitter_cofirmation_sms_details = DB::table(DB::Raw('tbl_remitter_confirmation_sms'))
														->where('remitter_id','=',$remitter_details['id'])
														->first();

				//REMITTER CONFIRMATION SMS
				if(!empty($remitter_cofirmation_sms_details['code'])){
					
					$remitter_confirmation_sms_id = RemitterConfirmationSms::where('remitter_id','=',$remitter_id)
																			->first(['id']);
																			
					$new_remitter_confirmation_sms = array();
					$new_remitter_confirmation_sms['remitter_id'] = $remitter_id;
					$new_remitter_confirmation_sms['code'] = (!empty($remitter_cofirmation_sms_details['code']) ? $remitter_cofirmation_sms_details['code'] : NULL);
					$new_remitter_confirmation_sms['count'] = (!empty($remitter_cofirmation_sms_details['count']) ? $remitter_cofirmation_sms_details['count'] : NULL);

					//UPDATE
					if(!is_null($remitter_confirmation_sms_id)){
						
						DB::table('remitter_confirmation_sms')
							->where('remitter_id','=',$remitter_id)
							->update($new_remitter_confirmation_sms);
						
						$remitter_confirmation_sms_id = $remitter_confirmation_sms_id['id'];
						
					}
					
					//SAVE
					else{
						
						$remitter_confirmation_sms_id = DB::table('remitter_confirmation_sms')
														->insertGetId($new_remitter_confirmation_sms);

					}
					
				}


				/*
				*--------------------------
				* STAND ALONE TABLE END
				*--------------------------
				*/

				//INSERT RECORD
				if($new_record){
					
					//LOG THAT IT HAS BEEN INSERTED
					\Log::info("Userid: {$userid} :: Old remitter ID: {$remitter_details['id']} :: New remitter ID: {$remitter_id} has been inserted!");
				
				}
				
				//UPDATE RECORD
				else{
					
					//LOG THAT IT HAS BEEN UPDATED
					\Log::info("Userid: {$userid} :: Old remitter ID: {$remitter_details['id']} :: New remitter ID: {$remitter_id} has been updated!");
					
				}
				
				//RECORD REMITTER THAT WILL BE PROCESSED
				$new_data = array();
				$new_data['userid'] = $userid;
				$new_data['old_remitter_id'] = $remitter_details['id'];
				$new_data['new_remitter_id'] = $remitter_id;
				$new_data['datetime'] = date('Y-m-d H:i:s');
				DB::table('migrate_remitters')->insert($new_data);

			}

			//RETURN TRUE
			return true;
		
		}
		
		//RETURN FALSE
		return false;
		 
	 }
	 

    /**
     * Migrate recipient
     *
     * @return boolean
     */
	private function _migrate_recipient($old_remitter_id = 0, $old_recipient_id = 0, $old_remittance_id = 0){
		
		//CHECK IF RECIPIENT ID IS NOT EMPTY
		if(!empty($old_remitter_id)&&!empty($old_recipient_id)&&!empty($old_remittance_id)){
			
			//CHECK IF REMITTER RECIPIENT IS ALREADY SYNCED
			$migrate_recipient_id = DB::table('migrate_recipients')
										->where('old_recipient_id','=',$old_recipient_id)
										->value('id');

			//CHECK IF MIGRATE RECIPEINT ID IS EMPTY
			if(empty($migrate_recipient_id)&&($migrate_recipient_id=='')){
				
				//GET OLD REMITTER DETAILS
				$remitter_details = DB::table(DB::Raw('tbl_remitter'))
										->where('id','=',$old_remitter_id)
										->first();
										
				//CHECK IF REMITTER RECIPIENT IS ALREADY SYNCED
				$new_remitter_id = DB::table('migrate_remitters')
									->where('old_remitter_id','=',$remitter_details['id'])
									->value('new_remitter_id');
									
				//CHECK IF NEW REMITTER ID IS EMPTY				
				if(empty($new_remitter_id)){
					
					//NEW REMITTER ID
					$new_remitter_id = DB::table('remitters')
										->where(DB::Raw('TRIM(LOWER(email))'),'=',trim(strtolower($remitter_details['email'])))
										->value('id');
					
				}
				
				//CHECK IF NEW REMITTER ID IS EMPTY				
				if(empty($new_remitter_id)){
					
					//NEW REMITTER ID
					$new_remitter_id = DB::table('remitters')
										->where(DB::Raw('TRIM(LOWER(secondary_email))'),'=',trim(strtolower($remitter_details['email'])))
										->value('id');
					
				}
									
				//GET OLD RECIPIENT RECIPIENT
				$recipient_details = DB::table(DB::Raw('tbl_recipient'))
										->where('id','=',$old_recipient_id)
										->first();
				
				//GET OLD REMITTANCE DETAILS
				$remittance_details = DB::table(DB::Raw('tbl_remittance'))
										->where('id','=',$old_remittance_id)
										->orderBy('id','DESC')
										->first();

				//GET OLD RECIEVE OPTION TRANSER
				$recieve_option_id = DB::table(DB::Raw('tbl_receive_options_transfer'))
										->where('id','=',$remittance_details['receive_options_transfer'])
										->first(['receive_options']);
				
				//GET OLD CIVIL STATUS DETAILS		
				$civil_status_details = DB::table('library_civil_statuses')
										->where(DB::Raw('TRIM(LOWER(name))'),'=',trim(strtolower($recipient_details['recipient_civil_status'])))
										->first();

				//RECIPIENT					
				$new_recipient = array();
				$new_recipient['id_number'] = Str::random(8);
				$new_recipient['remitter_id'] = $new_remitter_id;
				$new_recipient['email'] = (!empty($recipient_details['recipient_email']) ? strtolower(trim($recipient_details['recipient_email'])) : NULL);
				$new_recipient['secondary_email'] = NULL;
				$new_recipient['filename'] = NULL;
				$new_recipient['remitter_relationship_id'] = (!empty($recipient_details['relationship_id']) ? $recipient_details['relationship_id'] : NULL);
				$new_recipient['type'] = (!empty($recipient_details['type'])&&strtolower(trim($recipient_details['type']))=='company' ? 'company' : 'person');
				$new_recipient['is_active'] = (strtolower(trim($remittance_details['disable_recipient'])) == 'yes' ? 0 : 1);
				$new_recipient['created_at'] = ($remittance_details['date_transaction']=='0000-00-00 00:00:00' ? date('Y-m-d h:i:s') : $remittance_details['date_transaction']);
				$new_recipient_id = DB::table('recipients')->insertGetId($new_recipient);

				//RECIPIENT COMPANY
				if(isset($recipient_details['type'])&&!empty($recipient_details['type'])&&strtolower(trim($recipient_details['type']))=='company'){
					
					$new_recipient_company = array();
					$new_recipient_company['recipient_id'] = $new_recipient_id;
					$new_recipient_company['company_name'] = (!empty($recipient_details['fname']) ? ucwords(strtolower($recipient_details['fname'])) : NULL);
					$new_recipient_company['reg_number'] = (!empty($recipient_details['reg_no']) ? $recipient_details['reg_no'] : NULL);
					$new_recipient_company['contact_person'] = (!empty($recipient_details['contact_person']) ? $recipient_details['contact_person'] : NULL);
					$new_recipient_company['authorized_person'] = (!empty($recipient_details['authorized_person']) ? $recipient_details['authorized_person'] : NULL);
					$recipient_company_id = DB::table('recipient_companies')->insertGetId($new_recipient_company);
					
				}

				//RECIPIENT PERSON
				else {

					$new_recipient_person = array();
					$new_recipient_person['recipient_id'] = $new_recipient_id;
					$new_recipient_person['title_id'] = (!empty($recipient_details['title']) ? $recipient_details['title'] : NULL);
					$new_recipient_person['firstname'] = (!empty($recipient_details['fname']) ? ucwords(strtolower($recipient_details['fname'])) : NULL);
					$new_recipient_person['middlename'] = (!empty($recipient_details['mname']) ? ucwords(strtolower($recipient_details['mname'])) : NULL);
					$new_recipient_person['lastname'] = (!empty($recipient_details['lname']) ? ucwords(strtolower($recipient_details['lname'])) : NULL);
					$new_recipient_person['gender'] = NULL;
					$new_recipient_person['birthday'] = NULL;
					$new_recipient_person['civil_status_id'] = (!empty($civil_status_details['id']) ? $civil_status_details['id'] : NULL);
					$new_recipient_person['nationality_id'] = 175; //PHILIPPINES
					$recipient_person_id = DB::table('recipient_persons')->insertGetId($new_recipient_person);

				}

				//RECIPIENT CONTACT
				if(!empty($recipient_details['phone1'])){
					
					$new_recipient_contact = array();
					$new_recipient_contact['recipient_id'] = $new_recipient_id;
					$new_recipient_contact['contact_type_id'] = 1; 
					$new_recipient_contact['contact'] = (!empty($recipient_details['phone1']) ? $recipient_details['phone1'] : NULL);
					$new_recipient_contact['is_preferred'] = 1;
					$recipient_contact_id = DB::table('recipient_contacts')->insertGetId($new_recipient_contact);
				
				}

				//RECIPIENT ADDRESS
				if(!empty($recipient_details['address_street1'])){

					//CITY DETAILS
					$city_details = DB::table('library_cities')
										->where(DB::Raw('TRIM(LOWER(name))'),'=',trim(strtolower($recipient_details['address_city'])))
										->first();
					
					$new_recipient_address = array();
					$new_recipient_address['recipient_id'] = $new_recipient_id;
					$new_recipient_address['current_address'] = (!empty(trim($recipient_details['address_street1'])) ? trim($recipient_details['address_street1']) : NULL);
					$new_recipient_address['permanent_address'] = (!empty(trim($recipient_details['address_street2'])) ? trim($recipient_details['address_street2']) : NULL);
					$new_recipient_address['country_id'] = 175; //PHILIPPINES
					$new_recipient_address['state_id'] = (!empty($city_details['id']) ? $city_details['state_id'] : NULL);
					$new_recipient_address['city_id'] = (!empty($city_details['id']) ? $city_details['id'] : NULL);
					$new_recipient_address['postal_code'] = (!empty($recipient_details['zip']) ? $recipient_details['zip'] : NULL);
					$new_recipient_address['is_preferred'] = 1;
					
					$recipient_address_id = DB::table('recipient_addresses')->insertGetId($new_recipient_address);
					
				}

				//RECIPIENT RECEIVE OPTION TRANSFER
				if(!empty($remittance_details['receive_options_transfer'])){
					
					//OTHER BANKS
					if($remittance_details['receive_options_transfer'] == 19){

						//OLD RECEIVE OPTION NAME
						$old_receive_option_name = $remittance_details['other_bank_name'];

					}
					else{

						//OLD RECEIVE OPTION NAME
						$old_receive_option_name = DB::table(DB::Raw('tbl_receive_options_transfer'))
														->where('id','=',$remittance_details['receive_options_transfer'])
														->where('receive_options','=',$recieve_option_id['receive_options'])
														->first(['name']);

						$old_receive_option_name = $old_receive_option_name['name'];

					}
					
					//CHCK IF RECEIVE OPTION NAME IS NOT EMPTY
					if(!empty($old_receive_option_name)){
						
						//RECIEVE OPTION TRANSFER ID							
						$recieve_option_transfer_id = LibraryReceiveOptionList::where(DB::Raw('LOWER(TRIM(name))'),'=',strtolower(trim($old_receive_option_name)))
																->where('receive_option_id','=',$recieve_option_id['receive_options'])
																->whereNotNull('name')
																->first(['id']);

					}

				}

				//RECIPIENT BANK DETAILS AND RECEIVE OPTION TRANSFER IS BANK TRANSFER
				if(!empty($remittance_details['bank_accnt_no'])&&(!empty($recieve_option_id['receive_options'])&&$recieve_option_id['receive_options']==2)){	
				
					$new_recipient_bank = array();
					$new_recipient_bank['recipient_id'] = $new_recipient_id;
					$new_recipient_bank['receive_option_list_id'] = (!empty($recieve_option_transfer_id['id']) ? $recieve_option_transfer_id['id'] : NULL);
					$new_recipient_bank['account_name'] = (!empty($remittance_details['bank_accnt_name']) ? $remittance_details['bank_accnt_name'] : NULL);
					$new_recipient_bank['account_branch'] = (!empty($remittance_details['bank_branch']) ? $remittance_details['bank_branch'] : NULL);
					$new_recipient_bank['account_no'] = (!empty($remittance_details['bank_accnt_no']) ? $remittance_details['bank_accnt_no'] : NULL);
					$new_recipient_bank['is_preferred'] = 1;
					$recipient_bank_id = DB::table('recipient_banks')->insertGetId($new_recipient_bank);
				
				}

				//RECORD REMITTER RECIPIENT AND NEW RECIPIENT ID THAT WILL BE PROCESSED
				$new_recipient_data = array();
				$new_recipient_data['new_remitter_id'] = $new_remitter_id;
				$new_recipient_data['old_recipient_id'] = $old_recipient_id;
				$new_recipient_data['new_recipient_id'] = $new_recipient_id;
				$new_recipient_data['firstname'] = trim(strtolower($recipient_details['fname']));
				$new_recipient_data['lastname'] = trim(strtolower($recipient_details['lname']));
				$new_recipient_data['datetime'] = date('Y-m-d H:i:s');
				DB::table('migrate_recipients')->insert($new_recipient_data);
				
				//LOG INFO
				\Log::info("New remitter ID: {$new_remitter_id} :: Old Recipient ID: {$old_recipient_id} :: New Recipient ID: {$new_recipient_id} has been inserted!");

				//RETURN TRUE
				return true;

			}

			//RETURN TRUE
			return true;

		}

		//RETURN FALSE
		return false;
		
	}

}
