<?php

namespace App\Jobs;

use Exception;

use Corcel\User;
use App\Models\Remitter;
use App\Models\RemitterContact;
use App\Models\RemitterAddress;
use App\Models\RemitterIdentification;
use App\Models\RemitterAccessToken;
use App\Models\RemitterConfirmationSms;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MigrateRemitter implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(){

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
		
		try {
			
			//NO TIME LIMIT
			set_time_limit(0);
		
			//WE DONT END THE LOOP
			while(true){
				
				//MIGRATE REMITTER
				$remitter_query = " SELECT ";
				$remitter_query .= " wu.ID, ";
				$remitter_query .= " wu.user_email, ";
				$remitter_query .= " wu.user_registered ";
				$remitter_query .= " FROM wp_users AS wu ";
				$remitter_query .= " WHERE wu.ID NOT IN (SELECT userid FROM crm_migrate_remitters) ";
				$remitter_query .= " ORDER BY wu.ID ASC LIMIT 500 ";
				
				//GET ALL RECORDS
				$remitters = array(); //RESET REMITTERS
				$remitters = DB::select(DB::raw($remitter_query));

				//CHECK IF REMITTER IS NOT EMPTY
				if(empty($remitters)){
					
					break;
					
				}

				//REMITTER MODEL
				foreach($remitters as $remitter){
					
					//CHECK IF REMITTER IS ALREADY SYNCED
					$migrate_remitter_id = DB::table('migrate_remitters')
											->where('userid',trim($remitter['ID']))
											->value('id');
					
					//CHECK IF MIGRATE REMITTER ID IS EMPTY
					if(empty($migrate_remitter_id)&&($migrate_remitter_id=='')){
						
						//USER CAPABILITIES
						$user = User::find($remitter['ID']);
						$wp_capabilites = unserialize($user->meta->wp_capabilities);
						$admin = (isset($wp_capabilites['administrator'])&&!empty($wp_capabilites['administrator'])?TRUE:FALSE);
						$subscriber = (isset($wp_capabilites['subscriber'])&&!empty($wp_capabilites['subscriber'])?TRUE:FALSE);
						
						//REMITTER DETAILS
						$remitter_details = DB::table(DB::Raw('tbl_remitter'))
												->where(DB::Raw('TRIM(LOWER(email))'),'=',trim(strtolower($remitter['user_email'])))
												->orderBy('id','DESC')
												->first();
												
						//CHECK IF REMITTER DETAILS IS EMPTY
						if(empty($remitter_details)){
							
							//REMITTER DETAILS
							$remitter_details = DB::table(DB::Raw('tbl_remitter'))
													->where('userid','=',$remitter['ID'])
													->orderBy('id','DESC')
													->first();
							
						}
						
						//CIVIL STATUS DETAILS		
						$civil_status_details = DB::table('library_civil_statuses')
													->where(DB::Raw('TRIM(LOWER(name))'),'=',trim(strtolower($remitter_details['remitter_civil_status'])))
													->first();
													
						//GET REMITTER RECORD
						$new_remitter_id = Remitter::where('userid','=',$remitter['ID'])
													->first(['id']);
						
						//CHECK IF SAVE OR UPDATE
						$new_remitter = array();
						$new_remitter['userid'] = (!empty($remitter['ID']) ? $remitter['ID'] : NULL);
						$new_remitter['type_id'] = 1; //INDIVIDUAL
						$new_remitter['title_id'] = (!empty($remitter_details['title']) ? $remitter_details['title'] : NULL);
						$new_remitter['firstname'] = (!empty($remitter_details['fname']) ? ucwords(strtolower($remitter_details['fname'])) : NULL);
						$new_remitter['middlename'] = (!empty($remitter_details['mname']) ? ucwords(strtolower($remitter_details['mname'])) : NULL);
						$new_remitter['lastname'] = (!empty($remitter_details['lname']) ? ucwords(strtolower($remitter_details['lname'])) : NULL);
						$new_remitter['gender'] = NULL;
						$new_remitter['civil_status_id'] = (!empty($civil_status_details['id']) ? $civil_status_details['id'] : NULL);
						$new_remitter['birthday'] = NULL;
						$new_remitter['email'] = (!empty($remitter['user_email']) ? trim(strtolower($remitter['user_email'])) : NULL);
						$new_remitter['secondary_email'] = (empty($remitter_details['sec_email']) ? NULL : trim(strtolower($remitter_details['sec_email'])));
						$new_remitter['filename'] = NULL; //NULL BY DEFAULT
						$new_remitter['nationality_id'] = 14; //AUSTRALIA BY DEFAULT
						$new_remitter['email_verified'] = (($admin || $subscriber) ? TRUE : FALSE);
						$new_remitter['sms_verified'] = (!empty($remitter_details['active_status'])||strtotime($remitter['user_registered'])<=strtotime('2016-11-08') ? TRUE : FALSE);
						$new_remitter['created_at'] = ($remitter['user_registered']=='0000-00-00 00:00:00' ? date('Y-m-d h:i:s') : $remitter['user_registered']);
 						
						//FLAG RECORD AS NEW
						$new_record = true;
						
						//UPDATE
						if(!is_null($new_remitter_id)){
							
							//FLAG RECORD AS EXISTING
							$new_record = false;
							
							DB::table('remitters')
								->where('id','=',$new_remitter_id['id'])
								->update($new_remitter);
								
							$remitter_id = $new_remitter_id['id'];
							
						}
						
						//SAVE
						else{
							$new_remitter['id_number'] = Str::random(8);
							$remitter_id = DB::table('remitters')
												->insertGetId($new_remitter);
						}
						
						//REMITTER CONTACT
						if(!empty($remitter_details['phone1'])){
							
							$remitter_contact_id = RemitterContact::where('remitter_id','=',$remitter_id)
													->first(['id']);

							$new_remitter_contact = array();
							$new_remitter_contact['remitter_id'] = $remitter_id;
							$new_remitter_contact['contact_type_id'] = 1; 
							$new_remitter_contact['contact'] = (!empty($remitter_details['phone1']) ? $remitter_details['phone1'] : NULL);
							$new_remitter_contact['is_preferred'] = 1;

							//UPDATE
							if(!is_null($remitter_contact_id)){
								
								DB::table('remitter_contacts')
									->where('remitter_id','=',$remitter_id)
									->update($new_remitter_contact);
								
								$remitter_contact_id = $remitter_contact_id['id'];
								
							}
							
							//SAVE
							else{
								
								$remitter_contact_id = DB::table('remitter_contacts')
															->insertGetId($new_remitter_contact);

							}
							
						}
						
						//REMITTER ADDRESS
						if(!empty($remitter_details['address_street1'])){
							
							$remitter_address_id = RemitterAddress::where('remitter_id','=',$remitter_id)
																	->first(['id']);
													
							$city_details = DB::table('library_cities')
												->where(DB::Raw('TRIM(LOWER(name))'),'=',trim(strtolower($remitter_details['address_city'])))
												->first();
							
							$new_remitter_address = array();
							$new_remitter_address['remitter_id'] = $remitter_id;
							$new_remitter_address['current_address'] = (!empty(trim($remitter_details['address_street1'])) ? trim($remitter_details['address_street1']) : NULL);
							$new_remitter_address['permanent_address'] = (!empty(trim($remitter_details['address_street2'])) ? trim($remitter_details['address_street2']) : NULL);
							$new_remitter_address['country_id'] = 14; //AUSTRALIA
							$new_remitter_address['state_id'] = (!empty($city_details['id']) ? $city_details['state_id'] : NULL);
							$new_remitter_address['city_id'] = (!empty($city_details['id']) ? $city_details['id'] : NULL);
							$new_remitter_address['postal_code'] = (!empty($city_details['id']) ? $city_details['postal_code'] : NULL);
							$new_remitter_address['is_resident'] = 1;
							$new_remitter_address['is_preferred'] = 1;
							
							//UPDATE
							if(!is_null($remitter_address_id)){
								
								DB::table('remitter_addresses')
									->where('remitter_id','=',$remitter_id)
									->update($new_remitter_address);
								
								$remitter_address_id = $remitter_address_id['id'];
								
							}
							
							//SAVE
							else{
								
								$remitter_address_id = DB::table('remitter_addresses')
															->insertGetId($new_remitter_address);

							}
							
						}
						
						//REMITTER IDENTIFICATION
						if(!empty($remitter_details['idnumber'])){
							
							$remitter_identification_id = RemitterIdentification::where('remitter_id','=',$remitter_id)
																				->first(['id']);
															
							$identification_type_details = DB::table('library_identification_types')
															->where(DB::Raw('REPLACE(REPLACE(TRIM(LOWER(name)), "\\\\", ""),"`","\'")'),'=',trim(strtolower($remitter_details['valid_id1'])))
															->first();
							
							$new_remitter_identification = array();
							$new_remitter_identification['remitter_id'] = $remitter_id;
							$new_remitter_identification['number'] = (!empty($remitter_details['idnumber']) ? $remitter_details['idnumber'] : NULL);
							$new_remitter_identification['expiration_date'] = NULL;
							$new_remitter_identification['filename'] = (!empty($remitter_details['file_url2']) ? $remitter_details['file_url2'] : NULL);
							$new_remitter_identification['identification_type_id'] = (!empty($identification_type_details['id']) ? $identification_type_details['id'] : NULL);
							$new_remitter_identification['is_preferred'] = 1;
							
							//UPDATE
							if(!is_null($remitter_identification_id)){
								
								DB::table('remitter_identifications')
									->where('remitter_id','=',$remitter_id)
									->update($new_remitter_identification);
								
								$remitter_identification_id = $remitter_identification_id['id'];
								
							}
							
							//SAVE
							else{
								
								$remitter_identification_id = DB::table('remitter_identifications')
																->insertGetId($new_remitter_identification);

							}
							
						}


						/*
						*--------------------------
						* STAND ALONE TABLE START
						*--------------------------
						*/

						//GET REMITTER ACCESS TOKEN DETAILS
						$remitter_access_token_details = DB::table(DB::Raw('tbl_access_tokens'))
															->where('user_id','=',$remitter['ID'])
															->first();

						//REMITTER ACCESS TOKEN
						if(!empty($remitter_access_token_details['access_token'])){
							
							$remitter_access_token_id = RemitterAccessToken::where('remitter_id','=',$remitter_id)
																			->first(['id']);
							
							$new_remitter_access_token = array();
							$new_remitter_access_token['remitter_id'] = $remitter_id;
							$new_remitter_access_token['access_token'] = (!empty($remitter_access_token_details['access_token']) ? $remitter_access_token_details['access_token'] : NULL);
							$new_remitter_access_token['device_token'] = (!empty($remitter_access_token_details['device_token']) ? $remitter_access_token_details['device_token'] : NULL);
							$new_remitter_access_token['device_type_id'] = (!empty($remitter_access_token_details['type']) ? $remitter_access_token_details['type'] : NULL);
							
							//UPDATE
							if(!is_null($remitter_access_token_id)){
								
								DB::table('remitter_access_tokens')
									->where('remitter_id','=',$remitter_id)
									->update($new_remitter_access_token);
								
								$remitter_access_token_id = $remitter_access_token_id['id'];
								
							}
							
							//SAVE
							else{
								
								$remitter_access_token_id = DB::table('remitter_access_tokens')
																->insertGetId($new_remitter_access_token);

							}
							
						}


						//GET REMITTER CONFIRMATION SMS DETAILS
						$remitter_cofirmation_sms_details = DB::table(DB::Raw('tbl_remitter_confirmation_sms'))
																->where('remitter_id','=',$remitter_details['id'])
																->first();

						//REMITTER CONFIRMATION SMS
						if(!empty($remitter_cofirmation_sms_details['code'])){
							
							$remitter_confirmation_sms_id = RemitterConfirmationSms::where('remitter_id','=',$remitter_id)
																					->first(['id']);
																					
							$new_remitter_confirmation_sms = array();
							$new_remitter_confirmation_sms['remitter_id'] = $remitter_id;
							$new_remitter_confirmation_sms['code'] = (!empty($remitter_cofirmation_sms_details['code']) ? $remitter_cofirmation_sms_details['code'] : NULL);
							$new_remitter_confirmation_sms['count'] = (!empty($remitter_cofirmation_sms_details['count']) ? $remitter_cofirmation_sms_details['count'] : NULL);
							$new_remitter_confirmation_sms['created_at'] = (!empty($remitter_cofirmation_sms_details['date']) ? $remitter_cofirmation_sms_details['date'] : NULL);

							//UPDATE
							if(!is_null($remitter_confirmation_sms_id)){
								
								DB::table('remitter_confirmation_sms')
									->where('remitter_id','=',$remitter_id)
									->update($new_remitter_confirmation_sms);
								
								$remitter_confirmation_sms_id = $remitter_confirmation_sms_id['id'];
								
							}
							
							//SAVE
							else{
								
								$remitter_confirmation_sms_id = DB::table('remitter_confirmation_sms')
																->insertGetId($new_remitter_confirmation_sms);

							}
							
						}


						/*
						*--------------------------
						* STAND ALONE TABLE END
						*--------------------------
						*/

						//INSERT RECORD
						if($new_record){
							
							//LOG THAT IT HAS BEEN INSERTED
							\Log::info("Userid: {$remitter['ID']} :: Old remitter ID: {$remitter_details['id']} :: New remitter ID: {$remitter_id} has been inserted!");
						
						}
						
						//UPDATE RECORD
						else{
							
							//LOG THAT IT HAS BEEN UPDATED
							\Log::info("Userid: {$remitter['ID']} :: Old remitter ID: {$remitter_details['id']} :: New remitter ID: {$remitter_id} has been updated!");
							
						}
						
						//RECORD REMITTER THAT WILL BE PROCESSED
						$new_data = array();
						$new_data['userid'] = $remitter['ID'];
						$new_data['old_remitter_id'] = $remitter_details['id'];
						$new_data['new_remitter_id'] = $remitter_id;
						$new_data['datetime'] = date('Y-m-d H:i:s');
						DB::table('migrate_remitters')->insert($new_data);

					}

				}

			}

		}
		catch(Exception $e){
			
			\Log::info($e->getMessage());
			
		}
	
    }
    
    /**
     * Catch the exception.
     *
     * @return void
     */
    public function failed(Exception $e){
		
        \Log::info($e->getMessage());
        
    }

}
