<?php

namespace App\Jobs;

use Exception;

use Corcel\User;
use App\Models\Recipient;
use App\Models\Remittance;
use App\Models\Migration\MigrateRecipient;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MigrateRemoveRecipientDup implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
		
		try{

			//NO TIME LIMIT
			set_time_limit(0);

			while(true){

				$recipient_dups_query = " SELECT ";
				$recipient_dups_query .= " COUNT(id) AS total_dup,  ";
				$recipient_dups_query .= " new_remitter_id AS new_remitter_id,  ";
				$recipient_dups_query .= " firstname AS recipient_firstname,  ";
				$recipient_dups_query .= " lastname AS recipient_lastname  ";
				$recipient_dups_query .= " FROM crm_migrate_recipients  ";
				$recipient_dups_query .= " GROUP BY new_remitter_id, firstname, lastname  ";
				$recipient_dups_query .= " HAVING total_dup > 1 ";
				$recipient_dups_query .= " ORDER BY id ASC LIMIT 500 ";
				
				$recipient_dups = array();
				$recipient_dups = DB::select(DB::raw($recipient_dups_query));
				\Log::info(print_r($recipient_dups, true));
				
				//CHECK IF REMITTANCES IS NOT EMPTY
				if(empty($recipient_dups)){
					
					break;
					
				}
				
				//LOOP ALL RECIPIENT DUPS
				foreach($recipient_dups as $recipient_dup){

					//GET ALL NEW RECIPIENT ID
					$migrate_recipients = DB::table('migrate_recipients')
											->where('new_remitter_id','=',$recipient_dup['new_remitter_id'])
											->where('firstname','=',$recipient_dup['recipient_firstname'])
											->where('lastname','=',$recipient_dup['recipient_lastname'])
											->orderBy('id','ASC')
											->get();

					//CHECK COUNT MIGRATE RECIPIENT
					if(count($migrate_recipients)){
						
						$x = 1;
						$first_existing_recipient_id = 0;
						
						//LOOP ALL MIGRATE RECIPIENT
						foreach($migrate_recipients as $migrate_recipient){
							
							//CHECK IF X IS EQUAL TO 1
							if($x == 1){
								
								//FIRST EXISTING RECIPIENT ID
								$first_existing_recipient_id = $migrate_recipient['new_recipient_id'];
								
							}
							
							//GET NEW REMITTER ID AND NEW RECIPIENT ID
							$migrate_recipient_new_remitter_id = $migrate_recipient['new_remitter_id'];
							$migrate_recipient_new_recipient_id = $migrate_recipient['new_recipient_id'];
							
							//GET ALL REMITTANCES HAVING THE SAME REMITTER ID AND RECIPIENT ID
							$remittances = Remittance::where([['remitter_id','=',$migrate_recipient_new_remitter_id],['recipient_id','=',$migrate_recipient_new_recipient_id]])->get(['id']);
							
							//CHECK COUNT REMITTANCES
							if(count($remittances)){
								
								//LOOP ALL REMITTANCES
								foreach($remittances as $remittance){
									
									//GET REMITTANCE ID
									$remittance_id = $remittance['id'];
									
									//UPDATE REMITTANCE CHANGE RECIPIENT ID
									$new_remittance = Remittance::find($remittance_id);
									$new_remittance->recipient_id = $first_existing_recipient_id;
									$new_remittance->save();
									$remittance_id = $new_remittance['id'];
									
								}
								
							}
							
							//CHECK IF FIRST EXISTING RECIPIENT ID NOT EQUAL TO MIGRATE RECIPIENT NEW RECIPIENT ID
							if($first_existing_recipient_id != $migrate_recipient['new_recipient_id']){

								//DELETE RECORD FROM MIGRATE RECIPIENT
								MigrateRecipient::find($migrate_recipient['id'])->forceDelete();
								\Log::info("Migrate Recipient ID: {$migrate_recipient['id']} has been deleted!");

								//DELETE RECORD FROM RECIPIENTS TABLE
								Recipient::find($migrate_recipient['new_recipient_id'])->forceDelete();
								\Log::info("Recipient ID: {$migrate_recipient['new_recipient_id']} has been deleted!");
							
							}
							
							//INCREMENT
							$x++;

						}
						
					}
					
				}

			}

		}
		
		catch(Exception $e){
			\Log::info($e->getMessage());
		}
    
    }
    
    /**
     * Catch the exception.
     *
     * @return void
     */
    public function failed(Exception $e){
		
        \Log::info($e->getMessage());
        
    }

}
