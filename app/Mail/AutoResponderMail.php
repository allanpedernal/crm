<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AutoResponderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $email;

    protected $transaction_data;

    public function __construct( $email, $transaction_data )
    {
        //
        $this->email = $email;

        $this->transaction_data = $transaction_data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
 
        $from = 'peter_john-814b58@inbox.mailtrap.io';

        $from_name = 'I-Remit to the Philippines';

        $amount = $this->transaction_data[ 'remittance_amount' ];

        $service_fee = $this->transaction_data[ 'transfer_fee' ];

        $subject = 'Remittance Advice of $'.$service_fee;

        return $this->view( $this->email )
 
                    ->with( 'transaction_data', $this->transaction_data )

                    ->from( $from, $from_name )

                    ->subject( $subject )

        ;

    }

}
