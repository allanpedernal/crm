<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    private $css;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( )
    {

        //
        //$this->$css = $css; //pass this to view using with

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.templates.beautyMailMinty');

    }
}
