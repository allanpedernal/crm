<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessReferrals extends Model {
	
	
	use SoftDeletes;

	use \Venturecraft\Revisionable\RevisionableTrait;
	

	protected $revisionEnabled = true;

	protected $dates = [ 'deleted_at' ];

	public $timestamps = true;
	
    
    //BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() {
		
        parent::boot();
        
    }

	//REMITTER
	public function remitter() {
		
		return $this -> hasOne( 'App\Models\Remitter', 'id', 'remitter_id' );
		
	}

}
