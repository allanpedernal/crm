<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryBankType extends Model {
	
    public $timestamps = false;
    
}
