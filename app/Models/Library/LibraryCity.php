<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryCity extends Model {
	
    public $timestamps = false;
    
}
