<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryCivilStatus extends Model {
	
    public $timestamps = false;
    
}
