<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryContactType extends Model {
	
    public $timestamps = false;
    
}
