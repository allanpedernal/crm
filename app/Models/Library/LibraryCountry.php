<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryCountry extends Model {
	
    public $timestamps = false;
    
}
