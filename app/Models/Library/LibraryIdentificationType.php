<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryIdentificationType extends Model {
	
    public $timestamps = false;
    
}
