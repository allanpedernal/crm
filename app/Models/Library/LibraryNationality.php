<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryNationality extends Model {
	
    public $timestamps = false;
    
}
