<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryNatureOfWork extends Model {
	
    public $timestamps = false;
    
}
