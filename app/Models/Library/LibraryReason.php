<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryReason extends Model {
	
    public $timestamps = false;
    
}
