<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryReceiveOptionList extends Model {
	
	public $timestamps = false;
	
	//GET LAST RECEIVE OPTION DETAILS
	public function get_last_receive_options_details( $bank_source_id = 0  )  {
            
        if( ! empty( $bank_source_id ) || ! is_null( $bank_source_id ) )  {
                
			$last_receive_options = $this -> find( $bank_source_id )

										-> orderBy( 'id', 'DESC' )

										-> first( );
                
            return $last_receive_options;
                
		}

		return array();
            
    }
	
	//RECEIVE OPTION
	public function receive_option() {
		
		return $this -> hasOne( '\App\Models\Library\LibraryReceiveOption', 'id', 'receive_option_id' );
		
	}

}
