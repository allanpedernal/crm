<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryRemittanceFrequency extends Model {
	
    public $timestamps = false;
    
}
