<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryRemittanceStatus extends Model {
	
    public $timestamps = false;
    
}
