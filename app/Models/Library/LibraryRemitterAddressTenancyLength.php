<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryRemitterAddressTenancyLength extends Model {
	
    public $timestamps = false;
    
}
