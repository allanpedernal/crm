<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryRemitterMarketingSource extends Model {
	
    public $timestamps = false;
    
}
