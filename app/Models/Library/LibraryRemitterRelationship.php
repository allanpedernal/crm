<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryRemitterRelationship extends Model {
	
    public $timestamps = false;
    
}
