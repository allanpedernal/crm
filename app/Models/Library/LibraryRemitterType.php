<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryRemitterType extends Model {

    public $timestamps = false;

}
