<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryState extends Model {
	
    public $timestamps = false;
    
}
