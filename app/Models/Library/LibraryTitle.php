<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryTitle extends Model {

    public $timestamps = false;

}
