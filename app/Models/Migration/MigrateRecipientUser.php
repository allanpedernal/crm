<?php

namespace App\Models\Migration;

use Illuminate\Database\Eloquent\Model;

class MigrateRecipientUser extends Model {
	
	public $timestamps = false;
    
}
