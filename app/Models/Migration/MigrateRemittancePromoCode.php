<?php

namespace App\Models\Migration;

use Illuminate\Database\Eloquent\Model;

class MigrateRemittancePromoCode extends Model {
	
	public $timestamps = false;
    
}
