<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class MobilePromotion extends Model {
	
	use SoftDeletes;

	use \Venturecraft\Revisionable\RevisionableTrait;
	

	protected $revisionEnabled = true;

	protected $dates = [ 'deleted_at' ];

	public $timestamps = true;
	
    
    //BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() {
		
        parent::boot();
        
    }
    
    public function getIsSendStringAttribute($value) {
		
    	if ( $value == 0 ) {
			
    		return '<span class="label label-danger">No</span>';
    		
    	}
    	
    	else {
			
    		return '<span class="label label-success">Yes</span>';
    		
    	}
    	
    }
    
}
