<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Recipient extends Model {
	
	
	use SoftDeletes;

	use \Venturecraft\Revisionable\RevisionableTrait;
	

	protected $revisionEnabled = true;

	protected $dates = [ 'deleted_at' ];

	public $timestamps = true;
	
    
    //BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() {
		
        parent::boot();
        
    }
	
    
	//REMITTER
	public function remitter() {
		
		return $this -> hasOne( 'App\Models\Remitter', 'id', 'remitter_id' );
		
	}

	//RELATIONSHIP
	public function relationship() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryRemitterRelationship', 'id', 'remitter_relationship_id' );
		
	}

	//RECIPIENTPERSON
	public function recipientPerson() {
		
		return $this -> hasOne('App\Models\RecipientPerson', 'recipient_id', 'id' );
		
	}	

	//RECIPIENTCOMPANY
	public function recipientCompany() {
		
		return $this -> hasOne('App\Models\RecipientCompany', 'recipient_id', 'id' );
		
	}	

	//RECIPIENTCONTACT
	public function recipientContact() {
		
		return $this -> hasOne( 'App\Models\RecipientContact', 'recipient_id', 'id' );
		
	}	

	/**
	 * crm_recipient_addresses
	 * @return RecipientAddress Eloquent Model
	 */
	public function address()
	{

		return $this -> hasOne( 'App\Models\RecipientAddress', 'recipient_id', 'id' );

	}

}
