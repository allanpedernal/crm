<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipientAddress extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//RECIPIENT
	public function recipient() {
		
		return $this -> belongsTo( 'App\Models\Recipient', 'id', 'recipient_id' );
		
	}

    //COUNTRY
    public function country() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryCountry', 'id', 'country_id' );
		
	}
	
    //STATE
    public function state() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryState', 'id', 'state_id' );
		
	}
	
    //CITY
    public function city() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryCity', 'id', 'city_id' );
		
	}
	
    
}
