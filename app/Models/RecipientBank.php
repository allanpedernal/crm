<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipientBank extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//RECIPIENT
	public function recipient() {
		
		return $this -> belongsTo( 'App\Models\Recipient', 'id', 'recipient_id' );
		
	}

	//LIST
	public function list() {
		
		return $this -> hasOne( '\App\Models\Library\LibraryReceiveOptionList', 'id', 'receive_option_list_id' );
		
	}
    
}
