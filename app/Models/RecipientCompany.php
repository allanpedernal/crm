<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipientCompany extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//RECIPIENT
	public function recipient() {
		
		return $this -> belongsTo( 'App\Models\Recipient', 'id', 'recipient_id' );
		
	}

	//COUNTRY
	public function country() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryCountry', 'id', 'country_id' );
		
	}
	
	//RELATIONSHIP
	public function relationship() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryRemitterRelationship', 'id', 'company_relationship_id' );
		
	}
	
	//SOURCE OF FUND
	public function source_of_fund() {
		
		return $this -> hasOne( 'App\Models\Library\LibrarySourceOfFund', 'id', 'source_of_fund_id' );
		
	}
	
	//FREQUENCY
	public function frequency() {
		
		return $this -> hasOne( 'App\Models\Library\LibrarySourceOfFund', 'id', 'frequency_id' );
		
	}
	
	//BANK TYPE
	public function bank_type() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryBankType', 'id', 'bank_type_id' );
		
	}
	
}
