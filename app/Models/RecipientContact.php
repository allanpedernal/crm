<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipientContact extends Model{
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//RECIPIENT
	public function recipient() {
		
		return $this -> belongsTo( 'App\Models\Recipient', 'id', 'recipient_id' );
		
	}

    //TYPE
    public function type() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryContactType', 'id', 'contact_type_id' );
		
	}
    
}
