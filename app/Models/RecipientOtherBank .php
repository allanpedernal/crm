<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipientOtherBank extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
	//GET BANK
	public function bank() {
		
		return $this -> belongsTo( 'App\Models\RecipientBank ', 'id', 'recipient_bank_id' );
		
	}
	
}
