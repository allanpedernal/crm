<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipientOtherRelationship extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//RECIPIENT
	public function recipient() {
		
		return $this -> belongsTo( 'App\Models\Recipient', 'id', 'recipient_id' );
		
	}
	
}
