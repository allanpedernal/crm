<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipientPerson extends Model {
	
	use \Venturecraft\Revisionable\RevisionableTrait;
	
	protected $table = 'recipient_persons';

	protected $revisionEnabled = true;
	
	public $timestamps = false;
	
	
    //BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() {
		
        parent::boot();
        
    }
	
	
    //MUTATORS
    public function getFullNameAttribute( $value ) {
		
		return sprintf(

			'%s %s %s', 

			$this -> attributes[ 'firstname' ],

			$this -> attributes[ 'middlename' ],

			$this -> attributes[ 'lastname' ]
			
		);

    }
	
    //GENERATE ID NUMBER
    public function generate_id_number() {

        $generated_id = '';

        $is_id_available = false;

        while( ! $is_id_available ) {

            $random_id = str_random( 8 );   

            $generated_id = $random_id;

			$is_id_available = $this -> where( 'id_number', '=', $generated_id )

									-> get()

									-> isEmpty(); 

        }

        return $generated_id;

    }
    
    
	//RECIPIENT
	public function recipient() {
		
		return $this -> belongsTo( 'App\Models\Recipient', 'id', 'recipient_id' );
		
	}
	
    //TITLE
    public function title() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryTitle', 'id', 'title_id' );
		
	}
    
    //CIVIL STATUS
    public function civil_status() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryCivilStatus', 'id', 'civil_status_id' );
		
	}
    
    //NATIONALITY
    public function nationality() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryNationality', 'id', 'nationality_id' );
		
	}
	
	
}
