<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Remittance extends Model {
	
	
	use SoftDeletes;

	use \Venturecraft\Revisionable\RevisionableTrait;
	

	protected $revisionEnabled = true;

	protected $dates = [ 'deleted_at' ];

	public $timestamps = true;
	
	
	//BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() {
		
        parent::boot();
        
    }
    
    //GET REMITTER
    public function remitter() {
		
    	return $this -> hasOne( '\App\Models\Remitter', 'id', 'remitter_id' );
    	
    }
    
    //GET RECIPIENT
    public function recipient() {
		
    	return $this -> hasOne( '\App\Models\Recipient', 'id', 'recipient_id' );
    	
    }
    
    //GET BANK SOURCE
    public function bank_source() {
		
    	return $this -> hasOne( '\App\Models\Library\LibraryBankSource', 'id', 'bank_source_id' );
    	
    }
    
    //GET STATUS
    public function status() {
		
    	return $this -> hasOne( '\App\Models\Library\LibraryRemittanceStatus', 'id', 'status_id' );
    	
    }
    
    //GET REASON
    public function reason() {
		
    	return $this -> hasOne( '\App\Models\Library\LibraryReason', 'id', 'reason_id' );
    	
    }
    
    //GET PROMO CODE
    public function promo_code() {
    	
    	return $this -> hasOne( '\App\Models\RemittancePromoCode', 'id', 'promo_code_id' );
    	
    }

    public function RecipientReceiveOption() {

        return $this -> hasOne( '\App\Models\RecipientReceiveOption' , 'recipient_id', 'recipient_id' );
    }
    
    //GET ACCESS TOKEN
    public function access_token() {
        
        return $this -> hasOne( 'App\Models\RemitterAccessToken', 'remitter_id', 'remitter_id' );
        
    }

    //SOURCE OF FUNDS
    public function source_of_fund()
    {

        return $this -> hasOne( 'App\Models\Library\LibrarySourceOfFund', 'id', 'source_of_fund_id' );

    }

    public function get_remittance_id( $id_type, $id )
    {
        $remittance_id = null;

        if( ! empty( $id_type ) && $id ) //use empty( ) because we expect $id_type as string? 
        {

            switch( $id_type )
            {

                case 'remitter-id':

                    $last_transaction = $this -> get_remitter_last_transaction( $id );

                    $remittance_id = $last_transaction[ 'id' ];

                    break;

                case 'remittance-id':

                    $remittance_id = $id;

                    break;   

                default: 

            }

        }

        return $remittance_id;

    }

    public function get_remitter_last_transaction( $remitter_id )
    {
            
        if( !empty( $remitter_id ) || !is_null( $remitter_id ) ) 
        {
                
            $last_transaction = $this -> where( 'remitter_id', '=', $remitter_id )

                                      -> orderBy( 'id', 'DESC' )

                                      -> first( );
                
            return $last_transaction;
                
        }
            
        return array();
            
    }  

    public function get_transferred_date( $remittance_id )
    {

        return $this->find( $remittance_id )[ 'date_transferred' ];

    } 

    public function get_bank_source_id( $remittance_id )
    {

        return $this->find( $remittance_id )[ 'bank_source_id' ];

    } 

    public function get_remitter_id( $remittance_id )
    {

        return $this->find( $remittance_id )[ 'remitter_id' ];

    }

    public function is_transaction_in_the_same_day( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return ( date( 'Y-m-d', strtotime( $date_transaction ) ) 
        
               == 

               date( 'Y-m-d' ) )

        ;   

    }

    public function is_holiday( $remittance_id )
    {

        $spider_calendar_id = 2; //determines what holiday calendar would be used. 2 is AU

        $is_holiday = false;

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        $is_same_date_in_sydney = $this->is_transaction_in_the_same_day( $date_transaction )

                                  && 

                                  date( 'Y-m-d', strtotime( $date_transaction ) ) 

                                  == 

                                  date( 'Y-m-d' )

        ; 

        if( $is_same_date_in_sydney ) 
        {

            $spider_calendar_event = new SpiderCalendarEvent;

            $holidays = $spider_calendar_event -> get_Holidays( $spider_calendar_id );

            if( count( $holidays ) > 0 ) 
            {

                foreach ( $holidays as $holiday ) 
                {
                       
                    $holiday_date = date( 'Y-m-d', strtotime( $holiday[ 'date' ] ) ); 

                    if( $holiday_date == $transaction_date ) 
                    {

                        $is_holiday = true;

                        break;

                    }

                }
                    
            }

        }  

        return $is_holiday;

    }

    public function is_sunday( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return date( 'D', strtotime( $date_transaction ) ) == 'Sun';

    } 

    public function is_saturday( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return date( 'D', strtotime( $date_transaction ) ) == 'Sat'; 
        
    }

    public function get_exchange_rate( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        $is_saturday = $this->is_saturday( $remittance_id ); 

        $is_sunday = $this->is_sunday( $remittance_id );
        
        $is_holiday = $this->is_saturday( $remittance_id );

        $is_before_9am = $this->is_before_9am( $remittance_id );

        $is_past_12noon = $this->is_past_12noon( $remittance_id );

        $is_past_6pm = $this->is_past_6pm( $remittance_id );

        $is_mon_to_sat_before_9am = !$is_sunday && $is_before_9am;

        $is_mon_to_sat_past_6pm = !$is_sunday && $is_past_6pm;

        $is_saturday_past_12noon = $is_saturday && $is_past_12noon;

        $is_not_office_hours = $is_sunday

                               ||

                               $is_holiday

                               ||

                               $is_mon_to_sat_before_9am

                               ||

                               $is_mon_to_sat_past_6pm

                               ||

                               $is_saturday_past_12noon

        ;

        $exchange_rate = null;

        if( $is_not_office_hours )
        {

            $exchange_rate = 'TBA Next Business Day';

        }
        else
        {

            $exchange_rate = $this->find( $remittance_id )[ 'forex_rate' ];

        }

        return $exchange_rate;

    }

    public function get_peso_amount( $remittance_id )
    {

        $exchange_rate = $this->get_exchange_rate( $remittance_id );

        $peso_amount = null;

        if( $exchange_rate == 'TBA Next Business Day' )
        {

            $peso_amount = 'TBA Next Business Day';

        }

        else
        {

            $amount_sent = $this->find( $remittance_id )[ 'amount_sent' ];

            $peso_amount = number_format( ( ( ( float ) $amount_sent ) * $remittance_details[ 'forex_rate' ] ), 2, '.', ',' );

        }

        return $peso_amount;

    }

    public function is_before_9am( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( date( $date_transaction ) ) <= strtotime( date( 'Y-m-d 09:00:01' ) );

    }

    public function is_past_12noon( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( date( $date_transaction ) ) >= strtotime( date( 'Y-m-d 12:00:01' ) );

    }

    public function is_past_6pm( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( date( $date_transaction ) ) >= strtotime( date( 'Y-m-d 18:00:01' ) );

    }

    public function is_saturday_12am_to_11am_bdo_metrobank_cashpick( $remittance_id )
    { 

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction ) 

               <= 

               strtotime( date( 'Y-m-d 11:00:00' ) ) 

        ;

    }

    public function is_saturday_12am_to_11am_door_to_door( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction ) 

               <= 

               strtotime( date( 'Y-m-d 11:00:00' ) )

        ;

    }

    public function is_saturday_11am_onwards( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction ) 

               >= 

               strtotime( date( 'Y-m-d 12:00:01' ) )

        ;       

    }

    public function is_12am_to_9am( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction ) 

               <= 

               strtotime( date( 'Y-m-d 9:00:00' ) )

        ;

    }

    public function is_9am_to_12_noon( $remittance_id )
    { 

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction )

               >= 

               strtotime( date( 'Y-m-d 9:00:01' ) )

               &&

               strtotime( $date_transaction )

               <= 

               strtotime( date( 'Y-m-d 12:00:00' ) )

        ;

    }

    public function is_12noon_to_6pm( $remittance_id )
    { 

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction )

               >= 

               strtotime( date( 'Y-m-d 12:00:01' ) ) 

               && 

               strtotime( $date_transaction ) 

               <= 
 
               strtotime( date( 'Y-m-d 18:00:00' ) )

       ;

    }

    public function is_monday_to_saturday_12am_to_11am_non_bdo_metrobank( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction )

               <= 

               strtotime( date( 'Y-m-d 11:00:00' ) )

        ;

    }

    public function is_monday_to_friday_11am_onwards_to_6pm_non_bdo_metrobank( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction )

               >= 

               strtotime( date( 'Y-m-d 11:00:01' ) )  

               && 

               strtotime( $date_transaction )

               <= 

               strtotime( date( 'Y-m-d 18:00:00' ) )

        ;

    }

    public function is_monday_to_friday_12am_to_6pm_bdo_metrobank_cashpick( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction )

               <= 

               strtotime( date( 'Y-m-d 18:00:00' ) )

        ;

    }

    public function is_monday_to_friday_12am_to_6pm_door_to_door( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction )

               <= 

               strtotime( date( 'Y-m-d 18:00:00' ) )

        ;

    }

    public function is_monday_to_friday_after_6pm( $remittance_id )
    {

        $date_transaction = $this -> get_transferred_date( $remittance_id );

        return strtotime( $date_transaction )

               >= 

               strtotime( date( 'Y-m-d 18:00:01' ) )

        ;       

    }

    //is_non_poli
    public function is_remittance_instruction_received( $remittance_id )
    {

        return $this -> find( $remittance_id ) -> status[ 'name' ] == "Remittance Instruction Received"; 

    }


    public function is_poli( $remittance_id )
    {

        return $this -> find( $remittance_id ) -> status[ 'name' ] == "POLi Successful";

    }

    public function is_autoresponder_accepted( $remittance_id )
    {

        return $this -> is_remittance_instruction_received( $remittance_id )

               ||

               $this -> is_poli( $remittance_id )

        ;

    }
    
}
