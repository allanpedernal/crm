<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemittancePolipayment extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
}
