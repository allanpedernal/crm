<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemittancePromoCode extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }

    public function remittances()
    {

    	return $this -> hasMany( '\App\Models\Remittance', 'promo_code_id', 'id' );	

    }
    
}
