<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemittancePromoCodeUser extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//PROMO CODE
	public function promo_code() {
		
		return $this -> belongsTo( 'App\Models\RemittancePromoCode', 'id', 'remittance_promo_code_id' );
		
	}
	
}
