<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemittancePromoCodeUserRecipient extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//PROMO CODE
	public function promo_code() {
		
		return $this -> belongsTo( 'App\Models\RemittancePromoCode', 'id', 'remittance_promo_code_id' );
		
	}
	
	//REMITTER
	public function remitter() {
		
		return $this -> hasOne( 'App\Models\Remitter', 'id', 'remitter_id' );
		
	}
	
	//RECIPIENT
	public function recipient() {
		
		return $this -> hasOne( 'App\Models\Recipient', 'id', 'recipient_id' );
		
	}
	
}
