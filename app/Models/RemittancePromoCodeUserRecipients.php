<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemittancePromoCodeUserRecipients extends Model
{
    //

    public function remitter()
    {

    	return $this -> hasOne( 'App\Models\Remitter', 'id', 'remitter_id' );

    }

    public function recipient()
    {

    	return $this -> hasOne( 'App\Models\Recipient', 'id', 'recipient_id' );

    }

}
