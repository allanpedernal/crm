<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemittanceReceipt extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//REMITTANCE
	public function remittance() {
		
		return $this -> belongsTo( 'App\Models\Remittance', 'id', 'remittance_id' );
		
	}
	
}
