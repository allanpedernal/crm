<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemittanceStatus extends Model
{
    // Constants
    const REMITTANCE_STATUS_PROCESSING_B = 10;
}
