<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Remitter extends Model {
	
	
	use SoftDeletes;

	use \Venturecraft\Revisionable\RevisionableTrait;
	

	protected $revisionEnabled = true;

	protected $dates = [ 'deleted_at' ];

	public $timestamps = true;
	
    
    //BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() 
    {
		
        parent::boot();
        
    }
	
	
    //MUTATORS
    public function getFullNameAttribute( $value ) 
    {
		
		return sprintf(

			'%s %s %s', 

			$this -> attributes[ 'firstname' ],

			$this -> attributes[ 'middlename' ],

			$this -> attributes[ 'lastname' ]
			
		);

    }
	
    //GENERATE ID NUMBER
    public function generate_id_number() 
    {

        $generated_id = '';

        $is_id_available = false;

        while( ! $is_id_available ) {

            $random_id = str_random( 8 );   

            $generated_id = $random_id;

			$is_id_available = $this -> where( 'id_number', '=', $generated_id )

									-> get()

									-> isEmpty(); 

        }

        return $generated_id;

    }
	
    
    //TYPE
    public function type() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryRemitterType', 'id', 'type_id' );
		
	}
    
    //TITLE
    public function title() 
    {
		
		return $this -> hasOne( 'App\Models\Library\LibraryTitle', 'id', 'title_id' );
		
	}
    
    //CIVIL STATUS
    public function civil_status() 
    {
		
		return $this -> hasOne( 'App\Models\Library\LibraryCivilStatus', 'id', 'civil_status_id' );
		
	}
    
    //NATIONALITY
    public function nationality() 
	{
		
		return $this -> hasOne( 'App\Models\Library\LibraryNationality', 'id', 'nationality_id' );
		
	}
	
    //MARKETING SOURCE
    public function marketing_source() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryRemitterMarketingSource', 'id', 'marketing_source_id' );
		
	}

	//GET RECIPIENTS BASED FROM remitter_id
	public function recipients()
    {

    	return $this -> hasMany( '\App\Models\Recipient', 'remitter_id', 'id' );

    }

	/**
	 * crm_remitter_addresses
	 * @return RemitterAddress Eloquent Model
	 */
	public function Address()
	{

		return $this -> hasOne( 'App\Models\RemitterAddress', 'remitter_id', 'id' );

	}

	/**
	 * crm_remitter_contacts
	 * @return RemitterContact Eloquent Model
	 */
	public function contact()
	{

		return $this -> hasOne( 'App\Models\RemitterContact', 'remitter_id', 'id' );

	}

	/**
	 * crm_library_identification_types
	 * @return LibraryIdentificationType Eloquent Model
	 */
    public function idType() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryIdentificationType', 'id', 'type_id' );
		
	}
    
}
