<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemitterAddress extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    //BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//REMITTER
	public function remitter() {
		
		return $this -> belongsTo( 'App\Models\Remitter', 'id', 'remitter_id' );
		
	}
    
    //COUNTRY
    public function country() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryCountry', 'id', 'country_id' );
		
	}
	
    //STATE
    public function state() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryState', 'id', 'state_id' );
		
	}
	
    //CITY
    public function city() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryCity', 'id', 'city_id' );
		
	}
	
    //TENANCY LENGTH
    public function tenancy_length() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryRemitterAddressTenancyLength', 'id', 'tenancy_length_id' );
		
	}
    
	
}
