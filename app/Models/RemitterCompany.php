<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemitterCompany extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    //BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//REMITTER
	public function remitter() {
		
		return $this -> belongsTo( 'App\Models\Remitter', 'id', 'remitter_id' );
		
	}
    
    //COUNTRY
    public function country() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryCountry', 'id', 'country_id' );
		
	}
	
    //RELATIONSHIP
    public function relationship() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryRemitterRelationship', 'id', 'company_relationship_id' );
		
	}
    
    //SOURCE OF FUND
    public function source_of_fund() {
		
		return $this -> hasOne( 'App\Models\Library\LibrarySourceOfFund', 'id', 'source_of_fund_id' );
		
	}
	
    //FREQUENCY
    public function frequency() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryRemittanceFrequency', 'id', 'frequency_id' );
		
	}
	
    //BANK TYPE
    public function bank_type() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryBankType', 'id', 'bank_type_id' );
		
	}
	
	
}
