<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemitterCompanyOtherSourceOfFund extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    //BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//REMITTER COMPANY
	public function remitter_company() {
		
		return $this -> belongsTo( 'App\Models\RemitterCompany', 'id', 'remitter_company_id' );
		
	}
    
}
