<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemitterIdentification extends Model {
	
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionEnabled = true;
    
	public $timestamps = false;
	
    //BOOT VENTURE CRAFT REVISIONABLE
    public static function boot() {
		
        parent::boot();
        
    }
    
    
	//REMITTER
	public function remitter() {
		
		return $this -> belongsTo( 'App\Models\Remitter', 'id', 'remitter_id' );
		
	}
    
    //TYPE
    public function type() {
		
		return $this -> hasOne( 'App\Models\Library\LibraryIdentificationType', 'id', 'identification_type_id' );
		
	}
	
}
