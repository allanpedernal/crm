<?php

namespace App\Models\Library;

use Illuminate\Database\Eloquent\Model;

class LibraryHolidays extends Model
{
    //
    public function get_Holidays( $country_id )
    {

    	return $this->where( 'calendar', '=', $country_id )

    				-> get( );

    }
}
