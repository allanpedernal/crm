<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAudToSellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('aud_to_sells', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//AMOUNT
			$table
			->double('amount', 11, 2)
			->unsigned()
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
		});

		//SCHEMA FOR MIGRATION
		Schema::create('migrate_aud_to_sells', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//AUD TO SELL ID
			$table
			->integer('aud_to_sell_id')
			->unsigned()
			->nullable();

			//DATETIME
			$table
			->dateTime('datetime')
			->nullable();

		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
