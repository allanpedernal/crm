<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryBankSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('library_bank_sources', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//SHORTNAME
			$table
			->string('shortname')
			->nullable();
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//ACCOUNT NO
			$table
			->string('account_no')
			->nullable();
			
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');
			
		});
		
		//INSERT BANK SOURCE
		DB::table('library_bank_sources')->insert(['shortname'=>'NAB', 'name' => 'National Australia Bank', 'account_no'=>'298040258','ordering'=>1]);
		DB::table('library_bank_sources')->insert(['shortname'=>'ANZ', 'name' => 'Australia and New Zealand Banking Group', 'account_no'=>'284219302','ordering'=>2]);
		DB::table('library_bank_sources')->insert(['shortname'=>'COMMBANK', 'name' => 'Commonwealth Bank', 'account_no'=>'14977580','ordering'=>3]);
		DB::table('library_bank_sources')->insert(['shortname'=>'WESTPAC', 'name' => 'Westpac Banking Corporation', 'account_no'=>'323618','ordering'=>4]);
		DB::table('library_bank_sources')->insert(['shortname'=>'POLI', 'name' => 'polipayments', 'account_no'=>'996112451908','ordering'=>5]);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
