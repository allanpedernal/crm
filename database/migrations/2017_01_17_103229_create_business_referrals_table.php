<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('business_referrals', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//CAPACITY
			$table
			->string('capcity')
			->nullable();
			
			//CAMPAIGN VALIDITY
			$table
			->integer('campaign_validity')
			->unsigned()
			->nullable();
			
			//ADDRESS
			$table
			->string('address')
			->nullable();
			
			//PHONE
			$table
			->string('phone')
			->unique()
			->nullable();
			
			//MOBILE
			$table
			->string('mobile')
			->unique()
			->nullable();
			
			//EMAIL
			$table
			->string('email')
			->unique()
			->nullable();
			
			//FB
			$table
			->string('fb')
			->nullable();
			
			//WEBSITE
			$table
			->string('website')
			->nullable();
			
			//BANK ACCOUNT NAME
			$table
			->string('bank_account_name')
			->nullable();
			
			//BANK ACCOUNT NUMBER
			$table
			->string('bank_account_no')
			->unique()
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
		});
		
		 //SCHEMA FOR MIGRATION
		 Schema::create('migrate_business_referrals', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//BUSINESS REFERRAL ID
			$table
			->integer('business_referral_id')
			->unsigned()
			->nullable();

			//DATETIME
			$table
			->dateTime('datetime')
			->nullable();

		 });
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
