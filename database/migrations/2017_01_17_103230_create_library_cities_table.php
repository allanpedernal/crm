<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Maatwebsite\Excel\Facades\Excel;

class CreateLibraryCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
		Schema::create('library_cities', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//NAME
			$table
			->string('name')
			->nullable();
			
			//POSTAL CODE
			$table
			->string('postal_code')
			->nullable();

			//STATE ID
			$table
			->integer('state_id')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('state_id');

		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
