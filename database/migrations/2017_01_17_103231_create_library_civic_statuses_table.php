<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryCivicStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('library_civil_statuses', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//DESCRIPTION
			$table
			->string('description')
			->nullable();
			
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');
			
		});
		
		 //INSERT CIVIL STATUSES
		 DB::table('library_civil_statuses')->insert(['name' => 'Single','ordering'=>1]);
		 DB::table('library_civil_statuses')->insert(['name' => 'Married','ordering'=>2]);
		 DB::table('library_civil_statuses')->insert(['name' => 'Divorced','ordering'=>3]);
		 DB::table('library_civil_statuses')->insert(['name' => 'De Facto','ordering'=>4]);
		 DB::table('library_civil_statuses')->insert(['name' => 'Separated','ordering'=>5]);
		 DB::table('library_civil_statuses')->insert(['name' => 'Widowed','ordering'=>6]);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		
    }
}
