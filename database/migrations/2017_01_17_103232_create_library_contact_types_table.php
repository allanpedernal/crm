<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryContactTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('library_contact_types', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//NAME
			$table
			->string('name')
			->nullable();

			//DESCRIPTION
			$table
			->string('description')
			->nullable();

			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		 });
		 
		//INSERT CONTACT TYPE
		DB::table('library_contact_types')->insert(['name' => 'Mobile','ordering'=>1]);
		DB::table('library_contact_types')->insert(['name' => 'Phone','ordering'=>2]);
		DB::table('library_contact_types')->insert(['name' => 'Fax','ordering'=>3]);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
