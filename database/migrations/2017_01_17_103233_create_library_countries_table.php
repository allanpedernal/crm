<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         Schema::create('library_countries', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
			
			//NUM CODE
			$table
			->string('num_code')
			->nullable();
			
			//ALPHA 2 CODE
			$table
			->string('alpha_2_code')
			->nullable();
			
			//ALPHA 3 CODE
			$table
			->string('alpha_3_code')
			->nullable();
			
			//EN SHORT NAME
			$table
			->string('en_short_name')
			->nullable();
			
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		 });
		 
		//GET ALL RECORD FROM AU.CSV
		Excel::load(storage_path('app/csv/countries.csv'), function($rows) {
			$rows->each(function($row) {
				$country = $row->toArray();
				$id = $country['id'];
				$num_code = $country['num_code'];
				$alpha_2_code = $country['alpha_2_code'];
				$alpha_3_code = $country['alpha_3_code'];
				$en_short_name = $country['en_short_name'];
				DB::table('library_countries')->insert(['num_code'=>$num_code,'alpha_2_code'=>$alpha_2_code,'alpha_3_code'=>$alpha_3_code,'en_short_name'=>$en_short_name,'ordering'=>$id]);
			});
		});
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
