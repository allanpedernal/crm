<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryIdentificationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('library_identification_types', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//DESCRIPTION
			$table
			->string('description')
			->nullable();
			
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		 });

		 //INSERT IDENTIFICATION TYPE
		 DB::table('library_identification_types')->insert(['name' => 'Other (provide description)','ordering'=>17]);
		 DB::table('library_identification_types')->insert(['name' => 'Alien registration number','ordering'=>1]);
		 DB::table('library_identification_types')->insert(['name' => 'Benefits Card / ID','ordering'=>2]);
		 DB::table('library_identification_types')->insert(['name' => 'Birth certificate','ordering'=>3]);
		 DB::table('library_identification_types')->insert(['name' => 'Business registration / licence','ordering'=>4]);
		 DB::table('library_identification_types')->insert(['name' => 'Customer account/ID','ordering'=>5]);
		 DB::table('library_identification_types')->insert(['name' => "Driver's Licence",'ordering'=>6]);
		 DB::table('library_identification_types')->insert(['name' => 'Employee ID','ordering'=>7]);
		 DB::table('library_identification_types')->insert(['name' => 'Employer number','ordering'=>8]);
		 DB::table('library_identification_types')->insert(['name' => 'Identity card/number','ordering'=>9]);
		 DB::table('library_identification_types')->insert(['name' => 'Membership ID','ordering'=>10]);
		 DB::table('library_identification_types')->insert(['name' => 'Passport','ordering'=>11]);
		 DB::table('library_identification_types')->insert(['name' => 'Photo ID','ordering'=>12]);
		 DB::table('library_identification_types')->insert(['name' => 'PRC ID','ordering'=>13]);
		 DB::table('library_identification_types')->insert(['name' => 'Security ID','ordering'=>14]);
		 DB::table('library_identification_types')->insert(['name' => 'Social Security ID','ordering'=>15]);
		 DB::table('library_identification_types')->insert(['name' => 'Student ID','ordering'=>16]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
