<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryNationalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('library_nationalities', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//COUNTRY ID
			$table
			->integer('country_id')
			->unsigned()
			->nullable();
			
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('country_id');
			$table->index('ordering');
			
		});
		
		//GET ALL RECORD FROM AU.CSV
		Excel::load(storage_path('app/csv/countries.csv'), function($rows) {
			$rows->each(function($row) {
				$country = $row->toArray();
				$id = $country['id'];
				$nationality = $country['nationality'];
				DB::table('library_nationalities')->insert(['name'=>$nationality,'country_id'=>$id,'ordering'=>$id]);
			});
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
