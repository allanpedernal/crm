<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryNatureOfWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('library_nature_of_works', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//NAME
			$table
			->string('name')
			->nullable();

			//DESCRIPTION
			$table
			->string('description')
			->nullable();

			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		 });

		 DB::table('library_nature_of_works')->insert(['name' => 'Others','ordering'=>22]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Architecture and Engineering','ordering'=>1]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Armed Forces and Security','ordering'=>2]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Arts, Media Practitioner and Communications','ordering'=>3]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Building and Grounds Cleaning and Maintenance','ordering'=>4]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Business, Banking, and Financial Operations','ordering'=>5]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Community, Education, and Social Services','ordering'=>6]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Farming, Fishing, and Forestry','ordering'=>7]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Healthcare Practitioner and Technical Occupation','ordering'=>8]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Housewife','ordering'=>9]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Installation, Maintenance, and Repair','ordering'=>10]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Legal','ordering'=>11]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Management','ordering'=>12]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Office and Administrative Support','ordering'=>13]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Personal Care and Service','ordering'=>14]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Production','ordering'=>15]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Retired Employee','ordering'=>16]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Sales or Other Sales Related','ordering'=>17]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Science Research and Technology','ordering'=>18]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Student','ordering'=>19]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Transportation and Material Moving','ordering'=>20]);
		 DB::table('library_nature_of_works')->insert(['name' => 'Unemployed','ordering'=>21]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
