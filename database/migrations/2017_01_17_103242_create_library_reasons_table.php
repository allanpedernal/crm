<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatelibraryReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('library_reasons', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//NAME
			$table
			->string('name')
			->nullable();

			//DESCRIPTION
			$table
			->string('description')
			->nullable();

			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		 });
		 
		 //INSERT library_reasonsS
		 DB::table('library_reasons')->insert(['name' => 'Others','ordering'=>36]);
		 DB::table('library_reasons')->insert(['name' => "Children's Tuition Fee",'ordering'=>1]);
		 DB::table('library_reasons')->insert(['name' => 'Condo Payment','ordering'=>2]);
		 DB::table('library_reasons')->insert(['name' => 'Credit Card Payment','ordering'=>3]);
		 DB::table('library_reasons')->insert(['name' => 'Donation','ordering'=>4]);
		 DB::table('library_reasons')->insert(['name' => 'Family Cost of the Living Allowance','ordering'=>5]);
		 DB::table('library_reasons')->insert(['name' => 'Financial Support','ordering'=>6]);
		 DB::table('library_reasons')->insert(['name' => 'Gift / Donation','ordering'=>7]);
		 DB::table('library_reasons')->insert(['name' => 'Hospital Payment','ordering'=>8]);
		 DB::table('library_reasons')->insert(['name' => 'House Rental','ordering'=>9]);
		 DB::table('library_reasons')->insert(['name' => 'House Repairs','ordering'=>10]);
		 DB::table('library_reasons')->insert(['name' => 'Loan Payment','ordering'=>11]);
		 DB::table('library_reasons')->insert(['name' => 'Medicine Payment','ordering'=>12]);
		 DB::table('library_reasons')->insert(['name' => 'Money for Medicine','ordering'=>13]);
		 DB::table('library_reasons')->insert(['name' => 'Payment for Filipino Staff','ordering'=>14]);
		 DB::table('library_reasons')->insert(['name' => 'Payment for product (Bill Payment)','ordering'=>15]);
		 DB::table('library_reasons')->insert(['name' => 'Payment for service (Bill Payment)','ordering'=>16]);
		 DB::table('library_reasons')->insert(['name' => 'Savings in the Philippines','ordering'=>17]);
		 DB::table('library_reasons')->insert(['name' => 'School Allowance','ordering'=>18]);
		 DB::table('library_reasons')->insert(['name' => 'Visa Application Payment','ordering'=>19]);
		 DB::table('library_reasons')->insert(['name' => 'Savings','ordering'=>20]);
		 DB::table('library_reasons')->insert(['name' => 'Allotment','ordering'=>21]);
		 DB::table('library_reasons')->insert(['name' => 'Allowance','ordering'=>22]);
		 DB::table('library_reasons')->insert(['name' => 'Amortization','ordering'=>23]);
		 DB::table('library_reasons')->insert(['name' => 'Bills Payment','ordering'=>24]);
		 DB::table('library_reasons')->insert(['name' => 'Business - Operating Expenses','ordering'=>25]);
		 DB::table('library_reasons')->insert(['name' => 'Business - Set up','ordering'=>26]);
		 DB::table('library_reasons')->insert(['name' => 'Education','ordering'=>27]);
		 DB::table('library_reasons')->insert(['name' => 'Home Improvement','ordering'=>28]);
		 DB::table('library_reasons')->insert(['name' => 'Insurance','ordering'=>29]);
		 DB::table('library_reasons')->insert(['name' => 'Investment','ordering'=>30]);
		 DB::table('library_reasons')->insert(['name' => 'Mortgage Payment','ordering'=>31]);
		 DB::table('library_reasons')->insert(['name' => 'Payment of Goods','ordering'=>32]);
		 DB::table('library_reasons')->insert(['name' => 'Real Estate Purchase','ordering'=>33]);
		 DB::table('library_reasons')->insert(['name' => 'Taxes','ordering'=>34]);
		 DB::table('library_reasons')->insert(['name' => 'Vehicle','ordering'=>35]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
