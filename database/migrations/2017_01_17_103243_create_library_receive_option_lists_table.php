<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryReceiveOptionListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

		Schema::create('library_receive_option_lists', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//RECEIVE OPTION ID
			$table
			->integer('receive_option_id')
			->unsigned()
			->nullable();

			//NAME
			$table
			->string('name')
			->nullable();

			//DESCRIPTION
			$table
			->string('description')
			->nullable();

			//ORDERING
			$table
			->integer('ordering')
			->unsigned();
			
			//CREATE INDEX
			$table->index('ordering');

		});

		//INSERT RECIEVE OPTION LIST - CASH PICKUP = 1
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'MLHUILLIER','ordering'=>1]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'CEBUANA','ordering'=>2]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'BDO REMIT','ordering'=>3]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'SM DEPARTMENT STORE','ordering'=>4]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'SM HYPERMARKET','ordering'=>5]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'METROBANK REMITTANCE','ordering'=>6]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'LBC','ordering'=>7]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'1ST VALLEY BANK','ordering'=>8]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'ASIAN HILLS BANK','ordering'=>9]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'PALAWAN PAWNSHOP','ordering'=>10]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'BANCO LAGAWE','ordering'=>11]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'BANCO NG MASA','ordering'=>12]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'BANGKO KABAYAN','ordering'=>13]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'BANGKO MABUHAY','ordering'=>14]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'BANK OF FLORIDA','ordering'=>15]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'BAYAD CENTER','ordering'=>16]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'CAMALIG BANK','ordering'=>17]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'CANTILAN BANK','ordering'=>18]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'CARD BANK','ordering'=>19]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'ENTERPRISE BANK','ordering'=>20]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'FICOBANK','ordering'=>21]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'GATEWAY RURAL BANK','ordering'=>22]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'GM BANK','ordering'=>23]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'GUAGUA RURAL BANK','ordering'=>24]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'ONE NETWORK BANK','ordering'=>25]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'MT. CARMEL RURAL BANK','ordering'=>26]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'PENBANK','ordering'=>27]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'PRODUCERS BANK','ordering'=>28]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'QUEZON CAPITAL RURAL BANK','ordering'=>29]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'RANG-AY BANK','ordering'=>30]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'RURAL BANK OF BAYOMBONG','ordering'=>31]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'RURAL BANK OF CAUAYAN','ordering'=>32]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'RURAL BANK OF CENTRAL PANGASINAN','ordering'=>33]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'TIAONG RURAL BANK','ordering'=>34]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'WALTERMART','ordering'=>35]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'ZAMBALES RURAL BANK','ordering'=>36]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'RD PAWNSHOP','ordering'=>37]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'GLOBE GCASH OUTLETS','ordering'=>38]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'ROBINSON DEPARTMENT STORE','ordering'=>39]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 1,'name'=>'PS BANK REMITTANCE','ordering'=>40]);

		//INSERT RECIEVE OPTION LIST - PHILIPPINE BANK TRANSFER = 2
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'OTHER BANK','ordering'=>41]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'METROBANK','ordering'=>42]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ALLIED BANK','ordering'=>43]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BDO','ordering'=>44]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BPI FAMILY SAVINGS','ordering'=>45]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BPI','ordering'=>46]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'CBC BANK','ordering'=>47]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'LAND BANK','ordering'=>48]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PNB','ordering'=>49]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PS BANK','ordering'=>50]);
		
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'RCBC','ordering'=>51]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'STERLING BANK','ordering'=>52]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BANK OF COMMERCE','ordering'=>53]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'UNION BANK OF THE PHILIPPINES','ordering'=>54]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ABN AMRO BANK, INC','ordering'=>55]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ACTIVE BANK','ordering'=>56]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'AIG','ordering'=>57]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ALLIED SAVINGS BANK','ordering'=>58]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'AMA BANK','ordering'=>59]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'AMERICAN EXPRESS','ordering'=>60]);
	
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ANZ BANK','ordering'=>61]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ASIA TRUST BANK','ordering'=>62]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ASIA UNITED BANK (AUB)','ordering'=>63]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ASIAN BANK','ordering'=>64]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ASIAN DEVELOPMENT BANK (ADB)','ordering'=>65]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BANCO FILIPINA','ordering'=>66]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BANCO SAN JUAN','ordering'=>67]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BANGKOK BANK','ordering'=>68]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BANK OF CHINA','ordering'=>69]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BANK OF TOKYO','ordering'=>70]);
		
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BANK ONE','ordering'=>71]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BANKARD','ordering'=>72]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'BDO EURO CURRENCY','ordering'=>73]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'CHINA BANK SAVINGS','ordering'=>74]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'CHINATRUST BANK','ordering'=>75]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'CITIBANK','ordering'=>76]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'CITIBANK SAVINGS','ordering'=>77]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'CITIBANK SAVING BANK','ordering'=>78]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'CITY ESTATE SAVINGS BANK','ordering'=>79]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'CITY SAVINGS BANK','ordering'=>80]);
	
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'DAO HENG BANK','ordering'=>81]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'DBP','ordering'=>82]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'DEUTSCHE BANK','ordering'=>83]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'DINERS CLUB','ordering'=>84]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'EASTWEST BANK','ordering'=>85]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'EASTERN BANK','ordering'=>86]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ECOLOGY SAVINGS BANK','ordering'=>87]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'EQUICOM SAVINGS BANK','ordering'=>88]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'EQUITABLE PCI BANK','ordering'=>89]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'EQUITABLE SAVINGS BANK','ordering'=>90]);

		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'EXPORT AND INDUSTRY BANK','ordering'=>91]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'EXPORT BANK','ordering'=>92]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'FAR EASTERN BANK','ordering'=>93]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'FIRST ALLIED BANK','ordering'=>94]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'FIRST CONSOLIDATED BANK','ordering'=>95]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'SECURITY BANK','ordering'=>96]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'FIRST E-BANK','ordering'=>97]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'FIRST MACRO BANK','ordering'=>98]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'GE BANK','ordering'=>99]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'GSIS FAMILY THRIFTY BANK','ordering'=>100]);

		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'GUAGUA SAVING BANK','ordering'=>101]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'HONGKONG & SHANGHAI BANK','ordering'=>102]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'HSBC SAVINGS','ordering'=>103]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'IDEAL BANK','ordering'=>104]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'INSULAR SAVINGS BANK','ordering'=>105]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'INTERNATIONAL EXCHANGE BANK','ordering'=>106]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ISLAMIC BANK','ordering'=>107]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'KEPPEL MONTE BANK','ordering'=>108]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'KOREA EXCHANGE BANK','ordering'=>109]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'LBC BANK','ordering'=>110]);

		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'LUZON DEVELOPMENTAL BANK','ordering'=>111]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'MALAYAN BANK','ordering'=>112]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'MANILA BANK','ordering'=>113]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'MAYBANK','ordering'=>114]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'MY DREAM JCB','ordering'=>115]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'ONE NETWORK BANK','ordering'=>116]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PEN BANK','ordering'=>117]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PHIL. BUSINESS BANK','ordering'=>118]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PHIL. NATIONAL BANK','ordering'=>119]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PHIL. SAVINGS BANK','ordering'=>120]);
		
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PHIL. TRUST BANK','ordering'=>121]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PHIL. VETERANS BANK','ordering'=>122]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PHILAM BANK','ordering'=>123]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PHILIPPINE BANK OF COMMUNICATIONS (PBCOM)','ordering'=>124]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PHILIPPINE TRUST COMPANY','ordering'=>125]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PILIPINO SAVERS BANK','ordering'=>126]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PLANTERS BANK','ordering'=>127]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'POSTAL BANK','ordering'=>128]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PREMIERE BANK','ordering'=>129]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'PRUDENTIAL BANK','ordering'=>130]);
		
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'RURAL BANK','ordering'=>131]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'RURAL BANK - ILOCOS NORTE','ordering'=>132]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'SARANGANI BANK','ordering'=>133]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'SECURITY BANK SAVINGS','ordering'=>134]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'STANDARD CHARTERED BANK','ordering'=>135]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'SUMMIT BANK','ordering'=>136]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'TONG YANG SAVINGS BANK','ordering'=>137]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'TRADERS ROYAL BANK','ordering'=>138]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'UCPB','ordering'=>139]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'UCPB SAVINGS BANK','ordering'=>140]);
		
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'UNITED OVERSEAS BANK','ordering'=>141]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'UNIVERSITY SAVINGS BANK','ordering'=>142]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'VETERANS BANK','ordering'=>143]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 2,'name'=>'WESTMONT BANK','ordering'=>144]);
	
		//INSERT RECIEVE OPTION LIST - MOBILE MONEY TRANSFER = 3
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 3,'name'=>'GLOBE CASH','ordering'=>145]);
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 3,'name'=>'SMART','ordering'=>146]);
		
		//INSERT RECIEVE OPTION LIST - DOOR TO DOOR DELIVERY = 4
		DB::table('library_receive_option_lists')->insert(['receive_option_id' => 4,'name'=>'DOOR TO DOOR','ordering'=>147]);
	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
