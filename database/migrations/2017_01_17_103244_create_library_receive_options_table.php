<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryReceiveOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('library_receive_options', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//DESCRIPTION
			$table
			->string('description')
			->nullable();
			
			//ORDERING
			$table
			->integer('ordering')
			->unsigned();
			
			//CREATE INDEX
			$table->index('ordering');
			
		});

		//INSERT RECEIVE OPTIONS
		DB::table('library_receive_options')->insert(['name' => 'Cash Pick-up','ordering'=>1]);
		DB::table('library_receive_options')->insert(['name' => 'Philippine Bank Account','ordering'=>2]);
		DB::table('library_receive_options')->insert(['name' => 'Mobile Money Transfer','ordering'=>3]);
		DB::table('library_receive_options')->insert(['name' => 'Door to Door Delivery','ordering'=>4]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
