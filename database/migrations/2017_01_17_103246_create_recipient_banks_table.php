<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('recipient_banks', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//RECIPIENT ID
			$table
			->integer('recipient_id')
			->unsigned()
			->nullable();
			
			//RECEIVE OPTION LIST ID
			$table
			->integer('receive_option_list_id')
			->unsigned()
			->nullable();
			
			//ACCOUNT NAME
			$table
			->string('account_name')
			->nullable();
			
			//ACCOUNT BRANCH
			$table
			->string('account_branch')
			->nullable();
			
			//ACCOUNT NO
			$table
			->string('account_no')
			->nullable();
			
			//IS PREFERRED
			$table
			->boolean('is_preferred')
			->default(0)
			->nullable();
			
			//CREATE INDEX
			$table->index('recipient_id');
			$table->index('receive_option_list_id');
			$table->index('is_preferred');
			
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
