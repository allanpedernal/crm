<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
		//SCHEMA FOR RECIPIENT
		Schema::create('recipients', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//ID NUMBER
			$table
			->uuid('id_number');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();

			//EMAIL
			$table
			->string('email')
			->nullable();

			//SECONDARY EMAIL
			$table
			->string('secondary_email')
			->nullable();

			//FILENAME
			$table
			->string('filename')
			->unique()
			->nullable();

			//REMITTER RELATIONSHIP ID
			$table
			->integer('remitter_relationship_id')
			->unsigned()
			->nullable();
			
			//TYPE
			$table
			->enum('type',array('person','company'))
			->nullable();

			//IS ACTIVE SET TO INACTIVE
			$table
			->boolean('is_active')
			->default(1)
			->nullable();
			

			//TIMESTAMP
			$table
			->timestamps();

			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('id_number');
			$table->index('email');
			$table->index('secondary_email');
			$table->index('remitter_relationship_id');
			$table->index('type');
			$table->index('is_active');

		});
		
		//SCHEMA FOR MIGRATION
		Schema::create('migrate_recipients', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
			
			//NEW REMITTER ID
			$table
			->integer('new_remitter_id')
			->unsigned()
			->nullable();
			
			//OLD RECIPIENT ID
			$table
			->integer('old_recipient_id')
			->unsigned()
			->nullable();
			
			//NEW RECIPIENT ID
			$table
			->integer('new_recipient_id')
			->unsigned()
			->nullable();
			
			//RECIPIENT FIRSTNAME
			$table
			->string('firstname')
			->nullable();
			
			//RECIPIENT LASTNAME
			$table
			->string('lastname')
			->nullable();
			
			//DATETIME
			$table
			->dateTime('datetime')
			->nullable();
			
			//CREATE INDEX
			$table->index('new_remitter_id');
			$table->index('old_recipient_id');
			$table->index('new_recipient_id');

		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
