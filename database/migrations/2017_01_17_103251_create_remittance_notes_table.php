<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemittanceNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('remittance_notes', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//REMITTANCE ID
			$table
			->integer('remittance_id')
			->unsigned()
			->nullable();

			//NOTE
			$table
			->text('note')
			->nullable();
			
			//CREATED BY
			$table
			->integer('created_by')
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('remittance_id');
			$table->index('created_by');

		 });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
