<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemittancePolipaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('remittance_polipayments', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//TRANSACTION NUMBER
			$table
			->string('transaction_number')
			->nullable();
			
			//TOKEN
			$table
			->string('token')
			->nullable();
			
			//REFERRENCE NUMBER
			$table
			->string('reference_number')
			->nullable();
			
			//STATUS
			$table
			->string('status')
			->nullable();
			
			//CONTENT
			$table
			->text('content')
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('transaction_number');
			$table->index('token');
			$table->index('reference_number');
			$table->index('status');
			
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
