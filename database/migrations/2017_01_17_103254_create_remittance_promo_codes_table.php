<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemittancePromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('remittance_promo_codes', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//NAME
			$table
			->string('name')
			->nullable();

			//DESCRIPTION
			$table
			->text('description')
			->nullable();
			
			//CLASSIFICATION
			$table
			->enum('classification',array('individual','business'))
			->nullable();
			
			//TYPE
			$table
			->enum('type',array('consumable','unli','unli100'))
			->nullable();
			
			//TOTAL CONSUMABLE
			$table
			->integer('total_consumable')
			->default(0)
			->unsigned()
			->nullable();
			
			//USER CLASSFICATION
			$table
			->enum('user_classification',array('new', 'existing', 'both'))
			->nullable();
			
			//USER ACTION
			$table
			->enum('user_action',array('assign', 'remove'))
			->default('assign')
			->nullable();
			
			//DURATION
			$table
			->integer('duration')
			->default(0)
			->unsigned()
			->nullable();
			
			//EXPIRATION DATE
			$table
			->date('expiration_date')
			->nullable();
			
			//VALUE
			$table
			->double('value', 11, 2)
			->unsigned()
			->nullable();
			
			//CREATED BY
			$table
			->integer('created_by')
			->unsigned()
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('classification');
			$table->index('type');
			$table->index('user_classification');
			$table->index('user_action');
			$table->index('created_by');

		 });
		 
		//SCHEMA FOR MIGRATION
		Schema::create('migrate_remittance_promo_codes', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
			
			//USERID
			$table
			->integer('promo_code_id')
			->unsigned()
			->nullable();
			
			//DATETIME
			$table
			->dateTime('datetime')
			->nullable();
			
			//CREATE INDEX
			$table->index('promo_code_id');

		});
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
