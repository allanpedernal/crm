<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemittancePromoCodeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('remittance_promo_code_users', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//PROMO CODE ID
			$table
			->integer('remittance_promo_code_id')
			->unsigned()
			->nullable();
			
			//REMITTER BUSINESS ID
			$table
			->integer('remitter_business_id')
			->unsigned()
			->nullable();
			
			//TOTAL CONSUMABLE
			$table
			->integer('total_consumable')
			->unsigned()
			->nullable();
			
			//ALL USER
			$table
			->boolean('all_user')
			->default(0)
			->nullable();
			
			//TYPE
			$table
			->enum('type',array('remitter', 'business'))
			->nullable();
			
			//ACTION
			$table
			->enum('action',array('assign', 'remove'))
			->nullable();
			
			//CREATE INDEX
			$table->index('remittance_promo_code_id');
			$table->index('remitter_business_id');
			$table->index('all_user');
			$table->index('type');
			$table->index('action');
			
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
