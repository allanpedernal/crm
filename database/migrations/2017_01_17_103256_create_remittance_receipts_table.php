<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemittanceReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         Schema::create('remittance_receipts', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//REMITTANCE ID
			$table
			->integer('remittance_id')
			->unsigned()
			->nullable();

			//FILENAME
			$table
			->string('filename')
			->nullable();
			
			//CREATE INDEX
			$table->index('remittance_id');

		 });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
