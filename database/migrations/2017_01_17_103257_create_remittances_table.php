<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemittancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('remittances', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//TRANSACTION NUMBER
			$table
			->string('transaction_number')
			->nullable();
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//RECIPIENT ID
			$table
			->integer('recipient_id')
			->unsigned()
			->nullable();
			
			//RECEIVE OPTION LIST ID
			$table
			->integer('receive_option_list_id')
			->unsigned()
			->nullable();
			
			//PAYMENT TYPE
			$table
			->enum('payment_type',array('eft','polipayment'))
			->nullable();
			
			//BANK SOURCE ID
			$table
			->integer('bank_source_id')
			->unsigned()
			->nullable();
			
			//AMOUNT SENT
			$table
			->double('amount_sent', 20, 2)
			->unsigned()
			->nullable();
			
			//STATUS ID
			$table
			->integer('status_id')
			->unsigned()
			->nullable();
			
			//HAS RECEIPT SET TO 0 - NO RECEIPT
			$table
			->boolean('has_receipt')
			->default(0)
			->nullable();
			
			//SERVICE FEE
			$table
			->double('service_fee', 11, 2)
			->unsigned()
			->nullable();
			
			//FOREX RATE
			$table
			->double('forex_rate', 11, 2)
			->unsigned()
			->nullable();
			
			//TRADE RATE
			$table
			->double('trade_rate', 11, 2)
			->unsigned()
			->nullable();
			
			//REASON ID
			$table
			->integer('reason_id')
			->unsigned()
			->nullable();
			
			//NATURE OF WORK ID
			$table
			->integer('nature_of_work_id')
			->unsigned()
			->nullable();
			
			//SOURCE OF FUND ID
			$table
			->integer('source_of_fund_id')
			->unsigned()
			->nullable();
			
			//PROMO CODE ID
			$table
			->integer('promo_code_id')
			->unsigned()
			->nullable();
			
			//DATE TRANSFERRED
			$table
			->dateTime('date_transferred')
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('transaction_number');
			$table->index('remitter_id');
			$table->index('recipient_id');
			$table->index('payment_type');
			$table->index('bank_source_id');
			$table->index('status_id');
			$table->index('has_receipt');
			$table->index('reason_id');
			$table->index('promo_code_id');
			
		});
		
		 //SCHEMA FOR MIGRATION
		 Schema::create('migrate_remittances', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//OLD REMITTANCE ID
			$table
			->integer('old_remittance_id')
			->unsigned()
			->nullable();
			
			//NEW REMITTANCE ID
			$table
			->integer('new_remittance_id')
			->unsigned()
			->nullable();

			//DATETIME
			$table
			->dateTime('datetime')
			->nullable();
			
			//CREATE INDEX
			$table->index('old_remittance_id');
			$table->index('new_remittance_id');

		 });
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
