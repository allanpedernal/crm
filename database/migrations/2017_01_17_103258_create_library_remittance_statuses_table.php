<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryRemittanceStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('library_remittance_statuses', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//DESCRIPTION
			$table
			->string('description')
			->nullable();
			
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		 });
		 
		 //INSERT REMITTANCE STATUS
		 DB::table('library_remittance_statuses')->insert(['name'=>'Remittance Instruction Received','ordering'=>1]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Waiting for Sender ID','ordering'=>2]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Expired ID','ordering'=>3]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Expiring ID Active Client','ordering'=>4]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Waiting for Transfer Receipt','ordering'=>5]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Waiting for Complete Recepient Details','ordering'=>6]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Waiting For AUD Transfer to Clear In Our Bank Account','ordering'=>7]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Waiting for AMLA requirement','ordering'=>8]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Waiting for CSR Upload','ordering'=>9]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'For Processing B','ordering'=>10]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'For Processing M','ordering'=>11]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Pending','ordering'=>12]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Money is in Transit to Your Recipient','ordering'=>13]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'For Reverse Remittance','ordering'=>14]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Reverse Transfer Completed','ordering'=>15]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'For Refund','ordering'=>16]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Remittance Returned Waiting Feedback from Sender','ordering'=>17]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Cancelled-Change in Recipient Details','ordering'=>18]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Cancelled - Client Deposit Refunded','ordering'=>19]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Cancelled: Duplicate Remittance','ordering'=>20]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Cancelled Transaction By Client','ordering'=>21]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Cancelled No Deposit Received','ordering'=>22]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Cancelled - Test Transaction','ordering'=>23]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Transfer Complete','ordering'=>24]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'POLi Successful','ordering'=>25]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'POLi Cancelled','ordering'=>26]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Remittance Cancelled','ordering'=>27]);
		 DB::table('library_remittance_statuses')->insert(['name'=>'Summary Page','ordering'=>28]);
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
