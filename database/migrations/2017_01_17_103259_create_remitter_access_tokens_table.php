<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('remitter_access_tokens', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//ACCESS TOKEN
			$table
			->string('access_token')
			->nullable();
			
			//DEVICE TOKEN
			$table
			->string('device_token')
			->nullable();
			
			//DEVICE TYPE ID
			$table
			->string('device_type_id')
			->nullable();
			
			//DEVICE TYPE ID
			$table
			->string('app_version',55)
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('remitter_id');
			$table->index('access_token');
			$table->index('device_token');
			$table->index('device_type_id');
			
		 });
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
