<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('remitter_addresses', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//CURRENT ADDRESS
			$table
			->string('current_address')
			->nullable();
			
			//PERMANENT ADDRESS
			$table
			->string('permanent_address')
			->nullable();
			
			//STATE ID
			$table
			->integer('country_id')
			->unsigned()
			->nullable();
			
			//STATE ID
			$table
			->integer('state_id')
			->unsigned()
			->nullable();
			
			//CITY ID
			$table
			->integer('city_id')
			->unsigned()
			->nullable();
			
			//POSTAL CODE
			$table
			->string('postal_code')
			->nullable();
			
			//TENANCY LENGTH ID
			$table
			->integer('tenancy_length_id')
			->unsigned()
			->nullable();
			
			//IS RESIDENT
			$table
			->boolean('is_resident')
			->default(0)
			->nullable();
			
			//IS PREFERRED
			$table
			->boolean('is_preferred')
			->default(0)
			->nullable();
			
			//CREATE INDEX
			$table->index('remitter_id');
			$table->index('country_id');
			$table->index('state_id');
			$table->index('city_id');
			$table->index('is_resident');
			$table->index('is_preferred');
			
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
