<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterConfirmationSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
		Schema::create('remitter_confirmation_sms', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//CODE
			$table
			->string('code',50)
			->nullable();
			
			//COUNT
			$table
			->integer('count')
			->default(0)
			->unsigned()
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('remitter_id');
			
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
