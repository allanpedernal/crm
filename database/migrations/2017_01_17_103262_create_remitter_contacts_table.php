<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
		Schema::create('remitter_contacts', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//CONTACT TYPE ID
			$table
			->integer('contact_type_id')
			->unsigned()
			->nullable();
			
			//CONTACT
			$table
			->string('contact')
			->nullable();
			
			//IS PREFERRED
			$table
			->boolean('is_preferred')
			->default(0)
			->nullable();
			
			//CREATE INDEX
			$table->index('remitter_id');
			$table->index('contact_type_id');
			$table->index('is_preferred');
			
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
