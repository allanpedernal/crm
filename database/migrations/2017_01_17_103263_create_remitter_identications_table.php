<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterIdenticationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remitter_identifications', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//NUMBER
			$table
			->string('number')
			->nullable();
			
			//EXPIRATION DATE
			$table
			->date('expiration_date')
			->nullable();
			
			//FILENAME
			$table
			->string('filename')
			->nullable();
			
			//IDENTIFICATION TYPE ID
			$table
			->integer('identification_type_id')
			->unsigned()
			->nullable();
			
			//OTHER TYPE
			$table
			->string('other_type')
			->nullable();
			
			//IS PREFERRED
			$table
			->boolean('is_preferred')
			->default(0)
			->nullable();
			
			//CREATE INDEX
			$table->index('remitter_id');
			$table->index('expiration_date');
			$table->index('identification_type_id');
			$table->index('is_preferred');
			
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
