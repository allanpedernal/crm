<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterReferralConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('remitter_referral_conversions', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//ID NUMBER
			$table
			->string('id_number')
			->nullable();
			
			//IS CONVERTED
			$table
			->boolean('is_converted')
			->default(0)
			->nullable();
			
			//IS CONVERTED
			$table
			->dateTime('converted_datetime')
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('id_number');
			$table->index('is_converted');
			
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
