<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
		Schema::create('remitter_referrals', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//FIRSTNAME
			$table
			->string('firstname')
			->nullable();
			
			//LASTNAME
			$table
			->string('lastname')
			->nullable();
			
			//PHONE
			$table
			->string('phone')
			->unique()
			->nullable();
			
			//EMAIL
			$table
			->string('email')
			->unique()
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('remitter_id');
			$table->index('email');
		
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
