<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryRemitterRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('library_remitter_relationships', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//DESCRIPTION
			$table
			->string('description')
			->nullable();
			
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		 });
		 
		  //INSERT REMITTER RELATIONSHIP
		  DB::table('library_remitter_relationships')->insert(['name' => 'Others','ordering'=>10]);
		  DB::table('library_remitter_relationships')->insert(['name' => 'Parent','ordering'=>1]);
		  DB::table('library_remitter_relationships')->insert(['name' => 'Sibling','ordering'=>2]);
		  DB::table('library_remitter_relationships')->insert(['name' => 'Spouse','ordering'=>3]);
		  DB::table('library_remitter_relationships')->insert(['name' => 'Child / Children','ordering'=>4]);
		  DB::table('library_remitter_relationships')->insert(['name' => 'Distant Relative','ordering'=>5]);
		  DB::table('library_remitter_relationships')->insert(['name' => 'Employee','ordering'=>6]);
		  DB::table('library_remitter_relationships')->insert(['name' => 'Business Associate','ordering'=>7]);
		  DB::table('library_remitter_relationships')->insert(['name' => 'Realtor','ordering'=>8]);
		  DB::table('library_remitter_relationships')->insert(['name' => 'Debtors','ordering'=>9]);
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
