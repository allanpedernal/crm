<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemittersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

		//SCHEMA FOR REMITTER
		Schema::create('remitters', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//ID NUMBER
			$table
			->uuid('id_number')
			->nullable();

			//WORDPRESS USERID
			$table
			->integer('userid')
			->unsigned()
			->nullable();
			
			//TYPE ID
			$table
			->integer('type_id')
			->unsigned()
			->nullable();
			
			//TITLE
			$table
			->integer('title_id')
			->unsigned()
			->nullable();

			//FIRSTNAME
			$table
			->string('firstname')
			->nullable();

			//MIDDLENAME
			$table
			->string('middlename')
			->nullable();

			//LASTNAME
			$table
			->string('lastname')
			->nullable();

			//GENDER
			$table
			->enum('gender',array('male','female'))
			->nullable();

			//BIRTHDAY
			$table
			->date('birthday')
			->nullable();

			//CIVIL STATUS ID
			$table
			->integer('civil_status_id')
			->unsigned()
			->nullable();
			
			//OCCUPATION
			$table
			->string('occupation')
			->nullable();

			//EMAIL
			$table
			->string('email')
			->nullable();

			//SECONDARY EMAIL
			$table
			->string('secondary_email')
			->nullable();

			//FILENAME
			$table
			->string('filename')
			->unique()
			->nullable();

			//NATIONALITY
			$table
			->integer('nationality_id')
			->unsigned()
			->nullable();
			
			//EMAIL VERIFIED
			$table
			->boolean('email_verified')
			->default(1)
			->nullable();

			//SMS VERIFIED
			$table
			->boolean('sms_verified')
			->default(1)
			->nullable();
			
			//MARKETING SOURCE ID
			$table
			->integer('marketing_source_id')
			->unsigned()
			->nullable();
			
			//IS PERSONAL
			$table
			->boolean('is_personal')
			->default(1)
			->nullable();
			
			//IS COMPANY
			$table
			->boolean('is_company')
			->default(0)
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();

			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('id_number');
			$table->index('userid');
			$table->index('type_id');
			$table->index('gender');
			$table->index('civil_status_id');
			$table->index('email');
			$table->index('secondary_email');
			$table->index('nationality_id');
			$table->index('email_verified');
			$table->index('sms_verified');
			$table->index('marketing_source_id');
			$table->index('is_personal');
			$table->index('is_company');

		});

		//SCHEMA FOR MIGRATION
		Schema::create('migrate_remitters', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//USERID
			$table
			->integer('userid')
			->unsigned()
			->nullable();
			
			//OLD REMITTER ID
			$table
			->integer('old_remitter_id')
			->unsigned()
			->nullable();
			
			//NEW REMITTER ID
			$table
			->integer('new_remitter_id')
			->unsigned()
			->nullable();

			//DATETIME
			$table
			->dateTime('datetime')
			->nullable();
			
			//CREATE INDEX
			$table->index('userid');
			$table->index('old_remitter_id');
			$table->index('new_remitter_id');

		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
