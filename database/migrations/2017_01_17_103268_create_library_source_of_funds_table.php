<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibrarySourceOfFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         Schema::create('library_source_of_funds', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//NAME
			$table
			->string('name')
			->nullable();

			//DESCRIPTION
			$table
			->string('description')
			->nullable();

			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		 });

		 //INSERT SOURCE OF FUNDS
		 DB::table('library_source_of_funds')->insert(['name' => 'Others','ordering'=>13]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Allotment','ordering'=>1]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Bank Deposits','ordering'=>2]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Bonds','ordering'=>3]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Business Asset','ordering'=>4]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Credit / Bank Lending','ordering'=>5]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Government Assistance','ordering'=>6]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Investment','ordering'=>7]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Pension','ordering'=>8]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Retirement Asset','ordering'=>9]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Salary/Wage','ordering'=>10]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Sale of Asset','ordering'=>11]);
		 DB::table('library_source_of_funds')->insert(['name' => 'Stocks','ordering'=>12]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
