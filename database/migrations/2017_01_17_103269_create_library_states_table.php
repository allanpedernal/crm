<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatelibraryStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         Schema::create('library_states', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//SHORTNAME
			$table
			->string('shortname')
			->nullable();

			//NAME
			$table
			->string('name')
			->nullable();

			//COUNTRY ID
			$table
			->integer('country_id')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('country_id');

		 });

		 //INSERT AU STATE
		 DB::table('library_states')->insert(['shortname'=>'ACT','name'=>'Australian Capital Territory','country_id'=>14]);
		 DB::table('library_states')->insert(['shortname'=>'NSW','name'=>'New South Whales','country_id'=>14]);
		 DB::table('library_states')->insert(['shortname'=>'NT','name'=>'Northern Territory','country_id'=>14]);
		 DB::table('library_states')->insert(['shortname'=>'QLD','name'=>'Queensland','country_id'=>14]);
		 DB::table('library_states')->insert(['shortname'=>'SA','name'=>'South Australia','country_id'=>14]);
		 DB::table('library_states')->insert(['shortname'=>'TAS','name'=>'Tasmania','country_id'=>14]);
		 DB::table('library_states')->insert(['shortname'=>'VIC','name'=>'Victoria','country_id'=>14]);
		 DB::table('library_states')->insert(['shortname'=>'WA','name'=>'Western Australia','country_id'=>14]);
		 
		 /*
		 //INSERT PH STATE = PROVINCE
		 DB::table('library_states')->insert(['name'=>'Abra','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Agusan del Norte','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Agusan del Sur','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Aklan','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Albay','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Antique','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Apayao','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Aurora','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Basilan','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Bataan','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Batanes','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Batangas','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Benguet','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Biliran','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Bohol','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Bukidnon','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Bulacan','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Cagayan','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Camarines Norte','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Camarines Sur','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Camiguin','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Capiz','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Catanduanes','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Cavite','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Cebu','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Compostela Valley','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Cotabato','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Davao del Norte','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Davao del Sur','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Davao Occidental','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Davao Oriental','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Dinagat Islands','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Eastern Samar','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Guimaras','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Ifugao','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Ilocos Norte','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Ilocos Sur','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Iloilo','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Isabela','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Kalinga','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'La Union','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Laguna','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Lanao del Norte','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Lanao del Sur','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Leyte','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Maguindanao','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Marinduque','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Masbate','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Metro Manila','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Misamis Occidental','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Misamis Oriental','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Mountain Province','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Negros Occidental','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Northern Samar','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Nueva Ecija','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Nueva Vizcaya','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Occidental Mindoro','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Oriental Mindoro','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Palawan','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Pampanga','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Pangasinan','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Quezon','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Quirino','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Rizal','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Romblon','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Samar','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Sarangani','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Siquijor','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Sorsogon','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'South Cotabato','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Southern Leyte','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Sultan Kudarat','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Sulu','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Surigao del Norte','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Surigao del Sur','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Tarlac','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Tawi-Tawi','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Zambales','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Zamboanga del Norte','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Zamboanga del Sur','country_id'=>2]);
		 DB::table('library_states')->insert(['name'=>'Zamboanga Sibugay','country_id'=>2]);
		 */
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		
    }
}
