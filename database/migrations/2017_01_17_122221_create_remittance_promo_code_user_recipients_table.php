<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemittancePromoCodeUserRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('remittance_promo_code_user_recipients', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//REMITTANCE PROMO CODE ID
			$table
			->integer('remittance_promo_code_id')
			->unsigned()
			->nullable();
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//RECIPIENT ID
			$table
			->integer('recipient_id')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('remittance_promo_code_id','remittance_promo_code_index');
			$table->index('remitter_id');
			$table->index('recipient_id');
			
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
