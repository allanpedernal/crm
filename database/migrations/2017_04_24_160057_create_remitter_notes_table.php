<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('remitter_notes', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//RECIPIENT ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();

			//NOTE
			$table
			->text('note')
			->nullable();
			
			//CREATED BY
			$table
			->integer('created_by')
			->nullable();
			
			//CREATE INDEX
			$table->index('remitter_id');
			$table->index('created_by');

		 });
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
