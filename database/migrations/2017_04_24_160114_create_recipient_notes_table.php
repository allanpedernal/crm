<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('recipient_notes', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//RECIPIENT ID
			$table
			->integer('recipient_id')
			->unsigned()
			->nullable();

			//NOTE
			$table
			->text('note')
			->nullable();
			
			//CREATED BY
			$table
			->integer('created_by')
			->nullable();
			
			//TIMESTAMP
			$table
			->timestamps();
			
			//SOFT DELETE
			$table
			->softDeletes();
			
			//CREATE INDEX
			$table->index('recipient_id');
			$table->index('created_by');

		 });
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
