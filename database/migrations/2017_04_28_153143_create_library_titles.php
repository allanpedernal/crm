<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryTitles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('library_titles', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
		
			//ABBR
			$table
			->string('abbr')
			->nullable();
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//DESCRIPTION
			$table
			->string('description')
			->nullable();
			
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		});
		
		//INSERT TITLES
		DB::table('library_titles')->insert(['abbr'=>'Mr.','name'=>'Mister','ordering'=>1]);
		DB::table('library_titles')->insert(['abbr'=>'Mrs.','name'=>'Misis','ordering'=>2]);
		DB::table('library_titles')->insert(['abbr'=>'Ms.','name'=>'Miss','ordering'=>3]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
