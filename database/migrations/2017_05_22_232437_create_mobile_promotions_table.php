<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobilePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('mobile_promotions', function (Blueprint $table) {
			
			//ID
            $table
            ->increments('id');
            
            //EXCERPT
            $table
            ->string('excerpt')
            ->nullable();
            
            //DESCRIPTION
            $table
            ->text('description')
            ->nullable();
            
            //SEND TIMES
            $table
            ->integer('send_times')
			->default(0)
			->nullable();
            
            //IS SEND
            $table
            ->boolean('is_send')
			->default(0)
			->nullable();
            
            //USER ID
            $table
            ->bigInteger('created_by')
            ->comment('userid')
            ->nullable();
            
			//TIMESTAMP
			$table
			->timestamps();

			//SOFT DELETE
			$table
			->softDeletes();
            
			//CREATE INDEX
			$table->index('send_times');
			$table->index('is_send');
			$table->index('created_by');
            
        });
        
		//SCHEMA FOR MIGRATION
		Schema::create('migrate_mobile_promotions', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
			
			//OLD MOBILE PROMOTION ID
			$table
			->integer('old_mobile_promotion_id')
			->unsigned()
			->nullable();
			
			//NEW MOBILE PROMOTION ID
			$table
			->integer('new_mobile_promotion_id')
			->unsigned()
			->nullable();
			
			//DATETIME
			$table
			->dateTime('datetime')
			->nullable();
			
			//CREATE INDEX
			$table->index('old_mobile_promotion_id');
			$table->index('new_mobile_promotion_id');

		});
		 
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
