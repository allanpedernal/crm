<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientOtherBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('recipient_other_banks', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//RECIPIENT BANK ID
			$table
			->integer('recipient_bank_id')
			->unsigned()
			->nullable();
			
			//BANK NAME
			$table
			->string('bank_name')
			->nullable();
			
			//CREATE INDEX
			$table->index('recipient_bank_id');
			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
