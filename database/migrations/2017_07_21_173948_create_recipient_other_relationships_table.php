<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientOtherRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('recipient_other_relationships', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//RECIPIENT ID
			$table
			->integer('recipient_id')
			->unsigned()
			->nullable();
			
			//RELATIONSHIP
			$table
			->string('relationship')
			->nullable();
			
			//CREATE INDEX
			$table->index('recipient_id');
			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
