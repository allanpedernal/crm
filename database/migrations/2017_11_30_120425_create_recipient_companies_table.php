<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('recipient_companies', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');

			//RECIPIENT ID
			$table
			->integer('recipient_id')
			->unsigned()
			->nullable();
			
			//COMPANY NAME
			$table
			->string('company_name')
			->nullable();

			//REG NUMBER
			$table
			->string('reg_number')
			->nullable();
			
			//CONTACT PERSON
			$table
			->string('contact_person')
			->nullable();
			
			//AUTHORIZED PERSON
			$table
			->string('authorized_person')
			->nullable();
			
			//CREATE INDEX
			$table->index('recipient_id');
			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
