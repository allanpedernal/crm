<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		//SCHEMA FOR RECIPIENT
		Schema::create('recipient_persons', function (Blueprint $table) {

			//ID
			$table
			->increments('id');

			//RECIPIENT ID
			$table
			->integer('recipient_id')
			->unsigned()
			->nullable();

			//TITLE
			$table
			->integer('title_id')
			->unsigned()
			->nullable();

			//FIRSTNAME
			$table
			->string('firstname')
			->nullable();

			//MIDDLENAME
			$table
			->string('middlename')
			->nullable();

			//LASTNAME
			$table
			->string('lastname')
			->nullable();

			//GENDER
			$table
			->enum('gender',array('male','female'))
			->nullable();

			//BIRTHDAY
			$table
			->date('birthday')
			->nullable();

			//CIVIL STATUS ID
			$table
			->integer('civil_status_id')
			->unsigned()
			->nullable();
			
			//NATIONALITY ID
			$table
			->integer('nationality_id')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('recipient_id');
			$table->index('title_id');
			$table->index('gender');
			$table->index('civil_status_id');
			$table->index('nationality_id');
			
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
