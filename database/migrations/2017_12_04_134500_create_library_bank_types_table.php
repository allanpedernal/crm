<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryBankTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('library_bank_types', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
		
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		});
		
		//INSERT REMITTER BANK TYPE
		DB::table('library_bank_types')->insert(['name'=>'Individual/Personal Bank Account','ordering'=>1]);
		DB::table('library_bank_types')->insert(['name'=>'Business Account','ordering'=>2]);
		DB::table('library_bank_types')->insert(['name'=>'Payroll Bank Account','ordering'=>3]);
		DB::table('library_bank_types')->insert(['name'=>'Joint Account-Husband/Wife/Partner with the same Residence Address','ordering'=>4]);
		DB::table('library_bank_types')->insert(['name'=>'Joint Account-Husband/Wife/Partner with different Residence Address','ordering'=>5]);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
