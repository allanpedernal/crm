<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryRemittanceFrequenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('library_remittance_frequencies', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
		
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		});
		
		//INSERT REMITTER REMITTANCE FREQUENCIES
		DB::table('library_remittance_frequencies')->insert(['name'=>'Once a month','ordering'=>1]);
		DB::table('library_remittance_frequencies')->insert(['name'=>'Twice a month','ordering'=>2]);
		DB::table('library_remittance_frequencies')->insert(['name'=>'Bi-monthly','ordering'=>3]);
		DB::table('library_remittance_frequencies')->insert(['name'=>'Every quarter','ordering'=>4]);
		DB::table('library_remittance_frequencies')->insert(['name'=>'Every six months','ordering'=>5]);
		DB::table('library_remittance_frequencies')->insert(['name'=>'Once a year','ordering'=>6]);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
