<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryRemitterMarketingSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('library_remitter_marketing_sources', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
		
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		});
		
		//INSERT REMITTER MARKETING SOURCES
		DB::table('library_remitter_marketing_sources')->insert(['name'=>'Others','ordering'=>6]);
		DB::table('library_remitter_marketing_sources')->insert(['name'=>'Referred by iRemit Employees','ordering'=>1]);
		DB::table('library_remitter_marketing_sources')->insert(['name'=>'Referred by a friend','ordering'=>2]);
		DB::table('library_remitter_marketing_sources')->insert(['name'=>'Via Facebook','ordering'=>3]);
		DB::table('library_remitter_marketing_sources')->insert(['name'=>'Online search','ordering'=>4]);
		DB::table('library_remitter_marketing_sources')->insert(['name'=>'Filipino event','ordering'=>5]);
	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
