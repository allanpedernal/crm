<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryRemitterAddressTenancyLengthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	
        Schema::create('library_remitter_address_tenancy_lengths', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
		
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		});
		
		//INSERT REMITTER ADDRESS TENANCY LENGTHS
		DB::table('library_remitter_address_tenancy_lengths')->insert(['name'=>'Not a permamnent residence','ordering'=>1]);
		DB::table('library_remitter_address_tenancy_lengths')->insert(['name'=>'Less than a year','ordering'=>2]);
		DB::table('library_remitter_address_tenancy_lengths')->insert(['name'=>'1-2 years','ordering'=>3]);
		DB::table('library_remitter_address_tenancy_lengths')->insert(['name'=>'2-5 years','ordering'=>4]);
		DB::table('library_remitter_address_tenancy_lengths')->insert(['name'=>'5-10 years','ordering'=>5]);
		DB::table('library_remitter_address_tenancy_lengths')->insert(['name'=>'10 years and up','ordering'=>6]);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
