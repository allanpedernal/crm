<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryRemitterTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('library_remitter_types', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//NAME
			$table
			->string('name')
			->nullable();
		
			//ORDERING
			$table
			->integer('ordering')
			->unsigned()
			->nullable();
			
			//CREATE INDEX
			$table->index('ordering');

		});
		
		//INSERT REMITTER TYPES
		DB::table('library_remitter_types')->insert(['name'=>'Individual','ordering'=>1]);
		DB::table('library_remitter_types')->insert(['name'=>'Company','ordering'=>2]);
		DB::table('library_remitter_types')->insert(['name'=>'List and/or regulated FI','ordering'=>3]);
		DB::table('library_remitter_types')->insert(['name'=>'Register cooperatives, Associations','ordering'=>4]);
		DB::table('library_remitter_types')->insert(['name'=>'Government/Public authority','ordering'=>5]);
		DB::table('library_remitter_types')->insert(['name'=>'Non regulated FI/Corporation','ordering'=>6]);
		DB::table('library_remitter_types')->insert(['name'=>'Non profit org., Charity institutions, Foundations','ordering'=>7]);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
