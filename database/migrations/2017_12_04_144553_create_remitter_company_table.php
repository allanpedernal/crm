<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
         Schema::create('remitter_companies', function (Blueprint $table) {

			//ID
			$table
			->increments('id');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//NAME
			$table
			->string('name')
			->nullable();
			
			//ADDRESS
			$table
			->string('address')
			->nullable();
			
			//CONTACT
			$table
			->string('contact')
			->nullable();
			
			//REGISTRATION DATE
			$table
			->date('registration_date')
			->nullable();
			
			//COUNTRY ID
			$table
			->integer('country_id')
			->unsigned()
			->nullable();
			
			//COMPANY RELATIONSHIP ID
			$table
			->integer('company_relationship_id')
			->unsigned()
			->nullable();
			
			//SOURCE OF FUND ID
			$table
			->integer('source_of_fund_id')
			->unsigned()
			->nullable();
			
			//REMITTANCE RANGE
			$table
			->enum('remittance_range',array('0 - 5,000','5,001 - 10,000','10,001 - above'))
			->nullable();
			
			//FREQUENCY ID
			$table
			->integer('frequency_id')
			->unsigned()
			->nullable();
			
			//BANK TYPE ID
			$table
			->integer('bank_type_id')
			->unsigned()
			->nullable();
			

			//CREATE INDEX
			$table->index('remitter_id');
			$table->index('country_id');
			$table->index('company_relationship_id');
			$table->index('source_of_fund_id');
			$table->index('remittance_range');
			$table->index('frequency_id');
			$table->index('bank_type_id');

		 });
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
