<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterCompanyOtherSourceOfFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remitter_company_other_source_of_funds', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//REMITTER COMPANY ID
			$table
			->integer('remitter_company_id')
			->unsigned()
			->nullable();
			
			//FUND
			$table
			->string('source_of_fund')
			->nullable();
			
			//CREATE INDEX
			$table->index('remitter_company_id');
			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
