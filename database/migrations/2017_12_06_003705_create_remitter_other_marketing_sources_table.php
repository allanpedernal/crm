<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemitterOtherMarketingSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('remitter_other_marketing_sources', function (Blueprint $table) {
			
			//ID
			$table
			->increments('id');
			
			//REMITTER ID
			$table
			->integer('remitter_id')
			->unsigned()
			->nullable();
			
			//MARKETING SOURCE
			$table
			->string('marketing_source')
			->nullable();
			
			//CREATE INDEX
			$table->index('remitter_id');
			
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
