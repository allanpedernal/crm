@extends('layouts.login')

@section('content')

	<div>
		
		<div class="login_wrapper">
			
			<div class="animate form login_form">
				
				<section class="login_content">
					
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
						
                        {{ csrf_field() }}
						
						<h1> iRemit CRM </h1>

						<div>
							
							<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

							@if ($errors->has('email'))

								<span class="help-block">
									
									<strong> {{ $errors->first('email') }} </strong>
									
								</span>
								
							@endif
									
						</div>

						<div>
							
							<input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
							
							@if ($errors->has('password'))

								<span class="help-block">
									
									<strong> {{ $errors->first('password') }} </strong>

								</span>

							@endif

						</div>

						<div>
							
							 <button type="submit" class="btn btn-default btn-sm submit"> <i class="fa fa-sign-in"></i> Log in </button>

						</div>

						<div class="clearfix"></div>

						<div class="separator">
							
							<div class="clearfix"></div>

							<div>
								
								<h1> <i class="fa fa-paw"></i> iRemit to the Philippines </h1>

								<p> &copy; All Rights Reserved. iRemit to the Philippines Pty. Ltd. Australian Business Number 36157688394 </p>

							</div>

						</div>

					</form>

				</section>

			</div>

		</div>

	</div>

@endsection
