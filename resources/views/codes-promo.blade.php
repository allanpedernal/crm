@extends('layouts.iremit')

@section('content')

	<div class="page-title">
		
		<div class="title_left">
			
			<h3> <i class="fa fa-percent fa-fw"></i> Promo Codes </h3>
			
		</div>
		
	</div>

	<div class="clearfix"></div>

	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
			<div class="x_panel">
				
				<div class="x_title">

					<!-- <button type="button" class="btn btn-primary btn-sm add_promo_code" style="margin: 5px 0px 0px;" data-toggle="modal" data-target="#add-promo-code"> -->					
					<button type="button" class="btn btn-primary btn-sm add_promo_code" style="margin: 5px 0px 0px;"> <i class="fa fa-plus" aria-hidden="true"></i> Add New Promo Code </button>
					
					<ul class="nav navbar-right panel_toolbox">
						
						<li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
						
						<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> </li>
						
						<li> <a class="close-link"> <i class="fa fa-close"></i> </a> </li>
					
					</ul>
					
					<div class="clearfix"></div>
					
				</div>
				
				<div class="x_content">

					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						
						<div class="panel panel-default">
							
							<div class="panel-heading" role="tab" id="headingOne">
								
								<h4 class="panel-title">
									
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="<?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'true' : 'false' ); ?>" aria-controls="collapseOne" <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? '' : 'class="collapsed"' ); ?>>

										<small> Click me to show search </small>

									</a>

								</h4>

							</div>

							<div id="collapseOne" class="panel-collapse collapse <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'in' : '' ); ?>" role="tabpanel" aria-labelledby="headingOne">

								<div class="panel-body">
									
									<form method="GET" id="promo_code_search_form" class="form-horizontal">

										<div class="row">
											
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="search_name"> Promo code: </label>
													
													<input type="text" id="search_name" name="search_name" class="form-control" value="<?php echo ( isset( $_GET[ 'search_name' ] ) && ! empty( $_GET[ 'search_name' ] ) ? $_GET[ 'search_name' ] : '' ); ?>"> 
												
												</div>
											
											</div>
											
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="search_classification"> Classification: </label>
													
														<select name="search_classification" id=search_classification" class="form-control chosen-select-width" data-placeholder="Classification">
														        
													        <option value=""></option>

													        <option value="individual" <?php echo ( isset( $_GET[ 'search_classification' ] ) && ! empty( $_GET[ 'search_classification' ] && ( $_GET[ 'search_classification' ] ) == "individual") ? 'selected' : '' );?> > Individual </option>
													        
													        <option value="business" <?php echo ( isset( $_GET[ 'search_classification' ] ) && ! empty( $_GET[ 'search_classification' ] && ( $_GET[ 'search_classification' ] ) == "business") ? 'selected' : '' );?> > Business </option>

														</select>

												</div>
											
											</div>
											
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label form="search_type"> Type: </label>
													
														<select name="search_type" id="search_type" class="form-control chosen-select-width" data-placeholder="Type">

														        <option value=""></option>

														        <option value="consumable" <?php echo ( isset( $_GET[ 'search_type' ] ) && ! empty( $_GET[ 'search_type' ] && ( $_GET[ 'search_type' ] ) == "consumable") ? 'selected' : '' );?> > Consumable </option>
														        
														        <option value="unli" <?php echo ( isset( $_GET[ 'search_type' ] ) && ! empty( $_GET[ 'search_type' ] && ( $_GET[ 'search_type' ] ) == "unli") ? 'selected' : '' );?> > Unli </option>
														        
														        <option value="unli100" <?php echo ( isset( $_GET[ 'search_type' ] ) && ! empty( $_GET[ 'search_type' ] && ( $_GET[ 'search_type' ] ) == "unli100") ? 'selected' : '' );?> > Unli 100 </option>

														</select>
												
												</div>
											
											</div>
											
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="search_user_classification"> User Classification: </label>
													
														<select name="search_user_classification" id="search_user_classification" class="form-control chosen-select-width" data-placeholder="User classification">

														        <option value=""></option>
														        
														        <option value="new" <?php echo ( isset( $_GET[ 'search_user_classification' ] ) && ! empty( $_GET[ 'search_user_classification' ] && ( $_GET[ 'search_user_classification' ] ) == "new") ? 'selected' : '' );?> > New </option>
														        
														        <option value="existing" <?php echo ( isset( $_GET[ 'search_user_classification' ] ) && ! empty( $_GET[ 'search_user_classification' ] && ( $_GET[ 'search_user_classification' ] ) == "existing") ? 'selected' : '' );?> > Existing </option>

														</select>

												</div>
											
											</div>
											
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="search_user_action"> User Action: </label>
													
														<select id="search_user_action" name="search_user_action" class="form-control chosen-select-width" data-placeholder="Action">

																<option value=""></option>
														        
														        <option value="assign" <?php echo ( isset( $_GET[ 'search_user_action' ] ) && ! empty( $_GET[ 'search_user_action' ] && ( $_GET[ 'search_user_action' ] ) == "assign") ? 'selected' : '' );?> >Assign</option>
														        
														        <option value="remove" <?php echo ( isset( $_GET[ 'search_user_action' ] ) && ! empty( $_GET[ 'search_user_action' ] && ( $_GET[ 'search_user_action' ] ) == "remove") ? 'selected' : '' );?> >Remove</option>

														</select>
												
												</div>
											
											</div>

											<div class="col-lg-1 col-md-1 col-sm-10 col-xs-10">
											
												<div class="form-group">
												
													<label for="search_duration"> Duration: </label>
													
													<input type="text" id="search_duration" name="search_duration" class="form-control" value="<?php echo ( isset( $_GET[ 'search_duration' ] ) && ! empty( $_GET[ 'search_duration' ] ) ? $_GET[ 'search_duration' ] : '' ); ?>"> 
												
												</div>
											
											</div>		

											<div class="col-lg-1 col-md-1 col-sm-10 col-xs-10">
											
												<div class="form-group">
												
													<label for="search_expiration_date"> Expiration: </label>
													
													<input type="text" id="search_expiration_date" name="search_expiration_date" class="form-control" value="<?php echo ( isset( $_GET[ 'search_expiration_date' ] ) && ! empty( $_GET[ 'search_expiration_date' ] ) ? $_GET[ 'search_expiration_date' ] : '' ); ?>"> 
												
												</div>
											
											</div>																					
										</div>
										
										<div class="row">
										
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												
												<a href="{{ url( 'codes/promos' ) }}" class="btn btn-default btn-sm"> <i class="fa fa-refresh" aria-hidden="true"></i> Reset </a>
												
												<button type="submit" name="btn_submit" value="search_promo_code" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> Search </button>
												
											</div>
										
										</div>

									</form>
									
								</div>

							</div>

						</div>

					</div>
					
					@include( 'tables.promo_code_table')
				
				</div>
			
			</div>
			
		</div>
		
	</div>

	<!-- Add New Promo Modal placeholder -->
	@include('modals.codes.promos.pop-up')

@endsection

@section('styles')

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
	
	<link href="{{ URL::asset('css/chosen.min.css') }}" rel="stylesheet">
	
@endsection

@section('scripts')

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js') }} "></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/jszip/dist/jszip.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/pdfmake/build/pdfmake.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
	
	<script src="{{ URL::asset('bower_components/gentelella/vendors/dropzone/dist/min/dropzone.min.js') }}"></script>
	
	<script src="{{ URL::asset('bower_components/gentelella/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
	
	<script src="{{ URL::asset('bower_components/gentelella/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
	
    <script src="{{ URL::asset('bower_components/gentelella/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/google-code-prettify/src/prettify.js') }}"></script>
    
	<script src="{{ URL::asset('js/jquery.isloading.min.js') }}"></script>

	<script src="{{ URL::asset('js/handlebars-v4.0.5.js') }}"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.min.js"></script>
	
	<script src="{{ URL::asset('js/chosen.jquery.min.js') }}"></script>
	
	<script src="{{ URL::asset('js/chosen.ajaxaddition.jquery.js') }}"></script>

	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.5.4/src/loadingoverlay.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.5.4/extras/loadingoverlay_progress/loadingoverlay_progress.min.js"></script>

	<script type="text/javascript">
		
		jQuery( document ).ready( function() {
		
			//INITIALIZE ALL
			init_all();

			//OPEN MODAL
			jQuery( document ).on( 'click', '.add_promo_code', function( e ) 
			{
				
				e.preventDefault();
				
				//OPEN MODAL
				jQuery('#promo_code_modal').modal( {
				
					backdrop: 'static',
					
					keyboard: true
				
				} );

			} );

			jQuery( document ).on( 

									'click'

									, 

									'.edit_promo_code'

									, 

									function( e ) 
									{
				
										e.preventDefault();

											var promo_code_id = $(this).data( 'promocodeid' );
				
											//EDIT PROMO CODE
											if( promo_code_id != undefined ) 
											{
												
												console.log( "edit has been clicked" );
												//SHOW LOADING
												$( "#promo_code_modal" ).isLoading({
													
													text: "Loading",
													
													position: "overlay",
													
													transparency: 0.5
													
												});
												
												var data = {

													action: 'iremit_state_base',

													state: 'get_promo_code_details',

													promo_code_id: promo_code_id

												};

												$.ajax(

														{

										                	type: "GET",

										                	url: "{{ url( '/codes/promos/data' ) }}",

										                	dataType: "json",

										                	data: data,

										                	success: function ( data, textStatus, jqXHR ) {


																console.log( 'get_recipient_list data' );

																console.log( data );

																append_promo_code_data( data );

																jQuery('#promo_code_modal').modal( {
				
																	backdrop: 'static',
					
																	keyboard: true
				
																} );

																$( "#promo_code_modal" ).isLoading('hide');

															},
										                
										                	error: function (data) {
										                    	
										                    	console.log(data.d);
										                	
										                	}

									                	}

           											);
												

				
											}

										}

									)

			;

			//OPEN MODAL DETAILS
			jQuery( document ).on( 'click', '.details_promo_code', function( e ) {
			
				alert( "This is under construction!" );
			
			} );

			//USAGE CLASSIFICATION
			jQuery( document ).on( 'change', 'select[name="usage_classification"]', function( e ) {
				
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				if( current_value == 'individual' ) {
					
					jQuery( '#user_classification_container' ).slideDown();
					
					jQuery( 'select[name="user_classification"]' ).prop( 'disabled', false ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="user_classification"]' ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'disabled', false );
					
					jQuery( 'select[name="usage_type"]' ).prop( 'disabled', false ).trigger( 'chosen:updated' );
					
					jQuery( '#user' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Remitters' ).attr( 'multiple', true );
					
					jQuery( '#user' ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( '#user_label' ).html( 'Remitters:' );
					
					jQuery( '#remitter_container' ).removeClass( 'col-lg-12' );
					
					jQuery( '#remitter_container' ).removeClass( 'col-md-12' );
					
					jQuery( '#remitter_container' ).addClass( 'col-lg-9' );
					
					jQuery( '#remitter_container' ).addClass( 'col-md-9' );
					
					jQuery( '#all_user_container' ).fadeIn();
					
					//CHECK IF USER CLASSIFICATION IS EXISTING OR BOTH
					if( jQuery( 'select[name="user_classification"]' ).val() == 'existing' || jQuery( 'input[name="user_classification_both"]' ).is( ':checked' ) ) {
						
						jQuery( '#user_container' ).slideDown();
						
					} else {
						
						jQuery( '#user_container' ).slideUp();
						
					}
				
				}
				
				else if( current_value == 'business' ) {
					
					jQuery( '#user_container' ).slideDown();
					
					jQuery( '#user_classification_container' ).slideDown();
					
					jQuery( 'select[name="usage_type"]' ).prop( 'disabled', true ).val( 'unli' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="user_classification"]' ).prop( 'disabled', true ).val( 'new' ).trigger( 'chosen:updated' );
					
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'disabled', true );
					
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'checked', false );

					jQuery( '#user' ).empty();
					
					jQuery( '#user' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Business' ).removeAttr( 'multiple' );
					
					jQuery( '#user' ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( '#user_label' ).html( 'Business:' );
					
					jQuery( '#all_user_container' ).hide();
					
					jQuery( '#remitter_container' ).removeClass( 'col-lg-9' );
					
					jQuery( '#remitter_container' ).removeClass( 'col-md-9' );
					
					jQuery( '#remitter_container' ).addClass( 'col-lg-12' );
					
					jQuery( '#remitter_container' ).addClass( 'col-md-12' );
					
					jQuery( '#user' ).prop( 'disabled', false ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( 'input[name="all_user"]' ).prop( 'checked', false );
					
					jQuery( '#promo_code_total_consumable_container' ).slideUp();
					
					jQuery( 'input[name="total_consumable"]' ).val( '' );
					
					jQuery( '#user_action_container' ).slideUp();
					
					jQuery( '#promo_code_recipient_container' ).slideUp();

					jQuery( '#user_action' ).val( 'assign' ).trigger( 'chosen:updated' );
					
					get_business_list();
					
				} 
				
				else {
					
					jQuery( '#user_container' ).slideUp();
					
					jQuery( '#user_classification_container' ).slideUp();
					
					jQuery( '#user_classification' ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="usage_type"]' ).prop( 'disabled', false ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="user_classification"]' ).prop( 'disabled', false ).trigger( 'chosen:updated' );
					
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'checked', false ).prop( 'disabled', false );
					
					jQuery( '#user_label' ).html( 'Remitters:' );
					
					jQuery( '#user' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Remitters' ).removeAttr( 'multiple' );
					
					jQuery( '#user' ).val( '' );
					
					jQuery( '#user' ).empty().trigger( 'chosen:updated' );
					
					jQuery( '#user_action_container' ).slideUp();
					
					jQuery( '#promo_code_recipient_container' ).slideUp();

				}
				
				init_chosen();
				
			} );

			//USER
			jQuery( document ).on( 'change', 'select[name="user[]"]', function( e ) { 
				
				e.preventDefault();
				
				//IF USAGE TYPE IS UNLI 100 GET RECIPIENT LIST
				if( jQuery( 'select[name="usage_type"]' ).val() == 'unli100' ) {
					
					var remitter_id = jQuery( this ).val();

					get_recipient_list( remitter_id, null );

				} 
				
			} );

			//ALL USER
			jQuery( document ).on( 'click', '#all_user', function( e ) {
				
				if( jQuery( this ).is( ':checked' ) ) {

					jQuery( '#user' ).prop( 'disabled', true ).val( '' ).trigger( 'chosen:updated' );

				} else {
					
					jQuery( '#user' ).prop( 'disabled', false ).val( '' ).trigger( 'chosen:updated' );
					
				}
				
			} );

			//USER CLASSIFICATION BOTH
			jQuery( document ).on( 'click', 'input[name="user_classification_both"]:checkbox', function( e ) { 

				if( jQuery( this ).is( ':checked' ) ) {

					jQuery( 'select[name="user_classification"]' ).val( '' ).prop( 'disabled', true ).trigger( 'chosen:updated' );
					
					jQuery( '#user_container' ).slideDown();
					
					//HIDE USER ACTION CONTAINER
					if( jQuery( '#user_action_container' ).is( ':visible' ) ) {
						
						jQuery( '#user_action_container' ).slideUp();
					
					}

					get_remitter_list();

				} else {
					
					jQuery( 'select[name="user_classification"]' ).prop( 'disabled', false ).trigger( 'chosen:updated' );
					
					jQuery( '#user_container' ).slideUp();
					
					jQuery( '#user' ).empty().trigger( 'chosen:updated' );
					
				}
			
			} );		

			//USAGE TYPE
			jQuery( document ).on( 'change', 'select[name="usage_type"]', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				if( current_value == 'consumable' ) {
					
					
					jQuery( '#promo_code_total_consumable_container' ).slideDown();
					
					jQuery( '#promo_code_recipient_container' ).slideUp();
					
					
					//SHOW REMITTER SELECTION
					if( jQuery( 'select[name="user_classification"]' ).val() == 'existing' || jQuery( 'input[name="user_classification_both"]:checked' ).length ) {
						
						jQuery( '#user_container' ).slideDown();
						
					}
					
					else {
						
						jQuery( '#user_container' ).slideUp();
						
					}
					
					
					jQuery( '#user' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Remitters' ).attr( 'multiple', true );
					
					jQuery( '#user' ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( '#user_label' ).html( 'Remitters:' );
					
					
					jQuery( '#recipient' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Recipients' ).attr( 'multiple', true );
					
					jQuery( '#recipient' ).empty().val( '' ).trigger( 'chosen:updated' );
					
					
					jQuery( '#remitter_container' ).removeClass( 'col-lg-12' );
					
					jQuery( '#remitter_container' ).removeClass( 'col-md-12' );
					
					jQuery( '#remitter_container' ).addClass( 'col-lg-9' );
					
					jQuery( '#remitter_container' ).addClass( 'col-md-9' );
					
					jQuery( '#all_user_container' ).fadeIn();
					
					
					
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'disabled', false );
					
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'checked', false );
					
					jQuery( 'input[name="all_user"]:checkbox' ).prop( 'disabled', false );
					
					jQuery( 'input[name="all_user"]:checkbox' ).prop( 'checked', false );
					
					jQuery( 'select[name="user_classification"]' ).prop( 'disabled', false ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="user_action"]' ).prop( 'disabled', false ).val( 'assign' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="usage_classification"]' ).prop( 'disabled', false ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="user[]"]' ).prop( 'disabled', false );
					
					
					
					
					jQuery( '#user_container' ).slideUp();
					
					jQuery( '#user_action_container' ).slideUp();
					
					jQuery( '#user_classification_container' ).slideUp();
					
					
					
				} 
				
				else if( current_value == 'unli100' ) {
					
					jQuery( '#promo_code_recipient_container' ).slideDown();
					
					jQuery( '#promo_code_recipient_container' ).slideDown();
					
					jQuery( '#promo_code_total_consumable_container' ).slideUp();
					
					
					jQuery( '#user_container' ).slideDown();
					
					jQuery( '#user' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Remitter' ).removeAttr( 'multiple' );
					
					jQuery( '#user' ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( '#user_label' ).html( 'Remitter:' );
					
					
					jQuery( '#recipient' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Recipients' ).attr( 'multiple', true );
					
					jQuery( '#recipient' ).empty().val( '' ).trigger( 'chosen:updated' );
					
					
					jQuery( '#remitter_container' ).removeClass( 'col-lg-9' );
					
					jQuery( '#remitter_container' ).removeClass( 'col-md-9' );
					
					jQuery( '#remitter_container' ).addClass( 'col-lg-12' );
					
					jQuery( '#remitter_container' ).addClass( 'col-md-12' );
					
					jQuery( '#all_user_container' ).hide();
					
				
				
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'disabled', true );
					
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'checked', false );
					
					jQuery( 'input[name="all_user"]:checkbox' ).prop( 'disabled', false );
					
					jQuery( 'input[name="all_user"]:checkbox' ).prop( 'checked', false );
					
					jQuery( 'select[name="user_classification"]' ).prop( 'disabled', true ).val( 'existing' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="user_action"]' ).prop( 'disabled', true ).val( 'assign' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="usage_classification"]' ).prop( 'disabled', true ).val( 'individual' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="user[]"]' ).prop( 'disabled', false );
					
				
				
					jQuery( '#user_container' ).slideDown();
					
					jQuery( '#user_action_container' ).slideDown();
					
					jQuery( '#user_classification_container' ).slideDown();

					get_remitter_list();
					
					
					
				} 
				
				else {
					
					jQuery( '#remitter_container label' ).html( 'Remitters' );
					
					jQuery( '#promo_code_total_consumable_container' ).slideUp();
					
					jQuery( '#promo_code_recipient_container' ).slideUp();
					
					jQuery( 'input[name="total_consumable"]' ).val( '' );


					//SHOW REMITTER SELECTION
					if( jQuery( 'select[name="user_classification"]' ).val() == 'existing' || jQuery( 'input[name="user_classification_both"]:checked' ).length ) {
						
						jQuery( '#user_container' ).slideDown();
						
					}
					
					else {
						
						jQuery( '#user_container' ).slideUp();
						
					}


					jQuery( '#user' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Remitters' ).attr( 'multiple', true );
					
					jQuery( '#user' ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( '#user_label' ).html( 'Remitters:' );
					
					
					jQuery( '#recipient' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Recipients' ).attr( 'multiple', true );
					
					jQuery( '#recipient' ).empty().val( '' ).trigger( 'chosen:updated' );
					
					
					jQuery( '#remitter_container' ).removeClass( 'col-lg-12' );
					
					jQuery( '#remitter_container' ).removeClass( 'col-md-12' );
					
					jQuery( '#remitter_container' ).addClass( 'col-lg-9' );
					
					jQuery( '#remitter_container' ).addClass( 'col-md-9' );
					
					jQuery( '#all_user_container' ).fadeIn();
					
					
					
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'disabled', false );
					
					jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'checked', false );
					
					jQuery( 'input[name="all_user"]:checkbox' ).prop( 'disabled', false );
					
					jQuery( 'input[name="all_user"]:checkbox' ).prop( 'checked', false );
					
					jQuery( 'select[name="user_classification"]' ).prop( 'disabled', false ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="user_action"]' ).prop( 'disabled', false ).val( 'assign' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="usage_classification"]' ).prop( 'disabled', false ).val( '' ).trigger( 'chosen:updated' );
					
					jQuery( 'select[name="user[]"]' ).prop( 'disabled', false );
					
					
					
					
					jQuery( '#user_container' ).slideUp();
					
					jQuery( '#user_action_container' ).slideUp();
					
					jQuery( '#user_classification_container' ).slideUp();
					
					
					
				}
				
				init_chosen();
			
			} );

			//USER CLASSIFICATION
			jQuery( document ).on( 'change', 'select[name="user_classification"]', function( e ) {

				//INDIVIDUAL
				if( jQuery( 'select[name="usage_classification"]' ).val() == 'individual' ) {

					//CHECK IF USER CLASSIFICATION IS EXISTING OR BOTH
					if( jQuery( this ).val() == 'existing' ) {

						jQuery( '#user_container' ).slideDown();
						
						jQuery( '#user_action_container' ).slideDown();

						get_remitter_list();

					} 
					
					else {
						
						jQuery( '#user_container' ).slideUp();
						
						jQuery( '#user_action_container' ).slideUp();
						
						jQuery( '#user' ).empty().trigger( 'chosen:updated' );
						
					}
				
				}

			} );

			//VALIDATE FORM THEN AJAX SUBMIT
			jQuery( document ).on( 'click', '#promo_code_btn', function( e ) {
			
				var usage_classification = jQuery( 'select[name="usage_classification"]' ).val();
				
				var usage_type = jQuery( 'select[name="usage_type"]' ).val();
				
				var user = jQuery( '#user' ).val();
				
				var user_classification = jQuery( 'select[name="user_classification"]' ).val();
				
				var user_classification_both = jQuery( 'input[name="user_classification_both"]:checked' ).length;
				
				var total_consumable = jQuery( 'input[name="total_consumable"]' ).val();
				
				var user_action = jQuery( 'select[name="user_action"]' ).val();
				
				var all_user = jQuery( 'input[name="all_user"]:checked' ).length;
				
				var duration = jQuery( 'input[name="modal_duration"]' ).val();
				
				var expiration_date = jQuery( 'input[name="expiration_date"]' ).val();
				
				var promo_code = jQuery( 'input[name="promo_code"]' ).val();
				
				var discounted_value = jQuery( 'input[name="discounted_value"]' ).val();
				
				var description = jQuery( 'textarea[name="description"]' ).val();
				
				/*
				* VALIDATE
				*/
				
				var valid = true;
				
				var error_message = '';
				
				var field = '';
				
				//USAGE CLASSIFICATION
				if( usage_classification == '' ) {
					
					valid = false;
					
					error_message += '\xb7 Usage classification \n';
					
					field = 'usage_classification';
					
				}
				
				//INDIVIDUAL
				if( usage_classification == 'individual' ) {
					
					//CHECK IF USER CLASSIFICATION IS EMPTY
					if( user_classification == '' ) {
						
						//CHECK IF USER CLASSIFICATION BOTH IS EMPTY
						if( user_classification_both == 0 ) {
							
								valid = false;
								
								error_message += '\xb7 User classification \n';
								
								field = 'user_classification';
							
						}
						
					}
					
					//CHECK IF USER CLASSIFICATION IS NOT EMPTY OR USER CLASSIFICATION BOTH IS NOT EMPTY
					if( ( user_classification != '' && user_classification != 'new' ) || user_classification_both != 0 ) {
						
						//CHECK IF ALL USER IS SELECTED
						if( all_user == 0 ) {
							
							//CHECK IF SELECT ATLEAST HAVE ONE SELECTED
							if( jQuery( '#user option:selected' ).length == 0 ) {
								
								valid = false;
								
								error_message += '\xb7 Remitters \n';
								
								field = 'user';
								
							} 
							
						}
						
					}
					
					//CHECK IF USER CLASSIFICATION IS EXISTING
					if( user_classification == 'existing' ) {
						
						//CHECK IF USER ACTION IS NOT EMPTY
						if( user_action == '' ) {
							
							valid = false;
							
							error_message += '\xb7 User action \n';
							
							field = 'user_action';
						
						}
						
					}
					
				}

				//BUSINESS
				if( usage_classification == 'business' ) {
					
					if( user == '' || user == null ) {
						
						valid = false;
						
						error_message += '\xb7 Business \n';
						
						field = 'user';
						
					}
					
				}
				
				//USAGE TYPE
				if( usage_type == '' ) {
					
					valid = false;
					
					error_message += '\xb7 Usage type \n';
					
					field = 'usage_type';
					
				}
				
				//USAGE TYPE - CONSUMABLE
				if( usage_type == 'consumable' ) {

					//CONSUMABLE EMPTY
					if( total_consumable == '' ) {
						
						valid = false;
						
						error_message += '\xb7 Total consumable \n';
						
						field = 'total_consumable';

					} 

					//CONSUMABLE NOT NUMERIC
					else if( ! jQuery.isNumeric( total_consumable ) ) {
						
						valid = false;
						
						error_message += '\xb7 Total consumable - number only \n';
						
						field = 'total_consumable';
					
					} 

					//CONSUMABLE = 0
					else if( total_consumable == 0 ) {  
						
						valid = false;
						
						error_message += '\xb7 Total consumable \n';
						
						field = 'total_consumable';
						
					}
					
				}
				
				//USAGE TYPE - UNLI100
				if( usage_type == 'unli100' ) {
					
					//REMITTER IS EMPTY
					if( user == '' ) {
						
						valid = false;
						
						error_message += '\xb7 Remitter \n';
						
						field = 'user';
						
					}
					
					//REMITTER IS NOT EMPTY
					else {

						var recipient = jQuery( '#recipient' ).val();

						//CHECK RECIPIENT
						if( recipient == '' || recipient == null ) {
							
							valid = false;
							
							error_message += '\xb7 Recipient \n';
							
							field = 'recipient';
							
						}
						
					}
					
				}
				
				//DURATION
				if( duration == '' ) {
					
					valid = false;
					
					error_message += '\xb7 Duration \n';
					
					field = 'duration';
					
				} 
				
				//DURATION NOT EMPTY
				else {
					
					//DURATION VALIDATE IN NUMBER
					if( ! jQuery.isNumeric( duration ) ) {
						
						valid = false;
						
						error_message += '\xb7 Duration - numbers only \n';
						
						field = 'duration';
						
					}
					
					//DURATION VALIDATE NOT EQUAL TO 0
					else if( duration == 0 ) {
						
						valid = false;
						
						error_message += '\xb7 Duration must not equal to 0 \n';
						
						field = 'duration';
						
					}
					
				}
				
				//NAME
				if( promo_code == '' ) {
					
					valid = false;
					
					error_message += '\xb7 Name \n';
					
					field = 'promo_code';
					
				}
				
				//DISCOUNTED VALUE
				if( discounted_value == '' ) {
					
					valid = false;
					
					error_message += '\xb7 Discounted value \n';
					
					field = 'discounted_value';
					
				}
				
				//DISCOUNT VALUE NOT EMPTY
				if( discounted_value != '' ) {
					
					//DISCOUNT VALUE IS NOT A NUMBER
					if( ! jQuery.isNumeric( discounted_value ) ) {
						
						valid = false;
						
						error_message += '\xb7 Discounted value - number only \n';
						
						field = 'discounted_value';
						
					}
					
				}
				
				//DESCRIPTION
				if( description == '' ) {
					
					valid = false;
					
					error_message += '\xb7 Description \n';
					
					field = 'description';
					
				}
				
				//CHECK IF THERE ARE ERROR MESSAGE
				if( error_message != '' ) {
					
					e.preventDefault();
					
					var new_error_message = 'Please fill up the following! \n\n';
					
					new_error_message += error_message;
					
					//ALERT ERROR MESSAGE
					alert( new_error_message );

					jQuery( '#' + field ).focus();
					
					jQuery( '#' + field ).trigger( 'chosen:open' );
				
				} 
				
				else {
					
					e.preventDefault();
					
					//SHOW LOADING
					jQuery( "#promo_code_modal" ).isLoading({
						
						text: "Loading",
						
						position: "overlay",
						
						transparency: 0.5
						
					});
					
					var promo_code_id = jQuery( '#promo_code_id' ).val();

					var data = jQuery( '.promocode' ).serializeArray();
					
					data.push( { name: 'action', value: 'iremit_state_base' } );

					if( ( typeof edit_promo_code != 'undefined' ) && edit_promo_code ) 
					{

   						data.push( { name: 'state', value: 'edit_promo_code' } );

   					}

   					else
   					{	

						data.push( { name: 'state', value: 'save_promo_code' } );
					
					}

					//CHECK IF PROMO CODE CLASSFICATION IS BUSINESS
					if( usage_classification == 'business' ) {

						//APPEND DISABLED VALUE
						data.push( { name: 'usage_type', value: 'unli' } );
						
						data.push( { name: 'user_classification', value: 'new' } );
						
					
					}

					data.push( { name: 'id', value: promo_code_id } );

					data.push( 

								{ 

									name : '_token' ,

									value :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' )

								}

							);
					
					$( '#promo_code_modal' ).isLoading(

														{
											
															text : "Loading",
											
															position : "overlay",
											
															transparency : 0.5,
											
														}

													)

					$.ajax(
						
							{

			                	type: "POST",

			                	url: "{{ url( 'codes/promos/form/post' ) }}",

			                	dataType: "json",

			                	data: data,

			                	success: function ( data, textStatus, jqXHR ) {

			                		console.log( data );

			                		$( "#promo_code_modal" ).isLoading('hide');

									var message = '';
							
									//CHECK IF RESPONSE RESULT NOT EQUAL TO 0
									if( data.result != 0 ) {
								
										//CHECK IF PROMO CODE ID IS EMPTY OR 0
										if( promo_code_id == '' || promo_code_id == 0 ) {
									
											message = 'Promo code has been saved!';
										
										} 

										else 
										{
									
											message = 'Promo code has been updated!';
									
										}
								
									}
							
									//ALERT MESSAGE
									alert( message );
							
									//REDIRECT TO PROMO CODE
									window.location.href = "{{ url( 'codes/promos' ) }}";

								},
			                
			                	error: function (data) {
							
									var error_message = '';
							
									jQuery.each( data.error_message, function( i, v ) {
								
										error_message += v + '\n';
								
									});
							
									alert( error_message );
							
									//HIDE PROMO CODE MODAL
									jQuery( "#promo_code_modal" ).isLoading( 'hide' );
							
								}

		                	}

		           	);
					
				}
			
			} );

			//DELETE PROMO CODE
			jQuery( document ).on( 'click', '.delete_promo_code', function( e ) {
				
				var answer = confirm( "Do you want to delete this promo code?" );
				
				//IF YES DELETE PROMO CODE
				if( answer ) {
				
					var promo_code_id = this.id;

					//EDIT PROMO CODE
					if( promo_code_id != undefined ) {
						
						//SHOW LOADING
						jQuery( "body" ).isLoading({
							
							text: "Loading",
							
							position: "overlay",
							
							transparency: 0.5
							
						});
						
						var data = {

							action: 'iremit_state_base',

							_token :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

							state: 'delete_promo_code',

							promo_code_id: promo_code_id

						};
						
						//CALL FOR PROMO CODE DETAILS
						jQuery.post( "{{ url( 'codes/promos/form/post' ) }}", data , function( response ) {

							if( response.success ) {

								jQuery( "body" ).isLoading( 'hide' );
								
								//SHOW SOME NOTIFICATION
								alert( "Promo code has been deleted!" );
								
								//RELOAD PAGE
								window.location.reload();
							
							}
							
						},'json');
					
					}
				
				}
				
			} );

			//MODAL ON CLOSE
			jQuery( document ).on( 'hidden.bs.modal', '#promo_code_modal',function( e ) {
			
				e.preventDefault();
				
				edit_promo_code = false;
				
				append = true;
				
				//RESET PROMO CODE ID
				jQuery( '#promo_code_id' ).val( 0 );
				
				//RESET ALL FORMS
				jQuery( ".promocode" ).trigger( "reset" );
				
				//RESET JQUERY FORM VALIDATION
				jQuery( ".promocode" ).validate().resetForm();
				
				//REMOVE CLASS ERROR TO ALL FORM CONTROL
				jQuery( '.form-control' ).removeClass( 'error' );
				
				//RESET ALL CHOSEN
				jQuery( '.chosen-select-width' ).val( '' ).trigger( 'chosen:updated' ).prop( 'disabled', false );
				
				//REMOVE DISABLE 
				jQuery( 'select[name="usage_classification"]' ).prop( 'disabled', false ).val( '' ).trigger( 'chosen:updated' );	
				
				//REMOVE DISABLE 
				jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'disabled', false );

				//SET ASSIGN TO USER ACTION
				jQuery( '#user_action' ).val( 'assign' ).trigger( 'chosen:updated' );
				
				//HIDE TOTAL CONSUMABLE CONTAINER
				jQuery( '#promo_code_total_consumable_container' ).slideUp();
				
				//HIDE USER ACTION CONTAINER
				jQuery( '#user_action_container' ).slideUp();
				
				//HIDE USER CONTAINER
				jQuery( '#user_container' ).slideUp();
				
				//HIDE USER CLASSIFICATION CONTAINER
				jQuery( '#user_classification_container' ).slideUp();
				
				//HIDE USER RECIPIENT CONTAINER
				jQuery( '#promo_code_recipient_container' ).slideUp();
				
				//CHANGE USER LABEL TO REMITTER
				jQuery( '#user_label' ).html( 'Business:' );
				
				//RESET USER PLACEHOLDER TO REMITTER AND REMOVE MULTIPLE ATTRIBUTES
				jQuery( '#user' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Business' ).removeAttr( 'multiple' );
				
				//RESET USER VALUE TO EMPTY
				jQuery( '#user' ).val( '' ).trigger( 'chosen:updated' );
				
				//RESET RECIPIENT
				jQuery( '#recipient' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Recipients' ).attr( 'multiple', true );
				
				//EMPTY RECIPIENT
				jQuery( '#recipient' ).empty().val( '' ).trigger( 'chosen:updated' );  
				
				//DESTROY ALL DATEPICKERS
				jQuery( "#duration_start_date" ).datepicker( "destroy" );
				
				jQuery( "#duration_end_date" ).datepicker( "destroy" );
				
				jQuery( "#expiration_date" ).datepicker( "destroy" );

				//REINTIALIZE ALL
				init_all();
			
			} );
			
		} );
		
		//INIT ALL
		function init_all(){
			
			init_chosen();
			
			init_datepicker();
			
		}
		
		//INIT CHOSEN
		function init_chosen() {
			
			var config = {
				
			  '.chosen-select'           : {},
			  
			  '.chosen-select-deselect'  : { allow_single_deselect : true },
			  
			  '.chosen-select-no-single' : { disable_search_threshold : 10 },
			  
			  '.chosen-select-no-results': { no_results_text : 'Oops, nothing found!' },
			  
			  '.chosen-select-width'     : { width : "100%", allow_single_deselect : true },
			  
			  '.chosen-recipient'		 : { width : "100%", allow_single_deselect : true, max_selected_options: 2 }
			  
			}
			
			for ( var selector in config ) {
				
			  jQuery( selector ).chosen( config[ selector ] );
			  
			}
			
		}
		
		//INIT DATEPICKER
		function init_datepicker() {
			
			//INITIALIZE DATEPICKER
			jQuery('.datepicker').daterangepicker( {
				
				singleDatePicker: true,
				
				showDropdowns: true,
				
				format: 'YYYY-MM-DD'
				
			} );
		
		}

		//GET BUSINESS LIST AND UPDATE BUSINESS DROPDOWN
		function get_business_list() {
			
			jQuery( '#user' ).ajaxChosen
			( 
				{
				
					dataType: 'json',
					
					type: 'POST',
					
					url:"{{ url( 'codes/promos/form/post' ) }}",
					
					data: 
					{ 

						'remitter' : jQuery( '#user' ).val(), 

						'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

						'state' : 'get_business_list'

					},
					
					success: function( data, textStatus, jqXHR ) 
					{

						return data;
						
					}
				
				}, 

				{
					
					processItems: function( data ) 
					{ 					
						
						var business_referrals = {};
						
						jQuery.each( data.results, function ( i, val ) {
							
							business_referrals[ val.id ] = val.name;
							
						} );
						
						console.log( business_referrals );
						
						return business_referrals; 
						
					},
					
					useAjax: function( e ) 
					{ 

						return true; 

					},
					
					generateUrl: function( q ) 
					{ 

						return "{{ url( 'codes/promos/form/post' ) }}"; 

					},
					
					loadingImg: "{{ URL::asset( 'img/loading.gif' ) }}",
					
					minLength: 2
					
				} 

			);
			
		}
		
		//GET REMITTER LIST AND UPDATE REMITTER DROPDOWN
		function get_remitter_list() {
			
			jQuery( '#user' ).ajaxChosen
			( 
				{
				
					dataType: 'json',
					
					type: 'POST',
					
					url:"{{ url( 'iremit/recipients/get-remitter/' ) }}",
					
					data: 
					{ 

						'remitter' : jQuery( '#user' ).val(), 

						'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ) 

					},
					
					success: function( data, textStatus, jqXHR ) 
					{
						
						return data;
						
					}
				
				}, 

				{
					
					processItems: function( data ) 
					{ 					
						
						var remitters = {};
						
						jQuery.each( data.results, function ( i, val ) {
							
							remitters[ val.id ] = val.text;
							
						} );
						
						console.log( remitters );
						
						return remitters; 
						
					},
					
					useAjax: function( e ) 
					{ 

						return true; 

					},
					
					generateUrl: function( q ) 
					{ 

						return "{{ url( 'iremit/recipients/get-remitter/' ) }}"; 

					},
					
					loadingImg: "{{ URL::asset( 'img/loading.gif' ) }}",
					
					minLength: 2
					
				} 

			);	
			
		}
		
		//GET RECIPIENT LIST AND UPDATE RECIPIENT DROPDOWN
		function get_recipient_list( remitter_id, selectedRecipientIds ) 
		{

			$( '#promo_code_modal' ).isLoading(

									{
						
										text : "Loading",
						
										position : "overlay",
						
										transparency : 0.5,
						
									}

								);

			$data =	{ 

						'remitter' : jQuery( '#user' ).val(), 

						'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

						'state' : 'get_recipient_list',

						'remitter_id' : remitter_id

					};	

			$.ajax(

					{

	                	type: "POST",

	                	url: "{{ url( 'codes/promos/form/post' ) }}",

	                	dataType: "json",

	                	data: $data,

	                	success: function ( data, textStatus, jqXHR ) {

	                		if( data.success )
	                		{

		                		$( "#promo_code_modal" ).isLoading('hide');

								var recipient_list_option = '<option value=""></option>';

								var recipients = data.results;

								console.log( selectedRecipientIds );

								jQuery.each( data.results

											 ,

											 function( index, value ) 
											 { 
											 /**
										 	  * Convert recipientIds to array
										 	  * @type array
										 	  */
									 		 // selectedRecipientsIds = ( Array.isArray( selectedRecipientIds ) && selectedRecipientIds == undefined ) ? selectedRecipientsIds : Array.from( selectedRecipientsIds );
									 	
									 			console.log( index );

									 			console.log( $.inArray( index, selectedRecipientIds ) );

									 			var selected = $.inArray( index, selectedRecipientIds ) > -1 ? 'selected' : '';
									
												recipient_list_option += '<option value="'+index+'" ' + selected + '>'+value+'</option>';
									
											 } 

										   )

								;
									
								jQuery( '#recipient' ).empty().append( recipient_list_option ).trigger( 'chosen:updated' );

							}

						},
	                
	                	error: function (data) {
	                    	
	                    	console.log( data );
	                	
	                	}

                	}

           	);

		}	
		
		//APPEND PROMO CODE DATA
		function append_promo_code_data( data ) 
		{

			console.log( data );

			var users = data.promoCodeUsers;

			var recipients = data.promoCodeRecipients; 

			jQuery( '#promo_code_id' ).val( data.id );

			edit_promo_code = true;
			
			//APPEND RECORDS
			
			jQuery( 'input[name="promo_code_id"]' ).val( data.id );
			
			jQuery( 'select[name="usage_classification"]' ).val( data.classification ).trigger( 'chosen:updated' ).change();

			jQuery( 'select[name="usage_type"]' ).val( data.type ).trigger( 'chosen:updated' );
			
			jQuery( 'input[name="total_consumable"]' ).val( data.total_consumable );
			
			jQuery( 'select[name="user_action"]' ).val( data.user_action ).trigger( 'chosen:updated' );
			
			if( data.user_classification == 'both' ) {
				
				jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'checked', true );
				
				//RESET USER CLASSIFICATION
				jQuery( 'select[name="user_classification"]' ).val( '' ).prop( 'disabled', true ).trigger( 'chosen:updated' ).change();
				
				get_remitter_list();

			}
			
			else {
				
				jQuery( 'select[name="user_classification"]' ).val( data.user_classification ).trigger( 'chosen:updated' ).change();
				
				//RESET USER CLASSIFICATION BOTH
				jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'checked', false );
				
			}
			
			jQuery( 'input[name="modal_duration"]' ).val( data.duration );
			
			jQuery( 'input[name="expiration_date"]' ).val( data.expiration_date );

			jQuery( 'input[name="promo_code"]' ).val( data.name );
			
			jQuery( 'input[name="discounted_value"]' ).val( data.value );
			
			jQuery( 'textarea[name="description"]' ).val( data.description );
			
			//NOT UNLI100
			if( data.type != 'unli100' ) 
			{
			
				//TYPE = CONSUMABLE
				if( data.type == 'consumable' ) {
					
					jQuery( '#promo_code_total_consumable_container' ).show();
					
				}
				
				//ALWAYS SHOW
				jQuery( '#user_classification_container' ).show();
				
				//SHOW USER CONTAINER IF USER CLASSIFICATION == NEW AND CLASSIFICATION == BUSINESS OR USER CLASSIFICATION NOT EQUAL TO NEW AND CLASSIFICATION IS EQUAL TO INDIVIDUAL
				if( ( data.user_classification == 'new' && data.classification == 'business' ) || ( data.user_classification != 'new' && data.classification == 'individual' ) ) {
					
					jQuery( '#user_container' ).show();
				
				}
				
			}
			
			//UNLI100
			else {
				
				//DISPLAY ALL OF THIS
				jQuery( '#promo_code_recipient_container' ).slideDown();
				
				jQuery( '#promo_code_recipient_container' ).slideDown();
				
				jQuery( '#promo_code_total_consumable_container' ).slideUp();
				
				
				jQuery( '#user_container' ).slideDown();
				
				jQuery( '#user' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Remitter' ).removeAttr( 'multiple' );
				
				jQuery( '#user' ).val( '' ).trigger( 'chosen:updated' );
				
				jQuery( '#user_label' ).html( 'Remitter:' );
				
				
				jQuery( '#recipient' ).chosen( 'destroy' ).attr( 'data-placeholder', 'Recipients' ).attr( 'multiple', true );
				
				jQuery( '#recipient' ).empty().val( '' ).trigger( 'chosen:updated' );
				
				
				jQuery( '#remitter_container' ).removeClass( 'col-lg-9' );
				
				jQuery( '#remitter_container' ).removeClass( 'col-md-9' );
				
				jQuery( '#remitter_container' ).addClass( 'col-lg-12' );
				
				jQuery( '#remitter_container' ).addClass( 'col-md-12' );
				
				jQuery( '#all_user_container' ).hide();
				
			
			
				jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'disabled', true );
				
				jQuery( 'input[name="user_classification_both"]:checkbox' ).prop( 'checked', false );
				
				jQuery( 'input[name="all_user"]:checkbox' ).prop( 'disabled', false );
				
				jQuery( 'input[name="all_user"]:checkbox' ).prop( 'checked', false );
				
				jQuery( 'select[name="user_classification"]' ).prop( 'disabled', true ).val( 'existing' ).trigger( 'chosen:updated' );
				
				jQuery( 'select[name="user_action"]' ).prop( 'disabled', true ).val( 'assign' ).trigger( 'chosen:updated' );
				
				jQuery( 'select[name="usage_classification"]' ).prop( 'disabled', true ).val( 'individual' ).trigger( 'chosen:updated' );
				
				jQuery( 'select[name="user[]"]' ).prop( 'disabled', false );							
			
				jQuery( '#user_container' ).slideDown();
				
				jQuery( '#user_action_container' ).slideDown();
				
				jQuery( '#user_classification_container' ).slideDown();

			}

			/*
			 * Get all values of column all_user
			 */
			var users_arr = ( users != '' && users.length > 0 ) ? users.map( function ( n ) { return n.all_user != undefined ? n.all_user : ''; } ) : [];

			//Check if all_user column has been flagged
			if( users_arr.includes( 1 ) ) //1 value means "All" checkbox has been checked
			{

				//CHECK ALL USER CHECKBOX
				jQuery( 'input[name="all_user"]:checkbox' ).prop( 'checked', true );
				
				//DISABLE USER DROPDOWN
				jQuery( '#user' ).prop( 'disabled', true ).val( '' ).trigger( 'chosen:updated' );

			}
					
			else if( users.length ) //CHECK IF USER HAS VALUE
			{

				var selectedRecipientIds = !$.isEmptyObject( recipients ) ? Object.keys( recipients ) : [];

				var selectedRemitterIds = ( users != '' ) ? users.map( function ( n ) { return n.id != undefined ? n.id : ''; } ) : [];

				/**
				 * Update user select options
				 */
			 	for( i = 0; i < users.length; i++ )
				{

					var value = users[i].id;

					var email = users[i].email;

					var fullname = users[i].firstname

								   +


								   ' '

								   +

								   users[i].lastname

					;

					var text = fullname

							   +


							   ' | '

							   +

							   email

					;

					//APPEND VALUE IN SELECT
					$('#user').append('<option value="' + value + '" selected="selected">' + text  + '</option>')

							  .trigger( 'chosen:updated' );

				}

				/**
				 * Update recipient select option
				 */
				get_recipient_list( selectedRemitterIds, selectedRecipientIds );

			}

			init_chosen();

		}

	</script>

@endsection
/
