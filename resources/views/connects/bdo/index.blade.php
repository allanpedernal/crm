@extends('layouts.iremit')

@section('content')

<div class="page-title">
	<div class="title_left">		
		<h3> <i class="fa fa-mobile fa-fw"></i> BDO BRS </h3>
	</div>
</div>

<div class="clearfix"></div>

<div class="row">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">          
		<div class="x_panel">
	    	<div class="x_title">
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
		            <ul class="dropdown-menu" role="menu">
		              <li><a href="#">Settings 1</a>
		              </li>
		              <li><a href="#">Settings 2</a>
		              </li>
		            </ul>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
	        </div>
         	
         	<div class="x_content">
         		{{ $remittances->links() }}
	            <table class="table table-striped">
	            	<thead>
	            		<tr>
	            			<th>Transaction No</th>
	            			<th>Remitter Name</th>
	            			<th>Remitter Email</th>
	            			<th>Amount</th>
	            			<th>Status</th>
	            			<th>Action</th>
	            		</tr>
	            	</thead>
	            	<tbody>
	            		@if (count($remittances) > 0)
	            		@foreach ($remittances as $remittance)
	            		<tr>
	            			<td>{{ $remittance->transaction_number }}</td>
	            			<td>{{ $remittance->remitter->full_name }}</td>
	            			<td>{{ $remittance->remitter->email }}</td>
	            			<td>{{ $remittance->amount_sent }}</td>
	            			<td>{{ $remittance->statuse_id }}</td>
	            			<td></td>
	            		</tr>
	            		@endforeach
	            		@else
	            		<tr>
	            			<td colspan="7">EMPTY</td>
	            		</tr>
	            		@endif
	            	</tbody>
	            </table>

          	</div>
        </div>
    </div>
</div>
@endsection