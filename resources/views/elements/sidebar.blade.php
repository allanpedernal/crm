<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

	<div class="menu_section">

		<h3> My Menu </h3>

		<ul class="nav side-menu">

			<li class="{{ Request::is(url('/dashboard')) ? 'active' : '' }}"> <a href="{{ Request::is(url('/dashboard')) ? '#' : url('/dashboard') }}"> <i class="fa fa-dashboard fa-fw"></i> Dashboard </a> </li>

			<li> 
				
				<a href="javascript:void(0);"> <i class="fa fa-globe fa-fw"></i> iRemit <span class="fa fa-chevron-down"></span> </a> 
				
				<ul class="nav child_menu">
					
					<li class="{{ Request::is(url('/iremit/remitters')) ? 'active' : '' }}"> <a href="{{ Request::is(url('/iremit/remitters')) ? '#' : url('/iremit/remitters') }}"> Remitters </a> </li>

					<li class="{{ Request::is(url('/iremit/recipients')) ? 'active' : '' }}"> <a href="{{ Request::is(url('/iremit/recipients')) ? '#' : url('/iremit/recipients') }}"> Recipients </a> </li>

					<li class="{{ Request::is(url('/iremit/remittances')) ? 'active' : '' }}"> <a href="{{ Request::is(url('/iremit/remittances')) ? '#' : url('/iremit/remittances') }}"> Remittances </a> </li>

				</ul>
				
			</li>

			<li> 
				
				<a href="javascript:void(0);"> <i class="fa fa-code fa-fw"></i> Codes <span class="fa fa-chevron-down"></span> </a> 
				
				<ul class="nav child_menu">
					
					<li class="{{ Request::is(url('/codes/promos')) ? 'active' : '' }}"> <a href="{{ Request::is(url('/codes/promos')) ? '#' : url('/codes/promos') }}"> Promos </a> </li>
					
					<li class="{{ Request::is(url('/codes/sms')) ? 'active' : '' }}"> <a href="{{ Request::is(url('/codes/sms')) ? '#' : url('/codes/sms') }}"> SMS </a> </li>
				
				</ul>
				
			</li>

			<li> 
				
				<a href="javascript:void(0);"> <i class="fa fa-mobile fa-fw"></i> Mobile <span class="fa fa-chevron-down"></span> </a> 
				
				<ul class="nav child_menu">
					
					<li class="{{ Request::is(url('/mobile/promotions')) ? 'active' : '' }}"> <a href="{{ Request::is(url('/mobile/promotions')) ? '#' : url('/mobile/promotions') }}"> Promotions </a> </li>

				</ul>
							
			</li>
			
			<li> 
				
				<a href="javascript:void(0);"> <i class="fa fa-cog fa-fw"></i> Settings <span class="fa fa-chevron-down"></span> </a> 
				
				<ul class="nav child_menu">
					
					<li class="{{ Request::is(url('/settings/rates-and-fees')) ? 'active' : '' }}"> <a href="{{ Request::is(url('/settings/rates-and-fees')) ? '#' : url('/settings/rates-and-fees') }}"> Rate &amp; Fees </a> </li>

				</ul>
				
			</li>
			
		</ul>

	</div>

</div>
