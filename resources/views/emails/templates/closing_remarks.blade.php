@extends('emails.layouts.autoresponder')		

@section('content')

	@if( ! empty( $transaction_data[ 'remitter_firstname' ] ) && $receive_option )

		<p style='margin:15px 0px; font-size:16px; text-align:justify; line-height: 25px;'>

			@if( $receive_option == 1 ) 

				Please be informed that the approximate peso amount to be sent above is based on the PHP conversion rate today. 

				The rate is indicative and <b>may go up or down</b> depending on the time of submission of your remittance form with complete requirements and the actual date and time of clearing of your funds transfer into our bank account. 

				<b>Please take note that our daily exchange rates expires by 6:00 PM Sydney Time and Remittance Forms, Documentary Requirement and Funds that clear after the expiration are processed using the next day rate.</b> 

				If your funds clear today with the complete remittance form and required documents (ID, Deposit Slip / Funds Transfer Receipt), the rate above applies. 

				Your remittance will be available for withdrawal from your selected Cash Pick Up Outlet the same day we receive the money you transferred to our Australian bank account.</u>

				</b>We will send a confirmation email to you and an SMS message to <b>{{$transaction_data[ 'remitter_firstname' ]}}</b> as soon as the peso available on your selected Cash Pick Up Outlet. 

				<br>

				<br>

				Should you have any other concern/inquiry, complaints or recommendations please do not hesitate to contact us. 

				<br>

				We always want to hear from you. 

				<br>

				All of our contact details are below:

				<!--DOOR TO DOOR-->

			@elseif( $receive_option == 4 ) 

				Your remittance will be delivered once your deposit is traced by our Team. 

				Please allow upto <b>5 Business days</b> for the delivery to be completed. 

				<br>

				<br>

				Should you have any other concern/inquiry, complaints or recommendations please do not hesitate to contact us. 

				<br>

				We always want to hear from you. <br>

				All of our contact details are below:

				<!--PHILIPPINE BANK AND MOBILE MONEY TRANSFER-->

			@else 

				Please be informed that the approximate peso amount to be sent above is based on the PHP conversion rate today.

				The rate is indicative and <b>may go up or down</b> depending on the time of submission of your remittance form with complete requirements and the actual date and time of clearing of your funds transfer into our bank account. 

				<b>Please take note that our daily exchange rates expires by 6:00 PM Sydney Time and Remittance Forms, Documentary Requirement and Funds that clear after the expiration are processed using the next day rate.</b>

				If your funds clear today with the complete remittance form and required documents (ID, Deposit Slip / Funds Transfer Receipt), the rate above applies. 

				We will get in touch with you and <b>{$firstname}</b> as soon as the peso is deposited into <b>{$bank_to} {$other_bank_name}</b> bank account. 	

				<br>

				<br>

				Should you have any other concern/inquiry, complaints or recommendations please do not hesitate to contact us. 

				<br>

				We always want to hear from you. 

				<br>

				All of our contact details are below:

			@endif

		</p>

	@endif

@endsection