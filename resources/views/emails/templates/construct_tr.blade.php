<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>iRemit Transaction Number:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'><b>{{$transaction_data[ 'transaction_no' ]}}</b></td>
				
</tr>

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Transaction Date/Time:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{date( 'F j, Y g:i A', strtotime( $transaction_data[ 'transaction_datetime' ] ) )}}</td>
				
</tr>

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Remitter's name:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'remitter_fullname' ]}}</td>
				
</tr>

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Recipient's name:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'recipient_fullname' ]}}</td>
				
</tr>

@if( $transaction_data[ 'receive_options' ] == 1 )

	<tr>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Transaction instruction type:</td>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>Credit to Cash Pick Up - {{$transaction_data[ 'transaction_instruction_type' ]}}</td>
				
	</tr>

@elseif( $transaction_data[ 'receive_options' ] == 4 )

	<tr>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Alternative beneficiary:</td>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'alternative_benefiiciary' ]}}</td>
			
	</tr>

	<tr>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Receiving option cash pick-up:</td>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'transaction_instruction_type' ]}}</td>
				
	</tr>

@else

	<tr>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Transaction instruction type:</td>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>Credit to Philippine Bank - {{$transaction_data[ 'transaction_instruction_type' ]}}</td>
				
	</tr>

	<tr>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Recipient's bank branch:</td>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'bank_branch' ]}}</td>
				
	</tr>

	<tr>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Recipient's bank account name:</td>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'bank_accnt_name' ]}}</td>
				
	</tr>

	<tr>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Recipient's bank account number:</td>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'bank_accnt_no' ]}}</td>
				
	</tr>

@endif

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Recipient's address:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'recipient_address' ]}}</td>
			
</tr>

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Recipient's birthdate:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'recipient_birthday' ]}}</td>
			
</tr>

@if( isset( $transaction_data[ 'promo_code' ] ) && !empty( $transaction_data[ 'promo_code' ] ) )

	<tr>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Promo code:</td>

		<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'promo_code' ]}}</td>
				
	</tr>

@endif	

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Remittance amount in $AUD:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'remittance_amount' ]}}</td>
			
</tr>

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Transfer fee in $AUD:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'transfer_fee' ]}}</td>
			
</tr>

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Total payable to iRemit in $AUD:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'total_payable_to_iremit' ]}}</td>
			
</tr>



<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Total payment sent to:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'bank_source' ]}}

		@if($transaction_data[ 'bank_source' ] != 'POLI')

		{{-- We only expect poli transactions 		

		<br>

		BSB: {{$transaction_data[ 'bank_bsb' ]}} 

		--}}

		<br>

			Account no.: {{$transaction_data[ 'bank_accountno' ]}}

		@endif

	</td>
			
</tr>



<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>AUD for transfer to PHP:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'remittance_amount' ]}}

	</td>
			
</tr>

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Exchange rate today:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'forex_rate' ]}}</td>
			
</tr>

<tr>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:55%;'>Approximate peso amount to be sent to <b>{{$transaction_data['recipient_fullname']}}</b>:</td>

	<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; width:45%;'>{{$transaction_data[ 'peso_amount' ]}}</td>
			
</tr>