<p style='margin:25px 0px; font-size:14px; text-align:justify; line-height: 25px;'>

	iRemit Support Team

	<br>

	iRemit to the Philippines Pty. Ltd.

	<br>

	Visit our website: <a href='".home_url()."' target=\"_blank\"> www.iRemit.com.au </a>

	<br>

	<br>

	<img src='https://iremit.com.au/wp-content/themes/responsive/images/sm_phone.jpg' border='0' /> Call us: 02 8355 2776

	<br>

	<img src='https://iremit.com.au/wp-content/themes/responsive/images/sm_sms.jpg' border='0' /> SMS/Text us: 0438-593-376 (We don’t take calls on this number)

	<br>

	<img src='https://iremit.com.au/wp-content/themes/responsive/images/sm_mail.jpg' border='0' /> Email us: <a href='mailto:support@iremit.com.au'> support@iremit.com.au </a>

	<br>

	<img src='https://iremit.com.au/wp-content/themes/responsive/images/sm_skype.jpg' border='0' /> Our Skype: <b> iRemitAustralia </b>

</p>

<table>

	<tr>

		<td colspan='5' style='margin:10px 0px;'> 

			<h3> Connect with us </h3> 

		</td>

	</tr>

	<tr>

		<td> 

			<a href='http://www.facebook.com/iRemitAustralia' target='_blank'> 

				<img src='https://iremit.com.au/wp-content/themes/responsive/images/sm_fb.jpg' border='0' /> 

			</a> 

		</td>

		<td> 

			<a href='https://twitter.com/@iremitaustralia' target='_blank'> 

				<img src='https://iremit.com.au/wp-content/themes/responsive/images/sm_tw.jpg' border='0' /> 

			</a> 

		</td>

		<td> 

			<a href='https://plus.google.com/+IremitAu/posts' target='_blank'> 

				<img src='https://iremit.com.au/wp-content/themes/responsive/images/sm_gl.jpg' border='0' /> 

			</a> 

		</td>

		<td> 

			<a href='https://www.pinterest.com/iremit/' target='_blank'> 

				<img src='https://iremit.com.au/wp-content/themes/responsive/images/sm_pn.jpg' border='0' /> 

			</a> 

		</td>

		<td> 

			<a href='https://www.linkedin.com/company/iremit-to-the-philippines' target='_blank'> 

				<img src='https://iremit.com.au/wp-content/themes/responsive/images/sm_ln.jpg' border='0' /> 

			</a> 

		</td>

	</tr>

</table>