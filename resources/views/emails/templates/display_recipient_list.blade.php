<table id="transaction-table" class="manage-recipients">

	<tbody>

		<tr id="transaction-headers">

			<td>Recipients Fullname</td>

			<td>Civil Status</td>

			<td>Birthday</td>

			<td>Contact #</td>

			<td>Address</td>

			<td>Action</td>

		</tr>';



		@foreach($data as $r)

			<tr id="transaction-content">

				<td>'.$r['recipient_fullname'].'</td>

				<td>'.$r['recipient_civil_status'].'</td>

				<td>'.$r['recipient_birthday'].'</td>

				<td>'.$r['recipient_phone'].'</td>

				<td>'.$r['address_street1'].' '.$r['address_street2'].'</td>

				<td><a href="/manage-recipient?id='.$r['id'].'&action=edit">edit</a> <a href="/manage-recipient?id='.$r['id'].'&action=delete" onclick="return confirm(\'Are you sure you want to delete this recipient?\'); ">delete</a></td>

			</tr>

		@endforeach 

	</tbody>

</table>
