@foreach($source as $r)

	{{$r['bank_shortname']}}

	@foreach($source as $r)

		{{$r['name']}}

		@foreach($source as $r)

			{{$r['name']}}
			
			Thank you for choosing I-Remit to the Philippines

			<br/>

			<br/>

			This is to confirm that we received your application for remittance. Please be advised that we have encoded your transaction based on the details you provided.

			<br/>

			<br/>

			<table border="0">

			<tbody>

				<tr>

					<td>Transaction No.</td>

					<td>'.$_SESSION['step4']['transaction_no'].'</td>

				</tr>

				<tr>

					<td style="text-align:left">Remitter\'s Fullname</td>

					<td>'.$_SESSION['step1']['first_name'].' '.$_SESSION['step1']['middle_name'].' '.$_SESSION['step1']['last_name'].'</td>

				</tr>
				
				<tr>

					<td style="text-align:left">Address</td>

					<td>Street Address: '.$_SESSION['step1']['street1'].'<br/>

						City: '.$_SESSION['step1']['city'].'<br>

						State / Province: '.$_SESSION['step1']['state'].'<br>

						Postal / Zip Code: '.$_SESSION['step1']['zip'].'<br>

						Country: Australia<br>

					</td>

				</tr>

				<tr>

					<td style="text-align:left">Email Address</td>

					<td><a href="mailto:'.$_SESSION['step1']['email'].'" target="_blank">'.$_SESSION['step1']['email'].'</a></td>

				</tr>

				<tr>

				  <td style="text-align:left">Australian Phone Number</td>

				  <td>'.$_SESSION['step1']['phone_no'].'</td>

				</tr>

				<tr>

					<td style="text-align:left">

				  		Any Valid ID number(Licensed, Passport,Medicare)

					</td>

					<td>

						'.$_SESSION['step1']['any_id'].' - '.$_SESSION['step1']['idnumber'].'

					</td>

				</tr>

				<tr>

					<td style="text-align:left">

						Philippine Recipient\'s Full Name

					</td>

					<td>

						'.$_SESSION['step2']['first_name'].' '.$_SESSION['step2']['middle_name'].' '.$_SESSION['step2']['last_name'].'

					</td>

				</tr>

				<tr>

					<td style="text-align:left">Recipients Birthday</td>

					<td>$_SESSION['step2']['recipient_birthday']</td>

				</tr>

				<tr>

					<td style="text-align:left">

						Address

					</td>

					<td>

						Street Address: $_SESSION['step2']['street1']

						<br>

						City: $_SESSION['step2']['city']

						<br>

						State / Province: $_SESSION['step2']['state']

						<br>

						Postal / Zip Code: $_SESSION['step2']['zip']

						<br>

						Country: Philippines

						<br>

					</td>

				</tr>

				<tr>

					<td style="text-align:left">Phone Number/Mobile Number</td>

					<td>

						<a href="tel:'.$_SESSION['step2']['phone_no'].'" value="" target="_blank">'.$_SESSION['step2']['phone_no'].'</a>

					</td>

				</tr>



				



		if(isset($_SESSION['step2']['receive_options']) && $_SESSION['step2']['receive_options'][0] == '1')



		{		



			$content .= '



				<tr>



				  <td>&nbsp;Bank in AU you will send your remittance<br>



				  </td>



				  <td>'.$bank_source.'</td>



				</tr>



				<tr>



				  <td style="text-align:left">Receiving Option</td>



				  <td>'.$tranfer.': '.$bank_to.'</td>



				</tr>



			';



		}			



			if(isset($_SESSION['step2']['receive_options']) && $_SESSION['step2']['receive_options'][0] == '2')



			{



				$content .= '



					<tr>



					  <td style="text-align:left">Receiving Option</td>



					  <td>'.$tranfer.'</td>



					</tr>



					<tr>



					  <td>Bank Name</td>



					  <td>'.$bank_to.'';



				if(isset($_SESSION['step2']['other_bank_name']))



				{



					$content .=': '.$_SESSION['step2']['other_bank_name'].'';



				}



					$content .='



						</td>



					</tr>



					<tr>



					  <td>Bank Account Name</td>



					  <td>'.$_SESSION['step2']['first_name'].' '.$_SESSION['step2']['middle_name'].' '.$_SESSION['step2']['last_name'].'</td>



					</tr>



					<tr>



					  <td>Bank Branch</td>



					  <td>'.$_SESSION['step2']['bank_branch'].'</td>



					</tr>



					<tr>



					  <td>Bank Account Number</td>



					  <td>'.$_SESSION['step2']['bank_account_no'].'</td>



					</tr>



					<tr>



					  <td>&nbsp;Bank in AU you will send your remittance<br>



					  </td>



					  <td>'.$bank_source.'</td>



					</tr>



					<tr>



					  <td>Amount to be deposited to IRemit in $AUD</td>



					  <td>'.number_format(((float)$_SESSION['step2']['amount'] + $_SESSION['step2']['service_fee']), 2, '.', ',').' (Including $'.$_SESSION['step2']['service_fee'].' service fee)</td>



					</tr>



					<tr>



					  <td>Promo Code</td>



					  <td>'.$_SESSION['step2']['promo_code'].'</td>



					</tr>



					<tr>



					  <td>Reason for Transfer</td>



					  <td>'.$_SESSION['step2']['reason_for'].'</td>



					</tr>';



			}



			else



			{



				$content .= '



				<tr>



				  <td style="text-align:left">Amount to be deposited to IRemit in $AUD</td>



				  <td>'.number_format(((float)$_SESSION['step2']['amount'] + $_SESSION['step2']['service_fee']), 2, '.', ',').' (Including $'.$_SESSION['step2']['service_fee'].' service fee)</td>



				</tr>



					<tr>



					  <td>Promo Code</td>



					  <td>'.$_SESSION['step2']['promo_code'].'</td>



					</tr>



				<tr>



				  <td>Reason for Transfer<br>



				  </td>



				  <td>'.$_SESSION['step2']['reason_for'].'</td>



				</tr>';



			}



		



		if($is_admin)



		{



			$content .= '<tr>



				  <td style="text-align:left">File Attachment</td>



				  <td><a target="_blank" href="'.get_site_url().'/wp-content/uploads/iremit-uploads/'.$_SESSION['step1']['file']['name'].'">'.$_SESSION['step1']['file']['name'].'</a></td>



				</tr><tr>



				  <td style="text-align:left">Attached Transaction Receipt</td>



				  <td><a target="_blank" href="'.get_site_url().'/wp-content/uploads/iremit-uploads/'.$_SESSION['step4']['file']['name'].'">'.$_SESSION['step4']['file']['name'].'</a></td>



				</tr>';



		}



		



		$content .= ' </tbody></table>';



		$content .= 'Your remittance will be credited / delivered once your deposit is traced by our Team.<br/>



					<br/>



					 We will also send a confirmation email once Deposit transfer is made to your Recipients bank account. Should you have any other concern/inquiry please do not hesitate to contact us, you may email us at <a href="mailto:support@iremit.com.au">support@iremit.com.au</a>/ or you may call us <a href="tel:02 8355 2776">02 8355 2776</a>. 



					<br/>



					<br/>



					<br/>



					Customer Support Team<br/> 



					<a href="http://iremit.com.au/" target="_BLANK">I-Remit to the Philippines</a><br/>



					Like Us on Facebook <a href="https://www.facebook.com/iRemitAustralia" target="_BLANK">I-Remit to the Philippines</a><br/>';



		



		return $content;



	}