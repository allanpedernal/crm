@extends('emails.layouts.autoresponder')		

@section('content')

	<p style='margin:0px 0px 15px; font-size:16px; text-align:justify; line-height: 25px;'>

		Dear {$firstname},

		<br>

		<br>

		This is to confirm we have received your remittance instruction today at {$time}. 

		We will start processing this remittance today and will be delivered to your recipient in 5 business days.

	</p>

	<p style='margin:15px 0px 0px; font-size:16px; text-align:justify; line-height: 25px;'> 

		The summary of your remittance is below. 

	</p>
			
	<!--$email_content .= $this -> closing_remarks( $firstname, $transaction_details[ 'transaction_instruction_type' ], $transaction_details[ 'other_bank_name' ], $transaction_details[ 'receive_options' ] );-->

	@include('emails.templates.summary_of_remittance') 

	@include('emails.templates.contact_us_page_signature_content')

@endsection