@extends('emails.layouts.autoresponder')		

@section('content')

	<p style='margin:0px 0px 15px; font-size:16px; text-align:justify; line-height: 25px;'>

		Dear {{$transaction_data[ 'remitter_firstname' ]}},

		<br>

		<br>

		This is to confirm we have received your remittance instruction today at {{date( 'g:i A', strtotime( $transaction_data[ 'transaction_datetime' ] ) )}}. 

		We will start processing your remittance as soon as your transfer clears.

		<!--CASH PICK UP -->

	{{-- 	@if( $transaction_data[ 'receive_options' ] == 1 )

			and be available for pick up today.


		@else 

			, show and available into your recipient’s bank account sometime today. 


		@endif --}}

		<b>This is assuming that your bank transfer / deposit clears into our bank account today, if not, please expect this to be received by your recipient in the next business day</b>.

		<br>

		<br>
				
		We will update you as soon as the payment is completed via email and your recipient via text/SMS.

		<br>

		<br>

		Since you have not paid online using Poli Payments, <b>the forex rate is not locked in and may be affected by any forex fluctuations. 

		The rate will depend on when your remittance clears into our bank account and may go up or down</b>.

		<b>Please pay using Poli Payments on your next transactions to ensure locked in rate that are not affected by ForEx fluctuations as long as your required documentations are complete or submitted within 24 hours of your remittance form</b>.

	</p>

	<p style='margin:15px 0px 0px; font-size:16px; text-align:justify; line-height: 25px;'> The summary of your remittance is below. </p>		

	<!--$email_content .= $this -> closing_remarks( $firstname, $transaction_details[ 'transaction_instruction_type' ], $transaction_details[ 'other_bank_name' ], $transaction_details[ 'receive_options' ] );-->			

	@include('emails.templates.summary_of_remittance')

	@include('emails.templates.contact_us_page_signature_content')

@endsection