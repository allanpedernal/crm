@extends('emails.layouts.autoresponder')		

@section('content')

	<p style='margin:0px 0px 15px; font-size:16px; text-align:justify; line-height: 25px;'>

		Dear {$firstname},

		<br>

		<br>

		This is to confirm we have received your remittance instruction today at {$time}. 

		We will start processing this remittance later please make sure you submit remittance instruction and payments on or before 11AM Sydney time.

		<br>

		<br>

		Since you have paid online using Poli Payments, 
		<b>
			the exchange rate for your remittance will be updated by 9:00 AM and will be locked in on that rate as long as your required documentations are complete or submitted within 24 hours of your remittance form
		</b>

	</p>

	<p style='margin:15px 0px 0px; font-size:16px; text-align:justify; line-height: 25px;'> 
		The summary of your remittance is below. 
	</p>

	<!--$email_content .= $this -> closing_remarks( $firstname, $transaction_details[ 'transaction_instruction_type' ], $transaction_details[ 'other_bank_name' ], $transaction_details[ 'receive_options' ] );-->

	@include('emails.templates.summary_of_remittance')

	@include('emails.templates.contact_us_page_signature_content')

@endsection