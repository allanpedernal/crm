@extends('emails.layouts.autoresponder')		

@section('content')

	<p style='margin:0px 0px 15px; font-size:16px; text-align:justify; line-height: 25px;'>

		Dear {$firstname},

		<br>

		<br>

		This is to confirm we have received your remittance instruction today at {$time}. 

		We will start processing this remittance next business day and will be available for withdrawal or pick up next business day.

		<b>
			This is assuming that your bank transfer / deposit clears into our bank account on or before 6PM next business day, if not, please expect this to be received by your recipient within 2 business days
		</b>

		<br>

		<br>

		For your next transaction, should you want to ensure same day clearing, please make sure you submit remittance instruction and payments on or before 11AM Sydney time. 

		<br>

		<br>

		Since you have not paid online using Poli Payments, 

		<b>
			the forex rate is not locked in and may be affected by any forex fluctuations
		</b>

		<b>
			The rate will depend on when your remittance clears into our bank account and may go up or down
		</b>

		<b>
			Please pay using Poli Payments on your next transactions to ensure locked in rate that are not affected by ForEx fluctuations as long as your required documentations are complete or submitted within 24 hours of your remittance form
		</b>

    </p>

	<p style='margin:15px 0px 0px; font-size:16px; text-align:justify; line-height: 25px;'> 
		The summary of your remittance is below. 
	</p>

	<!--$email_content .= $this -> closing_remarks( $firstname, $transaction_details[ 'transaction_instruction_type' ], $transaction_details[ 'other_bank_name' ], $transaction_details[ 'receive_options' ] );-->

	{{-- @include('emails.templates.summary_of_remittance') --}}

	@include('emails.templates.contact_us_page_signature_content')

@endsection