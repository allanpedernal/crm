private function saturday_12am_to_11am_bdo_metrobank_cashpick_non_poli_auto_responder_email_content( $firstname = '', $time = '00:00 AM', $transaction_details = array() ) {

			

			$email_content = "";

			

			$email_content .= "<p style='margin:0px 0px 15px; font-size:16px; text-align:justify; line-height: 25px;'>";

			

			$email_content .= "Dear {$firstname},";

			

			$email_content .= "<br>";

			

			$email_content .= "<br>";

			

			$email_content .= "This is to confirm we have received your remittance instruction today at {$time}. ";



			$email_content .= "We will start processing this remittance today and will be available for withdrawal or pick up today.";

			

			//CASH PICK UP

			if( $transaction_details[ 'receive_options' ] == 1) {

			

				//$email_content .= " and be available for pick up today.";

			

			} else {

			

				//$email_content .= ", show and available into your recipient’s bank account sometime today. ";

			

			}

			

			$email_content .= "<b>This is assuming that your bank transfer / deposit clears into our bank account today, if not, please expect this to be received by your recipient in the next business day</b>.";



			$email_content .= '<br>';

			

			$email_content .= '<br>';

			

			$email_content .= 'For your next transaction, should you want to ensure same day clearing, please make sure you submit remittance instruction and payments on or before 11AM Sydney Time.';



			$email_content .= '<br>';

			

			$email_content .= '<br>';

			

			$email_content .= 'Since you have not paid online using Poli Payments, <b>the forex rate is not locked in and may be affected by any forex fluctuations. ';

	

			$email_content .= 'The rate will depend on when your remittance clears into our bank account and may go up or down</b>.';

			

			$email_content .= '<b>Please pay using Poli Payments on your next transactions to ensure locked in rate that are not affected by ForEx fluctuations as long as your required documentations are complete or submitted within 24 hours of your remittance form</b>.';

	

			$email_content .= "</p>";

			

			$email_content .= "<p style='margin:15px 0px 0px; font-size:16px; text-align:justify; line-height: 25px;'> The summary of your remittance is below. </p>";

			

			$email_content .= $this -> summary_of_remittance( $transaction_details );

			

			//$email_content .= $this -> closing_remarks( $firstname, $transaction_details[ 'transaction_instruction_type' ], $transaction_details[ 'other_bank_name' ], $transaction_details[ 'receive_options' ] );

			

			$email_content .= $this -> contact_us_page_signature_content();



			return $email_content;

			

		}