private function saturday_12am_to_11am_bdo_metrobank_cashpick_poli_auto_responder_email_content( $firstname = '', $time = '00:00 AM', $transaction_details = array() ) {

			

			$email_content = "";

			

			$email_content .= "<p style='margin:0px 0px 15px; font-size:16px; text-align:justify; line-height: 25px;'>";

			

			$email_content .= "Dear {$firstname},";

			

			$email_content .= "<br>";

			

			$email_content .= "<br>";

			

			$email_content .= "This is to confirm we have received your remittance instruction today at {$time}. ";



			$email_content .= "We will start processing this remittance today and will be available for withdrawal or pick up today.";

			

			//CASH PICK UP

			if( $transaction_details[ 'receive_options' ] == 1) {

			

				//$email_content .= " and be available for pick up today.";

			

			} else {

			

				//$email_content .= ", show and available into your recipient’s bank account sometime today. ";

			

			}

			

			$email_content .= "We will update you as soon as the payment is completed via email and your recipient via text/SMS.";



			$email_content .= '<br>';

			

			$email_content .= '<br>';

			

			$email_content .= 'Since you have paid online using Poli Payments, <b>rest assured that the forex rate is locked in and will not be affected by any forex fluctuations as long as your required documentations are complete or submitted within 24 hours of your remittance form</b>.';

	

			$email_content .= "</p>";

			

			$email_content .= "<p style='margin:15px 0px 0px; font-size:16px; text-align:justify; line-height: 25px;'> The summary of your remittance is below. </p>";

			

			$email_content .= $this -> summary_of_remittance( $transaction_details );

			

			//$email_content .= $this -> closing_remarks( $firstname, $transaction_details[ 'transaction_instruction_type' ], $transaction_details[ 'other_bank_name' ], $transaction_details[ 'receive_options' ] );

			

			$email_content .= $this -> contact_us_page_signature_content();



			return $email_content;

			

		}