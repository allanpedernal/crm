private function send_auto_responder_for_transaction_complete( $remittance_id = 0 ) {

		

			try {

				

				//VALIDATE REMITTER ID

				if( $remittance_id ) {

					

					$transaction_details = $this -> db -> get_transaction_details( $remittance_id );

					

					//CHECK REMITTANCE SYSTEM BY TRACKING NUMBER

					$tracking_number = $transaction_details[ 'tracking_number' ];

					

					$remittance_system = 'bdo remittance'; //SET DEFAULT

					

					//REMITTANCE SYSTEM IS BDO

					if( strpos( strtoupper( $tracking_number ) , "IR15" ) !== false ) {

						

						$remittance_system = 'bdo remittance';

						

					}

					

					//REMITTANCE SYSTEM IS METROBANK

					else if( strpos( strtoupper( $tracking_number ) , "IRPA" ) !== false ) {

						

						$remittance_system = 'metrobank remittance';

						

					}

				

					$email_template = $this -> transaction_complete_email_content( $transaction_details[ 'remitter_firstname' ], $remittance_system, $transaction_details );

				

					return $email_template;

				

				}

			

			} catch( Exception $e ) {

				

				echo "<pre>";

				

				print_r( $e -> getMessage() );

				

				die;

				

			}

			

		}