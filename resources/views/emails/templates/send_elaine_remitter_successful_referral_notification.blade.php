public function send_elaine_remitter_successful_referral_notification( $remitter_referral = array(), $total_remitter_referral = 0 ) {

			

			//VALIDATE IF REMITTER DETAILS AND TOTAL SUCCESSFUL REMITTER REFERRAL

			if( count( $remitter_referral ) && $total_remitter_referral ) {

				

				/*

				* SET DEFAULT PARAMETERS

				*/

				$header = array();



				$header['from'] = "From: I-Remit to the Philippines <support@iremit.com.au>";



				$header['subject'] = "I-Remit to the Philippines::Update and Count Total Remitter Referral";

				

				/*

				* SEND AUTO RESPONDER TO ELAINE

				*/

				if( WEBSITE == 'dev' ) {

					

					$header['to'] = "allan@iremit.com.au,alvin@iremit.com.au,";

					

					$header['to'] .= 'elaine@iremit.com.au';

				

				} else{

					

					$header['to'] = 'elaine@iremit.com.au,marckjoseph@iremit.com.au';

					

				}

				

				/*

				* CONSTRUCT EMAIL CONTENT

				*/

				$email_content = "";

				

				$email_content .= "<p style='margin:0px 0px 15px; font-size:16px; text-align:justify; line-height: 25px;'>";



				$email_content .= "Dear Ms. Elaine Malonzo,";

				

				$email_content .= '<br>';

				

				$email_content .= '<br>';

				

				$email_content .= "We would like to inform you that: ";

				

				$email_content .= '<br>';

				

				$email_content .= "Remitter Referral Name: {$remitter_referral[ 'firstname' ]} {$remitter_referral[ 'lastname' ]} ";

				

				$email_content .= '<br>';

				

				$email_content .= "Remitter Referral Phone: {$remitter_referral[ 'phone' ]}";

				

				$email_content .= '<br>';

				

				$email_content .= "Remitter Referral Email: {$remitter_referral[ 'email' ]}";

				

				$email_content .= '<br>';

				

				$email_content .= "Remitter Referral Generated Promo Code: {$remitter_referral[ 'generated_promo_code' ]}";

				

				$email_content .= '<br>';

				

				$email_content .= "Latest total successful referral count is: {$total_remitter_referral}";

				

				$email_content .= '<br>';

				

				if( empty( $remitter_referral[ 'userid' ] ) ) {

					

					$email_content .= "Note: This Remitter Referral Person is not yet registered remitter in our system."; 

					

				} else {

					

					$email_content .= "Note: This Remitter Referral Person is registered remitter in our system.";

					

				}

	

				$email_content .= '<br>';

	

				$email_content .= "</p>";



				$email_content .= $this -> contact_us_page_signature_content();



				/*

				* SEND EMAIL

				*/

				add_filter( 'wp_mail_content_type', array( &$this, 'set_html_content_type' ) );



				$this -> _execute_autoresponder( $header, $email_content );



				remove_filter( 'wp_mail_content_type', array( &$this, 'set_html_content_type' ) );



				

			}

			

		}