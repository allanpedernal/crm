<table style='width:100%; border: 1px solid black; border-collapse: collapse; font-family:calibri; font-size:16px; margin: 0px 0px 30px;'>

	<tbody>
			
		@include('emails.templates.construct_tr')

	</tbody>

</table>



@if( $transaction_data[ 'receive_options' ] == 1 )

	<p style='margin:25px 0px; font-size:14px; text-align:justify; line-height: 25px;'><b>Disclaimer</b>: This Transaction number: <b>{{$transaction_data['transaction_no']}}</b> is an internal control number within the I-Remit system. <br> <b>You will be emailed a separate Reference Number to use for claiming your remittance through {{$transaction_data['transaction_instruction_type']}}</b></p>

@endif