private function summary_of_transaction_complete( $transaction_data = array() ) {

			

			try {

				

				$summary_of_transaction_complete = "";

				

				$summary_of_transaction_complete .= "<table style='width:70%; border: 1px solid black; border-collapse: collapse; font-family:calibri; font-size:16px; margin: 0px;'>";

				

				$summary_of_transaction_complete .= '

				

				<thead>

				

					<tr>

					

						<th style="border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;"> AUD Amount </th>

						

						<th style="border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;"> Total Payment Sent To </th>

						

						<th style="border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;"> Transfer Fees $AUD </th>

						

						<th style="border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;"> $AUD For Transfer to PH </th>

						

						<th style="border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;"> Exchange Rate </th>

						

						<th style="border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;"> Deposit PHP Amount </th>

						

					</tr>

				

				</thead>

				

				';

				

				$summary_of_transaction_complete .= "

				

				<tbody>

				

					<tr style='background-color:#ffd966;'>

					

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;'> <b>{$transaction_data['total_payable_to_iremit']}</b> </td>

						

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;'> <b>{$transaction_data['bank_source']}</b> </td>

						

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;'> <b>{$transaction_data['transfer_fee']}</b> </td>

						

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;'> <b>{$transaction_data['remittance_amount']}</b> </td>

						

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;'> <b>{$transaction_data['forex_rate']}</b> </td>

						

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top;'> <b>{$transaction_data['peso_amount']}</b> </td>

					

					</tr>

					

					<tr style='background-color:#ffd966;'>

					

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; text-align:center;' colspan='2'> <b>NAME OF RECIPIENT</b> </td>

						

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; text-align:center;' colspan='2'> <b>TRANSACTION TYPE</b> </td>

						

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; text-align:center;' colspan='2'> <b>REFERENCE NUMBER</b> </td>

					

					</tr>

					

					<tr style='background-color:#ffd966;'>

					

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; text-align:center;' colspan='2'> {$transaction_data['recipient_fullname']} </td>

						

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; text-align:center;' colspan='2'> <b>{$transaction_data['receive_options_name']} - {$transaction_data['receive_option_transfer']}</b>  </td>

						

						<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; color: #000000; vertical-align:top; text-align:center;' colspan='2'> <b>{$transaction_data['tracking_number']}</b> </td>

					

					</tr>

				

				</tbody>

				

				";

				

				$summary_of_transaction_complete .= "</table>";

				

				return $summary_of_transaction_complete;

			

			} catch ( Exception $e ) {

				

				echo "<pre>";

				

				echo $e -> getMessage();

				

				exit;

				

			}

			

		}