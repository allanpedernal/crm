private function sunday_poli_auto_responder_email_content( $firstname = '', $time = '00:00 AM', $transaction_details = array() ) {

			

			$email_content = "";

			

			$email_content .= "<p style='margin:0px 0px 15px; font-size:16px; text-align:justify; line-height: 25px;'>";

			

			$email_content .= "Dear {$firstname},";

			

			$email_content .= "<br>";

			

			$email_content .= "<br>";

			

			$email_content .= "This is to confirm we have received your remittance instruction today at {$time}. ";



			$email_content .= "We will start processing this remittance first thing on Monday and will be available for withdrawal or pick up by the next business day assuming the money you have transferred have cleared. ";

			

			$email_content .= "For your next transaction, should you want to ensure same day processing, ";



			$email_content .= "please make sure you submit remittance instruction and payments on or before 11AM Sydney time on any weekdays.";

			

			$email_content .= '<br>';

			

			$email_content .= '<br>';



			$email_content .= 'Since you have paid online using Poli Payments, <b>the exchange rate for your remittance will be updated by 9:00 AM and will be locked in on that rate as long as your required documentations are complete or submitted within 24 hours of your remittance form</b>.';

    

			$email_content .= "</p>";

			

			$email_content .= "<p style='margin:15px 0px 0px; font-size:16px; text-align:justify; line-height: 25px;'> The summary of your remittance is below. </p>";

			

			$email_content .= $this -> summary_of_remittance( $transaction_details );

			

			//$email_content .= $this -> closing_remarks( $firstname, $transaction_details[ 'transaction_instruction_type' ], $transaction_details[ 'other_bank_name' ], $transaction_details[ 'receive_options' ] );

			

			$email_content .= $this -> contact_us_page_signature_content();



			return $email_content;

			

		}