private function transaction_complete_email_content( $firstname = '', $remittance_system = 'bdo remittance', $transaction_details = array() ) {

			

			$email_content = "";

			

			$email_content .= "<p style='margin:0px 0px 15px; font-size:16px; text-align:justify; line-height: 25px;'>";

			

			$email_content .= "Hi {$firstname},";

			

			$email_content .= "<br>";

			

			$email_content .= "<br>";

			

			$email_content .= "Good day! Thank you for choosing I-Remit to the Philippines.";

			

			$email_content .= "<br>";



			$email_content .= "Please be advice that we have <b>COMPLETED THE TRANSFER</b> of your Remittance to <b>{$transaction_details[ 'recipient_fullname' ]}</b> ";

			

			$email_content .= "with the following details:";



			$email_content .= "<br>";

			

			$email_content .= "Your remittance is now available for pickup at any <b>{$transaction_details[ 'receive_option_transfer' ]}</b> ";



			$email_content .= "</p>";

			

			//INSTRUCTION 1 START 

			$email_content .= "<ul style='margin:15px 0px; list-style-type:circle;'>";

			

			//NOTE 1

			$email_content .= "<li style='font-size:16px; line-height: 25px; text-align:justify;'>";

			

			$email_content .= "If you provided us a Philippine mobile number of your recipient, ";



			$email_content .= "we would have already sent the claim details to <b>{$transaction_details[ 'recipient_fullname' ]}</b> ";



			$email_content .= "Sit back, relax we have already taken care of this ";



			$email_content .= "last step for you. ";

			

			$email_content .= "</li>";

			

			//NOTE 2

			$email_content .= "<li style='font-size:16px; line-height: 25px; text-align:justify;'>";

			

			$email_content .= "If you have NOT provided us with contact details of kindly copy ";



			$email_content .= "the details in Italics below and send it to <b>{$transaction_details[ 'recipient_fullname' ]}</b>. ";



			$email_content .= "These details are required to claim the cash from <b>{$transaction_details[ 'receive_option_transfer' ]}</b>: ";

			

			$email_content .= "<br>";



			//INSTRUCTION 2 START 

			$email_content .= "Details Need to Claim Cash Remittance: ";

			

			$email_content .= "<ol style='margin:15px 0px; list-style-type:decimal;'>";

			

			$email_content .= "<li style='font-size:16px; line-height: 25px; text-align:justify;'> Sender’s full Name is <b>{$transaction_details[ 'remitter_fullname' ]}</b> </li>";

			

			$email_content .= "<li style='font-size:16px; line-height: 25px; text-align:justify;'> Reference Number for claiming is: <b>{$transaction_details[ 'tracking_number' ]}</b> </li>";

			

			$email_content .= "<li style='font-size:16px; line-height: 25px; text-align:justify;'> Amount to be received is Php <b>{$transaction_details[ 'peso_amount' ]}</b> </li>";

			

			$email_content .= "<li style='font-size:16px; line-height: 25px; text-align:justify;'> Please bring 2 valid photo IDs </li>";

			

			$email_content .= "<li>";

			

			$email_content .= "Please inform <b>{$transaction_details[ 'receive_option_transfer' ]}</b> that remittance came from Australia via ";



			

			if( $remittance_system == 'bdo remittance' ) {

				

				$email_content .= "I-Remit to the Philippines and BDO Remit Partner.";

			

			}

			

			else if( $remittance_system == 'metrobank remittance' ) {

				

				$email_content .= "I-Remit to the Philippines and Metrobank Remittance Partner.";

				

			}



			$email_content .= "</li>";

			

			$email_content .= "</ol>";

			

			$email_content .= "</li>";

			

			$email_content .= "</ul>";

			//INSTRUCTION END 

			

			

			//TIP

			$email_content .=  "<p style='font-size:14px; text-align:justify; line-height: 25px; margin: 0px 0px 15px;'>";

			

			$email_content .= "TIP: Sometimes cash pick up branches runs out of cash. They will tell you ";



			$email_content .= 'that the "money is not in yet" or "they cannot find the transaction". When ';



			$email_content .= "this happens, go to other cash pick up branch or check ";

			

			if( $remittance_system == 'bdo remittance' ) {

				

				//BDO TIPS

				$email_content .= "<a href='https://www.bdocashcard.com/rts/remitinquire.asp' target='_blank'>https://www.bdocashcard.com/rts/remitinquire.asp</a> enter your ";



				$email_content .= "reference number then show transfer details to the branch. We are ";



				$email_content .= "supported by BDO Remit, one of the biggest banks in the Philippines. A ";



				$email_content .= "reference number is only generated when money is transferred. So a ";



				$email_content .= "reference number is a confirmation that money is already available for pick up.";

				

			}

			

			else if( $remittance_system == 'metrobank remittance' ) {

				

				//METROBANK TIPS

				$email_content .=  "back with us. We ";



				$email_content .=  "are supported by Metrobank Remittance, one of the biggest banks in the ";



				$email_content .=  "Philippines. A reference number is only generated <b>when money is transferred</b>. ";



				$email_content .=  "So a reference number is a confirmation that money is ";



				$email_content .=  "already available for pick up. ";

				

			}

			

			$email_content .=  "</p>";

			

			//TABLE

			$email_content .= $this -> summary_of_transaction_complete( $transaction_details );



			$email_content .= "<p style='margin:15px 0px; font-size:16px; text-align:justify; line-height: 25px;'>";



			$email_content .= "Should you have any other concern/inquiry , complaints or ";



			$email_content .= "recommendations please do not hesitate to contact us. We ";



			$email_content .= "always want to hear from you. All of our contact details are below: ";

			

			$email_content .=  "</p>";

			

			

			$email_content .= $this -> transfer_complete_signature_content( $transaction_details );



			return $email_content;



		}