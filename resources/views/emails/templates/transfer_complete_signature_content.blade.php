private function transfer_complete_signature_content( $transaction_data = array() ) {

			

			$transfer_complete_signature_content = "<p style='margin:25px 0px; font-size:14px; text-align:justify; line-height: 25px;'>";

			

			$transfer_complete_signature_content .= "Take care and see you again. Love, ";

			

			$transfer_complete_signature_content .= "<br>";

			

			$transfer_complete_signature_content .= "<a href=''>I-Remit to the Philippines</a>";

			

			$transfer_complete_signature_content .= "<br>";

			

			$transfer_complete_signature_content .= "Telephone: <a href='tel:02 8355 2776'>02 8355 2776</a>";

			

			$transfer_complete_signature_content .= "<br>";

			

			$transfer_complete_signature_content .= "SMS Hotline : 0438 593 376 (We don’t accept calls in this number)";

			

			$transfer_complete_signature_content .= "<br>";

			

			$transfer_complete_signature_content .= "Email : <a href='mailto:support@iremit.com.au'>support@iremit.com.au</a>";

			

			$transfer_complete_signature_content .= "<br>";

			

			$transfer_complete_signature_content .= "Facebook: <a href='https://www.facebook.com/iRemitAustralia' target='_blank'>www.facebook.com/iRemitAustralia</a>";

			

			$transfer_complete_signature_content .= "<br>";

			

			$transfer_complete_signature_content .= "Skype Us: <b>iremitaustralia</b>";

 

			$transfer_complete_signature_content .= "</p>";

			

			$transfer_complete_signature_content .= "<p style='font-size:10px; margin:5px 0px;'> REMITTANCE FORM TRANSACTION NUMBER : {$transaction_data['transaction_no']} \ {$transaction_data['tracking_number']} </p>";

			

			$transfer_complete_signature_content .= "<p style='font-size:10px; margin:5px 0px;'> REMITTER EMAIL ADDRESS : <a href='mailto:{$transaction_data['remitter_email']}'>{$transaction_data['remitter_email']}</a> </p>";

			

			$transfer_complete_signature_content .= "<p style='font-size:10px; margin:5px 0px;'> REMITTANCE RECIPIENT PHILIPPINE CONTACT NUMBER : <a href='tel:{$transaction_data['recipient_phone']}'>{$transaction_data['recipient_phone']}</a> </p>";

			

			return $transfer_complete_signature_content; 

			

		}