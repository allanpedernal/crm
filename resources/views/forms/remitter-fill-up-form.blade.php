<!-- REGISTRATION AND REMITTER FILL UP FORM START -->
	<form method="post" enctype="multipart/form-data" action="" id="iremit-form-step1" class="iremit-form" style="margin:0px;" autocomplete="off">

		{{ csrf_field() }}

		<input type="hidden" value="step1" name="wp_form_id" id="wp_form_id">	

		<input type="hidden" value="0" name="is_back_step" id="is_back_step">
		
		<input type="hidden" name="custom_token" value="echo $_SESSION['custom_token'];">
		
		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<h5 style="margin:0px 0px 20px;"> Note: Please fill up all required fields<span class="form-required">*</span></h5>
			
			</div>
		
		</div>

		<div class="row">
		
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="first_name"> First name:<span class="form-required">*</span> </label>
					
					<input type="text" value='Peter John' id="first_name" name="first_name" placeholder="First name (As shown in Valid Id)" class="form-control" required="required">

					<!--value="echo (isset($_SESSION['step1']['first_name']) ? $_SESSION['step1']['first_name'] : "-->
				
				</div>
			
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="middle_name"> Middle name: </label>
					
					<input type="text" value="Sorianosos" placeholder="Middle name" id="middle_name" name="middle_name" class="form-control">
				
					<!-- value="echo (isset($_SESSION['step1']['middle_name']) ? $_SESSION['step1']['middle_name'] : " -->

				</div>
			
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="last_name"> Last name:<span class="form-required">*</span> </label>
					
					<input type="text" value="Jimenez" id="last_name" name="last_name" placeholder="Last name (As shown in Valid Id)" class="form-control" required="required">

					<!-- value="echo (isset($_SESSION['step1']['last_name']) ? $_SESSION['step1']['last_name'] : " -->
				
				</div>
			
			</div>
		
		</div>
		
		<!-- DISCLAIMER START -->
		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<div class="well well-sm">
				
					<p style="color:#cc7880; font-size: 12px; font-weight: 500; margin:0px;"> <strong>Disclaimer</strong>: Incorrectly spelled or missing Name information can delay your remittance, please ensure that your are encoding your full name AS SHOWN IN YOUR VALID ID. </p>
				
				</div>
			
			</div>
		
		</div>
		<!-- DISCLAIMER END -->
		
		<div class="row">
		
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="email"> Email address:<span class="form-required">*</span> </label>
				
					<input type="email" value='pj@iremit.com.au' placeholder="Email address" id="email" name="email"  class="form-control" required="required">
				
					<!-- value="echo (isset($_SESSION['step1']['email']) ? $_SESSION['step1']['email'] : " -->

				</div>
			
			<!-- DISCLAIMER START -->
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="well well-sm">
						<p style="color:#cc7880; font-size: 12px; font-weight: 500; margin:0px;"> <strong>Disclaimer</strong>: Please ensure that you add correctly spelled and existent email. To get real updates from us. </p>
					</div>
				</div>
			</div>
			<!-- DISCLAIMER END -->

			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="phone_no"> Australian mobile number:<span class="form-required">*</span> </label>
				
					<input type="text" value="0491570157" id="phone_no"  name="phone_no" placeholder="Australian mobile number" class="form-control" required="required">

					<!-- value="echo (isset($_SESSION['step1']['phone_no']) ? $_SESSION['step1']['phone_no'] : " -->

				</div>
			
			<!-- DISCLAIMER START -->
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="well well-sm">
						<p style="color:#cc7880; font-size: 12px; font-weight: 500; margin:0px;"> <strong>Disclaimer</strong>: Must be a working AU number. </p>
					</div>
				</div>
			</div>
			<!-- DISCLAIMER END -->
			
			</div>
		
		</div>

		<div class="row">
		
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="password"> Password:<span class="form-required">*</span></label>
				
					<input type="password" placeholder="Desired Password" value='serial' id="password" name="password" class="form-control" required="required">

					<!-- value="echo (isset($_SESSION['step1']['password']) ? $_SESSION['step1']['password'] : " -->
				
				</div>
			
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="confirm_password"> Confirm Password:<span class="form-required">*</span></label>
				
					<input type="password" id="confirm_password" value='serial' name="confirm_password" placeholder="Confirm Desired Password" class="form-control" required="required">

					<!--value="echo (isset($_SESSION['step1']['confirm_password']) ? $_SESSION['step1']['confirm_password'] : "-->

				</div>
			
			</div>
		
		</div>
	
		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="street1"> Residential Address:<span class="form-required">*</span> </label>
				
					<input type="text" value='Mansilingan' placeholder="Residential Address" id="street1" name="street1" class="form-control" required="required">

					<!-- value="echo (isset($_SESSION['step1']['street1']) ? $_SESSION['step1']['street1'] : " -->
				
				</div>
			
			</div>
		
		</div>
	
		<div class="row">
		
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for=""> Country:<span class="form-required">*</span> </label>
				
					<select id="country" name="country" required="required" readonly="readonly" class="form-control chosen-select-deselect">

					</select>
					
				</div>
			
			</div>
			
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="remitter_state"> State:<span class="form-required">*</span> </label>
					
					<select id="remitter_state" name="state" class="form-control chosen-select-deselect" data-placeholder="State" required="required">

						<option value=""> State </option>

					</select>
				
				</div>
			
			</div>
			
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="city"> City:<span class="form-required">*</span></label>
				
					<select id="city" name="city" class="form-control chosen-select-deselect" required="required" data-placeholder="City">
					
						<option value=""> City </option>				

					</select>
				
				</div>
			
			</div>
			
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
					
					<label for="zip"> Postal code:<span class="form-required">*</span></label>
					
					<input id="zip" type="text" value="echo (isset($_SESSION['step1']['zip']) ? $_SESSION['step1']['zip'] : " name="zip" title="This will be automatically filled with your chosen state and city" readonly placeholder="Post code" type="number" class="form-control">
				
				</div>
			
			</div>
		
		</div>
		
		<hr>
	
		<div class="row">
		
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="nationality"> Nationality: </label>
				
					<select id="nationality" name="nationality" class="form-control chosen-select-deselect">

						<option value=""> Nationality </option>

							<option value=" echo $nationality->id ">  echo $nationality->name </option>

					</select> 
					
				</div>
			
			</div>
		
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="remitter_civil_status"> Civil status:<span class="form-required">*</span> </label>
					
					<select id="remitter_civil_status" name="remitter_civil_status" class="form-control chosen-select-deselect" required="required" data-placeholder="Civil status">

						<option value=""> Civil status </option>

					</select>
				
				</div>
			
			</div>
			
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="birthday"> Birthday:<span class="form-required">*</span> </label>
					
					<input class="calendarpicker form-control" type="text" name="remitter_birthday" id="birthday" placeholder="dd/mm/yyyy" value="echo (isset($_SESSION['step1']['remitter_birthday']) ? $_SESSION['step1']['remitter_birthday'] : " readonly="readonly" required="required">

				</div>
			
			</div>
			
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				
				<div class="form-group">
				
					<label for="sec_email"> Secondary email: </label>
					
					<input type="email" id="sec_email" value='peterjohnjimenez@gmail.com' name="sec_email" placeholder="Secondary email" class="form-control">

					<!-- value="echo (isset($_SESSION['step1']['sec_email']) ? $_SESSION['step1']['sec_email'] : " -->

				</div>
			
			<!-- DISCLAIMER START -->
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="well well-sm">
						<p style="color:#cc7880; font-size: 12px; font-weight: 500; margin:0px;"> <strong>Disclaimer</strong>: Please ensure that you add correctly spelled and existent email. To get real updates from us. </p>
					</div>
				</div>
			</div>
			<!-- DISCLAIMER END -->

			</div>
			
		</div>
	
		<hr>
		
		<div class="row">
		
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="any_id_type"> ID Type:<span class="form-required">*<span> </label>

					<select name="any_id" id="any_id_type" class="form-control chosen-select-deselect" required="required" data-placeholder="ID Type">

					<option value=""> ID Type </option>

<!-- need to fill with jQuery -->

					</select>
					
					<hr>
					
											<!-- other id type -->	
					<div id="other_id_type_container" class="row" style="display:none;"> 
					
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						
							<div class="form-group">
							
								<label for="other_id_type">Other ID TYPE:<span class="form-required">*</span></label>
								
								<input type="text" id="other_any_id" class="form-control" placeholder="provide description" maxlength="20" value="echo (isset($_SESSION['step1']['any_id']) ? $_SESSION['step1']['any_id'] : ''); ">
							
							</div>
							
						</div>
						
					</div>
					<!-- other id type -->	

				</div>
			
			</div>
			
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="idnumber"> ID Number:<span class="form-required">*<span></label>
					
					<input type="text" value='EC8641' placeholder="ID Number" id="idnumber" name="idnumber" class="form-control" >

					<!-- value=" echo (isset($_SESSION['step1']['idnumber']) ? $_SESSION['step1']['idnumber'] : ''); " -->

				</div>
			
			</div>
			
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="expiredid"> ID Expiration date:<span class="form-required">*</span> </label>
					
					<input class="expiredpicker form-control" type="text" placeholder="dd/mm/yyyy" value="php echo (isset($_SESSION['step1']['expiredid']) ? $_SESSION['step1']['expiredid'] : ''); " required="required" readonly="readonly" id="expiredid" name="expiredid">

				</div>
			
			</div>
			
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="remitter_referral_promo_code"> Referral Code: </label>
					
					<input class="form-control" type="text" placeholder="Promo code" id="remitter_referral_promo_code" name="remitter_referral_promo_code">

				</div>
			
			</div>
		
		</div>
		
		<hr style="color:none; background:none; border:none; margin: 5px 0px;">
	
		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="file_attachment"> Upload the I.D. attachment (<strong>one time only</strong>)<span class="form-required">*</span> </label>
					
					<input type="file" id="file_attachment" name="file_attachment"  onchange="ValidateSingleInput(this);" file-accept="pdf, doc, docx, mp3, wma, mpg, flv, jpg, png, gif" required="required" file-maxsize="512000" value=" print $_SESSION['step1']['file']['name'];"/> 

					<!-- file attachment -->
					
						<p style="margin:5px 0px;"> <strong>File attachment</strong>: <a target="_blank" href="/wp-content/uploads/iremit-uploads/echo $_SESSION['step1']['file']['name'];"> print $_SESSION['step1']['file']['name'];</a> </p>

						<!-- if file attachment -->
	
					<p style="color:red;" id="checkupload"></p>
				</div>
			
			</div>
		
		</div>
		
		<!-- DISCLAIMER START -->
		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<div class="well well-sm">
				
					<p style="color:#cc7880; font-size: 12px; font-weight: 500; margin:0px;"> <strong>Disclaimer</strong>: Expired or Invalid ID can delay your remittance, please ensure that you are uploading a valid and unexpired ID. </p>
				
				</div>
			
			</div>
		
		</div>
		<!-- DISCLAIMER END -->
		
				<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<p style="margin:0px 0px;"> We need at least 1 ID Number to abide by the AUSTRAC regulatory requirements. We are registered with AUSTRAC and reports all remittances as per regulation for your protection.</p>
			
			</div>
		
		</div>
		
		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<p class="text-right"> By Clicking <strong>Create an Account</strong>, You are Agreeing to this <a href="/terms-and-conditions-of-service/" target="_blank" style="color: #ec664e;"> Terms and Conditions of Service </a> </p>
			
			</div>
		
		</div>

		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
			
				<input type="submit" class="iremit-submit btn btn-primary btn-sm nav-button gradient round" name="send_step1" id="send_step1" value="Create an Account" />
				
				<input type="hidden" name="send_step1" />
				
			</div>
		
		</div>
	</form>

</style>

	<script type="text/javascript">
    jQuery('form#iremit-form-step1 input').on('keyup blur', function () {
        if (jQuery('form#iremit-form-step1').valid()) {
            jQuery('#send_step1').prop('disabled', false);
        } else {
            jQuery('#send_step1').prop('disabled', 'disabled');
        }
    });

    jQuery('form#iremit-form-step1 input#expiredid').on('change', function () {
        if (jQuery('form#iremit-form-step1').valid()) {
            jQuery('#send_step1').prop('disabled', false);
        } else {
            jQuery('#send_step1').prop('disabled', 'disabled');
        }
    });    
    </script>
    
	<!-- if logged in -->

{{-- 		<script type="text/javascript">

			//OTHER ID
			if(jQuery('#any_id_type').val() == "") {
				
				jQuery("select#any_id_type").removeAttr( "required" );
				
				jQuery("select#any_id_type").removeAttr( "name" );
				
				jQuery("input#other_any_id").attr( "required", "required" );
				
				jQuery("input#other_any_id").attr( "name", "any_id" );
				
				jQuery("select#any_id_type").val("Other (provide description)");
				
			}

		</script> --}}

	
	<script type="text/javascript">

		var text;

		var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".pdf", ".doc", ".docx"];   
		 
		function ValidateSingleInput(oInput) {
			
			if (oInput.type == "file") {
				
				var sFileName = oInput.value;

				if (sFileName.length > 0) {
					
					var blnValid = false;

					for (var j = 0; j < _validFileExtensions.length; j++) {
						
						var sCurExtension = _validFileExtensions[j];

						if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
								
							blnValid = true;

							break;

						}

					}

					if (!blnValid) {
						
						alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));

						text = "Invalid File Type. Accepted File Types (.jpg, .gif, .pdf, .doc, .jpeg, .docx, .png, .bmp)";

						oInput.value = "";

						document.getElementById("checkupload").innerHTML = text;

						return false;

					} else {
						
						text = "Accepted File.";

						document.getElementById("checkupload").innerHTML = text;

					}

				}

			}

			return true;

		}

	jQuery( function() {



		jQuery('#remitter_state').change(function() {
			//LOADING
			jQuery( "body" ).isLoading( {

				text: "Loading",

				position: "overlay",

				transparency: 0.5

			} );

			jQuery( document ).ajaxStop( function() {
				
			  jQuery( "body" ).isLoading( "hide" );
			  
			} );

		});	

		jQuery('#city').change(function() {
			//LOADING
			jQuery( "body" ).isLoading( {

				text: "Loading",

				position: "overlay",

				transparency: 0.5

			} );

			jQuery( document ).ajaxStop( function() {
				
			  jQuery( "body" ).isLoading( "hide" );
			  
			} );

		});	

			
	} );

	</script>	
	<!-- REGISTRATION AND REMITTER FILL UP FORM END -->


