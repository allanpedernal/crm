<script id="recipient_address_template" type="text/x-handlebars-template">
	
	<div id="recipient_address_item_{{hash}}" class="well">
	
		<input type="hidden" id="recipient_address_id_{{hash}}" name="recipient_address[{{hash}}][id]" value="0">
	
		<!-- RECIPIENT ADDRESS ITEM REMOVE START -->
		<div class="row">
	
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">

				<a href="#" class="btn btn-danger btn-xs remove_recipient_address_item" rel="{{hash}}" style="margin:0px 0px 15px;"><i class="fa fa-trash"></i> Remove</a>
				
			</div>
		
		</div>
		<!-- RECIPIENT ADDRESS ITEM REMOVE END -->
		
		<div class="row">
		
			<!-- RECIPIENT ADDRESS COUNTRY START -->
			<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
			
				<div class="form-group">
				
					<label for="recipient_country_{{hash}}"> Country:<span style="color:#E85445;">*</span> </label>
					
					<select id="recipient_country_{{hash}}" name="recipient_address[{{hash}}][country]" class="form-control chosen-select recipient_country" data-hash="{{hash}}" required="required" data-placeholder="Country">
						
						<option value=""></option>
						
						{{#each countries}}
						
							<option value="{{ id }}"> {{ en_short_name }} </option>

						{{/each}}
						
					</select>
				
				</div>
			
			</div>
			<!-- RECIPIENT ADDRESS COUNTRY END -->
			
			<!-- RECIPIENT ADDRESS PROVINCE START -->
			<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
				
				<div class="form-group">
				
					<label for="recipient_state_{{hash}}"> Province:<span style="color:#E85445;">*</span> </label>
					
					<select id="recipient_state_{{hash}}" name="recipient_address[{{hash}}][state]" class="form-control chosen-select recipient_state" data-hash="{{hash}}" required="required" data-placeholder="Province">
						
						<option value=""></option>

					</select>
				
				</div>
			
			</div>
			<!-- RECIPIENT ADDRESS PROVINCE END -->
			
			<!-- RECIPIENT ADDRESS CITY START -->
			<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
				
				<div class="form-group">
				
					<label for="recipient_city_{{hash}}"> City:<span style="color:#E85445;">*</span> </label>
					
					<select id="recipient_city_{{hash}}" name="recipient_address[{{hash}}][city]" class="form-control chosen-select recipient_city" data-hash="{{hash}}" required="required" data-placeholder="City">
						
						<option value=""></option>
						
					</select>
				
				</div>
			
			</div>
			<!-- RECIPIENT ADDRESS CITY END -->
			
			<!-- RECIPIENT POSTAL START -->
			<div class="col-lg-2 col-md-2 col-sm-12 col-sm-12">
				
				<div class="form-group">
				
					<label for="recipient_postal_{{hash}}"> Postal:<span style="color:#E85445;">*</span> </label>
					
					<input type="text" id="recipient_postal_{{hash}}" name="recipient_address[{{hash}}][postal]" class="form-control recipient_postal" placeholder="Postal" required="required">
				
				</div>
			
			</div>
			<!-- RECIPIENT POSTAL END -->
			
			<!-- RECIPIENT ADDRESS IS PREFERRED START -->
			<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-left">
				
				<div class="form-group text-center">
				
					<label for="recipient_address_is_preferred_{{hash}}"> Use </label>
					
					<br>
					
					<input type="radio" id="recipient_address_is_preferred_{{hash}}" name="recipient_address[{{hash}}][is_preferred]" class="recipient_address_is_preferred" value="1">
				
				</div>
			
			</div>
			<!-- RECIPIENT ADDRESS IS PREFERRED END -->
		
		</div>
		
		<div class="row">
			
			<!-- RECIPIENT CURRENT ADDRESS START -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				
				<div class="form-group">
				
					<label for="recipient_current_address_{{hash}}"> Current address:<span style="color:#E85445;">*</span> </label>
					
					<textarea id="recipient_current_address_{{hash}}" name="recipient_address[{{hash}}][current_address]" placeholder="Current address" class="form-control" rows="2" required="required"></textarea>
				
				</div>
			
			</div>
			<!-- RECIPIENT CURRENT ADDRESS END -->
		
			<!-- RECIPIENT PERMANENT ADDRESS START -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				
				<div class="form-group">
				
					<label for="recipient_permanent_address_{{hash}}"> Permanent address:<span style="color:#E85445;">*</span> </label>
					
					<textarea id="recipient_permanent_address_{{hash}}" name="recipient_address[{{hash}}][permanent_address]" placeholder="Permanent address" class="form-control" rows="2" required="required"></textarea>
				
				</div>
			
			</div>
			<!-- RECIPIENT PERMANENT ADDRESS END -->
		
		</div>

	</div>

</script>

<script id="recipient_state_option_template" type="text/x-handlebars-template">
	
	<option value=""></option>
	
	{{#each states}}
	
		<option value="{{ id }}"> {{ name }} </option>
	
	{{/each}}
	
</script>

<script id="recipient_city_option_template" type="text/x-handlebars-template">
	
	<option value=""></option>
	
	{{#each cities}}
	
		<option value="{{ id }}"> {{ name }} </option>
	
	{{/each}}
	
</script>
