<script id="recipient_bank_template" type="text/x-handlebars-template">
	
	<div id="recipient_bank_item_{{hash}}" class="well well-sm">
	
		<input type="hidden" id="recipient_bank_id_{{hash}}" name="recipient_bank[{{hash}}][id]" value="0">
	
		<div class="row">
		
			<!-- RECIPIENT BANK IS PREFERRED START -->
			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center">
				
				<div class="form-group">
				
					<label for="recipient_bank_type_is_preferred_{{hash}}"> Use </label>
					
					<br>
				
					<input type="radio" id="recipient_bank_type_is_preferred_{{hash}}" name="recipient_bank[{{hash}}][is_preferred]" class="recipient_bank_is_preferred" value="1">
			
				</div>
				
			</div>
			<!-- RECIPIENT BANK IS PREFERRED END -->
			
			<!-- RECIPIENT BANK TYPE START -->
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
			
				<div class="form-group">
				
					<label for="recipient_bank_{{hash}}"> Bank:<span style="color:#E85445;">*</span> </label>
					
					<select id="recipient_bank_{{hash}}" name="recipient_bank[{{hash}}][receive_option_list_id]" class="form-control chosen-select recipient_bank" required="required" data-hash="{{hash}}" data-placeholder="Bank">
					
						<option value=""></option>
						
						{{#each bank_types}}
						
							<option value="{{ id }}"> {{ name }} </option>
						
						{{/each}}
					
					</select>
				
				</div>
			
			</div>
			<!-- RECIPIENT BANK TYPE END -->
			
			<!-- RECIPIENT BANK ITEM REMOVE START -->
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
			
				<a href="#" class="btn btn-danger btn-xs remove_recipient_bank_item" rel="{{hash}}" style="margin:30px 0px 0px;"><i class="fa fa-trash"></i> Remove</a>
			
			</div>
			<!-- RECIPIENT BANK ITEM REMOVE END -->

		</div>
		
		<div class="row">

			<!-- RECIPIENT BANK ACCOUNT NAME START -->
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			
				<div class="form-group">
				
					<label for="recipient_account_name_{{hash}}"> Account name: </label>
					
					<input type="text" id="recipient_account_name_{{hash}}" name="recipient_bank[{{hash}}][account_name]" class="form-control recipient_account_name" placeholder="Account name">
					
				</div>
			
			</div>
			<!-- RECIPIENT BANK ACCOUNT NAME END -->

			<!-- RECIPIENT BANK ACCOUNT BRANCH START -->
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			
				<div class="form-group">
				
					<label for="recipient_account_branch_{{hash}}"> Account branch: </label>
					
					<input type="text" id="recipient_account_branch_{{hash}}" name="recipient_bank[{{hash}}][account_branch]" class="form-control recipient_account_branch" placeholder="Account branch">
					
				</div>
			
			</div>
			<!-- RECIPIENT BANK ACCOUNT BRANCH END -->
			
			<!-- RECIPIENT BANK ACCOUNT NO START -->
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			
				<div class="form-group">
				
					<label for="recipient_account_no_{{hash}}"> Account number:<span style="color:#E85445;">*</span> </label>
					
					<input type="text" id="recipient_account_no_{{hash}}" name="recipient_bank[{{hash}}][account_no]" class="form-control recipient_account_no" placeholder="Account no." required="required">
					
				</div>
			
			</div>
			<!-- RECIPIENT BANK ACCOUNT NO END -->

		</div>
		
		<!-- RECIPIENT OTHER BANK START -->
		<div id="recipient_other_bank_container_{{hash}}" class="row" style="display:none;">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="recipient_other_bank_{{hash}}"> Other bank:<span style="color:#E85445;">*</span> </label>
					
					<input type="text" id="recipient_other_bank_{{hash}}" name="recipient_bank[{{hash}}][bank_name]" class="form-control recipient_other_bank" placeholder="Other bank">
				
				</div>
			
			</div>
		
		</div>
		<!-- RECIPIENT OTHER BANK END -->
	
	</div>
	
</script>
