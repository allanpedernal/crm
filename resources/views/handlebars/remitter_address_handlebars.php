<script id="remitter_address_template" type="text/x-handlebars-template">
	
	<div id="remitter_address_item_{{hash}}" class="well">
	
		<input type="hidden" id="remitter_address_id_{{hash}}" name="remitter_address[{{hash}}][id]" value="0">
	
		<!-- REMITTER ADDRESS ITEM REMOVE START -->
		<div class="row">
	
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">

				<a href="#" class="btn btn-danger btn-xs remove_remitter_address_item" rel="{{hash}}" style="margin:0px 0px 15px;"><i class="fa fa-trash"></i> Remove</a>
				
			</div>
		
		</div>
		<!-- REMITTER ADDRESS ITEM REMOVE END -->
		
		<div class="row">
			
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
				
				<div class="checkbox" style="margin: 0px 0px 15px;">
					
					<label>
						
						<input type="checkbox" value="1" id="remitter_address_is_resident_{{hash}}" name="remitter_address[{{hash}}][is_resident]" class="remitter_address_is_resident" data-hash="{{hash}}" value="1"> Resident?

					</label>

				</div>
			
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			
				<div class="form-group">
					
					<label for="remitter_tenancy_length_id_{{hash}}" class="control-label col-lg-7 col-md-7 col-sm-12 col-xs-12"> Number of years of residency? </label>

					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
						
						<select id="remitter_tenancy_length_id_{{hash}}" name="remitter_address[{{hash}}][tenancy_length_id]" class="form-control chosen-select remitter_tenancy_frequency" data-placeholder="Years" data-hash="{{hash}}">
							
							<option value=""></option>
							
							{{#each tenancy_frequencies}}
							
								<option value="{{ id }}"> {{ name }} </option>

							{{/each}}

						</select>

					</div>

				</div>
			
			</div>

		</div>
		
		<div class="row">
		
			<!-- REMITTER ADDRESS COUNTRY START -->
			<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
			
				<div class="form-group">
				
					<label for="remitter_country_{{hash}}"> Country:<span style="color:#E85445;">*</span> </label>
					
					<select id="remitter_country_{{hash}}" name="remitter_address[{{hash}}][country]" class="form-control chosen-select remitter_country" data-hash="{{hash}}" data-placeholder="Country" required="required">
						
						<option value=""></option>
						
						{{#each countries}}
						
							<option value="{{ id }}"> {{ en_short_name }} </option>

						{{/each}}
						
					</select>
				
				</div>
			
			</div>
			<!-- REMITTER ADDRESS COUNTRY END -->
			
			<!-- REMITTER ADDRESS STATE START -->
			<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
				
				<div class="form-group">
				
					<label for="remitter_state_{{hash}}"> State:<span style="color:#E85445;">*</span> </label>
					
					<select id="remitter_state_{{hash}}" name="remitter_address[{{hash}}][state]" class="form-control chosen-select remitter_state" data-hash="{{hash}}" data-placeholder="State" required="required">
						
						<option value=""></option>

					</select>
				
				</div>
			
			</div>
			<!-- REMITTER ADDRESS STATE END -->
			
			<!-- REMITTER ADDRESS CITY START -->
			<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
				
				<div class="form-group">
				
					<label for="remitter_city_{{hash}}"> City:<span style="color:#E85445;">*</span> </label>
					
					<select id="remitter_city_{{hash}}" name="remitter_address[{{hash}}][city]" class="form-control chosen-select remitter_city" data-hash="{{hash}}" data-placeholder="City" required="required">
						
						<option value=""></option>
						
					</select>
				
				</div>
			
			</div>
			<!-- REMITTER ADDRESS CITY END -->
			
			<!-- REMITTER POSTAL START -->
			<div class="col-lg-2 col-md-2 col-sm-12 col-sm-12">
				
				<div class="form-group">
				
					<label for="remitter_postal_{{hash}}"> Postal:<span style="color:#E85445;">*</span> </label>
					
					<input type="text" id="remitter_postal_{{hash}}" name="remitter_address[{{hash}}][postal]" class="form-control remitter_postal" placeholder="Postal" required="required">
				
				</div>
			
			</div>
			<!-- REMITTER POSTAL END -->
			
			<!-- REMITTER ADDRESS IS PREFERRED START -->
			<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-left">
				
				<div class="form-group text-center">
				
					<label for="remitter_address_is_preferred_{{hash}}"> Use </label>
					
					<br>
					
					<input type="radio" id="remitter_address_is_preferred_{{hash}}" name="remitter_address[{{hash}}][is_preferred]" class="remitter_address_is_preferred" value="1">
				
				</div>
			
			</div>
			<!-- REMITTER ADDRESS IS PREFERRED END -->
		
		</div>
		
		<div class="row">
			
			<!-- REMITTER CURRENT ADDRESS START -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				
				<div class="form-group">
				
					<label for="remitter_current_address_{{hash}}"> Current address:<span style="color:#E85445;">*</span> </label>
					
					<textarea id="remitter_current_address_{{hash}}" name="remitter_address[{{hash}}][current_address]" placeholder="Current address" class="form-control" rows="2" required="required"></textarea>
				
				</div>
			
			</div>
			<!-- REMITTER CURRENT ADDRESS END -->
		
			<!-- REMITTER PERMANENT ADDRESS START -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				
				<div class="form-group">
				
					<label for="remitter_permanent_address_{{hash}}"> Permanent address:<span style="color:#E85445;">*</span> </label>
					
					<textarea id="remitter_permanent_address_{{hash}}" name="remitter_address[{{hash}}][permanent_address]" placeholder="Permanent address" class="form-control" rows="2" required="required"></textarea>
				
				</div>
			
			</div>
			<!-- REMITTER PERMANENT ADDRESS END -->
		
		</div>

	</div>

</script>

<script id="remitter_state_option_template" type="text/x-handlebars-template">
	
	<option value=""></option>
	
	{{#each states}}
	
		<option value="{{ id }}"> {{ name }} </option>
	
	{{/each}}
	
</script>

<script id="remitter_city_option_template" type="text/x-handlebars-template">
	
	<option value=""></option>
	
	{{#each cities}}
	
		<option value="{{ id }}"> {{ name }} </option>
	
	{{/each}}
	
</script>
