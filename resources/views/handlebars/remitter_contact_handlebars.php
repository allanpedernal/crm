<script id="remitter_contact_template" type="text/x-handlebars-template">
	
	<div id="remitter_contact_item_{{hash}}" class="well well-sm">
	
		<input type="hidden" id="remitter_contact_id_{{hash}}" name="remitter_contact[{{hash}}][id]" value="0">
	
		<div class="row">
			
			<!-- REMITTER CONTACT IS PREFERRED START -->
			<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 text-center">
				
				<div class="form-group">
				
					<label for="remitter_contact_is_preferred_{{hash}}"> Use </label>
					
					<br>
				
					<input type="radio" id="remitter_contact_is_preferred_{{hash}}" name="remitter_contact[{{hash}}][is_preferred]" class="remitter_contact_is_preferred" value="1">
			
				</div>
				
			</div>
			<!-- REMITTER CONTACT IS PREFERRED END -->
			
			<!-- REMITTER CONTACT TYPE START -->
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			
				<div class="form-group">
				
					<label for="remitter_contact_type_{{hash}}"> Type:<span style="color:#E85445;">*</span> </label>
					
					<select id="remitter_contact_type_{{hash}}" name="remitter_contact[{{hash}}][type]" class="form-control chosen-select remitter_contact_type" data-placeholder="Type" required="required">
					
						<option value=""></option>
						
						{{#each contact_types}}
						
							<option value="{{ id }}"> {{ name }} </option>
						
						{{/each}}
					
					</select>
				
				</div>
			
			</div>
			<!-- REMITTER CONTACT TYPE END -->
			
			<!-- REMITTER CONTACT VALUE START -->
			<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
			
				<div class="form-group">
				
					<label for="remitter_contact_value_{{hash}}"> Contact:<span style="color:#E85445;">*</span> </label>
					
					<input type="text" id="remitter_contact_value_{{hash}}" name="remitter_contact[{{hash}}][value]" class="form-control remitter_contact_value" placeholder="Contact" required="required">
					
				</div>
			
			</div>
			<!-- REMITTER CONTACT VALUE END -->

			<!-- REMITTER CONTACT ITEM REMOVE START -->
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
			
				<a href="#" class="btn btn-danger btn-xs remove_remitter_contact_item" rel="{{hash}}" style="margin:30px 0px 0px;"><i class="fa fa-trash"></i> Remove</a>
			
			</div>
			<!-- REMITTER CONTACT ITEM REMOVE END -->

		</div>
	
	</div>
	
</script>
