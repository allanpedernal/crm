<script id="remitter_identification_template" type="text/x-handlebars-template">
	
	<div id="remitter_identification_item_{{hash}}" class="well well-sm">
	
		<input type="hidden" id="remitter_identification_id_{{hash}}" name="remitter_identification[{{hash}}][id]" value="0">
	
		<div class="row">
			
			<!-- REMITTER IDENTIFICATION IS PREFERRED START -->
			<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 text-center">
				
				<div class="form-group">
				
					<label for="remitter_identification_is_preferred_{{hash}}"> Use </label>
					
					<br>
				
					<input type="radio" id="remitter_identification_is_preferred_{{hash}}" name="remitter_identification[{{hash}}][is_preferred]" class="remitter_identification_is_preferred" value="1">
			
				</div>
				
			</div>
			<!-- REMITTER IDENTIFICATION IS PREFERRED END -->
			
			<!-- REMITTER IDENTIFICATION NUMBER START -->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
			
				<div class="form-group">
				
					<label for="remitter_identification_number_{{hash}}"> Number:<span style="color:#E85445;">*</span> </label>
					
					<input type="text" id="remitter_identification_number_{{hash}}" name="remitter_identification[{{hash}}][number]" class="form-control remitter_identification_number" placeholder="Number" required="required">
				
				</div>
			
			</div>
			<!-- REMITTER IDENTIFICATION NUMBER END -->
			
			<!-- REMITTER IDENTIFICATION EXPIRATION START -->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
			
				<div class="form-group">
				
					<label for="remitter_identification_expiration_{{hash}}"> Expiration:<span style="color:#E85445;">*</span> </label>
					
					<input type="text" id="remitter_identification_expiration_{{hash}}" name="remitter_identification[{{hash}}][expiration]" class="form-control remitter_identification_expiration datepicker" placeholder="Expiration" readonly="readonly" required="required">
				
				</div>
			
			</div>
			<!-- REMITTER IDENTIFICATION EXPIRATION END -->
			
			<!-- REMITTER IDENTIFICATION TYPE START -->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
			
				<div class="form-group">
				
					<label for="remitter_identification_type_{{hash}}"> Type:<span style="color:#E85445;">*</span> </label>
					
					<select id="remitter_identification_type_{{hash}}" name="remitter_identification[{{hash}}][type]" class="form-control chosen-select remitter_identification_type" data-hash="{{hash}}" data-placeholder="Type" required="required">
					
						<option value=""></option>
						
						{{#each identification_types}}
						
							<option value="{{ id }}"> {{ name }} </option>
						
						{{/each}}
					
					</select>
					
				</div>
			
			</div>
			<!-- REMITTER IDENTIFICATION TYPE END -->
			
			<!-- REMITTER IDENTIFICATION ITEM REMOVE START -->
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
			
				<a href="#" class="btn btn-danger btn-xs remove_remitter_identification_item" rel="{{hash}}" style="margin:30px 0px 0px;"><i class="fa fa-trash"></i> Remove</a>
			
			</div>
			<!-- REMITTER IDENTIFICATION ITEM REMOVE START -->

		</div>
		
		<!-- REMITTER IDENTIFICATION OTHER TYPE START -->
		<div id="remitter_identification_other_type_container_{{hash}}" class="row" style="display:none;">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label for="remitter_identification_other_type_{{hash}}"> Other type:<span style="color:#E85445;">*</span> </label>
					
					<input type="text" id="remitter_identification_other_type_{{hash}}" name="remitter_identification[{{hash}}][other_type]" class="form-control remitter_identification_other_type" placeholder="Other type">
				
				</div>
			
			</div>
		
		</div>
		<!-- REMITTER IDENTIFICATION OTHER TYPE START -->
		
		<!-- REMITTER IDENTIFICATION FILE START -->
		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<div class="form-group">
					
					<label for="remitter_identification_file_{{hash}}"> File: </label>
					
					<div id="remitter_identification_file_{{hash}}" class="dropzone text-center" style="min-height:100px; height:auto;">
						
						<div class="dz-default dz-message">
							
							<span> Drop identification here to upload </span>

						</div>
						
					</div>

				</div>

			</div>
		
		</div>
		<!-- REMITTER IDENTIFICATION FILE END -->
	
	</div>
	
</script>
