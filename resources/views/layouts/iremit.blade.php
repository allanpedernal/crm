<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="CRM iRemit">
    
    <meta name="author" content="Allan Joseph Pedernal">
    
    <!-- CSRF TOKEN -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- TITLE -->
    <title> {{ config('app.name', 'iRemit CRM') }} </title>

	<!-- BOOTSTRAP -->
	<link href="{{ URL::asset('bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

	<!-- FONT AWESOME -->
	<link href="{{ URL::asset('bower_components/gentelella/vendors/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
	
	@yield('styles')
    
	<!-- GENTELELLA THEME STYLE -->
	<link href="{{ URL::asset('bower_components/gentelella/build/css/custom.min.css') }}" rel="stylesheet">

    <!-- SCRIPTS -->
    <script>
		
        window.Laravel = <?php echo json_encode([
        
            'csrfToken' => csrf_token(),
            
        ]); ?>
        
    </script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  
    <!--[if lt IE 9]>
    
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        
    <![endif]-->
    
</head>

  <body class="nav-md">
	  
    <div class="container body">
		
		<div class="main_container">

			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 left_col">

				<div class="left_col scroll-view">
					  
					<div class="navbar nav_title" style="border: 0;">
						
						<a href="javascript:void(0);" class="site_title">
							
							<i class="fa fa-paw"></i> <span> iRemit CRM </span>
							
						</a>
						
					</div>

					<div class="clearfix"></div>

					<!-- PROFILE INFO START -->
					<div class="profile">
						
						<div class="profile_pic">
						
							<img src="{{ URL::asset('img/img.png') }}" alt="..." class="img-circle profile_img">

						</div>

						<div class="profile_info">
							
							<span> Welcome, </span>

							<h2> {{ Auth::user()->first_name }} </h2>

						</div>

					</div>
					<!-- PROFILE INFO END -->

					<br />

					<!-- SIDE MENU START -->
					@include('elements.sidebar')
					<!-- SIDE MENU END -->
					
				</div>

			</div>

			<!-- TOP NAV START -->
			<div class="top_nav">
				
				<div class="nav_menu">
					
					<nav role="navigation">
						
						<div class="nav toggle">
							
							<a id="menu_toggle"> <i class="fa fa-bars"></i> </a>
							
						</div>

						<ul class="nav navbar-nav navbar-right">
							
							<li>
								
								<a href="javascript:void(0);" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									
									<img src="{{ URL::asset('img/img.png') }}" alt=""> {{ Auth::user()->first_name }}
									
									<span class=" fa fa-angle-down"></span>
									
								</a>
								
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									
									<li> <a href="javascript:void(0);"> Profile </a> </li>
									
									<li> <a href="javascript:void(0);"> Help </a> </li>
									
									<li> <a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out </a> </li>
									
								</ul>
								
							</li>

							<li role="presentation" class="dropdown">
								
								<a href="javascript:void(0);" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
									
									<i class="fa fa-envelope-o"></i>
									
									<span class="badge bg-green"> 4 </span>
									
								</a>
								
								<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
									
									<li>
										
										<a href="javascript:void(0);">
											
											<span class="image"> <img src="{{ URL::asset('img/img.png') }}" alt="Profile Image" /> </span>
											
											<span>
												
												<span> John Smith </span>
												
												<span class="time"> 3 mins ago </span>
												
											</span>
											
											<span class="message">
												
												Film festivals used to be do-or-die moments for movie makers. They were where...
												
											</span>
											
										</a>
										
									</li>
									
									<li>
										
										<a href="javascript:void(0);">
											
											<span class="image"> <img src="{{ URL::asset('img/img.png') }}" alt="Profile Image" /> </span>
											
											<span>
													
												<span> John Smith </span>
													
												<span class="time"> 3 mins ago </span>
													
											</span>
												
											<span class="message">
													
												Film festivals used to be do-or-die moments for movie makers. They were where...
													
											</span>
												
										</a>
										
									</li>
									
									<li>
									
										<a href="javascript:void(0);">
											
											<span class="image"> <img src="{{ URL::asset('img/img.png') }}" alt="Profile Image" /> </span>
											
											<span>
												
												<span> John Smith </span>
												
												<span class="time"> 3 mins ago </span>
												
											</span>
											
											<span class="message">
												
												Film festivals used to be do-or-die moments for movie makers. They were where...
												
											</span>
											
										</a>
								
									</li>
								
									<li>
										
										<a href="javascript:void(0);">
											
											<span class="image"> <img src="{{ URL::asset('img/img.png') }}" alt="Profile Image" /> </span>
										
											<span>
												
												<span> John Smith </span>
												
												<span class="time"> 3 mins ago </span>
												
											</span>
											
											<span class="message">
												
												Film festivals used to be do-or-die moments for movie makers. They were where...
											
											</span>
											
										</a>
										
									</li>
									
									<li>
										
										<div class="text-center">
											
											<a href="javascript:void(0);">
												
												<strong> See All Alerts </strong>
												
												<i class="fa fa-angle-right"></i>
												
											</a>
											
										</div>
									
									</li>
								
								</ul>
							
							</li>
							
						</ul>
					
					</nav>
					
				</div>
			
			</div>
			<!-- TOP NAV END -->

			<!-- PAGE CONTENT START -->
			<div class="right_col" role="main" style="padding: 0px 20px;">
				
				@yield('content')

			</div>
			<!-- PAGE CONTENT END -->

			<!-- FOOTER CONTENT START -->
			<footer>
				
				<div class="pull-right">
					
					iRemit to the Philippines Pty. Ltd. Australian Business Number 36157688394 

				</div>

				<div class="clearfix"></div>

			</footer>
			<!-- FOOTER CONTENT END -->
			
      </div>
      
    </div>
    
    <!-- JQUERY -->
    <script type="text/javascript" src="{{ URL::asset('bower_components/gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
    
     <!-- BOOTSTRAP -->
    <script type="text/javascript" src="{{ URL::asset('bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    
    <!-- NPROGRESS -->
    <script type="text/javascript" src="{{ URL::asset('bower_components/gentelella/vendors/nprogress/nprogress.js') }}"></script>

    <!-- BOOTSTRAP DATERANGERPICKER -->
    <script type="text/javascript" src="{{ URL::asset('bower_components/gentelella/production/js/moment/moment.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ URL::asset('bower_components/gentelella/production/js/datepicker/daterangepicker.js') }}"></script>
    
    @yield('scripts')
    
    <!-- GENTELELLA SCRIPT -->
    <script type="text/javascript" src="{{ URL::asset('bower_components/gentelella/build/js/custom.min.js') }}"></script>

  </body>

</html>
