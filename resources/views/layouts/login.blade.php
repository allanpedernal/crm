<!DOCTYPE html>

<html lang="en">

	<head>

		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF TOKEN -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<!-- TITLE -->
		<title>{{ config('app.name', 'iRemit CRM Login') }}</title>

		<!-- BOOTSTRAP -->
		<link href="{{ URL::asset('bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

		<!-- FONT AWESOME -->
		<link href="{{ URL::asset('/bower_components/gentelella/vendors/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">

		<!-- ANIMATE CSS -->
		<link href="{{ URL::asset('bower_components/gentelella/vendors/animate.css/animate.min.css') }}" rel="stylesheet">

		<!-- GENTELELLA THEME STYLE -->
		<link href="{{ URL::asset('bower_components/gentelella/build/css/custom.min.css') }}" rel="stylesheet">

		<!-- Scripts -->
		<script>

			window.Laravel = <?php echo json_encode([ 'csrfToken' => csrf_token() ]); ?>

		</script>

	</head>

	<body class="login">

		<div id="app">

			@yield('content')

		</div>

	</body>

</html>
