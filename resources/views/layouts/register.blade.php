<!DOCTYPE html>

<html lang="en-US" prefix="og: http://ogp.me/ns#">

<head>
	
	<title>Transfer Money To The Philippines | iRemit to the Philippines</title>
	
	@yield( 'meta' )
	
 	@yield( 'css' )

	@yield( 'scripts' ) 
	
</head>

<body class="page-template page-template-iremit-steps-page page-template-iremit-steps-page-php page page-id-2125">
	
	@yield('navigational-bar')
	
	<!-- CONTAINER FLUID START -->
	<div class="wrapper">

@yield( 'isloading-css' )

<div id="content" class="container">

	<div class="row">
	
		<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
			
			<img src="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/images/steps-header.png" style="margin:0px 0px 5px;">
		
		</div>
		
		<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 hidden-sm hidden-xs">
		
						
				<div class="allogin">
				
					<h6 class="text-right" style="margin:35px 0px 25px;"> Already an iRemit member? <a href="/login/"> Login </a> here </h6>
				
				</div>
			
						
		</div>
		
	</div>
	
	<div class="row">

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<div class="well" style="padding:15px; margin-bottom:0px;">

				<div class="page_container" style="padding:15px; background:#ffffff; height:auto;">

									
						<marquee behavior="alternate" direction="left" scrollamount="5"><p style="color:#ff0000; font-weight: 500; margin:0px;">
													</p></marquee>	
				
									
					<div class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						
							@yield( 'remitter-fill-up-form' )
			
						</div>
						
					</div>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
</div>



	
	</div>
	<!-- CONTAINER FLUID END -->
	
	<!-- FOOT RATE START -->
	<div id="footer_rate" class="hidden-md hidden-sm hidden-xs">

		$1 AUD = &#x20B1; 38.30 PHP
		
		<br>
		
		
		
	</div>
	<!-- FOOT RATE END -->
	
	<!-- FOOTER START -->
	<footer class="footer">
		
		<div class="footer-content-container" style="padding: 20px 0px;">
			
			<div class="container">
			
				<div class="row">
				
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						
						<div class="helpful_links_container">
							
							<h3 style="color:#6c6c6c; font-weight:700; font-size:14px;"> Helpful Links </h3>
							
							<ul id="footer_nav" class="list-unstyled"><li id="menu-item-2241" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2125 current_page_item menu-item-2241"><a href="http://103.254.139.202/v2/steps/">Send Money Now</a></li>
<li id="menu-item-1634" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1634"><a href="http://103.254.139.202/v2/testimonials-from-satisfied-clients/">Testimonials</a></li>
<li id="menu-item-1537" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1537"><a href="http://103.254.139.202/v2/terms-of-use/">Terms of Use</a></li>
<li id="menu-item-1536" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1536"><a href="http://103.254.139.202/v2/privacy-policy/">Privacy Policy</a></li>
<li id="menu-item-1530" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1530"><a href="http://103.254.139.202/v2/contact-us/">Contact Us</a></li>
</ul>							
							<!--
							<ul class="list-unstyled">
							
								<li> <a href="#" style="color:#737373; font-size:12px;"> About Us </a> </li>
								
								<li> <a href="#" style="color:#737373; font-size:12px;"> How Does it Works? </a> </li>
								
								<li> <a href="#" style="color:#737373; font-size:12px;"> Contact Us </a> </li>
								
								<li> <a href="#" style="color:#737373; font-size:12px;"> Blog </a> </li>
								
								<li> <a href="#" style="color:#737373; font-size:12px;"> Terms of Use </a> </li>
								
								<li> <a href="#" style="color:#737373; font-size:12px;"> Privacy Policy </a> </li>
							
							</ul>
							-->
					
						</div>
					
						<hr class="hidden-lg hidden-md visible-sm visible-xs">

					</div>
					
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						
						<div class="contact_information_container">
							
							<h3 style="color:#6c6c6c; font-weight:700; font-size:14px;"> Contact Information </h3>
							
							<address style="color:#737373; font-size:13px; line-height: 24px;">
								
								Call: <a href="tel:0283552776" style="color:#737373;">(02) 8355 2776</a> <br>

								SMS: (0438) 593 376 <br>

								Email: <a href="mail:support@iremit.com.au" style="color:#737373;">support@iremit.com.au</a> <br>

								Skype: <a href="skype:iRemitAustralia?call" style="color:#737373;">iRemitAustralia</a>

							</address>
						
						</div>
					
						<hr class="hidden-lg hidden-md visible-sm visible-xs">
						
					</div>
					
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						
						<div class="socila_network_container">
							
							<h3 style="color:#6c6c6c; font-weight:700; font-size:14px;"> Social Networks </h3>
							
							<div class="footer-social-icons">
								
								<ul class="social-icons" style="padding:0px;">
									
									<li> <a href="https://www.facebook.com/iRemitAustralia" class="social-icon" target="_blank"> <i class="fa fa-facebook"></i> </a> </li>

									<li> <a href="https://twitter.com/@iremitaustralia" class="social-icon" target="_blank"> <i class="fa fa-twitter"></i> </a> </li>

									<li> <a href="https://plus.google.com/+IremitAu/posts" class="social-icon" target="_blank"> <i class="fa fa-google-plus"></i> </a> </li>
									
									<li> <a href="#" class="social-icon" target="_blank"> <i class="fa fa-instagram"></i> </a> </li>
									
									<li> <a href="https://www.linkedin.com/company/iremit-to-the-philippines" class="social-icon" target="_blank"> <i class="fa fa-linkedin"></i> </a> </li>

								</ul>

							</div>
							
							<ul class="list-unstyled">
								
								<li>
								
									<div class="row">
										
										<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
									
											<img src="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/images/austrac-logo.png" class="img-responsive" style="margin:15px auto; max-width:185px;">
										
										</div>
									
									</div>
								
								</li>
								
								<li>
									
									<div class="row">
									
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
											<script src="//secure.comodo.net/trustlogo/javascript/trustlogo.js" type="text/javascript"></script>

											<script type="text/javascript"> TrustLogo("//www.mysecuressls.com/images/seals/crazy_secure_01.png", "SC5");</script>

										</div>
									
									</div>
								
								</li>
							
							</ul>

						</div>

						<hr class="hidden-lg hidden-md visible-sm visible-xs">
						
					</div>

					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						
						<div class="iremit_logo_container">

							<div class="row">

								<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text-center">

									<a href="http://103.254.139.202/v2">

										<img src="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/images/grey-logo.png" class="img-responsive" style="margin: 0px auto; max-width:150px;">

									</a>

								</div>

							</div>
							
							<div class="row">
								
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
									<p style="margin: 20px 0px 10px; line-height: 15px; color:#737373;"> 
										
										<small style="font-size:75%;">
											
											iRemit to the Philippines Pty. Ltd <br> 
											
											Australian Remittance Company <br> 
											
											Australian Company Number: 157 688 394 <br> 
											
											AUSTRAC Remittance Sector Registry Independent <br>
											
											Remittance Dealer: IND100350163-0014
										
										</small>
										
									</p>
									
								</div>
								
							</div>
							
							<div class="row">
								
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								
									<h3 style="color:#6c6c6c; font-weight:700; font-size:14px;"> Download iRemit App </h3>
									
									<ul class="list-inline">
										
										<li> 
											
											<a href="https://play.google.com/store/apps/details?id=au.com.iremit.iremitapp" target="blank">
												
												<img src="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/images/google-play.png" class="img-responsive" style="width:100px; height:30px;">
											
											</a>
											
										 </li>
									
										<li> 
											
											<a href="https://geo.itunes.apple.com/au/app/iremit/id1122346806?mt=8" target="blank">
											
												<img src="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/images/apple-store.png" class="img-responsive" style="width:100px; height:30px;"> 
											
											</a>
											
										</li>
									
									</ul>
								
								</div>
							
							</div>
							
						</div>
						
					</div>
				
				</div>
			
			</div>
			
		</div>
		
		<div class="copyright-container" style="background-color: #e5e5e5; padding: 25px 0px; border-top: 1px solid #efefef;">
		
			<div class="container">
				
				<p class="text-muted text-center" style="font-size: 13px; letter-spacing:.5px; margin: 0px;"> &copy; Copyright 2017 iRemit to the Philippines Pty. Ltd. All Rights Reserved </p>

			</div>
		
		</div>

	</footer>
	<!-- FOOTER END -->
	
	<!-- SMS VERIFICATION MODAL START -->
	<div id="mobile_verification_modal" class="modal fade" tabindex="-1" role="dialog">
	
		<div class="modal-dialog" role="document">
		
			<div class="modal-content">
			
				<div class="modal-header">
				
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true"> &times; </span> </button>
					
					<h4 class="modal-title"> Mobile Verification Form </h4>
					
				</div>
				
				<div class="modal-body">
				
					<input type="hidden" id="remitter_id" name="remitter_id" class="form-control">
				
					<div id="sms_verification_code_container" class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						
							<div class="form-group">
							
								<label for="sms_verification_code"> Please enter your mobile verification code: </label>
							
								<div class="input-group">
								
									<span class="input-group-btn">
									
										<button id="send_sms_code_btn" class="btn btn-info btn-sm" type="button" style="padding:10px 12px;"> Resend Code </button>
										
									</span>
								
									<input type="text" id="sms_verification_code" name="sms_verification_code" value="" class="form-control" placeholder="SMS verification code" style="height:40px !important;">
									
									<span class="input-group-btn">
									
										<button id="verify_sms_code_btn" class="btn btn-primary btn-sm" type="button" style="padding:10px 12px;"> Verify Code </button>
										
									</span>
								
								</div>

							</div>
						
						</div>
					
					</div>
					
				</div>
				
				<div class="modal-footer">
				
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"> Close </button>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	<!-- SMS VERIFICATION MODAL END -->

</body>

</html> 