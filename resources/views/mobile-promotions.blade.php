@extends('layouts.iremit')

@section('content')

	<div class="page-title">
		
		<div class="title_left">
			
			<h3> <i class="fa fa-bullhorn"></i> Promotions </h3>
			
		</div>
		
	</div>

	<div class="clearfix"></div>

	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
			<div class="x_panel">
				
				<div class="x_title">
					
					<a href="{{ url('mobile/promotions/modal') }}" class="btn btn-primary btn-xs show_modal_btn" style="margin: 5px 0px;"> <i class="fa fa-plus"></i> Add New Promotion </a>
					
					<ul class="nav navbar-right panel_toolbox">
						
						<li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
						
						<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> </li>
						
						<li> <a class="close-link"> <i class="fa fa-close"></i> </a> </li>
					
					</ul>
					
					<div class="clearfix"></div>
					
				</div>
				
				<div class="x_content">
					
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 5px;">
						
						<div class="panel panel-default">
							
							<div class="panel-heading" role="tab" id="search_remittance">
								
								<h4 class="panel-title">
									
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="<?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'true' : 'false' ); ?>" aria-controls="collapseOne" <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? '' : 'class="collapsed"' ); ?>>

										<small> Click me to show search </small>

									</a>

								</h4>

							</div>

							<div id="collapseOne" class="panel-collapse collapse <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'in' : '' ); ?>" role="tabpanel" aria-labelledby="search_sms_code">

								<div class="panel-body">
									
									<form method="GET" id="search_remittance_form" class="form-horizontal">

										<div class="row">
											
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
												
												<div class="form-group">

													<label for="search_mobile_promotion_excerpt"> Excerpt: </label>

													<input type="text" id="search_mobile_promotion_excerpt" name="search_mobile_promotion_excerpt" class="form-control" placeholder="Excerpt" value="<?php echo ( isset( $_GET[ 'search_mobile_promotion_excerpt' ] ) && ! empty( $_GET[ 'search_mobile_promotion_excerpt' ] ) ? $_GET[ 'search_mobile_promotion_excerpt' ] : '' ); ?>">

												</div>
													
											</div>

											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
												
												<div class="form-group">

													<label for="search_mobile_promotion_send_times"> Send times: </label>
													
													<input type="text" id="search_mobile_promotion_send_times" name="search_mobile_promotion_send_times" class="form-control" placeholder="Send times" value="<?php echo ( isset( $_GET[ 'search_mobile_promotion_send_times' ] ) && ! empty( $_GET[ 'search_mobile_promotion_send_times' ] ) ? $_GET[ 'search_mobile_promotion_send_times' ] : '' ); ?>">

												</div>
													
											</div>

											<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
											
												<div class="form-group text-center">
												
													<label> 
														
														<small>
														
															<i class="fa fa-check"></i> is send: 
														
															<br>
														
															<input type="checkbox" id="search_mobile_promotion_is_send" name="search_mobile_promotion_is_send" style="margin:15px 0px 0px;" <?php echo ( isset( $_GET[ 'search_mobile_promotion_is_send' ] ) && ! empty( $_GET[ 'search_mobile_promotion_is_send' ] ) ? 'checked="checked"' : '' ); ?> value="1">
															
														</small>
														
													</label>
													
												</div>
											
											</div>
											
											<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
												
												<div class="row">
													
													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
													
														<div class="form-group">
														
															<label for="search_sms_code_start_date"> SMS start date: </label>
															
															<div class="input-prepend input-group" style="margin:0px;">

																<span class="add-on input-group-addon"> <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> </span>

																<input type="text" class="form-control datepicker" id="search_sms_code_start_date" name="search_sms_code_start_date" placeholder="Start date" value="<?php echo ( isset( $_GET[ 'search_sms_code_start_date' ] ) && ! empty( $_GET[ 'search_sms_code_start_date' ] ) ? $_GET[ 'search_sms_code_start_date' ] : '' ); ?>">

															</div>
															
														</div>
													
													</div>
													
													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
													
														<div class="form-group">
														
															<label for="search_remittance_transaction_end_date"> SMS end date: </label>
															
															<div class="input-prepend input-group" style="margin:0px;">

																<span class="add-on input-group-addon"> <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> </span>

																<input type="text" class="form-control datepicker" id="search_sms_code_end_date" name="search_sms_code_end_date" placeholder="End date" value="<?php echo ( isset( $_GET[ 'search_sms_code_end_date' ] ) && ! empty( $_GET[ 'search_sms_code_end_date' ] ) ? $_GET[ 'search_sms_code_end_date' ] : '' ); ?>">

															</div>
															
														</div>
													
													</div>
												
												</div>

											</div>
											
										</div>

										<div class="row">

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

												<a href="{{ url( 'mobile/promotions' ) }}" class="btn btn-default btn-sm"> <i class="fa fa-refresh" aria-hidden="true"></i> Reset </a>

												<button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> Search </button>

											</div>

										</div>

									</form>
									
								</div>

							</div>

						</div>

					</div>

					@include( 'tables.mobile_promotion_table' )

				</div>
			
			</div>
			
		</div>
		
	</div>

@endsection

@section('styles')

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('bower_components/gentelella/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('css/chosen.min.css') }}" rel="stylesheet">
    
	<style type="text/css">
	
		/*IS LOADING PLUGIN*/
		.isloading-wrapper.isloading-right{ margin-left:10px; }

		.isloading-overlay{ position:relative;text-align:center; }

		.isloading-overlay .isloading-wrapper {

			line-height: 20px; 
			
			background:#ffffff; 
			
			-webkit-border-radius:7px; 
			
			-webkit-background-clip:padding-box; 
			
			-moz-border-radius:7px; 
			
			-moz-background-clip:padding;
			
			border-radius:7px;
			
			background-clip:padding-box;
			
			display:inline-block;
			
			margin:0 auto;
			
			padding:10px 20px;
			
			z-index:9000;

		}
		
		.pagination{ margin: 10px 0px 5px; }
		
		.help-block { color: #E74C3C; }
		
		.input-group-addon{ padding: 3px; }
		
	</style>

@endsection

@section('scripts')


	<script src="{{ URL::asset('bower_components/gentelella/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
	
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/iCheck/icheck.min.js') }}"></script>
    
	<script src="{{ URL::asset('js/jquery.isloading.min.js') }}"></script>
	
	<script src="{{ URL::asset('js/chosen.jquery.min.js') }}"></script>
	
	<script type="text/javascript">
		
		jQuery( document ).ready( function() {

			//INIT ALL
			init_all();


			//SHOW MODAL
			jQuery( document ).on( 'click', '.show_modal_btn', function( e ) {
				
				e.preventDefault();
				
				var href = jQuery( this ).attr( 'href' );
				
				//SHOW LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Loading',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );
				
				//CALL SMS CODE AJAX
				jQuery.get( href, function( response ) {
					
					if( response.success ) {
						
						//GET ACTION
						var action = response.result.action;
						
						//APPEND AJAX MODAL
						jQuery( 'body' ).append( response.result.content );
						
						//OPEN MODAL
						jQuery( '#mobile_promotion_modal' ).modal( { backdrop: 'static', keyboard: false } );
						
						//REINITIALIZE ALL
						init_all();

					}
					
					else {
						
						//SHOW SOME ERROR MESSAGE
						alert( response.error_message );
						
					}
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
				}, 'json' );

			} );
			
			//REMOVE SMS CODE MODAL
			jQuery( document ).on( 'hidden.bs.modal', '#mobile_promotion_modal', function( e ) {
				
				jQuery( this ).remove();

				jQuery( '.modal-backdrop' ).remove();

				jQuery( 'body' ).removeClass( 'modal-open' );
				
			} );


			//CHECK ALL MOBILE PROMOTION CHECKBOX
			jQuery( document ).on( 'ifChanged', '#select_all_mobile_promotions', function() {
					
				//CHECK IF MOBILE PROMOTION SELECT ALL IS NOT CHECKED
				if ( ! jQuery( this ).is( ':checked' ) ) {
					
					//REMOVE ALL CHECKBOX CHECKED ATTR
					jQuery( '.mobile_promotion_checkbox' ).prop( 'checked', false ).iCheck( 'update' );

					//REMOVE CLASS SELECTED TO ALL TR
					jQuery( '.jambo_table tbody tr' ).removeClass( 'selected' );

				} 

				else {
					
					//ADD ALL CHECKBOX CHECKED ATTR
					jQuery( '.mobile_promotion_checkbox' ).prop( 'checked', true ).iCheck( 'update' );
					
					//ADD CLASS SELECTED TO ALL TR
					jQuery( '.jambo_table tbody tr' ).addClass( 'selected' );
				
				} 
				  
			} );
			
			//CHECK MOBILE PROMOTION CHECKBOX
			jQuery( document ).on( 'ifChanged', '.mobile_promotion_checkbox', function() {
				
				var all = jQuery( '.mobile_promotion_checkbox' );
				
				//CHECK IF ALL MOBILE PROMOTION CHECKBOX IS CHECKED
				if ( all.length === all.filter( ':checked' ).length ) {
					
					//CHECK SELECT ALL CHECKBOX
					jQuery( '#select_all_mobile_promotions' ).prop( 'checked', true ).iCheck( 'update' );

				}

				else {
					
					//REMOVE CHECK SELECT ALL CHECKBOX
					jQuery( '#select_all_mobile_promotions' ).prop( 'checked', false ).iCheck( 'update' );

				}

			} );


			//SEND PROMOTION
			jQuery( document ).on( 'click', '.send_btn', function( e ) {
				
				e.preventDefault();
				
				var answer = confirm( 'Do you want to send this promotion to all mobile devices?' );
				
				if( answer ) {
					
					var href = jQuery( this ).attr( 'href' );
					
					//SHOW LOADING
					jQuery( 'body' ).isLoading( {
						
						text: 'Loading',
						
						class: 'fa fa fa-spinner fa-spin',
						
						position: 'overlay',
						
						transparency: 0.5
						
					} );
					
					//CALL SMS CODE AJAX
					jQuery.get( href, function( response ) {
						
						if( response.success ) {
							
							//ADD NEW RECORD
							if( response.result.action == 'send' ) {
								
								new PNotify( {
									
									title: 'Success!',

									text: 'Promotion has been sent to all mobile devices!',

									type: 'success',
									
									styling: 'bootstrap3'

								} );

							}
							
							//UPDATE SMS CODE TABLE
							jQuery( '.mobile_promotion_table_container' ).html( response.result.mobile_promotion_table_template );
							
							//INIT ALL
							init_all();

						}
						
						else {
							
							//SHOW SOME ERROR MESSAGE
							alert( response.error_message );
							
						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
					}, 'json' );
					
				}
				
			} );
			
			//SUBMIT SMS CODE FORM
			jQuery( document ).on( 'submit', '#mobile_promotion_form', function( e ) {
				
				e.preventDefault();
				
				//CONFIRM ANSWER
				var answer = confirm( 'Do you want to submit this mobile promotion form?' );
				
				//IF YES
				if( answer ) {
					
					//SHOW LOADING
					jQuery( 'body' ).isLoading( {
						
						text: 'Submitting form',
						
						class: 'fa fa fa-spinner fa-spin',
						
						position: 'overlay',
						
						transparency: 0.5
						
					} );
					
					//GET DATA
					var data = jQuery( this ).serializeArray();
					
					data.push( { name: '_token', value: '{{ csrf_token() }}' } );
					
					//AJAX FORM DATA
					jQuery.post( '{{ url( "/mobile/promotions/form/post" ) }}', data, function( response ) {
					
						//FORM SUBMIT SUCCESSFUL
						if( response.success ) {

							//GET MOBILE PROMOTION ID
							var mobile_promotion_id = response.result.mobile_promotion_id;

							//ADD NEW RECORD
							if( response.result.action == 'add' ) {
								
								new PNotify( {
									
									title: 'Success!',

									text: 'Mobile promotion has been added!',

									type: 'success',
									
									styling: 'bootstrap3'

								} );

							}

							//EDIT RECORD
							if( response.result.action == 'edit' ) {
								
								new PNotify( {
									
									title: 'Success!',

									text: 'Mobile promotion has been updated!',
									
									styling: 'bootstrap3'

								} );

							}
							
							//UPDATE MOBILE PROMOTION TABLE
							jQuery( '.mobile_promotion_table_container' ).html( response.result.mobile_promotion_table_template );

							//HIDE MODAL
							jQuery( '#mobile_promotion_modal' ).modal( 'hide' );
							
							//INIT ALL
							init_all();

						}

						else {

							//SHOW SOME ERROR MESSAGE
							alert( response.error_message );

						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
					}, 'json' );
				
				}
				
				
			} );

			//DELETE MOBILE PROMOTION
			jQuery( document ).on( 'submit', '#mobile_promotion_table', function( e ) {
				
				e.preventDefault();
				
				var total_checked_mobile_promotions = jQuery( 'input[name="mobile_promotion[]"]:checked' ).length;
				
				if( total_checked_mobile_promotions ) {
					
					var answer_text = 'Do you want to delete this mobile promotion' + ( total_checked_mobile_promotions > 1 ? 's' : '' );
					
					var answer = confirm( answer_text + '?' );
					
					if( answer ) {
						
						var data = jQuery( this ).serializeArray();
						
						data.push( { name: '_token', value: '{{ csrf_token() }}' } );
						
						jQuery.post( '{{ url( "/mobile/promotions/delete-promotions" ) }}', data, function( response ) {
							
							//FORM SUBMIT SUCCESSFUL
							if( response.success ) {
								
								if( response.result.action == 'delete' ) {
									
									var pnotify_text = 'Mobile promotion' + ( response.result.total_code > 1 ? 's' : '' ) + ' has been deleted!';
									
									new PNotify( {
										
										title: 'Success!',

										text: pnotify_text + '!',

										type: 'error',
										
										styling: 'bootstrap3'

									} );

								}

								//UPDATE MOBILE PROMOTION TABLE
								jQuery( '.mobile_promotion_table_container' ).html( response.result.mobile_promotion_table_template );

								//HIDE MODAL
								jQuery( '#mobile_promotion_modal' ).modal( 'hide' );
								
								//INIT ALL
								init_all();
								
							}

							else {

								//SHOW SOME ERROR MESSAGE
								alert( response.error_message );

							}
							
						} );
						
					}
				
				}
				
				else {
					
					alert( 'Please select mobile promotion that you want to delete!' );
					
				}
				
			} );


		} );

		//INIT ALL
		function init_all() {
			
			//INIT DATEPICKER
			init_datepicker();
			
			//INIT CHOSEN
			init_chosen();
		
			//INIT PARSELEY
			init_parseley();
			
			//REINITIALIZE ICHECK EVENT
			reinitialize_icheck_event();
			
		}
		
		//INIT DATEPICKER
		function init_datepicker() {
			
			//INITIALIZE DATEPICKER
			jQuery( '.datepicker' ).daterangepicker( {
				
				singleDatePicker: true,
				
				showDropdowns: true,
				
				format: 'YYYY-MM-DD'
				
			} );
		
		}
		
		//INIT CHOSEN
		function init_chosen() {
			
			var config = {
				
				'.chosen-select'           : { width: '100%', allow_single_deselect: true },
				
				'.chosen-select-deselect'  : { allow_single_deselect: true },
				
				'.chosen-select-no-single' : { disable_search_threshold: 10 },
				
				'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
				
				'.chosen-select-rtl'       : { rtl: true },
				
				'.chosen-select-width'     : { width: '100%' }
				
			}

			for ( var selector in config ) {
				
				jQuery( selector ).chosen( config[ selector ] );

			}
			
		}

		//INIT PARSELEY
		function init_parseley() {
			
			jQuery( '#mobile_promotion_form' ).parsley( {
				
				errorsContainer: function(el) {
					
					return el.$element.closest( '.form-group' );
					
				},

				errorsWrapper: '<span class="help-block"></span>',

				errorTemplate: '<span></span>',

				errorMessage: 'Required'
				
			} );
			
		}

		//ON VALIDATE
		function on_validate() {

			if( ! jQuery( '#mobile_promotion_form' ).parsley().validate() ) {
				
				return false;

			}

			return true;

		}


		//REINITIALIZE ICHECK EVENT
		function reinitialize_icheck_event() {
			
			jQuery( document ).ready( function() {
			
				jQuery( "input.flat" )[ 0 ] && jQuery( document ).ready( function() {
					
					jQuery( "input.flat" ).iCheck( {
						
						checkboxClass: "icheckbox_flat-green",
						
						radioClass: "iradio_flat-green"
						
					} );
					
					jQuery( "table input" ).on( "ifChecked", function() { checkState = "", jQuery( this ).parent().parent().parent().addClass( "selected" ), countChecked() } ); 
					
					jQuery( "table input" ).on( "ifUnchecked", function() { checkState = "", jQuery( this ).parent().parent().parent().removeClass( "selected" ), countChecked() } );
					
				} );
				
			} );
			
		}


	</script>


@endsection
