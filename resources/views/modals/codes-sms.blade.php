<div id="sms_code_modal" class="modal fade" tabindex="-1" role="dialog">
		
	<div class="modal-dialog modal-sm" role="document">
		
		<div class="modal-content">
			
			<form method="POST" action="{{ URL('codes/sms/form/post') }}" id="sms_code_form" class="form-horizontal" data-validate="parsley" onSubmit="return on_validate();">
				
				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						
						<span aria-hidden="true"> &times; </span>
						
					</button>

					<h4 class="modal-title"> <i class="fa fa-wpforms"></i> SMS Form </h4>

				</div>

				<div class="modal-body">
					
					<!-- SMS CODE ID START -->
					<input type="hidden" name="sms_code_id" id="sms_code_id" class="form-control" value="{{ ( isset( $sms_code_id ) && ! empty( $sms_code_id ) ? $sms_code_id : 0 ) }}">
					<!-- SMS CODE ID END -->

					<div class="row">
						
						<!-- SMS CODE REMITTER START -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<div class="form-group">
								
								<label for="sms_code_remitter"> Remitter: </label>

								<select id="sms_code_remitter" name="sms_code_remitter" class="form-control chosen-select" required="required" data-placeholder="Select remitter" {{ ($action == 'edit' ? 'disabled="disabled"' : '' ) }}>
								
									<option value=""></option>
									
									@if( isset( $remitter ) AND ! empty( $remitter ) )
									
										<option value="{{ $remitter[ 'id' ] }}" selected="selected"> {{ $remitter[ 'fullname' ] }} | {{ $remitter[ 'email' ] }} </option>
									
									@endif
								
								</select>
								
							</div>
							
						</div>
						<!-- SMS REMITTER END -->
						
						<!-- SMS CODE START -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<div class="form-group">
								
								<label for="sms_code"> Code: </label>
								
								<div class="input-group">
									
									<span class="input-group-btn">
										
										<button class="btn btn-primary" type="button" onclick="jQuery( '#sms_code' ).val( Math.random().toString( 36 ).substr( 2, 8 ) );"> Generate </button>

									</span>
									
									<input type="text" class="form-control" id="sms_code" name="sms_code" placeholder="Code" required="required" readonly="readonly" value="{{ ( $action == 'new' ? '' : $sms_code_details[ 'code' ] ) }}">

								</div>

							</div>
							
						</div>
						<!-- SMS CODE END -->		
					
						<!-- SMS CODE TRIES START -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<div class="form-group">
								
								<label for="sms_code_tries"> Tries: </label>

								<input type="text" class="form-control" id="sms_code_tries" name="sms_code_tries" placeholder="Tries" required="required" readonly="readonly" value="{{ ( $action == 'new' ? 0 : $sms_code_details[ 'count' ] ) }}">

							</div>
							
						</div>
						<!-- SMS CODE TRIES END -->	
						
					</div>
							
				</div>
				
				<div class="modal-footer">
					
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" style="margin:0px;"> <i class="fa fa-times" aria-hidden="true"></i> Close </button>

					<button type="submit" class="btn btn-sm btn-primary" style="margin:0px;"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save </button>
					
				</div>

			</form>
			
		</div>
		
	</div>
	
</div>
