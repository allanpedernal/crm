<!-- CLASSIFICATION START -->

<div class="row">

	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

		<div class="form-group">

			<label for="usage_classification"> Classification: </label>

			@include('modals/Form/classification')
			
		</div>	

	</div>

	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		
		<div class="form-group">
			
			<label for="usage_type"> Type: </label>

			@include('modals/Form/type')
			
		</div>

	</div>

</div>

<!-- CLASSIFICATION END -->
