<!-- TOTAL COSUMBALE START -->

<div id="promo_code_total_consumable_container" class="row" style="display:none;">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	
		<div class="form-group">
		
			<label for="total_consumable"> Total consumable: </label>

			@include('modals/Form/consumable')
		
		</div>
	
	</div>

</div>

<!-- TOTAL COSUMBALE END -->
