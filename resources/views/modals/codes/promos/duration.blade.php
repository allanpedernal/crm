<!-- DURATION START -->
<div class="row">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	
		<div class="form-group">
			
			<label for="duration"> Duration: </label>

			<div class="row">
			
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
					@include('modals/Form/duration')
				
				</div>
			
			</div>
			
		</div>
	
	</div>

</div>
<!-- DURATION END -->
