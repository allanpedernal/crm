<!-- PROMO CODE FORM START -->
<div id="promo_code_modal" class="modal fade" tabindex="-1" role="dialog">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">
		
			<form class="promocode" method="POST">			

				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						
						<span aria-hidden="true"> &times; </span>
						
					</button>
					
					<h4 class="modal-title"> <i class="fa fa-wpforms"></i> Promo code Form </h4>

				</div>

				<div class="modal-body">
				
					<input type="hidden" name="promo_code_id" id="promo_code_id" class="form-control" value="0">

					<div class="row">

						@include('modals.codes.promos.usage-details-layout')

						@include('modals.codes.promos.promo-details-layout')

					</div>	
				
				</div>

				<div class="modal-footer">
					
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" style="margin:0px;"> <i class="fa fa-times" aria-hidden="true"></i> Close </button>

					<button id="promo_code_btn" type="submit" class="btn btn-sm btn-primary" style="margin:0px;"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save </button>
					
				</div>
			
			</form>

		</div>

	</div>

</div>
<!-- PROMO CODE FORM END -->

{{-- <div id="promo_code_modal" class="modal fade">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">

			{!! Form::open(['method' => 'POST', 'action' => 'PromoCodesController@postForm', 'class' => 'form-horizontal']) !!}

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title">Promo code Form</h4>

			</div>

			<div class="modal-body">

				{!! Form::hidden('value', '0', ['class'=>'form-control', 'name'=>'promo_code_id','id'=>'promo_code_id'] )!!}
				
				@include('modals.codes.promos.usage-details-layout')

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
				<button type="sumbit" id="promo_code_btn" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save changes</button>
			</div>

			{!! Form::close() !!}

		</div>
	</div>
</div> --}}

