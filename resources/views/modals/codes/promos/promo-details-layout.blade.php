<!-- PROMO CODE DETAILS START -->
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

	<h4> Details </h4>

	<div class="well">
	
		<!-- NAME START -->
		<div class="row">
		
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label> Name: </label>
					
					<input type="text" name="promo_code" id="promo_code" class="form-control" value="" placeholder="Name">
				
				</div>
			
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label> Discounted value: </label>
					
					<input type="text" name="discounted_value" id="discounted_value" class="form-control" value="" placeholder="Discounted value">
				
				</div>
			
			</div>
			
		</div>
		<!-- NAME END -->
		
		<!-- DESCRIPTION START -->
		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label> Description: </label>
					
					<textarea id="description" name="description" rows="3" class="form-control" placeholder="Description" style="max-width:100%; min-width:100%; max-height:100px; min-height:100px;"></textarea>
				
				</div>
			
			</div>
			
		</div>
		<!-- DESCRIPTION END -->
		
	</div>

</div>
<!-- PROMO CODE DETAILS END -->