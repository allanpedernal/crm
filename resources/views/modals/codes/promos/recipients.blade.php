<!-- RECIPIENT START -->
<div id="promo_code_recipient_container" class="row" style="display:none;">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	
		<div class="form-group">

			<label for="recipient"> Recipients: </label>
		
			@include('modals/Form/recipients')
			
		</div>
	
	</div>

</div>
<!-- RECIPIENT END -->
