<!-- USAGE DETAILS START -->

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

	<h4> Usage Details </h4>

	<div class="well">
			
		@include('modals.codes.promos.classification')

		@include('modals.codes.promos.consumable')

		@include('modals.codes.promos.user-action')

		@include('modals.codes.promos.user')

		@include('modals.codes.promos.recipients')

		@include('modals.codes.promos.user-classification')

		@include('modals.codes.promos.duration')

		@include('modals.codes.promos.expiration')

    </div>	

</div>


<!-- USAGE DETAILS END -->