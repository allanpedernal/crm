<!-- USER ACTION CONTAINER START-->

<div id="user_action_container" style="display:none;">

	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		
			<div class="form-group">

				<label> Action: </label>
	
				@include('modals/Form/user-action')
				
			</div>
			
		</div>
	
	</div>

</div>

<!-- USER ACTION CONTAINER END-->
