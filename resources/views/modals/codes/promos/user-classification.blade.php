<!-- USER CLASSIFICATION START -->
<div id="user_classification_container" class="row" style="display:none;">

	<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	
		<div class="form-group">
		
			<label id="user_classification" for="user_classification"> User classification: </label>

			@include('modals/Form/user-classification')
		
		</div>
	
	</div>
	
	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

		<label style="margin:35px 0px 15px;">

			@include('modals/Form/chkbox-user-classification')

		</label>
	
	</div>

</div>
<!-- USER END -->
