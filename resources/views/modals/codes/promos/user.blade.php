<!-- USER START -->
	<div id="user_container">
		
		<div class="row">
		
			<div id="remitter_container" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<div class="form-group">
				
					<label id="user_label" for="user"> Remitters: </label>

					@include('modals/Form/user')
				
				</div>
			
			</div>
			
			<div id="all_user_container" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="display:none;">

				<label style="margin:25px 0px 10px;">

					@include('modals/Form/chkbox-all-user')

				</label>
			
			</div>
		
		</div>

	</div>
<!-- USER END -->
