@if( isset( $recipient_details ) && ! empty( $recipient_details ) )

	<div class="panel panel-default" style="border-radius: 0px; margin-top: 70px; border: 3px solid transparent; border-color: #ccc; margin-bottom: 0px;">

		<div class="panel-body">
			
			<div class="row">

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<div class="text-center" style="margin-top: -80px;">

						<img src="{{ URL::asset('img/img.png') }}" class="img-circle" style="width: 125px; height: 125px; border: 5px solid #ccc;">

						<p style="margin: 15px 0px 5px;"> <span class="label label-primary"> <small> {{ $recipient_details -> id_number }} </small> </span> </p>
						
						@if( $recipient_details -> type == 'person' )
						
							<h5 style="margin: 0px;"> {{ $recipient_person_details -> firstname }} {{ $recipient_person_details -> middlename }} {{ $recipient_person_details -> lastname }} </h5>
						
						@else 
						
							<h5 style="margin: 0px;"> {{ $recipient_company_details -> company_name }} </h5>
						
						@endif
						
						<p style="margin: 0px 0px 15px;"> <small> {{ $recipient_details -> email }} </small> </p>
						
					</div>
					
					<div class="accordion" id="recipient_accordion" role="tablist" aria-multiselectable="true">

						<!-- RECIPIENT REMITTER START -->
						<div class="panel">
							
							<a class="panel-heading collapsed" role="tab" id="recipient_remitter_heading" data-toggle="collapse" data-parent="#accordion" href="#recipient_remitter" aria-expanded="false" aria-controls="recipient_remitter" style="padding: 5px 15px;">
								
								<h4 class="panel-title"> <small> <i class="fa fa-user"></i> Remitter <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
							
							</a>
							
							<div id="recipient_remitter" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="recipient_remitter_heading" aria-expanded="false" style="height: 0px;">

								<div class="panel-body" style="padding: 0px 10px 5px;">
									
									<div class="row">

										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

											<ul class="list-group" style="margin: 15px 0px 10px;">

												<li class="list-group-item" style="padding: 5px 10px;"> <small> Relationship: <b> {{ ( isset( $recipient_details -> remitter_relationship_id ) && ! empty( $recipient_details -> remitter_relationship_id ) && $recipient_details -> remitter_relationship_id != 1 ? $recipient_details -> relationship -> name : ( isset( $recipient_other_relationship -> relationship ) && ! empty( $recipient_other_relationship -> relationship ) ? $recipient_other_relationship -> relationship : '' ) ) }} </b> </small> </li>
												
												<li class="list-group-item" style="padding: 5px 10px;"> <small> Receive option: <b> {{ ( isset( $recipient_receive_option -> receive_option_list_id ) && ! empty( $recipient_receive_option -> receive_option_list_id ) && $recipient_receive_option -> receive_option_list_id != 41 ? $recipient_receive_option -> list -> name : ( isset( $recipient_receive_option_other_bank -> bank_name ) && ! empty( $recipient_receive_option_other_bank -> bank_name ) ? $recipient_receive_option_other_bank -> bank_name : '' ) ) }} </b> </small> </li>
												
											</ul>

										</div>

									</div>
					
								</div>

							</div>

						</div>
						<!-- RECIPIENT REMITTER END -->
						
						
						
						@if( $recipient_details -> type == 'person' )
						
							<!-- RECIPIENT PERSONAL START -->
							<div class="panel">
								
								<a class="panel-heading collapsed" role="tab" id="recipient_personal_heading" data-toggle="collapse" data-parent="#accordion" href="#recipient_personal" aria-expanded="false" aria-controls="recipient_personal" style="padding: 5px 15px;">
									
									<h4 class="panel-title"> <small> <i class="fa fa-user"></i> Personal <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
								
								</a>
								
								<div id="recipient_personal" class="panel-collapse collapse" role="tabpanel" aria-labelledby="recipient_personal_heading" aria-expanded="false" style="height: 0px;">

									<div class="panel-body" style="padding: 0px 10px 5px;">
										
										<div class="row">

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

												<ul class="list-group" style="margin: 15px 0px 10px;">

													<li class="list-group-item" style="padding: 5px 10px;"> <small> Birthday: <b> {{ ( isset( $recipient_person_details -> birthday ) && ! empty( $recipient_person_details -> birthday ) ? date( 'F j, Y', strtotime( $recipient_person_details -> birthday ) ) : '' ) }} </b> </small> </li>
													
													<li class="list-group-item" style="padding: 5px 10px;"> <small> Gender: <b> {{ ( isset( $recipient_person_details -> gender ) && ! empty( $recipient_person_details -> gender ) ? ucfirst( $recipient_person_details -> gender ) : '' ) }} </b> </small> </li>
													
													<li class="list-group-item" style="padding: 5px 10px;"> <small> Civil status: <b> {{ ( isset( $recipient_person_details -> civil_status_id ) && ! empty( $recipient_person_details -> civil_status_id ) ? $recipient_person_details -> civil_status -> name : '' ) }} </b> </small> </li>

													<li class="list-group-item" style="padding: 5px 10px;"> <small> Nationality: <b> {{ ( isset( $recipient_person_details -> nationality_id ) && ! empty( $recipient_person_details -> nationality_id ) ? ucfirst( $recipient_person_details -> nationality -> name ) : '' ) }} </b> </small> </li>

													@if( isset( $recipient_contact -> contact_type_id ) && ! empty( $recipient_contact -> contact_type_id ) )
													 
														<li class="list-group-item" style="padding: 5px 10px;"> <small> {{ ucfirst( $recipient_contact -> type -> name ) }}: <b> {{ ( isset( $recipient_contact -> contact ) && ! empty( $recipient_contact -> contact ) ? $recipient_contact -> contact : '' ) }} </b> </small> </li>
													
													@endif

													<li class="list-group-item" style="padding: 5px 10px;"> <small> Secondary email: <b> {{ ( isset( $recipient_details -> secondary_email ) && ! empty( $recipient_details -> secondary_email ) ? $recipient_details -> secondary_email : '' ) }} </b> </small> </li>

												</ul>

											</div>

										</div>
						
									</div>

								</div>

							</div>
							<!-- RECIPIENT PERSONAL END -->
							
						@else
						
							<!-- RECIPIENT COMPANY START -->
							<div class="panel">
								
								<a class="panel-heading collapsed" role="tab" id="recipient_company_heading" data-toggle="collapse" data-parent="#accordion" href="#recipient_company" aria-expanded="false" aria-controls="recipient_company" style="padding: 5px 15px;">
									
									<h4 class="panel-title"> <small> <i class="fa fa-building"></i> Company <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
								
								</a>
								
								<div id="recipient_company" class="panel-collapse collapse" role="tabpanel" aria-labelledby="recipient_company_heading" aria-expanded="false" style="height: 0px;">

									<div class="panel-body" style="padding: 0px 10px 5px;">
										
										<div class="row">

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

												<ul class="list-group" style="margin: 15px 0px 10px;">

													<li class="list-group-item" style="padding: 5px 10px;"> <small> ACN/BRN/BIR number: <b> {{ ( isset( $recipient_company_details -> reg_number ) && ! empty( $recipient_company_details -> reg_number ) ? $recipient_company_details -> reg_number : '' ) }} </b> </small> </li>
													
													<li class="list-group-item" style="padding: 5px 10px;"> <small> Contact person: <b> {{ ( isset( $recipient_company_details -> contact_person ) && ! empty( $recipient_company_details -> contact_person ) ? ucwords( $recipient_company_details -> contact_person ) : '' ) }} </b> </small> </li>

													<li class="list-group-item" style="padding: 5px 10px;"> <small> Authorized person: <b> {{ ( isset( $recipient_company_details -> authorized_person ) && ! empty( $recipient_company_details -> authorized_person ) ? ucwords( $recipient_company_details -> authorized_person ) : '' ) }} </b> </small> </li>

													<li class="list-group-item" style="padding: 5px 10px;"> <small> Secondary email: <b> {{ ( isset( $recipient_details -> secondary_email ) && ! empty( $recipient_details -> secondary_email ) ? $recipient_details -> secondary_email : '' ) }} </b> </small> </li>

												</ul>

											</div>

										</div>
						
									</div>

								</div>

							</div>
							<!-- RECIPIENT COMPANY END -->
						
						@endif
						
						
						
						<!-- RECIPIENT ADDRESS START -->
						<div class="panel">
							
							<a class="panel-heading collapsed" role="tab" id="recipient_address_heading" data-toggle="collapse" data-parent="#accordion" href="#recipient_address" aria-expanded="false" aria-controls="recipient_address" style="padding: 5px 15px;">
								
								<h4 class="panel-title"> <small> <i class="fa fa-address-book"></i> Address <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
							
							</a>
							
							<div id="recipient_address" class="panel-collapse collapse" role="tabpanel" aria-labelledby="recipient_address_heading" aria-expanded="false" style="height: 0px;">

								<div class="panel-body" style="padding: 0px 10px 5px;">
									
									<ul class="list-group" style="margin: 15px 0px 10px;">

										<li class="list-group-item" style="padding: 5px 10px;"> <small> Country: <b> {{ ( isset( $recipient_address -> country_id ) && ! empty( $recipient_address -> country_id ) ? $recipient_address -> country -> en_short_name : '' ) }} </b> </small> </li>
										
										<li class="list-group-item" style="padding: 5px 10px;"> <small> State: <b> {{ ( isset( $recipient_address -> state_id ) && ! empty( $recipient_address -> state_id ) ? $recipient_address -> state -> name : '' ) }} </b> </small> </li>
										
										<li class="list-group-item" style="padding: 5px 10px;"> <small> City: <b> {{ ( isset( $recipient_address -> city_id ) && ! empty( $recipient_address -> city_id ) ? $recipient_address -> city -> name : '' ) }} </b> </small> </li>

										<li class="list-group-item" style="padding: 5px 10px;"> <small> Postal: <b> {{ ( isset( $recipient_address -> postal_code ) && ! empty( $recipient_address -> postal_code ) ? $recipient_address -> postal_code : '' ) }} </b> </small> </li>

										<li class="list-group-item" style="padding: 5px 10px;"> <small> Current address: <b> {{ ( isset( $recipient_address -> current_address ) && ! empty( $recipient_address -> current_address ) ? $recipient_address -> current_address : '' ) }} </b> </small> </li>

										<li class="list-group-item" style="padding: 5px 10px;"> <small> Permanent address: <b> {{ ( isset( $recipient_address -> permanent_address ) && ! empty( $recipient_address -> permanent_address ) ? $recipient_address -> permanent_address : '' ) }} </b> </small> </li>

									</ul>
											
								</div>

							</div>

						</div>
						<!-- RECIPIENT ADDRESS END -->
						
						

						@if( isset( $recipient_receive_option -> list -> receive_option -> id ) && ! empty( $recipient_receive_option -> list -> receive_option -> id ) && $recipient_receive_option -> list -> receive_option -> id == 2 )
						
							<!-- RECIPIENT BANK START -->
							<div class="panel">
								
								<a class="panel-heading collapsed" role="tab" id="recipient_bank_heading" data-toggle="collapse" data-parent="#accordion" href="#recipient_bank" aria-expanded="false" aria-controls="recipient_bank" style="padding: 5px 15px;">
									
									<h4 class="panel-title"> <small> <i class="fa fa-university" aria-hidden="true"></i> Bank <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
								
								</a>

								<div id="recipient_bank" class="panel-collapse collapse" role="tabpanel" aria-labelledby="recipient_bank_heading" aria-expanded="false" style="height: 0px;">

									<div class="panel-body" style="padding: 0px 10px 5px;">

										<ul class="list-group" style="margin: 15px 0px 10px;">

											<li class="list-group-item" style="padding: 5px 10px;"> <small> Bank: <b> {{ ( isset( $recipient_bank -> receive_option_list_id ) && ! empty( $recipient_bank -> receive_option_list_id ) && $recipient_bank -> receive_option_list_id != 41 ? $recipient_bank -> list -> name : ( isset( $recipient_other_bank -> bank_name ) && ! empty( $recipient_other_bank -> bank_name ) ? $recipient_other_bank -> bank_name : '' ) ) }} </b> </small> </li>

											<li class="list-group-item" style="padding: 5px 10px;"> <small> Account name: <b> {{ ( isset( $recipient_bank -> account_name ) && ! empty( $recipient_bank -> account_name ) ? $recipient_bank -> account_name : '' ) }} </b> </small> </li>

											<li class="list-group-item" style="padding: 5px 10px;"> <small> Account branch: <b> {{ ( isset( $recipient_bank -> account_branch ) && ! empty( $recipient_bank -> account_branch ) ? $recipient_bank -> account_branch : '' ) }} </b> </small> </li>
											
											<li class="list-group-item" style="padding: 5px 10px;"> <small> Account number: <b> {{ ( isset( $recipient_bank -> account_no ) && ! empty( $recipient_bank -> account_no ) ? $recipient_bank -> account_no : '' ) }} </b> </small> </li>

										</ul>

									</div>

								</div>

							</div>
							<!-- RECIPIENT BANK END -->
							
						@endif
						
						
						
						@if( isset( $recipient_note -> note ) && ! empty( $recipient_note -> note ) )
						
							<!-- RECIPIENT NOTE START -->
							<div class="panel">
								
								<a class="panel-heading collapsed" role="tab" id="recipient_note_heading" data-toggle="collapse" data-parent="#accordion" href="#recipient_note" aria-expanded="false" aria-controls="recipient_note" style="padding: 5px 15px;">
									
									<h4 class="panel-title"> <small> <i class="fa fa-sticky-note-o"></i> Note <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
								
								</a>
								
								<div id="recipient_note" class="panel-collapse collapse" role="tabpanel" aria-labelledby="recipient_note_heading" aria-expanded="false" style="height: 0px;">

									<div class="panel-body" style="padding: 0px 10px 5px;">
										
										<p style="margin: 5px 0px;"> {{ ( isset( $recipient_note -> note ) && ! empty( $recipient_note -> note ) ? $recipient_note -> note : '' ) }} </p>

									</div>

								</div>

								
							</div>
							<!-- RECIPIENT NOTE END -->
						
						@endif
						
					</div>

				</div>

			</div>

		</div>

	</div>

@else

	<div class="row">
		
		<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
			
			<div class="alert alert-danger" role="alert" style="margin:0px;">

				<h6 class="text-center" style="margin:5px 0px;"> Please select recipient! </h6>

			</div>
		
		</div>
	
	</div>

@endif
