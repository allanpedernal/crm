@if( isset( $remitter_details ) && ! empty( $remitter_details ) )

	<div class="panel panel-default" style="border-radius: 0px; margin-top: 70px; border: 3px solid transparent; border-color: #ccc; margin-bottom: 0px;">
		
		<div class="panel-body">
			
			<div class="row">

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<div class="text-center" style="margin-top: -80px;">

						<img src="{{ URL::asset('img/img.png') }}" class="img-circle" style="width: 125px; height: 125px; border: 5px solid #ccc;">

						<p style="margin: 15px 0px 5px;"> <span class="label label-primary"> <small> {{ $remitter_details -> id_number }} </small> </span> </p>
						
						<h5 style="margin: 0px;"> {{ $remitter_details -> firstname }} {{ $remitter_details -> middlename }} {{ $remitter_details -> lastname }} </h5>
						
						<p style="margin: 0px 0px 15px;"> <small> {{ $remitter_details -> email }} </small> </p>
						
					</div>
					
					<div class="accordion" id="remitter_accordion" role="tablist" aria-multiselectable="true">
						
						<!-- REMITTER PERSONAL START -->
						<div class="panel">
							
							<a class="panel-heading collapsed" role="tab" id="personal_heading" data-toggle="collapse" data-parent="#accordion" href="#personal" aria-expanded="false" aria-controls="personal" style="padding: 5px 15px;">
								
								<h4 class="panel-title"> <small> <i class="fa fa-user"></i> Personal <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
							
							</a>
							
							<div id="personal" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="personal_heading" aria-expanded="false" style="height: 0px;">

								<div class="panel-body" style="padding: 0px 10px 5px;">
									
									<div class="row">

										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

											<ul class="list-group" style="margin: 15px 0px 10px;">

												<li class="list-group-item" style="padding: 5px 10px;"> <small> Birthday: <b> {{ ( isset( $remitter_details -> birthday ) && ! empty( $remitter_details -> birthday ) ? date( 'F j, Y', strtotime( $remitter_details -> birthday ) ) : '' ) }} </b> </small> </li>
												
												<li class="list-group-item" style="padding: 5px 10px;"> <small> Gender: <b> {{ ( isset( $remitter_details -> gender ) && ! empty( $remitter_details -> gender ) ? ucfirst( $remitter_details -> gender ) : '' ) }} </b> </small> </li>
												
												<li class="list-group-item" style="padding: 5px 10px;"> <small> Civil status: <b> {{ ( isset( $remitter_details -> civil_status_id ) && ! empty( $remitter_details -> civil_status_id ) ? $remitter_details -> civil_status -> name : '' ) }} </b> </small> </li>

												<li class="list-group-item" style="padding: 5px 10px;"> <small> Nationality: <b> {{ ( isset( $remitter_details -> nationality_id ) && ! empty( $remitter_details -> nationality_id ) ? ucfirst( $remitter_details -> nationality -> name ) : '' ) }} </b> </small> </li>
												
												@if( isset( $remitter_contact -> contact_type_id ) && ! empty( $remitter_contact -> contact_type_id ) )
												 
													<li class="list-group-item" style="padding: 5px 10px;"> <small> {{ ucfirst( $remitter_contact -> type -> name ) }}: <b> {{ ( isset( $remitter_contact -> contact ) && ! empty( $remitter_contact -> contact ) ? $remitter_contact -> contact : '' ) }} </b> </small> </li>
												
												@endif
												
												<li class="list-group-item" style="padding: 5px 10px;"> <small> Secondary email: <b> {{ ( isset( $remitter_details -> secondary_email ) && ! empty( $remitter_details -> secondary_email ) ? $remitter_details -> secondary_email : '' ) }} </b> </small> </li>

											</ul>

										</div>

									</div>
					
								</div>

							</div>

						</div>
						<!-- REMITTER PERSONAL END -->
						
						<!-- REMITTER ADDRESS START -->
						<div class="panel">
							
							<a class="panel-heading collapsed" role="tab" id="remitter_address_heading" data-toggle="collapse" data-parent="#accordion" href="#remitter_address" aria-expanded="false" aria-controls="remitter_address" style="padding: 5px 15px;">
								
								<h4 class="panel-title"> <small> <i class="fa fa-address-book"></i> Address <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
							
							</a>
							
							<div id="remitter_address" class="panel-collapse collapse" role="tabpanel" aria-labelledby="remitter_address_heading" aria-expanded="false" style="height: 0px;">

								<div class="panel-body" style="padding: 0px 10px 5px;">
									
									<ul class="list-group" style="margin: 15px 0px 10px;">

										<li class="list-group-item" style="padding: 5px 10px;"> <small> Country: <b> {{ ( isset( $remitter_address -> country_id ) && ! empty( $remitter_address -> country_id ) ? $remitter_address -> country -> 	en_short_name : '' ) }} </b> </small> </li>
										
										<li class="list-group-item" style="padding: 5px 10px;"> <small> State: <b> {{ ( isset( $remitter_address -> state_id ) && ! empty( $remitter_address -> state_id ) ? $remitter_address -> state -> name : '' ) }} </b> </small> </li>
										
										<li class="list-group-item" style="padding: 5px 10px;"> <small> City: <b> {{ ( isset( $remitter_address -> city_id ) && ! empty( $remitter_address -> city_id ) ? $remitter_address -> city -> name : '' ) }} </b> </small> </li>

										<li class="list-group-item" style="padding: 5px 10px;"> <small> Postal: <b> {{ ( isset( $remitter_address -> postal_code ) && ! empty( $remitter_address -> postal_code ) ? $remitter_address -> postal_code : '' ) }} </b> </small> </li>

										<li class="list-group-item" style="padding: 5px 10px;"> <small> Current address: <b> {{ ( isset( $remitter_address -> current_address ) && ! empty( $remitter_address -> current_address ) ? $remitter_address -> current_address : '' ) }} </b> </small> </li>

										<li class="list-group-item" style="padding: 5px 10px;"> <small> Permanent address: <b> {{ ( isset( $remitter_address -> permanent_address ) && ! empty( $remitter_address -> permanent_address ) ? $remitter_address -> permanent_address : '' ) }} </b> </small> </li>

									</ul>
											
								</div>

							</div>

						</div>
						<!-- REMITTER ADDRESS END -->
						
						<!-- REMITTER IDENTIFICATION START -->
						<div class="panel">
							
							<a class="panel-heading collapsed" role="tab" id="remitter_identification_heading" data-toggle="collapse" data-parent="#accordion" href="#remitter_identification" aria-expanded="false" aria-controls="remitter_identification" style="padding: 5px 15px;">
								
								<h4 class="panel-title"> <small> <i class="fa fa-id-card"></i> Identification <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
							
							</a>
							
							<div id="remitter_identification" class="panel-collapse collapse" role="tabpanel" aria-labelledby="remitter_identification_heading" aria-expanded="false" style="height: 0px;">

								<div class="panel-body" style="padding: 0px 10px 5px;">
									
									<ul class="list-group" style="margin: 15px 0px 10px;">

										<li class="list-group-item" style="padding: 5px 10px;"> <small> Number: <b> {{ ( isset( $remitter_identification -> number ) && ! empty( $remitter_identification -> number ) ? $remitter_identification -> number : '' ) }} </b> </small> </li>
										
										<li class="list-group-item" style="padding: 5px 10px;"> <small> Expiration: <b> {{ ( isset( $remitter_identification -> expiration_date ) && ! empty( $remitter_identification -> expiration_date ) ? date( 'F j, Y', strtotime( $remitter_identification -> expiration_date ) ) : '' ) }} </b> </small> </li>
										
										<li class="list-group-item" style="padding: 5px 10px;"> <small> Type: <b> {{ ( isset( $remitter_identification -> identification_type_id ) && ! empty( $remitter_identification -> identification_type_id ) && $remitter_identification -> identification_type_id == 1 ? $remitter_identification -> other_type : ( isset( $remitter_identification -> identification_type_id ) && ! empty( $remitter_identification ->identification_type_id ) ? $remitter_identification -> type -> name : '' ) ) }} </b> </small> </li>
										
										<li class="list-group-item text-center" style="padding: 5px 10px;">
											
											<a href="{{ URL::asset('img/id.jpg') }}" target="_blank">
												
												<img src="{{ URL::asset('img/id.jpg') }}" style="width: 230px; height: 120px; margin: 20px 0px;">

											</a>
											
										</li>

									</ul>

								</div>

							</div>

						</div>
						<!-- REMITTER IDENTIFICATION END -->
						
						@if( isset( $remitter_note -> note ) && ! empty( $remitter_note -> note ) )
						
							<!-- REMITTER NOTE START -->
							<div class="panel">
								
								<a class="panel-heading collapsed" role="tab" id="remitter_note_heading" data-toggle="collapse" data-parent="#accordion" href="#remitter_note" aria-expanded="false" aria-controls="remitter_note" style="padding: 5px 15px;">
									
									<h4 class="panel-title"> <small> <i class="fa fa-sticky-note-o"></i> Note <i class="fa fa-chevron-down pull-right" style="margin-top: 4px;"></i> </small> </h4>
								
								</a>
								
								<div id="remitter_note" class="panel-collapse collapse" role="tabpanel" aria-labelledby="remitter_note_heading" aria-expanded="false" style="height: 0px;">

									<div class="panel-body" style="padding: 0px 10px 5px;">
										
										<p style="margin: 5px 0px;"> {{ ( isset( $remitter_note -> note ) && ! empty( $remitter_note -> note ) ? $remitter_note -> note : '' ) }} </p>

									</div>

								</div>

								
							</div>
							<!-- REMITTER NOTE END -->
						
						@endif
						
					</div>

				</div>

			</div>

		</div>

	</div>

@else

	<div class="row">
		
		<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
			
			<div class="alert alert-danger" role="alert" style="margin:0px;">

				<h6 class="text-center" style="margin:5px 0px;"> Please select remitter! </h6>

			</div>
		
		</div>
	
	</div>

@endif
