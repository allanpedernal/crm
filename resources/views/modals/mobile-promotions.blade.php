<div id="mobile_promotion_modal" class="modal fade" tabindex="-3" role="dialog">
		
	<div class="modal-dialog" role="document">
		
		<div class="modal-content">
			
			<form method="POST" action="{{ URL('/mobile/promitions/form/post') }}" id="mobile_promotion_form" class="form-horizontal" data-validate="parsley" onSubmit="return on_validate();">
				
				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						
						<span aria-hidden="true"> &times; </span>
						
					</button>

					<h4 class="modal-title"> <i class="fa fa-wpforms"></i> Mobile Promotion Form </h4>

				</div>

				<div class="modal-body">
					
					<!-- MOBILE PROMOTION ID START -->
					<input type="hidden" name="mobile_promotion_id" id="mobile_promotion_id" class="form-control" value="{{ ( isset( $mobile_promotion_id ) && ! empty( $mobile_promotion_id ) ? $mobile_promotion_id : 0 ) }}">
					<!-- MOBILE PROMOTION ID END -->
					
					<!-- MOBILE PROMOTION EXCERPT START -->
					<div class="row">

						<div class="col-g-12 col-md-12 col-sm-12 col-xs-12">
						
							<div class="form-group">
							
								<label for="mobile_promotion_excerpt"> Excerpt: </label>
								
								<input type="text" id="mobile_promotion_excerpt" name="mobile_promotion_excerpt" class="form-control" placeholder="Excerpt" required="required" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="80" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" value="{{ ( isset( $mobile_promotion_details[ 'excerpt' ] ) && ! empty( $mobile_promotion_details[ 'excerpt' ] ) ? $mobile_promotion_details[ 'excerpt' ] : '' ) }}">
							
							</div>
						
						</div>

					</div>
					<!-- MOBILE PROMOTION EXCERPT END -->
					
					<!-- MOBILE PROMOTION DESCRIPTION START -->
					<div class="row">

						<div class="col-g-12 col-md-12 col-sm-12 col-xs-12">
						
							<div class="form-group">
							
								<label for="mobile_promotion_description"> Description: </label>
								
								<textarea id="mobile_promotion_description" name="mobile_promotion_description" class="form-control" placeholder="Description" required="required" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="1000" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" rows="10">{{ ( isset( $mobile_promotion_details[ 'description' ] ) && ! empty( $mobile_promotion_details[ 'description' ] ) ? $mobile_promotion_details[ 'description' ] : '' ) }}</textarea>
							
							</div>
						
						</div>

					</div>
					<!-- MOBILE PROMOTION DESCRIPTION END -->
					
				</div>
				
				<div class="modal-footer">
					
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" style="margin:0px;"> <i class="fa fa-times" aria-hidden="true"></i> Cancel </button>

					<button type="submit" class="btn btn-sm btn-primary" style="margin:0px;"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save </button>
					
				</div>

			</form>
			
		</div>
		
	</div>
	
</div>
