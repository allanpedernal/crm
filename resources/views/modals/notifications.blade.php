<div id="notification_modal" class="modal fade" tabindex="-3" role="dialog">
		
	<div class="modal-dialog" role="document">
		
		<div class="modal-content">
			
			<form method="POST" action="{{ URL('/notification/user/form/post') }}" id="notification_form" class="form-horizontal" data-parsley-validate>
				
				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						
						<span aria-hidden="true"> &times; </span>
						
					</button>

					<h4 class="modal-title"> <i class="fa fa-wpforms"></i> Notification Form </h4>

				</div>

				<div class="modal-body">
					
					<!-- NOTIFICATION ID START -->
					<input type="hidden" name="notification_id" id="notification_id" class="form-control" value="0">
					<!-- NOTIFICATION ID END -->
					
					<!-- NOTIFICATION REMITTER START -->
					<div class="row">

						<div class="col-g-6 col-md-6 col-sm-12 col-xs-12">
						
							<div class="form-group">
							
								<label for="notification_remitter"> Remitter: </label>
								
								<select id="notification_remitter" name="notification_remitter" class="form-control" required="required">
									
									<option value=""> Remitter </option>
								
								</select>
							
							</div>
						
						</div>

						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						
							<div class="form-group">
							
								<label for="notification_type"> Type: </label>
								
								<select id="notificaition_type" name="notification_type" class="form-control"  required="required">
								
									<option value=""> Type </option>
									
									<option value="user"> User </option>
									
									<option value="group"> Group </option>
								
								</select>
							
							</div>
						
						</div>
					
					</div>
					<!-- NOTIFICATION REMITTER  END-->
					
					<!-- NOTIFICATION MESSAGE START -->
					<div class="row">
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<div class="form-group">
							
								<label for="notification_message"> Message: </label>
								
								<textarea id="notification_message" name="notificaition_message" class="form-control" rows="4" required="required"></textarea>
							
							</div>
						
						</div>
					
					</div>
					<!-- NOTIFICATION MESSAGE END -->
					
				</div>
				
				<div class="modal-footer">
					
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" style="margin:0px;"> <i class="fa fa-times" aria-hidden="true"></i> Cancel </button>

					<button type="submit" class="btn btn-sm btn-primary" style="margin:0px;"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save </button>
					
				</div>

			</form>
			
		</div>
		
	</div>
	
</div>
