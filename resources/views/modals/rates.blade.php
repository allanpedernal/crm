<div id="rates_modal" class="modal fade" tabindex="-3" role="dialog">
		
	<div class="modal-dialog" role="document">
		
		<div class="modal-content">
			
			<form method="POST" action="{{ URL('/rates/form/post') }}" id="rates_form" class="form-horizontal" data-parsley-validate>
				
				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						
						<span aria-hidden="true"> &times; </span>
						
					</button>

					<h4 class="modal-title"> <i class="fa fa-wpforms"></i> Rates Form </h4>

				</div>

				<div class="modal-body">
					
					<!-- RATES ID START -->
					<input type="hidden" name="rates_id" id="rates_id" class="form-control" value="0">
					<!-- RATES ID END -->
					
					<!--EXCHANGES RATE START -->
					<div class="row">

						<div class="col-g-12 col-md-12 col-sm-12 col-xs-12">
						
							<div class="form-group">
							
								<label for="rate_exchange"> Exchange: </label>
								
								<input type="text" id="rate_exchange" name="rate_exchange" class="form-control" placeholder="Exchange">
							
							</div>
						
						</div>

					</div>
					<!-- EXCHANGES RATE END -->

					<!--TRADE RATE START -->
					<div class="row">

						<div class="col-g-12 col-md-12 col-sm-12 col-xs-12">
						
							<div class="form-group">
							
								<label for="rate_trade"> Trade: </label>
								
								<input type="text" id="rate_trade" name="rate_trade" class="form-control" placeholder="Trade">
							
							</div>
						
						</div>

					</div>
					<!-- TRADE RATE END -->

				</div>
				
				<div class="modal-footer">
					
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" style="margin:0px;"> <i class="fa fa-times" aria-hidden="true"></i> Cancel </button>

					<button type="submit" class="btn btn-sm btn-primary" style="margin:0px;"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save </button>
					
				</div>

			</form>
			
		</div>
		
	</div>
	
</div>
