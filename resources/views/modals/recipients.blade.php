<div id="recipient_modal" class="modal fade" tabindex="-1" role="dialog">
		
	<div class="modal-dialog modal-lg" role="document">
		
		<div class="modal-content">
			
			<form method="POST" action="{{ URL('/iremit/recipient/form/post') }}" id="recipient_form" class="form-horizontal" data-validate="parsley" onSubmit="return on_validate();">
				
				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						
						<span aria-hidden="true"> &times; </span>
						
					</button>

					<h4 class="modal-title"> <i class="fa fa-wpforms"></i> Recipient Form </h4>

				</div>

				<div class="modal-body">
					
					<!-- RECIPIENT ID START -->
					<input type="hidden" name="recipient_id" id="recipient_id" class="form-control" value="{{ ( isset( $recipient_id ) && ! empty( $recipient_id ) ? $recipient_id : 0 ) }}">
					<!-- RECIPIENT ID END -->
					
					<!-- RECIPIENT TABS START -->
					<div role="tabpanel" data-example-id="togglable-tabs">
						
						<!-- RECIPIENT FORM NAV START -->
						<ul id="recipient_form_nav" class="nav nav-tabs bar_tabs" role="tablist">
							
							<!-- PERSONAL INFORMATION NAV START -->
							<li role="presentation" class="active">
								
								<a href="#recipient_personal_tab" id="personal-tabb" role="tab" data-toggle="tab" aria-controls="personal" aria-expanded="false"> <span class="recipient_type"> <i class="{{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? 'fa fa-user' : 'fa fa-building-o' ) }}"></i> {{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? 'Person' : 'Company' ) }} </span> </a>
								
							</li>
							<!-- PERSONAL INFORMATION NAV END -->
							
							<!-- ADDRESS INFORMATION NAV START -->
							<li role="presentation">
								
								<a href="#recipient_address_tab" id="address-tabb" role="tab" data-toggle="tab" aria-controls="address" aria-expanded="false"> <i class="fa fa-address-book" aria-hidden="true"></i> Address </a>
								
							</li>
							<!-- ADDRESS INFORMATION NAV END -->

							<!-- CONTACT INFORMATION NAV START -->
							<li role="presentation">
								
								<a href="#recipient_contact_tab" id="contact-tabb" role="tab" data-toggle="tab" aria-controls="contact" aria-expanded="false"> <i class="fa fa-phone" aria-hidden="true"></i> Contact </a>
								
							</li>
							<!-- CONTACT INFORMATION NAV START -->
							
							<!-- BANK INFORMATION START -->
							<li role="presentation">
								
								<a href="#recipient_bank_tab" id="bank-tabb" role="tab" data-toggle="tab" aria-controls="bank" aria-expanded="false"> <i class="fa fa-university" aria-hidden="true"></i> Bank </a>
								
							</li>
							<!-- BANK INFORMATION END -->
							
							<!-- NOTE INFORMATION START -->
							<li role="presentation">
								
								<a href="#recipient_note_tab" id="note-tabb" role="tab" data-toggle="tab" aria-controls="note" aria-expanded="false"> <i class="fa fa-sticky-note" aria-hidden="true"></i> Note </a>
								
							</li>
							<!-- NOTE INFORMATION END -->
							
							<!-- HISTORY INFORMATION START -->
							<li role="presentation">
								
								<a href="#recipient_history_tab" id="history-tabb" role="tab" data-toggle="tab" aria-controls="history" aria-expanded="false"> <i class="fa fa-clock-o" aria-hidden="true"></i> History </a>
								
							</li>
							<!-- HISTORY INFORMATION END -->

						</ul>
						<!-- RECIPIENT FORM NAV END -->
						
						<!-- RECIPIENT FORM TAB CONTENT START -->
						<div id="recipient_form_tab_content" class="tab-content">
							
							<!-- RECIPIENT PERSONAL TAB START -->
							<div role="tabpanel" class="tab-pane fade active in" id="recipient_personal_tab" aria-labelledby="personal-tab">
									
								<div class="row">
									
									<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
										
										<!-- PERSONAL INFORMATION START -->
										<div class="panel panel-primary" style="margin:0px 0px 20px;">
											
											<div class="panel-heading"> <span class="recipient_type"> <i class="{{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? 'fa fa-user' : 'fa fa-building-o' ) }}"></i> {{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? 'Person' : 'Company' ) }} </span> </div>

											<div class="panel-body">
												
												<div class="row">
												
													<div class="col-lg-12 col-md-12 col-sm-12 col-sm-12 text-right">
													
														<div class="form-group">
															
															<label>
																
																<input id="recipient_is_company" name="recipient_is_company" value="1" type="checkbox" class="js-switch" {{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? "" : "checked='checked'" ) }} /> Is this company?

															</label>

														</div>
													
													</div>
												
												</div>
												
												<!-- PERSONAL CONTAINER START -->
												<div class="personal_container" style="{{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? "display:block;" : "display:none;" ) }}">
																
													<!-- RECIPIENT FULLNAME START -->
													<div class="row">
														
														<!-- RECIPIENT TITLE START -->
														<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_title"> Title:<span style="color:#E85445;">*</span> </label>

																<select id="recipient_title" name="recipient_title" class="form-control chosen-select" {{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? "required='required'" : "" ) }} data-placeholder="Title">
																
																	<option value=""></option>
																	
																	@foreach ( $titles as $title )
																	
																		<option {{ ( isset( $recipient_person_details[ 'title_id' ] ) && ! empty( $recipient_person_details[ 'title_id' ] ) && $recipient_person_details[ 'title_id' ] == $title[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $title[ 'id' ] }}"> {{ $title[ 'abbr' ] }} </option>
																	
																	@endforeach
																
																</select>

															</div>
															
														</div>
														<!-- RECIPIENT TITLE END -->
														
														<!-- RECIPIENT FIRSTNAME START -->
														<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_firstname"> First name:<span style="color:#E85445;">*</span> </label>

																<input type="text" class="form-control" id="recipient_firstname" name="recipient_firstname" placeholder="First name" {{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? "required='required'" : "" ) }} value="{{ ( isset( $recipient_person_details[ 'firstname' ] ) && ! empty( $recipient_person_details[ 'firstname' ] ) ? $recipient_person_details[ 'firstname' ] : "" ) }}">

															</div>
															
														</div>
														<!-- RECIPIENT FIRSTNAME END -->

														<!-- RECIPIENT MIDDLENAME START -->
														<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_middlename"> Middle name: </label>

																<input type="text" class="form-control" id="recipient_middlename" name="recipient_middlename" placeholder="Middle name" value="{{ ( isset( $recipient_person_details[ 'middlename' ] ) && ! empty( $recipient_person_details[ 'middlename' ] ) ? $recipient_person_details[ 'middlename' ] : "" ) }}">

															</div>
														
														</div>
														<!-- RECIPIENT MIDDLENAME END -->
														
														<!-- RECIPIENT LASTNAME START -->
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_lastname"> Last name:<span style="color:#E85445;">*</span> </label>

																<input type="text" class="form-control" id="recipient_lastname" name="recipient_lastname" placeholder="Last name" {{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? "required='required'" : "" ) }} value="{{ ( isset( $recipient_person_details[ 'lastname' ] ) && ! empty( $recipient_person_details[ 'lastname' ] ) ? $recipient_person_details[ 'lastname' ] : "" ) }}">

															</div>
														
														</div>
														<!-- RECIPIENT LASTNAME START -->
														
													</div>
													<!-- RECIPIENT FULLNAME END -->
													
													<!-- RECIPIENT OTHER DETAILS START -->
													<div class="row">
														
														<!-- RECIPIENT NATIONALITY START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_nationality"> Nationality: </label>
																
																<select id="recipient_nationality" name="recipient_nationality" class="form-control chosen-select" data-placeholder="Nationality">
																
																	<option value=""></option>
																	
																	@foreach ( $nationalities as $nationality )
																	
																		<option {{ ( isset( $recipient_person_details[ 'nationality_id' ] ) && ! empty( $recipient_person_details[ 'nationality_id' ] ) && $recipient_person_details[ 'nationality_id' ] == $nationality[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $nationality[ 'id' ] }}"> {{ $nationality[ 'name' ] }} </option>
																	
																	@endforeach
																
																</select>

															</div>
															
														</div>
														<!-- RECIPIENT NATIONALITY END -->
													
														<!-- RECIPIENT BIRTHDAY START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_birthday"> Birthday:<span style="color:#E85445;">*</span> </label>

																<div class="input-prepend input-group" style="margin:0px;">
																	
																	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>

																	<input type="text" class="form-control datepicker" id="recipient_birthday" name="recipient_birthday" placeholder="Birthday" readonly="readonly" {{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? "required='required'" : "" ) }} value="{{ ( isset( $recipient_person_details[ 'birthday' ] ) && ! empty( $recipient_person_details[ 'birthday' ] ) ? $recipient_person_details[ 'birthday' ] : "" ) }}">

																</div>
																
															</div>
															
														</div>
														<!-- RECIPIENT BIRTHDAY END -->
														
													</div>
													
													<div class="row">
														
														<!-- RECIPIENT CIVIL STATUS START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_civil_status"> Civil status:<span style="color:#E85445;">*</span> </label>

																<select class="form-control chosen-select" id="recipient_civil_status" name="recipient_civil_status" {{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? "required='required'" : "" ) }} data-placeholder="Civil status">
																
																	<option value=""></option>
																	
																	@foreach ( $civil_status as $cs )
																	
																		<option {{ ( isset( $recipient_person_details[ 'civil_status_id' ] ) && ! empty( $recipient_person_details[ 'civil_status_id' ] ) && $recipient_person_details[ 'civil_status_id' ] == $cs[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $cs[ 'id' ] }}"> {{ $cs[ 'name' ] }} </option>
																	
																	@endforeach
																
																</select>
																
															</div>
															
														</div>
														<!-- RECIPIENT CIVIL STATUS END -->
														
														<!-- REMITTER GENDER START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															
															<div class="form-group">
															
																<label for="recipient_gender"> Gender:<span style="color:#E85445;">*</span> </label>
																
																<select id="recipient_gender" name="recipient_gender" class="form-control chosen-select recipient_gender" {{ ( ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'person' ) || $action == 'new' ? "required='required'" : "" ) }} data-placeholder="Gender">

																	<option value=""></option>

																	<option {{ ( isset( $recipient_person_details[ 'gender' ] ) && ! empty( $recipient_person_details[ 'gender' ] ) && $recipient_person_details[ 'gender' ] == 'male'  ? 'selected="selected"' : "" ) }} value="male"> Male </option>

																	<option {{ ( isset( $recipient_person_details[ 'gender' ] ) && ! empty( $recipient_person_details[ 'gender' ] ) && $recipient_person_details[ 'gender' ] == 'female'  ? 'selected="selected"' : "" ) }} value="female"> Female </option>

																</select>

															</div>

														</div>
														<!-- REMITTER GENDER END -->
													
													</div>
													<!-- RECIPIENT OTHER DETAILS END -->
												
												</div>
												<!-- PERSONAL CONTAINER END -->
												
												<!-- COMPANY CONTAINER START -->
												<div class="company_container" style="{{ ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'company' ? "display:block;" : "display:none;" ) }}">
													
													<!-- COMPANY NAME START -->
													<div class="row">
														
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														
															<div class="form-group">
																
																<label for="recipient_company_name"> Company name:<span style="color:#E85445;">*</span> </label>

																<input type="text" class="form-control" id="recipient_company_name" name="recipient_company_name" placeholder="Company name" {{ ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'company' ? "required='required'" : "" ) }} value="{{ ( isset( $recipient_company_details[ 'company_name' ] ) && ! empty( $recipient_company_details[ 'company_name' ] ) ? $recipient_company_details[ 'company_name' ] : "" ) }}">

															</div>
														
														</div>
													
													</div>
													<!-- COMPANY NAME START -->
													
													<div class="row">
														
														<!-- REGISTER NUMBER START -->
														<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_reg_number"> ACN/BRN/BIR number:<span style="color:#E85445;">*</span> </label>

																<input type="text" class="form-control" id="recipient_reg_number" name="recipient_reg_number" placeholder="ACN/BRN/BIR number" {{ ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'company' ? "required='required'" : "" ) }} value="{{ ( isset( $recipient_company_details[ 'reg_number' ] ) && ! empty( $recipient_company_details[ 'reg_number' ] ) ? $recipient_company_details[ 'reg_number' ] : "" ) }}">

															</div>
														
														</div>
														<!-- REGISTER NUMBER END -->
														
														<!-- CONTACT PERSON START -->
														<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_contact_person"> Contact person:<span style="color:#E85445;">*</span> </label>

																<input type="text" class="form-control" id="recipient_contact_person" name="recipient_contact_person" placeholder="Contact person" {{ ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'company' ? "required='required'" : "" ) }} value="{{ ( isset( $recipient_company_details[ 'contact_person' ] ) && ! empty( $recipient_company_details[ 'contact_person' ] ) ? $recipient_company_details[ 'contact_person' ] : "" ) }}">

															</div>
														
														</div>
														<!-- CONTACT PERSON END -->
														
														<!-- AUTHORIZED PERSON START -->
														<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
															
															<div class="form-group">
																
																<label for="recipient_authorized_person"> Authorized person:<span style="color:#E85445;">*</span> </label>

																<input type="text" class="form-control" id="recipient_authorized_person" name="recipient_authorized_person" placeholder="Authorized person" {{ ( isset( $recipient_details[ 'type' ] ) && ! empty( $recipient_details[ 'type' ] ) && $recipient_details[ 'type' ] == 'company' ? "required='required'" : "" ) }} value="{{ ( isset( $recipient_company_details[ 'authorized_person' ] ) && ! empty( $recipient_company_details[ 'authorized_person' ] ) ? $recipient_company_details[ 'authorized_person' ] : "" ) }}">

															</div>
														
														</div>
														<!-- AUTHORIZED PERSON END -->
														
													</div>
													
												</div>
												<!-- COMPANY CONTAINER END -->
												
											</div>

										</div>
										<!-- PERSONAL INFORMATION END -->
										
										<!-- EMAIL INFORMATION START -->
										<div class="panel panel-warning" style="margin:0px 0px 20px;">
											
											<div class="panel-heading"> <i class="fa fa-envelope" aria-hidden="true"></i> Email </div>
											
											<div class="panel-body">
												
												<!-- PRIMARY AND SECONDARY EMAIL ADDRESS START -->
												<div class="row">

													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
														
														<div class="form-group">
															
															<label for="recipient_primary_email_address"> Primary email address: </label>

															<input type="text" class="form-control" id="recipient_primary_email_address" name="recipient_primary_email_address" placeholder="Primary email address" value="{{ ( isset( $recipient_details[ 'email' ] ) && ! empty( $recipient_details[ 'email' ] ) ? $recipient_details[ 'email' ] : "" ) }}">

														</div>

													</div>

													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
														
														<div class="form-group">
															
															<label for="recipient_secondary_email_address"> Secondary email address: </label>

															<input type="text" class="form-control" id="recipient_secondary_email_address" name="recipient_secondary_email_address" placeholder="Secondary email address" value="{{ ( isset( $recipient_details[ 'secondary_email' ] ) && ! empty( $recipient_details[ 'secondary_email' ] ) ? $recipient_details[ 'secondary_email' ] : "" ) }}">

														</div>

													</div>

												</div>
												<!-- PRIMARY AND SECONDARY EMAIL ADDRESS END -->

											</div>
											
										</div>
										<!-- EMAIL INFORMATION END -->
										
									</div>

									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
										
										<!-- REMITTER INFORMATION START -->
										<div class="panel panel-success" style="margin:0px 0px 20px;">
											
											<div class="panel-heading"> <i class="fa fa-user" aria-hidden="true"></i> Remitter </div>
											
											<div class="panel-body">
												
												<!-- RECIPIENT REMITTER START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														
														<div class="form-group">
															
															<label for="recipient_remitter"> Remitter:<span style="color:#E85445;">*</span> </label>
															
															<select id="recipient_remitter" name="recipient_remitter" class="chosen-select form-control" required="required">
																
																<option value=""></option>
																
																@if( isset( $recipient_remitter ) AND ! empty( $recipient_remitter ) )
																
																	<option value="{{ $recipient_remitter[ 'id' ] }}" selected="selected"> {{ $recipient_remitter[ 'firstname' ] }} {{ $recipient_remitter[ 'lastname' ] }} | {{ $recipient_remitter[ 'email' ] }} </option>
																
																@endif
																
															</select>

														</div>
													
													</div>

												</div>
												<!-- RECIPIENT REMITTER END -->
												
												
												<!-- RECIPIENT RELATIONSHIP START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														
														<div class="form-group">
															
															<label for="recipient_relationship"> Relationship: </label>
															
															<select id="recipient_relationship" name="recipient_relationship" class="form-control chosen-select" data-placeholder="Relationship">
															
																<option value=""></option>
																
																@foreach ( $relationships as $relationship )
																
																	<option {{ ( isset( $recipient_details[ 'remitter_relationship_id' ] ) && ! empty( $recipient_details[ 'remitter_relationship_id' ] ) && $recipient_details[ 'remitter_relationship_id' ] == $relationship[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $relationship[ 'id' ] }}"> {{ $relationship[ 'name' ] }} </option>
																
																@endforeach
															
															</select>

														</div>
													
													</div>
												
												</div>
												<!-- RECIPIENT RELATIONSHIP END -->
												
												<!-- RECIPIENT OTHER RELATIONSHIP START -->
												<div id="recipient_other_relationship_container" class="row" {{ ( isset( $recipient_details[ 'remitter_relationship_id' ] ) && ! empty( $recipient_details[ 'remitter_relationship_id' ] ) && $recipient_details[ 'remitter_relationship_id' ] == 1 ? 'style=display:block;' : 'style=display:none;' ) }} >
												
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													
														<div class="form-group">
														
															<label for="recipient_other_relationship"> Other relationship:<span style="color:#E85445;">*</span> </label>
															
															<input type="text" id="recipient_other_relationship" name="recipient_other_relationship" class="form-control recipient_other_relationship" placeholder="Other relationship" value="{{ ( isset( $recipient_details[ 'remitter_relationship_id' ] ) && ! empty( $recipient_details[ 'remitter_relationship_id' ] ) && $recipient_details[ 'remitter_relationship_id' ] == 1 ? $recipient_other_relationship[ 'relationship' ] : '' ) }}">
														
														</div>
													
													</div>
												
												</div>
												<!-- RECIPIENT OTHER RELATIONSHIP END -->


											</div>
											
											
										</div>
										<!-- REMITTER INFORMATION END -->
										
									</div>
									
								</div>
									
							</div>
							<!-- RECIPIENT PERSONAL TAB END -->
							
							<!-- RECIPIENT ADDRESS TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="recipient_address_tab" aria-labelledby="address-tab">
								
								<div class="row">
									
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
										<!-- ADDRESS INFORMATION START -->
										<div class="panel panel-default" style="margin:0px;">
											
											<div class="panel-body">
												
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
														
														<a href="#" class="btn btn-primary btn-xs add_address_btn" style="margin: 0px 0px 10px;"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add address </a>
													
													</div>
												
												</div>
												
												<!-- ADDRESS LIST START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													
														<div class="panel panel-default">
															
															<div class="panel-heading"> <i class="fa fa-list-alt" aria-hidden="true"></i> Address list </div>

															<div class="panel-body">
																
																<!-- RECIPIENT ADDRESS CONTAINER START -->
																<div class="recipient_address_container">
																	
																	@if ( isset( $recipient_addresses ) AND ! empty( $recipient_addresses ) AND COUNT( $recipient_addresses ) )
																		
																		@foreach( $recipient_addresses AS $recipient_address )
																			
																			<div id="recipient_address_item_{{ $recipient_address[ 'id' ] }}_hash" class="well">
																			
																				<input type="hidden" id="recipient_address_id_{{ $recipient_address[ 'id' ] }}_hash" name="recipient_address_old[{{ $recipient_address[ 'id' ] }}_hash][id]" value="{{ ( isset( $recipient_address[ 'id' ] ) && ! empty( $recipient_address[ 'id' ] ) ? $recipient_address[ 'id' ] : 0 ) }}">
																			
																				<!-- RECIPIENT ADDRESS ITEM REMOVE START -->
																				<div class="row">
																			
																					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">

																						<a href="#" class="btn btn-danger btn-xs remove_recipient_address_item" rel="{{ $recipient_address[ 'id' ] }}_hash" style="margin:0px 0px 15px;"><i class="fa fa-trash"></i> Remove</a>
																						
																					</div>
																				
																				</div>
																				<!-- RECIPIENT ADDRESS ITEM REMOVE END -->
																				
																				<div class="row">
																				
																					<!-- RECIPIENT ADDRESS COUNTRY START -->
																					<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
																					
																						<div class="form-group">
																						
																							<label for="recipient_country_{{ $recipient_address[ 'id' ] }}_hash"> Country:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="recipient_country_{{ $recipient_address[ 'id' ] }}_hash" name="recipient_address_old[{{ $recipient_address[ 'id' ] }}_hash][country]" class="form-control chosen-select recipient_country" data-hash="{{ $recipient_address[ 'id' ] }}_hash" required="required" data-placholder="Country">
																								
																								<option value=""></option>
																								
																								@foreach( $countries as $country )
																								
																									<option {{ ( isset( $recipient_address[ 'country_id' ] ) && ! empty( $recipient_address[ 'country_id' ] ) && $recipient_address[ 'country_id' ] == $country[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $country[ 'id' ] }}"> {{ $country[ 'en_short_name' ] }} </option>

																								@endforeach
																								
																							</select>
																						
																						</div>
																					
																					</div>
																					<!-- RECIPIENT ADDRESS COUNTRY END -->
																					
																					<!-- RECIPIENT ADDRESS PROVINCE START -->
																					<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
																						
																						<div class="form-group">
																						
																							<label for="recipient_state_{{ $recipient_address[ 'id' ] }}_hash"> Province:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="recipient_state_{{ $recipient_address[ 'id' ] }}_hash" name="recipient_address_old[{{ $recipient_address[ 'id' ] }}_hash][state]" class="form-control chosen-select recipient_state" data-hash="{{ $recipient_address[ 'id' ] }}_hash" required="required" data-placeholder="Province">
																								
																								<option value=""></option>
																								
																								@foreach( $states as $state )
																								
																									<option {{ ( isset( $recipient_address[ 'state_id' ] ) && ! empty( $recipient_address[ 'state_id' ] ) && $recipient_address[ 'state_id' ] == $state[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $state[ 'id' ] }}"> {{ $state[ 'name' ] }} </option>
																								
																								@endforeach

																							</select>
																						
																						</div>
																					
																					</div>
																					<!-- RECIPIENT ADDRESS PROVINCE END -->
																					
																					<!-- RECIPIENT ADDRESS CITY START -->
																					<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
																						
																						<div class="form-group">
																						
																							<label for="recipient_city_{{ $recipient_address[ 'id' ] }}_hash"> City:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="recipient_city_{{ $recipient_address[ 'id' ] }}_hash" name="recipient_address_old[{{ $recipient_address[ 'id' ] }}_hash][city]" class="form-control chosen-select recipient_city" data-hash="{{ $recipient_address[ 'id' ] }}_hash" required="required" data-placeholder="City">
																								
																								<option value=""></option>
																								
																								@foreach( $cities as $city )
																								
																									@if( $recipient_address[ 'state_id' ] == $city[ 'state_id' ] )
																								
																										<option {{ ( isset( $recipient_address[ 'city_id' ] ) && ! empty( $recipient_address[ 'city_id' ] ) && $recipient_address[ 'city_id' ] == $city[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $city[ 'id' ] }}"> {{ $city[ 'name' ] }} </option>
																								
																									@endif
																								
																								@endforeach
																					
																							</select>
																						
																						</div>
																					
																					</div>
																					<!-- RECIPIENT ADDRESS CITY END -->
																					
																					<!-- RECIPIENT POSTAL START -->
																					<div class="col-lg-2 col-md-2 col-sm-12 col-sm-12">
																						
																						<div class="form-group">
																						
																							<label for="recipient_postal_{{ $recipient_address[ 'id' ] }}_hash"> Postal:<span style="color:#E85445;">*</span> </label>
																							
																							<input type="text" id="recipient_postal_{{ $recipient_address[ 'id' ] }}_hash" name="recipient_address_old[{{ $recipient_address[ 'id' ] }}_hash][postal]" class="form-control recipient_postal" placeholder="Postal" required="required" value="{{ ( isset( $recipient_address[ 'postal_code' ] ) && ! empty( $recipient_address[ 'postal_code' ] ) ? $recipient_address[ 'postal_code' ] : '' ) }}">
																						
																						</div>
																					
																					</div>
																					<!-- RECIPIENT POSTAL END -->
																					
																					<!-- RECIPIENT ADDRESS IS PREFERRED START -->
																					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-left">
																						
																						<div class="form-group text-center">
																						
																							<label for="recipient_address_is_preferred_{{ $recipient_address[ 'id' ] }}_hash"> Use </label>
																							
																							<br>
																							
																							<input type="radio" id="recipient_address_is_preferred_{{ $recipient_address[ 'id' ] }}_hash" name="recipient_address_old[{{ $recipient_address[ 'id' ] }}_hash][is_preferred]" class="recipient_address_is_preferred" {{ ( isset( $recipient_address[ 'is_preferred' ] ) && ! empty( $recipient_address[ 'is_preferred' ] ) && $recipient_address[ 'is_preferred' ] ? 'checked="checked"' : '' ) }} value="1">
																						
																						</div>
																					
																					</div>
																					<!-- RECIPIENT ADDRESS IS PREFERRED END -->
																				
																				</div>
																				
																				<div class="row">
																					
																					<!-- RECIPIENT CURRENT ADDRESS START -->
																					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																						
																						<div class="form-group">
																						
																							<label for="recipient_current_address_{{ $recipient_address[ 'id' ] }}_hash"> Current address:<span style="color:#E85445;">*</span> </label>
																							
																							<textarea id="recipient_current_address_{{ $recipient_address[ 'id' ] }}_hash" name="recipient_address_old[{{ $recipient_address[ 'id' ] }}_hash][current_address]" placeholder="Current address" class="form-control" rows="2" required="required">{{ ( isset( $recipient_address[ 'current_address' ] ) && ! empty( $recipient_address[ 'current_address' ] ) ? $recipient_address[ 'current_address' ] : '' ) }}</textarea>
																						
																						</div>
																					
																					</div>
																					<!-- RECIPIENT CURRENT ADDRESS END -->
																				
																					<!-- RECIPIENT PERMANENT ADDRESS START -->
																					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																						
																						<div class="form-group">
																						
																							<label for="recipient_permanent_address_{{ $recipient_address[ 'id' ] }}_hash"> Permanent address:<span style="color:#E85445;">*</span> </label>
																							
																							<textarea id="recipient_permanent_address_{{ $recipient_address[ 'id' ] }}_hash" name="recipient_address_old[{{ $recipient_address[ 'id' ] }}_hash][permanent_address]" placeholder="Permanent address" class="form-control" rows="2" required="required">{{ ( isset( $recipient_address[ 'permanent_address' ] ) && ! empty( $recipient_address[ 'permanent_address' ] ) ? $recipient_address[ 'permanent_address' ] : '' ) }}</textarea>
																						
																						</div>
																					
																					</div>
																					<!-- RECIPIENT PERMANENT ADDRESS END -->
																				
																				</div>

																			</div>
																		
																		@endforeach
																	
																	@else
																	
																		<div class="alert alert-danger" role="alert" style="margin:10px 0px;">
																			
																			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No address for this recipient! </h5>
																		
																		</div>
																	
																	@endif
																
																</div>
																<!-- RECIPIENT ADDRESS CONTAINER END -->
															
															</div>

														</div>
													
													</div>
												
												</div>
												<!-- ADDRESS LIST END -->
											
											</div>

										</div>
										<!-- ADDRESS INFORMATION END -->
									
									</div>
									
								</div>
							
							</div>
							<!-- RECIPIENT ADDRESS TAB END -->
							
							<!-- RECIPIENT CONTACT TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="recipient_contact_tab" aria-labelledby="contact-tab">
							
								<div class="row">
									
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
										<!-- CONTACT INFORMATION START -->
										<div class="panel panel-default" style="margin:0px;">
											
											<div class="panel-body">
												
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
														
														<a href="#" class="btn btn-primary btn-xs add_contact_btn" style="margin: 0px 0px 10px;"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add contact </a>
													
													</div>
												
												</div>

												<!-- CONTACT LIST START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													
														<div class="panel panel-default">
															
															<div class="panel-heading"> <i class="fa fa-list-alt" aria-hidden="true"></i> Contact list </div>

															<div class="panel-body">
																
																<!-- RECIPIENT CONTACT CONTAINER START -->
																<div class="recipient_contact_container">
																	
																	@if ( isset( $recipient_contacts ) AND ! empty( $recipient_contacts ) AND COUNT( $recipient_contacts ) )
																		
																		@foreach( $recipient_contacts AS $recipient_contact )
																		
																			<div id="recipient_contact_item_{{ $recipient_contact[ 'id' ] }}_hash" class="well well-sm">
																			
																				<input type="hidden" id="recipient_contact_id_{{ $recipient_contact[ 'id' ] }}_hash" name="recipient_contact_old[{{ $recipient_contact[ 'id' ] }}_hash][id]" value="{{ ( isset( $recipient_contact[ 'id' ] ) && ! empty( $recipient_contact[ 'id' ] ) ? $recipient_contact[ 'id' ] : 0 ) }}">
																			
																				<div class="row">
																					
																					<!-- RECIPIENT CONTACT IS PREFERRED START -->
																					<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 text-center">
																						
																						<div class="form-group">
																						
																							<label for="recipient_contact_is_preferred_{{ $recipient_contact[ 'id' ] }}_hash"> Use </label>
																							
																							<br>
																						
																							<input type="radio" id="recipient_contact_is_preferred_{{ $recipient_contact[ 'id' ] }}_hash" name="recipient_contact_old[{{ $recipient_contact[ 'id' ] }}_hash][is_preferred]" class="recipient_contact_is_preferred" {{ ( isset( $recipient_contact[ 'is_preferred' ] ) && ! empty( $recipient_contact[ 'is_preferred' ] ) && $recipient_contact[ 'is_preferred' ] ? 'checked="checked"' : '' ) }} value="1">
																					
																						</div>
																						
																					</div>
																					<!-- RECIPIENT CONTACT IS PREFERRED END -->
																					
																					<!-- RECIPIENT CONTACT TYPE START -->
																					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																					
																						<div class="form-group">
																						
																							<label for="recipient_contact_type_{{ $recipient_contact[ 'id' ] }}_hash"> Type:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="recipient_contact_type_{{ $recipient_contact[ 'id' ] }}_hash" name="recipient_contact_old[{{ $recipient_contact[ 'id' ] }}_hash][type]" class="form-control chosen-select recipient_contact_type" required="required" data-placeholder="Type">
																							
																								<option value=""></option>
																								
																								@foreach( $contact_types AS $contact_type )
																								
																									<option {{ ( isset( $recipient_contact[ 'contact_type_id' ] ) && ! empty( $recipient_contact[ 'contact_type_id' ] ) && $recipient_contact[ 'contact_type_id' ] == $contact_type[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $contact_type[ 'id' ] }}"> {{ $contact_type[ 'name' ] }} </option>
																								
																								@endforeach
																							
																							</select>
																						
																						</div>
																					
																					</div>
																					<!-- RECIPIENT CONTACT TYPE END -->
																					
																					<!-- RECIPIENT CONTACT VALUE START -->
																					<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
																					
																						<div class="form-group">
																						
																							<label for="recipient_contact_value_{{ $recipient_contact[ 'id' ] }}_hash"> Contact:<span style="color:#E85445;">*</span> </label>
																							
																							<input type="text" id="recipient_contact_value_{{ $recipient_contact[ 'id' ] }}_hash" name="recipient_contact_old[{{ $recipient_contact[ 'id' ] }}_hash][value]" class="form-control recipient_contact_value" placeholder="Contact" required="required" value="{{ ( isset( $recipient_contact[ 'contact' ] ) && ! empty( $recipient_contact[ 'contact' ] ) ? $recipient_contact[ 'contact' ] : '' ) }}">
																							
																						</div>
																					
																					</div>
																					<!-- RECIPIENT CONTACT VALUE END -->

																					<!-- RECIPIENT CONTACT ITEM REMOVE START -->
																					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
																					
																						<a href="#" class="btn btn-danger btn-xs remove_recipient_contact_item" rel="{{ $recipient_contact[ 'id' ] }}_hash" style="margin:30px 0px 0px;"><i class="fa fa-trash"></i> Remove</a>
																					
																					</div>
																					<!-- RECIPIENT CONTACT ITEM REMOVE END -->

																				</div>
																			
																			</div>
																		
																		@endforeach
																		
																	@else
																		
																		<div class="alert alert-danger" role="alert" style="margin:10px 0px;">
																			
																			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No contact for this recipient! </h5>
																		
																		</div>
																	
																	@endif
																	
																</div>
																<!-- RECIPIENT CONTACT CONTAINER END -->

															</div>

														</div>
													
													</div>
												
												</div>
												<!-- CONTACT LIST END -->
											
											</div>
											
										</div>
										<!-- CONTACT INFORMATION END -->
										
									</div>
									
								</div>
							
							</div>
							<!-- REMITTER CONTACT TAB END -->
							
							<!-- RECIPIENT BANK TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="recipient_bank_tab" aria-labelledby="id-tab">
								
								<div class="row">
									
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								
										<!-- BANK INFORMATION START -->
										<div class="panel panel-default" style="margin:0px;">
											
											<div class="panel-body">
												
												<!-- ADD NEW BANK BUTTON START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
														
														<a href="#" class="btn btn-primary btn-xs add_bank_btn" style="margin: 0px 0px 10px;"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add bank </a>
													
													</div>
												
												</div>
												<!-- ADD NEW BANK BUTTON END -->
												
												<!-- BANK LIST START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													
														<div class="panel panel-default">
															
															<div class="panel-heading"> <i class="fa fa-list-alt" aria-hidden="true"></i> Bank list </div>

															<div class="panel-body">
																
																<div class="recipient_bank_container">
																	
																	@if ( isset( $recipient_banks ) AND ! empty( $recipient_banks ) AND COUNT( $recipient_banks ) )
																		
																		@foreach( $recipient_banks AS $recipient_bank )
																		
																			<div id="recipient_bank_item_{{ $recipient_bank[ 'id' ] }}_hash" class="well well-sm">
																			
																				<input type="hidden" id="recipient_bank_id_{{ $recipient_bank[ 'id' ] }}_hash" name="recipient_bank_old[{{ $recipient_bank[ 'id' ] }}_hash][id]" value="{{ ( isset( $recipient_bank[ 'id' ] ) && ! empty( $recipient_bank[ 'id' ] ) ? $recipient_bank[ 'id' ] : 0 ) }}">

																				<div class="row">
																					
																					<!-- RECIPIENT BANK IS PREFERRED START -->
																					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center">
																						
																						<div class="form-group">
																						
																							<label for="recipient_bank_type_is_preferred_{{ $recipient_bank[ 'id' ] }}_hash"> Use </label>
																							
																							<br>
																						
																							<input type="radio" id="recipient_bank_type_is_preferred_{{ $recipient_bank[ 'id' ] }}_hash" name="recipient_bank_old[{{ $recipient_bank[ 'id' ] }}_hash][is_preferred]" class="recipient_bank_is_preferred" {{ ( isset( $recipient_bank[ 'is_preferred' ] ) && ! empty( $recipient_bank[ 'is_preferred' ] ) && $recipient_bank[ 'is_preferred' ] ? 'checked="checked"' : '' ) }} value="1">
																					
																						</div>
																						
																					</div>
																					<!-- RECIPIENT BANK IS PREFERRED END -->
																					
																					<!-- RECIPIENT RECEIVE OPTION LIST START -->
																					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
																					
																						<div class="form-group">
																						
																							<label for="recipient_bank_{{ $recipient_bank[ 'id' ] }}_hash"> Bank:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="recipient_bank_{{ $recipient_bank[ 'id' ] }}_hash" name="recipient_bank_old[{{ $recipient_bank[ 'id' ] }}_hash][receive_option_list_id]" class="form-control chosen-select recipient_bank" required="required" data-hash="{{ $recipient_bank[ 'id' ] }}_hash" data-placeholder="Bank">
																							
																								<option value=""></option>
																								
																								@foreach( $bank_types AS $bank_type )
																								
																									<option {{ ( isset( $recipient_bank[ 'receive_option_list_id' ] ) && ! empty( $recipient_bank[ 'receive_option_list_id' ] ) && $recipient_bank[ 'receive_option_list_id' ] == $bank_type[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $bank_type[ 'id' ] }}"> {{ $bank_type[ 'name' ] }} </option>
																								
																								@endforeach
																							
																							</select>
																						
																						</div>
																					
																					</div>
																					<!-- RECIPIENT RECEIVE OPTION LIST END -->
																					
																					<!-- RECIPIENT BANK ITEM REMOVE START -->
																					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
																					
																						<a href="#" class="btn btn-danger btn-xs remove_recipient_bank_item" rel="{{ $recipient_bank[ 'id' ] }}_hash" style="margin:30px 0px 0px;"><i class="fa fa-trash"></i> Remove</a>
																					
																					</div>
																					<!-- RECIPIENT BANK ITEM REMOVE END -->
																					
																				</div>
																				
																				<div class="row">
																					
																					<!-- RECIPIENT BANK ACCOUNT NAME START -->
																					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																					
																						<div class="form-group">
																						
																							<label for="recipient_account_name_{{ $recipient_bank[ 'id' ] }}_hash"> Account name: </label>
																							
																							<input type="text" id="recipient_account_name_{{ $recipient_bank[ 'id' ] }}_hash" name="recipient_bank_old[{{ $recipient_bank[ 'id' ] }}_hash][account_name]" class="form-control recipient_account_name" placeholder="Account name" value="{{ ( isset( $recipient_bank[ 'account_name' ] ) && ! empty( $recipient_bank[ 'account_name' ] ) ? $recipient_bank[ 'account_name' ] : '' ) }}">
																							
																						</div>
																					
																					</div>
																					<!-- RECIPIENT BANK ACCOUNT NAME END -->
																					
																					<!-- RECIPIENT BANK ACCOUNT BRANCH START -->
																					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																					
																						<div class="form-group">
																						
																							<label for="recipient_account_branch_{{ $recipient_bank[ 'id' ] }}_hash"> Account branch: </label>
																							
																							<input type="text" id="recipient_account_branch_{{ $recipient_bank[ 'id' ] }}_hash" name="recipient_bank_old[{{ $recipient_bank[ 'id' ] }}_hash][account_branch]" class="form-control recipient_account_branch" placeholder="Account branch" value="{{ ( isset( $recipient_bank[ 'account_branch' ] ) && ! empty( $recipient_bank[ 'account_branch' ] ) ? $recipient_bank[ 'account_branch' ] : '' ) }}">
																							
																						</div>
																					
																					</div>
																					<!-- RECIPIENT BANK ACCOUNT BRANCH END -->
																					
																					<!-- RECIPIENT BANK ACCOUNT NO START -->
																					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																					
																						<div class="form-group">
																						
																							<label for="recipient_account_no_{{ $recipient_bank[ 'id' ] }}_hash"> Account number:<span style="color:#E85445;">*</span> </label>
																							
																							<input type="text" id="recipient_account_no_{{ $recipient_bank[ 'id' ] }}_hash" name="recipient_bank_old[{{ $recipient_bank[ 'id' ] }}_hash][account_no]" class="form-control recipient_account_no" placeholder="Account no." required="required" value="{{ ( isset( $recipient_bank[ 'account_no' ] ) && ! empty( $recipient_bank[ 'account_no' ] ) ? $recipient_bank[ 'account_no' ] : '' ) }}">
																							
																						</div>
																					
																					</div>
																					<!-- RECIPIENT BANK ACCOUNT NO END -->

																				</div>
																				
																				<!-- RECIPIENT OTHER BANK START -->
																				<div id="recipient_other_bank_container_{{ $recipient_bank[ 'id' ] }}_hash" class="row" {{ ( isset( $recipient_bank[ 'receive_option_list_id' ] ) && ! empty( $recipient_bank[ 'receive_option_list_id' ] ) && $recipient_bank[ 'receive_option_list_id' ] == 41 ? 'style=display:block;' : 'style=display:none;' ) }} >
																				
																					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																					
																						<div class="form-group">
																						
																							<label for="recipient_other_bank_{{ $recipient_bank[ 'id' ] }}_hash"> Other bank:<span style="color:#E85445;">*</span> </label>
																							
																							<input type="text" id="recipient_other_bank_{{ $recipient_bank[ 'id' ] }}_hash" name="recipient_bank_old[{{ $recipient_bank[ 'id' ] }}_hash][bank_name]" class="form-control recipient_other_bank" placeholder="Other bank" value="{{ ( isset( $recipient_bank[ 'receive_option_list_id' ] ) && ! empty( $recipient_bank[ 'receive_option_list_id' ] ) && $recipient_bank[ 'receive_option_list_id' ] == 41 ? $recipient_bank[ 'bank_name' ] : '' ) }}">
																						
																						</div>
																					
																					</div>
																				
																				</div>
																				<!-- RECIPIENT OTHER BANK END -->
																				
																			</div>
																		
																		@endforeach
																	
																	@else
																	
																		<div class="alert alert-danger" role="alert" style="margin:10px 0px;">
																			
																			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No bank for this recipient! </h5>
																		
																		</div>
																	
																	@endif
																
																</div>

															</div>

														</div>
													
													</div>
												
												</div>
												<!-- BANK LIST END -->
												
											</div>
											
										</div>
										<!-- BANK INFORMATION END -->
										
									</div>
										
								</div>
								
							</div>
							<!-- RECIPIENT ID TAB END -->
							
							<!-- RECIPIENT NOTE TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="recipient_note_tab" aria-labelledby="note-tab">
								
								<div class="row">
								
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
										<div class="form-group">
											
											<label for="recipient_added_note"> Added note <small>(20 chars min, 1000 max)</small>: </label>
											
											<textarea id="recipient_added_note" class="form-control" name="recipient_added_note" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="1000" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" rows="10">{{ ( isset( $recipient_notes[ 'note' ] ) && ! empty( $recipient_notes[ 'note' ] ) ? $recipient_notes[ 'note' ] : '' ) }}</textarea>

										</div>
										
									</div>

								</div>
								
							</div>
							<!-- RECIPIENT NOTE TAB END -->
							
							<!-- RECIPIENT HISTORY TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="recipient_history_tab" aria-labelledby="history-tab">
							
								<div class="row">
								
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									
										@if( isset( $histories ) AND ! empty( $histories ) )
										
											<div class="panel panel-default">

												<div class="panel-heading"> 
													
													<i class="fa fa-list-alt" aria-hidden="true"></i> History list 
													
												</div>

												<div class="panel-body">
													
													<div class="scrollbar-light">
																								
														<div class="table-responsive">
														
															<table class="table table-striped table-bordered table-hover" style="margin: 0px;">
																
																<thead>
																
																	<tr> 
																		
																		<th style="width: 20%;"> <small> Field </small> </th>
																		
																		<th style="width: 30%;"> <small> Old value </small> </th>
																		
																		<th style="width: 30%;"> <small> New value </small> </th>
																		
																		<th style="width: 20%;"> <small> CSR </small> </th>

																	</tr>
																
																</thead>
																
																<tbody>
														
																	@foreach( $histories as $history )

																		<tr>

																			<td> <small> {{ $history[ 'field_name' ] }} </small> </td>
																			
																			<td> <small> {{ $history[ 'old_value' ] }} </small> </td>
																			
																			<td> <small> {{ $history[ 'new_value' ] }} </small> </td>
																			
																			<td> <small> {{ $history[ 'full_name' ] }} </small> </td>
																			
																		</tr>
																			
																	@endforeach
																
																</tbody>
															
															</table>
															
														</div>
													
													</div>
													
												</div>
												
											</div>
											
										@else
										
											<div class="alert alert-danger" role="alert" style="margin:10px 0px;">
										
												<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No history changes for this recipient! </h5>
											
											</div>
											
										@endif
											
									</div>
								
								</div>
							
							</div>
							<!-- RECIPIENT HISTORY TAB END -->
						
						</div>
						<!-- RECIPIENT FORM TAB CONTENT END -->
					
					</div>
					<!-- RECIPIENT TABS END -->
					
				</div>

				<div class="modal-footer">
					
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" style="margin:0px;"> <i class="fa fa-times" aria-hidden="true"></i> Close </button>

					<button type="submit" class="btn btn-sm btn-primary" style="margin:0px;"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save </button>
					
				</div>

			</form>
			
		</div>

	</div>

</div>
