<div id="remittance_modal" class="modal fade" tabindex="-1" role="dialog">
		
	<div class="modal-dialog modal-lg" role="document">
		
		<div class="modal-content">
			
			<form method="POST" action="{{ URL('/iremit/remittance/form/post') }}" id="remittance_form" class="form-horizontal" data-parsley-validate>
				
				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						
						<span aria-hidden="true"> &times; </span>
						
					</button>

					<h4 class="modal-title"> <i class="fa fa-wpforms"></i> Remittance Form </h4>

				</div>

				<div class="modal-body">
					
					<!-- REMITTANCE ID START -->
					<input type="hidden" name="remittance_id" id="remittance_id" class="form-control" value="{{ ( isset( $remittance_id ) && ! empty( $remittance_id ) ? $remittance_id : 0 ) }}">
					<!-- REMITTANCE ID END -->
					
					<!-- REMITTANCE TABS START -->
					<div role="tabpanel" data-example-id="togglable-tabs">
						
						<!-- REMITTANCE FORM NAV START -->
						<ul id="remittance_form_nav" class="nav nav-tabs bar_tabs" role="tablist">
							
							<!-- REMITTANCE DETAIL NAV START -->
							<li role="presentation" class="active">
								
								<a href="#remittance_detail_tab" id="detail-tabb" role="tab" data-toggle="tab" aria-controls="detail" aria-expanded="false"> <i class="fa fa-money" aria-hidden="true"></i> Detail </a>
								
							</li>
							<!-- REMITTANCE DETAIL NAV END -->
							
							<!-- REMITTANCE ADDITIONAL REQUIREMENT NAV START -->
							<li id="additional_requirement_nav" role="presentation" style="{{ ( isset( $remittance_details[ 'amount_sent' ] ) && ! empty( $remittance_details[ 'amount_sent' ] ) && $remittance_details[ 'amount_sent' ] >= 10000 ? 'display: inline-block;' : 'display: none;' ) }}">
								
								<a href="#remittance_additional_requirement_tab" id="additional_requirement-tabb" role="tab" data-toggle="tab" aria-controls="additional_requirement" aria-expanded="false"> <i class="fa fa-list-alt" aria-hidden="true"></i> Additional requirement </a>
								
							</li>
							<!-- REMITTANCE ADDITIONAL REQUIREMENT NAV START -->
							
							<!-- REMITTANCE REMITTER AND RECIPEINT NAV START -->
							<li role="presentation">
								
								<a href="#remittance_remitter_and_recipient_tab" id="remitter_and_recipient-tabb" role="tab" data-toggle="tab" aria-controls="remitter_and_recipient" aria-expanded="false"> <i class="fa fa-users" aria-hidden="true"></i> Remitter &amp; Recipient </a>
								
							</li>
							<!-- REMITTANCE REMITTER AND RECIPEINT NAV START -->
							
							<!-- REMITTANCE NOTE NAV START -->
							<li role="presentation">
								
								<a href="#remittance_note_tab" id="note-tabb" role="tab" data-toggle="tab" aria-controls="note" aria-expanded="false"> <i class="fa fa-sticky-note" aria-hidden="true"></i> Note </a>
								
							</li>
							<!-- REMITTANCE NOTE NAV END -->
							
							<!-- REMITTANCE SETTING NAV START -->
							<li role="presentation">
								
								<a href="#remittance_setting_tab" id="setting-tabb" role="tab" data-toggle="tab" aria-controls="note" aria-expanded="false"> <i class="fa fa-cog" aria-hidden="true"></i> Setting </a>
								
							</li>
							<!-- REMITTANCE SETTING NAV END -->
							
							<!-- REMITTANCE HISTORY NAV START -->
							<li role="presentation">
								
								<a href="#remittance_history_tab" id="history-tabb" role="tab" data-toggle="tab" aria-controls="history" aria-expanded="false"> <i class="fa fa-clock-o" aria-hidden="true"></i> History </a>
								
							</li>
							<!-- REMITTANCE HISTORY NAV END -->

						</ul>
						<!-- REMITTANCE FORM NAV END -->
						
						<!-- REMITTANCE FORM TAB CONTENT START -->
						<div id="remittance_form_tab_content" class="tab-content">
							
							<!-- REMITTANCE DETAIL TAB START -->
							<div role="tabpanel" class="tab-pane fade active in" id="remittance_detail_tab" aria-labelledby="detail-tab">

								<div class="row">

									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

										<div class="row">
											
											<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

												<div class="panel panel-primary" style="margin: 20px 0px;">
													
													<div class="panel-heading"> <i class="fa fa-money" aria-hidden="true"></i> Details </div>
													
													<div class="panel-body">

														<div class="row">
															
															<!-- BANK SOURCE START -->
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remittance_bank_source_id"> Bank source:<span style="color:#E85445;">*</span> </label>

																	<select id="remittance_bank_source_id" name="remittance_bank_source_id" class="form-control chosen-select" data-placeholder="Bank source" required="required">
																	
																		<option value=""></option>
																		
																		@if( isset( $bank_sources ) && ! empty( $bank_sources ) )
																		
																			@foreach( $bank_sources AS $bank_source )
																			
																				<option value="{{ $bank_source[ 'id' ] }}" {{ ( isset( $remittance_details[ 'bank_source_id' ] ) && ! empty( $remittance_details[ 'bank_source_id' ] ) && $remittance_details[ 'bank_source_id' ] == $bank_source[ 'id' ] ? "selected='selected'" : "" ) }}> {{ ucfirst( $bank_source[ 'shortname' ] ) }} </option>
																			
																			@endforeach
																		
																		@endif
																	
																	</select>
																	
																</div>
															
															</div>
															<!-- BANK SOURCE END -->
															
															<!-- REMITTANCE STATUS START -->
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remittance_status_id"> Remittance status:<span style="color:#E85445;">*</span> </label>

																	<select id="remittance_status_id" name="remittance_status_id" class="form-control chosen-select" data-placeholder="Status" required="required">
																		
																		<option value=""></option>
																		
																		@if( isset( $remittance_statuses ) && ! empty( $remittance_statuses ) )
																		
																			@foreach( $remittance_statuses AS $remittance_status )
																			
																				<option value="{{ $remittance_status[ 'id' ] }}" {{ ( isset( $remittance_details[ 'status_id' ] ) && ! empty( $remittance_details[ 'status_id' ] ) && $remittance_details[ 'status_id' ] == $remittance_status[ 'id' ] ? "selected='selected'" : "" ) }} > {{ $remittance_status[ 'name' ] }} </option>
																			
																			@endforeach
																		
																		@endif
																	
																	</select>
																	
																</div>
															
															</div>
															<!-- REMITTANCE STATUS END -->
															
															<!-- PROMO CODE START -->
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

																<div class="form-group">
																	
																	<label for="remittance_promo_code_id"> Promo code: </label>

																	<select id="remittance_promo_code_id" name="remittance_promo_code_id" class="form-control chosen-select" data-placeholder="Promo code">
																		
																		<option value=""></option>
																		
																		@if( isset( $promo_codes ) && ! empty( $promo_codes ) )
																			
																			@foreach( $promo_codes AS $promo_code )
																			
																				<option value="{{ $promo_code[ 'id' ] }}" {{ ( isset( $remittance_details[ 'promo_code_id' ] ) && ! empty( $remittance_details[ 'promo_code_id' ] ) && $remittance_details[ 'promo_code_id' ] == $promo_code[ 'id' ] ? "selected='selected'" : "" ) }}> {{ $promo_code[ 'name' ] }} </option>
																			
																			@endforeach
																		
																		@endif;
																	
																	</select>
																	
																</div>
															
															</div>
															<!-- PROMO CODE END -->
															
														</div>
														
														<hr style="margin: 20px 0px;">
														
														<div class="row">
															
															<!-- REASON AND OTHER REASON START -->
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
															
																<div class="form-group">
																	
																	<label for="remittance_reason_id"> Reason:<span style="color:#E85445;">*</span> </label>

																	<select id="remittance_reason_id" name="remittance_reason_id" class="form-control chosen-select" data-placeholder="Reason" required="required">
																	
																		<option value=""></option>
																		
																		@if( isset( $reasons ) && ! empty( $reasons ) )
																		
																			@foreach( $reasons AS $reason )
																			
																				<option value="{{ $reason[ 'id' ] }}" {{ ( isset( $remittance_details[ 'reason_id' ] ) && ! empty( $remittance_details[ 'reason_id' ] ) && $remittance_details[ 'reason_id' ] == $reason[ 'id' ] ? "selected='selected'" : "" ) }}> {{ $reason[ 'name' ] }} </option>
																			
																			@endforeach
																		
																		@endif
																	
																	</select>
																	
																</div>
																
																<div id="other_reason_container" style="{{ ( isset( $remittance_details[ 'reason_id' ] ) && ! empty( $remittance_details[ 'reason_id' ] ) && $remittance_details[ 'reason_id' ] == 1 ? 'display: inline-block;' : 'display: none;' ) }}">
																	
																	<div class="form-group">
																		
																		<label for="remittance_other_reason"> Other reason:<span style="color:#E85445;">*</span> </label>

																		<input type="text" id="remittance_other_reason" name="remittance_other_reason" class="form-control" placeholder="Other reason" value="{{ ( isset( $remittance_other_details[ 'reason' ] ) && ! empty( $remittance_other_details[ 'reason' ] ) ? $remittance_other_details[ 'reason' ] : "" ) }}">
																		
																	</div>
																	
																</div>

															</div>
															<!-- REASON AND OTHER REASON END -->
															
															<!-- NATURE OF WORK AND OTHER NATURE OF WORK START -->
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remittance_nature_of_work_id"> Nature of work: </label>

																	<select id="remittance_nature_of_work_id" name="remittance_nature_of_work_id" class="form-control chosen-select" data-placeholder="Nature of work">
																	
																		<option value=""></option>
																		
																		@if( isset( $nature_of_works ) && ! empty( $nature_of_works ) )
																		
																			@foreach( $nature_of_works AS $nature_of_work )
																			
																				<option value="{{ $nature_of_work[ 'id' ] }}" {{ ( isset( $remittance_details[ 'nature_of_work_id' ] ) && ! empty( $remittance_details[ 'nature_of_work_id' ] ) && $remittance_details[ 'nature_of_work_id' ] == $nature_of_work[ 'id' ] ? "selected='selected'" : "" ) }}> {{ $nature_of_work[ 'name' ] }} </option>
																			
																			@endforeach
																		
																		@endif
																	
																	</select>
																	
																</div>
																
																<div id="other_nature_of_work_container" style="{{ ( isset( $remittance_details[ 'nature_of_work_id' ] ) && ! empty( $remittance_details[ 'nature_of_work_id' ] ) && $remittance_details[ 'nature_of_work_id' ] == 1 ? 'display: inline-block;' : 'display: none;' ) }}">
																	
																	<div class="form-group">
																		
																		<label for="remittance_other_nature_of_work"> Other nature of work:<span style="color:#E85445;">*</span> </label>

																		<input type="text" id="remittance_other_nature_of_work" name="remittance_other_nature_of_work" class="form-control" placeholder="Other nature of work" value="{{ ( isset( $remittance_other_nature_of_work[ 'nature_of_work' ] ) && ! empty( $remittance_other_nature_of_work[ 'nature_of_work' ] ) ? $remittance_other_nature_of_work[ 'nature_of_work' ] : "" ) }}">
																		
																	</div>
																	
																</div>

															</div>
															<!-- NATURE OF WORK AND OTHER NATURE OF WORK END -->
															
															<!-- SOURCE OF FUND AND OTHER SOURCE OF FUND START -->
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remittance_source_of_fund_id"> Source of funds: </label>

																	<select id="remittance_source_of_fund_id" name="remittance_source_of_fund_id" class="form-control chosen-select" data-placeholder="Source of fund">
																	
																		<option value=""></option>
																		
																		@if( isset( $source_of_funds ) && ! empty( $source_of_funds ) )
																		
																			@foreach( $source_of_funds AS $source_of_fund )
																			
																				<option value="{{ $source_of_fund[ 'id' ] }}" {{ ( isset( $remittance_details[ 'source_of_fund_id' ] ) && ! empty( $remittance_details[ 'source_of_fund_id' ] ) && $remittance_details[ 'source_of_fund_id' ] == $source_of_fund[ 'id' ] ? "selected='selected'" : '' ) }}> {{ $source_of_fund[ 'name' ] }} </option>
																			
																			@endforeach
																		
																		@endif
																	
																	</select>
																	
																</div>
																
																<div id="other_source_of_fund_container" style="{{ ( isset( $remittance_details[ 'source_of_fund_id' ] ) && ! empty( $remittance_details[ 'source_of_fund_id' ] ) && $remittance_details[ 'source_of_fund_id' ] == 1 ? 'display: inline-block;' : 'display: none;' ) }}">
																	
																	<div class="form-group">
																		
																		<label for="remittance_other_source_of_fund"> Other source of fund:<span style="color:#E85445;">*</span> </label>

																		<input type="text" id="remittance_other_source_of_fund" name="remittance_other_source_of_fund" class="form-control" placeholder="Other source of fund" value="{{ ( isset( $remittance_other_source_of_fund[ 'source_of_fund' ] ) && ! empty( $remittance_other_source_of_fund[ 'source_of_fund' ] ) ? $remittance_other_source_of_fund[ 'source_of_fund' ] : "" ) }}">
																		
																	</div>
																	
																</div>
																
															</div>
															<!-- SOURCE OF FUND AND OTHER SOURCE OF FUND END -->
															
														</div>
														
														<hr style="margin: 20px 0px;">
														
														<!-- REMITTANCE RECEIPT START -->
														<div class="row">
														
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remittance_receipt"> Receipt: </label>
																	
																	<div id="remittance_receipt" class="dropzone text-center" style="min-height:200px; height:auto;">
																		
																		<div class="dz-default dz-message" style="margin: 6em 0em;">
																			
																			<span> Drop receipt here to upload </span>

																		</div>
																		
																	</div>

																</div>

															</div>
														
														</div>
														<!-- REMITTANCE RECEIPT END -->

													</div>

												</div>

											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												
												<div class="panel panel-info" style="margin: 20px 0px;">
													
													<div class="panel-heading"> Receive Method </div>
													
													<div class="panel-body">
														
														<div class="row">
														
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															
																<div class="form-group">

																	<label for="remittance_receive_option"> Option:<span style="color:#E85445;">*</span> </label>

																	<select id="remittance_receive_option" name="remittance_receive_option" class="form-control chosen-select" data-placeholder="Select option" required="required">
																		
																		<option value=""></option>
																		
																		@if( isset( $receive_options ) && ! empty( $receive_options ) )
																		
																			@foreach( $receive_options AS $receive_option )
																			
																				<option value="{{ $receive_option[ 'id' ] }}"> {{ $receive_option[ 'name' ] }} </option>
																			
																			@endforeach
																		
																		@endif
																	
																	</select>
																		
																</div>
															
															</div>
														
														</div>
													
														<div class="row">
														
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															
																<div class="form-group">

																	<label for="remittance_receive_option_list"> List:<span style="color:#E85445;">*</span> </label>

																	<select id="remittance_receive_option_list" name="remittance_receive_option_list" class="form-control remittance_receive_option_list chosen-select" data-placeholder="Select list" required="required">
																	
																		<option value=""></option>
																	
																	</select>
																		
																</div>
															
															</div>
														
														</div>
													
													</div>
												
												</div>
												
												<div class="panel panel-success" style="margin: 0px;">
													
													<div class="panel-heading"> <i class="fa fa-calculator"></i> Computation </div>
													
													<div class="panel-body">
														
														<div class="row">

															<!-- FOREX START -->
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remittance_forex_rate"> Forex rate:<span style="color:#E85445;">*</span> </label>
																	
																	@if( $action == 'edit' )
																	
																		<input type="text" class="form-control" id="remittance_forex_rate" name="remittance_forex_rate" placeholder="Forex rate" required="required" value="{{ ( isset( $remittance_details[ 'forex_rate' ] ) && ! empty( $remittance_details[ 'forex_rate' ] ) ? number_format( $remittance_details[ 'forex_rate' ], 2, '.', ',' ) : '0.00' ) }}">
																	
																	@else
																	
																		<input type="text" class="form-control" id="remittance_forex_rate" name="remittance_forex_rate" placeholder="Forex rate" required="required" value="{{ number_format( $forex_rate, 2, '.', ',' ) }}">
																	
																	@endif
																	
																</div>
															
															</div>
															<!-- FOREX END -->
															
															<!-- TRADE RATE START -->
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remittance_trade_rate"> Trade rate:<span style="color:#E85445;">*</span> </label>
																	
																	@if( $action == 'edit' )
																	
																		<input type="text" class="form-control" id="remittance_trade_rate" name="remittance_trade_rate" placeholder="Trade rate" required="required" value="{{ ( isset( $remittance_details[ 'trade_rate' ] ) && ! empty( $remittance_details[ 'trade_rate' ] ) ? number_format( $remittance_details[ 'trade_rate' ], 2, '.', ',' ) : '0.00' ) }}">
																	
																	@else
																	
																		<input type="text" class="form-control" id="remittance_trade_rate" name="remittance_trade_rate" placeholder="Trade rate" required="required" value="{{ number_format( $trade_rate, 2, '.', ',' ) }}">
																	
																	@endif
																	
																</div>
															
															</div>
															<!-- TRADE RATE END -->
															
														</div>
														
														<div class="row">
															
															<!-- SERVICE FEE START -->
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																
																<div class="form-group">

																	<label for="remittance_service_fee"> Service fee:<span style="color:#E85445;">*</span> </label>

																	<input type="text" class="form-control" id="remittance_service_fee" name="remittance_service_fee" placeholder="Service fee" required="required" value="{{ ( isset( $remittance_details[ 'service_fee' ] ) && ! empty( $remittance_details[ 'service_fee' ] ) ? number_format( $remittance_details[ 'service_fee' ], 2, '.', ',' ) : '0.00' ) }}">

																</div>
															
															</div>
															<!-- SERVICE FEE END -->
															
														</div>
														
														<div class="row">
														
															<!-- AMOUNT SENT START -->
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remittance_amount_sent"> Amount sent:<span style="color:#E85445;">*</span> </label>
																	
																	<input type="text" class="form-control" id="remittance_amount_sent" name="remittance_amount_sent" placeholder="Amount sent" required="required" value="{{ ( isset( $remittance_details[ 'amount_sent' ] ) && ! empty( $remittance_details[ 'amount_sent' ] ) ? number_format( $remittance_details[ 'amount_sent' ], 2, '.', ',' ) : '0.00' ) }}">
																	
																</div>
															
															</div>
															<!-- AMOUNT SENT END -->
															
														</div>
														
														<div class="row">
															
															<!-- TOTAL PHP DEPOSIT START -->
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remittance_total_php_deposit"> Total PHP Deposit: </label>
																	
																	<input type="text" class="form-control" id="remittance_total_php_deposit" name="remittance_total_php_deposit" placeholder="Total PHP deposit" disabled="disabled" value="{{ ( isset( $remittance_details[ 'total_php_deposit' ] ) && ! empty( $remittance_details[ 'total_php_deposit' ] ) ? $remittance_details[ 'total_php_deposit' ] : '0.00' ) }}">

																</div>
																
															</div>
															<!-- TOTAL PHP DEPOSIT END -->
															
														</div>
														
													</div>
													
												</div>
												
											</div>
											
										</div>

									</div>

								</div>

							</div>
							<!-- REMITTANCE DETAIL TAB END -->
							
							<!-- REMITTANCE ADDITIONAL REQUIREMENT TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remittance_additional_requirement_tab" aria-labelledby="additional_requirement-tab">

								<div class="panel panel-primary" style="margin: 20px 0px;">
									
									<div class="panel-heading"> <i class="fa fa-list-alt" aria-hidden="true"></i> Additional requirement </div>
									
									<div class="panel-body">
								
										<!-- SOURCE OF INCOME START -->
										<div class="row">
												
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												
												<div class="form-group">
													
													<label for="remittance_source_of_income"> Source of income:<span style="color:#E85445;">*</span> </label>

													<input type="text" id="remittance_source_of_income" name="remittance_source_of_income" class="form-control" placeholder="Source of income" value="{{ ( isset( $remittance_addtional_requirement[ 'source_of_income' ] ) && ! empty( $remittance_addtional_requirement[ 'source_of_income' ] ) ? $remittance_addtional_requirement[ 'source_of_income' ] : '' ) }}">
													
												</div>
													
											</div>
											
										</div>
										<!-- SOURCE OF INCOME END -->
									
										<!-- REMITTANCE ADDITIONAL REQUIREMENTS START -->
										<div class="row">
										
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												
												<div class="form-group">
													
													<label for="remittance_additional_requirement"> Additional requirement:<span style="color:#E85445;">*</span> </label>
													
													<div id="remittance_additional_requirement" class="dropzone text-center" style="min-height:200px; height:auto;">
														
														<div class="dz-default dz-message" style="margin: 6em 0em;">
															
															<span> Drop additional requirement here to upload </span>

														</div>
														
													</div>
													
													<input type="text" id="remittance_additional_requirement_hidden" data-parsley-errors-container="#remittance_additional_requirement_error_container" style="display: none;">
													
													<div class="remittance_additional_requirement_error_container"></div>

												</div>

											</div>
											
										</div>
										<!-- REMITTANCE ADDITIONAL REQUIREMENTS END -->
									
									</div>

								</div>

							</div>
							<!-- REMITTANCE ADDITIONAL REQUIREMENT TAB END -->
							
							<!-- REMITTANCE REMITTER AND RECIPIENT TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remittance_remitter_and_recipient_tab" aria-labelledby="remitter_and_recipient-tab">
							
								<div class="row">
									
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										
										<div class="panel panel-success" style="margin: 20px 0px;">
											
											<div class="panel-heading"> <i class="fa fa-user" aria-hidden="true"></i> Remitter </div>
											
											<div class="panel-body">
										
												<!-- REMITTER START -->
												<div class="row">
												
													<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-sm-12 col-xs-12">
														
														<div class="form-group">
															
															<label for="remittance_remitter_id"> Remitter:<span style="color:#E85445;">*</span> </label>

															<select id="remittance_remitter_id" name="remittance_remitter_id" class="form-control chosen-select" data-placeholder="Remitter" required="required">
																
																<option value=""></option>
																
																@if( isset( $remittance_remitter ) AND ! empty( $remittance_remitter ) )
																
																	<option value="{{ $remittance_remitter[ 'id' ] }}" selected="selected"> {{ $remittance_remitter[ 'fullname' ] }} | {{ $remittance_remitter[ 'email' ] }} </option>
																
																@endif
																
															</select>
															
														</div>
													
													</div>
													
												</div>
												<!-- REMITTER END -->
												
												<!-- REMITTER FULL DETAILS START -->
												<div class="remitter_full_details_container">

													@include( 'modals.content.remitter_full_details' )

												</div>
												<!-- REMITTER FULL DETAILS END -->
												
											</div>
											
										</div>

									</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										
										<div class="panel panel-warning" style="margin: 20px 0px;">
											
											<div class="panel-heading"> <i class="fa fa-user" aria-hidden="true"></i> Recipient </div>
											
											<div class="panel-body">
										
												<!-- RECIPIENT START -->
												<div class="row">

													<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-sm-12 col-xs-12">
														
														<div class="form-group">
															
															<label for="remittance_recipient_id"> Recipient:<span style="color:#E85445;">*</span> </label>

															<select id="remittance_recipient_id" name="remittance_recipient_id" class="form-control chosen-select" data-placeholder="Recipient" required="required">
																
																<option value=""></option>
																
															</select>
															
														</div>
													
													</div>
												
												</div>
												<!-- RECIPIENT END -->
												
												<!-- RECIPIENT FULL DETAILS START -->
												<div class="recipient_full_details_container">
													
													@include( 'modals.content.recipient_full_details' )

												</div>
												<!-- RECIPIENT FULL DETAILS END -->
												
											</div>
											
										</div>

									</div>
									
								</div>

							</div>
							<!-- REMITTANCE REMITTER AND RECIPIENT TAB END -->
							
							<!-- REMITTANCE NOTE TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remittance_note_tab" aria-labelledby="note-tab">
								
								<div class="row">
								
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
										<div class="form-group">
											
											<label for="remittance_added_note"> Added note <small>(20 chars min, 1000 max)</small>: </label>
											
											<textarea id="remittance_added_note" class="form-control" name="remittance_added_note" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="1000" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" rows="10">{{ ( isset( $remittance_notes[ 'note' ] ) && ! empty( $remittance_notes[ 'note' ] ) ? $remittance_notes[ 'note' ] : '' ) }}</textarea>

										</div>
										
									</div>

								</div>
								
							</div>
							<!-- REMITTANCE NOTE TAB END -->
							
							<!-- REMITTANCE SETTING TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remittance_setting_tab" aria-labelledby="note-tab">
								
								<div class="panel panel-primary" style="margin:0px 0px 20px;">
									
									<div class="panel-heading"> <i class="fa fa-wrench" aria-hidden="true"></i> Configuration </div>

									<div class="panel-body">
										
										<div class="row">

											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

												<div class="form-group">
												
													<label> Do you like to send: </label>
													
													<br>
													
													<!-- REMITTANCE ADVICE START -->
													<label style="font-weight:400;">

														<input type="checkbox" name="remittance_advice_client_and_support" value="1"> (Remittance Advice) to client and support

													</label>
													<!-- REMITTANCE ADVICE START -->
													
													<br>
													
													<!-- UPDATED RATE START -->
													<label style="font-weight:400;">

														<input type="checkbox" name="remittance_updated_rate" value="1"> (Updated Rate) to client and support

													</label>
													<!-- UPDATED RATE END -->
													
													<br>
													
													<!-- POLI LINK START -->
													<label style="font-weight:400;">

														<input type="checkbox" name="remittance_poli_link" value="1" checked="checked"> (Poli Link) to client

													</label>
													<!-- POLI LINK END -->
												
												</div>

											</div>
											
											<!-- REMITTANCE AUTORESPONDER START -->
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

												<div class="form-group">
												
													<label> Do you like to send remittance status: </label>
													
													<br>
													
													<label style="font-weight:400;">

														<input type="checkbox" name="remittance_status_autoresponder" value="1" checked="checked"> Autoresponder

													</label>
													
													<br>
													
													<label style="font-weight:400;">

														<input type="checkbox" name="remittance_status_push_notification" value="1" checked="checked"> Push notification (IOS &amp; Android)

													</label>
												
												</div>
												
											</div>
											<!-- REMITTANCE AUTORESPONDER END -->

										</div>

									</div>
									
								</div>
								
							</div>
							<!-- REMITTANCE SETTING TAB END -->
							
							<!-- REMITTANCE HISTORY TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remittance_history_tab" aria-labelledby="history-tab">
							
								<div class="row">
								
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									
										@if( isset( $histories ) AND ! empty( $histories ) )
										
											<div class="panel panel-default">

												<div class="panel-heading"> 
													
													<i class="fa fa-list-alt" aria-hidden="true"></i> History list 
													
												</div>

												<div class="panel-body">
													
													<div class="scrollbar-light">
																								
														<div class="table-responsive">
														
															<table class="table table-striped table-bordered table-hover" style="margin: 0px;">
																
																<thead>
																
																	<tr> 
																		
																		<th style="width: 20%;"> <small> Field </small> </th>
																		
																		<th style="width: 30%;"> <small> Old value </small> </th>
																		
																		<th style="width: 30%;"> <small> New value </small> </th>
																		
																		<th style="width: 20%;"> <small> CSR </small> </th>

																	</tr>
																
																</thead>
																
																<tbody>
														
																	@foreach( $histories as $history )

																		<tr>

																			<td> <small> {{ $history[ 'field_name' ] }} </small> </td>
																			
																			<td> <small> {{ $history[ 'old_value' ] }} </small> </td>
																			
																			<td> <small> {{ $history[ 'new_value' ] }} </small> </td>
																			
																			<td> <small> {{ $history[ 'full_name' ] }} </small> </td>
																			
																		</tr>
																			
																	@endforeach
																
																</tbody>
															
															</table>
															
														</div>
													
													</div>
													
												</div>
												
											</div>
											
										@else
										
											<div class="alert alert-danger" role="alert" style="margin:10px 0px;">
										
												<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No history changes for this remittance! </h5>
											
											</div>
											
										@endif
											
									</div>
								
								</div>
							
							</div>
							<!-- REMITTER HISTORY TAB END -->
						
						</div>
						<!-- REMITTANCE FORM TAB CONTENT END -->
					
					</div>
					<!-- REMITTANCE TABS END -->
					
				</div>

				<div class="modal-footer">
					
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" style="margin:0px;"> <i class="fa fa-times" aria-hidden="true"></i> Close </button>

					<button type="submit" class="btn btn-sm btn-primary" style="margin:0px;"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save </button>
					
				</div>

			</form>
			
		</div>

	</div>

</div>

@if( 

	( isset( $remitter_id ) && ! empty( $remitter_id ) )
	
	&&
	
	( isset( $recipient_id ) && ! empty( $recipient_id ) )
	
)

	<!-- CUSTOM SCRIPT FOR REMITTER AND RECIPIENT FULL DETAILS START -->
	<script type="text/javascript">
		
		jQuery( function() {
			
			get_remitter_recipients( {{ $remitter_id }}, {{ $recipient_id }} );
			
			get_remitter_full_details( {{ $remitter_id }} );
			
			get_recipient_full_details( {{ $recipient_id }} );
			
		} );

	</script>
	<!-- CUSTOM SCRIPT FOR REMITTER AND RECIPIENT FULL DETAILS START -->

@endif
