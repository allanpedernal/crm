<div id="remitter_modal" class="modal fade" tabindex="-1" role="dialog">
		
	<div class="modal-dialog modal-lg" role="document">
		
		<div class="modal-content">
			
			<form method="POST" action="{{ URL('/iremit/remitters/form/post') }}" id="remitter_form" class="form-horizontal" data-validate="parsley" onSubmit="return on_validate();">
				
				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						
						<span aria-hidden="true"> &times; </span>
						
					</button>

					<h4 class="modal-title"> <i class="fa fa-wpforms"></i> Remitter Form </h4>

				</div>

				<div class="modal-body">
					
					<!-- REMITTER TABS START -->
					<div role="tabpanel" data-example-id="togglable-tabs">
						
						<!-- REMITTER FORM NAV START -->
						<ul id="remitter_form_nav" class="nav nav-tabs bar_tabs" role="tablist">
							
							<!-- PERSONAL INFORMATION NAV START -->
							<li role="presentation" class="active">
								
								<a href="#remitter_personal_tab" id="personal-tabb" role="tab" data-toggle="tab" aria-controls="personal" aria-expanded="false"> <i class="fa fa-user"></i> Personal </a>
								
							</li>
							<!-- PERSONAL INFORMATION NAV END -->
							
							<!-- COMPANY INFORMATION NAV START -->
							<li id="remitter_company_nav" role="presentation" style="{{ ( isset( $remitter_details[ 'type_id' ] ) && ! empty( $remitter_details[ 'type_id' ] ) && $remitter_details[ 'type_id' ] != 1 ? 'display: block;' : 'display: none;' ) }}">
								
								<a href="#remitter_company_tab" id="company-tabb" role="tab" data-toggle="tab" aria-controls="company" aria-expanded="false"> <i class="fa fa-building-o"></i> Company </a>
								
							</li>
							<!-- COMPANY INFORMATION NAV END -->
							
							<!-- ADDRESS INFORMATION NAV START -->
							<li role="presentation">
								
								<a href="#remitter_address_tab" id="address-tabb" role="tab" data-toggle="tab" aria-controls="address" aria-expanded="false"> <i class="fa fa-address-book"></i> Address </a>
								
							</li>
							<!-- ADDRESS INFORMATION NAV END -->

							<!-- CONTACT INFORMATION NAV START -->
							<li role="presentation">
								
								<a href="#remitter_contact_tab" id="contact-tabb" role="tab" data-toggle="tab" aria-controls="contact" aria-expanded="false"> <i class="fa fa-phone"></i> Contact </a>
								
							</li>
							<!-- CONTACT INFORMATION NAV START -->
							
							<!-- IDENTIFICATION INFORMATION START -->
							<li role="presentation">
								
								<a href="#remitter_identification_tab" id="identification-tabb" role="tab" data-toggle="tab" aria-controls="identification" aria-expanded="false"> <i class="fa fa-id-card"></i> ID </a>
								
							</li>
							<!-- IDENTIFICATION INFORMATION END -->
							
							<!-- MARKETING INFORMATION START -->
							<li role="presentation">
								
								<a href="#remitter_marketing_tab" id="marketing-tabb" role="tab" data-toggle="tab" aria-controls="marketing" aria-expanded="false"> <i class="fa fa-line-chart"></i> Marketing </a>
								
							</li>
							<!-- MARKETING INFORMATION END -->
							
							<!-- NOTE INFORMATION START -->
							<li role="presentation">
								
								<a href="#remitter_note_tab" id="note-tabb" role="tab" data-toggle="tab" aria-controls="note" aria-expanded="false"> <i class="fa fa-sticky-note"></i> Note </a>
								
							</li>
							<!-- NOTE INFORMATION END -->
							
							<!-- HISTORY INFORMATION START -->
							<li role="presentation">
								
								<a href="#remitter_history_tab" id="history-tabb" role="tab" data-toggle="tab" aria-controls="history" aria-expanded="false"> <i class="fa fa-clock-o"></i> History </a>
								
							</li>
							<!-- HISTORY INFORMATION END -->
							
						</ul>
						<!-- REMITTER FORM NAV END -->
						
						<!-- REMITTER FORM TAB CONTENT START -->
						<div id="remitter_form_tab_content" class="tab-content">
							
							<!-- REMITTER PERSONAL TAB START -->
							<div role="tabpanel" class="tab-pane fade active in" id="remitter_personal_tab" aria-labelledby="personal-tab">
									
								<div class="row">
									
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
										<!-- PERSONAL INFORMATION START -->
										<div class="panel panel-primary" style="margin:0px 0px 20px;">
											
											<div class="panel-heading"> <i class="fa fa-user" aria-hidden="true"></i> Personal </div>

											<div class="panel-body">
												
												<!-- REMITTER ID START -->
												<input type="hidden" name="remitter_id" id="remitter_id" class="form-control" value="{{ ( isset( $remitter_id ) && ! empty( $remitter_id ) ? $remitter_id : 0 ) }}">
												<!-- REMITTER ID END -->
												
												<div class="row">
													
													<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
													
														<div class="row">
														
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

																<!-- AVATAR START -->
																<div class="row">
																	
																	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																			
																		<div class="form-group">
																			
																			<label for="remitter_profile_avatar"> Avatar: </label>
																			
																			<div id="remitter_profile_avatar" class="dropzone text-center" style="min-height:200px; height:auto;">
																				
																				<div class="dz-default dz-message" style="margin: 6em 0em;">
																					
																					<span> Drop avatar here to upload </span>

																				</div>
																				
																			</div>
																			
																			@if( isset( $remitter_details[ 'filename' ] ) && ! empty( $remitter_details[ 'filename' ] ) )
																			
																				<input type="hidden" name="remitter_profile_avatar" value="{{ $remitter_details[ 'filename' ] }}">
																			
																			@endif

																		</div>
																		
																	</div>
																	
																</div>
																<!-- AVATAR END -->

																<!-- REMITTER TYPE START -->
																<div class="row">
																
																	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	
																		<div class="form-group">
																			
																			<label for="remitter_type_id"> Type:<span style="color:#E85445;">*</span> </label>

																			<select id="remitter_type_id" name="remitter_type_id" class="form-control chosen-select" data-placeholder="Type" required="required">
																			
																				<option value=""></option>
																				
																				@if( isset( $remitter_types ) && ! empty( $remitter_types ) )
																				
																					@foreach ( $remitter_types as $remitter_type )
																					
																						<option {{ ( isset( $remitter_details[ 'type_id' ] ) && ! empty( $remitter_details[ 'type_id' ] ) && $remitter_details[ 'type_id' ] == $remitter_type[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $remitter_type[ 'id' ] }}"> {{ $remitter_type[ 'name' ] }} </option>
																					
																					@endforeach
																				
																				@endif
																			
																			</select>

																		</div>
															
																	</div>
																
																</div>
																<!-- REMITTER TYPE END -->

															</div>

														</div>

													</div>
												
													<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
														
														<!-- REMITTER FULLNAME WITH OCCUPATION START -->
														<div class="row">
															
															<!-- REMITTER TITLE START -->
															<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remitter_title"> Title:<span style="color:#E85445;">*</span> </label>

																	<select id="remitter_title" name="remitter_title" class="form-control chosen-select" data-placeholder="Title" required="required">
																	
																		<option value=""></option>
																		
																		@foreach ( $titles as $title )
																		
																			<option {{ ( isset( $remitter_details[ 'title_id' ] ) && ! empty( $remitter_details[ 'title_id' ] ) && $remitter_details[ 'title_id' ] == $title[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $title[ 'id' ] }}"> {{ $title[ 'abbr' ] }} </option>
																		
																		@endforeach
																	
																	</select>

																</div>
																
															</div>
															<!-- REMITTER TITLE END -->
															
															<!-- REMITTER FIRSTNAME START -->
															<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remitter_firstname"> First name:<span style="color:#E85445;">*</span> </label>

																	<input type="text" class="form-control" id="remitter_firstname" name="remitter_firstname" placeholder="First name" required="required" value="{{ ( isset( $remitter_details[ 'firstname' ] ) && ! empty( $remitter_details[ 'firstname' ] ) ? $remitter_details[ 'firstname' ] : "" ) }}">

																</div>
																
															</div>
															<!-- REMITTER FIRSTNAME END -->

															<!-- REMITTER MIDDLENAME START -->
															<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remitter_middlename"> Middle name: </label>

																	<input type="text" class="form-control" id="remitter_middlename" name="remitter_middlename" placeholder="Middle name" value="{{ ( isset( $remitter_details[ 'middlename' ] ) && ! empty( $remitter_details[ 'middlename' ] ) ? $remitter_details[ 'middlename' ] : "" ) }}">

																</div>
															
															</div>
															<!-- REMITTER MIDDLENAME END -->
															
														</div>
														
														<div class="row">
															
															<!-- REMITTER LASTNAME START -->
															<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remitter_lastname"> Last name:<span style="color:#E85445;">*</span> </label>

																	<input type="text" class="form-control" id="remitter_lastname" name="remitter_lastname" placeholder="Last name" required="required" value="{{ ( isset( $remitter_details[ 'lastname' ] ) && ! empty( $remitter_details[ 'lastname' ] ) ? $remitter_details[ 'lastname' ] : "" ) }}">

																</div>
															
															</div>
															<!-- REMITTER LASTNAME START -->
															
															<!-- REMITTER OCCUPATION START -->
															<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remitter_occupation"> Occupation: </label>

																	<input type="text" class="form-control" id="remitter_occupation" name="remitter_occupation" placeholder="Occupation" value="{{ ( isset( $remitter_details[ 'occupation' ] ) && ! empty( $remitter_details[ 'occupation' ] ) ? $remitter_details[ 'occupation' ] : "" ) }}">

																</div>
															
															</div>
															<!-- REMITTER OCCUPATION START -->
															
														</div>
														<!-- REMITTER FULLNAME WITH OCCUPATION END -->
														
														<!-- REMITTER OTHER DETAILS START -->
														<div class="row">

															<!-- REMITTER NATIONALITY START -->
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remitter_nationality"> Nationality: </label>
																	
																	<select id="remitter_nationality" name="remitter_nationality" class="form-control chosen-select" data-placeholder="Nationality">
																	
																		<option value=""></option>
																		
																		@foreach ( $nationalities as $nationality )
																		
																			<option {{ ( isset( $remitter_details[ 'nationality_id' ] ) && ! empty( $remitter_details[ 'nationality_id' ] ) && $remitter_details[ 'nationality_id' ] == $nationality[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $nationality[ 'id' ] }}"> {{ $nationality[ 'name' ] }} </option>
																		
																		@endforeach
																	
																	</select>

																</div>
																
															</div>
															<!-- REMITTER NATIONALITY END -->

															<!-- REMITTER BIRTHDAY START -->
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remitter_birthday"> Birthday:<span style="color:#E85445;">*</span> </label>

																	<div class="input-prepend input-group" style="margin:0px;">
																		
																		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>

																		<input type="text" class="form-control datepicker" id="remitter_birthday" name="remitter_birthday" placeholder="Birthday" readonly="readonly" required="required" value="{{ ( isset( $remitter_details[ 'birthday' ] ) && ! empty( $remitter_details[ 'birthday' ] ) ? $remitter_details[ 'birthday' ] : "" ) }}">

																	</div>
																	
																</div>
																
															</div>
															<!-- REMITTER BIRTHDAY END -->
															
														</div>
														
														<div class="row">
															
															<!-- REMITTER CIVIL STATUS START -->
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																
																<div class="form-group">
																	
																	<label for="remitter_civil_status"> Civil status:<span style="color:#E85445;">*</span> </label>

																	<select id="remitter_civil_status" name="remitter_civil_status" class="form-control chosen-select" data-placeholder="Civil status" required="required">
																	
																		<option value=""></option>
																		
																		@foreach ( $civil_status as $cs )
																		
																			<option {{ ( isset( $remitter_details[ 'civil_status_id' ] ) && ! empty( $remitter_details[ 'civil_status_id' ] ) && $remitter_details[ 'civil_status_id' ] == $cs[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $cs[ 'id' ] }}"> {{ $cs[ 'name' ] }} </option>
																		
																		@endforeach
																	
																	</select>
																	
																</div>
																
															</div>
															<!-- REMITTER CIVIL STATUS END -->

															<!-- REMITTER GENDER START -->
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																
																<div class="form-group">
																
																	<label for="remitter_gender"> Gender:<span style="color:#E85445;">*</span> </label>
																	
																	<select id="remitter_gender" name="remitter_gender" class="form-control chosen-select remitter_gender" data-placeholder="Gender" required="required">

																		<option value=""></option>

																		<option {{ ( isset( $remitter_details[ 'gender' ] ) && ! empty( $remitter_details[ 'gender' ] ) && $remitter_details[ 'gender' ] == 'male'  ? 'selected="selected"' : "" ) }} value="male"> Male </option>

																		<option {{ ( isset( $remitter_details[ 'gender' ] ) && ! empty( $remitter_details[ 'gender' ] ) && $remitter_details[ 'gender' ] == 'female'  ? 'selected="selected"' : "" ) }} value="female"> Female </option>

																	</select>

																</div>

															</div>
															<!-- REMITTER GENDER END -->
														
														</div>
														<!-- REMITTER OTHER DETAILS END -->
														
													</div>
												
												</div>
												
											</div>

										</div>
										<!-- PERSONAL INFORMATION END -->
										
										<!-- EMAIL INFORMATION START -->
										<div class="panel panel-success" style="margin:0px 0px 20px;">
											
											<div class="panel-heading"> <i class="fa fa-envelope" aria-hidden="true"></i> Email </div>
											
											<div class="panel-body">
												
												<!-- PRIMARY AND SECONDARY EMAIL ADDRESS START -->
												<div class="row">

													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
														
														<div class="form-group">
															
															<label for="remitter_primary_email_address"> Primary email address:<span style="color:#E85445;">*</span> </label>

															<input type="email" class="form-control" id="remitter_primary_email_address" name="remitter_primary_email_address" placeholder="Primary email address" required="required" @if($action != 'new' ) readonly="readonly" @endif value="{{ ( isset( $remitter_details[ 'email' ] ) && ! empty( $remitter_details[ 'email' ] ) ? $remitter_details[ 'email' ] : "" ) }}">

														</div>

													</div>

													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
														
														<div class="form-group">
															
															<label for="remitter_secondary_email_address"> Secondary email address: </label>

															<input type="email" class="form-control" id="remitter_secondary_email_address" name="remitter_secondary_email_address" placeholder="Secondary email address" value="{{ ( isset( $remitter_details[ 'secondary_email' ] ) && ! empty( $remitter_details[ 'secondary_email' ] ) ? $remitter_details[ 'secondary_email' ] : "" ) }}">

														</div>

													</div>

												</div>
												<!-- PRIMARY AND SECONDARY EMAIL ADDRESS END -->

											</div>
											
										</div>
										<!-- EMAIL INFORMATION END -->
										
									</div>
								
								</div>
									
							</div>
							<!-- REMITTER PERSONAL TAB END -->
							
							<!-- REMITTER COMPANY TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remitter_company_tab" aria-labelledby="company-tab">
							
								<div class="row">
									
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
										<div class="panel panel-primary" style="margin:0px 0px 20px;">
											
											<div class="panel-heading"> <i class="fa fa-building-o"></i> Company </div>
											
											<div class="panel-body">
												
												<!-- REMITTER COMPANY ID START -->
												<input type="hidden" name="remitter_company_id" id="remitter_company_id" class="form-control" value="{{ ( isset( $remitter_company_id ) && ! empty( $remitter_company_id ) ? $remitter_company_id : 0 ) }}">
												<!-- REMITTER COMPANY ID END -->
												
												<!-- AUTHORIZATION START -->
												<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
													
													<!-- AUTHORIZATION START -->
													<div class="row">
														
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																
															<div class="form-group">
																
																<label for="remitter_company_authorization"> Authorization: </label>
																
																<div id="remitter_company_authorization" class="dropzone text-center" style="min-height:200px; height:auto;">
																	
																	<div class="dz-default dz-message" style="margin: 6em 0em;">
																		
																		<span> Drop authorization here to upload </span>

																	</div>
																	
																</div>
																
																<input type="text" id="remitter_company_authorization_hidden" data-parsley-errors-container="#remitter_company_authorization_error_container" style="display: none;">
																
																<div class="remitter_company_authorization_error_container"></div>
																
																@if( isset( $remitter_company_authorization[ 'filename' ] ) && ! empty( $remitter_company_authorization[ 'filename' ] ) )
																
																	<input type="hidden" name="remitter_company_authorization" value="{{ $remitter_company_authorization[ 'filename' ] }}">
																
																@endif

															</div>
															
														</div>
														
													</div>
													<!-- AUTHORIZATION END -->
												
												</div>
												<!-- AUTHORIZATION START -->
												
												<!-- COMPANY DETAILS START -->
												<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
													
													<div class="row">
														
														<!-- REMITTER COMPANY NAME START -->
														<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

															<div class="form-group">

																<label for="remitter_company_name"> Company name:<span style="color:#E85445;">*</span> </label>

																<input type="text" class="form-control" id="remitter_company_name" name="remitter_company_name" placeholder="Company name" value="{{ ( isset( $remitter_company_details[ 'name' ] ) && ! empty( $remitter_company_details[ 'name' ] ) ? $remitter_company_details[ 'name' ] : "" ) }}">

															</div>

														</div>
														<!-- REMITTER COMPANY NAME END -->
														
														<!-- REMITTER COMPANY CONTACT START -->
														<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
															
															<div class="form-group">

																<label for="remitter_company_contact"> Contact:<span style="color:#E85445;">*</span> </label>

																<input type="text" class="form-control" id="remitter_company_contact" name="remitter_company_contact" placeholder="Contact" value="{{ ( isset( $remitter_company_details[ 'contact' ] ) && ! empty( $remitter_company_details[ 'contact' ] ) ? $remitter_company_details[ 'contact' ] : "" ) }}">

															</div>

														</div>
														<!-- REMITTER COMPANY CONTACT END -->
														
													</div>
													
													<div class="row">
														
														<!-- REMITTER COMPANY ADDRESS START -->
														<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
															
															<div class="form-group">

																<label for="remitter_company_address"> Company address:<span style="color:#E85445;">*</span> </label>

																<input type="text" class="form-control" id="remitter_company_address" name="remitter_company_address" placeholder="Company address" value="{{ ( isset( $remitter_company_details[ 'address' ] ) && ! empty( $remitter_company_details[ 'address' ] ) ? $remitter_company_details[ 'address' ] : "" ) }}">

															</div>
															
														</div>
														<!-- REMITTER COMPANY ADDRESS START -->
														
														<!-- REMITTER COMPANY COUNTRY START -->
														<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

															<div class="form-group">

																<label for="remitter_company_country_id"> Country:<span style="color:#E85445;">*</span> </label>

																<select id="remitter_company_country_id" name="remitter_company_country_id" class="form-control chosen-select" data-placeholder="Country">
																
																	<option value=""></option>
																	
																	@if( isset( $countries ) && ! empty( $countries ) )
																	
																		@foreach( $countries AS $country )
																		
																			<option value="{{ $country[ 'id' ] }}" {{ ( isset( $remitter_company_details[ 'country_id' ] ) && ! empty( $remitter_company_details[ 'country_id' ] ) && $remitter_company_details[ 'country_id' ] == $country[ 'id' ] ? "selected='selected'" : "" ) }}> {{ $country[ 'en_short_name' ] }} </option>
																		
																		@endforeach
																	
																	@endif
																
																</select>

															</div>
															
														</div>
														<!-- REMITTER COMPANY COUNTRY END -->
														
													</div>
															
													<div class="row">
														
														<!-- REMITTER COMPANY REGISTRATION START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															
															<div class="form-group">

																<label for="remitter_company_registration"> Registration date:<span style="color:#E85445;">*</span> </label>

																	<div class="input-prepend input-group" style="margin:0px;">
																		
																		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>

																		<input type="text" class="form-control datepicker" id="remitter_company_registration" name="remitter_company_registration" readonly="readonly" placeholder="Registration date" value="{{ ( isset( $remitter_company_details[ 'registration_date' ] ) && ! empty( $remitter_company_details[ 'registration_date' ] ) ? $remitter_company_details[ 'registration_date' ] : "" ) }}">
																	
																	</div>
																	
															</div>
															
														</div>
														<!-- REMITTER COMPANY REGISTRATION END -->
														
														<!-- REMITTER COMPANY REMITTANCE RANGE START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															
															<div class="form-group">

																<label for="remitter_company_remittance_range"> Remittance range:<span style="color:#E85445;">*</span> </label>

																<select id="remitter_company_remittance_range" name="remitter_company_remittance_range" class="form-control chosen-select" data-placeholder="Remittance range">
																
																	<option value=""></option>
																	
																	<option value="0 - 5,000" {{ ( isset( $remitter_company_details[ 'remittance_range' ] ) && ! empty( $remitter_company_details[ 'remittance_range' ] ) && $remitter_company_details[ 'remittance_range' ] == "0 - 5,000" ? "selected='selected'" : "" ) }}> Up to AUD5000 </option>
																	
																	<option value="5,001 - 10,000" {{ ( isset( $remitter_company_details[ 'remittance_range' ] ) && ! empty( $remitter_company_details[ 'remittance_range' ] ) && $remitter_company_details[ 'remittance_range' ] == "5,001 - 10,000" ? "selected='selected'" : "" ) }}> More than AUD5000 But less than AUD10,000 </option>
																	
																	<option value="10,001 - above" {{ ( isset( $remitter_company_details[ 'remittance_range' ] ) && ! empty( $remitter_company_details[ 'remittance_range' ] ) && $remitter_company_details[ 'remittance_range' ] == "10,001 - above" ? "selected='selected'" : "" ) }}> AUD10,000 and Above </option>
																
																</select>

															</div>
														
														</div>
														<!-- REMITTER COMPANY REMITTANCE RANGE END -->

													</div>
													
													<div class="row">
														
														<!-- REMITTER COMPANY SOURCE OF FUND START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															
															<div class="form-group">

																<label for="remitter_company_source_of_fund_id"> Source of fund:<span style="color:#E85445;">*</span> </label>

																<select id="remitter_company_source_of_fund_id" name="remitter_company_source_of_fund_id" class="form-control chosen-select" data-placeholder="Source of fund">
																
																	<option value=""></option>
																	
																	@if( isset( $remitter_company_source_of_funds ) && ! empty( $remitter_company_source_of_funds ) )
																	
																		@foreach( $remitter_company_source_of_funds AS $remitter_company_source_of_fund )
																		
																			<option value="{{ $remitter_company_source_of_fund[ 'id' ] }}" {{ ( isset( $remitter_company_details[ 'source_of_fund_id' ] ) && ! empty( $remitter_company_details[ 'source_of_fund_id' ] ) && $remitter_company_details[ 'source_of_fund_id' ] == $remitter_company_source_of_fund[ 'id' ] ? "selected='selected'" : "" ) }}> {{ $remitter_company_source_of_fund[ 'name' ] }} </option>
																		
																		@endforeach
																	
																	@endif
																
																</select>

															</div>
														
															<div id="remitter_company_other_source_of_fund_container" class="row" style="{{ ( isset( $remitter_company_details[ 'source_of_fund_id' ] ) && ! empty( $remitter_company_details[ 'source_of_fund_id' ] ) && $remitter_company_details[ 'source_of_fund_id' ] == 1 ? 'display: block;' : 'display: none;' ) }}">
																
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	
																	<div class="form-group">
																		
																		<label for="remitter_company_other_source_of_fund"> Other source of fund:<span style="color:#E85445;">*</span> </label>

																		<input type="text" id="remitter_company_other_source_of_fund" name="remitter_company_other_source_of_fund" class="form-control" placeholder="Other source of fund" value="{{ ( isset( $remitter_company_details[ 'source_of_fund_id' ] ) && ! empty( $remitter_company_details[ 'source_of_fund_id' ] ) && $remitter_company_details[ 'source_of_fund_id' ] == 1 ? $remitter_company_other_source_of_fund[ 'source_of_fund' ] : '' ) }}" {{ ( isset( $remitter_company_details[ 'source_of_fund_id' ] ) && ! empty( $remitter_company_details[ 'source_of_fund_id' ] ) && $remitter_company_details[ 'source_of_fund_id' ] == 1 ? 'required="required"' : '' ) }}>
																		
																	</div>
																
																</div>
																
															</div>
														
														</div>
														<!-- REMITTER COMPANY SOURCE OF FUND END -->
														
														<!-- REMITTER COMPANY RELATIONSHIP START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															
															<div class="form-group">

																<label for="remitter_company_relationship_id"> Company relationship:<span style="color:#E85445;">*</span> </label>

																<select id="remitter_company_relationship_id" name="remitter_company_relationship_id" class="form-control chosen-select" data-placeholder="Company relationship">
																
																	<option value=""></option>
																	
																	@if( isset( $remitter_company_relationships ) && ! empty( $remitter_company_relationships ) )
																	
																		@foreach( $remitter_company_relationships AS $remitter_company_relationship )
																		
																			<option value="{{ $remitter_company_relationship[ 'id' ] }}" {{ ( isset( $remitter_company_details[ 'company_relationship_id' ] ) && ! empty( $remitter_company_details[ 'company_relationship_id' ] ) && $remitter_company_details[ 'company_relationship_id' ] == $remitter_company_relationship[ 'id' ] ? "selected='selected'" : "" ) }}> {{ $remitter_company_relationship[ 'name' ] }} </option>
																		
																		@endforeach
																	
																	@endif
																
																</select>

															</div>
															
															<!-- REMITTER COMPANY OTHER RELATIONSHIP START -->
															<div id="remitter_company_other_relationship_container" class="row" {{ ( isset( $remitter_company_details[ 'company_relationship_id' ] ) && ! empty( $remitter_company_details[ 'company_relationship_id' ] ) && $remitter_company_details[ 'company_relationship_id' ] == 1 ? 'style=display:block;' : 'style=display:none;' ) }} >
															
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																
																	<div class="form-group">
																	
																		<label for="remitter_company_other_relationship"> Other relationship:<span style="color:#E85445;">*</span> </label>
																		
																		<input type="text" id="remitter_company_other_relationship" name="remitter_company_other_relationship" class="form-control remitter_company_other_relationship" placeholder="Other relationship" value="{{ ( isset( $remitter_company_details[ 'company_relationship_id' ] ) && ! empty( $remitter_company_details[ 'company_relationship_id' ] ) && $remitter_company_details[ 'company_relationship_id' ] == 1 ? $remitter_company_other_relationship[ 'relationship' ] : '' ) }}" {{ ( isset( $remitter_company_details[ 'company_relationship_id' ] ) && ! empty( $remitter_company_details[ 'company_relationship_id' ] ) && $remitter_company_details[ 'company_relationship_id' ] == 1 ? 'required="required"' : '' ) }}>
																	
																	</div>
																
																</div>
															
															</div>
															<!-- REMITTER COMPANY OTHER RELATIONSHIP END -->
															
														</div>
														<!-- REMITTER COMPANY RELATIONSHIP END -->
													
													</div>
													
													<div class="row">
														
														<!-- REMITTER COMPANY FREQUENCY START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															
															<div class="form-group">

																<label for="remitter_company_frequency_id"> Frequency:<span style="color:#E85445;">*</span> </label>

																<select id="remitter_company_frequency_id" name="remitter_company_frequency_id" class="form-control chosen-select" data-placeholder="Frequency">
																
																	<option value=""></option>
																	
																	@if( isset( $remitter_company_frequencies ) && ! empty( $remitter_company_frequencies ) )
																	
																		@foreach( $remitter_company_frequencies AS $remitter_company_frequency )
																		
																			<option value="{{ $remitter_company_frequency[ 'id' ] }}" {{ ( isset( $remitter_company_details[ 'frequency_id' ] ) && ! empty( $remitter_company_details[ 'frequency_id' ] ) && $remitter_company_details[ 'frequency_id' ] == $remitter_company_frequency[ 'id' ] ? "selected='selected'" : "" ) }}> {{ $remitter_company_frequency[ 'name' ] }} </option>
																		
																		@endforeach
																	
																	@endif
																
																</select>

															</div>
														
														</div>
														<!-- REMITTER COMPANY FREQUENCY END -->
														
														<!-- REMITTER COMPANY BANK TYPE START -->
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
														
															<div class="form-group">

																<label for="remitter_company_bank_type_id"> Bank type:<span style="color:#E85445;">*</span> </label>

																<select id="remitter_company_bank_type_id" name="remitter_company_bank_type_id" class="form-control chosen-select" data-placeholder="Bank type">
																
																	<option value=""></option>
																	
																	@if( isset( $remitter_company_bank_types ) && ! empty( $remitter_company_bank_types ) )
																	
																		@foreach( $remitter_company_bank_types AS $remitter_company_bank_type )
																		
																			<option value="{{ $remitter_company_bank_type[ 'id' ] }}" {{ ( isset( $remitter_company_details[ 'bank_type_id' ] ) && ! empty( $remitter_company_details[ 'bank_type_id' ] ) && $remitter_company_details[ 'bank_type_id' ] == $remitter_company_bank_type[ 'id' ] ? "selected='selected'" : "" ) }}> {{ $remitter_company_bank_type[ 'name' ] }} </option>
																		
																		@endforeach
																	
																	@endif
																
																</select>

															</div>
														
														</div>
														<!-- REMITTER COMPANY BANK TYPE END -->
													
													</div>
													
												</div>
												<!-- COMPANY DETAILS START -->
												
											</div>
											
										</div>
										
									</div>
									
								</div>
							
							</div>
							<!-- REMITTER COMPANY TAB END -->
							
							<!-- REMITTER ADDRESS TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remitter_address_tab" aria-labelledby="address-tab">
								
								<div class="row">
									
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
										<!-- ADDRESS INFORMATION START -->
										<div class="panel panel-default" style="margin:0px;">
											
											<div class="panel-body">
												
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
														
														<a href="#" class="btn btn-primary btn-xs add_address_btn" style="margin: 0px 0px 10px;"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add address </a>
													
													</div>
												
												</div>
												
												<!-- ADDRESS LIST START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													
														<div class="panel panel-default">
															
															<div class="panel-heading"> <i class="fa fa-list-alt" aria-hidden="true"></i> Address list </div>

															<div class="panel-body">
																
																<!-- REMITTER ADDRESS CONTAINER START -->
																<div class="remitter_address_container">
																	
																	@if ( isset( $remitter_addresses ) AND ! empty( $remitter_addresses ) AND COUNT( $remitter_addresses ) )
																		
																		@foreach( $remitter_addresses AS $remitter_address )
																			
																			<div id="remitter_address_item_{{ $remitter_address[ 'id' ] }}_hash" class="well">
																			
																				<input type="hidden" id="remitter_address_id_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][id]" value="{{ ( isset( $remitter_address[ 'id' ] ) && ! empty( $remitter_address[ 'id' ] ) ? $remitter_address[ 'id' ] : 0 ) }}">
																			
																				<!-- REMITTER ADDRESS ITEM REMOVE START -->
																				<div class="row">
																			
																					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">

																						<a href="#" class="btn btn-danger btn-xs remove_remitter_address_item" rel="{{ $remitter_address[ 'id' ] }}_hash" style="margin:0px 0px 15px;"><i class="fa fa-trash"></i> Remove</a>
																						
																					</div>
																				
																				</div>
																				<!-- REMITTER ADDRESS ITEM REMOVE END -->
																				
																				<!-- IS RESIDENT START -->
																				<div class="row">
																					
																					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">

																						<div class="checkbox" style="margin: 0px 0px 15px;">
																							
																							<label>
																								
																								Resident? <input type="checkbox" value="1" id="remitter_address_is_resident_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][is_resident]" class="remitter_address_is_resident" data-hash="{{ $remitter_address[ 'id' ] }}_hash" {{ ( isset( $remitter_address[ 'is_resident' ] ) && ! empty( $remitter_address[ 'is_resident' ] ) && $remitter_address[ 'is_resident' ] ? 'checked="checked"' : '' ) }} value="1">

																							</label>

																						</div>

																					</div>

																					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																					
																						<div class="form-group">
																							
																							<label class="control-label col-lg-7 col-md-7 col-sm-12 col-xs-12"> Number of years of residency? </label>

																							<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
																								
																								<select id="remitter_tenancy_length_id_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][tenancy_length_id]" class="form-control chosen-select remitter_tenancy_frequency" data-placeholder="Years" data-hash="{{ $remitter_address[ 'id' ] }}_hash">
																									
																									<option value=""></option>
																									
																									@if( isset( $tenancy_frequencies ) && ! empty( $tenancy_frequencies ) )
																									
																										@foreach( $tenancy_frequencies as $tenancy_frequency )
																										
																											<option {{ ( isset( $remitter_address[ 'tenancy_length_id' ] ) && ! empty( $remitter_address[ 'tenancy_length_id' ] ) && $remitter_address[ 'tenancy_length_id' ] == $tenancy_frequency[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $tenancy_frequency[ 'id' ] }}"> {{ $tenancy_frequency[ 'name' ] }} </option>
																										
																										@endforeach
																									
																									@endif

																								</select>

																							</div>

																						</div>
																					
																					</div>

																				</div>
																				<!-- IS RESIDENT END -->
																				
																				<div class="row">
																				
																					<!-- REMITTER ADDRESS COUNTRY START -->
																					<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
																					
																						<div class="form-group">
																						
																							<label for="remitter_country_{{ $remitter_address[ 'id' ] }}_hash"> Country:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="remitter_country_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][country]" class="form-control chosen-select remitter_country" data-hash="{{ $remitter_address[ 'id' ] }}_hash" data-placeholder="Country" required="required">
																								
																								<option value=""></option>
																								
																								@if( isset( $countries ) && ! empty( $countries ) )
																								
																									@foreach( $countries as $country )
																									
																										<option {{ ( isset( $remitter_address[ 'country_id' ] ) && ! empty( $remitter_address[ 'country_id' ] ) && $remitter_address[ 'country_id' ] == $country[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $country[ 'id' ] }}"> {{ $country[ 'en_short_name' ] }} </option>

																									@endforeach
																								
																								@endif
																								
																							</select>
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER ADDRESS COUNTRY END -->
																					
																					<!-- REMITTER ADDRESS STATE START -->
																					<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
																						
																						<div class="form-group">
																						
																							<label for="remitter_state_{{ $remitter_address[ 'id' ] }}_hash"> State:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="remitter_state_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][state]" class="form-control chosen-select remitter_state" data-hash="{{ $remitter_address[ 'id' ] }}_hash" data-placeholder="State" required="required">
																								
																								<option value=""></option>
																								
																								@foreach( $states as $state )
																								
																									<option {{ ( isset( $remitter_address[ 'state_id' ] ) && ! empty( $remitter_address[ 'state_id' ] ) && $remitter_address[ 'state_id' ] == $state[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $state[ 'id' ] }}"> {{ $state[ 'name' ] }} </option>
																								
																								@endforeach

																							</select>
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER ADDRESS STATE END -->
																					
																					<!-- REMITTER ADDRESS CITY START -->
																					<div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
																						
																						<div class="form-group">
																						
																							<label for="remitter_city_{{ $remitter_address[ 'id' ] }}_hash"> City:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="remitter_city_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][city]" class="form-control chosen-select remitter_city" data-hash="{{ $remitter_address[ 'id' ] }}_hash" data-placeholder="City" required="required">
																								
																								<option value=""></option>
																								
																								@foreach( $cities as $city )
																								
																									@if( $remitter_address[ 'state_id' ] == $city[ 'state_id' ] )
																								
																										<option {{ ( isset( $remitter_address[ 'city_id' ] ) && ! empty( $remitter_address[ 'city_id' ] ) && $remitter_address[ 'city_id' ] == $city[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $city[ 'id' ] }}"> {{ $city[ 'name' ] }} </option>
																								
																									@endif
																								
																								@endforeach
																					
																							</select>
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER ADDRESS CITY END -->
																					
																					<!-- REMITTER POSTAL START -->
																					<div class="col-lg-2 col-md-2 col-sm-12 col-sm-12">
																						
																						<div class="form-group">
																						
																							<label for="remitter_postal_{{ $remitter_address[ 'id' ] }}_hash"> Postal:<span style="color:#E85445;">*</span> </label>
																							
																							<input type="text" id="remitter_postal_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][postal]" class="form-control remitter_postal" placeholder="Postal" required="required" value="{{ ( isset( $remitter_address[ 'postal_code' ] ) && ! empty( $remitter_address[ 'postal_code' ] ) ? $remitter_address[ 'postal_code' ] : '' ) }}">
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER POSTAL END -->
																					
																					<!-- REMITTER ADDRESS IS PREFERRED START -->
																					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-left">
																						
																						<div class="form-group text-center">
																						
																							<label for="remitter_address_is_preferred_{{ $remitter_address[ 'id' ] }}_hash"> Use </label>
																							
																							<br>
																							
																							<input type="radio" id="remitter_address_is_preferred_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][is_preferred]" class="remitter_address_is_preferred" {{ ( isset( $remitter_address[ 'is_preferred' ] ) && ! empty( $remitter_address[ 'is_preferred' ] ) && $remitter_address[ 'is_preferred' ] ? 'checked="checked"' : '' ) }} value="1">
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER ADDRESS IS PREFERRED END -->
																				
																				</div>
																				
																				<div class="row">
																					
																					<!-- REMITTER CURRENT ADDRESS START -->
																					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																						
																						<div class="form-group">
																						
																							<label for="remitter_current_address_{{ $remitter_address[ 'id' ] }}_hash"> Current address:<span style="color:#E85445;">*</span> </label>
																							
																							<textarea id="remitter_current_address_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][current_address]" placeholder="Current address" class="form-control" rows="2" required="required">{{ ( isset( $remitter_address[ 'current_address' ] ) && ! empty( $remitter_address[ 'current_address' ] ) ? $remitter_address[ 'current_address' ] : '' ) }}</textarea>
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER CURRENT ADDRESS END -->
																				
																					<!-- REMITTER PERMANENT ADDRESS START -->
																					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																						
																						<div class="form-group">
																						
																							<label for="remitter_permanent_address_{{ $remitter_address[ 'id' ] }}_hash"> Permanent address:<span style="color:#E85445;">*</span> </label>
																							
																							<textarea id="remitter_permanent_address_{{ $remitter_address[ 'id' ] }}_hash" name="remitter_address_old[{{ $remitter_address[ 'id' ] }}_hash][permanent_address]" placeholder="Permanent address" class="form-control" rows="2" required="required">{{ ( isset( $remitter_address[ 'permanent_address' ] ) && ! empty( $remitter_address[ 'permanent_address' ] ) ? $remitter_address[ 'permanent_address' ] : '' ) }}</textarea>
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER PERMANENT ADDRESS END -->
																				
																				</div>

																			</div>
																		
																		@endforeach
																	
																	@else
																	
																		<div class="alert alert-danger" role="alert" style="margin:10px 0px;">
																			
																			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No address for this remitter! </h5>
																		
																		</div>
																		
																	@endif
																	
																</div>
																<!-- REMITTER ADDRESS CONTAINER END -->
															
															</div>

														</div>
													
													</div>
												
												</div>
												<!-- ADDRESS LIST END -->
											
											</div>

										</div>
										<!-- ADDRESS INFORMATION END -->
									
									</div>
									
								</div>
							
							</div>
							<!-- REMITTER ADDRESS TAB END -->
							
							<!-- REMITTER CONTACT TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remitter_contact_tab" aria-labelledby="contact-tab">
							
								<div class="row">
									
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
										<!-- CONTACT INFORMATION START -->
										<div class="panel panel-default" style="margin:0px;">
											
											<div class="panel-body">
												
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
														
														<a href="#" class="btn btn-primary btn-xs add_contact_btn" style="margin: 0px 0px 10px;"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add contact </a>
													
													</div>
												
												</div>

												<!-- CONTACT LIST START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													
														<div class="panel panel-default">
															
															<div class="panel-heading"> <i class="fa fa-list-alt" aria-hidden="true"></i> Contact list </div>

															<div class="panel-body">
																
																<!-- REMITTER CONTACT CONTAINER START -->
																<div class="remitter_contact_container">
																	
																	@if ( isset( $remitter_contacts ) AND ! empty( $remitter_contacts ) AND COUNT( $remitter_contacts ) )
																		
																		@foreach( $remitter_contacts AS $remitter_contact )
																		
																			<div id="remitter_contact_item_{{ $remitter_contact[ 'id' ] }}_hash" class="well well-sm">
																			
																				<input type="hidden" id="remitter_contact_id_{{ $remitter_contact[ 'id' ] }}_hash" name="remitter_contact_old[{{ $remitter_contact[ 'id' ] }}_hash][id]" value="{{ ( isset( $remitter_contact[ 'id' ] ) && ! empty( $remitter_contact[ 'id' ] ) ? $remitter_contact[ 'id' ] : 0 ) }}">
																			
																				<div class="row">
																					
																					<!-- REMITTER CONTACT IS PREFERRED START -->
																					<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 text-center">
																						
																						<div class="form-group">
																						
																							<label for="remitter_contact_is_preferred_{{ $remitter_contact[ 'id' ] }}_hash"> Use </label>
																							
																							<br>
																						
																							<input type="radio" id="remitter_contact_is_preferred_{{ $remitter_contact[ 'id' ] }}_hash" name="remitter_contact_old[{{ $remitter_contact[ 'id' ] }}_hash][is_preferred]" class="remitter_contact_is_preferred" {{ ( isset( $remitter_contact[ 'is_preferred' ] ) && ! empty( $remitter_contact[ 'is_preferred' ] ) && $remitter_contact[ 'is_preferred' ] ? 'checked="checked"' : '' ) }} value="1">
																					
																						</div>
																						
																					</div>
																					<!-- REMITTER CONTACT IS PREFERRED END -->
																					
																					<!-- REMITTER CONTACT TYPE START -->
																					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																					
																						<div class="form-group">
																						
																							<label for="remitter_contact_type_{{ $remitter_contact[ 'id' ] }}_hash"> Type:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="remitter_contact_type_{{ $remitter_contact[ 'id' ] }}_hash" name="remitter_contact_old[{{ $remitter_contact[ 'id' ] }}_hash][type]" class="form-control chosen-select remitter_contact_type" data-placeholder="Type" required="required">
																							
																								<option value=""></option>
																								
																								@foreach( $contact_types AS $contact_type )
																								
																									<option {{ ( isset( $remitter_contact[ 'contact_type_id' ] ) && ! empty( $remitter_contact[ 'contact_type_id' ] ) && $remitter_contact[ 'contact_type_id' ] == $contact_type[ 'id' ] ? 'selected="selected"' : '' ) }} value="{{ $contact_type[ 'id' ] }}"> {{ $contact_type[ 'name' ] }} </option>
																								
																								@endforeach
																							
																							</select>
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER CONTACT TYPE END -->
																					
																					<!-- REMITTER CONTACT VALUE START -->
																					<div class="col-lg-5 col-md-5 col-sm-4 col-xs-4">
																					
																						<div class="form-group">
																						
																							<label for="remitter_contact_value_{{ $remitter_contact[ 'id' ] }}_hash"> Contact:<span style="color:#E85445;">*</span> </label>
																							
																							<input type="text" id="remitter_contact_value_{{ $remitter_contact[ 'id' ] }}_hash" name="remitter_contact_old[{{ $remitter_contact[ 'id' ] }}_hash][value]" class="form-control remitter_contact_value" placeholder="Contact" required="required" value="{{ ( isset( $remitter_contact[ 'contact' ] ) && ! empty( $remitter_contact[ 'contact' ] ) ? $remitter_contact[ 'contact' ] : '' ) }}">
																							
																						</div>
																					
																					</div>
																					<!-- REMITTER CONTACT VALUE END -->

																					<!-- REMITTER CONTACT ITEM REMOVE START -->
																					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
																					
																						<a href="#" class="btn btn-danger btn-xs remove_remitter_contact_item" rel="{{ $remitter_contact[ 'id' ] }}_hash" style="margin:30px 0px 0px;"><i class="fa fa-trash"></i> Remove</a>
																					
																					</div>
																					<!-- REMITTER CONTACT ITEM REMOVE END -->

																				</div>
																			
																			</div>
																		
																		@endforeach
																		
																	@else
																		
																		<div class="alert alert-danger" role="alert" style="margin:10px 0px;">
																			
																			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No contact for this remitter! </h5>
																		
																		</div>
																	
																	@endif
																	
																</div>
																<!-- REMITTER CONTACT CONTAINER END -->

															</div>

														</div>
													
													</div>
												
												</div>
												<!-- CONTACT LIST END -->
											
											</div>
											
										</div>
										<!-- CONTACT INFORMATION END -->
										
									</div>
									
								</div>
							
							</div>
							<!-- REMITTER CONTACT TAB END -->
							
							<!-- REMITTER IDENTIFICATION TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remitter_identification_tab" aria-labelledby="identification-tab">
								
								<div class="row">
									
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								
										<!-- IDENTIFICATION INFORMATION START -->
										<div class="panel panel-default" style="margin:0px;">
											
											<div class="panel-body">
												
												<!-- ADD NEW IDENTIFICATION BUTTON START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
														
														<a href="#" class="btn btn-primary btn-xs add_identification_btn" style="margin: 0px 0px 10px;"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add ID </a>
													
													</div>
												
												</div>
												<!-- ADD NEW IDENTIFICATION BUTTON END -->
												
												<!-- IDENTIFICATION LIST START -->
												<div class="row">
													
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													
														<div class="panel panel-default">
															
															<div class="panel-heading"> <i class="fa fa-list-alt" aria-hidden="true"></i> Idenfication list </div>

															<div class="panel-body">
																
																<!-- REMITTER IDENTIFICATION CONTAINER START -->
																<div class="remitter_identification_container">
																	
																	@if( isset( $remitter_identifications ) AND ! empty( $remitter_identifications ) AND COUNT( $remitter_identifications ) )
																		
																		@foreach( $remitter_identifications AS $remitter_identification )
																		
																			<div id="remitter_identification_item_{{ $remitter_identification[ 'id' ] }}_hash" class="well well-sm">
																			
																				<input type="hidden" id="remitter_identification_id_{{ $remitter_identification[ 'id' ] }}_hash" name="remitter_identification_old[{{ $remitter_identification[ 'id' ] }}_hash][id]" class="remitter_identifiation_hidden" value="{{ ( isset( $remitter_identification[ 'id' ] ) && ! empty( $remitter_identification[ 'id' ] ) ? $remitter_identification[ 'id' ] : 0 ) }}">
																			
																				<div class="row">
																					
																					<!-- REMITTER IDENTIFICATION IS PREFERRED START -->
																					<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 text-center">
																						
																						<div class="form-group">
																						
																							<label for="remitter_identification_is_preferred_{{ $remitter_identification[ 'id' ] }}_hash"> Use </label>
																							
																							<br>
																						
																							<input type="radio" id="remitter_identification_is_preferred_{{ $remitter_identification[ 'id' ] }}_hash" name="remitter_identification_old[{{ $remitter_identification[ 'id' ] }}_hash][is_preferred]" class="remitter_identification_is_preferred" {{ ( isset( $remitter_identification[ 'is_preferred' ] ) && ! empty( $remitter_identification[ 'is_preferred' ] ) && $remitter_identification[ 'is_preferred' ] ? 'checked="checked"' : '' ) }} value="1">
																					
																						</div>
																						
																					</div>
																					<!-- REMITTER IDENTIFICATION IS PREFERRED END -->
																					
																					<!-- REMITTER IDENTIFICATION NUMBER START -->
																					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
																					
																						<div class="form-group">
																						
																							<label for="remitter_identification_number_{{ $remitter_identification[ 'id' ] }}_hash"> Number:<span style="color:#E85445;">*</span> </label>
																							
																							<input type="text" id="remitter_identification_number_{{ $remitter_identification[ 'id' ] }}_hash" name="remitter_identification_old[{{ $remitter_identification[ 'id' ] }}_hash][number]" class="form-control remitter_identification_number" placeholder="Number" required="required" value="{{ ( isset( $remitter_identification[ 'number' ] ) && ! empty( $remitter_identification[ 'number' ] ) ? $remitter_identification[ 'number' ] : '' ) }}">
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER IDENTIFICATION NUMBER END -->
																					
																					<!-- REMITTER IDENTIFICATION EXPIRATION START -->
																					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
																					
																						<div class="form-group">
																						
																							<label for="remitter_identification_expiration_{{ $remitter_identification[ 'id' ] }}_hash"> Expiration:<span style="color:#E85445;">*</span> </label>
																							
																							<input type="text" id="remitter_identification_expiration_{{ $remitter_identification[ 'id' ] }}_hash" name="remitter_identification_old[{{ $remitter_identification[ 'id' ] }}_hash][expiration]" class="form-control remitter_identification_expiration datepicker" placeholder="Expiration" readonly="readonly" required="required" value="{{ ( isset( $remitter_identification[ 'expiration_date' ] ) && ! empty( $remitter_identification[ 'expiration_date' ] ) ? $remitter_identification[ 'expiration_date' ] : '' ) }}">
																						
																						</div>
																					
																					</div>
																					<!-- REMITTER IDENTIFICATION EXPIRATION END -->
																					
																					<!-- REMITTER IDENTIFICATION TYPE START -->
																					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
																					
																						<div class="form-group">
																						
																							<label for="remitter_identification_type_{{ $remitter_identification[ 'id' ] }}_hash"> Type:<span style="color:#E85445;">*</span> </label>
																							
																							<select id="remitter_identification_type_{{ $remitter_identification[ 'id' ] }}_hash" name="remitter_identification_old[{{ $remitter_identification[ 'id' ] }}_hash][type]" class="form-control chosen-select remitter_identification_type" data-hash="{{ $remitter_identification[ 'id' ] }}_hash" data-placeholder="Type" required="required">
																							
																								<option value=""></option>
																								
																								@foreach( $identification_types as $identification_type )
																								
																									<option {{ ( isset( $remitter_identification[ 'identification_type_id' ] ) && ! empty( $remitter_identification[ 'identification_type_id' ] ) && $remitter_identification[ 'identification_type_id' ] == $identification_type[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $identification_type[ 'id' ] }}"> {{ $identification_type[ 'name' ] }} </option>
																								
																								@endforeach
																							
																							</select>
																							
																						</div>
																					
																					</div>
																					<!-- REMITTER IDENTIFICATION TYPE END -->
																					
																					<!-- REMITTER IDENTIFICATION ITEM REMOVE START -->
																					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
																					
																						<a href="#" class="btn btn-danger btn-xs remove_remitter_identification_item" rel="{{ $remitter_identification[ 'id' ] }}_hash" style="margin:30px 0px 0px;"><i class="fa fa-trash"></i> Remove</a>
																					
																					</div>
																					<!-- REMITTER IDENTIFICATION ITEM REMOVE START -->

																				</div>
																				
																				<!-- REMITTER IDENTIFICATION OTHER TYPE START -->
																				<div id="remitter_identification_other_type_container_{{ $remitter_identification[ 'id' ] }}_hash" class="row" {{ ( isset( $remitter_identification[ 'identification_type_id' ] ) && ! empty( $remitter_identification[ 'identification_type_id' ] ) && $remitter_identification[ 'identification_type_id' ] == 1 ? 'style=display:block;' : 'style=display:none;' ) }} >
																				
																					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																					
																						<div class="form-group">
																						
																							<label for="remitter_identification_other_type_{{ $remitter_identification[ 'id' ] }}_hash"> Other type:<span style="color:#E85445;">*</span> </label>
																							
																							<input type="text" id="remitter_identification_other_type_{{ $remitter_identification[ 'id' ] }}_hash" name="remitter_identification_old[{{ $remitter_identification[ 'id' ] }}_hash][other_type]" class="form-control remitter_identification_other_type" placeholder="Other type" value="{{ ( isset( $remitter_identification[ 'other_type' ] ) && ! empty( $remitter_identification[ 'other_type' ] ) ? $remitter_identification[ 'other_type' ] : '' ) }}" {{ ( isset( $remitter_identification[ 'identification_type_id' ] ) && ! empty( $remitter_identification[ 'identification_type_id' ] ) && $remitter_identification[ 'identification_type_id' ] == 1 ? 'required="required"' : '' ) }}>
																						
																						</div>
																					
																					</div>
																				
																				</div>
																				<!-- REMITTER IDENTIFICATION OTHER TYPE END -->
																				
																				<!-- REMITTER IDENTIFICATION FILE START -->
																				<div class="row">
																				
																					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																						
																						<div class="form-group">
																							
																							<label for="remitter_identification_file_{{ $remitter_identification[ 'id' ] }}_hash"> File: </label>
																							
																							<div id="remitter_identification_file_{{ $remitter_identification[ 'id' ] }}_hash" class="dropzone text-center" style="min-height:100px; height:auto;">
																								
																								<div class="dz-default dz-message">
																									
																									<span> Drop identification here to upload </span>

																								</div>
																								
																							</div>

																						</div>

																					</div>
																				
																				</div>
																				<!-- REMITTER IDENTIFICATION FILE END -->

																				@if( isset( $remitter_identification[ 'filename' ] ) && ! empty( $remitter_identification[ 'filename' ] ) )
																				
																					<input type="hidden" name="remitter_identification_old[{{ $remitter_identification[ 'id' ] }}_hash][file]" value="{{ $remitter_identification[ 'filename' ] }}">
																				
																				@endif
																			
																			</div>
																		
																		@endforeach
																		
																	@else

																		<div class="alert alert-danger" role="alert" style="margin:10px 0px;">

																			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No I.D. for this remitter! </h5>

																		</div>

																	@endif
																
																</div>
																<!-- REMITTER IDENTIFICATION CONTAINER END -->

															</div>

														</div>
													
													</div>
												
												</div>
												<!-- IDENTIFICATION LIST END -->
												
											</div>
											
										</div>
										<!-- IDENTIFICATION INFORMATION END -->
										
									</div>
										
								</div>
								
							</div>
							<!-- REMITTER IDENTIFICATION TAB END -->
							
							<!-- REMITTER MARKETING TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remitter_marketing_tab" aria-labelledby="marketing-tab">
							
								<div class="row">
								
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									
										<div class="panel panel-warning" style="margin:0px 0px 20px;">
											
											<div class="panel-heading"> <i class="fa fa-info-circle"></i> Details </div>
											
											<div class="panel-body">
												
												<!-- WHERE DID YOU FIND US START -->
												<div class="row">
												
													<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
														
														<div class="form-group">

															<label for="remitter_marketing_source_id"> How did you find us? </label>

															<select id="remitter_marketing_source_id" name="remitter_marketing_source_id" class="form-control chosen-select" data-placeholder="Source">

																<option value=""></option>
																
																@if( isset( $remitter_maketing_sources ) && ! empty( $remitter_maketing_sources ) )
																
																	@foreach( $remitter_maketing_sources AS $remitter_maketing_source )
																	
																		<option {{ ( isset( $remitter_details[ 'marketing_source_id' ] ) && ! empty( $remitter_details[ 'marketing_source_id' ] ) && $remitter_details[ 'marketing_source_id' ] == $remitter_maketing_source[ 'id' ]  ? 'selected="selected"' : "" ) }} value="{{ $remitter_maketing_source[ 'id' ] }}"> {{ $remitter_maketing_source[ 'name' ] }} </option>
																	
																	@endforeach
																
																@endif

															</select>

														</div>

													</div>
												
												</div>
												<!-- WHERE DID YOU FIND US END -->
												
												<div id="remitter_other_marketing_source_container" class="row" {{ ( isset( $remitter_details[ 'marketing_source_id' ] ) && ! empty( $remitter_details[ 'marketing_source_id' ] ) && $remitter_details[ 'marketing_source_id' ] == 1 ? 'style=display:block;' : 'style=display:none;' ) }}>

													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

														<div class="form-group">

															<label for="remitter_other_marketing_source"> Please indicate:<span style="color:#E85445;">*</span> </label>

															<input type="text" id="remitter_other_marketing_source" name="remitter_other_marketing_source" class="form-control" placeholder="Other source" value="{{ ( isset( $remitter_other_marketing_source[ 'marketing_source' ] ) && ! empty( $remitter_other_marketing_source[ 'marketing_source' ] ) ? $remitter_other_marketing_source[ 'marketing_source' ] : '' ) }}" {{ ( isset( $remitter_details[ 'marketing_source_id' ] ) && ! empty( $remitter_details[ 'marketing_source_id' ] ) && $remitter_details[ 'marketing_source_id' ] == 1 ? 'required="required"' : '' ) }}>

														</div>

													</div>

												</div>
												
											</div>
											
										</div>
									
									</div>
								
								</div>
							
							</div>
							<!-- REMITTER MARKETING TAB END -->
							
							<!-- REMITTER NOTE TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remitter_note_tab" aria-labelledby="note-tab">
								
								<div class="row">
								
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
										<div class="form-group">
											
											<label for="remitter_added_note"> Added note <small>(20 chars min, 1000 max)</small>: </label>
											
											<textarea id="remitter_added_note" class="form-control" name="remitter_added_note" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="1000" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" rows="10">{{ ( isset( $remitter_notes[ 'note' ] ) && ! empty( $remitter_notes[ 'note' ] ) ? $remitter_notes[ 'note' ] : '' ) }}</textarea>

										</div>
										
									</div>

								</div>
								
							</div>
							<!-- REMITTER NOTE TAB END -->
							
							<!-- REMITTER HISTORY TAB START -->
							<div role="tabpanel" class="tab-pane fade in" id="remitter_history_tab" aria-labelledby="history-tab">
							
								<div class="row">
								
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									
										@if( isset( $histories ) AND ! empty( $histories ) )
										
											<div class="panel panel-default">

												<div class="panel-heading"> 
													
													<i class="fa fa-list-alt" aria-hidden="true"></i> History list 
													
												</div>

												<div class="panel-body">
													
													<div class="scrollbar-light">
																								
														<div class="table-responsive">
														
															<table class="table table-striped table-bordered table-hover" style="margin: 0px;">
																
																<thead>
																
																	<tr> 
																		
																		<th style="width: 20%;"> <small> Field </small> </th>
																		
																		<th style="width: 30%;"> <small> Old value </small> </th>
																		
																		<th style="width: 30%;"> <small> New value </small> </th>
																		
																		<th style="width: 20%;"> <small> CSR </small> </th>

																	</tr>
																
																</thead>
																
																<tbody>
														
																	@foreach( $histories as $history )

																		<tr>

																			<td> <small> {{ $history[ 'field_name' ] }} </small> </td>
																			
																			<td> <small> {{ $history[ 'old_value' ] }} </small> </td>
																			
																			<td> <small> {{ $history[ 'new_value' ] }} </small> </td>
																			
																			<td> <small> {{ $history[ 'full_name' ] }} </small> </td>
																			
																		</tr>
																			
																	@endforeach
																
																</tbody>
															
															</table>
															
														</div>
													
													</div>
													
												</div>
												
											</div>
											
										@else
										
											<div class="alert alert-danger" role="alert" style="margin:10px 0px;">
										
												<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No history changes for this remitter! </h5>
											
											</div>
											
										@endif
											
									</div>
								
								</div>
							
							</div>
							<!-- REMITTER HISTORY TAB END -->
						
						</div>
						<!-- REMITTER FORM TAB CONTENT END -->
					
					</div>
					<!-- REMITTER TABS END -->

				</div>

				<div class="modal-footer">
					
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" style="margin:0px;"> <i class="fa fa-times" aria-hidden="true"></i> Close </button>

					<button type="submit" class="btn btn-sm btn-primary" style="margin:0px;"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save </button>
					
				</div>

			</form>

		</div>

	</div>

</div>
