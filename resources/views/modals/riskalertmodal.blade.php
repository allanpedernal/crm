<div id="risk-alert-modal" class="modal fade" tabindex="-1" role="dialog">

	<div class="modal-dialog" role="document" style="width:95%">

		<div class="modal-content">	

			<div class="modal-header">
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					
					<span aria-hidden="true"> &times; </span>
					
				</button>
				
				<h4 class="modal-title"> <i class="fa fa-wpforms"></i> Name Placeholded </h4>

			</div>

			<div class="modal-body" id="risk-alert-modal-body">
		
				@include('tables.riskAlert.remittertable')
			
			</div>

			<div class="modal-footer">
				
				<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" style="margin:0px;"> <i class="fa fa-times" aria-hidden="true"></i> Close </button>

				<span id='btnExportRecipientPlaceHolder'></span>
	
				<!-- converted this to anchor rather than jquery -->
				<!-- <button type="button" name="btnExport" value="exportRecipientRemittance" class="btn btn-sm btn-success" style="margin:0px;"> <i class="fa fa-download" aria-hidden="true"></i> Export </button> -->
				
			</div>

		</div>

	</div>

</div>
