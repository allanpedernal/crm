<!-- Registration navigational bar -->
<nav class="navbar navbar-default navbar-fixed-top" style="background-color: #ffffff;">
		
	<div class="container">
		
		<div class="row">
			
			<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
		
				<div class="navbar-header">
					
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						
						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

					</button>

					<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
					<a class="navbar-brand" href="http://103.254.139.202/v2" style="height:auto;"> 
						
						<!-- This are still hard coded. Don't have enough time the image file to laravel environment -->
						<img src="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/images/logo.png" class="logo img-responsive"> 
						
					</a>

				</div>
				
			</div>
				
			<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
					
				<div class="collapse navbar-collapse">
					
					<div class="menu-iremit-container">

						<ul id="menu-iremit" class="nav navbar-nav">

							<li id="registration-menu-home" >
								
								<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
								<a title="Home" href="http://103.254.139.202/v2/">Home</a>

							</li>

							<li id="registration-menu-send-money-now">
								
								<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
								<a title="Send Money Now" href="http://103.254.139.202/v2/steps/">Send Money Now</a>

							</li>

							<li id="registration-menu-services-and-fees">

								<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
								<a title="Services &amp; Fees" href="http://103.254.139.202/v2/services-and-fees/">Services &#038; Fees</a>

							</li>

							<li id="registration-menu-blog">

								<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
								<a title="Blog" href="http://103.254.139.202/v2/fil-aus-blog/">Blog</a>

							</li>

							<li id="registration-menu-about-us">

								<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
								<a title="About Us" href="http://103.254.139.202/v2/about-us/">About Us</a>

							</li>

							<li id="registration-menu-help-and-support">

								<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
								<a title="Help and Support" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Help and Support <span class="caret"></span></a>

								<ul role="menu" class=" dropdown-menu">

									<li id="help-and-support-menu-contact-us">

										<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
										<a title="Contact Us" href="http://103.254.139.202/v2/contact-us/">Contact Us</a>

									</li>

									<li id="help-and-support-menu-customer-service">

										<a title="Customer Service" href="#">Customer Service</a>

									</li>

									<li id="help-and-support-menu-FAQ">

										<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
										<a title="FAQ" href="http://103.254.139.202/v2/frequently-asked-questions/">FAQ</a>

									</li>

								</ul>

							</li>
										
							<li class="hidden-md hidden-sm hidden-xs"> 

								<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
								<button class="btn btn-default btn-sm nav-button gradient round" onclick="window.location.href='{!! url("/") !!}'"> Login </button> 

							</li>

							<li class="hidden-md hidden-sm hidden-xs"> 

								<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
								<button class="btn btn-primary btn-sm nav-button round" onclick="window.location.href=window.location.href='{!! url("/register") !!}'"> Register </button> 

							</li>

							<li class="hidden-md hidden-sm hidden-xs"> <hr style="margin: 5px 0px;"> </li>		
							
							<li class="visible-md visible-sm visible-xs">

								<a href="window.location.href='{!! url("/") !!}'">Login</a>

							</li>
							
							<li class="visible-md visible-sm visible-xs">

								<!-- This are still hard coded. Don't have enough time to migrate the page view in laravel -->
								<a href="window.location.href=window.location.href='{!! url("/register") !!}">Register</a>

							</li>
									
						</ul>

					</div>

				</div>

			</div>
				
		</div>
			
	</div>

</nav>