@extends('layouts.iremit')

@section('content')

	<div class="page-title">
		
		<div class="title_left">
			
			<h3> <i class="fa fa-bell" aria-hidden="true"></i> Notifications </h3>
			
		</div>
		
	</div>

	<div class="clearfix"></div>

	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
			<div class="x_panel">
				
				<div class="x_title">
					
					<a id="add_notification_btn" class="btn btn-primary btn-xs" style="margin: 5px 0px;"> <i class="fa fa-plus"></i> Add Notification </a>
					
					<ul class="nav navbar-right panel_toolbox">
						
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						
						<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a></li>
						
						<li><a class="close-link"><i class="fa fa-close"></i></a></li>
					
					</ul>
					
					<div class="clearfix"></div>
					
				</div>
				
				<div class="x_content">
					
					<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					  
						<thead>
							
							<tr>
								
								<th> Name </th>
								
								<th> Email </th>

								<th> Message </th>
								
								<th> Type </th>

								<th class="text-center"> Action </th>

							</tr>

						</thead>

						<tbody>
							
							<tr>
								
								<td> Allan Joseph Pedernal </td>
								
								<td> allan@iremit.com.au </td>

								<td> This is sample message </td>

								<td> Group </td>

								<td class="text-center">
									
									<a class="btn btn-info btn-xs"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send</a>
									
									<a class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Edit</a>
									
									<a class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
									
								</td>

							</tr>

							<tr>
								
								<td> Allan Joseph Pedernal </td>
								
								<td> allan@iremit.com.au </td>

								<td> This is sample message </td>

								<td> User </td>

								<td class="text-center">
									
									<a class="btn btn-info btn-xs"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send</a>
									
									<a class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Edit</a>
									
									<a class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
									
								</td>

							</tr>
							
							<tr>
								
								<td> Allan Joseph Pedernal </td>
								
								<td> allan@iremit.com.au </td>

								<td> This is sample message </td>

								<td> Group </td>

								<td class="text-center">
									
									<a class="btn btn-info btn-xs"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send</a>
									
									<a class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Edit</a>
									
									<a class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
									
								</td>

							</tr>
							
							<tr>
								
								<td> Allan Joseph Pedernal </td>
								
								<td> allan@iremit.com.au </td>

								<td> This is sample message </td>

								<td> User </td>

								<td class="text-center">
									
									<a class="btn btn-info btn-xs"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send</a>
									
									<a class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Edit</a>
									
									<a class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
									
								</td>

							</tr>
							
						</tbody>
					
					</table>
				
				</div>
			
			</div>
			
		</div>
		
	</div>
	
@endsection

@section('styles')

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">

	<link href="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">

@endsection

@section('scripts')

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js') }} "></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/jszip/dist/jszip.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/pdfmake/build/pdfmake.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
	
	<script src="{{ URL::asset('bower_components/gentelella/vendors/dropzone/dist/min/dropzone.min.js') }}"></script>
	
	<script src="{{ URL::asset('bower_components/gentelella/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
	
	<script src="{{ URL::asset('bower_components/gentelella/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
	
    <script src="{{ URL::asset('bower_components/gentelella/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/google-code-prettify/src/prettify.js') }}"></script>
	
	<script type="text/javascript">
		
		jQuery( document ).ready( function() {
			
			//INIT ALL
			init_all();

			//ADDING OF NOTIFICATION
			jQuery( document ).on( 'click', '#add_notification_btn', function( e ) {
				
				e.preventDefault();
				
				
				//SHOW MODAL
				jQuery( '#notification_modal' ).modal( { backdrop: 'static', keyboard: false } );

			} );
			
		} );
		
		//INIT ALL
		function init_all() {
			
			//INIT DATATABLES
			init_datatables();
			
			//INIT DATEPICKER
			init_datepicker();
			
		}

		//INIT DATATABLES
		function init_datatables() {
			
			jQuery( '#datatable-responsive' ).DataTable( {
				
				ordering: false,

				responsive: true

			} );
		
		}

		//INIT DATEPICKER
		function init_datepicker() {
			
			//INITIALIZE DATEPICKER
			jQuery('.datepicker').daterangepicker( {
				
				singleDatePicker: true,
				
				showDropdowns: true,
				
				format: 'YYYY-MM-DD'
				
			} );
		
		}

	</script>
	
@endsection
