@extends('layouts.iremit')

@section('content')

	<div class="page-title">
		
		<div class="title_left">
			
			<h3> Polipayment Connect </h3>
			
		</div>
		
	</div>

	<div class="clearfix"></div>
	
	
	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
			<div class="x_panel">
				
				<div class="x_title">
					
					<ul class="nav navbar-right panel_toolbox">
						
						<li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
						
						<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> </li>
						
						<li> <a class="close-link"> <i class="fa fa-close"></i> </a> </li>
					
					</ul>
					
					<div class="clearfix"></div>
					
				</div>
				
				<div class="x_content">
					
					<div class="alert alert-danger" role="alert" style="margin:10px 0px;">
						
						<h5 class="text-center" style="margin:5px 0px;"> PAGE UNDER CONSTRUCTION </h5>
					
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</div>

	
@endsection
