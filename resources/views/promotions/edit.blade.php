@extends('layouts.iremit')

@section('content')

<div class="page-title">
	<div class="title_left">		
		<h3> <i class="fa fa-mobile fa-fw"></i> Edit Promotion </h3>
	</div>
</div>

<div class="clearfix"></div>

<div class="row">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 

		<form class="form-horizontal form-label-left" method="POST" action="{{ url('/promotions/' . $promotion->id) }}">
			{{ csrf_field() }}
			<input name="_method" type="hidden" value="PUT">
			<div class="item form-group">
		        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Short Description <span class="required">*</span>
		        </label>
		        <div class="col-md-6 col-sm-6 col-xs-12">
		          <input id="name" class="form-control col-md-7 col-xs-12" name="short_description" value="{{ $promotion->short_description }}" required="required" type="text">
		        </div>
	      	</div>
	        <div class="item form-group">
	        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Detail Description <span class="required">*</span>
	            </label>
	        	<div class="col-md-6 col-sm-6 col-xs-12">
	          		  <textarea id="textarea" required="required" name="detail_description" class="form-control col-md-7 col-xs-12">{{ $promotion->detail_description }}</textarea>
	        	</div>
	        </div>
	        <div class="ln_solid"></div>
          	<div class="form-group">
	            <div class="col-md-6 col-md-offset-3">
	              	<button id="send" type="submit" class="btn btn-success">Update</button>
	            </div>
        	</div>
        </form>
               
	</div>

</div>

@endsection