@extends('layouts.iremit')

@section('content')

<div class="page-title">
	<div class="title_left">		
		<h3> <i class="fa fa-mobile fa-fw"></i> Promotions </h3>
	</div>
</div>

<div class="clearfix"></div>

<div class="row">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">          
		<div class="x_panel">
	    	<div class="x_title">
		        
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
		            <ul class="dropdown-menu" role="menu">
		              <li><a href="#">Settings 1</a>
		              </li>
		              <li><a href="#">Settings 2</a>
		              </li>
		            </ul>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
	        </div>
         	
         	<div class="x_content">

	            <table class="table table-striped">
	            	<thead>
	            		<tr>
	            			<th>Short Description</th>
	            			<th>Created By</th>
	            			<th>Is Send?</th>
	            			<th>Send Times</th>
	            			<th>Created At</th>
	            			<th>Action</th>
	            		</tr>
	            	</thead>
	            	<tbody>
	            		@if (count($promotions) > 0)
	            		@foreach ($promotions as $promotion)
	            		<tr>
	            			<td>{{ $promotion->short_description }}</td>
	            			<td>{{ $promotion->user_id }}</td>
	            			<td>{!! $promotion->is_send_string !!}</td>
	            			<td>{{ $promotion->send_times }}</td>
	            			<td>{{ $promotion->created_at }}</td>
	            			<td>
	            				<a href="/promotions/{{ $promotion->id }}/edit" class="btn btn-default btn-sm">
                                	<i class="fa fa-eye fw"></i> Edit
                            	</a>

                            	<button type="button" data-id="27" class="btn btn-default btn-sm send-btn">
	                                <i class="fa fa-send fw"></i> Send
	                            </button>

                            	<form method="POST" action="{{ url('/promotions/' . $promotion->id) }}">
                            		{{ csrf_field() }}
                            		<input name="_method" type="hidden" value="DELETE">
                            		<button class="btn btn-danger btn-sm" type="submit">
		                                <i class="fa fa-trash fw"></i> Delete
		                            </button>
                            	</form>
                            	
	            			</td>
	            		</tr>
	            		@endforeach
	            		@else
	            		<tr>
	            			<td colspan="6">EMPTY</td>
	            		</tr>
	            		@endif
	            	</tbody>
	            </table>

          	</div>
        </div>
    </div>
</div>
@endsection