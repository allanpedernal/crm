@extends('layouts.iremit')

@section('content')



	<form method="POST" action="{{ URL('/settings/form/post') }}" id="setting_form" class="form-horizontal" data-validate="parsley" onSubmit="return on_validate();">

		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<button type="submit" class="btn btn-lg btn-primary" style="margin-top: 10px;"> <i class="fa fa-floppy-o"></i> Save </button>
			
			</div>
		
		</div>

		
		<br>

		
		<div class="page-title">
			
			<div class="title_left">
				
				<h3> <i class="fa fa-bar-chart" aria-hidden="true"></i> Rates </h3>
				
			</div>
			
		</div>

		<div class="clearfix"></div>
		
		<br>
		
		<div class="row">
			
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				
				<div class="form-group">

					<h4> Forex :<span style="color:#E85445;">*</span> </h4>

					<div class="input-group input-group-lg">
					
						<span class="input-group-addon">$</span>

						<input type="text" id="setting_forex_rate" name="setting_forex_rate" class="form-control" placeholder="Forex rate" value="{{ ( isset( $rates[ 'forex' ] ) && ! empty( $rates[ 'forex' ] ) ? number_format( $rates[ 'forex' ], 2, '.', ',' ) : 0.00 ) }}">

					</div>

				</div>

			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				
				<div class="form-group">

					<h4> Trade :<span style="color:#E85445;">*</span> </h4>

					<div class="input-group input-group-lg">
					
						<span class="input-group-addon">$</span>

						<input type="text" id="setting_trade_rate" name="setting_trade_rate" class="form-control" placeholder="Trade rate" value="{{ ( isset( $rates[ 'trade' ] ) && ! empty( $rates[ 'trade' ] ) ? number_format( $rates[ 'trade' ], 2, '.', ',' ) : 0.00 ) }}">

					</div>

				</div>

			</div>
			
		</div>
		
		<hr>
		
		
		
		<div class="page-title">
			
			<div class="title_left">
				
				<h3> <i class="fa fa-bar-chart" aria-hidden="true"></i> Fees </h3>
				
			</div>
			
		</div>

		<div class="clearfix"></div>
		
		<br>
		
		<div class="row">
			
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				
				<div class="form-group">

					<h4> Cash pickup :<span style="color:#E85445;">*</span> </h4>

					<div class="input-group input-group-lg">
					
						<span class="input-group-addon">$</span>

						<input type="text" id="setting_service_fee_cash_pickup" name="setting_service_fee_cash_pickup" class="form-control" placeholder="Cash pickup" value="{{ ( isset( $fees[ 'cash-pickup' ] ) && ! empty( $fees[ 'cash-pickup' ] ) ? number_format( $fees[ 'cash-pickup' ], 2, '.', ',' ) : 0.00 ) }}">

					</div>

				</div>

			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				
				<div class="form-group">

					<h4> Bank to bank :<span style="color:#E85445;">*</span> </h4>

					<div class="input-group input-group-lg">
					
						<span class="input-group-addon">$</span>

						<input type="text" id="setting_service_fee_bank_to_bank" name="setting_service_fee_bank_to_bank" class="form-control" placeholder="Bank to bank" value="{{ ( isset( $fees[ 'bank-to-bank' ] ) && ! empty( $fees[ 'bank-to-bank' ] ) ? number_format( $fees[ 'bank-to-bank' ], 2, '.', ',' ) : 0.00 ) }}">

					</div>

				</div>

			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				
				<div class="form-group">

					<h4> Door to door :<span style="color:#E85445;">*</span> </h4>

					<div class="input-group input-group-lg">
					
						<span class="input-group-addon">$</span>

						<input type="text" id="setting_service_fee_door_to_door" name="setting_service_fee_door_to_door" class="form-control" placeholder="Door to door" value="{{ ( isset( $fees[ 'door-to-door' ] ) && ! empty( $fees[ 'door-to-door' ] ) ? number_format( $fees[ 'door-to-door' ], 2, '.', ',' ) : 0.00 ) }}">

					</div>

				</div>

			</div>
			
		</div>

		<hr>
		
		
		
		<div class="page-title">
			
			<div class="title_left">
				
				<h3> Table Rates </h3>
				
			</div>
			
		</div>

		<div class="clearfix"></div>
		
		<br>
		
		<div class="row">
		
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				
				<h3> Mlhuilier </h3>
				
				<br>
				
				<div class="form-group" style="margin-bottom: 20px;">

					<h4> 50k - 100k :<span style="color:#E85445;">*</span> </h4>

					<div class="input-group input-group-lg">
					
						<span class="input-group-addon">$</span>

						<input type="text" name="setting_custom_service_fee[mlhuilier][50-100]" class="form-control" placeholder="Service fee" value="{{ ( isset( $custom_fees[ 'mlhuillier' ][ '50-100' ] ) && ! empty( $custom_fees[ 'mlhuillier' ][ '50-100' ] ) ? number_format( $custom_fees[ 'mlhuillier' ][ '50-100' ], 2, '.', ',' ) : 0.00 ) }}">

					</div>

				</div>
			
				<div class="form-group" style="margin-bottom: 20px;">

					<h4> 101k - 150k :<span style="color:#E85445;">*</span> </h4>

					<div class="input-group input-group-lg">
					
						<span class="input-group-addon">$</span>

						<input type="text" name="setting_custom_service_fee[mlhuilier][101-150]" class="form-control" placeholder="Service fee" value="{{ ( isset( $custom_fees[ 'mlhuillier' ][ '101-150' ] ) && ! empty( $custom_fees[ 'mlhuillier' ][ '101-150' ] ) ? number_format( $custom_fees[ 'mlhuillier' ][ '101-150' ], 2, '.', ',' ) : 0.00 ) }}">

					</div>

				</div>
			
				<div class="form-group" style="margin-bottom: 20px;">

					<h4> 151k - 200k :<span style="color:#E85445;">*</span> </h4>

					<div class="input-group input-group-lg">
					
						<span class="input-group-addon">$</span>

						<input type="text" name="setting_custom_service_fee[mlhuilier][151-200]" class="form-control" placeholder="Service fee" value="{{ ( isset( $custom_fees[ 'mlhuillier' ][ '151-200' ] ) && ! empty( $custom_fees[ 'mlhuillier' ][ '151-200' ] ) ? number_format( $custom_fees[ 'mlhuillier' ][ '151-200' ], 2, '.', ',' ) : 0.00 ) }}">

					</div>

				</div>

				<div class="form-group" style="margin-bottom: 20px;">

					<h4> 201k - 250k :<span style="color:#E85445;">*</span> </h4>

					<div class="input-group input-group-lg">
					
						<span class="input-group-addon">$</span>

						<input type="text" name="setting_custom_service_fee[mlhuilier][201-250]" class="form-control" placeholder="Service fee" value="{{ ( isset( $custom_fees[ 'mlhuillier' ][ '201-250' ] ) && ! empty( $custom_fees[ 'mlhuillier' ][ '201-250' ] ) ? number_format( $custom_fees[ 'mlhuillier' ][ '201-250' ], 2, '.', ',' ) : 0.00 ) }}">

					</div>

				</div>
			
			</div>
			
		</div>

		
		<br>
		

		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				<button type="submit" class="btn btn-lg btn-primary" style="margin-bottom: 10px;"> <i class="fa fa-floppy-o"></i> Save </button>
			
			</div>
		
		</div>

		
	</form>



@endsection

@section('styles')

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('bower_components/gentelella/build/css/custom.min.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('css/chosen.min.css') }}" rel="stylesheet">

	<style type="text/css">
	
		/*IS LOADING PLUGIN*/
		.isloading-wrapper.isloading-right{ margin-left:10px; }

		.isloading-overlay{ position:relative;text-align:center; }

		.isloading-overlay .isloading-wrapper {

			line-height: 20px; 
			
			background:#ffffff; 
			
			-webkit-border-radius:7px; 
			
			-webkit-background-clip:padding-box; 
			
			-moz-border-radius:7px; 
			
			-moz-background-clip:padding;
			
			border-radius:7px;
			
			background-clip:padding-box;
			
			display:inline-block;
			
			margin:0 auto;
			
			padding:10px 20px;
			
			z-index:9000;

		}
		
		.help-block { color: #E74C3C; }

	</style>

@endsection

@section('scripts')

	<script src="{{ URL::asset('bower_components/gentelella/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
	
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>
    
	<script src="{{ URL::asset('js/jquery.isloading.min.js') }}"></script>
	
	<script src="{{ URL::asset('js/chosen.jquery.min.js') }}"></script>
	
    <script type="text/javascript">
		
		jQuery( document ).ready( function() {
			
			//INITIALIZE ALL
			init_all();

			//SUBMIT SETTING FORM
			jQuery( document ).on( 'submit', '#setting_form', function( e ) { 
				
				e.preventDefault();
				
				//CONFIRM ANSWER
				var answer = confirm( 'Do you want to submit this setting form?' );
				
				//IF YES
				if( answer ) {
					
					//SHOW LOADING
					jQuery( 'body' ).isLoading( {
						
						text: 'Submitting form',
						
						class: 'fa fa fa-spinner fa-spin',
						
						position: 'overlay',
						
						transparency: 0.5
						
					} );
					
					//GET DATA
					var data = jQuery( this ).serializeArray();
					
					data.push( { name: '_token', value: '{{ csrf_token() }}' } );
					
					//AJAX FORM DATA
					jQuery.post( '{{ url( "/settings/form/post" ) }}', data, function( response ) { 
					
						//FORM SUBMIT SUCCESSFUL
						if( response.success ) {
							
							new PNotify( {
								
								title: 'Success!',

								text: 'Settings has been updated!',

								type: 'success',
								
								styling: 'bootstrap3'

							} );

							//INIT ALL
							init_all();

						}

						else {

							//SHOW SOME ERROR MESSAGE
							alert( response.error_message );

						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
					}, 'json' );
				
				}
				
			} );

		} );
    
		//INIT ALL
		function init_all() {
			
			//INIT DATEPICKER
			init_datepicker();
			
			//INIT CHOSEN
			init_chosen();

			//INIT PARSELEY
			init_parseley();

		}

		//INIT DATEPICKER
		function init_datepicker() {
			
			//INITIALIZE DATEPICKER
			jQuery( '.datepicker' ).daterangepicker( {
				
				singleDatePicker: true,
				
				showDropdowns: true,
				
				format: 'YYYY-MM-DD'
				
			} );
		
		}

		//INIT CHOSEN
		function init_chosen() {
			
			var config = {
				
				'.chosen-select'           : { width: '100%', allow_single_deselect: true },
				
				'.chosen-select-deselect'  : { allow_single_deselect: true },
				
				'.chosen-select-no-single' : { disable_search_threshold: 10 },
				
				'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
				
				'.chosen-select-rtl'       : { rtl: true },
				
				'.chosen-select-width'     : { width: '100%' }
				
			}

			for ( var selector in config ) {
				
				jQuery( selector ).chosen( config[ selector ] );

			}
			
		}
		
		//INIT PARSELEY
		function init_parseley() {
			
			jQuery( '#setting_form' ).parsley( {
				
				errorsContainer: function(el) {
					
					return el.$element.closest( '.form-group' );
					
				},

				errorsWrapper: '<span class="help-block"></span>',

				errorTemplate: '<span></span>',

				errorMessage: 'Required'
				
			} );
			
		}
		
		//ON VALIDATE
		function on_validate() {

			if( ! jQuery( '#setting_form' ).parsley().validate() ) {

				return false;

			}

			return true;

		}

    </script>

@endsection
