@extends('layouts.iremit')

@section('content')

	<div class="page-title">
		
		<div class="title_left">
			
			<h3> <i class="fa fa-users"></i> Recipients </h3>
			
		</div>
		
	</div>

	<div class="clearfix"></div>

	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
			<div class="x_panel">
				
				<div class="x_title">
					
					<a href="{{ url('/iremit/recipients/modal') }}" class="btn btn-primary btn-xs show_modal_btn" style="margin: 5px 0px;"> <i class="fa fa-plus"></i> Add New Recipient </a>
					
					<ul class="nav navbar-right panel_toolbox">
						
						<li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
						
						<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> </li>
						
						<li> <a class="close-link"> <i class="fa fa-close"></i> </a> </li>
					
					</ul>
					
					<div class="clearfix"></div>
					
				</div>
				
				<div class="x_content">
					
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 5px;">
						
						<div class="panel panel-default">
							
							<div class="panel-heading" role="tab" id="search_recipient">
								
								<h4 class="panel-title">
									
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="<?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'true' : 'false' ); ?>" aria-controls="collapseOne" <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? '' : 'class="collapsed"' ); ?>>

										<small> Click me to show search </small>

									</a>

								</h4>

							</div>

							<div id="collapseOne" class="panel-collapse collapse <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'in' : '' ); ?>" role="tabpanel" aria-labelledby="search_recipient">

								<div class="panel-body">
									
									<form method="GET" id="search_recipient_form" class="form-horizontal">

										<div class="row">
											
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="search_recipient_full_name"> Person Full Name / Company Name: </label>
													
													<input type="text" id="search_recipient_full_name" name="search_recipient_full_name" class="form-control" value="<?php echo ( isset( $_GET[ 'search_recipient_full_name' ] ) && ! empty( $_GET[ 'search_recipient_full_name' ] ) ? $_GET[ 'search_recipient_full_name' ] : '' ); ?>"> 
												
												</div>
											
											</div>
											
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="search_recipient_contact"> Contact: </label>
													
													<input type="text" id="search_recipient_contact" name="search_recipient_contact" class="form-control" value="<?php echo ( isset( $_GET[ 'search_recipient_contact' ] ) && ! empty( $_GET[ 'search_recipient_contact' ] ) ? $_GET[ 'search_recipient_contact' ] : '' ); ?>"> 
												
												</div>
											
											</div>

											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
												
												<div class="form-group">
													
													<label for="search_recipient_remitter"> Remitter: </label>
													
													<select id="search_recipient_remitter" name="search_recipient_remitter" class="chosen-select form-control" data-placeholder="Select remitter">
														
														<option value=""></option>
														
														@if( isset( $remitter ) AND ! empty( $remitter ) )
														
															<option value="{{ $remitter[ 'id' ] }}" selected="selected"> {{ $remitter[ 'fullname' ] }} | {{ $remitter[ 'email' ] }} </option>
														
														@endif

													</select>
													
												</div>
											
											</div>

										</div>
										
										<div class="row">
										
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												
												<a href="{{ url( 'iremit/recipients' ) }}" class="btn btn-default btn-sm"> <i class="fa fa-refresh" aria-hidden="true"></i> Reset </a>
												
												<button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> Search </button>
												
											</div>
										
										</div>

									</form>
									
								</div>

							</div>

						</div>

					</div>

					@include( 'tables.recipient_table' )

				</div>
			
			</div>
			
		</div>
		
	</div>

@endsection

@section('styles')

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('bower_components/gentelella/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('bower_components/gentelella/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('bower_components/gentelella/build/css/custom.min.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('css/chosen.min.css') }}" rel="stylesheet">
    
	<style type="text/css">
	
		/*IS LOADING PLUGIN*/
		.isloading-wrapper.isloading-right{ margin-left:10px; }

		.isloading-overlay{ position:relative;text-align:center; }

		.isloading-overlay .isloading-wrapper {

			line-height: 20px; 
			
			background:#ffffff; 
			
			-webkit-border-radius:7px; 
			
			-webkit-background-clip:padding-box; 
			
			-moz-border-radius:7px; 
			
			-moz-background-clip:padding;
			
			border-radius:7px;
			
			background-clip:padding-box;
			
			display:inline-block;
			
			margin:0 auto;
			
			padding:10px 20px;
			
			z-index:9000;

		}
		
		.pagination{ margin: 10px 0px 5px; }
		
		.help-block { color: #E74C3C; }
		
		.scrollbar-light { height: 215px; overflow-y: scroll; }
		
		.checkbox label, .radio label{ font-weight: 700; padding-left: 0px; }
	
	</style>
	
@endsection

@section('scripts')

	<script src="{{ URL::asset('bower_components/gentelella/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
	
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/iCheck/icheck.min.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/switchery/dist/switchery.min.js') }}"></script>
    
	<script src="{{ URL::asset('js/jquery.isloading.min.js') }}"></script>
	
	<script src="{{ URL::asset('js/handlebars-v4.0.5.js') }}"></script>
	
	<script src="{{ URL::asset('js/chosen.jquery.min.js') }}"></script>
	
	<script src="{{ URL::asset('js/chosen.ajaxaddition.jquery.js') }}"></script>
	
	<script type="text/javascript">
		
		jQuery( document ).ready( function() {
			
			//INIT ALL
			init_all();
			
			//SHOW MODAL
			jQuery( document ).on( 'click', '.show_modal_btn', function( e ) {
				
				e.preventDefault();
				
				var href = jQuery( this ).attr( 'href' );
				
				//SHOW LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Loading',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );
				
				//CALL RECIPIENT AJAX
				jQuery.get( href, function( response ) {
					
					if( response.success ) {
						
						//GET ACTION
						var action = response.result.action;
						
						//APPEND AJAX MODAL
						jQuery( 'body' ).append( response.result.content );
						
						//OPEN MODAL
						jQuery( '#recipient_modal' ).modal( { backdrop: 'static', keyboard: false } );
						
						//INIT SWITCHERY
						init_switchery();
						
						//REINITIALIZE ALL
						init_all();

					}
					
					else {
						
						//SHOW SOME ERROR MESSAGE
						alert( response.error_message );
						
					}
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
				}, 'json' );

			} );
			
			//REMOVE RECIPIENT MODAL
			jQuery( document ).on( 'hidden.bs.modal', '#recipient_modal', function( e ) {
				
				jQuery( this ).remove();

				jQuery( '.modal-backdrop' ).remove();

				jQuery( 'body' ).removeClass( 'modal-open' );
				
			} );

			
			//RECIPIENT IS COMPANY
			jQuery( document ).on( 'click', '#recipient_is_company', function( e ) {
				
				//CHECK IF RECIPIENT IS COMPANY IS CHECKED
				if( jQuery( this ).is( ':checked' ) ) {
					
					//FADEOUT PERSONAL CONTAINER
					jQuery( '.personal_container' ).fadeOut( 'normal', function() {
						
						//FADEIN COMPANY CONTAINER
						jQuery( '.company_container' ).fadeIn();
						
						//REMOVE RECIPIENT PERSON REQUIRED ATTR
						remove_recipient_person_required_attr();
						
						//ADD RECIPIENT COMPANY REQUIRED ATTR
						add_recipient_company_required_attr();
						
						//RECIPIENT TYPE
						jQuery( '.recipient_type' ).html( '<i class="fa fa-building-o"></i> Company' );
						
					} );
					
				}
				
				//RECIPIENT IS COMPANY IS NOT CHECKED
				else {
					
					//FADEOUT COMPANY CONTAINER
					jQuery( '.company_container' ).fadeOut( 'normal', function() {
						
						//FADEIN PERSONAL CONTAINER
						jQuery( '.personal_container' ).fadeIn();
						
						//REMOVE RECIPIENT COMPANY REQUIRED ATTR
						remove_recipient_company_required_attr();
						
						//ADD RECIPIENT PERSON REQUIRED ATTR
						add_recipient_person_required_attr();
						
						//RECIPIENT TYPE
						jQuery( '.recipient_type' ).html( '<i class="fa fa-user"></i> Personal' );
						
					} );
					
				}
				
			} );


			//SHOW RECIPIENT OTHER RELATIONSHIP CONTAINER
			jQuery( document ).on( 'change', '#recipient_relationship', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				//OTHERS
				if( current_value == 1 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#recipient_other_relationship_container' ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#recipient_other_relationship_container' ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#recipient_other_relationship' ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#recipient_other_relationship_container' ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#recipient_other_relationship_container' ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#recipient_other_relationship' ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#recipient_other_relationship' ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#recipient_other_relationship' ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );

			//SHOW RECIPIENT RECEIVE OPTION OTHER BANK CONTAINER
			jQuery( document ).on( 'change', '#recipient_receive_option', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				//OTHERS
				if( current_value == 41 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#recipient_receive_option_other_bank_container' ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#recipient_receive_option_other_bank_container' ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#recipient_receive_option_other_bank' ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#recipient_receive_option_other_bank_container' ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#recipient_receive_option_other_bank_container' ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#recipient_receive_option_other_bank' ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#recipient_receive_option_other_bank' ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#recipient_receive_option_other_bank' ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );


			//ADDRESS
			jQuery( document ).on( 'click', '.add_address_btn', function( e ) { 
			
				e.preventDefault();
				
				
				var hash = Math.random().toString( 36 ).slice( 2 );
				
				var countries = {!! $countries !!};

				
				var source = jQuery( '#recipient_address_template' ).html();
				
				var template = Handlebars.compile( source );

				var html = template( {

					hash: hash,

					countries: countries
					
				} );
				
				
				var total_is_preferred = jQuery( '.recipient_address_is_preferred' ).length;
				
				if( total_is_preferred >= 3 ) {
					
					alert( "You already reach maximum remitter address!" );
					
				}
				
				else {
				
					//CHECK IF IS PREFFERED IS ALREADY POPULATED
					if( total_is_preferred == 0 ) {
						
						//REMOVE NOTIFICATION
						jQuery( '.recipient_address_container' ).html( html );
					
					}
					
					else {
						
						//APPEND NEW REMITTER ADDRESS TEMPLATE
						jQuery( '.recipient_address_container' ).append( html ).fadeIn();
						
					}
					
				}
				
				//REINITIALIZE ALL
				init_all();
			
			} );

			//REMOVE RECIPIENT ADDRESS ITEM
			jQuery( document ).on( 'click', '.remove_recipient_address_item', function( e ) {
				
				var hash = jQuery( this ).attr( 'rel' );
			
				e.preventDefault();
				
				jQuery( '#recipient_address_item_' + hash ).remove();

				var total_is_preferred = jQuery( '.recipient_address_is_preferred' ).length;
				
				//CHECK IF IS PREFFERED IS ALREADY POPULATED
				if( total_is_preferred == 0 ) {
					
					//NOTIFICATION TEMPLATE
					var notification_template = '';
					
					notification_template += '<div class="alert alert-danger" role="alert" style="margin:10px 0px;">';

					notification_template += '<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No address for this recipient! </h5>';

					notification_template += '</div>';
					
					//REMOVE NOTIFICATION
					jQuery( '.recipient_address_container' ).html( notification_template );
				
				}
				
			} );

			//GET RECIPIENT STATE
			jQuery( document ).on( 'change', '.recipient_country', function( e ) {
				
				e.preventDefault();
				
				var hash = jQuery( this ).data( 'hash' );
				
				var country_id = jQuery( '#recipient_country_' + hash ).val();


				//LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Retreiving states',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );
				
				//CHECK IF COUNTRY ID IS EMPTY
				if( country_id == '' ) {
					
					//RESET STATE
					jQuery( '#remitter_state_' + hash ).empty().append( '<option value=""></option>' ).trigger( 'chosen:updated' );
					
					//RESET CITY
					jQuery( '#remitter_city_' + hash ).empty().append( '<option value=""></option>' ).trigger( 'chosen:updated' );
					
					//RESET POSTAL
					jQuery( '#remitter_postal_' + hash ).val();
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
					//INIT ALL
					init_all();
					
				}
				
				//GET STATES FROM COUNTRY
				else {

					jQuery.get( '{{ url( "iremit/library/get-states-from-country" ) }}' + '/' + country_id + '/' + hash, function( response ) {
						
						if( response.success ) {
							
							
							var hash = response.result.hash;
							
							var states = response.result.states;
							
									
							var source = jQuery( '#recipient_state_option_template' ).html();
							
							var template = Handlebars.compile( source );

							var html = template( {
								
								states: states
								
							} );
							
							
							jQuery( '#recipient_state_' + hash ).html( html ).trigger( 'chosen:updated' );
							
							
						}
						
						else {
							
							alert( response.error_message );
							
						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
						//REINITIALIZE ALL
						init_all();
						
					}, 'json' );
				
				}
			
			} );

			//GET RECIPIENT CITY
			jQuery( document ).on( 'change', '.recipient_state', function( e ) {
			
				e.preventDefault();


				var hash = jQuery( this ).data( 'hash' );
				
				var province_id = jQuery( '#recipient_state_' + hash ).val();


				//LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Retreiving cities',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );

				//CHECK IF PROVINCE ID IS EMPTY
				if( province_id == '' ) {
					
					//RESET CITY
					jQuery( '#remitter_city_' + hash ).empty().append( '<option value=""></option>' ).trigger( 'chosen:updated' );
					
					//RESET POSTAL
					jQuery( '#remitter_postal_' + hash ).val();
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
					//INIT ALL
					init_all();
					
				}
				
				//GET CITIES FROM STATE
				else {
						
					jQuery.get( '{{ url( "iremit/library/get-cities-from-state" ) }}' + '/' + province_id + '/' + hash, function( response ) {
						
						if( response.success ) {
							
							
							var hash = response.result.hash;
							
							var cities = response.result.cities;
							
									
							var source = jQuery( '#recipient_city_option_template' ).html();
							
							var template = Handlebars.compile( source );

							var html = template( {
								
								cities: cities
								
							} );
							
							
							jQuery( '#recipient_city_' + hash ).html( html ).trigger( 'chosen:updated' );
							
							
						}
						
						else {
							
							alert( response.error_message );
							
						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
					
						//REINITIALIZE ALL
						init_all();
						
					}, 'json' );
				
				}
				
			} );

			//GET RECIPIENT POSTAL
			jQuery( document ).on( 'change', '.recipient_city', function( e ) { 
				
				e.preventDefault();
				
				var hash = jQuery( this ).data( 'hash' );
				
				var city_id = jQuery( '#recipient_city_' + hash ).val();


				//LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Retreiving postal',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );
				
				//CHECK IF CITY ID IS EMPTY
				if( city_id == '' ) {
					
					//RESET POSTAL
					jQuery( '#remitter_postal_' + hash ).val();
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
					//INIT ALL
					init_all();
					
				}
				
				//GET POSTAL FROM CITY
				else {
						
					jQuery.get( '{{ url( "iremit/library/get-postal-code-from-city" ) }}' + '/' + city_id + '/' + hash, function( response ) {
						
						if( response.success ) {
							
							var hash = response.result.hash;
							
							var postal_code = response.result.postal_code;
							
							jQuery( '#recipient_postal_' + hash ).val( postal_code );
							
						}
						
						else {
							
							alert( response.error_message );
							
						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
						//REINITIALIZE ALL
						init_all();
						
					}, 'json' );
				
				}	
			
			} );
			
			//RECIPIENT ADDRESS IS PREFERRED
			jQuery( document ).on( 'ifChanged', '.recipient_address_is_preferred', function( e ) { 
			
				jQuery( '.recipient_address_is_preferred' ).removeAttr( 'checked' ).iCheck( 'update' );
				
				jQuery( this ).prop( 'checked', true ).iCheck( 'update' );
				
			} );

			//CONTACT
			jQuery( document ).on( 'click', '.add_contact_btn', function( e ) { 
				
				e.preventDefault();
				
				var hash = Math.random().toString( 36 ).slice( 2 );
				
				var contact_types = {!! $contact_types !!};

				
				var source = jQuery( '#recipient_contact_template' ).html();
				
				var template = Handlebars.compile( source );

				var html = template( {

					hash: hash,

					contact_types: contact_types
					
				} );
				
				
				var total_is_preferred = jQuery( '.recipient_contact_is_preferred' ).length;
				
				if( total_is_preferred >= 3 ) {
					
					alert( "You already reach maximum recipient contact!" );
					
				}
				
				else {
				
					//CHECK IF IS PREFFERED IS ALREADY POPULATED
					if( total_is_preferred == 0 ) {
						
						//REMOVE NOTIFICATION
						jQuery( '.recipient_contact_container' ).html( html );
					
					}
					
					else {
						
						//APPEND NEW RECIPIENT CONTACT TEMPLATE
						jQuery( '.recipient_contact_container' ).append( html ).fadeIn();
						
					}
					
				}
				
				//REINITIALIZE ALL
				init_all();
				
			} );

			//REMOVE RECIPIENT CONTACT ITEM
			jQuery( document ).on( 'click', '.remove_recipient_contact_item', function( e ) {
				
				var hash = jQuery( this ).attr( 'rel' );
			
				e.preventDefault();
				
				jQuery( '#recipient_contact_item_' + hash ).remove();

				var total_is_preferred = jQuery( '.recipient_contact_is_preferred' ).length;
				
				//CHECK IF IS PREFFERED IS ALREADY POPULATED
				if( total_is_preferred == 0 ) {
					
					//NOTIFICATION TEMPLATE
					var notification_template = '';
					
					notification_template += '<div class="alert alert-danger" role="alert" style="margin:10px 0px;">';

					notification_template += '<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No contact for this recipient! </h5>';

					notification_template += '</div>';
					
					//REMOVE NOTIFICATION
					jQuery( '.recipient_contact_container' ).html( notification_template );
				
				}
				
			} );

			//RECIPIENT CONTACT IS PREFERRED
			jQuery( document ).on( 'ifChanged', '.recipient_contact_is_preferred', function( e ) { 
			
				jQuery( '.recipient_contact_is_preferred' ).removeAttr( 'checked' ).iCheck( 'update' );
				
				jQuery( this ).prop( 'checked', true ).iCheck( 'update' );
				
			} );

			//BANK
			jQuery( document ).on( 'click', '.add_bank_btn', function( e ) { 
				
				e.preventDefault();
				
				var hash = Math.random().toString( 36 ).slice( 2 );
				
				var bank_types = {!! $bank_types !!};

				
				var source = jQuery( '#recipient_bank_template' ).html();
				
				var template = Handlebars.compile( source );

				var html = template( {

					hash: hash,

					bank_types: bank_types
					
				} );
				
				
				var total_recipient_bank = jQuery( '.recipient_bank' ).length;
				
				if( total_recipient_bank >= 3 ) {
					
					alert( "You already reach maximum recipient bank!" );
					
				}
				
				else {
				
					//CHECK IF RECIPIENT BANK TYPE IS ALREADY POPULATED
					if( total_recipient_bank == 0 ) {
						
						//REMOVE NOTIFICATION
						jQuery( '.recipient_bank_container' ).html( html );
					
					}
					
					else {
						
						//APPEND NEW RECIPIENT BANK TEMPLATE
						jQuery( '.recipient_bank_container' ).append( html ).fadeIn();
						
					}
					
				}
				
				//REINITIALIZE ALL
				init_all();
				
			} );

			//SHOW OTHER BANK
			jQuery( document ).on( 'change', '.recipient_bank', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				var hash = jQuery( this ).data( 'hash' );
				
				//OTHERS
				if( current_value == 41 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#recipient_other_bank_container_' + hash ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#recipient_other_bank_container_' + hash ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#recipient_other_bank_' + hash ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#recipient_other_bank_container_' + hash ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#recipient_other_bank_container_' + hash ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#recipient_other_bank_' + hash ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#recipient_other_bank_' + hash ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#recipient_other_bank_' + hash ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );

			//REMOVE RECIPIENT BANK ITEM
			jQuery( document ).on( 'click', '.remove_recipient_bank_item', function( e ) {
				
				e.preventDefault();
				
				var hash = jQuery( this ).attr( 'rel' );
				
				jQuery( '#recipient_bank_item_' + hash ).remove();

				var total_is_preferred = jQuery( '.recipient_bank_is_preferred' ).length;
				
				//CHECK IF IS PREFFERED IS ALREADY POPULATED
				if( total_is_preferred == 0 ) {
					
					//NOTIFICATION TEMPLATE
					var notification_template = '';
					
					notification_template += '<div class="alert alert-danger" role="alert" style="margin:10px 0px;">';

					notification_template += '<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No bank for this recipient! </h5>';

					notification_template += '</div>';
					
					//REMOVE NOTIFICATION
					jQuery( '.recipient_bank_container' ).html( notification_template );
				
				}
				
			} );

			//RECIPIENT BANK IS PREFERRED
			jQuery( document ).on( 'ifChanged', '.recipient_bank_is_preferred', function( e ) { 
			
				jQuery( '.recipient_bank_is_preferred' ).removeAttr( 'checked' ).iCheck( 'update' );
				
				jQuery( this ).prop( 'checked', true ).iCheck( 'update' );
				
			} );


			//CHECK ALL RECIPIENT CHECKBOX
			jQuery( document ).on( 'ifChanged', '#select_all_recipients', function() {
					
				//CHECK IF REMITTANCE SELECT ALL IS NOT CHECKED
				if ( ! jQuery( this ).is( ':checked' ) ) {
					
					//REMOVE ALL CHECKBOX CHECKED ATTR
					jQuery( '.recipient_checkbox' ).prop( 'checked', false ).iCheck( 'update' );
					
					//REMOVE CLASS SELECTED TO ALL TR
					jQuery( '.jambo_table tbody tr' ).removeClass( 'selected' );

				} 

				else {
					
					//ADD ALL CHECKBOX CHECKED ATTR
					jQuery( '.recipient_checkbox' ).prop( 'checked', true ).iCheck( 'update' );
					
					//ADD CLASS SELECTED TO ALL TR
					jQuery( '.jambo_table tbody tr' ).addClass( 'selected' );
				
				} 
				  
			} );
			
			//CHECK RECIPIENT CHECKBOX
			jQuery( document ).on( 'ifChanged', '.recipient_checkbox', function() {
				
				var all = jQuery( '.recipient_checkbox' );
				
				//CHECK IF ALL REMITTANCE CHECKBOX IS CHECKED
				if ( all.length === all.filter( ':checked' ).length ) {
					
					//CHECK SELECT ALL CHECKBOX
					jQuery( '#select_all_recipients' ).prop( 'checked', true ).iCheck( 'update' );

				}

				else {
					
					//REMOVE CHECK SELECT ALL CHECKBOX
					jQuery( '#select_all_recipients' ).prop( 'checked', false );

				}

			} );
			

			//SUBMIT RECIPIENT FORM
			jQuery( document ).on( 'submit', '#recipient_form', function( e ) {
				
				e.preventDefault();
				
				
				//CHECK IF ADDRESS IS ALREADY SELECTED
				var total_address_is_preferred = jQuery( '.recipient_address_is_preferred' ).length;
				
				if( total_address_is_preferred >= 1 ) {
					
					//CHECK IF ADDRESS IS ALREADY SELECTED
					var address_selected = jQuery( '.recipient_address_is_preferred:checked' ).length;
					
					//IF ADDRESS IS NOT SELECTED
					if( address_selected == null || address_selected == 0 ) {
						
						//ALERT CURRENT USER
						alert( 'Please select recipient address!' );
						
						jQuery( 'a[href="#recipient_address_tab"]' ).trigger( 'click' );
						
						return false;
						
					}
					
				}
				
				
				//CHECK IF CONTACT IS ALREADY SELECTED
				var total_contact_is_preferred = jQuery( '.recipient_contact_is_preferred' ).length;
				
				if( total_contact_is_preferred >= 1 ) {
					
					//CHECK IF CONTACT IS ALREADY SELECTED
					var contact_selected = jQuery( '.recipient_contact_is_preferred:checked' ).length;
					
					//IF CONTACT IS NOT SELECTED
					if( contact_selected == null || contact_selected == 0 ) {
						
						//ALERT CURRENT USER
						alert( 'Please select recipient contact!' );
						
						jQuery( 'a[href="#recipient_contact_tab"]' ).trigger( 'click' );
						
						return false;
						
					}
					
				}
				
				
				//CHECK IF BANK IS ALREADY SELECTED
				var total_bank_is_preferred = jQuery( '.recipient_bank_is_preferred' ).length;
				
				if( total_bank_is_preferred >= 1 ) {
					
					//CHECK IF BANK IS ALREADY SELECTED
					var bank_selected = jQuery( '.recipient_bank_is_preferred:checked' ).length;
					
					//IF BANK IS NOT SELECTED
					if( bank_selected == null || bank_selected == 0 ) {
						
						//ALERT CURRENT USER
						alert( 'Please select recipient bank!' );
						
						jQuery( 'a[href="#recipient_bank_tab"]' ).trigger( 'click' );
						
						return false;
						
					}
					
				}
				
				
				//CONFIRM ANSWER
				var answer = confirm( 'Do you want to submit this recipient form?' );
				
				//IF YES
				if( answer ) {
					
					//SHOW LOADING
					jQuery( 'body' ).isLoading( {
						
						text: 'Submitting form',
						
						class: 'fa fa fa-spinner fa-spin',
						
						position: 'overlay',
						
						transparency: 0.5
						
					} );
					
					//GET DATA
					var data = jQuery( this ).serializeArray();
					
					data.push( { name: '_token', value: '{{ csrf_token() }}' } );
					
					//AJAX FORM DATA
					jQuery.post( '{{ url( "/iremit/recipients/form/post" ) }}', data, function( response ) { 
					
						//FORM SUBMIT SUCCESSFUL
						if( response.success ) {

							//GET RECIPIENT ID
							var recipient_id = response.result.recipient_id;

							//ADD NEW RECORD
							if( response.result.action == 'add' ) {
								
								new PNotify( {
									
									title: 'Success!',

									text: 'Recipient has been added!',

									type: 'success',
									
									styling: 'bootstrap3'

								} );

							}

							//EDIT RECORD
							if( response.result.action == 'edit' ) {
								
								new PNotify( {
									
									title: 'Success!',

									text: 'Recipient has been updated!',
									
									styling: 'bootstrap3'

								} );

							}
							
							//UPDATE RECIPIENT TABLE
							jQuery( '.recipient_table_container' ).html( response.result.recipient_table_template );

							//HIDE MODAL
							jQuery( '#recipient_modal' ).modal( 'hide' );
							
							//INIT ALL
							init_all();

						}

						else {

							//SHOW SOME ERROR MESSAGE
							alert( response.error_message );

						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
					}, 'json' );
				
				}
				
				
			} );


			//DELETE RECIPIENTS
			jQuery( document ).on( 'submit', '#recipient_table', function( e ) {
				
				e.preventDefault();
				
				var total_checked_recipients = jQuery( 'input[name="recipient[]"]:checked' ).length;
				
				if( total_checked_recipients ) {
					
					var answer_text = 'Do you want to delete this recipient' + ( total_checked_recipients > 1 ? 's' : '' );
					
					var answer = confirm( answer_text + '?' );
					
					if( answer ) {
						
						var data = jQuery( this ).serializeArray();
						
						data.push( { name: '_token', value: '{{ csrf_token() }}' } );
						
						jQuery.post( '{{ url( "/iremit/recipients/delete-recipients" ) }}', data, function( response ) {
							
							//FORM SUBMIT SUCCESSFUL
							if( response.success ) {
								
								if( response.result.action == 'delete' ) {
									
									var pnotify_text = 'Recipient' + ( response.result.total_recipient > 1 ? 's' : '' ) + ' has been deleted!';
									
									new PNotify( {
										
										title: 'Success!',

										text: pnotify_text + '!',

										type: 'error',
										
										styling: 'bootstrap3'

									} );

								}

								//UPDATE REMITTANCE TABLE
								jQuery( '.recipient_table_container' ).html( response.result.recipient_table_template );

								//HIDE MODAL
								jQuery( '#recipient_modal' ).modal( 'hide' );
								
								//INIT ALL
								init_all();
								
							}

							else {

								//SHOW SOME ERROR MESSAGE
								alert( response.error_message );

							}
							
						} );
						
					}
				
				}
				
				else {
					
					alert( 'Please select recipient that you want to delete!' );
					
				}
				
			} );
			
		} );
		
		//INIT ALL
		function init_all() {
			
			//INIT DATEPICKER
			init_datepicker();
			
			//INIT CHOSEN
			init_chosen();
			
			//INIT ICHECK
			init_icheck();
			
			//INIT PARSELEY
			init_parseley();
			
			//REINITIALIZE ICHECK EVENT
			reinitialize_icheck_event();
			
			//INIT RECIPIENT REMITTER
			init_recipient_remitter();
			
			//INIT SEARCH RECIPIENT REMITTER
			init_search_recipient_remitter();

		}
		
		//INIT DATEPICKER
		function init_datepicker() {
			
			//INITIALIZE DATEPICKER
			jQuery( '.datepicker' ).daterangepicker( {
				
				singleDatePicker: true,
				
				showDropdowns: true,
				
				format: 'YYYY-MM-DD'
				
			} );
		
		}
		
		//INIT CHOSEN
		function init_chosen() {
			
			var config = {
				
				'.chosen-select'           : { width: '100%', allow_single_deselect: true },
				
				'.chosen-select-deselect'  : { allow_single_deselect: true },
				
				'.chosen-select-no-single' : { disable_search_threshold: 10 },
				
				'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
				
				'.chosen-select-rtl'       : { rtl: true },
				
				'.chosen-select-width'     : { width: '100%' }
				
			}

			for ( var selector in config ) {
				
				jQuery( selector ).chosen( config[ selector ] );

			}
			
		}
		
		//INIT ICHECK
		function init_icheck() {
			
			jQuery( '.recipient_address_is_preferred, .recipient_contact_is_preferred, .recipient_bank_is_preferred' ).iCheck( {
				
				checkboxClass: 'icheckbox_flat-green',
				
				radioClass: 'iradio_flat-green'
				
			} );
			
		}

		//INIT SWITCHERY
		function init_switchery() {
			
			var elem = document.querySelector( '.js-switch' );
			
			var switchry = new Switchery( elem, { className:"switchery" } );

		}

		//INIT RECIPIENT REMITTER
		function init_recipient_remitter() {
			
			jQuery( '#recipient_remitter' ).ajaxChosen( {
				
				dataType: 'json',
				
				type: 'POST',
				
				url:"{{ url( 'iremit/recipients/get-remitter/' ) }}",
				
				data: { 'remitter' : jQuery( '#recipient_remitter' ).val(), '_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ) },
				
				success: function( data, textStatus, jqXHR ) {
					
					return data;
					
				}
				
			}, {
				
				processItems: function( data ) { 					
					
					var remitters = {};
					
					jQuery.each( data.results, function ( i, val ) {
						
						remitters[ val.id ] = val.text;
						
					} );
					
					return remitters; 
					
				},
				
				useAjax: function( e ) { return true; },
				
				generateUrl: function( q ) { return "{{ url( 'iremit/recipients/get-remitter' ) }}"; },
				
				loadingImg: "{{ URL::asset( 'img/loading.gif' ) }}",
				
				minLength: 2
				
			} );
			
		}

		//INIT SEARCH RECIPIENT REMITTER
		function init_search_recipient_remitter() {
			
			jQuery( '#search_recipient_remitter' ).ajaxChosen( {
				
				dataType: 'json',
				
				type: 'POST',
				
				url:"{{ url( 'iremit/recipients/get-remitter/' ) }}",
				
				data: { 'remitter' : jQuery( '#search_recipient_remitter' ).val(), '_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ) },
				
				success: function( data, textStatus, jqXHR ) {
					
					return data;
					
				}
				
			}, {
				
				processItems: function( data ) { 					
					
					var remitters = {};
					
					jQuery.each( data.results, function ( i, val ) {
						
						remitters[ val.id ] = val.text;
						
					} );
					
					return remitters; 
					
				},
				
				useAjax: function( e ) { return true; },
				
				generateUrl: function( q ) { return "{{ url( 'iremit/recipients/get-remitter' ) }}"; },
				
				loadingImg: "{{ URL::asset( 'img/loading.gif' ) }}",
				
				minLength: 2
				
			} );
			
		}

		//INIT PARSELEY
		function init_parseley() {
			
			jQuery( '#recipient_form' ).parsley( {
				
				errorsContainer: function(el) {
					
					return el.$element.closest( '.form-group' );
					
				},

				errorsWrapper: '<span class="help-block"></span>',

				errorTemplate: '<span></span>',

				errorMessage: 'Required'
				
			} );
			
		}

		//ON VALIDATE
		function on_validate() {

			if( ! jQuery( '#recipient_form' ).parsley().validate() ) {
				
				var tab_id = jQuery( '.parsley-error' ).closest( '.tab-pane' ).attr( 'id' );
				
				jQuery( 'a[href="#'+tab_id+'"]' ).trigger( 'click' );

				return false;

			}

			return true;

		}



		//ADD RECIPIENT PERSON REQUIRED ATTR
		function add_recipient_person_required_attr() {
			
			var recipient_person_fields = [];
			
			recipient_person_fields[0] = 'recipient_title';
			
			recipient_person_fields[1] = 'recipient_firstname';
			
			recipient_person_fields[2] = 'recipient_lastname';
			
			recipient_person_fields[3] = 'recipient_birthday';
			
			recipient_person_fields[4] = 'recipient_civil_status';
			
			recipient_person_fields[5] = 'recipient_gender';

			//LOOP ALL FIELDS
			jQuery.each( recipient_person_fields, function( i, v ) {
			
				jQuery( '#' + v ).attr( 'required', 'true' );
							
			} );
			
		}

		//REMOVE RECIPIENT PERSON REQUIRED ATTR
		function remove_recipient_person_required_attr() {
			
			var parseley_id = 0;
			
			var recipient_person_fields = [];
			
			recipient_person_fields[0] = 'recipient_title';
			
			recipient_person_fields[1] = 'recipient_firstname';
			
			recipient_person_fields[2] = 'recipient_lastname';
			
			recipient_person_fields[3] = 'recipient_birthday';
			
			recipient_person_fields[4] = 'recipient_civil_status';
			
			recipient_person_fields[5] = 'recipient_gender';
			
			//LOOP ALL FIELDS
			jQuery.each( recipient_person_fields, function( i, v ) {
			
				parseley_id = jQuery( '#' + v ).data( 'parsley-id' );

				jQuery( '#' + v ).removeAttr( 'required' );

				jQuery( '#' + v ).removeClass( 'parsley-error' );

				jQuery( '#parsley-id-' + parseley_id ).remove();
			
			} );

		}


		//ADD RECIPIENT COMPANY REQUIRED ATTR
		function add_recipient_company_required_attr() {
			
			var recipient_company_fields = [];
			
			recipient_company_fields[0] = 'recipient_company_name';
			
			recipient_company_fields[1] = 'recipient_reg_number';
			
			recipient_company_fields[2] = 'recipient_contact_person';
			
			recipient_company_fields[3] = 'recipient_authorized_person';

			
			//LOOP ALL FIELDS
			jQuery.each( recipient_company_fields, function( i, v ) {
			
				jQuery( '#' + v ).attr( 'required', 'true' );
							
			} );
			
		}

		//REMOVE RECIPIENT COMPANY REQUIRED ATTR
		function remove_recipient_company_required_attr() {
			
			var parseley_id = 0;
			
			var recipient_company_fields = [];
			
			recipient_company_fields[0] = 'recipient_company_name';
			
			recipient_company_fields[1] = 'recipient_reg_number';
			
			recipient_company_fields[2] = 'recipient_contact_person';
			
			recipient_company_fields[3] = 'recipient_authorized_person';
			
			//LOOP ALL FIELDS
			jQuery.each( recipient_company_fields, function( i, v ) {
			
				parseley_id = jQuery( '#' + v ).data( 'parsley-id' );

				jQuery( '#' + v ).removeAttr( 'required' );

				jQuery( '#' + v ).removeClass( 'parsley-error' );

				jQuery( '#parsley-id-' + parseley_id ).remove();
			
			} );

		}


		//REINITIALIZE ICHECK EVENT
		function reinitialize_icheck_event() {
			
			jQuery( document ).ready( function() {
			
				jQuery( "input.flat" )[ 0 ] && jQuery( document ).ready( function() {
					
					jQuery( "input.flat" ).iCheck( {
						
						checkboxClass: "icheckbox_flat-green",
						
						radioClass: "iradio_flat-green"
						
					} );
					
					jQuery( "table input" ).on( "ifChecked", function() { checkState = "", jQuery( this ).parent().parent().parent().addClass( "selected" ), countChecked() } ); 
					
					jQuery( "table input" ).on( "ifUnchecked", function() { checkState = "", jQuery( this ).parent().parent().parent().removeClass( "selected" ), countChecked() } );
					
				} );
				
			} );
			
		}


	</script>
	
	
	<!-- INCLUDE HANDLEBARS -->
	@include( 'handlebars.recipient_address_handlebars' )
	
	@include( 'handlebars.recipient_contact_handlebars' )
	
	@include( 'handlebars.recipient_bank_handlebars' )
	

@endsection
