@extends( 'layouts.register' )

@section( 'navigational-bar' )

	@include( 'navbars.registration_step1' )

@endsection

@section( 'remitter-fill-up-form' )

	@include( 'forms.remitter-fill-up-form' )

@endsection	

@section( 'meta' )

    <!-- CSRF TOKEN -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<meta name="robots" content="noindex, nofollow">
		
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">	

    <!-- This site is optimized with the Yoast SEO plugin v4.9 - https://yoast.com/wordpress/plugins/seo/ -->
	<meta name="description" content="Fill the form to complete remittance instruction to the Philippines. I-Remit To the Philippines transfers to Philippine Major Banks as well as cash collections like BDO Remit, Cebuanna and SM Store Cash collection."/>

	<meta property="og:locale" content="en_US" />

	<meta property="og:type" content="article" />

	<meta property="og:title" content="Transfer Money To The Philippines | iRemit to the Philippines" />

	<meta property="og:description" content="Fill the form to complete remittance instruction to the Philippines. I-Remit To the Philippines transfers to Philippine Major Banks as well as cash collections like BDO Remit, Cebuanna and SM Store Cash collection." />

	<meta property="og:url" content="http://103.254.139.202/v2/steps/" />

	<meta property="og:site_name" content="iRemit To The Philippines" />

	<meta property="article:publisher" content="https://www.facebook.com/iRemitAustralia" />

	<meta property="og:image" content="https://iremit.com.au/wp-content/uploads/2016/12/iRemit-to-the-Philippines.jpg" />

	<meta name="twitter:card" content="summary_large_image" />

	<meta name="twitter:description" content="Fill the form to complete remittance instruction to the Philippines. I-Remit To the Philippines transfers to Philippine Major Banks as well as cash collections like BDO Remit, Cebuanna and SM Store Cash collection." />

	<meta name="twitter:title" content="Transfer Money To The Philippines | iRemit to the Philippines" />

	<meta name="twitter:site" content="@iRemitAustralia" />

	<meta name="twitter:image" content="https://iremit.com.au/wp-content/uploads/2016/12/iRemit-to-the-Philippines.jpg" />

	<meta name="twitter:creator" content="@iRemitAustralia" />
	<!-- / Yoast SEO plugin. -->

	<meta name="generator" content="WordPress 4.8.3" />

	<meta property="og:title" content="Send Money Now" /><meta property="og:url" content="http://103.254.139.202/v2/steps/" />

@endsection

@section( 'css' )

	<!-- Note: devdmbootstrap3 should be on the top most -->

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='bootstrap.css-css'  href='http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3/css/bootstrap.css?ver=1' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='stylesheet-css'  href='http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/style.css?ver=1' type='text/css' media='all' />

	<link rel="pingback" href="http://103.254.139.202/v2/xmlrpc.php" />

	<!-- This site is optimized with the Yoast SEO plugin v4.9 - https://yoast.com/wordpress/plugins/seo/ -->

	<link rel="canonical" href="http://103.254.139.202/v2/steps/" />

	<link rel="publisher" href="https://plus.google.com/+iRemitAu/"/>

	<!-- / Yoast SEO plugin. -->

	<link rel='dns-prefetch' href='//ajax.googleapis.com' />

	<link rel='dns-prefetch' href='//ajax.aspnetcdn.com' />

	<link rel='dns-prefetch' href='//s.w.org' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel="alternate" type="application/rss+xml" title="iRemit To The Philippines &raquo; Feed" href="http://103.254.139.202/v2/feed/" />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel="alternate" type="application/rss+xml" title="iRemit To The Philippines &raquo; Comments Feed" href="http://103.254.139.202/v2/comments/feed/" />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel="alternate" type="application/rss+xml" title="iRemit To The Philippines &raquo; Send Money Now Comments Feed" href="http://103.254.139.202/v2/steps/feed/" />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='theme-my-login-css'  href='http://103.254.139.202/v2/wp-content/plugins/theme-my-login/theme-my-login.css?ver=6.4.9' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='form-style-css'  href='http://103.254.139.202/v2/wp-content/plugins/newsletter-subscription-form/options/css/form-style.css?ver=4.8.3' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='contact-form-7-css'  href='http://103.254.139.202/v2/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.8' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='thickbox.css-css'  href='http://103.254.139.202/v2/wp-includes/js/thickbox/thickbox.css?ver=1.0' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='dashicons-css'  href='http://103.254.139.202/v2/wp-includes/css/dashicons.min.css?ver=4.8.3' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='thickbox-css'  href='http://103.254.139.202/v2/wp-includes/js/thickbox/thickbox.css?ver=4.8.3' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='wp_share_button_style-css'  href='http://103.254.139.202/v2/wp-content/plugins/wp-share-button/css/style.css?ver=4.8.3' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='font-awesome-css'  href='http://103.254.139.202/v2/wp-content/plugins/types/library/toolset/toolset-common/res/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='screen' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='jquery-ui-css'  href='http://103.254.139.202/v2/wp-content/plugins/wp-share-button/admin/css/jquery-ui.css?ver=4.8.3' type='text/css' media='all' />
	
	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link type="text/css" rel="stylesheet" href="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/css/font-awesome.min.css">

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link type="text/css" rel="stylesheet" href="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/css/buttons.css">

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link type="text/css" rel="stylesheet" href="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/css/animations.css">
	
	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link type="text/css" rel="stylesheet" href="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/css/hover-min.css">
	
	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->		
	<link type="text/css" rel="stylesheet" href="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/css/responsive-homepage.css">

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link type="text/css" rel="stylesheet" href="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/css/global.css">

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='wp-pagenavi-css'  href='http://103.254.139.202/v2/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=2.70' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='testimonial-rotator-style-css'  href='http://103.254.139.202/v2/wp-content/plugins/testimonial-rotator/testimonial-rotator-style.css?ver=4.8.3' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='load_iremit_css-css'  href='http://103.254.139.202/v2/wp-content/plugins/iremit/iremit_style.css?ver=4.8.3' type='text/css' media='all' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    
    <!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
    <link rel='https://api.w.org/' href='http://103.254.139.202/v2/wp-json/' />

    <!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://103.254.139.202/v2/xmlrpc.php?rsd" />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://103.254.139.202/v2/wp-includes/wlwmanifest.xml" /> 

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='shortlink' href='http://103.254.139.202/v2/?p=2125' />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel="alternate" type="application/json+oembed" href="http://103.254.139.202/v2/wp-json/oembed/1.0/embed?url=http%3A%2F%2F103.254.139.202%2Fv2%2Fsteps%2F" />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel="alternate" type="text/xml+oembed" href="http://103.254.139.202/v2/wp-json/oembed/1.0/embed?url=http%3A%2F%2F103.254.139.202%2Fv2%2Fsteps%2F&#038;format=xml" />

	<!-- Still hard coded. Don't have enough time to migrate the file and update the url in laravel -->	
	<link rel='stylesheet' id='Css-css'  href='http://103.254.139.202/v2/wp-content/plugins/spider-event-calendar/elements/calendar-jos.css?ver=1.5.53' type='text/css' media='' />

	<link rel='stylesheet' id='wp-color-picker-css'  href='http://103.254.139.202/v2/wp-admin/css/color-picker.min.css?ver=4.8.3' type='text/css' media='all' />

	<link rel='stylesheet' id='sticky_post-admin-ui-css-css'  href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css?ver=1.9.0' type='text/css' media='' />

	<style type="text/css">

		img.wp-smiley,

		img.emoji 
		{

			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;

		}

	</style>

	<style>

		.custom-header-text-color { color: #000 }
    
    </style>

    <style type="text/css">
		
		.navbar-collapse.in {
			
			overflow-y: auto !important;
			
		}
		
		.theme-default .nivo-directionNav a { width: 120px !important; height: 120px !important; background: url( 'http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/images/arrows.png' ) no-repeat !important; }

		.theme-default a.nivo-nextNav { background-position: -145px 0px !important; }
		
		/* Custom, iPhone Retina */
		@media only screen and (min-width: 320px) {
			
							
			.theme-default .nivo-controlNav { position: relative; margin-top: 0px; }
			
						
					
		}
		
		/* Extra Small Devices, Phones */
		@media only screen and (min-width: 480px) {
			
							
			.theme-default .nivo-controlNav { position: relative; margin-top: -52px; }
			
						
						
		}

		/* Small Devices, Tablets */
		@media only screen and (min-width: 768px) {

							
			.theme-default .nivo-controlNav { position: relative; margin-top: -52px; }
			
						
					
		}

		/* Medium Devices, Desktops */
		@media only screen and (min-width: 992px) {
			
							
			.theme-default .nivo-controlNav { position: relative; margin-top: -52px; }
			
						
					
		}

		/* Large Devices, Wide Screens */
		@media only screen and (min-width: 1200px) {
			
							
			.theme-default .nivo-controlNav { position: relative; margin-top: -52px; }
			
						
						
		}
			
		@media only screen and (min-width: 1321px) {
			
			.forex_calculator_well{ top: -505px !important; background: #ffffff; border-radius: 0px; position: absolute; left: 0px; max-width: 435px;margin: 0px 0px 0px 10%; }
			
		}
			
				
	</style>

@endsection	

@section( 'scripts' )

	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/newsletter-subscription-form/options/js/form_js.js?ver=4.8.3'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript'>

		/* <![CDATA[ */
		var wp_share_button_ajax = {"wp_share_button_ajaxurl":"http:\/\/103.254.139.202\/v2\/wp-admin\/admin-ajax.php"};
		/* ]]> */

	</script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/wp-share-button/js/scripts.js?ver=4.8.3'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/testimonial-rotator/js/jquery.cycletwo.js?ver=4.8.3'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/testimonial-rotator/js/jquery.cycletwo.addons.js?ver=4.8.3'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/theme-my-login/modules/themed-profiles/themed-profiles.js?ver=4.8.3'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/spider-event-calendar/elements/calendar.js?ver=1.5.53'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/spider-event-calendar/elements/calendar-setup.js?ver=1.5.53'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/spider-event-calendar/elements/calendar_function.js?ver=1.5.53'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script src="http://103.254.139.202/v2/wp-content/plugins/iremit/js/jquery.isloading.min.js"></script>

	<script type='text/javascript'>
		/* <![CDATA[ */
		var wpcf7 = {"apiSettings":{"root":"http:\/\/103.254.139.202\/v2\/wp-json\/","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
		/* ]]> */
	</script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.8'></script>

	<script type='text/javascript'>
		/* <![CDATA[ */
		var thickboxL10n = {"next":"Next >","prev":"< Prev","image":"Image","of":"of","close":"Close","noiframes":"This feature requires inline frames. You have iframes disabled or your browser does not support them.","loadingAnimation":"http:\/\/103.254.139.202\/v2\/wp-includes\/js\/thickbox\/loadingAnimation.gif"};
		var thickboxL10n = {"next":"Next >","prev":"< Prev","image":"Image","of":"of","close":"Close","noiframes":"This feature requires inline frames. You have iframes disabled or your browser does not support them.","loadingAnimation":"http:\/\/103.254.139.202\/v2\/wp-includes\/js\/thickbox\/loadingAnimation.gif"};
		/* ]]> */
	</script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-includes/js/thickbox/thickbox.js?ver=3.1-20121105'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3/js/bootstrap.js?ver=1.80'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/iremit/js/custom_iremit.js?ver=0.0.1.0'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript'>
		/* <![CDATA[ */
		var iremit_ajax_script = {"ajaxurl":"http:\/\/103.254.139.202\/v2\/wp-admin\/admin-ajax.php","nextnonce":"ebe118bf5d"};
		/* ]]> */
	</script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-content/plugins/iremit/js/iremit.js?ver=0.0.1.0'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js?ver=0.0.1.0'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-includes/js/wp-embed.min.js?ver=4.8.3'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>

	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type='text/javascript' src='http://103.254.139.202/v2/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
	
	<!-- Still hard coded. Didn't have enough time to update the script in laravel -->
	<script type="text/javascript" src="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/js/css3-animate-it.js"></script>

	<script type="text/javascript">
		
		jQuery( function() {
			
			//SET OUR CALCULATOR VARIABLE AFTER PAGE LOAD
			jQuery( window ).load(function () {
			
				append_currency_values();

			} );
			
			/*
			//HEADER OFFSET
			jQuery( window ).scroll( function() {
				
				if ( jQuery( document ).scrollTop() > 50 ) {
					
					jQuery( 'nav' ).addClass( 'shrink' );

				} 
				
				else {
					
					jQuery( 'nav' ).removeClass( 'shrink' );

				}

			} );
			*/
			
			if ( ( jQuery( window ).height() + 100) < jQuery( document ).height() ) {
				
				jQuery( '#top-link-block' ).removeClass( 'hidden' ).affix( {

					offset: { top:100 }

				} );

			}
			
			//NAVIGATION HOVER
			jQuery( 'ul.nav li.dropdown' ).hover( function() {
				
				jQuery( this ).find( '.dropdown-menu' ).stop( true, true ).delay( 200 ).fadeIn( 300 );
				
			}, function() {
				
				jQuery( this ).find( '.dropdown-menu' ).stop( true, true ).delay( 200 ).fadeOut( 300 );
				
			} );
			
			//PHILIPPINE PESO CALCULATOR
			jQuery( document ).on( 'keyup', '#australian_dollar', function() {
				
				var result = parseFloat( jQuery( this ).val() ) * parseFloat( philippine_peso );

				jQuery( '#philippine_peso' ).val( result.formatMoney( 2, '.', ',' ) );

			} );
			
			//AUSTRALIAN CALCULATOR
			jQuery( document ).on( 'keyup', '#philippine_peso', function() {
				
				var result = parseFloat( jQuery( this ).val() ) / parseFloat( philippine_peso );

				jQuery( '#australian_dollar' ).val( result.formatMoney( 2, '.', ',' ) );

			} );

			//CALCULATOR LEARN MORE
			jQuery( document ).on( 'click', '.calculator_learn_more', function( e ) {
				
				e.preventDefault();
				
				window.location.href = 'http://103.254.139.202/v2/how-to-send-to-philippines-money-online';
				
			} );
			
			//CALCULATOR SEND MONEY TODAY AND BANNER SEND MONEY TODAY
			jQuery( document ).on( 'click', '.calculator_send_money_today, .banner_send_money_today', function( e ) {
				
				e.preventDefault();
				
				window.location.href = 'http://103.254.139.202/v2/steps';
				
			} );

			//BANNER CONTACT US
			jQuery( document ).on( 'click', '.banner_contact_us', function( e ) {
				
				e.preventDefault();
				
				window.location.href = 'http://103.254.139.202/v2/contact-us';
				
			} );


		} );
		
		//CURRENCY FORMATTER
		Number.prototype.formatMoney = function( c, d, t ) {
			
			var n = this, 

			c = isNaN( c = Math.abs( c ) ) ? 2 : c, 

			d = d == undefined ? "." : d, 

			t = t == undefined ? "," : t, 

			s = n < 0 ? "-" : "", 

			i = parseInt( n = Math.abs( +n || 0 ).toFixed( c ) ) + "", 

			j = ( j = i.length ) > 3 ? j % 3 : 0;

			return s + ( j ? i.substr( 0, j ) + t : "" ) + i.substr( j ).replace( /(\d{3})(?=\d)/g, "$1" + t ) + ( c ? d + Math.abs( n - i ).toFixed( c ).slice( 2 ) : "" );

		};
		
		//APPEND CURRENCY VALUE
		function append_currency_values() {
			
			//APPEND CURRENCY IN OUR CALCULATOR WIDGET
			jQuery( '#australian_dollar' ).val( australian_dollar.toFixed( 2 ) );
			
			jQuery( '#philippine_peso' ).val( philippine_peso.toFixed( 2 ) );
			
		}

	</script>
	
	<script type="text/javascript">

		jQuery( function() {
			
			//SUBMIT LOGIN FORM AND LOGIN FORM 1
			jQuery( document ).on( "submit", "#loginform, #loginform1", function( e ) {
				
				e.preventDefault();
				
				var user_login;
				
				var user_pass;
				
				var login_type = 1;
				
				//LOGIN FORM
				if( jQuery( this ).attr( "id" ) == "loginform" ) {
					
					user_login = jQuery.trim( jQuery( "#user_login" ).val() );
					
					user_pass = jQuery.trim( jQuery( "#user_pass" ).val() );

					//TRIM DOWN USER LOGIN AND USER PASSWORD
					jQuery( "#user_login" ).val( user_login ); //REMOVE WHITE SPACE
					
					jQuery( "#user_pass" ).val( user_pass ); //REMOVE WHITE SPACE
					
					login_type = 2;

				} 
				
				//LOGIN FORM 1
				else {
					
					user_login = jQuery.trim( jQuery( "#user_login1" ).val() );
					
					user_pass = jQuery.trim( jQuery( "#user_pass1" ).val() );

					//TRIM DOWN USER LOGIN AND USER PASSWORD
					jQuery( "#user_login1" ).val( user_login ); //REMOVE WHITE SPACE
					
					jQuery( "#user_pass1" ).val( user_pass ); //REMOVE WHITE SPACE
					
					login_type = 1;
					
				}
				
				//VALIDATE USER LOGIN
				if( user_login == "" ) {
					
					alert( "Please fill up your email address!" );
					
					( login_type == 2 ? jQuery( "#user_login" ).focus() : jQuery( "#user_login1" ).focus() );
					
				}
				
				//VALIDATE PASSWORD
				else if( user_pass == "" ) {
					
					alert( "Please fill up your password!" );
					
					( login_type == 2 ? jQuery( "#user_pass" ).focus() : jQuery( "#user_pass1" ).focus() );
					
				} 
				
				//CHECK SMS VERIFICATION
				else {
					
					//CONSTRUCT DATA
					var data = {
						
						action: "iremit_state_base",
						
						state: "verify_mobile_no",
						
						user_login: user_login,
						
						user_pass: user_pass
						
					};
				
					//AJAX CALL TO VERIFY MOBILE NUMBER
					jQuery.post( iremit_ajax_script.ajaxurl , data, function( response ) {
						
						//CHECK IF AJAX IS SUCCESS
						if( ! response.success ) {
							
							//EMPTY REMITTER ID
							if( response.result.remitter_id != "" ) {
								
								jQuery( "#remitter_id" ).val( response.result.remitter_id );
								
								//OPEN MODAL
								jQuery( "#mobile_verification_modal" ).modal( {
								
									backdrop: "static",
									
									keyboard: true
								
								} );
							
							}
							
							else {
								
								//SET ERROR MESSAGE
								var error_message = "";
								
								//LOOP ALL THE ERRORS
								jQuery.each( response.error_message, function( i, v ) {

									error_message += v + "\n";

								} );
								
								//SHOW SOME ERRORS
								alert( error_message );
								
							}
							
						}
						
						else {
							
							//UNBIND E
							jQuery( "#loginform, #loginform1" ).unbind( e );

							//SUBMIT LOGIN FORM
							jQuery( "#loginform, #loginform1" ).submit();
							
						}
						
					}, "json" );
				
				}
				
			} );
			
			//RESET REMITTER ID
			jQuery( document ).on( "hidden.bs.modal", "#mobile_verification_modal", function( e ) {
				
				//RESET REMITTER ID
				jQuery( "#remitter_id" ).val( "" );
				
				jQuery( "#sms_verification_code" ).val( "" );
				
			} );
			
			//SEND SMS CODE
			jQuery( document ).on( "click", "#send_sms_code_btn", function( e ) {
				
				e.preventDefault();
				
				send_sms_code_btn( jQuery( this ) );
				
			} );
			
			//VERIFY SMS CODE
			jQuery( document ).on( "click", "#verify_sms_code_btn", function( e ) {
				
				e.preventDefault();
				
				verify_sms_code_btn( jQuery( this ) );
				
			} );
			
		} );
		
		//SEND SMS CODE BTN
		function send_sms_code_btn( thisObj ) {
			
			//FETCH REMITTER ID
			var remitter_id = jQuery( "#remitter_id" ).val();
			
			//CHECK REMITTER ID IF NOT EMPTY
			if( remitter_id != "" ) {
				
				//CONTRUCT DATA
				var data = {
					
					remitter_id : remitter_id,
				
					action : "iremit_state_base",
					
					state : "send_sms_code"
				
				};
				
				//AJAX CALL
				jQuery.post( iremit_ajax_script.ajaxurl , data, function( response ) {
					
					//CHECK IF AJAX CALL IS SUCCESSFUL
					if( response.success ) { 
						
						alert( response.result );
					
					} 
					
					//AJAX NOT SUCCESSFUL
					else {
						
						//SET ERROR MESSAGE
						var error_message = "";
						
						//LOOP ALL ERROR MESSAGE
						jQuery.each( response.error_message, function( i, v ) {

							error_message += v + "\n";

						} );
						
						//SHOW SOME ERROR MESSAGE
						alert( error_message );
						
					}
					
				}, "json" );
			
			} 
			
			else {
				
				//SHOW SOME MOBILE NO ERROR MESSAGE
				alert( "Please enter your mobile no." );
				
				jQuery( "#sms_verification_code" ).focus();
				
				return false;
				
			}
			
		}

		//VERIFY SMS CODE BTN
		function verify_sms_code_btn( thisObj ) {
			
			//FETCH REMITTER ID
			var remitter_id = jQuery( "#remitter_id" ).val();
			
			//FETCH CODE
			var code = jQuery( "#sms_verification_code" ).val();
			
			//CHECK CODE IF NOT EMPTY
			if( code == "" ) {
				
				alert( "Please enter your sms verification code!" );

				jQuery( "#sms_verification_code" ).focus();
				
			} 
			
			//CHECK REMITTER ID IF NOT EMPTY
			else if( remitter_id == "" ) {
				
				alert( "Invalid remitter!" );

				jQuery( "#sms_verification_code" ).focus();
			
			}
			
			else {
				
				//CONTRCUCT DATA
				var data = {
					
					remitter_id : remitter_id,
					
					code : code,
				
					action : "iremit_state_base",
					
					state : "verify_sms_code"
				
				};
				
				//AJAX CALL
				jQuery.post( iremit_ajax_script.ajaxurl , data, function( response ) {
					
					//CHECK AJAX CALL IF SUCCESSFUL
					if( response.success ) {
						
						//SHOW SOME RESPONSE MESSAGE
						alert( response.result.message );
						
						//HIDE MODAL
						jQuery( "#mobile_verification_modal" ).modal( "hide" );
						
						//CHECK IF EMAIL IS VERIFIED
						if( response.result.email_verified ) {
							
							//SUBMIT LOGIN FORM
							jQuery( "#loginform, #loginform1" ).submit();
							
						}
					
					} 
					
					//AJAX NOT SUCCESSFULE
					else {
						
						//SET ERROR MESSAGE
						var error_message = "";
						
						//LOOP ERROR MESSAGE
						jQuery.each( response.error_message, function( i, v ) {

							error_message += v + "\n";

						} );
						
						//SHOW ERROR MESSAGE
						alert( error_message );
						
					}
				
				}, "json" );

			}
			
		}
		
	</script>

	<script type="text/javascript">

		jQuery(window).bind("load", function() {
			//reverse
			//jQuery( "#normal_remittance" ).addClass( "hide" );
		});

		jQuery( function() { 
	
			jQuery( window ).on( 'beforeunload', function() {

			//LOADING
				jQuery( "body" ).isLoading( {

					text: "Loading",

					position: "overlay",

					transparency: 0.5

				} );

			} );
	
		} );

	</script>

	<script type="text/javascript">

		var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"c24447c3da830194d96ec4988615fcd780a5fb8a8ee9db3b30e87313b3568184", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
	
	</script>

	<script> 

		$zoho.salesiq.internalready = function() { }

	</script>

	<script type="text/javascript">
	
		var dateToday = new Date(); 

		jQuery(function() {

			jQuery( ".datepicker" ).datepicker({

				dateFormat : "yy-mm-dd"

			});

			jQuery( ".calendarpicker" ).datepicker({

				dateFormat : "dd/mm/yy",

				changeMonth: true,

				changeYear: true,

				yearRange: '1925:2005',

				maxDate: '31/12/2005'

			});

			jQuery( ".expiredpicker" ).datepicker({

				dateFormat : "dd/mm/yy",

				changeMonth: true,

				changeYear: true,

				minDate: dateToday,

				yearRange: '2015:2050'

			});  

			jQuery( ".adminpicker" ).datepicker({

				dateFormat : "dd/mm/yy",

				changeMonth: true,

				changeYear: true,

				yearRange: '2015:2050'

			});

			jQuery( ".reportDate" ).datepicker({

				dateFormat : "m-dd-yy"

			});

			checkSize();

			jQuery(window).resize(checkSize);

		});
		
		function checkSize(){
			if (jQuery(window).width() <= 650){  
				jQuery( ".socialtm" ).removeClass( "col-md-2" );
			} 
			if (jQuery(window).width() <= 650){  
				jQuery( ".topmenulinks" ).removeClass( "col-md-10" );
			}   
			if (jQuery(window).width() <= 650){  
				jQuery( "#logosmall" ).removeClass( "col-md-3" );
			}  
			if (jQuery(window).width() <= 650){  
				jQuery( "#foot_mobile" ).removeClass( "col-md-8" );
				jQuery( "#foot_mobile" ).removeClass( "col-lg-8" );
				jQuery( "#foot_mobile" ).removeClass( "col-sm-8" );
				jQuery( "#foot_mobile" ).removeClass( "col-xs-12" );
			}  
			if (jQuery(window).width() <= 650){  
				jQuery( "#mobpost" ).removeClass( "col-md-4" );
				jQuery( "#mobpost" ).removeClass( "col-lg-4" );
				jQuery( "#mobpost" ).removeClass( "col-sm-4" );
				jQuery( "#mobpost" ).removeClass( "col-xs-12" );
			}         
		}
	
	</script>

	<script type="text/javascript">

		window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/103.254.139.202\/v2\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.3"}};

		!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);

	</script>

	<script type="text/javascript">
		
		var website = 'dev';

	</script>

	<script type="text/javascript">
		
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-58705914-1', 'auto');

		ga('send', 'pageview');

	</script>

	<script type="application/ld+json">

		{"@context":"http:\/\/schema.org\/","@type":"Article","mainEntityOfPage":{"@type":"WebPage","@id":"http:\/\/103.254.139.202\/v2\/steps\/"},"headline":"Send Money Now","name":"Send Money Now","datePublished":"2013-12-02","dateModified":"2017-03-07","author":{"@type":"Person","name":"","url":"http:\/\/103.254.139.202\/v2\/author\/iremitaustralia\/","description":"g00dPa$$w0rD - afmirjnt - Single - 01\/01\/1967 - 3137 Laguna Street - San Francisco - Australian Capital Territory - 555-666-0606 - Passport - 8506292de2cfb1dcedb7e796d0ea4075 -  -  - 2016-08-03 10:00:01 - 94102 - sample@email.tst -  - 11\/2011 - 1","image":{"@type":"ImageObject","url":"http:\/\/1.gravatar.com\/avatar\/d61355a692ea780acf60aac0ea985997?s=96&d=mm&r=g","height":96,"width":96}},"url":"http:\/\/103.254.139.202\/v2\/steps\/"}

	</script>  

	<script>
    	var xx_cal_xx = '&';
  	</script>

	<script type="text/javascript"> 

		var australian_dollar = 1;  var philippine_peso = 38.30; 

	</script>   

	<!--[if lt IE 9]>

	<link type="text/css" rel="stylesheet" href="http://103.254.139.202/v2/wp-content/themes/devdmbootstrap3-child/css/animations-ie-fix.css">

		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

	<![endif]-->       


	<!-- NEW CUSTOMIZED SCRIPT FOR LARAVEL IMPLEMENTATATION -->
	<script type="text/javascript">
		
		$( document )

		 	.ready( 

				function() 
				{

					init_countries( );

					init_nationalities( );

					init_civil_status( );

					init_id_type( );

					$( '#countries' )

						.change( 

							update_states 

						)

					;

					$( '#remitter_state' )

						.change(  

							update_cities

						)

					;

					$( '#city' )

						.change(  

							update_postal

						)

					;									

				}

		 	)

		;		

		function init_countries( )
		{

			var data = {

						'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

						'request_type' : 'get_countries'

					   }

			$( "body" )

				.isLoading( 

							{

								text: "Loading",

								position: "overlay",

								transparency: 0.5

							}

    			);

			$.ajax(

					{

		            	type: "GET",

		            	url: "{{ url( 'iremit/library/get-countries' ) }}",

		            	dataType: "json",

		            	data: data,

		            	success: 

		            		function ( data, textStatus, jqXHR ) 
		            		{

			            		if( data.success )
			            		{

			            			var countries = '<option value=""></option>';

									$.each( 

											data.result, 

											function( index, value )
											{

												if( value.name == 'Australia' )
												{

													countries += '<option selected id="' + value.id + '" value="' + value.id + '">' + value.name + '</option>'; 

												}

												else
												{

													countries += '<option value="' + value.id + '">' + value.name + '</option>'; 

												}

												$( '#country' )

													.empty()

													.append( countries )

												;


												var country_id = $('#country :selected').attr( 'id' );
												
												//check if a country is selected	
												if(	country_id > 0 )
												{

													//initialize states after setting the selected county 
													update_states( );

												}

												$( "body" ).isLoading('hide');

											} 

									)

			            		}

							}

						,
		            
		            	error: function (data) {
					
							var error_message = '';
					
							jQuery.each( data.error_message, function( i, v ) {
						
								error_message += v + '\n';
						
							});
					
							alert( error_message );
					
						}

		        	}

		        )

			;

		}

		function update_states( )
		{

			var country_id = $('#country :selected').attr( 'id' );

			var url = "{{ url( 'iremit/library/get-states-from-country' ) }}" + '/' + country_id;

			if( country_id != undefined )
			{

				var data = {

							'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

							'country_id' : country_id,

							'hash' : 'no hash',

							'request_type' : 'get_states'

						   }

				$( "body" )

					.isLoading( 

								{

									text: "Loading",

									position: "overlay",

									transparency: 0.5

								}

	    			)

	    		;

				$.ajax(

						{

			            	type: "GET",

			            	url: url,

			            	dataType: "json",

			            	data: data,

			            	success: 

			            		function ( data, textStatus, jqXHR ) 
			            		{

				            		if( data.success )
				            		{

				            			var states = '<option value="State">State</option>';

										$.each( 

												data.result.states, 

												function( index, value )
												{

													states += '<option id="' + value.id + '" value="' + value.id + '">' + value.name + '</option>'; 

												} 

										)

										$( '#remitter_state' )

											.empty()

											.append( states )

										;

										$( "body" ).isLoading('hide');

				            		}

				            	}	

							,
			            
			            	error: function (data) {
						
								var error_message = '';
						
								jQuery.each( data.error_message, function( i, v ) {
							
									error_message += v + '\n';
							
								});
						
								alert( error_message );
						
							}

			        	}

			        )

				;

			}	

		}

		function update_cities( )
		{

			var state_id = $('#remitter_state :selected').attr( 'id' );

			var url = "{{ url( 'iremit/library/get-cities-from-state' ) }}" + '/' + state_id;

			if( state_id != undefined )
			{

				var data = {

							'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

							'state_id' : state_id,

							'hash' : 'no hash',

							'request_type' : 'get_cities'

						   }

				$( "body" )

					.isLoading( 

								{

									text: "Loading",

									position: "overlay",

									transparency: 0.5

								}

	    			)

	    		;

				$.ajax(

						{

			            	type: "GET",

			            	url: url,

			            	dataType: "json",

			            	data: data,

			            	success: 

			            		function ( data, textStatus, jqXHR ) 
			            		{

				            		if( data.success )
				            		{

				            			var cities = '<option value="City">City</option>';

										$.each( 

												data.result.cities, 

												function( index, value )
												{

													cities += '<option id="' + value.id + '" value="' + value.id + '">' + value.name + '</option>'; 

													console.log( cities);

												} 

										)

										$( '#city' )

											.empty()

											.append( cities )

										;

										$( "body" ).isLoading('hide');

				            		}

				            	}	

							,
			            
			            	error: function (data) {
						
								var error_message = '';
						
								jQuery.each( data.error_message, function( i, v ) {
							
									error_message += v + '\n';
							
								});
						
								alert( error_message );
						
							}

			        	}

			        )

				;

			}	

		}

		function update_postal( )
		{

			var city_id = $('#city :selected').attr( 'id' );

			var url = "{{ url( 'iremit/library/get-postal-code-from-city' ) }}" + '/' + city_id;

			if( city_id != undefined )
			{

				var data = {

							'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

							'city_id' : city_id,

							'hash' : 'no hash',

							'request_type' : 'get_postal'

						   }

				$( "body" )

					.isLoading( 

								{

									text: "Loading",

									position: "overlay",

									transparency: 0.5

								}

	    			)

	    		;

				$.ajax(

						{

			            	type: "GET",

			            	url: url,

			            	dataType: "json",

			            	data: data,

			            	success: 

			            		function ( data, textStatus, jqXHR ) 
			            		{

				            		console.log( data );

				            		if( data.success )
				            		{

										$( '#zip' ).val( data.result.postal_code );

										$( "body" ).isLoading('hide');

				            		}

				            	}	

							,
			            
			            	error: function (data) {
						
								var error_message = '';
						
								jQuery.each( data.error_message, function( i, v ) {
							
									error_message += v + '\n';
							
								});
						
								alert( error_message );
						
							}

			        	}

			        )

				;

			}	

		}

		function init_nationalities( )
		{

			var url = "{{ url( 'iremit/library/get' ) }}";

			var data = {

						'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

						'hash' : 'no hash',

						'request_type' : 'get_nationalities'

					   }

			$( "body" )

				.isLoading( 

							{

								text: "Loading",

								position: "overlay",

								transparency: 0.5

							}

    			)

    		;

			$.ajax(

					{

		            	type: "GET",

		            	url: url,

		            	dataType: "json",

		            	data: data,

		            	success: 

		            		function ( data, textStatus, jqXHR ) 
		            		{

			            		if( data.success )
			            		{

			            			var nationalities = '<option value="nationality">Nationality</option>';

									$.each( 

											data.result.nationalities, 

											function( index, value )
											{

												nationalities += '<option id="' + value.id + '" value="' + value.country_id + '">' + value.name + '</option>'; 					

											} 

									)

									$( '#nationality' )

										.empty()

										.append( nationalities )

									;

									$( "body" ).isLoading('hide');

			            		}

			            	}	

						,
		            
		            	error: function (data) {
					
							var error_message = '';
					
							jQuery.each( data.error_message, function( i, v ) {
						
								error_message += v + '\n';
						
							});
					
							alert( error_message );
					
						}

		        	}

		        )

			;	

		}

		function init_nationalities( )
		{

			var url = "{{ url( 'iremit/library/get' ) }}";

			var data = {

						'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

						'hash' : 'no hash',

						'request_type' : 'get_nationalities'

					   }

			$( "body" )

				.isLoading( 

							{

								text: "Loading",

								position: "overlay",

								transparency: 0.5

							}

    			)

    		;

			$.ajax(

					{

		            	type: "GET",

		            	url: url,

		            	dataType: "json",

		            	data: data,

		            	success: 

		            		function ( data, textStatus, jqXHR ) 
		            		{

			            		if( data.success )
			            		{

			            			var nationalities = '<option value="nationality">Nationality</option>';

									$.each( 

											data.result.nationalities, 

											function( index, value )
											{

												nationalities += '<option id="' + value.id + '" value="' + value.country_id + '">' + value.name + '</option>'; 					

											} 

									)

									$( '#nationality' )

										.empty()

										.append( nationalities )

									;

									$( "body" ).isLoading('hide');

			            		}

			            	}	

						,
		            
		            	error: function (data) {
					
							var error_message = '';
					
							jQuery.each( data.error_message, function( i, v ) {
						
								error_message += v + '\n';
						
							});
					
							alert( error_message );
					
						}

		        	}

		        )

			;	

		}

		function init_civil_status( )
		{

			var url = "{{ url( 'iremit/library/get' ) }}";

			var data = {

						'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

						'hash' : 'no hash',

						'request_type' : 'get_civil_statuses'

					   }

			$( "body" )

				.isLoading( 

							{

								text: "Loading",

								position: "overlay",

								transparency: 0.5

							}

    			)

    		;

			$.ajax(

					{

		            	type: "GET",

		            	url: url,

		            	dataType: "json",

		            	data: data,

		            	success: 

		            		function ( data, textStatus, jqXHR ) 
		            		{

			            		if( data.success )
			            		{

			            			var civil_statuses = '<option value="nationality">Civil Status</option>';

									$.each( 

											data.result.civil_statuses, 

											function( index, value )
											{

												civil_statuses += '<option id="' + value.id + '" value="' + value.id + '">' + value.name + '</option>'; 					

											} 

									)

									$( '#remitter_civil_status' )

										.empty()

										.append( civil_statuses )

									;

									$( "body" ).isLoading('hide');

			            		}

			            	}	

						,
		            
		            	error: function (data) {
					
							var error_message = '';
					
							jQuery.each( data.error_message, function( i, v ) {
						
								error_message += v + '\n';
						
							});
					
							alert( error_message );
					
						}

		        	}

		        )

			;	

		}

		function init_id_type( )
		{

			var url = "{{ url( 'iremit/library/get' ) }}";

			var data = {

						'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ),

						'hash' : 'no hash',

						'request_type' : 'get_id_type'

					   }

			$( "body" )

				.isLoading( 

							{

								text: "Loading",

								position: "overlay",

								transparency: 0.5

							}

    			)

    		;

			$.ajax(

					{

		            	type: "GET",

		            	url: url,

		            	dataType: "json",

		            	data: data,

		            	success: 

		            		function ( data, textStatus, jqXHR ) 
		            		{

			            		if( data.success )
			            		{

			            			var id_type = '<option value="ID Type">ID Type</option>';

			            			console.log( data );

									$.each( 

											data.result.id_type, 

											function( index, value )
											{

												id_type += '<option id="' + value.id + '" value="' + value.id + '">' + value.name + '</option>'; 					

											} 

									)

									$( '#any_id_type' )

										.empty()

										.append( id_type )

									;

									$( "body" ).isLoading('hide');

			            		}

			            	}	

						,
		            
		            	error: function (data) {
					
							var error_message = '';
					
							jQuery.each( data.error_message, function( i, v ) {
						
								error_message += v + '\n';
						
							});
					
							alert( error_message );
					
						}

		        	}

		        )

			;	

		}

	</script>
	<!-- END OF NEW CUSTOMIZED SCRIPT FOR LARAVEL IMPLEMENTATATION -->

@endsection

