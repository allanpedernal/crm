@extends('layouts.iremit')

@section('content')
	  
<div class="page-title">
	
	<div class="title_left">
		
		<h3> Remittance Details. </h3>

	</div>

</div>

<div class="clearfix"></div>

<div class="row">
	
	<div class="col-md-12">
		
		<div class="x_panel">
			
			<div class="x_title">
				
				<ul class="nav navbar-right panel_toolbox">
					
					<li> <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a> </li>

					<li class="dropdown">
						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

						<ul class="dropdown-menu" role="menu">
							
							<li> <a href="#"> Settings 1 </a> </li>

							<li> <a href="#"> Settings 2 </a> </li>

						</ul>

					</li>

					<li> <a class="close-link"> <i class="fa fa-close"></i> </a> </li>

				</ul>

				<div class="clearfix"></div>

			</div>

			<div class="x_content">

				<section class="content invoice">
					
					<div class="row">
						
						<div class="col-xs-12 invoice-header">
							
							<h1>
								
								<i class="fa fa-book fa-fw"></i> 39404-105193

								<small class="pull-right"> <i class="fa fa-calendar"></i> Date: 01/26/17 </small>

							</h1>
							
						</div>

					</div>
						
					<div class="row invoice-info">
						
						<div class="col-sm-4 invoice-col">
							
							From

							<address>
								
								<strong> Rosario Mallari Celon </strong>

								<br> 7/109 Hubert Street East Victoria Park 

								<br> Australia, Western Australia 6101

								<br> Phone: 0414060046

								<br> Email: rhiocelon@yahoo.com
								
								<br> ID: PRC, 162262, 01/27/17

							</address>

						</div>

						<div class="col-sm-4 invoice-col">
							
							To

							<address>
								
								<strong> Rosario Concepcion Mallari </strong>

								<br> 85 Gandus 

								<br> Philippines, Pampanga Mexico 2021

								<br> Phone: 639267402048

								<br> Relationship: Parent

							</address>

						</div>

						<div class="col-sm-4 invoice-col">
							
							<b> Transaction #: 39404-105193 </b>

							<br>

							<br>

							<b> Status: </b> Transfer Complete

							<br>

							<b> Method: </b> LBC

							<br>

							<b> Bank: </b> POLI

						</div>

					</div>

					<div class="row">
						
						<div class="col-xs-12 table">
							
							<h3> Added Note </h3>
							
							<table class="table table-striped table-bordered">
								
								<thead>
									
									<tr>
										
										<th> CSR </th>

										<th> Note </th>

										<th> Date Time </th>

									</tr>

								</thead>

								<tbody>
									
									<tr>
										
										<td> Allan Joseph Pedernal </td>

										<td> This is sample note for remittance. </td>

										<td> 01/27/17 </td>

									</tr>

									<tr>
										
										<td> Allan Joseph Pedernal </td>

										<td> This is sample note for remittance. </td>

										<td> 01/27/17 </td>

									</tr>

									<tr>
										
										<td> Allan Joseph Pedernal </td>

										<td> This is sample note for remittance. </td>

										<td> 01/27/17 </td>

									</tr>

									<tr>
										
										<td> Allan Joseph Pedernal </td>

										<td> This is sample note for remittance. </td>

										<td> 01/27/17 </td>

									</tr>

								</tbody>

							</table>

						</div>
						
						<div class="col-xs-12 table">
							
							<h3> Histories </h3>
							
							<table class="table table-striped table-bordered">
								
								<thead>
									
									<tr>
										
										<th> CSR </th>

										<th> Activity </th>

										<th> IP Address </th>
										
										<th> Date Time </th>

									</tr>

								</thead>

								<tbody>
									
									<tr>
										
										<td> Allan Joseph Pedernal </td>

										<td> This is sample history for remittance. </td>
										
										<td> 49.147.164.126 </td>

										<td> 01/27/17 </td>

									</tr>

									<tr>
										
										<td> Allan Joseph Pedernal </td>

										<td> This is sample history for remittance. </td>
										
										<td> 49.147.164.126 </td>

										<td> 01/27/17 </td>

									</tr>

									<tr>
										
										<td> Allan Joseph Pedernal </td>

										<td> This is sample history for remittance. </td>
										
										<td> 49.147.164.126 </td>

										<td> 01/27/17 </td>

									</tr>

									<tr>
										
										<td> Allan Joseph Pedernal </td>

										<td> This is sample history for remittance. </td>
										
										<td> 49.147.164.126 </td>

										<td> 01/27/17 </td>

									</tr>

								</tbody>

							</table>

						</div>

					</div>

					<div class="row">
						
						<div class="col-xs-6">
							
							<p class="lead"> Payment Methods: </p>

							<img src="/img/visa.png" alt="Visa">

							<img src="/img/mastercard.png" alt="Mastercard">

							<img src="/img/american-express.png" alt="American Express">

							<img src="/img/paypal.png" alt="Paypal">

							<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
								
								Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.

							</p>

						</div>

						<div class="col-xs-6">
							
							<p class="lead"> <i class="fa fa-calendar"></i> Transfer Date: 01/27/17 </p>

							<div class="table-responsive">
								
								<table class="table">
									
									<tbody>
										
										<tr>
											
											<th style="width:50%"> AUD Sent: </th>

											<td> $ 250.00 </td>

										</tr>

										<tr>
											
											<th> Service Fee </th>

											<td> $ 9.00 </td>

										</tr>

										<tr>
											
											<th> Forex Rate: </th>

											<td> P 37.00 </td>

										</tr>

										<tr>
											
											<th> Total: </th>

											<td> P 9,583.00 </td>

										</tr>

									</tbody>

								</table>

							</div>

						</div>

					</div>

					<div class="row no-print">
						
						<div class="col-xs-12">
							
							<button class="btn btn-default" onclick="window.print();"> <i class="fa fa-print"></i> Print </button>

							<button class="btn btn-success pull-right"> <i class="fa fa-credit-card"></i> Submit Payment </button>

							<button class="btn btn-primary pull-right" style="margin-right: 5px;"> <i class="fa fa-download"></i> Generate PDF </button>

						</div>

					</div>

				</section>

			</div>

		</div>

	</div>

</div>

@endsection

@section('styles')

@endsection

@section('scripts')

@endsection
