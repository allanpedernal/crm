@extends('layouts.iremit')

@section('content')

	<div class="page-title">
		
		<div class="title_left">
			
			<h3> <i class="fa fa-book fa-fw"></i> Remittances </h3>
			
		</div>
		
	</div>

	<div class="clearfix"></div>

	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
			<div class="x_panel">
				
				<div class="x_title">
					
					<a href="{{ url('/iremit/remittances/modal') }}" class="btn btn-primary btn-xs show_modal_btn" style="margin: 5px 0px;"> <i class="fa fa-plus"></i> Add New Remittance </a>
					
					<ul class="nav navbar-right panel_toolbox">
						
						<li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
						
						<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> </li>
						
						<li> <a class="close-link"> <i class="fa fa-close"></i> </a> </li>
					
					</ul>
					
					<div class="clearfix"></div>
					
				</div>
				
				<div class="x_content">
					
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 5px;">
						
						<div class="panel panel-default">
							
							<div class="panel-heading" role="tab" id="search_remittance">
								
								<h4 class="panel-title">
									
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="<?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'true' : 'false' ); ?>" aria-controls="collapseOne" <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? '' : 'class="collapsed"' ); ?>>

										<small> Click me to show search </small>

									</a>

								</h4>

							</div>

							<div id="collapseOne" class="panel-collapse collapse <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'in' : '' ); ?>" role="tabpanel" aria-labelledby="search_remittance">

								<div class="panel-body">
									
									<form method="GET" id="search_remittance_form" class="form-horizontal">

										<div class="row">
											
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="search_remittance_transaction_no"> Transaction no.: </label>
													
													<input type="text" id="search_remittance_transaction_no" name="search_remittance_transaction_no" class="form-control" value="<?php echo ( isset( $_GET[ 'search_remittance_transaction_no' ] ) && ! empty( $_GET[ 'search_remittance_transaction_no' ] ) ? $_GET[ 'search_remittance_transaction_no' ] : '' ); ?>"> 
												
												</div>
											
											</div>

											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="search_remittance_remitter"> Remitter: </label>
												
													<select id="search_remittance_remitter" name="search_remittance_remitter" class="form-control chosen-select" data-placeholder="Select remitter">
													
														<option value=""></option>
														
														@if( isset( $remitter ) AND ! empty( $remitter ) )
														
															<option value="{{ $remitter[ 'id' ] }}" selected="selected"> {{ $remitter[ 'fullname' ] }} {{ ( isset( $remitter[ 'email' ] ) && ! empty( $remitter[ 'email' ] ) ? '|' . $remitter[ 'email' ] : '' ) }} </option>
														
														@endif
													
													</select>
												
												</div>
											
											</div>

											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="search_remittance_recipient"> Recipient: </label>
													
													<select id="search_remittance_recipient" name="search_remittance_recipient" class="form-control chosen-select" data-placeholder="Select recipient">
													
														<option value=""></option>
														
														@if( isset( $recipient ) AND ! empty( $recipient ) )
														
															<option value="{{ $recipient[ 'id' ] }}" selected="selected"> {{ $recipient[ 'fullname' ] }} {{ ( isset( $recipient[ 'email' ] ) && ! empty( $recipient[ 'email' ] ) ? '|' . $recipient[ 'email' ] : '' ) }} </option>
														
														@endif
													
													</select>
													
												</div>
											
											</div>

											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
												
												<div class="form-group">
													
													<label for="search_remittance_recipient_receive_option"> Receipient receive option: </label>
													
													<select id="search_remittance_recipient_receive_option" name="search_remittance_recipient_receive_option" class="form-control chosen-select" data-placeholder="Select recipient receive option">
													
														<option value=""></option>
														
														@if( isset( $receive_option_list ) AND ! empty( $receive_option_list ) )
														
															@foreach( $receive_option_list AS $receive_option )
															
																<option value="{{ $receive_option[ 'id' ] }}" @if( isset( $_GET[ 'search_remittance_recipient_receive_option' ] ) AND ! empty( $_GET[ 'search_remittance_recipient_receive_option' ] ) AND $_GET[ 'search_remittance_recipient_receive_option' ] == $receive_option[ 'id' ] ) selected="selected" @endif> {{ $receive_option[ 'name' ] }} </option>
															
															@endforeach
														
														@endif
													
													</select>
													
												</div>

											</div>

										</div>
										
										<div class="row">
										
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
												
												<div class="form-group">
												
													<label for="search_remittance_status"> Remittance status: </label>
													
													<select id="search_remittance_status" name="search_remittance_status" class="form-control chosen-select" data-placeholder="Select remittance status">
													
														<option value=""></option>
														
														@if( isset( $remittance_statuses ) AND ! empty( $remittance_statuses ) )
														
															@foreach( $remittance_statuses AS $remittance_status )
															
																<option value="{{ $remittance_status[ 'id' ] }}" @if( isset( $_GET[ 'search_remittance_status' ] ) AND ! empty( $_GET[ 'search_remittance_status' ] ) AND $_GET[ 'search_remittance_status' ] == $remittance_status[ 'id' ] ) selected="selected" @endif> {{ $remittance_status[ 'name' ] }} </option>
															
															@endforeach
															
														@endif
													
													</select>

												</div>

											</div>
											
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
												
												<div class="form-group">
												
													<label for="search_remittance_bank_source"> Bank source: </label>
													
													<select id="search_remittance_bank_source" name="search_remittance_bank_source" class="form-control chosen-select" data-placeholder="Select bank source">
													
														<option value=""></option>
														
														@if( isset( $bank_sources ) AND ! empty( $bank_sources ) )
														
															@foreach( $bank_sources AS $bank_source )
															
																<option value="{{ $bank_source[ 'id' ] }}" @if( isset( $_GET[ 'search_remittance_bank_source' ] ) AND ! empty( $_GET[ 'search_remittance_bank_source' ] ) AND $_GET[ 'search_remittance_bank_source' ] == $bank_source[ 'id' ] ) selected="selected" @endif> {{ $bank_source[ 'shortname' ] }} </option>
															
															@endforeach
															
														@endif
													
													</select>

												</div>

											</div>
											
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												
												<div class="row">
													
													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
													
														<div class="form-group">
														
															<label for="search_remittance_transaction_start_date"> Transaction start date: </label>
															
															<div class="input-prepend input-group" style="margin:0px;">

																<span class="add-on input-group-addon"> <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> </span>

																<input type="text" class="form-control datepicker" id="search_remittance_transaction_start_date" name="search_remittance_transaction_start_date" placeholder="Start date" value="<?php echo ( isset( $_GET[ 'search_remittance_transaction_start_date' ] ) && ! empty( $_GET[ 'search_remittance_transaction_start_date' ] ) ? $_GET[ 'search_remittance_transaction_start_date' ] : '' ); ?>">

															</div>
															
														</div>
													
													</div>
													
													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
													
														<div class="form-group">
														
															<label for="search_remittance_transaction_end_date"> Transaction end date: </label>
															
															<div class="input-prepend input-group" style="margin:0px;">

																<span class="add-on input-group-addon"> <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> </span>

																<input type="text" class="form-control datepicker" id="search_remittance_transaction_end_date" name="search_remittance_transaction_end_date" placeholder="End date" value="<?php echo ( isset( $_GET[ 'search_remittance_transaction_end_date' ] ) && ! empty( $_GET[ 'search_remittance_transaction_end_date' ] ) ? $_GET[ 'search_remittance_transaction_end_date' ] : '' ); ?>">

															</div>
															
														</div>
													
													</div>
												
												</div>

											</div>
										
										</div>

										<div class="row">

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

												<a href="{{ url( 'iremit/remittances' ) }}" class="btn btn-default btn-sm"> <i class="fa fa-refresh" aria-hidden="true"></i> Reset </a>

												<button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> Search </button>

											</div>

										</div>

									</form>
									
								</div>

							</div>

						</div>

					</div>

					@include( 'tables.remittance_table' )

				</div>
			
			</div>
			
		</div>
		
	</div>

@endsection

@section('styles')

	<link href="{{ URL::asset('bower_components/gentelella/vendors/dropzone/dist/min/dropzone.min.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('bower_components/gentelella/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('css/chosen.min.css') }}" rel="stylesheet">
    
	<style type="text/css">
	
		/*IS LOADING PLUGIN*/
		.isloading-wrapper.isloading-right{ margin-left:10px; }

		.isloading-overlay{ position:relative;text-align:center; }

		.isloading-overlay .isloading-wrapper {

			line-height: 20px; 
			
			background:#ffffff; 
			
			-webkit-border-radius:7px; 
			
			-webkit-background-clip:padding-box; 
			
			-moz-border-radius:7px; 
			
			-moz-background-clip:padding;
			
			border-radius:7px;
			
			background-clip:padding-box;
			
			display:inline-block;
			
			margin:0 auto;
			
			padding:10px 20px;
			
			z-index:9000;

		}
		
		.pagination{ margin: 10px 0px 5px; }
		
		.help-block { color: #E74C3C; }
		
		.scrollbar-light { height: 215px; overflow-y: scroll; }
		
		.input-group-addon{ padding: 3px; }
		
		.input-group-bulk-action .chosen-container-single .chosen-single{ height: 30px; border-radius: 0px; line-height: 30px; }
	
	</style>

@endsection

@section('scripts')

	<script src="{{ URL::asset('bower_components/gentelella/vendors/dropzone/dist/min/dropzone.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
	
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/iCheck/icheck.min.js') }}"></script>
    
	<script src="{{ URL::asset('js/jquery.isloading.min.js') }}"></script>

	<script src="{{ URL::asset('js/handlebars-v4.0.5.js') }}"></script>
	
	<script src="{{ URL::asset('js/chosen.jquery.min.js') }}"></script>
	
	<script src="{{ URL::asset('js/chosen.ajaxaddition.jquery.js') }}"></script>
	
	<script src="{{ URL::asset('js/jquery.number.js') }}"></script>
	
	<script type="text/javascript">
		
		jQuery( document ).ready( function() {
			
			
			//INIT ALL
			init_all();
			
			
			//SHOW MODAL
			jQuery( document ).on( 'click', '.show_modal_btn', function( e ) {
				
				e.preventDefault();
				
				var href = jQuery( this ).attr( 'href' );
				
				//SHOW LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Loading',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );
				
				//CALL REMITTANCE AJAX
				jQuery.get( href, function( response ) {
					
					if( response.success ) {
						
						//GET ACTION
						var action = response.result.action;
						
						//APPEND AJAX MODAL
						jQuery( 'body' ).append( response.result.content );
						
						//OPEN MODAL
						jQuery( '#remittance_modal' ).modal( { backdrop: 'static', keyboard: false } );
						
						//REINITIALIZE ALL
						init_all();

					}
					
					else {
						
						//SHOW SOME ERROR MESSAGE
						alert( response.error_message );
						
					}
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
				}, 'json' );

			} );
			
			//REMOVE REMITTANCE MODAL
			jQuery( document ).on( 'hidden.bs.modal', '#remittance_modal', function( e ) {
				
				jQuery( this ).remove();

				jQuery( '.modal-backdrop' ).remove();

				jQuery( 'body' ).removeClass( 'modal-open' );
				
			} );
			
			
			//SHOW OTHER REASON
			jQuery( document ).on( 'change', '#remittance_reason_id', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				//OTHERS
				if( current_value == 1 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#other_reason_container' ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#other_reason_container' ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#remittance_other_reason' ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#other_reason_container' ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#other_reason_container' ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#remittance_other_reason' ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#remittance_other_reason' ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#remittance_other_reason' ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );
			
			//SHOW OTHER NATURE OF WORK
			jQuery( document ).on( 'change', '#remittance_nature_of_work_id', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				//OTHERS
				if( current_value == 1 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#other_nature_of_work_container' ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#other_nature_of_work_container' ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#remittance_other_nature_of_work' ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#other_nature_of_work_container' ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#other_nature_of_work_container' ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#remittance_other_nature_of_work' ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#remittance_other_nature_of_work' ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#remittance_other_nature_of_work' ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );
			
			//SHOW OTHER SOURCE OF FUND
			jQuery( document ).on( 'change', '#remittance_source_of_fund_id', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				//OTHERS
				if( current_value == 1 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#other_source_of_fund_container' ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#other_source_of_fund_container' ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#remittance_other_source_of_fund' ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#other_source_of_fund_container' ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#other_source_of_fund_container' ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#remittance_other_source_of_fund' ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#remittance_other_source_of_fund' ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#remittance_other_source_of_fund' ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );
			
			
			//CHECK IF INPUT IS NUMBERS ONLY
			jQuery( document ).on( 'keyup', '#remittance_amount_sent, #remittance_forex_rate, #remittance_service_fee, #remittance_trade_rate', function( e ) {
				
				var id = jQuery( this ).attr( 'id' );
				
				//VALIDATE
				if( ! jQuery.isNumeric( jQuery( '#' + id ).val() ) && ( jQuery( '#' + id ).val() != '' ) ) {
					
					alert( 'Please enter numbers only!' );
					
					jQuery( '#' + id ).val( parseFloat( jQuery( '#' + id ).val().replace( /[^\d.]/g, '' ) ) );
					
					jQuery( '#' + id ).focus();
					
					return false;
					
				}
				
				compute_total_php_deposit();
				
			} );
			
			
			//CHECK IF AMOUNT SENT IS GREATHER THAN 10,000
			jQuery( document ).on( 'keyup', '#remittance_amount_sent', function( e ) {
				
				var current_amount = parseFloat( jQuery( this ).val() );
				
				if( current_amount >= 10000 ) {
					
					if( jQuery( '#additional_requirement_nav' ).is( ':hidden' ) ) {
						
						//SHOW ADDITIONAL REQUIREMENT NAV
						jQuery( '#additional_requirement_nav' ).hide().fadeIn();
						
						jQuery( '#remittance_source_of_income' ).attr( 'required', 'required' );
						
						jQuery( '#remittance_additional_requirement_hidden' ).attr( 'required', 'required' );
						
					}
					
				} 
				
				else {
					
					if( jQuery( '#additional_requirement_nav' ).is( ':visible' ) ) {
						
						//HIDE ADDITIONAL REQUIREMENT NAV
						jQuery( '#additional_requirement_nav' ).show().fadeOut();
						
						//GET REMITTANCE SOURCE OF INCOME ID PARSELEY ID
						var remittance_source_of_income_id_parseley_id = jQuery( '#remittance_source_of_income' ).data( 'parsley-id' );
						
						//REMOVE REQUIRED ATTRIBUTE
						jQuery( '#remittance_source_of_income' ).removeAttr( 'required' );
						
						//REMOVE PARSELEY ERROR CLASS
						jQuery( '#remittance_source_of_income' ).removeClass( 'parsley-error' );
						
						//REMOVE PARSELEY ERROR LIST
						jQuery( '#parsley-id-' + remittance_source_of_income_id_parseley_id ).remove();
						
						
						//GET REMITTER SOURCE OF FUND ID PARSELEY ID
						var remittance_additional_requirement_hidden_parseley_id = jQuery( '#remittance_additional_requirement_hidden' ).data( 'parsley-id' );
						
						//REMOVE REQUIRED ATTRIBUTE
						jQuery( '#remittance_additional_requirement_hidden' ).removeAttr( 'required' );
						
						//REMOVE PARSELEY ERROR CLASS
						jQuery( '#remittance_additional_requirement_hidden' ).removeClass( 'parsley-error' );
						
						//REMOVE PARSELEY ERROR LIST
						jQuery( '#parsley-id-' + remittance_additional_requirement_hidden_parseley_id ).remove();
						
						
					}
					
				}
				
			} );
			
			
			//GET RECIPIENTS ON REMITTER CHANGE
			jQuery( document ).on( 'change', '#remittance_remitter_id', function( e ) {
				
				var remitter_id = jQuery( this ).val();
				
				//CHECK IF REMITTER ID IS NOT EMPTY
				if( remitter_id == '' ) {
					
					//RESET RECIPIENT LIST
					jQuery( '#remittance_recipient_id' ).empty().append( '<option value=""></option>' ).trigger( 'chosen:updated' );
					
					//RESET REMITTER FULL DETAILS
					jQuery( '.remitter_full_details_container' ).html( '<div class="alert alert-danger" role="alert" style="margin:0px;"><h6 class="text-center" style="margin:5px 0px;">Please select remitter!</h6></div>' );
					
					//RESET RECIPIENT FULL DETAILS
					jQuery( '.recipient_full_details_container' ).html( '<div class="alert alert-danger" role="alert" style="margin:0px;"><h6 class="text-center" style="margin:5px 0px;"> Please select remitter! </h6></div>' );

				}
				
				//REMITTER ID IS NOT EMPTY
				else {
					
					//GET REMITTER RECIPIENTS
					get_remitter_recipients( remitter_id );
					
					//GET REMITTER FULL DETAILS
					get_remitter_full_details( remitter_id );
					
				}
				
			} );
			
			
			//GET RECIPIENTS ON REMITTER CHANGE
			jQuery( document ).on( 'change', '#remittance_recipient_id', function( e ) { 
				
				var recipient_id = jQuery( this ).val();
				
				//CHECK IF RECIPIENT ID IS NOT EMPTY
				if( recipient_id == '' ) {
					
					//RESET RECIPIENT FULL DETAILS CONTAINER
					jQuery( '.recipient_full_details_container' ).html( '<div class="alert alert-danger" role="alert" style="margin:0px;"><h6 class="text-center" style="margin:5px 0px;"> Please select remitter! </h6></div>' );

				}
				
				//RECIPIENT ID IS NOT EMPTY
				else {
					
					//GET RECIPIENT FULL DETAILS
					get_recipient_full_details( recipient_id );
					
				}
				
			} );
			
			
			//CHECK ALL REMITTANCE CHECKBOX
			jQuery( document ).on( 'ifChanged', '#select_all_remittances', function() {
					
				//CHECK IF REMITTANCE SELECT ALL IS NOT CHECKED
				if ( ! jQuery( this ).is( ':checked' ) ) {
					
					//REMOVE ALL CHECKBOX CHECKED ATTR
					jQuery( '.remittance_checkbox' ).prop( 'checked', false ).iCheck( 'update' );
					
					//REMOVE CLASS SELECTED TO ALL TR
					jQuery( '.jambo_table tbody tr' ).removeClass( 'selected' );

				} 

				else {
					
					//ADD ALL CHECKBOX CHECKED ATTR
					jQuery( '.remittance_checkbox' ).prop( 'checked', true ).iCheck( 'update' );
					
					//ADD CLASS SELECTED TO ALL TR
					jQuery( '.jambo_table tbody tr' ).addClass( 'selected' );
				
				} 
				  
			} );
			
			//CHECK REMITTANCE CHECKBOX
			jQuery( document ).on( 'ifChanged', '.remittance_checkbox', function() {
				
				var all = jQuery( '.remittance_checkbox' );
				
				//CHECK IF ALL REMITTANCE CHECKBOX IS CHECKED
				if ( all.length === all.filter( ':checked' ).length ) {
					
					//CHECK SELECT ALL CHECKBOX
					jQuery( '#select_all_remittances' ).prop( 'checked', true ).iCheck( 'update' );

				}

				else {
					
					//REMOVE CHECK SELECT ALL CHECKBOX
					jQuery( '#select_all_remittances' ).prop( 'checked', false ).iCheck( 'update' );

				}

			} );
			
			
			//SUBMIT REMITTANCE FORM
			jQuery( document ).on( 'submit', '#remittance_form', function( e ) {
				
				e.preventDefault();
				
				//CONFIRM ANSWER
				var answer = confirm( 'Do you want to submit this remittance form?' );
				
				//IF YES
				if( answer ) {
					
					//SHOW LOADING
					jQuery( 'body' ).isLoading( {
						
						text: 'Submitting form',
						
						class: 'fa fa fa-spinner fa-spin',
						
						position: 'overlay',
						
						transparency: 0.5
						
					} );
					
					//GET DATA
					var data = jQuery( this ).serializeArray();
					
					data.push( { name: '_token', value: '{{ csrf_token() }}' } );
					
					//AJAX FORM DATA
					jQuery.post( '{{ url( "/iremit/remittances/form/post" ) }}', data, function( response ) { 
					
						//FORM SUBMIT SUCCESSFUL
						if( response.success ) {

							//GET REMITTANCE ID
							var remittance_id = response.result.remittance_id;

							//ADD NEW RECORD
							if( response.result.action == 'add' ) {
								
								new PNotify( {
									
									title: 'Success!',

									text: 'Remittance has been added!',

									type: 'success',
									
									styling: 'bootstrap3'

								} );

							}

							//EDIT RECORD
							if( response.result.action == 'edit' ) {
								
								new PNotify( {
									
									title: 'Success!',

									text: 'Remittance has been updated!',
									
									styling: 'bootstrap3'

								} );

							}
							
							//UPDATE REMITTANCE TABLE
							jQuery( '.remittance_table_container' ).html( response.result.remittance_table_template );

							//HIDE MODAL
							jQuery( '#remittance_modal' ).modal( 'hide' );
							
							//INIT ALL
							init_all();

						}

						else {

							//SHOW SOME ERROR MESSAGE
							alert( response.error_message );

						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
					}, 'json' );
				
				}
				
				
			} );

			//DELETE REMITTANCES
			jQuery( document ).on( 'submit', '#remittance_table', function( e ) {
				
				e.preventDefault();
				
				var total_checked_remittances = jQuery( 'input[name="remittance[]"]:checked' ).length;
				
				if( total_checked_remittances ) {
					
					var answer_text = 'Do you want to delete this remittance' + ( total_checked_remittances > 1 ? 's' : '' );
					
					var answer = confirm( answer_text + '?' );
					
					if( answer ) {
						
						var data = jQuery( this ).serializeArray();
						
						data.push( { name: '_token', value: '{{ csrf_token() }}' } );
						
						jQuery.post( '{{ url( "/iremit/remittances/delete-remittances" ) }}', data, function( response ) {
							
							//FORM SUBMIT SUCCESSFUL
							if( response.success ) {
								
								if( response.result.action == 'delete' ) {
									
									var pnotify_text = 'Remittance' + ( response.result.total_remittance > 1 ? 's' : '' ) + ' has been deleted!';
									
									new PNotify( {
										
										title: 'Success!',

										text: pnotify_text + '!',

										type: 'error',
										
										styling: 'bootstrap3'

									} );

								}

								//UPDATE REMITTANCE TABLE
								jQuery( '.remittance_table_container' ).html( response.result.remittance_table_template );

								//HIDE MODAL
								jQuery( '#remittance_modal' ).modal( 'hide' );
								
								//INIT ALL
								init_all();
								
							}

							else {

								//SHOW SOME ERROR MESSAGE
								alert( response.error_message );

							}
							
						} );
						
					}
				
				}
				
				else {
					
					alert( 'Please select remittance that you want to delete!' );
					
				}
				
			} );
			
			
			//CHANGE REMITTANCE BULK ACTION
			jQuery( document ).on( 'change', '.remittance_bulk_action', function( e ) {
				
				var current_value = jQuery( this ).val();
				
				jQuery( '.remittance_bulk_action' ).val( current_value ).trigger( 'chosen:updated' );
				
			} );
			
			
			//BULK ACTION
			jQuery( document ).on( 'click', '.bulk_action_btn', function( e ) {
				
				e.preventDefault();
				
				var action = jQuery( '.remittance_bulk_action' ).val();
				
				//CHECK IF ACTION IS NOT EMPTY
				if( action != '' ) {
					
					//CHECK IF REMITTANCES IS SELECTED
					var total_checked_remittances = jQuery( 'input[name="remittance[]"]:checked' ).length;

					if( total_checked_remittances ) {
						
						var data = jQuery( '#remittance_table' ).serializeArray();
						
						var content = jQuery( '.remittance_bulk_content' ).val();
						
						data.push( { name: '_token', value: '{{ csrf_token() }}' } );
						
						data.push( { name: 'action', value: action } );
						
						data.push( { name: 'content', value: content } );
						
						jQuery.post( '{{ url( "/iremit/remittances/bulk-action" ) }}', data, function( response ) {
							
							//FORM SUBMIT SUCCESSFUL
							if( response.success ) {
								
								var pnotify_text = '';
								
								//NOTE
								if( response.result.action == 'note' ) {
									
									pnotify_text = 'Remittance' + ( response.result.total_remittance > 1 ? 's' : '' ) + ' note has been added!';

								}
								
								//FOREX RATE
								if( response.result.action == 'forex_rate' ) {
									
									pnotify_text = 'Remittance' + ( response.result.total_remittance > 1 ? 's' : '' ) + ' forex rate has been updated!';
									
								}
								
								//TRADE RATE
								if( response.result.action == 'trade_rate' ) {
									
									pnotify_text = 'Remittance' + ( response.result.total_remittance > 1 ? 's' : '' ) + ' trade rate has been updated!';
									
								}
								
								new PNotify( {
									
									title: 'Success!',

									text: pnotify_text + '!',

									type: 'error',
									
									styling: 'bootstrap3'

								} );

								//UPDATE REMITTANCE TABLE
								jQuery( '.remittance_table_container' ).html( response.result.remittance_table_template );

								//HIDE MODAL
								jQuery( '#remittance_bulk_action_modal' ).modal( 'hide' );

							}
							
							else {
								
								//SHOW SOME ERROR MESSAGE
								alert( response.error_message );
								
							}
							
						} );

					}

					else {

						alert( 'Please select remittances that you want to bulk action!' );

					}

				}

				else {

					alert( 'Please select bulk action!' );

				}

			} );
			
		} );
			
			
		//INIT ALL
		function init_all() {
			
			
			//INIT DATEPICKER
			init_datepicker();
			
			//INIT CHOSEN
			init_chosen();
			
			//INIT PARSELEY
			init_parseley();
			
			//REINITIALIZE ICHECK EVENT
			reinitialize_icheck_event();
			
			//INIT SEARCH REMITTANCE REMITTER
			init_search_remittance_remitter();
			
			//INIT SEARCH REMITTANCE RECIPIENT
			init_search_remittance_recipient();
			
			
			//INIT REMITTANCE REMITTER
			init_remittance_remitter();
			
			
			//INIT REMITTANCE RECEIPT DROPZONE
			init_remittance_receipt_dropzone();
			
			//INIT REMITTANCE ADDITIONAL REQUIREMENT DROPZONE
			init_remittance_additional_requirement_dropzone();
			
			
		}
		
		//INIT DATEPICKER
		function init_datepicker() {
			
			//INITIALIZE DATEPICKER
			jQuery( '.datepicker' ).daterangepicker( {
				
				singleDatePicker: true,
				
				showDropdowns: true,
				
				format: 'YYYY-MM-DD'
				
			} );
		
		}
		
		//INIT CHOSEN
		function init_chosen() {
			
			var config = {
				
				'.chosen-select'           : { width: '100%', allow_single_deselect: true },
				
				'.chosen-select-deselect'  : { allow_single_deselect: true },
				
				'.chosen-select-no-single' : { disable_search_threshold: 10 },
				
				'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
				
				'.chosen-select-rtl'       : { rtl: true },
				
				'.chosen-select-width'     : { width: '100%' }
				
			}

			for ( var selector in config ) {
				
				jQuery( selector ).chosen( config[ selector ] );

			}
			
		}


		//INIT SEARCH REMITTANCE REMITTER
		function init_search_remittance_remitter() {
			
			jQuery( '#search_remittance_remitter' ).ajaxChosen( {
				
				dataType: 'json',
				
				type: 'POST',
				
				url:"{{ url( 'iremit/remittances/get-remitter/' ) }}",
				
				data: { 'remitter' : jQuery( '#search_remittance_remitter' ).val(), '_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ) },
				
				success: function( data, textStatus, jqXHR ) {
					
					return data;
					
				}
				
			}, {
				
				processItems: function( data ) { 					
					
					var remitters = {};
					
					jQuery.each( data.results, function ( i, val ) {
						
						remitters[ val.id ] = val.text;
						
					} );
					
					return remitters; 
					
				},
				
				useAjax: function( e ) { return true; },
				
				generateUrl: function( q ) { return "{{ url( 'iremit/remittances/get-remitter' ) }}"; },
				
				loadingImg: "{{ URL::asset( 'img/loading.gif' ) }}",
				
				minLength: 2
				
			} );
			
		}

		//INIT SEARCH REMITTANCE RECIPIENT
		function init_search_remittance_recipient() {
			
			jQuery( '#search_remittance_recipient' ).ajaxChosen( {
				
				dataType: 'json',
				
				type: 'POST',
				
				url:"{{ url( 'iremit/remittances/get-recipient/' ) }}",
				
				data: { 'recipient' : jQuery( '#search_remittance_recipient' ).val(), '_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ) },
				
				success: function( data, textStatus, jqXHR ) {
					
					return data;
					
				}
				
			}, {
				
				processItems: function( data ) { 					
					
					var recipients = {};
					
					jQuery.each( data.results, function ( i, val ) {
						
						recipients[ val.id ] = val.text;
						
					} );
					
					return recipients; 
					
				},
				
				useAjax: function( e ) { return true; },
				
				generateUrl: function( q ) { return "{{ url( 'iremit/remittances/get-recipient' ) }}"; },
				
				loadingImg: "{{ URL::asset( 'img/loading.gif' ) }}",
				
				minLength: 2
				
			} );
			
		}
		
		
		
		//INIT REMITTANCE REMITTER
		function init_remittance_remitter() {
			
			jQuery( '#remittance_remitter_id' ).ajaxChosen( {
				
				dataType: 'json',
				
				type: 'POST',
				
				url:"{{ url( 'iremit/remittances/get-remitter/' ) }}",
				
				data: { 'remitter' : jQuery( '#remittance_remitter_id' ).val(), '_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' ) },
				
				success: function( data, textStatus, jqXHR ) {
					
					return data;
					
				}
				
			}, {
				
				processItems: function( data ) { 					
					
					var remitters = {};
					
					jQuery.each( data.results, function ( i, val ) {
						
						remitters[ val.id ] = val.text;
						
					} );
					
					return remitters; 
					
				},
				
				useAjax: function( e ) { return true; },
				
				generateUrl: function( q ) { return "{{ url( 'iremit/remittances/get-remitter' ) }}"; },
				
				loadingImg: "{{ URL::asset( 'img/loading.gif' ) }}",
				
				minLength: 2
				
			} );
			
		}
			
			
		/*
		* RECEIPT AND ADDITIONAL REQUIREMENT
		*/
		
		//INIT REMITTANCE RECEIPT DROPZONE
		function init_remittance_receipt_dropzone() {
			
			var token = "{{ Session::getToken() }}";
			
			Dropzone.options.myAwesomeDropzone = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.options.imageUpload = {
				
				maxFilesize: 1,
				
				acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf"
				
			};
			
			jQuery( "#remittance_receipt" ).dropzone( {
				
				url: "{{ url('/upload/remittance_receipt/temp') }}",
				
				maxFiles: 1,

				maxfilesexceeded: function( file ) {
					
					this.removeAllFiles();

					this.addFile( file );

				},

				params: { _token: token },

				addRemoveLinks: true,
				
				init: function() {
					
					//ADD EVENT ON REMOVED FILE
					this.on( 'removedfile', function( file ) {
						
						//GET THE NEW IMAGE NAME
						var image_name = jQuery( file.previewElement ).find( '[data-dz-name]' ).html();
						
						//REMOVE HIDDEN FIELD
						jQuery( 'input[name="remittance_receipt"][value="' + image_name + '"]' ).remove();
						
					} );
					
				},

				success: function ( file, response ) {
					
					var img_name = response.filname;
					
					file.previewElement.classList.add( "dz-success" );
					
					jQuery( file.previewElement ).find( '[data-dz-name]' ).html( img_name );
					
					//APPEND PANEL BODY
					var hidden_image_name_template = '';
					
					//GENERATE HIDDEN IMAGE NAME TEMPLATE
					hidden_image_name_template += '<input type="hidden" name="remittance_receipt" value="' + img_name + '">';
					
					//APPEND HIDDEN ON PANEL BODY
					jQuery( '#remittance_modal .modal-body' ).append( hidden_image_name_template );
					
				},
				
				error: function ( file, response ) {
					
					file.previewElement.classList.add( "dz-error" );
					
				}
				
			} );
			
		}
		
		//INIT REMITTANCE ADDITIONAL REQUIREMENT DROPZONE
		function init_remittance_additional_requirement_dropzone() {
			
			var token = "{{ Session::getToken() }}";
			
			Dropzone.options.myAwesomeDropzone = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.options.imageUpload = {
				
				maxFilesize: 1,
				
				acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf"
				
			};
			
			jQuery( "#remittance_additional_requirement" ).dropzone( {
				
				url: "{{ url('/upload/remittance_additional_requirement/temp') }}",
				
				maxFiles: 1,

				maxfilesexceeded: function( file ) {
					
					this.removeAllFiles();

					this.addFile( file );

				},

				params: { _token: token },

				addRemoveLinks: true,
				
				init: function() {
					
					//ADD EVENT ON REMOVED FILE
					this.on( 'removedfile', function( file ) {
						
						//GET THE NEW IMAGE NAME
						var image_name = jQuery( file.previewElement ).find( '[data-dz-name]' ).html();
						
						//REMOVE HIDDEN FIELD
						jQuery( 'input[name="remittance_additional_requirement"][value="' + image_name + '"]' ).remove();
						
						//REMOVE REMITTANCE ADDITIONAL REQUIREMENT HIDDEN VALUE
						jQuery( '#remittance_additional_requirement_hidden' ).val( '' );
						
					} );
					
				},

				success: function ( file, response ) {
					
					var img_name = response.filname;
					
					file.previewElement.classList.add( "dz-success" );
					
					jQuery( file.previewElement ).find( '[data-dz-name]' ).html( img_name );
					
					//APPEND PANEL BODY
					var hidden_image_name_template = '';
					
					//GENERATE HIDDEN IMAGE NAME TEMPLATE
					hidden_image_name_template += '<input type="hidden" name="remittance_additional_requirement" value="' + img_name + '">';
					
					//APPEND HIDDEN ON PANEL BODY
					jQuery( '#remittance_modal .modal-body' ).append( hidden_image_name_template );
					
					//ADD VALUE TO REMITTANCE ADDITIONAL REQUIREMENT HIDDEN VALUE
					jQuery( '#remittance_additional_requirement_hidden' ).val( '1' );
					
				},
				
				error: function ( file, response ) {
					
					file.previewElement.classList.add( "dz-error" );
					
				}
				
			} );
			
		}
		
		
		
		//INIT PARSELEY
		function init_parseley() {
			
			jQuery( '#remittance_form' ).parsley( {
				
				errorsContainer: function(el) {
					
					return el.$element.closest( '.form-group' );
					
				},

				errorsWrapper: '<span class="help-block"></span>',

				errorTemplate: '<span></span>',

				errorMessage: 'Required'
				
			} );
			
		}

		//ON VALIDATE
		function on_validate() {

			if( ! jQuery( '#remittance_form' ).parsley().validate() ) {
				
				var tab_id = jQuery( '.parsley-error' ).closest( '.tab-pane' ).attr( 'id' );
				
				jQuery( 'a[href="#'+tab_id+'"]' ).trigger( 'click' );

				return false;

			}

			return true;

		}


		//COMPUTE TOTAL PHP DEPOSIT
		function compute_total_php_deposit() {
			
			
			var amount_sent = jQuery( '#remittance_amount_sent' ).val();
			
			var forex = jQuery( '#remittance_forex_rate' ).val();
			
			var service_fee = jQuery( '#remittance_service_fee' ).val();
			
			
			var total_php_deposit = 0;
			
			
			//CHECK IF AMOUNT SENT IS EMPTY OR UNDEFINED
			if( amount_sent == '' || amount_sent == undefined ) {
			
				amount_sent = 0;
				
			}
			
			//CHECK IF FOREX IS EMPTY OR UNDEFINED
			if( forex == '' || forex == undefined ) {

				forex = 0;

			}
			
			//CHECK IF SERVICE FEE IS EMPTY OR UNDEFINED
			if( service_fee == '' || service_fee == undefined ) {

				service_fee = 0;

			}
			
			//FORMULA
			total_php_deposit = ( ( parseFloat( amount_sent ) + parseFloat( service_fee ) ) * parseFloat( forex ) );

			//APPEND THIS TO TOTAL PHP DEPOSIT
			jQuery( '#remittance_total_php_deposit' ).val( parseFloat( total_php_deposit ) ).number( true, 2 );
			
		}
		
		
		
		//GET REMITTER RECIPIENTS
		function get_remitter_recipients( remitter_id, recipient_id ) {
			
			//CHECK IF REMITTER ID IS NOT EMPTY AND NOT UNDEFINED
			if( remitter_id != '' && remitter_id != undefined ) {
				
				var data = {
					
					remitter_id : remitter_id,
					
					_token : '{{ csrf_token() }}'
					
				};
				
				//AJAX FORM DATA
				jQuery.post( '{{ url( "/iremit/remittances/get-remitter-recipient" ) }}', data, function( response ) {
				
					//FORM SUBMIT SUCCESSFUL
					if( response.success ) {
						
						var recipient_template = '<option value=""></option>';
							
						jQuery.each( response.result.remitter_recipient, function ( i, val ) {

							recipient_template += '<option value="' + val.id + '" '+ ( recipient_id != '' && recipient_id != undefined ? 'selected="selected"' : '' ) +' >' + val.text + '</option>';
							
						} );

						//APPEND THE RECIPIENT LISTS
						jQuery( '#remittance_recipient_id' ).empty().append( recipient_template ).trigger( 'chosen:updated' );

					}

					else {

						//SHOW SOME ERROR MESSAGE
						alert( response.error_message );

					}
					
				}, 'json' );
			
			}
			
		}
		
		//GET REMITTER FULL DETAILS
		function get_remitter_full_details( remitter_id ) {
			
			//CHECK IF REMITTER ID IS NOT EMPTY AND NOT UNDEFINED
			if( remitter_id != '' && remitter_id != undefined ) {
				
				//AJAX FORM DATA
				jQuery.get( '{{ url( "/iremit/remittances/get-remitter-full-details" ) }}', { remitter_id : remitter_id }, function( response ) {
				
					//FORM SUBMIT SUCCESSFUL
					if( response.success ) {
						
						jQuery( '.remitter_full_details_container' ).html( response.result.content );

					}

					else {
						
						//SHOW SOME ERROR MESSAGE
						alert( response.error_message );

					}
					
				}, 'json' );
				
			}
			
		}
		
		//GET RECIPIENT FULL DETAILS
		function get_recipient_full_details( recipient_id ) {
			
			//CHECK IF RECIPIENT ID IS NOT EMPTY AND NOT UNDEFINED
			if( recipient_id != '' && recipient_id != undefined ) {
				
				//AJAX FORM DATA
				jQuery.get( '{{ url( "/iremit/remittances/get-recipient-full-details" ) }}', { recipient_id : recipient_id }, function( response ) {
				
					//FORM SUBMIT SUCCESSFUL
					if( response.success ) {
						
						jQuery( '.recipient_full_details_container' ).html( response.result.content );

					}

					else {
						
						//SHOW SOME ERROR MESSAGE
						alert( response.error_message );

					}
					
				}, 'json' );
				
			}
			
		}



		//GET RECEIVE OPTION LIST
		function get_receive_option_list( $receive_option_id ) {
			
			//CHECK IF RECIPIENT ID IS NOT EMPTY AND NOT UNDEFINED
			if( receive_option_id != '' && receive_option_id != undefined ) {
				
				//AJAX FORM DATA
				jQuery.get( '{{ url( "/iremit/remittances/get-receive-option-list" ) }}', { receive_option_id : receive_option_id }, function( response ) {
				
					//FORM SUBMIT SUCCESSFUL
					if( response.success ) {
						
						var receive_option_list = '';
						
						jQuery.each( data.results.receive_option_list, function ( i, val ) {
							
							receive_option_list += '<option value="' + val.id + '">' + val.name + '</option>';
							
						} );
						
						jQuery( '.remittance_receive_option_list' ).empty().append( receive_option_list ).trigger( 'chosen:updated' );

					}

					else {
						
						//SHOW SOME ERROR MESSAGE
						alert( response.error_message );

					}
					
				}, 'json' );
				
			}
			
		}



		//REINITIALIZE ICHECK EVENT
		function reinitialize_icheck_event() {
			
			jQuery( document ).ready( function() {
			
				jQuery( "input.flat" )[ 0 ] && jQuery( document ).ready( function() {
					
					jQuery( "input.flat" ).iCheck( {
						
						checkboxClass: "icheckbox_flat-green",
						
						radioClass: "iradio_flat-green"
						
					} );
					
					jQuery( "table input" ).on( "ifChecked", function() { checkState = "", jQuery( this ).parent().parent().parent().addClass( "selected" ), countChecked() } ); 
					
					jQuery( "table input" ).on( "ifUnchecked", function() { checkState = "", jQuery( this ).parent().parent().parent().removeClass( "selected" ), countChecked() } );
					
				} );
				
			} );
			
		}


	</script>

@endsection
