@extends('layouts.iremit')

@section('content')

	<div class="page-title">
		
		<div class="title_left">
			
			<h3> <i class="fa fa-users"></i> Remitters </h3>
			
		</div>
		
	</div>

	<div class="clearfix"></div>

	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
			<div class="x_panel">
				
				<div class="x_title">
					
					<a href="{{ url('/iremit/remitters/modal') }}" class="btn btn-primary btn-xs show_modal_btn" style="margin: 5px 0px;"> <i class="fa fa-plus"></i> Add New Remitter </a>
					
					<ul class="nav navbar-right panel_toolbox">
						
						<li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
						
						<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> </li>
						
						<li> <a class="close-link"> <i class="fa fa-close"></i> </a> </li>
					
					</ul>
					
					<div class="clearfix"></div>
					
				</div>
				
				<div class="x_content">
					
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 5px;">
						
						<div class="panel panel-default">
							
							<div class="panel-heading" role="tab" id="search_remitter">
								
								<h4 class="panel-title">
									
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="<?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'true' : 'false' ); ?>" aria-controls="collapseOne" <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? '' : 'class="collapsed"' ); ?>>

										<small> Click me to show search </small>

									</a>

								</h4>

							</div>

							<div id="collapseOne" class="panel-collapse collapse <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'in' : '' ); ?>" role="tabpanel" aria-labelledby="search_remitter">

								<div class="panel-body">
									
									<form method="GET" id="remitter_search_form" class="form-horizontal">

										<div class="row">
											
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="remitter_firstname"> Firstname: </label>
													
													<input type="text" id="remitter_firstname" name="remitter_firstname" class="form-control" value="<?php echo ( isset( $_GET[ 'remitter_firstname' ] ) && ! empty( $_GET[ 'remitter_firstname' ] ) ? $_GET[ 'remitter_firstname' ] : '' ); ?>"> 
												
												</div>
											
											</div>
											
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="remitter_lastname"> Lastname: </label>
													
													<input type="text" id="remitter_lastname" name="remitter_lastname" class="form-control" value="<?php echo ( isset( $_GET[ 'remitter_lastname' ] ) && ! empty( $_GET[ 'remitter_lastname' ] ) ? $_GET[ 'remitter_lastname' ] : '' ); ?>"> 
												
												</div>
											
											</div>
											
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label form="remitter_email"> Email: </label>
													
													<input type="text" id="remitter_email" name="remitter_email" class="form-control" value="<?php echo ( isset( $_GET[ 'remitter_email' ] ) && ! empty( $_GET[ 'remitter_email' ] ) ? $_GET[ 'remitter_email' ] : '' ); ?>"> 
												
												</div>
											
											</div>
											
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
											
												<div class="form-group">
												
													<label for="remitter_contact"> Contact: </label>
													
													<input type="text" id="remitter_contact" name="remitter_contact" class="form-control" value="<?php echo ( isset( $_GET[ 'remitter_contact' ] ) && ! empty( $_GET[ 'remitter_contact' ] ) ? $_GET[ 'remitter_contact' ] : '' ); ?>"> 
												
												</div>
											
											</div>
											
											<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
											
												<div class="form-group text-center">
												
													<label> 
														
														<small>
														
															<i class="fa fa-envelope-o"></i> Verified: 
														
															<br>
														
															<input type="checkbox" id="remitter_email_verified" name="remitter_email_verified" style="margin:15px 0px 0px;" <?php echo ( isset( $_GET[ 'remitter_email_verified' ] ) && ! empty( $_GET[ 'remitter_email_verified' ] ) ? 'checked="checked"' : '' ); ?>>
															
														</small>
														
													</label>
													
												</div>
											
											</div>
											
											<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
											
												<div class="form-group text-center">
												
													<label> 
														
														<small>
														
															<i class="fa fa-mobile"></i> Verified: 
														
															<br>
														
															<input type="checkbox" id="remitter_sms_verified" name="remitter_sms_verified" style="margin:15px 0px 0px;" <?php echo ( isset( $_GET[ 'remitter_sms_verified' ] ) && ! empty( $_GET[ 'remitter_sms_verified' ] ) ? 'checked="checked"' : '' ); ?>>
															
														</small>
														
													</label>
													
												</div>
											
											</div>
										
										</div>
										
										<div class="row">
										
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												
												<a href="{{ url( 'iremit/remitters' ) }}" class="btn btn-default btn-sm"> <i class="fa fa-refresh" aria-hidden="true"></i> Reset </a>
												
												<button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> Search </button>
												
											</div>
										
										</div>

									</form>
									
								</div>

							</div>

						</div>

					</div>

					@include( 'tables.remitter_table' )

				</div>
			
			</div>
			
		</div>
		
	</div>
	
@endsection

@section('styles')

	<link href="{{ URL::asset('bower_components/gentelella/vendors/dropzone/dist/min/dropzone.min.css') }}" rel="stylesheet">
	
    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('bower_components/gentelella/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('bower_components/gentelella/build/css/custom.min.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('css/chosen.min.css') }}" rel="stylesheet">
    
	<style type="text/css">
	
		/*IS LOADING PLUGIN*/
		.isloading-wrapper.isloading-right{ margin-left:10px; }

		.isloading-overlay{ position:relative;text-align:center; }

		.isloading-overlay .isloading-wrapper {

			line-height: 20px; 
			
			background:#ffffff; 
			
			-webkit-border-radius:7px; 
			
			-webkit-background-clip:padding-box; 
			
			-moz-border-radius:7px; 
			
			-moz-background-clip:padding;
			
			border-radius:7px;
			
			background-clip:padding-box;
			
			display:inline-block;
			
			margin:0 auto;
			
			padding:10px 20px;
			
			z-index:9000;

		}
		
		.pagination{ margin: 10px 0px 5px; }
		
		.help-block { color: #E74C3C; }
		
		.scrollbar-light { height: 215px; overflow-y: scroll; }
		
		.checkbox label, .radio label{ font-weight: 700; padding-left: 0px; }
	
	</style>

@endsection

@section('scripts')

	<script src="{{ URL::asset('bower_components/gentelella/vendors/dropzone/dist/min/dropzone.min.js') }}"></script>
	
	<script src="{{ URL::asset('bower_components/gentelella/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
	
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/iCheck/icheck.min.js') }}"></script>
    
	<script src="{{ URL::asset('js/jquery.isloading.min.js') }}"></script>
	
	<script src="{{ URL::asset('js/handlebars-v4.0.5.js') }}"></script>
	
	<script src="{{ URL::asset('js/chosen.jquery.min.js') }}"></script>
	
	<script src="{{ URL::asset('js/chosen.ajaxaddition.jquery.js') }}"></script>
	
	<script type="text/javascript">

		jQuery( document ).ready( function() {
			
			//INITIALIZE ALL
			init_all();

			//SHOW MODAL
			jQuery( document ).on( 'click', '.show_modal_btn', function( e ) {
				
				e.preventDefault();
				
				var href = jQuery( this ).attr( 'href' );
				
				//SHOW LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Loading',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );
				
				//CALL REMITTER AJAX
				jQuery.get( href, function( response ) {
					
					if( response.success ) {
						
						//GET ACTION
						var action = response.result.action;
						
						//APPEND AJAX MODAL
						jQuery( 'body' ).append( response.result.content );
						
						//OPEN MODAL
						jQuery( '#remitter_modal' ).modal( { backdrop: 'static', keyboard: false } );
						
						
						//ACTION == EDIT
						if( action == 'edit' ) {
							
							var remitter_id = response.result.remitter_id;
							
							var remitter_company_id = response.result.remitter_company_id;
							
							
							retrive_remitter_avatar( remitter_id );
							
							retrive_remitter_ids( remitter_id );
							
							retrive_remitter_company_authorization( remitter_company_id );
						
						}
						
						//ACTION == NEW
						else {
							
							//REINITIALIZE AVATAR
							init_remitter_avatar_dropzone();
							
							init_remitter_company_authorization_dropzone();
						
						}
						
												
						//REINITIALIZE ALL
						init_all();

					}
					
					else {
						
						//SHOW SOME ERROR MESSAGE
						alert( response.error_message );
						
					}
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
				}, 'json' );

			} );
			
			//REMOVE REMITTER MODAL
			jQuery( document ).on( 'hidden.bs.modal', '#remitter_modal', function( e ) {
				
				jQuery( this ).remove();

				jQuery( '.modal-backdrop' ).remove();

				jQuery( 'body' ).removeClass( 'modal-open' );
				
			} );


			//REMITTER TYPE
			jQuery( document ).on( 'change', '#remitter_type_id', function( e ) { 
				
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				//COMPANY
				if( current_value == 1 || current_value == '' ) {

					//HIDE CONTAINER
					jQuery( '#remitter_company_nav' ).fadeOut();
					
					//REMOVE REMITTER COMPANY REQUIRED ATTR
					remove_remitter_company_required_attr();
					
				}
				
				//PERSONAL
				else {

					//SHOW CONTAINER
					jQuery( '#remitter_company_nav' ).fadeIn();
					
					//ADD REMITTER COMPANY REQUIRED ATTR
					add_remitter_company_required_attr();

				}
				
			} );


			//ADDRESS
			jQuery( document ).on( 'click', '.add_address_btn', function( e ) { 
			
				e.preventDefault();
				
				
				var hash = Math.random().toString( 36 ).slice( 2 );
				
				var countries = {!! $countries !!};
				
				var tenancy_frequencies = {!! $tenancy_frequencies !!};

				
				var source = jQuery( '#remitter_address_template' ).html();
				
				var template = Handlebars.compile( source );

				var html = template( {

					hash: hash,

					countries: countries,
					
					tenancy_frequencies: tenancy_frequencies
					
				} );
				
				
				var total_is_preferred = jQuery( '.remitter_address_is_preferred' ).length;
				
				if( total_is_preferred >= 3 ) {
					
					alert( "You already reach maximum remitter address!" );
					
				}
				
				else {
				
					//CHECK IF IS PREFFERED IS ALREADY POPULATED
					if( total_is_preferred == 0 ) {
						
						//REMOVE NOTIFICATION
						jQuery( '.remitter_address_container' ).html( html );
					
					}
					
					else {
						
						//APPEND NEW REMITTER ADDRESS TEMPLATE
						jQuery( '.remitter_address_container' ).append( html ).fadeIn();
						
					}
					
				}
				
				//REINITIALIZE ALL
				init_all();
			
			} );

			//REMOVE REMITTER ADDRESS ITEM
			jQuery( document ).on( 'click', '.remove_remitter_address_item', function( e ) {
				
				var hash = jQuery( this ).attr( 'rel' );
			
				e.preventDefault();
				
				jQuery( '#remitter_address_item_' + hash ).remove();

				var total_is_preferred = jQuery( '.remitter_address_is_preferred' ).length;
				
				//CHECK IF IS PREFFERED IS ALREADY POPULATED
				if( total_is_preferred == 0 ) {
					
					//NOTIFICATION TEMPLATE
					var notification_template = '';
					
					notification_template += '<div class="alert alert-danger" role="alert" style="margin:10px 0px;">';

					notification_template += '<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No address for this remitter! </h5>';

					notification_template += '</div>';
					
					//REMOVE NOTIFICATION
					jQuery( '.remitter_address_container' ).html( notification_template );
				
				}
				
			} );

			//GET REMITTER STATE
			jQuery( document ).on( 'change', '.remitter_country', function( e ) {
				
				e.preventDefault();
				
				var hash = jQuery( this ).data( 'hash' );
				
				var country_id = jQuery( '#remitter_country_' + hash ).val();
				
				//LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Retreiving states',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );
				
				//CHECK IF COUNTRY ID IS EMPTY
				if( country_id == '' ) {
					
					//RESET STATE
					jQuery( '#remitter_state_' + hash ).empty().append( '<option value=""></option>' ).trigger( 'chosen:updated' );
					
					//RESET CITY
					jQuery( '#remitter_city_' + hash ).empty().append( '<option value=""></option>' ).trigger( 'chosen:updated' );
					
					//RESET POSTAL
					jQuery( '#remitter_postal_' + hash ).val();
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
					//INIT ALL
					init_all();
					
				}
				
				//GET STATES FROM COUNTRY
				else {

					jQuery.get( '{{ url( "iremit/library/get-states-from-country" ) }}' + '/' + country_id + '/' + hash, function( response ) {
						
						if( response.success ) {
							
							
							var hash = response.result.hash;
							
							var states = response.result.states;
							
									
							var source = jQuery( '#remitter_state_option_template' ).html();
							
							var template = Handlebars.compile( source );

							var html = template( {
								
								states: states
								
							} );
							
							
							jQuery( '#remitter_state_' + hash ).html( html ).trigger( 'chosen:updated' );
							
							
						}
						
						else {
							
							alert( response.error_message );
							
						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
						//REINITIALIZE ALL
						init_all();
						
					}, 'json' );
					
				}
				
			} );

			//GET REMITTER CITY
			jQuery( document ).on( 'change', '.remitter_state', function( e ) {
			
				e.preventDefault();


				var hash = jQuery( this ).data( 'hash' );
				
				var state_id = jQuery( '#remitter_state_' + hash ).val();


				//LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Retreiving cities',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );
				
				//CHECK IF STATE ID IS EMPTY
				if( state_id == '' ) {
					
					//RESET CITY
					jQuery( '#remitter_city_' + hash ).empty().append( '<option value=""></option>' ).trigger( 'chosen:updated' );
					
					//RESET POSTAL
					jQuery( '#remitter_postal_' + hash ).val();
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
					//INIT ALL
					init_all();
					
				}
				
				//GET CITIES FROM STATE
				else {
					
					jQuery.get( '{{ url( "iremit/library/get-cities-from-state" ) }}' + '/' + state_id + '/' + hash, function( response ) {
						
						if( response.success ) {
							
							
							var hash = response.result.hash;
							
							var cities = response.result.cities;
							
									
							var source = jQuery( '#remitter_city_option_template' ).html();
							
							var template = Handlebars.compile( source );

							var html = template( {
								
								cities: cities
								
							} );
							
							
							jQuery( '#remitter_city_' + hash ).html( html ).trigger( 'chosen:updated' );
							
							
						}
						
						else {
							
							alert( response.error_message );
							
						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
					
						//REINITIALIZE ALL
						init_all();
						
					}, 'json' );
					
				}
				
			} );

			//GET REMITTER POSTAL
			jQuery( document ).on( 'change', '.remitter_city', function( e ) { 
				
				e.preventDefault();
				
				var hash = jQuery( this ).data( 'hash' );
				
				var city_id = jQuery( '#remitter_city_' + hash ).val();


				//LOADING
				jQuery( 'body' ).isLoading( {
					
					text: 'Retreiving postal',
					
					class: 'fa fa fa-spinner fa-spin',
					
					position: 'overlay',
					
					transparency: 0.5
					
				} );
				
				//CHECK IF CITY ID IS EMPTY
				if( city_id == '' ) {
					
					//RESET POSTAL
					jQuery( '#remitter_postal_' + hash ).val();
					
					//HIDE LOADING
					jQuery( 'body' ).isLoading( 'hide' );
					
					//INIT ALL
					init_all();
					
				}
				
				//GET POSTAL FROM CITY
				else {
					
					jQuery.get( '{{ url( "iremit/library/get-postal-code-from-city" ) }}' + '/' + city_id + '/' + hash, function( response ) {
						
						if( response.success ) {
							
							var hash = response.result.hash;
							
							var postal_code = response.result.postal_code;
							
							jQuery( '#remitter_postal_' + hash ).val( postal_code );
							
						}
						
						else {
							
							alert( response.error_message );
							
						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
						//REINITIALIZE ALL
						init_all();
						
					}, 'json' );
					
				}
				
			} );
			
			//REMITTER ADDRESS IS PREFERRED
			jQuery( document ).on( 'ifChanged', '.remitter_address_is_preferred', function( e ) { 
			
				jQuery( '.remitter_address_is_preferred' ).removeAttr( 'checked' ).iCheck( 'update' );
				
				jQuery( this ).prop( 'checked', true ).iCheck( 'update' );
				
			} );


			//CONTACT
			jQuery( document ).on( 'click', '.add_contact_btn', function( e ) { 
				
				e.preventDefault();
				
				var hash = Math.random().toString( 36 ).slice( 2 );
				
				var contact_types = {!! $contact_types !!};

				
				var source = jQuery( '#remitter_contact_template' ).html();
				
				var template = Handlebars.compile( source );

				var html = template( {

					hash: hash,

					contact_types: contact_types
					
				} );
				
				
				var total_is_preferred = jQuery( '.remitter_contact_is_preferred' ).length;
				
				if( total_is_preferred >= 3 ) {
					
					alert( "You already reach maximum remitter contact!" );
					
				}
				
				else {
				
					//CHECK IF IS PREFFERED IS ALREADY POPULATED
					if( total_is_preferred == 0 ) {
						
						//REMOVE NOTIFICATION
						jQuery( '.remitter_contact_container' ).html( html );
					
					}
					
					else {
						
						//APPEND NEW REMITTER CONTACT TEMPLATE
						jQuery( '.remitter_contact_container' ).append( html ).fadeIn();
						
					}
					
				}
				
				//REINITIALIZE ALL
				init_all();
				
			} );

			//REMOVE REMITTER CONTACT ITEM
			jQuery( document ).on( 'click', '.remove_remitter_contact_item', function( e ) {
				
				var hash = jQuery( this ).attr( 'rel' );
			
				e.preventDefault();
				
				jQuery( '#remitter_contact_item_' + hash ).remove();

				var total_is_preferred = jQuery( '.remitter_contact_is_preferred' ).length;
				
				//CHECK IF IS PREFFERED IS ALREADY POPULATED
				if( total_is_preferred == 0 ) {
					
					//NOTIFICATION TEMPLATE
					var notification_template = '';
					
					notification_template += '<div class="alert alert-danger" role="alert" style="margin:10px 0px;">';

					notification_template += '<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No contact for this remitter! </h5>';

					notification_template += '</div>';
					
					//REMOVE NOTIFICATION
					jQuery( '.remitter_contact_container' ).html( notification_template );
				
				}
				
			} );

			//REMITTER CONTACT IS PREFERRED
			jQuery( document ).on( 'ifChanged', '.remitter_contact_is_preferred', function( e ) { 
			
				jQuery( '.remitter_contact_is_preferred' ).removeAttr( 'checked' ).iCheck( 'update' );
				
				jQuery( this ).prop( 'checked', true ).iCheck( 'update' );
				
			} );


			//IDENTIFICATION
			jQuery( document ).on( 'click', '.add_identification_btn', function( e ) { 
				
				e.preventDefault();
				
				var hash = Math.random().toString( 36 ).slice( 2 );
				
				var identification_types = {!! $identification_types !!};

				
				var source = jQuery( '#remitter_identification_template' ).html();
				
				var template = Handlebars.compile( source );

				var html = template( {

					hash: hash,

					identification_types: identification_types
					
				} );
				
				
				var total_is_preferred = jQuery( '.remitter_identification_is_preferred' ).length;
				
				if( total_is_preferred >= 3 ) {
					
					alert( "You already reach maximum remitter idenfication!" );
					
				}
				
				else {
				
					//CHECK IF IS PREFFERED IS ALREADY POPULATED
					if( total_is_preferred == 0 ) {
						
						//REMOVE NOTIFICATION
						jQuery( '.remitter_identification_container' ).html( html );
					
					}
					
					else {
						
						//APPEND NEW REMITTER IDENTIFICATION TEMPLATE
						jQuery( '.remitter_identification_container' ).append( html ).fadeIn();
						
					}
					
				}
				
				//INIT DROPZONE
				init_remitter_id_dropzone( hash );
				
				//REINITIALIZE ALL
				init_all();
				
			} );
			
			//SHOW OTHER IDENTIFICATION TYPE
			jQuery( document ).on( 'change', '.remitter_identification_type', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				var hash = jQuery( this ).data( 'hash' );
				
				//OTHERS
				if( current_value == 1 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#remitter_identification_other_type_container_' + hash ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#remitter_identification_other_type_container_' + hash ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#remitter_identification_other_type_' + hash ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#remitter_identification_other_type_container_' + hash ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#remitter_identification_other_type_container_' + hash ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#remitter_identification_other_type_' + hash ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#remitter_identification_other_type_' + hash ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#remitter_identification_other_type_' + hash ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );

			//REMOVE REMITTER IDENTIFICATION ITEM
			jQuery( document ).on( 'click', '.remove_remitter_identification_item', function( e ) {
				
				var hash = jQuery( this ).attr( 'rel' );
			
				e.preventDefault();
				
				jQuery( '#remitter_identification_item_' + hash ).remove();
				
				jQuery( 'input[name="remitter_identification_file[' + hash + ']"]' ).remove();

				var total_is_preferred = jQuery( '.remitter_identification_is_preferred' ).length;
				
				//CHECK IF IS PREFFERED IS ALREADY POPULATED
				if( total_is_preferred == 0 ) {
					
					//NOTIFICATION TEMPLATE
					var notification_template = '';
					
					notification_template += '<div class="alert alert-danger" role="alert" style="margin:10px 0px;">';

					notification_template += '<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No identification for this remitter! </h5>';

					notification_template += '</div>';
					
					//REMOVE NOTIFICATION
					jQuery( '.remitter_identification_container' ).html( notification_template );
				
				}
				
			} );

			//REMITTER IDENTIFICATION IS PREFERRED
			jQuery( document ).on( 'ifChanged', '.remitter_identification_is_preferred', function( e ) { 
				
				jQuery( '.remitter_identification_is_preferred' ).removeAttr( 'checked' ).iCheck( 'update' );
				
				jQuery( this ).prop( 'checked', true ).iCheck( 'update' );
				
			} );


			//SHOW REMITTER COMPANY OTHER SOURCE OF FUND
			jQuery( document ).on( 'change', '#remitter_company_source_of_fund_id', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				//OTHERS
				if( current_value == 1 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#remitter_company_other_source_of_fund_container' ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#remitter_company_other_source_of_fund_container' ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#remitter_company_other_source_of_fund' ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#remitter_company_other_source_of_fund_container' ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#remitter_company_other_source_of_fund_container' ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#remitter_company_other_source_of_fund' ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#remitter_company_other_source_of_fund' ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#remitter_company_other_source_of_fund' ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );

			//SHOW REMITTER COMPANY OTHER COMPANY RELATIONSHIP
			jQuery( document ).on( 'change', '#remitter_company_relationship_id', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				//OTHERS
				if( current_value == 1 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#remitter_company_other_relationship_container' ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#remitter_company_other_relationship_container' ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#remitter_company_other_relationship' ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#remitter_company_other_relationship_container' ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#remitter_company_other_relationship_container' ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#remitter_company_other_relationship' ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#remitter_company_other_relationship' ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#remitter_company_other_relationship' ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );


			//SHOW REMITTER OTHER MARKETING SOURCE
			jQuery( document ).on( 'change', '#remitter_marketing_source_id', function( e ) { 
			
				e.preventDefault();
				
				var current_value = jQuery( this ).val();
				
				//OTHERS
				if( current_value == 1 ) {
					
					//CHECK IF CONTAINER IS HIDDEN
					if( jQuery( '#remitter_other_marketing_source_container' ).is( ':hidden' ) ) {
						
						//SHOW CONTAINER
						jQuery( '#remitter_other_marketing_source_container' ).fadeIn();
						
					}
					
					//ADD REQUIRED ATTRIBUTE
					jQuery( '#remitter_other_marketing_source' ).attr( 'required', 'true' );
					
				}
				
				else {

					//CHECK IF CONTAINER IS VISIBLE
					if( jQuery( '#remitter_other_marketing_source_container' ).is( ':visible' ) ) {

						//HIDE CONTAINER
						jQuery( '#remitter_other_marketing_source_container' ).fadeOut();

					}
					
					//GET PARSELEY ID
					var parseley_id = jQuery( '#remitter_other_marketing_source' ).data( 'parsley-id' );
					
					//REMOVE REQUIRED ATTRIBUTE
					jQuery( '#remitter_other_marketing_source' ).removeAttr( 'required' );
					
					//REMOVE PARSELEY ERROR CLASS
					jQuery( '#remitter_other_marketing_source' ).removeClass( 'parsley-error' );
					
					//REMOVE PARSELEY ERROR LIST
					jQuery( '#parsley-id-' + parseley_id ).remove();

				}

			} );


			//CHECK ALL REMITTER CHECKBOX
			jQuery( document ).on( 'ifChanged', '#select_all_remitters', function() {
					
				//CHECK IF REMITTANCE SELECT ALL IS NOT CHECKED
				if ( ! jQuery( this ).is( ':checked' ) ) {
					
					//REMOVE ALL CHECKBOX CHECKED ATTR
					jQuery( '.remitter_checkbox' ).prop( 'checked', false ).iCheck( 'update' );
					
					//REMOVE CLASS SELECTED TO ALL TR
					jQuery( '.jambo_table tbody tr' ).removeClass( 'selected' );

				} 

				else {
					
					//ADD ALL CHECKBOX CHECKED ATTR
					jQuery( '.remitter_checkbox' ).prop( 'checked', true ).iCheck( 'update' );
					
					//ADD CLASS SELECTED TO ALL TR
					jQuery( '.jambo_table tbody tr' ).addClass( 'selected' );
				
				} 
				  
			} );
			
			//CHECK REMITTER CHECKBOX
			jQuery( document ).on( 'ifChanged', '.remitter_checkbox', function() {
				
				var all = jQuery( '.remitter_checkbox' );
				
				//CHECK IF ALL REMITTER CHECKBOX IS CHECKED
				if ( all.length === all.filter( ':checked' ).length ) {
					
					//CHECK SELECT ALL CHECKBOX
					jQuery( '#select_all_remitters' ).prop( 'checked', true ).iCheck( 'update' );

				}

				else {
					
					//REMOVE CHECK SELECT ALL CHECKBOX
					jQuery( '#select_all_remitters' ).prop( 'checked', false ).iCheck( 'update' );

				}

			} );
			

			//SUBMIT REMITTER FORM
			jQuery( document ).on( 'submit', '#remitter_form', function( e ) { 
				
				e.preventDefault();
				
				
				//CHECK IF ADDRESS IS ALREADY SELECTED
				var total_address_is_preferred = jQuery( '.remitter_address_is_preferred' ).length;
				
				if( total_address_is_preferred >= 1 ) {
					
					//CHECK IF ADDRESS IS ALREADY SELECTED
					var address_selected = jQuery( '.remitter_address_is_preferred:checked' ).length;
					
					//IF ADDRESS IS NOT SELECTED
					if( address_selected == null || address_selected == 0 ) {
						
						//ALERT CURRENT USER
						alert( 'Please select remitter address!' );
						
						jQuery( 'a[href="#remitter_address_tab"]' ).trigger( 'click' );
						
						return false;
						
					}
					
				}
				
				
				//CHECK IF CONTACT IS ALREADY SELECTED
				var total_contact_is_preferred = jQuery( '.remitter_contact_is_preferred' ).length;
				
				if( total_contact_is_preferred >= 1 ) {
					
					//CHECK IF CONTACT IS ALREADY SELECTED
					var contact_selected = jQuery( '.remitter_contact_is_preferred:checked' ).length;
					
					//IF CONTACT IS NOT SELECTED
					if( contact_selected == null || contact_selected == 0 ) {
						
						//ALERT CURRENT USER
						alert( 'Please select remitter contact!' );
						
						jQuery( 'a[href="#remitter_contact_tab"]' ).trigger( 'click' );
						
						return false;
						
					}
					
				}
				
				
				//CHECK IF IDENTIFICATION IS ALREADY SELECTED
				var total_identification_is_preferred = jQuery( '.remitter_identification_is_preferred' ).length;
				
				if( total_identification_is_preferred >= 1 ) {
					
					//CHECK IF IDENTIFICATION IS ALREADY SELECTED
					var identification_selected = jQuery( '.remitter_identification_is_preferred:checked' ).length;
					
					//IF CONTACT IS NOT SELECTED
					if( identification_selected == null || identification_selected == 0 ) {
						
						//ALERT CURRENT USER
						alert( 'Please select remitter identification!' );
						
						jQuery( 'a[href="#remitter_identification_tab"]' ).trigger( 'click' );
						
						return false;
						
					}
					
				}
				
				
				//CONFIRM ANSWER
				var answer = confirm( 'Do you want to submit this remitter form?' );
				
				//IF YES
				if( answer ) {
					
					//SHOW LOADING
					jQuery( 'body' ).isLoading( {
						
						text: 'Submitting form',
						
						class: 'fa fa fa-spinner fa-spin',
						
						position: 'overlay',
						
						transparency: 0.5
						
					} );
					
					//GET DATA
					var data = jQuery( this ).serializeArray();
					
					data.push( { name: '_token', value: '{{ csrf_token() }}' } );
					
					//AJAX FORM DATA
					jQuery.post( '{{ url( "/iremit/remitters/form/post" ) }}', data, function( response ) { 
					
						//FORM SUBMIT SUCCESSFUL
						if( response.success ) {

							//GET REMITTER ID
							var remitter_id = response.result.remitter_id;

							//ADD NEW RECORD
							if( response.result.action == 'add' ) {
								
								new PNotify( {
									
									title: 'Success!',

									text: 'Remitter has been added!',

									type: 'success',
									
									styling: 'bootstrap3'

								} );

							}

							//EDIT RECORD
							if( response.result.action == 'edit' ) {
								
								new PNotify( {
									
									title: 'Success!',

									text: 'Remitter has been updated!',
									
									styling: 'bootstrap3'

								} );

							}
							
							//UPDATE REMITTER TABLE
							jQuery( '.remitter_table_container' ).html( response.result.remitter_table_template );

							//HIDE MODAL
							jQuery( '#remitter_modal' ).modal( 'hide' );
							
							//INIT ALL
							init_all();

						}

						else {

							//SHOW SOME ERROR MESSAGE
							alert( response.error_message );

						}
						
						//HIDE LOADING
						jQuery( 'body' ).isLoading( 'hide' );
						
					}, 'json' );
				
				}
				
				
			} );


			//DELETE REMITTER
			jQuery( document ).on( 'submit', '#remitter_table', function( e ) {
				
				e.preventDefault();
				
				var total_checked_remitters = jQuery( 'input[name="remitter[]"]:checked' ).length;
				
				if( total_checked_remitters ) {
					
					var answer_text = 'Do you want to delete this remitter' + ( total_checked_remitters > 1 ? 's' : '' );
					
					var answer = confirm( answer_text + '?' );
					
					if( answer ) {
						
						var data = jQuery( this ).serializeArray();
						
						data.push( { name: '_token', value: '{{ csrf_token() }}' } );
						
						jQuery.post( '{{ url( "/iremit/remitters/delete-remitters" ) }}', data, function( response ) {
							
							//FORM SUBMIT SUCCESSFUL
							if( response.success ) {
								
								if( response.result.action == 'delete' ) {
									
									var pnotify_text = 'Remitter' + ( response.result.total_remitter > 1 ? 's' : '' ) + ' has been deleted!';
									
									new PNotify( {
										
										title: 'Success!',

										text: pnotify_text + '!',

										type: 'error',
										
										styling: 'bootstrap3'

									} );

								}

								//UPDATE REMITTER TABLE
								jQuery( '.remitter_table_container' ).html( response.result.remitter_table_template );

								//HIDE MODAL
								jQuery( '#remitter_modal' ).modal( 'hide' );
								
								//INIT ALL
								init_all();
								
							}

							else {

								//SHOW SOME ERROR MESSAGE
								alert( response.error_message );

							}
							
						} );
						
					}
				
				}
				
				else {
					
					alert( 'Please select remitter that you want to delete!' );
					
				}
				
			} );
			
		} );

		//INIT ALL
		function init_all() {
			
			//INIT DATEPICKER
			init_datepicker();
			
			//INIT CHOSEN
			init_chosen();
			
			//INIT ICHECK
			init_icheck();
			
			//INIT PARSELEY
			init_parseley();
			
			//REINITIALIZE ICHECK EVENT
			reinitialize_icheck_event();
			
		}

		//INIT DATEPICKER
		function init_datepicker() {
			
			//INITIALIZE DATEPICKER
			jQuery( '.datepicker' ).daterangepicker( {
				
				singleDatePicker: true,
				
				showDropdowns: true,
				
				format: 'YYYY-MM-DD'
				
			} );
		
		}

		//INIT CHOSEN
		function init_chosen() {
			
			var config = {
				
				'.chosen-select'           : { width: '100%', allow_single_deselect: true },
				
				'.chosen-select-deselect'  : { allow_single_deselect: true },
				
				'.chosen-select-no-single' : { disable_search_threshold: 10 },
				
				'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
				
				'.chosen-select-rtl'       : { rtl: true },
				
				'.chosen-select-width'     : { width: '100%' }
				
			}

			for ( var selector in config ) {
				
				jQuery( selector ).chosen( config[ selector ] );

			}
			
		}
		
		//INIT ICHECK
		function init_icheck() {
			
			jQuery( '.remitter_address_is_resident, .remitter_address_is_preferred, .remitter_contact_is_preferred, .remitter_identification_is_preferred' ).iCheck( {
				
				checkboxClass: 'icheckbox_flat-green',
				
				radioClass: 'iradio_flat-green'
				
			} );
			
		}

		//INIT REMITTER AVATAR DROPZONE
		function init_remitter_avatar_dropzone() {
			
			var token = "{{ Session::getToken() }}";
			
			Dropzone.options.myAwesomeDropzone = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.options.imageUpload = {
				
				maxFilesize: 5,
				
				acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf"
				
			};
			
			jQuery( "#remitter_profile_avatar" ).dropzone( {
				
				url: "{{ url('/upload/remitter_avatar/temp') }}",
				
				maxFiles: 1,

				maxfilesexceeded: function( file ) {
					
					this.removeAllFiles();

					this.addFile( file );

				},

				params: { _token: token },

				addRemoveLinks: true,
				
				init: function() {
					
					//ADD EVENT ON REMOVED FILE
					this.on( 'removedfile', function( file ) {
						
						//GET THE NEW IMAGE NAME
						var image_name = jQuery( file.previewElement ).find( '[data-dz-name]' ).html();
						
						//REMOVE HIDDEN FIELD
						jQuery( 'input[name="remitter_profile_avatar"][value="' + image_name + '"]' ).remove();
						
					} );
					
				},

				success: function ( file, response ) {
					
					var img_name = response.filname;
					
					file.previewElement.classList.add( "dz-success" );
					
					jQuery( file.previewElement ).find( '[data-dz-name]' ).html( img_name );
					
					//APPEND PANEL BODY
					var hidden_image_name_template = '';
					
					//GENERATE HIDDEN IMAGE NAME TEMPLATE
					hidden_image_name_template += '<input type="hidden" name="remitter_profile_avatar" value="' + img_name + '">';
					
					//APPEND HIDDEN ON PANEL BODY
					jQuery( '#remitter_modal .modal-body' ).append( hidden_image_name_template );
					
				},
				
				error: function ( file, response ) {
					
					file.previewElement.classList.add( "dz-error" );
					
				}
				
			} );
			
		}
		
		//INIT REMITTER ID DROPZONE
		function init_remitter_id_dropzone( hash ) {
			
			var token = "{{ Session::getToken() }}";
			
			Dropzone.options.myAwesomeDropzone = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.options.imageUpload = {
				
				maxFilesize: 5,
				
				acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf"
				
			};
			
			jQuery( "#remitter_identification_file_" + hash ).dropzone( {
				
				url: "{{ url('/upload/remitter_id/temp') }}",
				
				maxFiles: 1,

				maxfilesexceeded: function( file ) {
					
					this.removeAllFiles();

					this.addFile( file );

				},

				params: { _token: token },

				addRemoveLinks: true,
				
				init: function() {
						
					//ADD EVENT ON REMOVED FILE
					this.on( 'removedfile', function( file ) {
						
						//GET THE NEW IMAGE NAME
						var image_name = jQuery( file.previewElement ).find( '[data-dz-name]' ).html();
						
						//REMOVE HIDDEN FIELD
						jQuery( 'input[name="remitter_identification[' + hash + ']"][file][value="' + image_name + '"]' ).remove();
						
					} );
					
				},

				success: function ( file, response ) {
					
					var img_name = response.filname;
					
					file.previewElement.classList.add( "dz-success" );
					
					jQuery( file.previewElement ).find( '[data-dz-name]' ).html( img_name );
					
					//APPEND PANEL BODY
					var hidden_image_name_template = '';
					
					//GENERATE HIDDEN IMAGE NAME TEMPLATE
					hidden_image_name_template += '<input type="hidden" name="remitter_identification[' + hash + '][file]" value="' + img_name + '">';
					
					//APPEND HIDDEN ON PANEL BODY
					jQuery( '#remitter_modal .modal-body' ).append( hidden_image_name_template );
					
				},
				
				error: function ( file, response ) {
					
					file.previewElement.classList.add( "dz-error" );
					
				}
				
			} );
			
		}
		
		//INIT REMITTER COMPANY AUTHORIZATION DROPZONE
		function init_remitter_company_authorization_dropzone() {
			
			var token = "{{ Session::getToken() }}";
			
			Dropzone.options.myAwesomeDropzone = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.options.imageUpload = {
				
				maxFilesize: 5,
				
				acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf"
				
			};
			
			jQuery( "#remitter_company_authorization" ).dropzone( {
				
				url: "{{ url('/upload/remitter_company_authorization/temp') }}",
				
				maxFiles: 1,

				maxfilesexceeded: function( file ) {
					
					this.removeAllFiles();

					this.addFile( file );

				},

				params: { _token: token },

				addRemoveLinks: true,
				
				init: function() {
					
					//ADD EVENT ON REMOVED FILE
					this.on( 'removedfile', function( file ) {
						
						//GET THE NEW IMAGE NAME
						var image_name = jQuery( file.previewElement ).find( '[data-dz-name]' ).html();
						
						//REMOVE HIDDEN FIELD
						jQuery( 'input[name="remitter_company_authorization"][value="' + image_name + '"]' ).remove();
						
						//REMOVE REMITTER COMPANY AUTHORIZATION HIDDEN VALUE
						jQuery( '#remitter_company_authorization_hidden' ).val( '' );
						
					} );
					
				},

				success: function ( file, response ) {
					
					var img_name = response.filname;
					
					file.previewElement.classList.add( "dz-success" );
					
					jQuery( file.previewElement ).find( '[data-dz-name]' ).html( img_name );
					
					//APPEND PANEL BODY
					var hidden_image_name_template = '';
					
					//GENERATE HIDDEN IMAGE NAME TEMPLATE
					hidden_image_name_template += '<input type="hidden" name="remitter_company_authorization" value="' + img_name + '">';
					
					//APPEND HIDDEN ON PANEL BODY
					jQuery( '#remitter_modal .modal-body' ).append( hidden_image_name_template );
					
					//ADD VALUE TO REMITTER COMPANY AUTHORIZATION HIDDEN VALUE
					jQuery( '#remitter_company_authorization_hidden' ).val( '1' );
					
				},
				
				error: function ( file, response ) {
					
					file.previewElement.classList.add( "dz-error" );
					
				}
				
			} );
			
		}
		
		//INIT PARSELEY
		function init_parseley() {
			
			jQuery( '#remitter_form' ).parsley( {
				
				errorsContainer: function(el) {
					
					return el.$element.closest( '.form-group' );
					
				},

				errorsWrapper: '<span class="help-block"></span>',

				errorTemplate: '<span></span>',

				errorMessage: 'Required'
				
			} );
			
		}
		
		//ON VALIDATE
		function on_validate() {

			if( ! jQuery( '#remitter_form' ).parsley().validate() ) {
				
				var tab_id = jQuery( '.parsley-error' ).closest( '.tab-pane' ).attr( 'id' );
				
				jQuery( 'a[href="#'+tab_id+'"]' ).trigger( 'click' );

				return false;

			}

			return true;

		}



		//ADD REMITTER COMPANY REQUIRED ATTR
		function add_remitter_company_required_attr() {
			
			var remitter_company_fields = [];
			
			remitter_company_fields[0] = 'remitter_company_name';
			
			remitter_company_fields[1] = 'remitter_company_contact';
			
			remitter_company_fields[2] = 'remitter_company_address';
			
			remitter_company_fields[3] = 'remitter_company_country';
			
			remitter_company_fields[4] = 'remitter_company_registration';
			
			remitter_company_fields[5] = 'remitter_company_remittance_range';
			
			remitter_company_fields[6] = 'remitter_company_source_of_fund_id';
			
			remitter_company_fields[7] = 'remitter_company_relationship_id';
			
			remitter_company_fields[8] = 'remitter_company_frequency_id';
			
			remitter_company_fields[9] = 'remitter_company_bank_type_id';
			
			remitter_company_fields[10] = 'remitter_company_authorization_hidden';
			
			//LOOP ALL FIELDS
			jQuery.each( remitter_company_fields, function( i, v ) {
			
				jQuery( '#' + v ).attr( 'required', 'true' );
							
			} );
			
		}

		//REMOVE REMITTER COMPANY REQUIRED ATTR
		function remove_remitter_company_required_attr() {
			
			var parseley_id = 0;
			
			var remitter_company_fields = [];
			
			remitter_company_fields[0] = 'remitter_company_name';
			
			remitter_company_fields[1] = 'remitter_company_contact';
			
			remitter_company_fields[2] = 'remitter_company_address';
			
			remitter_company_fields[3] = 'remitter_company_country';
			
			remitter_company_fields[4] = 'remitter_company_registration';
			
			remitter_company_fields[5] = 'remitter_company_remittance_range';
			
			remitter_company_fields[6] = 'remitter_company_source_of_fund_id';
			
			remitter_company_fields[7] = 'remitter_company_relationship_id';
			
			remitter_company_fields[8] = 'remitter_company_frequency_id';
			
			remitter_company_fields[9] = 'remitter_company_bank_type_id';
			
			remitter_company_fields[10] = 'remitter_company_authorization_hidden';
			
			//LOOP ALL FIELDS
			jQuery.each( remitter_company_fields, function( i, v ) {
			
				parseley_id = jQuery( '#' + v ).data( 'parsley-id' );

				jQuery( '#' + v ).removeAttr( 'required' );

				jQuery( '#' + v ).removeClass( 'parsley-error' );

				jQuery( '#parsley-id-' + parseley_id ).remove();
			
			} );

		}



		/*
		*EDIT FUNCTIONALITY
		*/



		//RETRIEVE REMITTER AVATAR
		function retrive_remitter_avatar( remitter_id ) {
			
			var token = "{{ Session::getToken() }}";
			
			Dropzone.options.myAwesomeDropzone = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.options.imageUpload = {
				
				maxFilesize: 5,
				
				acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf"
				
			};
			
			jQuery( "#remitter_profile_avatar" ).dropzone( {
				
				url: "{{ url('/upload/remitter_avatar/temp') }}",
				
				maxFiles: 1,

				maxfilesexceeded: function( file ) {
					
					this.removeAllFiles();

					this.addFile( file );

				},

				params: { _token: token },

				addRemoveLinks: true,
				
				init: function() {
					
					var myDropzone = this;
					
					//RETRIEVE STORED PROFILE AVATAR
					jQuery.get( "{{ url( '/iremit/remitters/profile/get-avatar' ) }}" + '/' + remitter_id, function( response ) {
						
						//SUCCESS
						if( response.success ) {

							//CHECK IF FILENAME IS NOT EMPTY
							if( response.result.filename != '' ) {
								
								//RETRIEVE FILE FROM STORAGE SERVER
								var file = { name: response.result.filename, size: response.result.size };
								
								myDropzone.createThumbnailFromUrl( file, '{{ asset( "img/remitter_avatar" ) }}' + '/' + response.result.filename );
								
								myDropzone.options.addedfile.call( myDropzone, file );
								
								myDropzone.options.thumbnail.call( myDropzone, file, '{{ asset( "img/remitter_avatar" ) }}' + '/' + response.result.filename );

								myDropzone.files[ 0 ] = file;
								
								myDropzone.emit( 'complete', file );
							
							}
							
						}
						
						else {
							
							//SHOW SOME ERROR MESSAGE
							alert( response.error_message );
							
						}
					
					}, 'json' );
					
					//ADD EVENT ON ADDED FILE
					this.on( 'addedfile', function() { 
						
						if ( this.files[ 1 ] != null ) { 
							
							this.removeFile( this.files[ 0 ] );
							
						}
						
					} );

					//ADD EVENT ON REMOVED FILE
					this.on( 'removedfile', function( file ) {
						
						//GET THE NEW IMAGE NAME
						var image_name = jQuery( file.previewElement ).find( '[data-dz-name]' ).html();

						//REMOVE HIDDEN FIELD
						jQuery( 'input[name="remitter_profile_avatar"][value="' + image_name + '"]' ).remove();

					} );
					
				},

				success: function ( file, response ) {
					
					var img_name = response.filname;
					
					file.previewElement.classList.add( "dz-success" );
					
					jQuery( file.previewElement ).find( '[data-dz-name]' ).html( img_name );
					
					//APPEND PANEL BODY
					var hidden_image_name_template = '';
					
					//GENERATE HIDDEN IMAGE NAME TEMPLATE
					hidden_image_name_template += '<input type="hidden" name="remitter_profile_avatar" value="' + img_name + '">';
					
					//APPEND HIDDEN ON PANEL BODY
					jQuery( '#remitter_modal .modal-body' ).append( hidden_image_name_template );
					
				},
				
				error: function ( file, response ) {
					
					file.previewElement.classList.add( "dz-error" );
					
				}
				
			} );
			
		}
		
		//RETRIEVE REMITTER IDS
		function retrive_remitter_ids( remitter_id ) {
			
			var token = "{{ Session::getToken() }}";
			
			Dropzone.options.myAwesomeDropzone = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.options.imageUpload = {
				
				maxFilesize: 5,
				
				acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf"
				
			};
			
			//RETRIEVE STORED PROFILE AVATAR
			jQuery.get( "{{ url( '/iremit/remitters/identification/get-images' ) }}" + '/' + remitter_id, function( response ) {

				if( response.success ) {
					
					var identification_images = response.result.idenfitication_images;
					
					//LOOP ALL IDENTIFICATION IMAGES
					jQuery.each( identification_images, function( index, value ) {
						
						//REINITIALIZE IDENTIFICATION FILE ONE BY ONE
						jQuery( "#remitter_identification_file_" + value.id + "_hash" ).dropzone( {
							
							url: "{{ url('/upload/remitter_id/temp') }}",
							
							maxFiles: 1,

							maxfilesexceeded: function( file ) {
								
								this.removeAllFiles();

								this.addFile( file );

							},

							params: { _token: token },

							addRemoveLinks: true,
							
							init: function() {
								
								if( value.filename != '' && value.filename != null ) {
								
									//RETRIEVE FILE FROM STORAGE SERVER
									var file = { name: value.filename, size: value.size };
									
									this.createThumbnailFromUrl( file, '{{ asset( "img/remitter_id" ) }}' + '/' + value.filename );

									this.options.addedfile.call( this, file );

									this.options.thumbnail.call( this, file, '{{ asset( "img/remitter_id" ) }}' + '/' + value.filename );
									
									this.files[ 0 ] = file;

									this.emit( 'complete', file );
								
								}
					
								//ADD EVENT ON ADDED FILE
								this.on( 'addedfile', function() { 
									
									if ( this.files[ 1 ] != null ) { 
										
										this.removeFile( this.files[ 0 ] );
										
									}
									
								} );

								//ADD EVENT ON REMOVED FILE
								this.on( 'removedfile', function( file ) {
									
									//GET THE NEW IMAGE NAME
									var image_name = jQuery( file.previewElement ).find( '[data-dz-name]' ).html();
									
									//GET HIDDEN FIELD NAME
									var hidden_field_name = jQuery( 'input[value="'+image_name+'"]' ).attr( 'name' );
									
									//REMOVE HIDDEN FIELD
									jQuery( 'input[name="'+hidden_field_name+'"]' ).remove();
									
								} );
								
							},

							success: function ( file, response ) {
								
								var img_name = response.filname;
								
								file.previewElement.classList.add( "dz-success" );
								
								jQuery( file.previewElement ).find( '[data-dz-name]' ).html( img_name );
								
								var hidden_value = jQuery( file.previewElement ).closest( '.well-sm' ).find( '.remitter_identifiation_hidden' ).val();
								
								//APPEND PANEL BODY
								var hidden_image_name_template = '';
								
								//GENERATE HIDDEN IMAGE NAME TEMPLATE
								hidden_image_name_template += '<input type="hidden" name="remitter_identification_old[' + hidden_value + '_hash][file]" value="' + img_name + '">';
								
								//APPEND HIDDEN ON PANEL BODY
								jQuery( '#remitter_modal .modal-body' ).append( hidden_image_name_template );
								
							},
							
							error: function ( file, response ) {
								
								file.previewElement.classList.add( "dz-error" );
								
							}
							
						} );
						
					} );
					
				}
				
				else {
					
					//SHOW SOME ERROR MESSAGE
					alert( response.error_message );
					
				}
				
			}, 'json' );
			
		}

		//RETRIEVE REMITTER COMPANY AUTHORIZATION
		function retrive_remitter_company_authorization( remitter_company_id ) {
			
			var token = "{{ Session::getToken() }}";
			
			Dropzone.options.myAwesomeDropzone = false;
			
			Dropzone.autoDiscover = false;
			
			Dropzone.options.imageUpload = {
				
				maxFilesize: 5,
				
				acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf"
				
			};
			
			jQuery( "#remitter_company_authorization" ).dropzone( {
				
				url: "{{ url('/upload/remitter_company_authorization/temp') }}",
				
				maxFiles: 1,

				maxfilesexceeded: function( file ) {
					
					this.removeAllFiles();

					this.addFile( file );

				},

				params: { _token: token },

				addRemoveLinks: true,
				
				init: function() {
					
					var myDropzone = this;
					
					//RETRIEVE STORED COMPANY AUTHORIZATION
					jQuery.get( "{{ url( '/iremit/remitters/company/get-authorization' ) }}" + '/' + remitter_company_id, function( response ) {
						
						//SUCCESS
						if( response.success ) {

							//CHECK IF FILENAME IS NOT EMPTY
							if( response.result.filename != '' ) {
								
								//RETRIEVE FILE FROM STORAGE SERVER
								var file = { name: response.result.filename, size: response.result.size };
								
								myDropzone.createThumbnailFromUrl( file, '{{ asset( "img/remitter_company_authorization" ) }}' + '/' + response.result.filename );
								
								myDropzone.options.addedfile.call( myDropzone, file );
								
								myDropzone.options.thumbnail.call( myDropzone, file, '{{ asset( "img/remitter_company_authorization" ) }}' + '/' + response.result.filename );
								
								myDropzone.files[ 0 ] = file;
								
								myDropzone.emit( 'complete', file );
							
							}
							
						}
						
						else {
							
							//SHOW SOME ERROR MESSAGE
							alert( response.error_message );
							
						}
					
					}, 'json' );
					
					//ADD EVENT ON ADDED FILE
					this.on( 'addedfile', function() { 
						
						if ( this.files[ 1 ] != null ) { 
							
							this.removeFile( this.files[ 0 ] );
							
						}
						
					} );

					//ADD EVENT ON REMOVED FILE
					this.on( 'removedfile', function( file ) {
						
						//GET THE NEW IMAGE NAME
						var image_name = jQuery( file.previewElement ).find( '[data-dz-name]' ).html();
						
						//REMOVE HIDDEN FIELD
						jQuery( 'input[name="remitter_company_authorization"][value="' + image_name + '"]' ).remove();
						
					} );
					
				},

				success: function ( file, response ) {
					
					var img_name = response.filname;
					
					file.previewElement.classList.add( "dz-success" );
					
					jQuery( file.previewElement ).find( '[data-dz-name]' ).html( img_name );
					
					//APPEND PANEL BODY
					var hidden_image_name_template = '';
					
					//GENERATE HIDDEN IMAGE NAME TEMPLATE
					hidden_image_name_template += '<input type="hidden" name="remitter_company_authorization" value="' + img_name + '">';
					
					//APPEND HIDDEN ON PANEL BODY
					jQuery( '#remitter_modal .modal-body' ).append( hidden_image_name_template );
					
				},
				
				error: function ( file, response ) {
					
					file.previewElement.classList.add( "dz-error" );
					
				}
				
			} );
			
		}


		//REINITIALIZE ICHECK EVENT
		function reinitialize_icheck_event() {
			
			jQuery( document ).ready( function() {
			
				jQuery( "input.flat" )[ 0 ] && jQuery( document ).ready( function() {
					
					jQuery( "input.flat" ).iCheck( {
						
						checkboxClass: "icheckbox_flat-green",
						
						radioClass: "iradio_flat-green"
						
					} );
					
					jQuery( "table input" ).on( "ifChecked", function() { checkState = "", jQuery( this ).parent().parent().parent().addClass( "selected" ), countChecked() } ); 
					
					jQuery( "table input" ).on( "ifUnchecked", function() { checkState = "", jQuery( this ).parent().parent().parent().removeClass( "selected" ), countChecked() } );
					
				} );
				
			} );
			
		}


	</script>
	
	<!--
	<script type="text/javascript">
	
		var conn = new WebSocket('ws://localhost:8080');
		
		conn.onopen = function(e) {
			
			console.log("Connection established!");
			
			conn.send( "Hi! I'm now connected" );
			
		};

		conn.onmessage = function(e) {
			
			console.log(e.data);
			
		};
	
	</script>
	-->

	<!-- INCLUDE HANDLEBARS -->
	@include( 'handlebars.remitter_address_handlebars' )
	
	@include( 'handlebars.remitter_contact_handlebars' )
	
	@include( 'handlebars.remitter_identification_handlebars' )
	
@endsection
