@extends('layouts.iremit')

@section('content')

	<div class="page-title">
		
		<div class="title_left">
			
			<h3> <i class="fa fa-file-text-o"></i> Risk Alert </h3>
			
		</div>
		
	</div>

	<div class="clearfix"></div>

	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
			<div class="x_panel">
				
				<div class="x_title">

					<button type="button" name="btnShowMultipleRemittance" value="getMultipleRemittance" class="btn btn-info btn-sm" style="margin: 5px 0px 0px;" data-page="1"> <i class="fa fa-search" aria-hidden="true"></i> Show Multiple Remittance </button>
				
					<button type="button" name="btnShowToRecipient" value="getRemittanceToSingleRecipient" class="btn btn-info btn-sm" style="margin: 5px 0px 0px;" data-page="1"> <i class="fa fa-list-alt" aria-hidden="true"></i> Filter Remittance to Recipient </button>

					<ul class="nav navbar-right panel_toolbox" >
						
						<li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
						
						<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> </li>
						
						<li> <a class="close-link"> <i class="fa fa-close"></i> </a> </li>
					
					</ul>
					
					<div class="clearfix"></div>
					
				</div>

				<div id="reportrange" class="pull-right col-lg-3.5 col-md-3.5 col-sm-3.5 col-xs-3.5" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">

					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>

					&nbsp;

					<span></span> <b class="caret"></b>
		
				</div>
				
				<div class="x_content">

					<div id="riskAlert-table-container">

						@include( 'tables.riskAlert.remitterstransactions' )
						
					</div>

				</div>

			</div>

		</div>

	</div>

	<div id="riskAlert-modal-container">

		@include('modals.riskalertmodal')

	</div>

@endsection

@section('styles')

	<link href="{{ URL::asset('bower_components/bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">

	<link href="{{ URL::asset('bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

@endsection

@section('scripts')

	<script src="{{ URL::asset('bower_components/gentelella/production/js/moment/moment.min.js') }}"></script>

	<script src="{{ URL::asset('bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

	{{-- <script src="{{ URL::asset('bower_components/gentelella/production/js/datepicker/daterangepicker.js') }}"></script> --}}

	{{-- Need to get this from cdn because the current daterangepicker is not compatible --}}
	{{-- <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script> --}}

	{{-- Datepicker --}}
	<script type="text/javascript">

		$(	function() 
			{

			    var start = moment().subtract(29, 'days');

			    var end = moment();

			    function cb(start, end) {

			        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

			        $('#reportrange').data( 'startdate', start.format('MMMM D, YYYY') );

			        $('#reportrange').data( 'enddate', end.format('MMMM D, YYYY') );

			        $( 'button[name*="btnShowMultipleRemittance"]' ).data( 'page', 1 );

			        $( 'button[name*="btnShowToRecipient"]' ).data( 'page', 1 );

			    }

			    $('#reportrange').daterangepicker(
			    									{

												        startDate: start,

												        endDate: end,

												        ranges: 
												        {

												           'Today': [moment(), moment()],

												           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

												           'This Week': [moment().startOf('week'), moment().endOf('week')],

												           'Last Week': [moment().subtract(1, 'weeks').startOf('week'), moment().subtract(1, 'weeks').endOf('week')],

												           'Last 7 Days': [moment().subtract(6, 'days'), moment()],

												           'This Month': [moment().startOf('month'), moment().endOf('month')],

												           'Last 30 Days': [moment().subtract(29, 'days'), moment()],

												           // 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
												        }
			    									}

			    									, 

			    									cb
			    								 )

			    ;

			    cb(start, end);
		    
			}

		  )

		;

	</script>

	{{-- Start of Risk Alert --}}
	<script type="text/javascript">

		$( document )

		.ready( documentReady )

		; 

		// End of jQuery document.ready

		
		$( document )

		.on( 

				'click'

				, 

				':button'

				, 

				function ( e )
				{

					btnClickedHandler( e ) 

				}	

		   )

		;  

		// End of jQuery .on(click) for buttons
		 
		
		$( document )

		.on( 

				'click'

				, 

				'a'

				, 

				function ( e )
				{

					anchorClickedHandler( e ) 

				}	

		   )

		;  

		// End of jQuery .on(click) for anchor tags


		function documentReady( )
		{

			$(document).on('click', '.pagination a',function(event)
		    {

		    	if( event.target.parentElement.nodeName == 'LI' )
		    	{

		    		$(this).parent('li').removeClass('active');
		    	
		    	}
		        //$('li').removeClass('active');

		        $(this).parent('li').addClass('active');

		        var myurl = $(this).attr('href');

		        var page = $(this).attr('href').split('page=')[1];

		        var tableType = $( 'table[name*="table-risk-alert"]' ).data( 'type' );

		        if( tableType == 'remitterMultipleTransaction' )
		        {

		        	if( $('#risk-alert-modal').is(':visible') ) //if modal is visible
		        	{

		        		updateRemitterModalTable( event.target );

		        	}
		        	else
		        	{
		        		
		        		getMultipleRemittance( page );

		        	}

		        }

		        else if( tableType == 'remittanceToRecipient' )
		        {

		        	if( $('#risk-alert-modal').is(':visible') ) //if modal is visible
		        	{

		        		updateRecipientModalTable( event.target );

		        	}
		        	else
		        	{

		        		getRemittanceToSingleRecipient( page );

		        	}

		        }

		        return false; //stops link from executing href

		    });

		}
		
		//End of documentReady() method


		function btnClickedHandler( event )
		{

			var targetElement = event

					            .

					            target	

			;

			var btnName = targetElement			   
	
						  .

						  name

			;

			switch( btnName )
			{

				case 'btnShowMultipleRemittance' :

					showMultiplRemittance( targetElement );

					break;

				case 'btnShowToRecipient' :

					showRemittanceToSingleRecipient( targetElement );

					break;

				// case 'btnExport' :

				// 	exportRemittances( targetElement );

				default:

			}

		}

		//End of btnClickedHandler() method
		

		function anchorClickedHandler( event )
		{

			var targetElement = event

					            .

					            target	

			;

			var anchorName = targetElement			   
	
						  	 .

						  	 name

			;

			switch( anchorName )
			{

				case 'riskAlertRemitter' :

					getRemitter( targetElement );
				
					break;	

				case 'riskAlertRecipient' :

					getRecipient( targetElement );
				
					break;		

				default:

			}

		}

		//End of anchorClickedHandler() method


		function showMultiplRemittance( element )
		{

			var btnValue = element

						   .

						   value

			;

			switch( btnValue )
			{

				case 'getMultipleRemittance' :

					getMultipleRemittance( $( element ).data( 'page' ) );

					break;

				default:

			}

		}

		// --- End of showMultiplRemittance() method --- //
		

		function showRemittanceToSingleRecipient( element )
		{

			var btnValue = element

						   .

						   value

			;

			switch( btnValue )
			{

				case 'getRemittanceToSingleRecipient' :

					getRemittanceToSingleRecipient( $( element ).data( 'page' ) );

					break;

				default:

			}

		}

		// --- End of showRemittanceToSingleRecipient() method --- //
		

		// function exportRemittances( element )
		// {

		// 	var btnValue = element

		// 				   .

		// 				   value

		// 	;

		// 	switch( btnValue )
		// 	{

		// 		case 'exportRecipientRemittance' :

		// 			exportRecipientRemittance( )

		// 			break;

		// 		default:

		// 	}

		// }

		// --- End of exportRemittances() method --- //


		function getMultipleRemittance( page )
		{

			var startdate = $('#reportrange')

							.data( 'startdate' )

			;

			var enddate = $('#reportrange')

						  .data( 'enddate' )

			;

			$( '#riskAlert-table-container' ).html( '<div style="text-align: center"> <br> <i class="fa fa-spinner fa-spin" style="font-size:64px; width: 50%; margin: 0 auto;"></i> <br> <br> </div>' );

			$.ajax
			  (
				{
				
				  	type: 'GET'

				  	,

					dataType: 'json'

					,
					
					url: "{{ url( 'riskalert/get' ) }}" + "?page=" + page

					,
						
					data: { 

							'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' )

							,


							'startdate' : startdate

							,

							'enddate' :  enddate

							,

							'queryType' : 'getMultipleRemittance'

						  }

					,
						
					success: function( data, textStatus, jqXHR ) 
							 {

							 	if( data.success )
							 	{

							 		$('#riskAlert-table-container').empty().html( data.remittersmultipletransaction );

							 		$( 'button[name*="btnShowMultipleRemittance"]' ).data( 'page', page );

							 		$( 'button[name*="btnShowMultipleRemittance"]' ).removeClass( 'btn-info' );

							 		$( 'button[name*="btnShowMultipleRemittance"]' ).addClass( 'btn-primary' );

							 		$( 'button[name*="btnShowToRecipient"]' ).removeClass( 'btn-primary' );

							 		$( 'button[name*="btnShowToRecipient"]' ).addClass( 'btn-info' );

							 	}
							
							 }
				
			 	}

			  ) 	 

			;

		}

		//End of getMultipleRemittance() method
		

		function getRemittanceToSingleRecipient( page )
		{

			var startdate = $('#reportrange')

							.data( 'startdate' )

			;

			var enddate = $('#reportrange')

						  .data( 'enddate' )

			;

			$( '#riskAlert-table-container' ).html( '<div style="text-align: center"> <br> <i class="fa fa-spinner fa-spin" style="font-size:64px; width: 50%; margin: 0 auto;"></i> <br> <br> </div>' );

			$.ajax
			  (
				{
				
				  	type: 'GET'

				  	,

					dataType: 'json'

					,
					
					url: "{{ url( 'riskalert/get' ) }}" + "?page=" + page

					,
						
					data: { 

							'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' )

							,

							'startdate' : startdate

							,

							'enddate' :  enddate

							,

							'queryType' : 'getRemittanceToSingleRecipient'

						  }

					,
						
					success: function( data, textStatus, jqXHR ) 
							 {

							 	if( data.success )
							 	{

							 		$('#riskAlert-table-container').empty().html( data.remittanceToSingleRecipient );

							 		$( 'button[name*="btnShowToRecipient"]' ).data( 'page', page );

							 		$( 'button[name*="btnShowMultipleRemittance"]' ).removeClass( 'btn-primary' );

							 		$( 'button[name*="btnShowMultipleRemittance"]' ).addClass( 'btn-info' );

							 		$( 'button[name*="btnShowToRecipient"]' ).removeClass( 'btn-info' );

							 		$( 'button[name*="btnShowToRecipient"]' ).addClass( 'btn-primary' );

							 	}
							
							 }
				
			 	}

			  ) 	 

			;

		}

		//End of getRemittanceToSingleRecipient() method
	
		
		function getRemitter( element )
		{

			var startdate = $('#reportrange')

							.data( 'startdate' )

			;

			var enddate = $('#reportrange')

						  .data( 'enddate' )

			;

			var remitterName = $.trim( 

										$( element )

										.text() 

									 )

			;

			var remitterId = $( element )

							 .data( 'remitter-id' )

			;
			
			$( '.modal-title' )

			 .empty()

			 .html( '<i class="fa fa-wpforms" data-remitter-id="' + remitterId + '"></i> ' + remitterName )

			;

			$('#risk-alert-modal').modal( {
				
				backdrop: 'static',
					
				keyboard: true
				
			} );

			$( '#risk-alert-modal-body' ).html( '<div style="text-align: center"> <br> <i class="fa fa-spinner fa-spin" style="font-size:64px; width: 50%; margin: 0 auto;"></i> <br> <br> </div>' );

			var page = element.attributes.href.value.split('page=')[1];

			$.ajax
			  (
				{
				
				  	type: 'GET'

				  	,

					dataType: 'json'

					,
					
					url: "{{ url( 'riskalert/get' ) }}" + "?page=" + page

					,
						
					data: { 

							'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' )

							,


							'startdate' : startdate

							,

							'enddate' :  enddate

							,

							'queryType' : 'getRemitterRemittances'

							,

							'remitterId' : $( element ).data( 'remitter-id' )

						  }

					,
						
					success: function( data, textStatus, jqXHR ) 
							 {

							 	if( data.success )
							 	{	

							 		$('#risk-alert-modal-body').empty().html( data.remitterRemittances );

							 		$('#btnExportRecipientPlaceHolder').empty().html( '<a href="{{ url( 'riskalert/download/remitterRemittance' ) }}/' + startdate  + '/' + enddate + '/' + remitterId + '" type="button" name="btnExport" value="exportRecipientRemittance" class="btn btn-sm btn-success" style="margin:0px;"> <i class="fa fa-download" aria-hidden="true" download></i> Export </a>');

							 	}
							
							 }
				
			 	}

			  ) 	 

			;

		}

		//End of getRemitter() method
		

		function getRecipient( element )
		{

			var startdate = $('#reportrange')

							.data( 'startdate' )

			;

			var enddate = $('#reportrange')

						  .data( 'enddate' )

			;

			var recipientName = $.trim( 

										$( element )

										.text() 

									 )

			;

			var recipientId = $( element )

							  .data( 'recipient-id' )

			;

			$( '.modal-title' )

			 .empty()

			 .html( '<i class="fa fa-wpforms" data-recipient-id="' + recipientId + '"></i> ' + recipientName )

			;

			$('#risk-alert-modal').modal( {
				
				backdrop: 'static',
					
				keyboard: true
				
			} );

			$( '#risk-alert-modal-body' ).html( '<div style="text-align: center"> <br> <i class="fa fa-spinner fa-spin" style="font-size:64px; width: 50%; margin: 0 auto;"></i> <br> <br> </div>' );

			var page = element.attributes.href.value.split('page=')[1];

			$.ajax
			  (
				{
				
				  	type: 'GET'

				  	,

					dataType: 'json'

					,
					
					url: "{{ url( 'riskalert/get' ) }}" + "?page=" + page

					,
						
					data: { 

							'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' )

							,


							'startdate' : startdate

							,

							'enddate' :  enddate

							,

							'queryType' : 'getRecipientRemittances'

							,

							'recipientId' : recipientId

						  }

					,
						
					success: function( data, textStatus, jqXHR ) 
							 {

							 	if( data.success )
							 	{	

							 		$('#risk-alert-modal-body').empty().html( data.recipientRemittances );
							 		
							 		$('#btnExportRecipientPlaceHolder').empty().html( '<a href="{{ url( 'riskalert/download/recipientRemittance' ) }}/' + startdate  + '/' + enddate + '/' + recipientId + '" type="button" name="btnExport" value="exportRecipientRemittance" class="btn btn-sm btn-success" style="margin:0px;"> <i class="fa fa-download" aria-hidden="true" download></i> Export </a>');

							 	}
							
							 }
				
			 	}

			  ) 	 

			;

		}

		// --- End of getRemitter() method --- //
		

		function updateRemitterModalTable( element )
		{

			var startdate = $('#reportrange')

							.data( 'startdate' )

			;

			var enddate = $('#reportrange')

						  .data( 'enddate' )

			;

			var page = element

					   .attributes

					   .href

					   .value

					   .split('page=')[1]

			;

			var remitterId = $( '.modal-title > i' ) //child italic element of parent class modal-title

							 .data( 'remitter-id' )

			;

			$( '#risk-alert-modal-body' ).html( '<div style="text-align: center"> <br> <i class="fa fa-spinner fa-spin" style="font-size:64px; width: 50%; margin: 0 auto;"></i> <br> <br> </div>' );

			$.ajax
			  (
				{
				
				  	type: 'GET'

				  	,

					dataType: 'json'

					,
					
					url: "{{ url( 'riskalert/get' ) }}" + "?page=" + page

					,
						
					data: { 

							'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' )

							,


							'startdate' : startdate

							,

							'enddate' :  enddate

							,

							'queryType' : 'getRemitterRemittances'

							,

							'remitterId' : remitterId 

						  }

					,
						
					success: function( data, textStatus, jqXHR ) 
							 {

							 	if( data.success )
							 	{	

							 		$('#risk-alert-modal-body').empty().html( data.remitterRemittances );

							 	}
							
							 }
				
			 	}

			  ) 	 

			;

		}

		function updateRecipientModalTable( element )
		{

			var startdate = $('#reportrange')

							.data( 'startdate' )

			;

			var enddate = $('#reportrange')

						  .data( 'enddate' )

			;

			var page = element

					   .attributes

					   .href

					   .value

					   .split('page=')[1]

			;

			var recipientId = $( '.modal-title > i' )

							 .data( 'recipient-id' )

			;

			$( '#risk-alert-modal-body' ).html( '<div style="text-align: center"> <br> <i class="fa fa-spinner fa-spin" style="font-size:64px; width: 50%; margin: 0 auto;"></i> <br> <br> </div>' );

			$.ajax
			  (
				{
				
				  	type: 'GET'

				  	,

					dataType: 'json'

					,
					
					url: "{{ url( 'riskalert/get' ) }}" + "?page=" + page

					,
						
					data: { 

							'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' )

							,


							'startdate' : startdate

							,

							'enddate' :  enddate

							,

							'queryType' : 'getRecipientRemittances'

							,

							'recipientId' : recipientId

						  }

					,
						
					success: function( data, textStatus, jqXHR ) 
							 {

							 	if( data.success )
							 	{	
							 		console.log( data );
							 		$('#risk-alert-modal-body').empty().html( data.recipientRemittances );

							 	}
							
							 }
				
			 	}

			  ) 	 

			;

		}

		// --- End of updateRecipientModalTable() method --- //


		// function exportRecipientRemittance( element )
		// {	

		// 	var startdate = $('#reportrange')

		// 					 .data( 'startdate' )

		// 	;

		// 	var enddate = $('#reportrange')

		// 				   .data( 'enddate' )

		// 	;

		// 	var recipientId = $( '.modal-title > i' )

		// 					   .data( 'recipient-id' )

		// 	;

		// 	$.ajax
		// 	  (
		// 		{
				
		// 		  	type: 'GET'

		// 		  	,

		// 			// dataType: 'json'

		// 			// ,
					
		// 			url: "{{ url( 'riskalert/get' ) }}"

		// 			,
						
		// 			data: { 

		// 					'_token' :  jQuery( 'meta[name="csrf-token"]' ).attr( 'content' )

		// 					,


		// 					'startdate' : startdate

		// 					,

		// 					'enddate' :  enddate

		// 					,

		// 					'queryType' : 'exportRecipientRemittance'

		// 					,

		// 					'recipientId' : recipientId

		// 				  }

		// 			,
						
		// 			success: function( data, textStatus, jqXHR )
		// 					{ 

		// 						console.log( data );

		// 						alert( "Data successfully exported" ); 

		// 					}

		// 			,

		// 			error: function (data) 
		// 				   { 

		// 					alert( 'error' );

		// 				   }
				
		// 	 	}

		// 	  ) 	 

		// 	;

		// }

		// --- End of exportRemittances() method --- //


	</script>
	{{-- End of Risk Alert --}}

@endsection
