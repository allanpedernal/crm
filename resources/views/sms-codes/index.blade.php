@extends('layouts.iremit')

@section('content')

	<div class="page-title">
		
		<div class="title_left">
			
			<h3> <i class="fa fa-book fa-fw"></i> SMS </h3>
			
		</div>
		
	</div>

	<div class="clearfix"></div>

	<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
			<div class="x_panel">
				
				<div class="x_title">
					
					<a href="{{ url('/codes/sms/modal') }}" class="btn btn-primary btn-xs show_modal_btn" style="margin: 5px 0px;"> <i class="fa fa-plus"></i> Add New SMS Code </a>
					
					<ul class="nav navbar-right panel_toolbox">
						
						<li> <a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
						
						<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> </li>
						
						<li> <a class="close-link"> <i class="fa fa-close"></i> </a> </li>
					
					</ul>
					
					<div class="clearfix"></div>
					
				</div>
				
				<div class="x_content">
					
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 5px;">
						
						<div class="panel panel-default">
							
							<div class="panel-heading" role="tab" id="search_remittance">
								
								<h4 class="panel-title">
									
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="<?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'true' : 'false' ); ?>" aria-controls="collapseOne" <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? '' : 'class="collapsed"' ); ?>>

										<small> Click me to show search </small>

									</a>

								</h4>

							</div>

							<div id="collapseOne" class="panel-collapse collapse <?php echo ( isset( $_GET ) && ! empty( $_GET ) ? 'in' : '' ); ?>" role="tabpanel" aria-labelledby="search_sms_code">

								<div class="panel-body">
									
									<form method="GET" id="search_remittance_form" class="form-horizontal">

										<div class="row">
											
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>

										</div>

										<div class="row">

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

												<a href="{{ url( 'codes/sms' ) }}" class="btn btn-default btn-sm"> <i class="fa fa-refresh" aria-hidden="true"></i> Reset </a>

												<button type="submit" class="btn btn-primary btn-sm"> <i class="fa fa-search" aria-hidden="true"></i> Search </button>

											</div>

										</div>

									</form>
									
								</div>

							</div>

						</div>

					</div>

					@include( 'tables.sms_table' )

				</div>
			
			</div>
			
		</div>
		
	</div>

@endsection

@section('styles')

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('css/chosen.min.css') }}" rel="stylesheet">
    
	<style type="text/css">
	
		/*IS LOADING PLUGIN*/
		.isloading-wrapper.isloading-right{ margin-left:10px; }

		.isloading-overlay{ position:relative;text-align:center; }

		.isloading-overlay .isloading-wrapper {

			line-height: 20px; 
			
			background:#ffffff; 
			
			-webkit-border-radius:7px; 
			
			-webkit-background-clip:padding-box; 
			
			-moz-border-radius:7px; 
			
			-moz-background-clip:padding;
			
			border-radius:7px;
			
			background-clip:padding-box;
			
			display:inline-block;
			
			margin:0 auto;
			
			padding:10px 20px;
			
			z-index:9000;

		}
		
		.pagination{ margin: 10px 0px 5px; }
		
		.help-block { color: #E74C3C; }
		
		.input-group-addon{ padding: 3px; }
		
	</style>

@endsection

@section('scripts')


	<script src="{{ URL::asset('bower_components/gentelella/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
	
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
    
    <script src="{{ URL::asset('bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>
    
	<script src="{{ URL::asset('js/jquery.isloading.min.js') }}"></script>

	<script src="{{ URL::asset('js/chosen.jquery.min.js') }}"></script>
	
	<script type="text/javascript">
		
		jQuery( document ).ready( function() {


		} );

		//INIT ALL
		function init_all() {
			
			
			//INIT DATEPICKER
			init_datepicker();
			
			//INIT PARSELEY
			init_parseley();
			
			//INIT CHOSEN
			init_chosen();
			
			
			//INIT SEARCH REMITTANCE REMITTER
			init_search_remittance_remitter();
			
			//INIT SEARCH REMITTANCE RECIPIENT
			init_search_remittance_recipient();
			
			
			//INIT REMITTANCE REMITTER
			init_remittance_remitter();
			
			
			//INIT REMITTANCE RECEIPT DROPZONE
			init_remittance_receipt_dropzone();
			
			//INIT REMITTANCE ADDITIONAL REQUIREMENT DROPZONE
			init_remittance_additional_requirement_dropzone();
			
			
		}
		
		//INIT CHOSEN
		function init_chosen() {
			
			var config = {
				
				'.chosen-select'           : { width: '100%', allow_single_deselect: true },
				
				'.chosen-select-deselect'  : { allow_single_deselect: true },
				
				'.chosen-select-no-single' : { disable_search_threshold: 10 },
				
				'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
				
				'.chosen-select-rtl'       : { rtl: true },
				
				'.chosen-select-width'     : { width: '100%' }
				
			}

			for ( var selector in config ) {
				
				jQuery( selector ).chosen( config[ selector ] );

			}
			
		}
		
	</script>


@endsection
