<div class="mobile_promotion_table_container">

	@if( isset( $mobile_promotions ) && ! empty( $mobile_promotions ) && COUNT( $mobile_promotions ) )
	
		<form method="POST" id="mobile_promotion_table" class="form-horizontal">
			
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>

				<div class="col-lg-7 col-lg-offset-4 col-md-7 col-md-offset-4 col-sm-12 col-xs-12 text-right">
					
					{{ $mobile_promotions -> appends( $_GET ) -> links() }}

				</div>

			</div>
			
			<div class="table-responsive">
					
				<table class="table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action" cellspacing="0" width="100%" style="margin:0px;">

					<thead>
						
						<tr class="headings">
							
							<th class="text-center"> <input type="checkbox" id="select_all_mobile_promotions" name="select_all_mobile_promotions" class="flat" value="1"> </th>
							
							<th class="column-title text-center"> <small> ID </small> </th>
							
	            			<th class="column-title"> <small> Excerpt </small> </th>
	            			
	            			<th class="column-title text-center"> <small> Send times </small> </th>
	            			
	            			<th class="column-title text-center"> <small> Send </small> </th>
	            			
	            			<th class="column-title text-center"> <small> Date </small> </th>
	            			
	            			<th class="column-title no-link last text-center"> <small> Action </small> </th>
	            			
						</tr>

					</thead>
					
					<tbody>
						
						@php ( $x = 1 )
						
						@foreach ( $mobile_promotions as $key => $mobile_promotion )
						
							<tr class="{{ ( $x % 2 ? 'even' : 'odd') }} pointer">
							
								<td class="text-center"> <input type="checkbox" id="mobile_promotion_{{ $mobile_promotion[ 'id' ] }}" name="mobile_promotion[]" class="flat mobile_promotion_checkbox" value="{{ $mobile_promotion[ 'id' ] }}"> </td>
								
								<td class="text-center"> <small> {{ $mobile_promotion[ 'id' ] }} </small> </td>
								
								<td> <small> {{ $mobile_promotion[ 'excerpt' ] }} </small> </td>
								
								<td class="text-center"> <small> {{ $mobile_promotion[ 'send_times' ] }} </small> </td>
								
								<td class="text-center"> <small> {!! ( $mobile_promotion[ 'is_send' ] == 1 ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>' ) !!} </small> </td>

								<td class="text-center"> <small> {{ date( 'M j, Y g:i A', strtotime( $mobile_promotion[ 'date' ] ) ) }} </small> </td>
								
								<td class="last text-center">
								
									<a href="{{ url( '/mobile/promotions/send/' . $mobile_promotion[ 'id' ] ) }}" class="btn btn-success btn-xs send_btn" data-id="{{ $mobile_promotion[ 'id' ] }}" target="_blank"> <i class="fa fa-paper-plane"></i> </a>
									
									<a href="{{ url( '/mobile/promotions/modal/' . $mobile_promotion[ 'id' ] ) }}" class="btn btn-warning btn-xs show_modal_btn" data-id="{{ $mobile_promotion[ 'id' ] }}"> <i class="fa fa-pencil"></i> </a>
									
								</td>
								
							</tr>
							
							@php ( $x++ )
						
						@endforeach
					
					</tbody>
				 
				</table>
				
			</div>
			
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>

				<div class="col-lg-7 col-lg-offset-4 col-md-7 col-md-offset-4 col-sm-12 col-xs-12 text-right">
					
					{{ $mobile_promotions -> appends( $_GET ) -> links() }}

				</div>

			</div>
			
		</form>
	
	@else 
	
		<div class="alert alert-danger" role="alert" style="margin: 20px 0px 0px;">

			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No mobile promotion list! </h5>

		</div>
	
	@endif
	
</div>
