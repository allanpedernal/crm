<div class="promo_code_table_container">

	@if( isset( $remittancePromoCodes ) && ! empty( $remittancePromoCodes ) && COUNT( $remittancePromoCodes ) )

		<div class="table-responsive">
				
			<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			  
				<thead>
					
					<tr>
						
						<th> Name </th>

						<th> Value </th>

						<th> PClass </th>
						
						<th> Type </th>

						<th> Consume </th>
						
						<th> UClass </th>
						
						<th> Range </th>
						
						<th> EDate </th>

						<th class="text-center"> Action </th>

					</tr>

				</thead>

				<tbody>

					@foreach( $remittancePromoCodes as $promoCode )
					
                    <tr>

						<td> {{ $promoCode->name }} </td>

						<td> {{ $promoCode->value }} </td>

						<td> {{ $promoCode->classification }} </td>

						<td> {{ $promoCode->type }} </td>

						<td> {{ $promoCode->total_consumable }} </td>

						<td> {{ $promoCode->user_classification }} </td>

						<td> {{ $promoCode->duration }} </td>

						<td> {{ $promoCode->expiration_date }} </td>
						
						<td class="text-center">
							
							<a class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i> View</a>
							
							<a class="btn btn-warning btn-xs edit_promo_code" data-promocodeid="{{$promoCode->id}}"><i class="fa fa-pencil"></i> Edit</a>
							
							<a class="btn btn-danger btn-xs delete_promo_code" id="{{$promoCode->id}}"><i class="fa fa-trash"></i> Delete</a>
							
						</td>

                    </tr>	

					@endforeach

				</tbody>
			
			</table>

		</div>
		
		<div class="row">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
			
				{{ $remittancePromoCodes->links() }}	

			</div>
		
		</div>

	@else

		<div class="alert alert-danger" role="alert" style="margin:0px;">

			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No  existing promo code! </h5>

		</div>

	@endif

</div>
