<div class="recipient_table_container">
	
	@if( isset( $recipients ) && ! empty( $recipients ) && COUNT( $recipients ) )
	
		<form method="POST" id="recipient_table" class="form-horizontal">
				
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>
			
				<div class="col-lg-7 col-lg-offset-4 col-md-7 col-md-offset-4 col-sm-12 col-xs-12 text-right">
				
					{{ $recipients -> appends( $_GET ) -> links() }}

				</div>
			
			</div>
		
			<div class="table-responsive">
			
				<table class="table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action" cellspacing="0" width="100%" style="margin:0px;">
				  
					<thead>
						
						<tr class="headings">
							
							<th class="text-center"> <input type="checkbox" id="select_all_recipients" name="select_all_recipients" class="flat" value="1"> </th>
							
							<th class="column-title text-center"> <small> ID </small> </th>
							
							<th class="column-title"> <small> Person Full Name / Company Name </small> </th>

							<th class="column-title"> <small> Contact # </small> </th>
							
							<th class="column-title"> <small> Relationship </small> </th>

							<th class="column-title"> <small> Remitter </small> </th>

							<th class="column-title no-link last text-center" width="8%"> <small> Action </small> </th>

						</tr>

					</thead>

					<tbody>
						
						@php ( $x = 1 )
					
						@foreach ( $recipients as $key => $recipient )
						
							<tr class="{{ ( $x % 2 ? 'even' : 'odd') }} pointer">
								
								<td class="text-center"> <input type="checkbox" id="recipient_{{ $recipient[ 'recipient_id' ] }}" name="recipient[]" class="flat recipient_checkbox" value="{{ $recipient[ 'recipient_id' ] }}"> </td>
								
								<td class="text-center"> <small> {{ $recipient[ 'recipient_id' ] }} </small> </td>

								<td> <small> {{ $recipient[ 'recipient_full_name' ] }} </small> </td>

								<td> <small> {{ $recipient[ 'recipient_contact' ] }} </small> </td>
								
								<td> <small> {{ ucfirst( $recipient[ 'recipient_relationship' ] ) }} </small> </td>
								
								<td> <small> {{ $recipient[ 'remitter_full_name' ] }} </small> </td>

								<td class="last text-center">
									
									<a href="{{ url( '/iremit/recipients/details/' . $recipient[ 'recipient_id' ] ) }}" class="btn btn-default btn-xs" data-id="{{ $recipient[ 'recipient_id' ] }}" target="_blank"> <i class="fa fa-folder-open"></i> </a>
										
									<a href="{{ url( '/iremit/recipients/modal/' . $recipient[ 'recipient_id' ] ) }}" class="btn btn-warning btn-xs show_modal_btn" data-id="{{ $recipient[ 'recipient_id' ] }}"> <i class="fa fa-pencil"></i> </a>

								</td>

							</tr>
							
							@php ( $x++ )
						
						@endforeach

					</tbody>
				
				</table>
			
			</div>
			
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>
			
				<div class="col-lg-7 col-lg-offset-4 col-md-7 col-md-offset-4 col-sm-12 col-xs-12 text-right">
				
					{{ $recipients -> appends( $_GET ) -> links() }}

				</div>
			
			</div>
			
		</form>
		
	@else
	
		<div class="alert alert-danger" role="alert" style="margin: 20px 0px 0px;">

			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No recipient list! </h5>

		</div>
	
	@endif
	
</div>
