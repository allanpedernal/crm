<div class="remittance_table_container">

	@if( isset( $remittances ) && ! empty( $remittances ) && COUNT( $remittances ) )
	
		<form method="POST" id="remittance_table" class="form-horizontal">
			
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>
			
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					
					<div class="input-group input-group-sm input-group-bulk-action" style="margin: 10px 0px 0px;">
						
						<span class="input-group-addon">
							
							Bulk action

						</span>

						<select class="remittance_bulk_action chosen-select form-control" data-placeholder="Please select action">

							<option value=""></option>
							
							<option value="note"> Note </option>
							
							<option value="forex_rate"> Forex rate </option>
							
							<option value="trade_rate"> Trade rate </option>

						</select>
						
						<span class="input-group-btn">
							
							<button class="btn btn-default btn-success bulk_action_btn" type="button"> <i class="fa fa-check"></i> Proceed </button>

						</span>

					</div>

				</div>

				<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 text-right">
				
					{{ $remittances -> appends( $_GET ) -> links() }}

				</div>
			
			</div>
			
			<div class="table-responsive">
					
				<table class="table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action" cellspacing="0" width="100%" style="margin:0px;">
				  
					<thead>
						
						<tr class="headings">
							
							<th width="2%" class="text-center"> <input type="checkbox" id="select_all_remittances" name="select_all_remittances" class="flat" value="1"> </th>
							
							<th width="8%" class="column-title text-center"> <small title="Transaction number"> TN  </small> </th>
							
							<th width="12%" class="column-title text-center"> <small title="Creation date"> CD </small> </th>
							
							<th width="10%" class="column-title text-center"> <small title="Receive method"> RM </small> </th>
							
							<th width="4%" class="column-title text-center"> <small title="Bank source"> BS </small> </th>
							
							<th width="2%" class="column-title text-center"> <small title="Forex exchange"> FE </small> </th>
							
							<th width="3%" class="column-title text-center"> <small title="Service fee"> SF </small> </th>
							
							<th width="6%" class="column-title text-center"> <small title="AUD"> AUD </small> </th>
							
							<th width="8%" class="column-title text-center"> <small title="PHP"> PHP </small> </th>
							
							<th width="11%" class="column-title text-center"> <small title="Status"> ST </small> </th>

							<th width="11%" class="column-title no-link last text-center"> <small> Action </small> </th>

						</tr>

					</thead>
					
					<tbody>
						
						@php ( $total_service_fee = 0 )
						
						@php ( $total_aud = 0 )
						
						@php ( $total_php = 0 )
						
						@php ( $x = 1 )
					
						@foreach ( $remittances as $key => $remittance )
							
							<tr class="{{ ( $x % 2 ? 'even' : 'odd') }} pointer">
								
								<td class="text-center"> <input type="checkbox" id="remittance_{{ $remittance[ 'remittance_id' ] }}" name="remittance[]" class="flat remittance_checkbox" value="{{ $remittance[ 'remittance_id' ] }}"> </td>
								
								<td class="text-center"> <small style="font-weight: 700;"> {{ $remittance[ 'transaction_no' ] }} </small> </td>
								
								<td class="text-center"> <small> {{ date( 'M j, Y g:i A', strtotime( $remittance[ 'date' ] ) ) }} </small> </td>
								
								<td class="text-center"> <small> {{ $remittance[ 'receive_method' ] }} </small> </td>
								
								<td class="text-center"> <small> {{ $remittance[ 'bank_source' ] }} </small> </td>
								
								<td class="text-center"> <small style="font-weight: 700;"> &#8369;{{ number_format( $remittance[ 'forex_rate' ], 2, '.', ',' ) }} </small> </td>

								<td class="text-center"> <small style="font-weight: 700;"> &#36;{{ ( empty( $remittance[ 'service_fee' ] ) || ( $remittance[ 'service_fee' ] == 0 ) ? "0.00" : number_format( $remittance[ 'service_fee' ], 2, '.', ',' ) ) }} </small> </td>
								
								<td class="text-center"> <small style="font-weight: 700;"> &#36;{{ number_format( $remittance[ 'aud' ], 2, '.', ',' ) }} </small> </td>
								
								<td class="text-center"> <small style="font-weight: 700;"> &#8369;{{ number_format( $remittance[ 'php' ], 2, '.', ',' ) }} </small> </td>
								
								<td class="text-center"> <small> {{ $remittance[ 'status' ] }} </small> </td>

								<td class="last text-center">
									
									<a href="{{ url( '/iremit/remittances/brs/' . $remittance[ 'remittance_id' ] ) }}" class="btn btn-default btn-xs" data-id="{{ $remittance[ 'remittance_id' ] }}" target="_blank"> <i class="fa fa-folder-open"></i> </a>

									<a href="{{ url( '/iremit/remittances/modal/' . $remittance[ 'remittance_id' ] ) }}" class="btn btn-warning btn-xs show_modal_btn" data-id="{{ $remittance[ 'remittance_id' ] }}"> <i class="fa fa-pencil"></i> </a>

									<a href="{{ url( '/iremit/remittances/details/' . $remittance[ 'remittance_id' ] ) }}" class="btn btn-info btn-xs" data-id="{{ $remittance[ 'remittance_id' ] }}"> <strong> BRS </strong> </a>

								</td>
							
							</tr>
							
							@php ( $total_service_fee += $remittance[ 'service_fee' ] )
							
							@php ( $total_aud += $remittance[ 'aud' ] )
							
							@php ( $total_php += $remittance[ 'php' ] )
							
							@php ( $x++ )
							
						@endforeach
					
					</tbody>
				
					<tfoot>
					
						<tr>
						
							<td class="text-right" colspan="6"> <small style="font-weight: 700;"> TOTALS: </small> </td>
							
							<td class="text-center"> <small style="font-weight: 700;"> &#36;{{ number_format( round( $total_service_fee, 2 ), 2, '.', ',' ) }} </small> </td>
							
							<td class="text-center"> <small style="font-weight: 700;"> &#36;{{ number_format( round( $total_aud, 2 ), 2, '.', ',' ) }} </small> </td>
							
							<td class="text-center"> <small style="font-weight: 700;"> &#8369;{{ number_format( round( $total_php, 2 ), 2, '.', ',' ) }} </small> </td>
							
							<td colspan="2"></td>
							
						</tr>
					
					</tfoot>
				
				</table>
				
			</div>
			
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>
			
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					
					<div class="input-group input-group-sm input-group-bulk-action" style="margin: 10px 0px 0px;">
						
						<span class="input-group-addon">
							
							Bulk action

						</span>

						<select class="remittance_bulk_action chosen-select form-control" data-placeholder="Please select action">

							<option value=""></option>
							
							<option value="note"> Note </option>
							
							<option value="forex_rate"> Forex rate </option>
							
							<option value="trade_rate"> Trade rate </option>

						</select>
						
						<span class="input-group-btn">
							
							<button class="btn btn-default btn-success bulk_action_btn" type="button"> <i class="fa fa-check"></i> Proceed </button>

						</span>

					</div>

				</div>

				<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 text-right">
				
					{{ $remittances -> appends( $_GET ) -> links() }}

				</div>
			
			</div>
			
		</form>
		
	@else

		<div class="alert alert-danger" role="alert" style="margin: 20px 0px 0px;">

			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No remittance list! </h5>

		</div>

	@endif

</div>
