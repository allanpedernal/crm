<div class="remitter_table_container">

	@if( isset( $remitters ) && ! empty( $remitters ) && COUNT( $remitters ) )
		
		<form method="POST" id="remitter_table" class="form-horizontal">
				
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>

				<div class="col-lg-7 col-lg-offset-4 col-md-7 col-md-offset-4 col-sm-12 col-xs-12 text-right">
					
					{{ $remitters -> appends( $_GET ) -> links() }}

				</div>

			</div>

			<div class="table-responsive">
					
				<table class="table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action" cellspacing="0" width="100%" style="margin:0px;">
				  
					<thead>
						
						<tr class="headings">
							
							<th class="text-center"> <input type="checkbox" id="select_all_remitters" name="select_all_remitters" class="flat" value="1"> </th>
							
							<th class="column-title text-center"> <small> ID </small> </th>
							
							<th class="column-title"> <small> Firstname </small> </th>
							
							<th class="column-title"> <small> Lastname </small> </th>
							
							<th class="column-title"> <small> Email </small> </th>
							
							<th class="column-title"> <small> Contact # </small> </th>
							
							<th class="column-title text-center"> <small> <i class="fa fa-envelope-o" aria-hidden="true"></i> Verified </small> </th>
							
							<th class="column-title text-center"> <small> <i class="fa fa-mobile" aria-hidden="true"></i> Verified </small> </th>

							<th class="column-title no-link last text-center" width="8%"> <small> Action </small> </th>

						</tr>

					</thead>
					
					<tbody>
						
						@php ( $x = 1 )
					
						@foreach ( $remitters as $key => $remitter )
							
							<tr class="{{ ( $x % 2 ? 'even' : 'odd') }} pointer">
								
								<td class="text-center"> <input type="checkbox" id="remitter_{{ $remitter[ 'remitter_id' ] }}" name="remitter[]" class="flat remitter_checkbox" value="{{ $remitter[ 'remitter_id' ] }}"> </td>
								
								<td class="text-center"> <small> {{ $remitter[ 'remitter_id' ] }} </small> </td>
								
								<td> <small> {{ $remitter[ 'remitter_firstname' ] }} </small> </td>
								
								<td> <small> {{ $remitter[ 'remitter_lastname' ] }} </small> </td>
								
								<td> <small> {{ $remitter[ 'remitter_email' ] }} </small> </td>
								
								<td> <small> {{ $remitter[ 'remitter_contact' ] }} </small> </td>
								
								<td class="text-center"> <small> {!! ( $remitter[ 'remitter_email_verified' ] == 1 ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-times" aria-hidden="true"></i>' ) !!} </small> </td>
								
								<td class="text-center"> <small> {!! ( $remitter[ 'remitter_sms_verified' ] == 1 ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-times" aria-hidden="true"></i>' ) !!} </small> </td>

								<td class="last text-center">
								
									<a href="{{ url( '/iremit/remitters/profile/' . $remitter[ 'remitter_id' ] ) }}" class="btn btn-default btn-xs" data-id="{{ $remitter[ 'remitter_id' ] }}" target="_blank"> <i class="fa fa-folder-open"></i> </a>
									
									<a href="{{ url( '/iremit/remitters/modal/' . $remitter[ 'remitter_id' ] ) }}" class="btn btn-warning btn-xs show_modal_btn" data-id="{{ $remitter[ 'remitter_id' ] }}"> <i class="fa fa-pencil"></i> </a>
									
								</td>
							
							</tr>
							
							@php ( $x++ )
							
						@endforeach
					
					</tbody>
				
				</table>
				
			</div>
			
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>

				<div class="col-lg-7 col-lg-offset-4 col-md-7 col-md-offset-4 col-sm-12 col-xs-12 text-right">
					
					{{ $remitters -> appends( $_GET ) -> links() }}

				</div>

			</div>
			
		</form>
		
	@else

		<div class="alert alert-danger" role="alert" style="margin: 20px 0px 0px;">

			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No remitter list! </h5>

		</div>

	@endif

</div>
