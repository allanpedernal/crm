@if( isset( $remittanceResult ) && ! empty( $remittanceResult ) && COUNT( $remittanceResult ) )

	<div class="table-responsive">
			
		<table id="datatable-responsive" name="table-risk-alert" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		  
			<thead>
				
				<tr>

					<th class="text-center"> Transaction Id </th>
					
					<th class="text-center"> Transaction Number </th>

					<th class="text-center"> Remitter </th>

					<th class="text-center"> Payment Type </th>
					
					<th class="text-center"> Bank Source </th>

					<th class="text-center"> Amount Sent </th>
					
					<th class="text-center"> Status </th>
					
					<th class="text-center"> Date Transferred </th>

				</tr>

			</thead>

			<tbody name="tbody-risk-alert" class="text-center">

	 			@foreach( $remittanceResult as $remittance )
				
		            <tr>

						<td> {{ $remittance->id }} </td>

						<td> {{ $remittance->transaction_number }} </td>

						<td> {{ isset( $remittance->remitter ) && ! empty( $remittance->remitter ) ? $remittance->remitter->firstname.' '.$remittance->remitter->lastname  : '' }} </td>

						<td> {{ $remittance->payment_type }} </td>	

						<td> {{ $remittance->bank_source_id }} </td> <!-- should be $remittance->bank_source->name -->

						<td> {{ $remittance->amount_sent }} </td>

						<td> {{ $remittance->status->name }} </td>

						<td> {{ $remittance->date_transferred }} </td>
						
	{{-- 					<td class="text-center">
							
							<a class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i> View</a>
							
							<a class="btn btn-warning btn-xs" name="btn-risk-alert-edit" data-risk-alert-id="{{$remittance->id}}"><i class="fa fa-pencil"></i> Edit</a>
							
							<a class="btn btn-danger btn-xs" name="btn-risk-alert-delete" data-risk-alert-id="{{$remittance->id}}"><i class="fa fa-trash"></i> Delete</a>
							
						</td> --}}

		            </tr>	

				@endforeach

			</tbody>
		
		</table>

	</div>

	<div class="row" name="div-pagination-links">

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" name='paginate' value='paginateLinks' >
		
			 {{ $remittanceResult->links() }}	 

		</div>

	</div>

@else

	<div name="div-no-remittance" class="alert alert-danger" role="alert" style="margin:0px;">

		<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No existing remittance! </h5>

	</div>

@endif


