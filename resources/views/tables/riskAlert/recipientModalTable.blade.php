@if( isset( $recipientRemittances ) && ! empty( $recipientRemittances ) && COUNT( $recipientRemittances ) )

	<div class="table-responsive">
			
		<table id="datatable-responsive" name="table-risk-alert" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		  
			<thead>
				
				<tr>

					<th class="text-center"> Recipient Name </th>

					<th class="text-center"> Remittance Id </th>

					<th class="text-center"> Transaction No. </th>

					<th class="text-center"> Remitter Name </th>	

					<th class="text-center"> Relationship </th>

					<th class="text-center"> Amount </th>

					<th class="text-center"> Source of Income </th>

					<th class="text-center"> Purpose </th>

					<!--<th class="text-center"> Bank </th>-->

					<th class="text-center"> Date </th>

				</tr>

			</thead>

			<tbody name="tbody-risk-alert" class="text-center">

	 			@foreach( $recipientRemittances as $remittance )
				
		            <tr>

						<td>
							
							{{ isset( $remittance->recipient ) && ! empty( $remittance->recipient ) ? ( isset( $remittance->recipient->fullname ) && ! empty( $remittance->recipient->fullname ) ? $remittance->recipient->fullname : '' ) : '' }} 

						</td>

						<td> {{ $remittance->id }} </td>

						<td> {{ $remittance->transaction_number }} </td>

						<td> 

							{{ isset( $remittance->remitter ) && ! empty( $remittance->remitter ) ? $remittance->remitter->firstname.' '.$remittance->remitter->lastname  : '' }} 

						</td>

						<td>
							{{ isset( $remittance->recipient ) && ! empty( $remittance->recipient ) ? ( isset( $remittance->recipient->remitter_relationship_id ) && ! empty( $remittance->recipient->remitter_relationship_id ) ? $remittance->recipient->relationship->name : '' ) : '' }} 

						</td>

						<td> {{ $remittance->amount_sent }} </td>

						<td>

							{{ isset( $remittance->source_of_fund ) && ! empty( $remittance->source_of_fund ) ? $remittance->source_of_fund->name : '' }}

						</td>

						<td>

							{{ isset( $remittance->reason_id ) && ! empty( $remittance->reason_id ) ? $remittance->reason->name : '' }}

						</td>

						{{-- <td> {{ $remittance->bank_source->name }} </td> --}}

						<td> {{ $remittance->date_transferred }} </td>

		            </tr>	

				@endforeach

			</tbody>
		
		</table>

	</div>

	<div class="row" name="div-pagination-links">

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" name='paginate' value='paginateLinks' >
		
			 {{ $recipientRemittances->links() }}	 

		</div>

	</div>

@else

	<div name="div-no-remittance" class="alert alert-danger" role="alert" style="margin:0px;">

		<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No existing remittance! </h5>

	</div>

@endif