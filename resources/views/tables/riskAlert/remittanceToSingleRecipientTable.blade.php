@if( isset( $remittanceToSingleRecipient ) && ! empty( $remittanceToSingleRecipient ) && COUNT( $remittanceToSingleRecipient ) )

	<div class="table-responsive">
			
		<table id="datatable-responsive" name="table-risk-alert" data-type="remittanceToRecipient" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		  
			<thead>
				
				<tr>

					<th class="text-center"> Recipient Id </th>
					
					<th class="text-center"> Recipient Name </th>

					<th class="text-center"> Last Transaction </th>	

					<th class="text-center"> Transaction Recieved </th>

				</tr>

			</thead>

			<tbody name="tbody-risk-alert" class="text-center">

	 			@foreach( $remittanceToSingleRecipient as $remittance )
				
		            <tr>

						<td> {{ $remittance->recipient_id }} </td>

						<td>
							
							<a href='#page=1' name='riskAlertRecipient' data-recipient-id='{{ $remittance->recipient_id }}' class='show-transactions' data-toggle="tooltip" title="" onMouseOver='this.style.fontWeight = "bold"' onMouseOut='this.style.fontWeight = "normal"' >{{ isset( $remittance->recipient ) && ! empty( $remittance->recipient ) ? ( isset( $remittance->recipient->fullname ) && ! empty( $remittance->recipient->fullname ) ? $remittance->recipient->fullname : '' ) : '' }} 
							</a>

						</td>

						<td> {{ $remittance->dateTransferred }} </td>

						<td> {{ $remittance->recipient_count }} </td>

		            </tr>	

				@endforeach

			</tbody>
		
		</table>

	</div>

	<div class="row" name="div-pagination-links">

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" name='paginate' value='paginateLinks' >
		
			 {{ $remittanceToSingleRecipient->links() }}	 

		</div>

	</div>

@else

	<div name="div-no-remittance" class="alert alert-danger" role="alert" style="margin:0px;">

		<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No existing remittance! </h5>

	</div>

@endif


