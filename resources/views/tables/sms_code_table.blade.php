<div class="sms_code_table_container">

	@if( isset( $sms_codes ) && ! empty( $sms_codes ) && COUNT( $sms_codes ) )
	
		<form method="POST" id="sms_code_table" class="form-horizontal">
			
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>

				<div class="col-lg-7 col-lg-offset-4 col-md-7 col-md-offset-4 col-sm-12 col-xs-12 text-right">
					
					{{ $sms_codes -> appends( $_GET ) -> links() }}

				</div>

			</div>
			
			<div class="table-responsive">
					
				<table class="table table-striped table-bordered dt-responsive nowrap jambo_table bulk_action" cellspacing="0" width="100%" style="margin:0px;">

					<thead>
						
						<tr class="headings">
							
							<th class="text-center"> <input type="checkbox" id="select_all_sms_codes" name="select_all_sms_codes" class="flat" value="1"> </th>
							
							<th class="column-title text-center"> <small> ID </small> </th>
							
	            			<th class="column-title"> <small> Fullname </small> </th>
	            			
	            			<th class="column-title"> <small> Email </small> </th>
	            			
	            			<th class="column-title"> <small> Code </small> </th>
	            			
	            			<th class="column-title text-center"> <small> Tries </small> </th>
	            			
	            			<th class="column-title text-center"> <small> Date </small> </th>
	            			
	            			<th class="column-title no-link last text-center"> <small> Action </small> </th>
	            			
						</tr>

					</thead>
					
					<tbody>
						
						@php ( $x = 1 )
						
						@foreach ( $sms_codes as $key => $sms_code )
						
							<tr class="{{ ( $x % 2 ? 'even' : 'odd') }} pointer">
							
								<td class="text-center"> <input type="checkbox" id="sms_code_{{ $sms_code[ 'id' ] }}" name="sms_code[]" class="flat sms_code_checkbox" value="{{ $sms_code[ 'id' ] }}"> </td>
								
								<td class="text-center"> <small> {{ $sms_code[ 'id' ] }} </small> </td>
								
								<td> <small> {{ $sms_code[ 'remitter_fullname' ] }} </small> </td>
								
								<td> <small> {{ $sms_code[ 'remitter_email' ] }} </small> </td>
								
								<td> <small> {{ $sms_code[ 'sms_code' ] }} </small> </td>
								
								<td class="text-center"> <small> {{ $sms_code[ 'tries' ] }} </small> </td>
								
								<td class="text-center"> <small> {{ date( 'M j, Y g:i A', strtotime( $sms_code[ 'date' ] ) ) }} </small> </td>
								
								<td class="last text-center">
								
									<a href="{{ url( '/codes/sms/verify/' . $sms_code[ 'id' ] ) }}" class="btn btn-success btn-xs verify_btn" data-id="{{ $sms_code[ 'id' ] }}" target="_blank"> <i class="fa fa-check"></i> </a>
									
									<a href="{{ url( '/codes/sms/modal/' . $sms_code[ 'id' ] ) }}" class="btn btn-warning btn-xs show_modal_btn" data-id="{{ $sms_code[ 'id' ] }}"> <i class="fa fa-pencil"></i> </a>
									
								</td>
								
							</tr>
							
							@php ( $x++ )
						
						@endforeach
					
					</tbody>
				 
				</table>
				
			</div>
			
			<div class="row">
				
				<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
					
					<button class="btn btn-xs btn-danger" type="submit" style="margin: 15px 0px 5px;"> <i class="fa fa-trash"></i> Delete </button>
				
				</div>

				<div class="col-lg-7 col-lg-offset-4 col-md-7 col-md-offset-4 col-sm-12 col-xs-12 text-right">
					
					{{ $sms_codes -> appends( $_GET ) -> links() }}

				</div>

			</div>
			
		</form>
	
	@else 
	
		<div class="alert alert-danger" role="alert" style="margin: 20px 0px 0px;">

			<h5 class="text-center" style="margin:5px 0px;"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No sms code list! </h5>

		</div>
	
	@endif
	
</div>
