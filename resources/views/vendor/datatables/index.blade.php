@extends('layouts.iremit')

@section('content')

    <table class="table table-bordered" id="users-table">
		
        <thead>
			
            <tr>
				
                <th> ID </th>
                
                <th> Login </th>
                
                <th> Nicename </th>
                
                <th> Email </th>
                
                <th> Display Name </th>
                
                <th> </th>
                
            </tr>
            
        </thead>
        
    </table>
    
@endsection

@section('styles')

	<link href="/bower_components/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

	<link href="/bower_components/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">

	<link href="/bower_components/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">

	<link href="/bower_components/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">

	<link href="/bower_components/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

@endsection

@section('scripts')

<script src="/bower_components/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>

<script src="/bower_components/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

<script src="/bower_components/gentelella/vendors/jszip/dist/jszip.min.js"></script>

<script src="/bower_components/gentelella/vendors/pdfmake/build/pdfmake.min.js"></script>

<script src="/bower_components/gentelella/vendors/pdfmake/build/vfs_fonts.js"></script>

<script type="text/javascript">
	
jQuery(function() {
	
    $('#users-table').DataTable({
		
        processing: true,
        
        serverSide: true,
        
        ajax: '/data',
        
        columns: [
        
            { data: 'ID', name: 'ID' },
            
            { data: 'user_login', name: 'user_login' },
            
            { data: 'user_nicename', name: 'user_nicename' },
            
            { data: 'user_email', name: 'user_email' },
            
            { data: 'display_name', name: 'display_name' }
            
        ],
        
        columnDefs: [ {
			
            targets: -1,
            
            data: null,
            
            defaultContent: "<button>Click!</button>"
            
        } ]
        
    });
    
});

</script>
@endsection
