<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//AUTH
Auth::routes();
Route::get('/',function(){return redirect('/login');});
Route::get('/logout',function(){ Auth::logout(); return redirect('/dashboard');});

//DASHBOARD
Route::get('/dashboard','DashboardController@index');

//REMITTER
Route::get('/iremit/remitters','RemittersController@index');
Route::get('/iremit/remitters/profile/{id_number?}',['uses'=>'RemittersController@showProfile']);
Route::get('/iremit/remitters/profile/get-avatar/{id?}',['uses'=>'RemittersController@getProfileAvatar']);
Route::get('/iremit/remitters/identification/get-images/{id?}',['uses'=>'RemittersController@getIdentificationImages']);
Route::get('/iremit/remitters/company/get-authorization/{id?}',['uses'=>'RemittersController@getCompanyAuthorization']);
Route::get('/iremit/remitters/modal/{id?}',['uses'=>'RemittersController@getModal']);
Route::post('/iremit/remitters/delete-remitters',['uses'=>'RemittersController@deleteRemitters']);
Route::post('/iremit/remitters/form/post',['uses'=>'RemittersController@postForm']);

//RECIPIENT
Route::get('/iremit/recipients','RecipientsController@index');
Route::get('/iremit/recipients/modal/{id?}',['uses'=>'RecipientsController@getModal']);
Route::post('/iremit/recipients/delete-recipients',['uses'=>'RecipientsController@deleteRecipients']);
Route::post('/iremit/recipients/get-remitter',['uses'=>'RecipientsController@getRemitters']);
Route::post('/iremit/recipients/form/post',['uses'=>'RecipientsController@postForm']);

//REMITTANCE
Route::get('/iremit/remittances','RemittancesController@index');
Route::get('/iremit/remittances/modal/{id?}',['uses'=>'RemittancesController@getModal']);
Route::get('/iremit/remittances/details/{id?}',['uses'=>'RemittancesController@showDetails']);
Route::get('/iremit/remittances/get-remitter-full-details/{remitter_id?}',['uses'=>'RemittancesController@getRemitterFullDetails']);
Route::get('/iremit/remittances/get-recipient-full-details/{recipient_id?}',['uses'=>'RemittancesController@getRecipientFullDetails']);
Route::post('/iremit/remittances/delete-remittances',['uses'=>'RemittancesController@deleteRemittances']);
Route::post('/iremit/remittances/get-remitter',['uses'=>'RemittancesController@getRemitters']);
Route::post('/iremit/remittances/get-recipient',['uses'=>'RemittancesController@getRecipients']);
Route::post('/iremit/remittances/get-remitter-recipient',['uses'=>'RemittancesController@getRemitterRecipients']);
Route::post('/iremit/remittances/get-receive-option-list',['uses'=>'RemittancesController@getReceiveOptionList']);
Route::post('/iremit/remittances/form/post',['uses'=>'RemittancesController@postForm']);
Route::post('/iremit/remittances/poli-nudge',['uses'=>'RemittancesController@poliNudge']);
Route::post('/iremit/remittances/bulk-action',['uses'=>'RemittancesController@bulkAction']);

//PROMO CODE
Route::get('/codes/promos/','PromoCodesController@index');
Route::get('/codes/promos/modal/{id?}',['uses'=>'PromoCodesController@getModal']);
Route::get('/codes/promos/data',['uses'=>'PromoCodesController@getData']);
Route::post('/codes/promos/form/post',['uses'=>'PromoCodesController@postForm']);
Route::get('/codes/promos/test/{id?}',['uses'=>'PromoCodesController@test']);

//SMS CODE
Route::get('/codes/sms','SMSController@index');
Route::get('/codes/sms/modal/{id?}',['uses'=>'SMSController@getModal']);
Route::get('/codes/sms/verify/{id?}',['uses'=>'SMSController@verify']);
Route::post('/codes/sms/delete-sms-codes',['uses'=>'SMSController@deleteSMSCodes']);
Route::post('/codes/sms/get-remitter',['uses'=>'SMSController@getRemitters']);
Route::post('/codes/sms/form/post',['uses'=>'SMSController@postForm']);

/* MOBILE FUNCTIONALITY START */
Route::get('/mobile/promotions','MobilePromotionsController@index');
Route::get('/mobile/promotions/modal/{id?}',['uses'=>'MobilePromotionsController@getModal']);
Route::get('/mobile/promotions/send/{id?}',['uses'=>'MobilePromotionsController@sendPromotion']);
Route::post('/mobile/promitions/delete-promotions',['uses'=>'MobilePromotionsController@deletePromotions']);
Route::post('/mobile/promotions/form/post',['uses'=>'MobilePromotionsController@postForm']);
/* MOBILE FUNCTIONALITY END */

//NOTIFICATION
Route::get('/notifications','NotificationsController@index');
Route::get('/promotions/modal/{id?}',['uses'=>'NotificationsController@getModal']);
Route::get('/notifications/data',['uses'=>'NotificationsController@getData']);
Route::post('/notifications/form/post',['uses'=>'NotificationsController@postForm']);
Route::get('/notifications/sendpushnotification/{remittance_id?}',['uses'=>'NotificationsController@send_push_notification']);
Route::get('/notifications/testpushnotification/{remittance_id?}',['uses'=>'NotificationsController@test']);

//CONNECTS
Route::get('/connects/bdo','BDOController@index');
Route::get('/connects/polipayment','PolipaymentController@index');

//SETTING
Route::get('/settings/rates-and-fees','SettingsController@index');
Route::post('/settings/form/post','SettingsController@postForm');

//UPLOAD ROUTE
Route::post('/upload/remitter_id/temp',['as'=>'dropzone.remitter.id.temp','uses'=>'UploadController@dropzoneRemitterIdTemp']);
Route::post('/upload/remitter_avatar/temp',['as'=>'dropzone.remitter.avatar.temp','uses'=>'UploadController@dropzoneRemitterAvatarTemp']);
Route::post('/upload/remitter_company_authorization/temp',['as'=>'dropzone.remitter.company.authorization.temp','uses'=>'UploadController@dropzoneRemitterCompanyAuthorizationTemp']);
Route::post('/upload/remittance_receipt/temp',['as'=>'dropzone.remittance.receipt.temp','uses'=>'UploadController@dropzoneRemittanceReceiptTemp']);
Route::post('/upload/remittance_additional_requirement/temp',['as'=>'dropzone.remittance.addtional_requirement.temp','uses'=>'UploadController@dropzoneRemittanceAdditionalReqirementTemp']);

//MIGRATION ROUTE
Route::get('/migrate/remitter',['uses'=>'MigrationController@migrateRemitter']);
Route::get('/migrate/recipient',['uses'=>'MigrationController@migrateRecipient']);
Route::get('/migrate/remittance',['uses'=>'MigrationController@migrateRemittance']);
Route::get('/migrate/promo-code',['uses'=>'MigrationController@migratePromoCode']);
Route::get('/migrate/mobile-promotion',['uses'=>'MigrationController@migrateMobilePromotion']);
Route::get('/migrate/library-city',['uses'=>'MigrationController@migrateLibraryCity']);
Route::get('/migrate/remove-recipient-dup',['uses'=>'MigrationController@migrateRemoveRecipientDup']);

//DATATABLES
Route::get('/datatables',['uses'=>'DatatablesController@getIndex']);
Route::get('/data',['uses'=>'DatatablesController@anyData']);

//LIBRARIES
Route::get('iremit/library/get',['uses'=>'LibraryController@getLibrary']);
Route::get('iremit/library/get-countries',['uses'=>'LibraryController@getCountries']);
Route::get('iremit/library/get-states-from-country/{country_id}/{hash?}',['uses'=>'LibraryController@getStatesFromCountry']);
Route::get('iremit/library/get-cities-from-state/{state_id}/{hash?}',['uses'=>'LibraryController@getCitiesFromState']);
Route::get('iremit/library/get-postal-code-from-city/{city_id}/{hash?}',['uses'=>'LibraryController@getPostalFromCity']);

//AUTORESPONDERS
Route::get( '/register',  ['uses'=>'AutoResponderController@register'] );
Route::post( '/register', ['uses'=>'AutoResponderController@postForm'] );
Route::get( '/autoresponder/{id_type?}/{id?}', ['uses'=>'AutoResponderController@send_auto_responder'] );
Route::get( '/autoresponder/test/{id?}', ['uses'=>'AutoResponderController@test'] );
Route::get( '/mailable/test', ['uses'=>'MailController@test'] );

//RISK ALERT 
Route::get( '/riskalert',  ['uses'=>'RiskAlertController@index'] );
Route::get( '/riskalert/get',  ['uses'=>'RiskAlertController@get'] );
Route::get( '/riskalert/download/remitterRemittance/{startDate?}/{endDate?}/{remitterId?}',  ['uses'=>'RiskAlertController@exportRemitterRemittance'] );
Route::get( '/riskalert/download/recipientRemittance/{startDate?}/{endDate?}/{recipientId?}',  ['uses'=>'RiskAlertController@exportRecipientRemittance'] );
Route::get( '/riskalert/test/{date}',  ['uses'=>'RiskAlertController@test'] );
